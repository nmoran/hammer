#!/bin/bash

set -e


if [[ "$2" == "recurse" ]]; then
    ##echo "First version"
    initial='F'

else
    ##echo "Second Version"
    initial='T'
fi

set -u

variable=$1
my_name=$(basename $0)

###Find first list of dependencies
DepList=$(cat dependencies | grep ^$variable: | awk '{for(i=2;i<=NF;i++){print $i}}')
##echo Dependencylist: $DepList
if [[ "$initial" == "F" ]]; then
    echo $DepList
fi

Full_List=""
for D in $DepList ; do
    ##   echo Dependency: $D
    Part_List=$(eval ./$my_name "$D" recurse)
    Full_List="${Full_List} ${Part_List}"
done

if [[ "$initial" == "T" ]]; then
    ##Echo the full list
    Full_List=$(echo $DepList $variable $Full_List | tr ' ' '\n' | sort -u)
    echo $Full_List
else
    echo $Full_List
fi
