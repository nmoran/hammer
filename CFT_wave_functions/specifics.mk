###Library flags
LIBS := -larpack -llapack -lblas
###Compiler
F90 := gfortran
###Compiler flags
F90FLAGS := -O3 
###Linker flags ###Use -pg for profiling
LDFLAGS := 
###OpenMP flag
OMPFLAG := -fopenmp
###Where to put the modules
MODFLAG := -J$(DIR_BLD_F90) -I$(DIR_BLD_F90)

###Fortran 90 flags
F90DBFLAGS := -Wall #-fbounds-check  -g # -fbacktrace -finit-real=nan
###To debug using the gnu profiler add the two flags lines bellow
##F90DBFLAGS := -g -pg $(F90DBFLAGS) 
##LDFLAGS := -pg $(LDFLAGS) 

##### C-FLAGS
CXX=g++
DISABLE=
MYCXXFLAGS= -msse2 -std=c++11 -O2 -Wall
CXXLDFLAGS= -lgsl -lgslcblas -lm -lhdf5 -lhdf5_cpp
HDF5_DIR_INCLUDE= -I /usr/include/hdf5/serial
###This is the path in 16.04####
HDF5_LIBS=-L/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5 -lhdf5_cpp
MYCXXLDFLAGS :=  -lgfortran 
###Use  -pg for profiling
##MYCXXLDFLAGS :=  -pg $(MYCXXLDFLAGS)
ifeq ($(cmp),intel)
	CXX=icpc
	DISABLE=-diag-disable 858
endif
