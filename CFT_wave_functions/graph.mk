###Rules for prodetermining the dependency graph
.PHONY: graph
graph: dependency-graph.png dependency-graph_with_tests.png

GRAPH_PROGRAM = dot
PATH_DOT := $(type -p $(GRAPH_PROGRAM))
##FIXME the above construc is wrong, to the bellow is use right now
PATH_DOT := "/usr/bin/dot"

###Inference Rules for making .png from .txt files
%.png : %.txt
#command -v $(GRAPH_PROGRAM) && $(GRAPH_PROGRAM) -Tpng $< -o $@ 
	-@command -v $(GRAPH_PROGRAM) >/dev/null 2>&1 && $(GRAPH_PROGRAM) -Tpng $< -o $@ || { echo >&2 "The program '$(GRAPH_PROGRAM)' is not installed, so no graphics will be produced. Please install it using:   sudo apt-get install graphviz"; }
	-@ ###Just so the earlier can be seen
	-@ echo $(GRAPH_PROGRAM) -Tpng $< -o $@	



##ifneq (,$(findstring  $(GRAPH_PROGRAM),$(PATH_DOT)))
##	 $(GRAPH_PROGRAM) -Tpng $< -o $@ 
##else
##Compiling has NOT been with the -pg flag so it needs to be set
##	-@echo "The program '$(GRAPH_PROGRAM)' is not installed,"
##	-@echo "so no graphics will be produced"
##	-@echo "Please install it using:"
##	-@echo "   sudo apt-get install graphviz"
##endif

dependency-graph_with_tests.txt: dependencies dependencies_c mk.graph.data.sh
	./mk.graph.data.sh dependencies dependencies_c  > $@

dependency-graph.txt: dependency-graph_with_tests.txt
	grep 'test_' $< -v > $@

