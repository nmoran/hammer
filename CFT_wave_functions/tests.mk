TEST_DIR:=test_scripts

F90_Test_SRC:=$(wildcard $(DIR_SRC_F90)/test_suite_*.f90)
F90_Test_OBJ:=$(patsubst $(DIR_SRC_F90)/%.f90, $(DIR_BLD_F90)/%.o, $(F90_Test_SRC))
F90_Test_Programs:=$(patsubst $(DIR_SRC_F90)/%.f90, $(TEST_DIR)/.%, $(F90_Test_SRC))

.PHONY : test ##test is not a file, but the name of a rule
test:  graph test_suites test_output notest
TEST_SCRIPTS:=$(wildcard $(TEST_DIR)/test_script_*.sh)
TEST_SCRIPTS_TARGET:=$(patsubst $(TEST_DIR)/test%, $(TEST_DIR)/.test%, $(TEST_SCRIPTS))


#TEST THE COMAND LINE OUTPUT'
.PHONY : test_output
test_output: $(TEST_SCRIPTS_TARGET)

#TEST THE COMAND LINE OUTPUT'
.PHONY : test_suites
test_suites: $(F90_Test_Programs)

.PHONY : clean_tests
clean_tests:
	-@echo '..........................'	
	-@echo ' Removing test result files '
	-@echo '..........................'
	rm $(TEST_DIR)/.test_*

.DELETE_ON_ERROR: test_fortran.mk
test_fortran.mk:mk.test_fortran.sh dependencies
	-@echo 'Building Fortran Tests'
	./mk.test_fortran.sh $(TEST_DIR) $(DIR_SRC_F90) $(DIR_BLD_F90) > $@

.DELETE_ON_ERROR: test_scripts.mk
test_scripts.mk:mk.test_scripts.sh $(TEST_SCRIPTS)
	-@echo 'Building Test Script Tests'
	./mk.test_scripts.sh $(TEST_DIR)  mk.test_scripts.sh > $@


include test_fortran.mk
include test_scripts.mk
