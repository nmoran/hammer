###Intructions for debuging (that is profiling)
.PHONY : debug ##debug is not a file, but the name of a rule
###To do profiling the gmon-file needs to exist
debug: gmon.out $(Prog_Coord)
ifneq (,$(findstring pg,$(LDFLAGS)))
###Compiling has been with the -pg flag so all is well
	gprof $(Prog_Coord) gmon.out > profiling.txt
	less profiling.txt 
else
###Compiling has NOT been with the -pg flag so it needs to be set
	-@echo "The flag -pg has NOT been used in linkage"
	-@echo "Please enable it by un-commenting some lines above"
	-@echo "Only then can profiling be enabled"
endif

###The gmon-file is computed through running the test_cases
gmon.out: $(Prog_Coord)
	""./$(Prog_Coord) -Ne 6 -D 2 --Kminus 3 2 2 3 -KL 3 -N 10000
	./$(Prog_Coord) -Ne 6 -D 2 --Kminus 1 2 2 1 --Kbar 1 0 0 1 -KL 3 -N 10000

