# No longer reuquired as this is set in the module file.
#MKLROOT=/ichec/packages/intel/2015.u1/composer_xe_2015.1.133/mkl

###Library flags
LIBS := -L/ichec/work/nmphy011b/Codes/ARPACK -larpack -L$(MKLROOT)/lib/intel64 -lmkl_intel_thread -lmkl_intel_lp64 -lmkl_core
###Compiler
F90 := ifort
###Compiler flags
F90FLAGS := -O3 -xHost 
###Linker flags
LDFLAGS := -fopenmp #-pg 
###OpenMP flag
OMPFLAG := -fopenmp
###Where to put the modules
MODFLAG := -module $(DIR_BLD_F90)

###Fortran 90 flags
F90DBFLAGS := #-fbounds-check -Wall -g # -fbacktrace -finit-real=nan
###To debug using the gnu profiler add the two flags lines bellow
##F90DBFLAGS := -g -pg $(F90DBFLAGS) 
##LDFLAGS := -pg $(LDFLAGS) 

### CFLAGS
CXX=icpc
HDF5_DIR_INCLUDE= -I {HDF5_DIR}/include
MYCXXFLAGS= -I${CPATH} -O3 -xHost -std=c++11
DISABLE=-diag-disable 858
CXXLDFLAGS=-lgsl -lgslcblas ${MYCXXLDFLAGS} 
HDF5_LIBS=-L${HDF5_DIR}/lib -lhdf5 -lhdf5_cpp
MYCXXLDFLAGS= -L${LD_LIBRARY_PATH} -lsvml -lintlc -lirng -limf -lifcore ${LIBS}                                                         
