#!/bin/bash
## This script lists all the dependencies that different f.90 files have
## FIXME: same module can be lsited twise (un-neccesary) (not a problem for make though)

set -e
set -u

SRC_F90=$1
BLD_F90=$2


##Fortran Files
for f in ${SRC_F90}/*.f90 ; do
    ## Replace .f90 with .o
    ## Replace source dir by build dir
    file=$(echo $f | sed 's|\.f90|.o|' | sed "s|${SRC_F90}/|${BLD_F90}/|")
    ## Search through .f90 for 'USE <module>',
    ## extract the module,
    ## Strip extra ','. Impotant for new_r250
    ##Convert upper case to lower case 
    ##remove iso_c_bindings, that is intrinsic
    ##then add '.o' at the end
    dep=$(grep '^\s*use ' -i $f | awk ' {print $2}' | sed 's|,||g' | tr '[:upper:]' '[:lower:]' | grep iso_c_binding -v | sed "s|\(.*\)|${BLD_F90}/\1.o|" )
    ## sort the unique ones (tr ads a new line so that sort can do it's thing)
    dep=$(echo $dep | tr ' ' '\n' | sort -u  )
    ### echo out
    echo $file: $dep
done

###Fortran-dependencies on the header file
for f in F2C_externals.h ; do
    file=$f  
    dep_f=$(grep _MOD_ $f | sed "s|^\(.*\)__\(.*\)_MOD_\(.*\)$|${BLD_F90}/\2|g" | tr '[:upper:]' '[:lower:]' | sed 's|$|.o|g')
    ## sort the unique ones (tr ads a new line so that sort can do it's thing)
    dep_f=$(echo $dep_f | tr ' ' '\n' | sort -u)
    ### echo out
    echo $file: $dep_f
done
