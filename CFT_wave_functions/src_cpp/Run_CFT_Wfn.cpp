#include "Run_CFT_Wfn.h"

int main(int argc, const char* argv[]) {
    //Get initial random seed from true randomness (like /dev/urandom)
    std::random_device rd;
    // Setup
    clock_t starttime=clock();
    vars V; setupV(V);//Setup default V-values
    int argi=get_args(argc, argv, V);//Read in comand-line V-values
    if(argi==-1) {showHelp(); Torus_Laughlin_MC::show_chiral_help(); return 0;}
    else if(argi==1) return 0;
    
    disp(V);
    cout << endl << endl <<  "Program started " << timestring() << endl 
	 << "-----------------------------------------" << endl << endl;
    
    //Check that elecrons have explicitly been specified
    //FIXME: C-parser should ideally not chatch the -Ne
    //       but rather figure out Ne by asking the foran code
    if(V.i["Ne"]<=0){
	cerr<<"ERROR: Number of electrons is not explcitly specified!"<<endl;
	cerr<<"       Plese supply <Ne> using the -Ne flag!"<<endl;
	return -1;}
    
    //Initiate the seed
    if(V.i["-seed"]==-1){V.i["-seed"]=rd();}
    cout<<"Using seed:"<<V.i["-seed"]<<endl;
    CRandomSFMT rangen(V.i["-seed"]);
    
    
    //Initiate K-sector
    if(V.i["KL"]==-1){//K-sector considered uninitalized
      if(V.i["qL"] % 2) {  //Odd qL
	if(V.i["Ne"] % 2){V.i["KL"]=0;} //Odd Ne
	else {V.i["KL"]=V.i["Ne"]/2;} //Even Ne
      }
      else{ V.i["KL"]=0; }//Even qL
    }
    
    // Generate Laughlin configurations and their CFT and probability weights
    Generate(V, rangen, argc, argv); 
    // End program
    double sec=((double)clock()-starttime)/CLOCKS_PER_SEC; cout << setprecision(4) << endl;
    cout << "-----------------------------------------" << endl 
	 << "Total Runtime: " << sec << "s = " << sec/60.0 << "min" << endl;
    cout << endl <<  "Program finished " << timestring() << endl;
    fclose (stdout); fclose (stderr); return 0;
}

// Generate Laughlin configurations and their CFT and probability weights
void Generate(vars V, CRandomSFMT& rangen, int argc, const char* argv[]) {
    // Setup
    int th=V.i["th"], dl=V.i["Ne"]*V.i["dl"];
    unsigned long long N=V.ull["N"];
    Torus_Laughlin_MC MCrun(V.i["-seed"], V.i["qL"], argc, argv, V.d["st"], V.i["KL"], !V.b["csv"], getfilenames(V), V.d["d_alt"], V.b["r"],V.b["-use-cft-as-wgt"],V.b["-no-wfn"],V.d["chL"]);
    
    // Run MC
    if(!V.b["r"]) {
	cout<<"-----------------------------------"<<endl;
	cout<<"       Generate new coords"<<endl;
	cout<<"-----------------------------------"<<endl;
	MCrun.generate_configurations(N, th, dl);
    }
    else {
	cout<<"-----------------------------------"<<endl;
	cout<<"    Reuse old Coords"<<endl;
	cout<<"-----------------------------------"<<endl;
	MCrun.compute_configurations(N);
    }
    
}

// Get command line arguments
int get_args(int argc, const char* argv[], vars& V) {


    //NB: This functions parses through the input list and send the full list forward to
    //    the FORTRAN CFT parser.
    //    This parser needs to keep an up to date list of the flags used here.
    
    if(argc==1) cout << endl << "No input parameters: All parameters will be set to default!" << endl << endl;
    else { //Loop through the arguments a mathc to flags
	for(int i=1;i<argc;i++) {
	    string s=argv[i];
	    //FIXME: A bit dangerous is first character is not '-'.
	    string ns=s.substr(1); //remove first character (assumed to be a '-')
	    //cout << endl<< "Matching input no. "<<i<<" : "<<s<<endl;
	    //Match integer flags
	    if(V.i.count(ns)){//cout << "Matched with integer flag:"<<endl;
		//Check next argument is integer
		if(argc==(i+1)){cerr<<"ERROR:No argument to flag "<<s<<endl;return 1;}
		if(isint(argv[i+1])) {
		    //cout << "Next argument was integer: "<<argv[i+1]<<endl;
		    V.i.at(ns)=atoi(argv[i+1]);
		    i++;} 
		
		else {cerr << "ERROR: Input parameter " << argv[i+1] << 
			" has wrong format in relation to flag " << s<<endl; return 1;}
	    }
	    else if(V.ull.count(ns)){//cout << "Matched with long flag:"<<endl;
		//Check next argument is integer
		if(argc==(i+1)){cerr<<"ERROR:No argument to flag "<<s<<endl;return 1;}
		if(isint(argv[i+1])) {
		    //cout << "Next argument was integer: "<<argv[i+1]<<endl;
		    char *c; V.ull.at(ns)=strtoull(argv[i+1], &c, 10);
		    i++;} 
		else {cerr << "ERROR: Input parameter " << argv[i+1] << 
			" has wrong format in relation to flag " << s<<endl; return 1;}
	    }
	    //Match double flags
	    else if(V.d.count(ns)){cout << "Matched with double flag:"<<endl;
		//Check next argument is double = integer or number
		if(argc==(i+1)){cerr<<"ERROR:No argument to flag "<<s<<endl;return 1;}
		if(isnumber(argv[i+1]) || isint(argv[i+1]) ) {
		    cout << "Next argument was a number: "<<argv[i+1]<<endl;
		    V.d.at(ns)=atof(argv[i+1]);
		    i++;} 
		else {cerr << "ERROR: Input parameter " << argv[i+1] << 
			" has wrong format in relation to flag " << s<<endl; return 1;}
	    }
	    //Match for string flags
	    else if(V.s.count(ns)) {cout << "Matched with string flag: "<<ns<<endl;
		//No check since input is always strings
		if(argc==(i+1)){cerr<<"ERROR:No argument to flag "<<s<<endl;return 1;}
		V.s.at(ns)=argv[i+1]; i++;}
	    //Match for char flags
	    else if(V.c.count(ns)) {cout << "Matched with char flag:"<<endl;
		//No check since input is always strings
		if(argc==(i+1)){cerr<<"ERROR:No argument to flag "<<s<<endl;return 1;}
		//FIXME: Here should check that input really is only one char!
		V.c.at(ns)=argv[i+1][0]; i++;}
	    
	    ///All the flags have been matched
	    
	    
	    //Match for csv flag
	    else if(!strcmp(argv[i], "-csv") || !strcmp(argv[i], "-CSV")) {
	      cout << "Matched with -csv flag:"<<endl;
	      V.b["csv"]=1;}
	    
	    
	    //Match for r (reuse coords) flag) 
	    else if(!strcmp(argv[i], "-r") || !strcmp(argv[i], "--reuse")) {
		cout << "Matched with -r flag:"<<endl;
		V.b["r"]=1;}

	    //Match for --use-cft-as-wgt flag
	    else if(!strcmp(argv[i], "--use-cft-as-wgt")) {
	      cout << "Matched with --use-cft-as-wgt flag:"<<endl;
	      V.b["-use-cft-as-wgt"]=1;}

	    //Match for --no-wfn flag
	    else if(!strcmp(argv[i], "--no-wfn")) {
	      cout << "Matched with --no-wfn flag:"<<endl;
	      V.b["-no-wfn"]=1;}

	    
	    //Match for help flag
	    else if(!strcmp(argv[i], "--h") || !strcmp(argv[i], "-h") || !strcmp(argv[i], "--help") || !strcmp(argv[i], "-help") ||
		    !strcmp(argv[i], "--H") || !strcmp(argv[i], "-H") || !strcmp(argv[i], "--Help") || !strcmp(argv[i], "-Help") ||
		    !strcmp(argv[i], "--HELP") || !strcmp(argv[i], "-HELP")) return -1;
	    
	    else {
		cerr << "Info: Input parameter '" << s << "' unknown by C parser"<<endl;
		cerr  << "Sending it forward to the fortran parser"<< endl;
	    }
	}
    }
    cout << "Parsing completed"<<endl;
    return 0;
}

// Set up arguments
void setupV(vars& V) {
    
    V.i["Ne"]=-1; // Number of electrons
    V.i["th"]=1000; // Thermalization time
    V.i["dl"]=1; // MC iterations between sampling
    V.i["-seed"]=-1; // Random seed (added to cpu time)
    V.i["KL"]=-1; // K sector of Laughlin function
    //NB: Default (-1) can happen, but only for Ne=1 and Ne=2.
    //    In these cases, use K=2 or K=5.
    V.i["qL"]=3; // Exponent of Laughlin probability
    V.ull["N"]=1; // Number of samples
    V.d["st"]=-1; // MC step length
    V.d["chL"]=0.00; // Size of "landfill" in the 2p correlation hole
    V.d["d_alt"]=0; // Alternative Laughlin exponent of absolute value
    V.s["wfn"]="wf_values.hdf5";
    V.s["wgt"]="weight_values.hdf5";
    V.s["pos"]="positions.hdf5";
    V.b["csv"]=0;
    V.b["r"]=0;
    V.b["-use-cft-as-wgt"]=0;
    V.b["-no-wfn"]=0;
}

vector<string> getfilenames(vars V) {
    vector<string> fn;
    //Put all filenames in one vector
    fn.push_back(V.s["pos"]);
    fn.push_back(V.s["wfn"]);
    fn.push_back(V.s["wgt"]);
    return fn;
}

// Explain c input
void showHelp() {
    cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
    cout << "Generate Laughlin configurations and their CFT and probability weights"<<endl;
    cout << endl << "Additional c input arguments:" << endl << endl;
    cout << "\t--seed\t Set the seed ( -1 gives random seed)" << endl;
    cout << "\t-st\tFactor multiplying magnetic length for MC step" << endl;
    cout << "\t-th\tThermalization time in units of number of electrons" << endl;
    cout << "\t-dl\tMC steps between sampling in units of number of electrons" << endl;
    cout << "\t-qL\tExponent of Laughlin probability" << endl;
    cout << "\t-chL\tShallowness as correlation hole. Used as |th1(z|tau)|+|chL|*|th3(z|tau)|" << endl;
    cout << "\t-d_alt\tExponent of absolute factor; -> uses alternative Laughlin as probabiility" << endl;
    cout << "\t-KL\tK-sector for Laughlin. If unset (or set to -1) the K-sector default to K=0 or K=Ne/2 depending on number of electrons and parity of qL."<<endl;
    cout << "\t\tNB: The default K=-1 can happen, but only for Ne=1 and Ne=2. In these cases, use K=2 or K=5."<<endl;

    cout << "\t-pos\tFilename for input/output of positions" << endl;
    cout << "\t-wfn\tFilename for output of WF values" << endl;
    cout << "\t-wgt\tFilename for output of sampling probability values" << endl;
    cout << "\t-csv\tUse csv format for files (default is hdf5)" << endl;
    cout << "\t-r\tUse already generated coordinates There are supplied using -pos" << endl;
    cout << "\t--use-cft-as-wgt\tUse the CFT wave functions as weight instead of the Laughlin wfn." << endl;
    cout << "\t--no-wnf\tSuppress the output of CFT wave functions to file. Useful when only MC configurations are needed. With this flag -D and --Kmatrix need not be set, and defaults to D=1 Kmatrix=1." << endl;

    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
    return;
}

// Display parameters
void disp(vars V) {
    
    cout << endl << "Parameters: " << endl << endl;
    if(V.i.size()>=1) cout << "Integers: " << endl;
    map<string,int>::iterator iti;
    for (map<string,int>::iterator iti=V.i.begin(); iti!=V.i.end(); iti++) cout << "\t" << iti->first << " = " << n2s(iti->second) << endl;
    if(V.ull.size()>=1)cout << "Unsigned long long integers: " << endl;
    map<string, unsigned long long>::iterator itu;
    for (map<string, unsigned long long>::iterator itu=V.ull.begin(); itu!=V.ull.end(); itu++) cout << "\t" << itu->first << " = " << n2s(itu->second) << endl;
    if(V.d.size()>=1)cout << "Floats: " << endl;
    map<string,double>::iterator itd;
    for (map<string,double>::iterator itd=V.d.begin(); itd!=V.d.end(); itd++) cout << "\t" << itd->first << " = " << itd->second << endl;
    if(V.s.size()>=1)cout << "Strings: " << endl;
    map<string,string>::iterator its;
    for (map<string,string>::iterator its=V.s.begin(); its!=V.s.end(); its++) cout << "\t" << its->first << " = " << its->second << endl;
    if(V.b.size()>=1)cout << "Booleans: " << endl;
    map<string,bool>::iterator itb;
    for (map<string,bool>::iterator itb=V.b.begin(); itb!=V.b.end(); itb++) cout << "\t" << itb->first << " = " << itb->second << endl;
    if(V.c.size()>=1)cout << "Chars: " << endl;
    map<string,char>::iterator itc;
    for (map<string,char>::iterator itc=V.c.begin(); itc!=V.c.end(); itc++) cout << "\t" << itc->first << " = " << itc->second << endl;
    if(V.cd.size()>=1)cout << "Complex doubles: " << endl;
    map<string,complex<double> >::iterator itcd;
    for (map<string,complex<double> >::iterator itcd=V.cd.begin(); itcd!=V.cd.end(); itcd++) cout << "\t" << itcd->first << " = " << itcd->second << endl;
}

// Current time
string timestring() {
    time_t t = time(0);
    struct tm * now = localtime( & t );
    stringstream ss; 
    return nostring(now->tm_year + 1900)+" "+nostring(now->tm_mday)+"."+nostring(now->tm_mon+1)+"  "+nostring(now->tm_hour)+":"
	+nostring(now->tm_min)+":"+nostring(now->tm_sec);
}

// Create readable string from number
string n2s(int n) {
    stringstream ss;
    if(abs(n)<1000) ss << n;
    else if (abs(n)<1e6) {
	if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else if (abs(n)<1e9) {
	if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
	else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else if (abs(n)<INT_MAX) {
	if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
	else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
	else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else ss << n;
    return ss.str();
}

// Create readable string from number
string n2s(unsigned long long n) {
    stringstream ss;
    if(abs((double)n)<1000) ss << n;
    else if (abs((double)n)<1e6) {
	if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else if (abs((double)n)<1e9) {
	if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
	else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else if (abs((double)n)<1e12) {
	if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
	else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
	else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else if (abs((double)n)<1e15) {
	if((abs(n/1.0e12)-abs((int)(n/1e12)))/1.0e12<1e-13) ss << n/1e9 << "T";
	else if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
	else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
	else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else if (abs((double)n)<1e18) {
	if((abs(n/1.0e15)-abs((int)(n/1e15)))/1.0e15<1e-13) ss << n/1e15 << "Q";
	else if((abs(n/1.0e12)-abs((int)(n/1e12)))/1.0e12<1e-13) ss << n/1e12 << "T";
	else if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
	else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
	else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
	else ss << n;
    }
    else ss << n;
    return ss.str();
}

// Something something
string nostring(int n) {
    stringstream ss;
    if(n<10) ss << 0 << n;
    else ss << n;
    return ss.str();
}

// Update time
double timeint(clock_t& time) {clock_t time2=clock(); double ti=((double)time2-time)/CLOCKS_PER_SEC; time=time2; return ti;}


// Check if number
bool isnumber(string s) {
    string::const_iterator it = s.begin(); bool hasnumbers=false, hase=false;
    if(!isdigit(*it)) {if(*it!='-') return false;}
    else hasnumbers=true; it++;
    while(it!=s.end()) {
	if ((isdigit(*it))) hasnumbers=true;
	else if(*it=='e') {
	    if(hase) return false;
	    hase=true; 
	    if(*(it+1)=='-') {if(it+2==s.end() || !isdigit(*(it+2))) return false; it++;}
	    else if(it+1==s.end() || !isdigit(*(it+1))) return false;
	}
	else if(*it!='.') return false;
	it++;
    }
    return hasnumbers;
}


// Check if integer
bool isint(string s) {
    string::const_iterator it = s.begin(); bool hase=false;
    if(!isdigit(*it)) {if(*it!='-') return false;}
    it++;
    while(it!=s.end()) {
	if(*it=='e') {
	    if(hase) return false;
	    hase=true; 
	    if(it+1==s.end() || !isdigit(*(it+1))) return false;
	}
	else if (!(isdigit(*it))) return false;
	it++;
    }
    return true;
}
