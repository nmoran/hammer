using namespace std;
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <vector>
#include <sys/stat.h>
#include <complex>
#include <limits.h>
#include <algorithm>
#include <random>
#include "sfmt.h"
#include <map>
#include "HDF5Wrapper.h"
#include "F2C_externals.h"

#ifndef TORUS_LAUGHLIN_MC_H
#define TORUS_LAUGHLIN_MC_H

class Torus_Laughlin_MC {
    
  private:
    
    int Nphi_L, seed, q_L, Ksector_L, numberargs, ls[], electron_index;
    double step, H_List=0.0, T_List=0.0, *x_new_list, *y_new_list,*x_list, *y_list,xnew, ynew, *wfvalues, d_alt;
    string filename, s;
    complex<double> tau_tilde, fac,znew,iunit;
    vector<complex<double> > z_list;
    double L, fluxfact, L_L, l_y_L, l_x_L, fluxfact_L;
    CRandomSFMT rangen;
    const char* passarg;
    double pi, twopi;
    
    HDF5Wrapper *pos_hdf5, *val_write_hdf5, *prob_write_hdf5;
    HDF5Buffer *xbuf_hdf5, *ybuf_hdf5, *val_buf_hdf5, *prob_buf_hdf5;
    ofstream pos_write_ascii, val_write_ascii, prob_write_ascii;
    ifstream pos_read_ascii;
    bool hdf5,reading,use_cft,no_wfn;
    double chL;
    
    double getLstep(); // steplength giving 50% accepted for nu=1/3
    string concinput(int, const char*[], int[]); // Prepare input for Fortran
    void randsquare(); // Random particles on the torus
    complex<double> log_Laughlin_ratio(); // log of the Laughlin ratio between two configurations
    complex<double> log_laughlin(); // log of the Laughlin wavefunction
    complex<double> log_wgt_function(); // log of the used wgt function
    void move_trial_particle(); // New x,y coordinate for weight
    complex<double> jastrowfunction(complex<double>,complex<double>);
    void (Torus_Laughlin_MC::*iocoords)(int);
    void (Torus_Laughlin_MC::*printvalues)(int);
    void (Torus_Laughlin_MC::*printprobs)(int);
    void printcoords_hdf5(int);
    void printcoords_ascii(int);
    void readcoords_hdf5(int);
    void readcoords_ascii(int);
    void printvalues_hdf5(int);
    void printvalues_ascii(int);
    void printprobs_hdf5(int);
    void printprobs_ascii(int);

    void update_wgt(); // MC iteration uptaing the chosen wgt function
    // Generate Laughlin configurations and their Laughlin and CFT WF values
    void Laughlin_it_psi(); // MC iteration using Laughlin as probability and updating wavefunction value on the fly
    void cft_it_psi(); // MC iteration using cft as probability and updating wavefunction value on the fly

  public:
    
    int Ne, accept, Nphi;
    complex<double> logpsi_L, tau, logpsi;
    double E, E_L;
    
    static void show_chiral_help(); // Help function from Fortran code
    complex<double> log_chiral_WF(); // log of CFT wavefunction
    complex<double> log_chiral_WF_new(); // log of CFT wavefunction

    void generate_configurations(unsigned long long, int, int);
    // Read Laughlin configurations and WF values and compute CFT WF values
    void compute_configurations(unsigned long long);
    
    // Constructor
 Torus_Laughlin_MC(int seed_in, int q_L_in, int argc, const char* argv[], double step_in, int Ksector_in, bool hdf5_in, vector<string> filenames, double d_alt_in, bool reuse_in,bool use_cft_in,bool no_wfn_in,double chL_in) :
    seed(seed_in), q_L(q_L_in),  Ksector_L(Ksector_in), d_alt(d_alt_in), rangen(seed), hdf5(hdf5_in) , reading(reuse_in), use_cft(use_cft_in), no_wfn(no_wfn_in), chL(chL_in), accept(0) {
	//Laughlin bc as computed first.
	//Then the CFT wave function is initialized
	cout<<" ---------------------------------"<<endl;
	cout<<"     Initialize Monte-Carlo " << endl;
	cout<<" ---------------------------------"<<endl;

	numberargs=argc-1;
	int ls[numberargs];
	s=concinput(argc, argv, ls);
	int strlength=s.length();
	passarg=s.c_str();
	//Sent the input parameters to the fortran code
	retreive_input_alt(passarg, strlength, numberargs, ls);
	check_kmatrix_consistency();
	compute_kmatrix_stat();
	
	get_wfn_ne(Ne);
	get_wfn_ns(Nphi);
	Nphi_L=q_L*Ne;
	
	if(!use_cft){
	  //Laughlin bc as computed first.
	  //Then the CFT wave function is initialized
	  cout<<" ---------------------------------"<<endl;
	  cout<<"     Initialize Laughlin wfn" << endl;
	  cout<<" ---------------------------------"<<endl;
	  get_h_t_list(Nphi_L, Ksector_L, q_L, H_List, T_List); //Initialize proper H,T lists
	}
	else {
	  cout<<" ---------------------------------"<<endl;
	  cout<<"     Use cft as weight" << endl;
	  cout<<" ---------------------------------"<<endl;
	}
	
	cout<<" ---------------------------------"<<endl;
	cout<<"     Initialize cft wfn" << endl;
	cout<<" ---------------------------------"<<endl;
	init_wfn();
	get_wfn_tau(tau);
	
	//Setup the physical system size
	iunit=complex<double>(0,1);
	pi=3.1415926535897932384;
	twopi=6.2831853071795864769;
	L=sqrt(2*pi*Nphi/imag(tau));
	fluxfact=0.5/sqrt(Nphi);
	L_L=sqrt(2*pi*Nphi_L/imag(tau));
	tau_tilde=tau*((double)q_L+(double)d_alt)-std::conj(tau)*(double)d_alt;
	fac=complex<double>(0, 1)*pi*tau_tilde*(double)Ne;
	fluxfact_L=0.5/sqrt(Nphi_L);
	if(step_in>0) step=step_in;
	else step=getLstep();
	//Computing the hopping length
	//Tries to hopp ~ magnetic length
	//But if ~l_b < orbital_spacing this is set to the step length
	cout << "Naive number of magnetic lengths to step:"<<step<<endl;
	l_x_L=max(1.0/Nphi_L,fluxfact_L*sqrt(imag(tau)))*step;
	l_y_L=max(1.0/Nphi_L,fluxfact_L/sqrt(imag(tau)))*step;
	cout << "Step-length on reduced lattice x-direction: "<<l_x_L<<endl;
	cout << "Step-length on reduced lattice y-direction: "<<l_y_L<<endl;
	cout << "Corresponding to number of orbitals in x: "<<l_x_L*Nphi_L<<endl;
	cout << "Corresponding to number of orbitals in y: "<<l_y_L*Nphi_L<<endl;

	
	z_list.resize(Ne);
	x_list=new double[Ne];
	y_list=new double[Ne];
	x_new_list=new double[Ne];
	y_new_list=new double[Ne];
	randsquare();

	//Initialize the weight function
	logpsi_L=log_wgt_function();
	wfvalues=new double[2];

	cout<<" ---------------------------------"<<endl;
	cout<<"     Setup Files For writing/Reading" << endl;
	cout<<" ---------------------------------"<<endl;

	
	// Setup file input/output
	//Order of names in filenames is: pos, wfn, wgt

	if(hdf5) {  //Use hdf5
	  if(no_wfn){cout<<"     Skip setup for wfn hdf5 output" << endl;}
	  else{
	    //Setup wfn-file (always write to this)
	    val_write_hdf5=new HDF5Wrapper;
	    if(val_write_hdf5->openFileToWrite(filenames[1].c_str())!=0) {
	      cerr << "ERROR: Failed to open WF value file "<< filenames[1].c_str() <<" to write" << endl;
	      throw std::exception();}
	    //Setup the functions pointers for hdf5 vs ascii
	    printvalues=&Torus_Laughlin_MC::printvalues_hdf5;
	  }
            if(reading){
		//Setup pos-file ( read from this)
		pos_hdf5=new HDF5Wrapper;
		if(pos_hdf5->openFileToRead(filenames[0].c_str())!=0) {
		    cerr << "ERROR: Failed to open Positions file "<< filenames[0].c_str() <<" to read" << endl;
		    throw std::exception();}	    
		//Setup the functions pointers for hdf5 vs ascii
		iocoords=&Torus_Laughlin_MC::readcoords_hdf5;
	    }
	    else { //No reuse - generate new
		//Setup pos-file ( write to  this)
		pos_hdf5=new HDF5Wrapper;
		if(pos_hdf5->openFileToWrite(filenames[0].c_str())!=0) {
		    cerr << "ERROR: Failed to open Positions file "<< filenames[0].c_str() <<" to write" << endl;
		    throw std::exception();}
		//Setup the functions pointers for hdf5 vs ascii
		iocoords=&Torus_Laughlin_MC::printcoords_hdf5;
		
		//Setup prob-file ( write to  this)
		prob_write_hdf5=new HDF5Wrapper;
		if(prob_write_hdf5->openFileToWrite(filenames[2].c_str())!=0) {
		    cerr << "ERROR: Failed to open wieght file "<< filenames[2].c_str() <<" to write" << endl;
		    throw std::exception();}
		//Setup the functions pointers for hdf5 vs ascii
		printprobs=&Torus_Laughlin_MC::printprobs_hdf5;
	    }
	}
	else { //Use ascii
	  if(no_wfn){cout<<"     Skip setup for wfn ascii  output" << endl;}
	  else{
	    //Setup wfn-file (always write to this)
	    val_write_ascii.open(filenames[1].c_str());
	    if(!val_write_ascii) {
		cerr << "ERROR: Failed to open WF value file "<< filenames[1].c_str() <<" to write" << endl;
		throw std::exception();}
	    val_write_ascii.precision(16);
	    //Setup the functions pointers for hdf5 vs ascii
	    printvalues=&Torus_Laughlin_MC::printvalues_ascii;
	  }
	    
	    if(reading){
		//Setup pos-file ( read from this)
		pos_read_ascii.open(filenames[0].c_str());
		if(!pos_read_ascii) {
		    cerr << "ERROR: Failed to open Positions file "<< filenames[0].c_str() <<" to read" << endl;
		    throw std::exception();}	    
		pos_read_ascii.precision(16);
		//Setup the functions pointers for hdf5 vs ascii
		iocoords=&Torus_Laughlin_MC::readcoords_ascii;
	    }
	    else {  //No reuse - generate new
		//Setup pos-file ( write to  this)
		pos_write_ascii.open(filenames[0].c_str());
		if(!pos_write_ascii) {
		    cerr << "ERROR: Failed to open Positions file "<< filenames[0].c_str() <<" to write" << endl;
		    throw std::exception();}
		pos_write_ascii.precision(16);
		//Setup the functions pointers for hdf5 vs ascii
		iocoords=&Torus_Laughlin_MC::printcoords_ascii;
		
		
		//Setup prob-file ( write to  this)
		prob_write_ascii.open(filenames[1].c_str());
		if(!prob_write_ascii) {
		    cerr << "ERROR: Failed to open wieght file "<< filenames[1].c_str() <<" to write" << endl;
		    throw std::exception();}
		prob_write_ascii.precision(16);
		//Setup the functions pointers for hdf5 vs ascii
		printprobs=&Torus_Laughlin_MC::printprobs_ascii;
	    }
	    
	    
	    //---------------------------------------------------------------
	}
	cout<<" ---------------------------------"<<endl;
	cout<<"     Setup Complete" << endl;
	cout<<" ---------------------------------"<<endl;
	
    }
    
    //Destructor
    ~Torus_Laughlin_MC() {
	delete [] x_list; delete [] y_list;
	delete [] wfvalues;
	
	if(hdf5) {  //Use hdf5
	    delete pos_hdf5;
	    if(!no_wfn){delete val_write_hdf5;}
	    if(!reading){delete prob_write_hdf5;}
	}
	else { //Use ascii
	  if(!no_wfn){val_write_ascii.close();}
	  if(reading){pos_read_ascii.close();}
	  else {
	    pos_write_ascii.close();
	    prob_write_ascii.close();
	  }
	}
    }
    
};

#endif
