#include "Torus_Laughlin_MC.h"
#include "F2C_externals.h"


void Torus_Laughlin_MC::move_trial_particle() {
  // New x,y coordinate for the trial weight
  double randx=rangen.Random()-0.5;
  double randy=rangen.Random()-0.5;
  //choose particle
  electron_index=floor(Ne*rangen.Random());
  //work out new coordinate
  xnew=x_list[electron_index]+randx*l_x_L;
  ynew=y_list[electron_index]+randy*l_y_L;
  xnew-=round(xnew);   //move particle to fundamental square
  ynew-=round(ynew);  //move particle to fundamental square
  znew=xnew+tau*ynew;
  //update new particle list
  for(int i=0;i<Ne;i++) x_new_list[i]=x_list[i]; //pointers not vectors
  for(int i=0;i<Ne;i++) y_new_list[i]=y_list[i]; //pointers not vectors
  x_new_list[electron_index]=xnew;
  y_new_list[electron_index]=ynew;
  return;
}

void Torus_Laughlin_MC::update_wgt() {
  // Move random particle
  move_trial_particle();

  //compute the log(ratio) of the wgt functions
  complex<double> logratio;
  if(use_cft) {logratio=log_chiral_WF_new()-logpsi_L;}
  else {logratio=log_Laughlin_ratio();}

  //else {logratio=log_laughlin_new()-logpsi_L;}
  // Accept / reject
  double exprat=exp(2.0*real(logratio));
  if(exprat>=1.0 || exprat>rangen.Random()) {
    logpsi_L+=logratio;
    x_list[electron_index]=xnew;
    y_list[electron_index]=ynew;
    z_list[electron_index]=znew;
    accept++;
  }
  return;
}

// steplength giving 50% accepted for nu=1/3
double Torus_Laughlin_MC::getLstep() {
    if(real(tau)==0) return 3.0;
    double ratio=imag(tau)/real(tau);
    if(ratio>=1.0) return 3.0;
    else if(ratio>=0.5) return ratio+1.6;
    else return 4.8*ratio+0.035;
}

// log of the Laughlin wavefunction
complex<double> Torus_Laughlin_MC::log_laughlin() {
    complex<double> logdke=log_dedekind_eta(tau);
    double ysum=0.0;for(int i=0;i<Ne;i++) ysum+=y_list[i];
    double xsum=0.0;for(int i=0;i<Ne;i++) xsum+=x_list[i];
    double y2sum=0.0;for(int i=0;i<Ne;i++) y2sum+=y_list[i]*y_list[i];

    complex<double> zsum_alt=(double)q_L*(xsum+T_List)+tau_tilde*(ysum+H_List);
    complex<double> phase=
      pi*iunit*H_List*tau_tilde*H_List
      +twopi*iunit*H_List*tau_tilde*ysum
      +twopi*iunit*H_List*(double)q_L*xsum;
    complex<double> Lval=fac*y2sum+logtheta3(zsum_alt, tau_tilde)+phase-logdke;

    complex<double> tmp=0.0;
    for(int i=0;i<Ne-1;i++) {
      for(int j=i+1;j<Ne;j++) {
	complex<double> zizj=z_list[i]-z_list[j];
	tmp+=jastrowfunction(zizj, tau)-logdke;
      }
    }
    return Lval+(double)q_L*tmp+2.0*d_alt*real(tmp);
}

// log of the Laughlin ratio between two configurations
complex<double> Torus_Laughlin_MC::log_Laughlin_ratio() {
  double ysum=0.0;for(int i=0;i<Ne;i++) ysum+=y_list[i];
  double xsum=0.0;for(int i=0;i<Ne;i++) xsum+=x_list[i];
  double ysumnew=0.0;for(int i=0;i<Ne;i++) ysumnew+=y_new_list[i];
  double xsumnew=0.0;for(int i=0;i<Ne;i++) xsumnew+=x_new_list[i];

  complex<double> zsum_alt=(double)q_L*(xsum+T_List)+tau_tilde*(ysum+H_List);
  complex<double> zsumnew_alt=(double)q_L*(xsumnew+T_List)+tau_tilde*(ysumnew+H_List);
  complex<double> phase=twopi*iunit*H_List*tau_tilde*(ysumnew-ysum)
    +twopi*iunit*H_List*(double)q_L*(xsumnew-xsum);
  
  complex<double> Lval=fac*(ynew*ynew-y_list[electron_index]*y_list[electron_index])+
    (logtheta3(zsumnew_alt, tau_tilde)-
     logtheta3(zsum_alt, tau_tilde))+phase;

  complex<double> tmp=0.0;
  for(int i=0;i<electron_index;i++) {
    complex<double> zizrnew=z_list[i]-znew, zizrold=z_list[i]-z_list[electron_index];
    tmp+=jastrowfunction(zizrnew, tau)-jastrowfunction(zizrold, tau);
  }
  for(int i=electron_index+1;i<Ne;i++) {
    complex<double> zizrnew=znew-z_list[i], zizrold=z_list[electron_index]-z_list[i];
    tmp+=jastrowfunction(zizrnew, tau)-jastrowfunction(zizrold, tau);
  }

  return Lval+(double)q_L*tmp+2.0*d_alt*real(tmp);
}


complex<double> Torus_Laughlin_MC::jastrowfunction(complex<double> z,complex<double> tau) {
  //cout << "chL " << chL << " z " << z << endl;
  if(chL==0.0)return logtheta1(z, tau);
  else {
    // add theta one and theta3 together as |th1(z|tau)|+|chL|*|th3(z|tau)|
    // This still should have correct bcs.
    complex<double> a=real(logtheta1(z, tau));
    complex<double> b=real(logtheta3(z, tau));
    complex<double> c=a+log(1.0 + abs(chL)*exp(b-a));
    //cout << "a " << a << endl << "b " << b << endl << "c " << c << endl;
    return c;
  }
}


complex<double> Torus_Laughlin_MC::log_wgt_function() {
  complex<double> psi;
  if(use_cft){psi=log_chiral_WF();}
  else{psi=log_laughlin();}
  return psi;
}



// Help function from Fortran code
void Torus_Laughlin_MC::show_chiral_help() {
    
    string s="-h"; const char* passarg=s.c_str(); int numberargs=1; int ls[numberargs]; ls[0]=2; int strlength=s.length();
    set_input();
    retreive_input_alt(passarg, strlength, numberargs, ls);
    
}

// Random particles on the torus
void Torus_Laughlin_MC::randsquare() {
  for(int i=0;i<Ne;i++) {
    x_list[i]=rangen.Random()-0.5;
    y_list[i]=rangen.Random()-0.5;
    z_list[i]=x_list[i]+tau*y_list[i];
  }
}

// log of CFT wavefunction
complex<double> Torus_Laughlin_MC::log_chiral_WF() {
  return chiral_sym_psi_alt(x_list, y_list, tau, Ne);
}
complex<double> Torus_Laughlin_MC::log_chiral_WF_new() {
  return chiral_sym_psi_alt(x_new_list, y_new_list, tau, Ne);
}

// Prepare input for Fortran
string Torus_Laughlin_MC::concinput(int n, const char* c[], int ks[]) {
  string outstr="";
  for(int i=1;i<n;i++) {
    string s=c[i];
    outstr+=s;
    ks[i-1]=s.length();
  }
  return outstr;
}

// Generate Laughlin configurations and their Laughlin and CFT WF values
void Torus_Laughlin_MC::generate_configurations(unsigned long long N, int th, int dl) {
    
    // Setup
    clock_t time=clock(); double secs;
    unsigned long long i_shift;
    unsigned long long th_count=th;
    
    cout << "Thermalizing using "<<th<<" steps..."<< endl;cout.flush();
    
    //Setup output
    if(hdf5) {
      pos_hdf5->createRealDataset("xvalues", N, Ne);
      xbuf_hdf5 = pos_hdf5->setupRealSamplesBuffer("xvalues", 0, 1000);
      pos_hdf5->createRealDataset("yvalues", N, Ne);
      ybuf_hdf5 = pos_hdf5->setupRealSamplesBuffer("yvalues", 0, 1000);
      if(!no_wfn){
	val_write_hdf5 ->createRealDataset("wf_values", N, 2);
	val_buf_hdf5  =  val_write_hdf5->setupRealSamplesBuffer("wf_values", 0, 1000);
      }
      prob_write_hdf5->createRealDataset("wf_values", N, 2);
      prob_buf_hdf5 = prob_write_hdf5->setupRealSamplesBuffer("wf_values", 0, 1000);
    }
    
    // Thermalize
    accept=0;
    setup_write_progress();
    for(int i=0;i<th;i++) {
      update_wgt();
      i_shift=i+2;//FIXME: Use to get nice counting on the timer
      write_progress(i_shift,th_count);
    }
    secs=((double)clock()-time)/CLOCKS_PER_SEC; cout << endl << "Thermalization time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl;
    cout << "therm accept: " << accept*100.0/(double)th << "%" << endl; time=clock();
    
    //Fortran code
    setup_write_progress();
    // Sample
    accept=0; cout << "Generate configurations and sampling..."<<endl; cout.flush();
    cout << "Keep every " << dl<<":th step"<<endl; cout.flush();
    for(unsigned long long i=0;i<N;i++) {
      if(!no_wfn){
	logpsi=log_chiral_WF(); // Get CFT WF value
	(this->*printvalues)(i);// Print WF values to files
      }
      (this->*iocoords)(i);// Print configurations  to files
      (this->*printprobs)(i);// Print coord probabilities to files
      for(int j=0;j<dl;j++) {update_wgt();} //Run dl MC updates before next sample
      i_shift=i+2;//FIXME: Use to get nice counting on the timer
      write_progress(i_shift,N);
      
    }
    
    // End
    cout << endl << "Total # samples: " << N << endl;
    secs=((double)clock()-time)/CLOCKS_PER_SEC; cout << "Sampling time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl;
    cout << setprecision(3) << 100.0*accept/((double)N*dl) << "% accepted" << endl << endl;
}

// Read Laughlin configurations and WF values and compute CFT WF values
void Torus_Laughlin_MC::compute_configurations(unsigned long long N) {
    
    // Setup
    clock_t time=clock(); double secs;
    unsigned long long i_shift;
    
    //Setup output
    if(hdf5) {
	val_write_hdf5->createRealDataset("wf_values", N, 2);
	val_buf_hdf5 = val_write_hdf5->setupRealSamplesBuffer("wf_values", 0);
	xbuf_hdf5 = pos_hdf5->setupRealSamplesBuffer("xvalues", 0);
	ybuf_hdf5 = pos_hdf5->setupRealSamplesBuffer("yvalues", 0);
	
    }
    
    // Read and compute
    //Fortran code
    setup_write_progress();
    // Sample
    accept=0; cout << "Sampling..."<<endl; cout.flush();
    for(unsigned long long i=0;i<N;i++) {
	(this->*iocoords)(i); // Read configurations from file
	logpsi=log_chiral_WF();
	(this->*printvalues)(i);// Print WF values to files
	i_shift=i+2;//FIXME: Use to get nice counting on the timer
	write_progress(i_shift,N);
    }
    
    cout << endl << "Total # samples: " << N << endl;
    secs=((double)clock()-time)/CLOCKS_PER_SEC; cout << "Computation time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl;
}

void Torus_Laughlin_MC::printcoords_hdf5(int start){
    xbuf_hdf5->writeToBuffer(x_list, Ne);
    ybuf_hdf5->writeToBuffer(y_list, Ne);
}

void Torus_Laughlin_MC::printcoords_ascii(int dummy){
    for(int i=0;i<Ne;i++) pos_write_ascii << x_list[i] << "," << y_list[i] << ","; pos_write_ascii << endl;
}

void Torus_Laughlin_MC::printvalues_hdf5(int start){
    wfvalues[0]=real(logpsi);
    wfvalues[1]=imag(logpsi);
    val_buf_hdf5->writeToBuffer(wfvalues, 2);
}

void Torus_Laughlin_MC::printprobs_hdf5(int start){
    wfvalues[0]=real(logpsi_L);
    wfvalues[1]=imag(logpsi_L);
    prob_buf_hdf5->writeToBuffer(wfvalues, 2);
}


void Torus_Laughlin_MC::printvalues_ascii(int dummy){
    val_write_ascii << real(logpsi) << "," << imag(logpsi) << "," << endl;
}

void Torus_Laughlin_MC::printprobs_ascii(int dummy){
    prob_write_ascii << real(logpsi_L) << "," << imag(logpsi_L) << "," << endl;
}


void Torus_Laughlin_MC::readcoords_hdf5(int start){
    xbuf_hdf5->readFromBuffer(x_list, Ne);
    ybuf_hdf5->readFromBuffer(y_list, Ne);
}

void Torus_Laughlin_MC::readcoords_ascii(int dummy){
    char c;
    for(int i=0;i<Ne;i++) pos_read_ascii >> x_list[i] >> c >> y_list[i] >> c;
}
