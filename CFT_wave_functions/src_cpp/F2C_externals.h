//This header contains a list of all the Fortran functions that have bind binded to c-format using the "bind(c)" option.
//All c-file that uses the fortran shoudl include this header file
//NB: As the fortran binds forget their MODULE origin when they get bind
//    we keep a comment on top of every funciton/subroutine to record that origin


//FIXME:Write a script that chwack that the nfunctions lester here actuallt exists in the MODULES pointed to.

// Fortran functions
extern "C" {
//__center_of_mass_MOD_get_h_t_list
extern void get_h_t_list(int&, int&, int&, double&, double&);

//__chiral_wfn_MOD_set_input
extern void set_input();

//__chiral_wfn_MOD_retreive_input_alt
extern void retreive_input_alt(const char* inarguments, int& inlength, int& no_arguments, int ks[]);

  //__chiral_wfn_MOD_check_kmatrix_consistency
extern void check_kmatrix_consistency();

//__chiral_wfn_MOD_compute_kmatrix_stat
extern void compute_kmatrix_stat();

// __chiral_wfn_MOD_init_wfn
extern void init_wfn();

//__chiral_wfn_MOD_get_wfn_ne
extern void get_wfn_ne(int&);

//__chiral_wfn_MOD_get_wfn_ns
extern void get_wfn_ns(int&);

//__chiral_wfn_MOD_get_wfn_k
extern void get_wfn_k(int&);

//__chiral_wfn_MOD_get_wfn_tau
extern void get_wfn_tau(complex<double>&);

//__timer_MOD_setup_time
extern void setup_write_progress();

//__timer_MOD_write_progress
extern void write_progress(long long unsigned int&,long long unsigned int&);

//__chiral_wave_function_MOD_chiral_sym_psi_alt
extern complex<double> chiral_sym_psi_alt(double x[], double y[], complex<double>& tau, int& dim);

//__jacobitheta_MOD_logtheta1
extern complex<double> logtheta1(complex<double>& z, complex<double>& tau);

//__jacobitheta_MOD_logtheta2
extern complex<double> logtheta2(complex<double>& z, complex<double>& tau);

//__jacobitheta_MOD_logtheta3
extern complex<double> logtheta3(complex<double>& z, complex<double>& tau);

//__jacobitheta_MOD_logtheta4
extern complex<double> logtheta4(complex<double>& z, complex<double>& tau);

//__jacobitheta_MOD_logthetagen
extern complex<double>logthetagen(double&, double&, complex<double>& z, complex<double>& tau);

//__dedekind_eta_function_MOD_log_dedekind_eta
extern complex<double> log_dedekind_eta(complex<double>& tau);

}
