using namespace std;
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <vector>
#include <sys/stat.h>
#include <complex>
#include <limits.h>
#include <algorithm>
#include "sfmt.h"
#include <map>
#include "Torus_Laughlin_MC.h"

// Constants
const double pi=3.1415926535897932384;
const double twopi=6.2831853071795864769;

// Variables
struct vars{
    map<string, int> i;
    map<string, unsigned long long> ull;
    map<string, double> d;
    map<string, string> s;
    map<string, bool> b;
    map<string, char> c;
    map<string, complex<double> > cd;
};

// Functions
void Generate(vars, CRandomSFMT&, int, const char*[]);
void Run_MC(vars, CRandomSFMT&, int, const char*[]);
void Test_generator(vars, CRandomSFMT&, int, const char*[]);
void Run_MC(vars, CRandomSFMT&, int, const char*[], Torus_Laughlin_MC&);
void CFT_MC(vars, CRandomSFMT&, int, const char*[]);
void Laughlin_MC(vars, CRandomSFMT&, int, const char*[]);
void showHelp();
vector<string> getfilenames(vars);
void setupV(vars&);
int get_args(int, const char*[], vars&);
void disp(vars);
string timestring();
string n2s(int);
string n2s(unsigned long long);
string nostring(int);
double timeint(clock_t&);
double bootstrap(vector<double>, int, CRandomSFMT&);
bool isnumber(string s);
bool isint(string s);
