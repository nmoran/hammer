
###------------------------------------
####Make file parts for generating configuration
###------------------------------------


###F90_dependencies for Run_Laughlin
Cpp_torus_f90_deps.mk:mk.dep_list.sh mk.build.cpp.sh dependencies $(MAKEFILES)
	-@###Saves Torus f90 dependencies to a file
	-@###FIXME: It\'s a bit flaky since is assumes (true for now) that chiral_wfn.o also has the same dependencies.
	./mk.build.cpp.sh CPP_TORUS_F90_DEPS build_f90/chiral_wfn.o > $@
include Cpp_torus_f90_deps.mk

Run_CFT_Wfn: $(DIR_BLD_CXX)/Run_CFT_Wfn.o $(DIR_BLD_CXX)/Torus_Laughlin_MC.o $(DIR_BLD_CXX)/sfmt.o $(CPP_TORUS_F90_DEPS) $(DIR_BLD_CXX)/HDF5Wrapper.o $(MAKEFILES)
	${CXX} ${MYCXXFLAGS} $(CPP_TORUS_F90_DEPS) $(DIR_BLD_CXX)/Run_CFT_Wfn.o $(DIR_BLD_CXX)/Torus_Laughlin_MC.o $(DIR_BLD_CXX)/HDF5Wrapper.o ${HDF5_LIBS} $(DIR_BLD_CXX)/sfmt.o -o Run_CFT_Wfn  ${MYCXXLDFLAGS} $(OMPFLAG)

$(DIR_BLD_CXX)/Run_CFT_Wfn.o: $(DIR_SRC_CXX)/Run_CFT_Wfn.cpp $(DIR_SRC_CXX)/Run_CFT_Wfn.h $(DIR_SRC_CXX)/Torus_Laughlin_MC.h $(DIR_SRC_CXX)/F2C_externals.h $(MAKEFILES) $(CPP_TORUS_F90_DEPS)
	${CXX} ${MYCXXFLAGS} -c $(DIR_SRC_CXX)/Run_CFT_Wfn.cpp -openmp ${HDF5_DIR_INCLUDE} -o $@  ${MYCXXLDFLAGS}

$(DIR_BLD_CXX)/Torus_Laughlin_MC.o: $(DIR_SRC_CXX)/Torus_Laughlin_MC.cpp $(DIR_SRC_CXX)/Torus_Laughlin_MC.h $(DIR_BLD_CXX)/sfmt.o $(CPP_TORUS_F90_DEPS) $(DIR_SRC_CXX)/F2C_externals.h $(MAKEFILES)
	${CXX} -c ${MYCXXFLAGS} $(DIR_SRC_CXX)/Torus_Laughlin_MC.cpp ${HDF5_DIR_INCLUDE} -o $@  ${MYCXXLDFLAGS}

$(DIR_BLD_CXX)/HDF5Wrapper.o: $(DIR_SRC_CXX)/HDF5Wrapper.C $(DIR_SRC_CXX)/HDF5Wrapper.h $(MAKEFILES)
	${CXX}  ${DISABLE} -c $(DIR_SRC_CXX)/HDF5Wrapper.C ${HDF5_DIR_INCLUDE} -o $@

$(DIR_BLD_CXX)/sfmt.o: $(DIR_SRC_CXX)/sfmt.cpp $(DIR_SRC_CXX)/randomc.h $(DIR_BLD_CXX)/userintf.o $(DIR_SRC_CXX)/sfmt.h
	${CXX} ${DISABLE} -c $(DIR_SRC_CXX)/sfmt.cpp ${MYCXXFLAGS} -o $@

$(DIR_BLD_CXX)/userintf.o: $(DIR_SRC_CXX)/randomc.h $(DIR_SRC_CXX)/userintf.cpp
	${CXX} ${DISABLE} -c $(DIR_SRC_CXX)/userintf.cpp ${MYCXXFLAGS} -o $@
