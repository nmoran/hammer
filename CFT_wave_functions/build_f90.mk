.SUFFIXES: $(SUFFIXES) .f90 .o

###Dependecies all object files have
$(OBJS_F90):  test_scripts/check_implicit_none.sh

$(DIR_BLD_F90)/%.o : $(DIR_SRC_F90)/%.f90 $(MAKEFILES)
	@test_scripts/check_implicit_none.sh $<
	$(F90) $(F90FLAGS) $(F90DBFLAGS) $(OMPFLAG) $(MODFLAG) -c -o $@  $<

###Inferencee Rules
%.o : %.mod # just in case there is a .mod and it isn't Modula-2 code

