.PHONY : clean ##Clean is not a file, but the name of a rule
clean: clean_f90 clean_cxx clean_programs clean_deps
	rm *.pyf core;
	-@echo 'Cleaning done'

.PHONY : clean_programs  ##Clean is not a file, but the name of a rule
clean_programs:
	-@echo 'Deleting programs'
	rm -f $(PROGS)

.PHONY : clean_f90  ##Clean is not a file, but the name of a rule
clean_f90:
	-@echo 'Deleting forgran .o and .mod files'
	rm -f $(DIR_BLD_F90)/*.mod $(DIR_BLD_F90)/*.o

.PHONY : clean_cxx  ##Clean is not a file, but the name of a rule
clean_cxx:
	-@echo 'Deleting c++ .o files'
	rm -f $(DIR_BLD_CXX)/*.o


.PHONY : clean_deps  ##Clean is not a file, but the name of a rule
clean_deps:
	-@echo 'Deleting dependecy informations'
	rm -f dependencies
	rm -f $(DEPFILE)
	rm -f dependency-*.txt
	rm -f dependency-*.png


