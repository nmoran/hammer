#!/bin/bash

set -e
LIBS=$3
set -u ##Look for unised after LIBS have (maybe been initalized)
prog_name=$1
variable=$2

Full_List=$(eval ./mk.dep_list.sh "$variable" )

##Echo the full list
echo $prog_name: $Full_List
echo '	$(F90) $(LDFLAGS) -o $@' $Full_List "$LIBS"
