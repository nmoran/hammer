#!/bin/bash


set -e
set -u

###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:TransformState.py
###MAKEDEP:Overlap.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_states_translation
#### "Uggly" ways of cleaning catalouge
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge

N=4
Nphi=12
k1=0
k2=2

echo "Testing full feature."
TorusDiagonalise.py -p $N -l $Nphi -y $k1 -c $k2 --inversion-sector 0 --eigenstate
TorusDiagonalise.py -p $N -l $Nphi -y $k1 -c $k2 --eigenstate
TorusDiagonalise.py -p $N -l $Nphi -y $k1  --eigenstate

echo "Testing translation feature"
TorusDiagonalise.py -p $N -l $Nphi -y $((${k1}+${N}))  --eigenstate
TorusDiagonalise.py -p $N -l $Nphi -y $((${k1}+2*${N}))  --eigenstate
TransformState.py -t 1 Torus_p_${N}_Ns_${Nphi}_K_${k1}_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0.vec --id _translated_t1
TransformState.py -t 2 Torus_p_${N}_Ns_${Nphi}_K_${k1}_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0.vec --id _translated_t2
echo "Following overlaps should be one"
Ov=$(Overlap.py -a Torus_p_${N}_Ns_${Nphi}_K_$((${k1+${N}}))_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0.vec Torus_p_${N}_Ns_${Nphi}_K_$((${k1+${N}}))_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0_translated_t1.vec)
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after translation is not 1.0 (but $Ov )!"
    exit -1
fi

Ov=$(Overlap.py -a Torus_p_${N}_Ns_${Nphi}_K_$((${k1+2*${N}}))_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0.vec Torus_p_${N}_Ns_${Nphi}_K_$((${k1+2*${N}}))_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0_translated_t2.vec)
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after translation is not 1.0 (but $Ov )!"
    exit -1
fi



###Cleaning afterwards
rm -r $Catalouge
