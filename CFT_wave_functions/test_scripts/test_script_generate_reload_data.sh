#!/bin/bash

##This script runs CFT code and tests that data can be loded and reloaded

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_generate_and_reload_data
mkdir -p $Catalouge
cd $Catalouge

N=1000
Ne=5
Kmom=0
q=3

pos_start=pos-here.hdf5

###Run the torus code one
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf.hdf5 -wgt $Catalouge/weight.hdf5

###Save initial output config
cp $Catalouge/$pos_start $Catalouge/pos_1.hdf5

####--------------------------------------------------------------

### Run the cft again (reusing old coord)
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf_recompute.hdf5 -r

###Test that the wave functions fields are the same
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_recompute.hdf5 loglog same same

###In case the program does something fishy with it
cp $Catalouge/$pos_start $Catalouge/pos_1.hdf5

####--------------------------------------------------------------

### Run an independent cft again
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf_indep.hdf5 -wgt $Catalouge/weight_indep.hdf5

###Now the coord file should have changed
cp $Catalouge/$pos_start $Catalouge/pos_2.hdf5

###Test that the fields are NOT the same
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_indep.hdf5 loglog diff diff

###Cleaning afterwards
rm -r $Catalouge
