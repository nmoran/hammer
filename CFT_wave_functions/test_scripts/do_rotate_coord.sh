#!/bin/bash

##This script runs CFT code, generates a 

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:ModularTransformState.py
###MAKEDEP:MCImportanceOverlaps.py

N=$1
Ne=$2
Kmom=$3
q=$4
TList=$5
Ns=$((q*Ne))

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_coords
mkdir -p $Catalouge

pos_start=$Catalouge/pos_in.hdf5
pos_stop=$Catalouge/pos_out.hdf5
wf_in=$Catalouge/wfn_in.hdf5
wf_out=$Catalouge/wfn_out.hdf5

###Run the torus code once
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -N $N -dl 100 -pos $pos_start -wfn $wf_in -wgt $Catalouge/weight.hdf5 

###Perform an S-transform
echo "Perform Modular transform"
ModularTransformState.py --wfn-in $wf_in --coord-in $pos_start --wfn-out $wf_out --coord-out $pos_stop --transformation $TList --fluxes $Ns 

####Check that the overlap between the wave functions is 1 again
echo "Check that the overlap between the wave functions is 1 again"
MCImportanceOverlaps.py --wfn-list $wf_in --ed-list $wf_out --wgt-list  $Catalouge/weight.hdf5 --cft-scale 'log' --ed-scale 'log' --output-prefix $Catalouge/Overlap --bins 10

##Extras 6th column of last (and only) line
Overlap=`tail -n 1 $Catalouge/Overlap.csv  | awk 'BEGIN { FS = "," } ; {print $6}'`
echo Overlap: $Overlap
if (( $(bc <<< "$Overlap < 0.99") )) ; then
    echo "Error:">&2
    echo "Error: Overlap is smaller than 1!" >&2
    echo "       Experiencing problems with Ne=$Ne,Kmom=$Kmom,q=$q">&2
    echo "       For the transformations '$TList' which should be unitary">&2
    exit -1
fi


###Cleaning afterwards
rm -r $Catalouge
