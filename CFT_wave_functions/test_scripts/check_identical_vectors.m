function check_identical_vectors(Path1Re,Path1Im,Path2Re,Path2Im,LogType,DiffType)
  %%%Load the data
  WF1=load(Path1Re)+i*load(Path1Im);
  WF2=load(Path2Re)+i*load(Path2Im);

  %%%Take the log if the functions is in linear scale
  if(LogType(1))
      WF1=log(WF1);
  end
  if(LogType(2))
      WF2=log(WF2);
  end


  %%%Compute the larges value 
  maxWF1=max(real(WF1));
  maxWF2=max(real(WF2));
  %%%Normalize by the largest value
  WF1=WF1-maxWF1;
  WF2=WF2-maxWF2;

  %%The diffrence between the tro ins now divided into scle
  %%differences a s angle differences
  ScaleDiff=real(WF1-WF2);
  AngleDiff=mod(imag(WF1-WF2),2*pi);
  %%%Rotate such the the diff is not at zero (but rather pi)
  %%%Then tae mod 2*pi and rotate back...
  %%THis should get rid of any 0~2*pi flucutatons that can occur
  meanAngleDiff=mean(AngleDiff);
  AngleDiff=mod(AngleDiff-mean(AngleDiff)+pi,2*pi)-pi-meanAngleDiff;


  if(DiffType(1)==1)
    SumDiffScale=sum(abs(ScaleDiff)>1e-11)
    if(SumDiffScale);
      disp("WF1      WF2       scaled(WF1)-scaled(WF2)")
      [WF1+maxWF1,WF2+maxWF2,WF1-WF2]
      [Freq,Vals]=hist(ScaleDiff)
      error('WF1 and WF2 vectors are not proportional');
    end;
  elseif(DiffType(1)==-1)
    SumDiffScale=sum(abs(ScaleDiff)<1e-11)
    if(SumDiffScale);
      disp("WF1      WF2       scaled(WF1)-scaled(WF2)")
      [WF1+maxWF1,WF2+maxWF2,WF1-WF2]
      error('WF1 and WF2 vectors are proportional (then they shouldnt be)');
    end;
  end


  if(DiffType(2)==1)
    SumDiffAngle=sum(abs(AngleDiff-mean(AngleDiff))>1e-10)
    if(SumDiffAngle);
      disp("WF1      WF2       scaled(WF1)-scaled(WF2)   angle_diff")
      [WF1+maxWF1,WF2+maxWF2,WF1-WF2,AngleDiff]
      error('WF1 and WF2 vectors do not have same phase');
  end;
  elseif(DiffType(2)==-1)
    SumDiffAngle=sum(abs(AngleDiff-mean(AngleDiff))<1e-10)
    if(SumDiffAngle);
      disp("WF1      WF2       scaled(WF1)-scaled(WF2)")
      [WF1+maxWF1,WF2+maxWF2,WF1-WF2]
      error('WF1 and WF2 vectors DO have same phase (when they shouldnt)');
    end;
  end

  if(isnan(sum(ScaleDiff)));
    error('Differnce WF1 and WF2 is NaN') ;
  end
  if(isnan(sum(AngleDiff)));
    error('Differnce WF1 and WF2 is NaN') ;
  end

  disp("The vectors passed the test")
  disp("")
  
return
