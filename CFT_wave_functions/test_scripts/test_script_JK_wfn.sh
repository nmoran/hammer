#!/bin/bash

##This script runs CFT and Hammer JK wnf and tries to generate a wavefunctoin. An then checks that eh wave functions is not zero.

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:FQHTorusGenerateJKWF.py
###MAKEDEP:test_scripts/Is_wfn_nonzero.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_JK_wfn
mkdir -p $Catalouge
cd $Catalouge

###We will create the nu=2/5 state

N=100
Ne=6
Kmom=3
lls=2
Ns=15
Nr=3
FP=1

pos=pos.hdf5
wfn=wf.hdf5
JKwfn=JKwf.hdf5
JKwfnLL=JKwfLL.hdf5
JKwfnNoDet=JKwfNoDet.hdf5
wgt=weight.hdf5


#####  Generate some positions with the CFT-script
$dir_CFT/Run_CFT_Wfn -D 1 --Kmatrix 1 -Ne $Ne -K $Kmom -N $N -pos $pos -wfn $wfn -wgt $wgt 

#### Now generate a JK wave function fromt he same data

### First some tests on consistency

function TestFail {
    echo
    echo Testing for failiure: "$@"
    if "$@" ; then
	echo The call \""$@"\" did not fail when it should
	exit -1
    else
	echo The call \""$@"\" failed as it should
    fi
    }

function TestSuceed {
    echo
    echo Testing for sucess: "$@"
    if "$@" ; then
	echo The call \""$@"\" did not fail ":)"
    else
	echo The call \""$@"\" failed ":("
	exit -1
    fi
    }

###Things that should not work
TestFail FQHTorusGenerateJKWF.py -Nr $Nr --dry-run
TestFail FQHTorusGenerateJKWF.py -Ne $Ne --dry-run
TestFail FQHTorusGenerateJKWF.py -Ns $Ns --dry-run
TestFail FQHTorusGenerateJKWF.py -Ne $Ne -Ns 4 -Nr $Nr --dry-run
TestFail FQHTorusGenerateJKWF.py -Ne $Ne -Ns 4 -Nr 5 --dry-run
TestFail FQHTorusGenerateJKWF.py -Ne $Ne -Nr 4 --dry-run
TestFail FQHTorusGenerateJKWF.py -Ne $Ne -FLL 3 -Ns $Ns -Nr $Nr --dry-run
TestFail FQHTorusGenerateJKWF.py -Ne $Ne -FLL 3 -Ns $Ns -Nr $Nr --dry-run
TestFail FQHTorusGenerateJKWF.py -Ne $Ne -FLL $lls -Ns $Ns -Nr $Nr -FP 2 --dry-run

###Things that should work
TestSuceed FQHTorusGenerateJKWF.py -Nr $Nr -FLL $lls --dry-run
TestSuceed FQHTorusGenerateJKWF.py -Ne $Ne -FLL $lls --dry-run
TestSuceed FQHTorusGenerateJKWF.py -Ne $Ne -Ns $Ns -FP $FP --dry-run
TestSuceed FQHTorusGenerateJKWF.py -Ne $Ne -FP $FP -Nr $Nr --dry-run
TestSuceed FQHTorusGenerateJKWF.py -Ne $Ne -Ns $Ns -Nr $Nr --dry-run
TestSuceed FQHTorusGenerateJKWF.py -FLL $lls -Ns $Ns -Nr $Nr --dry-run

#### Now generate a JK wave function from the same data
FQHTorusGenerateJKWF.py -Ne $Ne -FLL $lls -Ns $Ns -Nr $Nr --coord $pos --wfn $JKwfn
###This one should be zero!
FQHTorusGenerateJKWF.py -Ne $Ne -FLL $lls -Ns $Ns -Nr $Nr --coord $pos --wfn $JKwfnLL --LL-force
FQHTorusGenerateJKWF.py -Ne $Ne -FLL $lls -Ns $Ns -Nr $Nr --coord $pos --wfn $JKwfnNoDet --No-determinant

TestSuceed python $dir_CFT/test_scripts/Is_wfn_nonzero.py $JKwfn
TestFail python $dir_CFT/test_scripts/Is_wfn_nonzero.py $JKwfnLL
TestSuceed python $dir_CFT/test_scripts/Is_wfn_nonzero.py $JKwfnNoDet

ls


###Cleaning afterwards
rm -r $Catalouge
