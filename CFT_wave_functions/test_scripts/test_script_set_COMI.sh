#!/bin/bash

####The that the wave-functoins are eigenfuntions of the many body translation operators.


set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py
###MAKEDEP:test_scripts/Move_coords.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_T1_T2q_eigenstate
mkdir -p $Catalouge
cd $Catalouge

N=30
Ne=5
q=3

Ns=15

pos_start=pos-here.hdf5
pos_move_y=pos-x.hdf5
pos_move_x=pos-y.hdf5

for Kmom in 5 10 0 ; do
    
    ###Run the torus code with random seed
    $dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf.hdf5 -wgt $Catalouge/weight.hdf5
    
    python $MyDir/Move_coords.py --coord-in $Catalouge/$pos_start --coord-out $Catalouge/$pos_move_x  -x 1 --fluxes $Ns
    python $MyDir/Move_coords.py --coord-in $Catalouge/$pos_start --coord-out $Catalouge/$pos_move_y  -y $q --fluxes $Ns
    
    ### Run with translated coordinates in x-direction
    $dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_move_x -wfn $Catalouge/wfx.hdf5 -wgt $Catalouge/weightx.hdf5 -r
    
    ### Run with translated coordinates in y-direction
    $dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_move_y -wfn $Catalouge/wfy.hdf5 -wgt $Catalouge/weighty.hdf5 -r


    echo " --- Test that a T1 translation gives a phase for K=$Kmom ----"
    
    ###Test that the T1 move gives eigenstate
    if $Kmom == 0; then
	python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wfx.hdf5 loglog same same
    else
	python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wfx.hdf5 loglog same prop
    fi

    echo " --- Test that a T2^q translation gives a phase for K=$Kmom ----"
    ###Test that the T2^q move gives eigenstate
    python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wfy.hdf5 loglog same diff

	
done
###Cleaning afterwards
rm -r $Catalouge
