#!/bin/bash


##This script first computes the Lalughlin state in real space,
##It then runt the Hammer and computes the real space wave function from thre as well,
##using the electron positions. It then compares the two to see it they give proportional output

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:MCSampleLaughlin.py
###MAKEDEP:test_scripts/check_identical_vectors.py


## Changes direcotry ot the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd) ###SHould be lovated in the test_script folder

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
dir_CFT=$MyDir/../../CFT_wave_functions
cd $dir_CFT
dir_CFT=$(pwd)
echo $dir_CFT

cells=4
Samples=100
Denom=3

for Tau_Re in `seq -0.3 0.6 0.5` ; do
    for Tau_Im in 0.4 2.6 4.2 ; do
	
	###Restrict tau to be 3 decimals long
	Tau_Im=$(LC_ALL=C /usr/bin/printf "%.*f\n" 3 $Tau_Im)
	Tau_Re=$(LC_ALL=C /usr/bin/printf "%.*f\n" 3 $Tau_Re)
	
	Tau_Re_Hamm=$(LC_ALL=C /usr/bin/printf "%.*f\n" 3 $(echo "-1 * $Tau_Re" | bc ))
	
	Ne=$cells
	Ns=$[ $cells*$Denom ]
	
	#Define cataluge name and create folders
	Catalouge=$MyDir/Test_Laughlin_Hammer_vs_Laughlin,q=$Denom,c=$cells,I=$Tau_Im,R=$Tau_Re
	echo "Files will be stored in $Catalouge"
	mkdir -p $Catalouge
	rm -r $Catalouge
	mkdir -v -p $Catalouge
	cd $Catalouge
	
	###We do not clean as any files will be deposited in the same catalouge
	
	
	##Compute Kmomentum
	Ns=$(($cells * $Denom))
	if [[ $(($cells%2)) == "1" ]]; then
	    Ksector=0;
	else
	    Ksector=$(($cells/2));
	fi
	
	Ksector_hamm=$[  $Ksector  ]
	
	LPow=$Denom
	
	###Run the chiral_wave_function first
	cd $Catalouge
	$dir_CFT/Run_CFT_Wfn -D 1 --kappa $Denom -K $Ksector -I $Tau_Im -R $Tau_Re -Ne $cells -N $Samples -KL $Ksector -wgt $Catalouge/wgt_cft.hdf5 -wfn $Catalouge/wfn_cft.hdf5 -qL $LPow -pos $Catalouge/pos.hdf5 --seed 0 > /dev/null


	MCSampleLaughlin.py -q $Denom -K1 $Ksector -I $Tau_Im -R $Tau_Re -Ne $cells -N $Samples -wfn $Catalouge/wfn_hammer.hdf5 -pos $Catalouge/pos.hdf5 -r #>/dev/null

	
	echo "COMPLETED LAUGHLIN"
	
	#### Compute the normalization directly on the data
	python $dir_CFT/test_scripts/check_identical_vectors.py $Catalouge/wfn_cft.hdf5 $Catalouge/wfn_hammer.hdf5 loglog prop prop --Precision -6
	
	###Clean by removing the calouge
	rm -r $Catalouge
	
	
    done 
done
