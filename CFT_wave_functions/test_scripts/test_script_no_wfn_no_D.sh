#!/bin/bash

##Test that if and only if  the --no-wfn flag has been given that no -D need to be supplied.

set -eux

###MAKEDEP:Run_CFT_Wfn


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

###Initalize the catalouge to be empty
Catalouge=$MyDir/Test_no_wfn_no_D
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge


N=100
Ne=5
Kmom=0
q=3

pos=$Catalouge/pos_tmp.hdf5
wfn=$Catalouge/wf_tmp.hdf5
wgt=$Catalouge/weight_tmp.hdf5

####--------------------------------------------------------------



### Test that no -D infomation need to be supplied if --no-wfn was given
$dir_CFT/Run_CFT_Wfn -Ne $Ne -N $N -pos $pos -wgt $wgt --no-wfn

if $dir_CFT/Run_CFT_Wfn -Ne $Ne -N $N -pos $pos -wgt $wgt ; then
    echo "Program did not fail as expected. Bad!" 
    exit -1
else 
    echo "Program failed as expected. Good!"
fi
####--------------------------------------------------------------

###Cleaning afterwards
rm -r $Catalouge
