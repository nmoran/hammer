#!/bin/bash


set -e
set -u

###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:TransformState.py
###MAKEDEP:Overlap.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_states_ph
#### "Uggly" ways of cleaning catalouge
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge

###Relevant Names
VecK1K2=$(FQHFileNames.py -p 4 -l 12 -y 2 -c 2 --tau1 0.5 --tau2 2.0 --type 2)
VecK1=$(FQHFileNames.py -p 4 -l 12 -y 2 --tau2 2.0 --tau1 0.5 --type 2)
VecK1K2PH=$(FQHFileNames.py -p 8 -l 12 -y 4 -c 0 --tau1 0.5 --tau2 2.0 --type 2)
VecK1PH=$(FQHFileNames.py -p 8 -l 12 -y 4 --tau1 0.5 --tau2 2.0 --type 2)


echo "Testing PH conjugation and the expansion functionality"
TorusDiagonalise.py -p 4 -l 12 -y 2 -c 2 --eigenstate --nbr-eigvals 1 --nbr-states 1  --tau1 0.5 --tau2 2.0

ls

###Transform the three paths..
### | a > ---- | PH >
###   |   \       |
###   |     \     |
### | full> --| PH,full>

TransformState.py -v -c ${VecK1K2}_State_0.vec --id _PH  ###PH COnjugate
TransformState.py -v -f ${VecK1K2}_State_0.vec --id _Full ###Make Full Basis
TransformState.py -v -c -f ${VecK1K2}_State_0.vec --id _PHF ###Make Full Basis

### Tansform the rest of the paths to PH...
TransformState.py -v -c ${VecK1}_State_0_Full.vec --id _PH  ###PH COnjugate
TransformState.py -v -f ${VecK1K2PH}_State_0_PH.vec --id _Full ###Make Full Basis


echo "Following overlaps should be one."
Ov=$(Overlap.py -a ${VecK1PH}_State_0_PHF.vec ${VecK1PH}_State_0_PH_Full.vec  )
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after differnt types of PH conjugattion is not 1.0 (but $Ov )!"
    exit -1
fi

Ov=$(Overlap.py -a ${VecK1PH}_State_0_PHF.vec ${VecK1PH}_State_0_Full_PH.vec  )
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after differnt types of PH conjugattion is not 1.0 (but $Ov )!"
    exit -1
fi

###SHould be redundant...
Ov=$(Overlap.py -a ${VecK1PH}_State_0_PH_Full.vec ${VecK1PH}_State_0_Full_PH.vec  )
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after differnt types of PH conjugattion is not 1.0 (but $Ov )!"
    exit -1
fi


###Cleaning afterwards
rm -r $Catalouge
