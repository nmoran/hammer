#!/bin/bash

##Test that the implicit none flag is set in the f.90 code

file=$1
##echo file: $file
Grep_result=$(grep -i 'implicit none' $file)
if [ "$Grep_result" == "" ] ; then 
    echo HARD WARNING:
    echo "There is no 'implicit none' in the module": $file
    exit -1
fi

##Test that random_seed is not used
##But expluce the file randome_seed.f90 as it is excluded
Grep_result=$(grep -i ' src/random_seed' $file)
if [ "$Grep_result" != "" ] ; then 
    if [ "$file" != misc_random.f90 ] ; then
	echo HARD WARNING:
	echo "There is a call to 'random_seed' instead of 'init_random_seed' in the module": $file
	grep -i ' random_seed' $file --color -C 1 -n 
	exit -1
    fi
fi

