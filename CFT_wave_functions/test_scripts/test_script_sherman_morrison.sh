#!/bin/bash

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:EvaluateStatesRealSpace.py
###MAKEDEP:test_scripts/compare_datasets.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_sherman
#### "Uggly" ways of cleaning catalouge
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge

Ne=4
N=100
Nflux=$((${Ne}*3))
K1=2


# Generate some samples
$dir_CFT/Run_CFT_Wfn -D 1 --Kmatrix 3 -N ${N} -Ne ${Ne} -K $K1

echo ...............................
echo Diagonalize Hamiltonian
echo ...............................


# Get some states
mpiexec -np 1 TorusDiagonalise.py -p ${Ne} -l ${Nflux} -y $K1 --eigenstate

echo ...............................
echo EVALUTE STATES THE STANDARD WAY
echo ...............................

# Evaluate the states
EvaluateStatesRealSpace.py --hdf5-positions positions.hdf5 --verbose Torus_p_${Ne}_Ns_${Nflux}_K_${K1}_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0.vec --output hammer  --hdf5-output

echo .....................................
echo EVALUTE STATES USING SHERMAN MORRISON
echo .....................................


# Evaluate the states with sherman morrison
EvaluateStatesRealSpace.py --hdf5-positions positions.hdf5 --verbose Torus_p_${Ne}_Ns_${Nflux}_K_${K1}_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0.vec --sherman-morrison --output hammer_sm --hdf5-output

python $MyDir/compare_datasets.py -f1 hammer.hdf5 -f2 hammer_sm.hdf5

###Cleaning afterwards
rm -r $Catalouge
