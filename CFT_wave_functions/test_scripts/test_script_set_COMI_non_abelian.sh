#!/bin/bash

##This script runs CFT code with qps: and extracs the 1p correlation functions

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py
###MAKEDEP:test_scripts/Move_coords.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_T1_T2q_eigenstate
mkdir -p $Catalouge
cd $Catalouge


N=100
D=2
Ne=2
Kmom=1
Kmatrix="3 1 1 3"
Ns=4
q_nu=2
q_group=4

pos_start=pos-here.hdf5
pos_move_x=pos-x.hdf5
pos_move_y=pos-y.hdf5

###Run the torus code with random seed
$dir_CFT/Run_CFT_Wfn -D $D --kappa $Kmatrix -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf.hdf5 -wgt $Catalouge/weight.hdf5

###...................Now change COM sector.......................

$dir_CFT/Run_CFT_Wfn -D $D --kappa $Kmatrix -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf_cm1.hdf5 -wgt $Catalouge/weight_cm1.hdf5 -r --COMI 1

###...................Translate coords.......................

python $MyDir/Move_coords.py --coord-in $Catalouge/$pos_start --coord-out $Catalouge/$pos_move_x  -x 1 --fluxes $Ns
python $MyDir/Move_coords.py --coord-in $Catalouge/$pos_start --coord-out $Catalouge/$pos_move_y  -y $q_nu --fluxes $Ns

### Run with translated coordinates in x-direction
$dir_CFT/Run_CFT_Wfn -D $D --kappa $Kmatrix -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_move_x -wfn $Catalouge/wfx.hdf5 -wgt $Catalouge/weightx.hdf5 -r

### Run with translated coordinates in y-direction
$dir_CFT/Run_CFT_Wfn -D $D --kappa $Kmatrix -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_move_y -wfn $Catalouge/wfy.hdf5 -wgt $Catalouge/weighty.hdf5 -r


###...................Now change COM sector.......................

### Run with translated coordinates in x-direction
$dir_CFT/Run_CFT_Wfn -D $D --kappa $Kmatrix -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_move_x -wfn $Catalouge/wfx_cm1.hdf5 -wgt $Catalouge/weightx_cm1.hdf5 -r --COMI 1

### Run with translated coordinates in y-direction
$dir_CFT/Run_CFT_Wfn -D $D --kappa $Kmatrix -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_move_y -wfn $Catalouge/wfy_cm1.hdf5 -wgt $Catalouge/weighty_cm1.hdf5 -r --COMI 1


###...................  Tests.......................


###Test that the T1 move give eigenstate
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wfx.hdf5 loglog same diff
python $MyDir/check_identical_vectors.py $Catalouge/wf_cm1.hdf5 $Catalouge/wfx_cm1.hdf5 loglog same diff

###Test that the T2^q move give eigenstate
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wfy.hdf5 loglog same diff
python $MyDir/check_identical_vectors.py $Catalouge/wf_cm1.hdf5 $Catalouge/wfy_cm1.hdf5 loglog same diff

###...................Now change COM sector.......................

###Test that the new com sector gives as new state
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_cm1.hdf5 loglog diff diff
python $MyDir/check_identical_vectors.py $Catalouge/wfx.hdf5 $Catalouge/wfx_cm1.hdf5 loglog diff diff
python $MyDir/check_identical_vectors.py $Catalouge/wfy.hdf5 $Catalouge/wfy_cm1.hdf5 loglog diff diff


###Cleaning afterwards
rm -r $Catalouge
