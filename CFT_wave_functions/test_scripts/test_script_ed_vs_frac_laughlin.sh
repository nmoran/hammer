#!/bin/bash

##test that the ed and cft gives the same values for Laughlin (even when the Kmatrix is fractionalized)

set -e
set -u


###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:MCImportanceOverlaps.py
###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:EvaluateStatesRealSpace.py
###MAKEDEP:test_scripts/check_identical_vectors.py


## Changes direcotry ot the location of the spript
MyName=$(basename $0)
cd $(dirname $0)


MyDir=$(pwd) ###SHould be lovated in the test_script folder

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit -1

###Set the directories usefull
dir_CFT=$MyDir/../../CFT_wave_functions
cd $dir_CFT
dir_CFT=$(pwd)
echo $dir_CFT


Samples=10
cells=5
Denom=3
Tau_Im=1.0
Tau_Re=0.4
LPow=$Denom

Tau_Im=$(LC_ALL=C /usr/bin/printf "%.*f\n" 3 $Tau_Im)
Tau_Re=$(LC_ALL=C /usr/bin/printf "%.*f\n" 3 $Tau_Re)
Tau_Re_Hamm=$(LC_ALL=C /usr/bin/printf "%.*f\n" 3 $(echo "-1 * $Tau_Re" | bc ))

Ne=$cells
Ns=$[ $cells*$Denom ]



##Compute Kmomentum
Ns=$(($cells * $Denom))
if [[ $(($cells%2)) == "1" ]]; then
    Ksector=0;
else
    Ksector=$(($cells/2));
fi

Ksector_hamm=$[  $Ksector  ]


echo '-----------------------'
echo ' CFT First and ED then'
echo '-----------------------'
for kdenom in 1 2 3 4 ; do 

    #Define cataluge name and create folders
    Catalouge=$MyDir/Test_Hammer_vs_Laughlin,q=$Denom,c=$cells,I=$Tau_Im,R=$Tau_Re
    echo "Files will be stored in $Catalouge"
    mkdir -v -p $Catalouge
    ###Clean catalouge so no old files a casuing trouble
    rm -f $Catalouge/*


    cd $dir_CFT
    echo "........................................."
    echo " Generating CFT data for kdenom = "$kdenom
    echo "........................................."
    ./Run_CFT_Wfn -D 1 --kappa $[$kdenom * $Denom] -K $Ksector -I $Tau_Im -R $Tau_Re -Ne $cells -N $Samples -KL $Ksector -wfn $Catalouge/wfn_cft.hdf5 -qL $LPow -pos $Catalouge/pos.hdf5 --k_denom $kdenom -wgt $Catalouge/prob.hdf5


    ####The last two are the CoM momentum and the inversion sector
    ###Use Hammer (assumed to be in path) and Get realspace wave-functions
    TorusDiagonalise.py -p $Ne -l $Ns -y $Ksector_hamm --tau1 $Tau_Re_Hamm --tau2 $Tau_Im  --nbr-states 1 --nbr-eigval 2 --interaction Delta --eigenstate -c $Ksector_hamm --inversion-sector 0
    ###The name of the output vector
    GS_NAME=Torus_p_${Ne}_Ns_${Ns}_K_${Ksector_hamm}_P_${Ksector_hamm}_I_0_tau1_${Tau_Re_Hamm}_tau2_${Tau_Im}_maglen_1.00_LL0Delta_State_0.vec
    cp -v $GS_NAME $Catalouge/GS_state.dat
    ###Evaluate the real space wave-functions 
    EvaluateStatesRealSpace.py -v --hdf5-positions $Catalouge/pos.hdf5 $GS_NAME --output $Catalouge/wfn_exdiag
    EvaluateStatesRealSpace.py -v --hdf5-positions $Catalouge/pos.hdf5 $GS_NAME --output $Catalouge/wfn_exdiag --hdf5-output --hdf5-nometa
    mv -v $GS_NAME $Catalouge/GS_state.dat
    ###Move the interesting files away
    mv -v Torus_p_${Ne}_Ns_${Ns}_K_${Ksector_hamm}_P_${Ksector_hamm}_I_0_Basis.basis $Catalouge/fock_basis.dat
    
    
    ###Clean any other data not used
    rm -f Torus_p_*
    rm -f RunLog_*
    
    #### Compute the normalization directly on the data
    python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft.hdf5 $Catalouge/wfn_exdiag.hdf5 loglin prop prop
    
    ###Clean by removing the Catalouge
    rm -r $Catalouge

    
done




