#!/bin/bash

##This script runs CFT code, generates a 

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:ModularTransformState.py
###MAKEDEP:MCImportanceOverlaps.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_basis_state
mkdir -p $Catalouge
cd $Catalouge

N=100
Ne=1
Kmom1=1
Kmom2=2
Kmom3=3
q=3
Ns=$((q*Ne))

pos=$Catalouge/pos.hdf5
pos_rot=$Catalouge/pos_rot.hdf5
wf_x_1=$Catalouge/wfn_x_1.hdf5
wf_x_1_rot=$Catalouge/wfn_x_1_rot.hdf5
wf_x_2=$Catalouge/wfn_x_2.hdf5
wf_x_2_rot=$Catalouge/wfn_x_2_rot.hdf5
wf_x_3=$Catalouge/wfn_x_3.hdf5
wf_x_3_rot=$Catalouge/wfn_x_3_rot.hdf5
wf_y_1=$Catalouge/wfn_y_1.hdf5
wf_y_2=$Catalouge/wfn_y_2.hdf5
wf_y_3=$Catalouge/wfn_y_3.hdf5

###Run the torus code once
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom1 -N $N -dl 100 -pos $pos -wfn $wf_x_1 -wgt $Catalouge/weight.hdf5  > /dev/null

$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom2 -N $N -dl 100 -pos $pos -wfn $wf_x_2  -r > /dev/null

$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom3 -N $N -dl 100 -pos $pos -wfn $wf_x_3  -r > /dev/null

###Perform an SS-transform
echo "Perform SS-transform on the wfn"
ModularTransformState.py --wfn-in $wf_x_1 --wfn-out $wf_x_1_rot --fluxes $Ns --coord-in $pos --coord-out $pos_rot --transformation SS
ModularTransformState.py --wfn-in $wf_x_2 --wfn-out $wf_x_2_rot --fluxes $Ns --coord-in $pos  --transformation SS
ModularTransformState.py --wfn-in $wf_x_3 --wfn-out $wf_x_3_rot --fluxes $Ns --coord-in $pos  --transformation SS

###Run the torus code once again with rotated coords
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom1 -N $N -pos $pos_rot -wfn $wf_y_1  -r  > /dev/null
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom2 -N $N -pos $pos_rot -wfn $wf_y_2  -r  > /dev/null
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom3 -N $N -pos $pos_rot -wfn $wf_y_3  -r  > /dev/null

ULimit=1.001
LLimit=0.999
Target=1.000


test_overlap_in_limit() {

wf1=$1
wf2=$2
####Check that the overlap between the wave functions is 1 again
echo "Check that the overlap between the wave functions $wf1 and $wf2 is in the range $ULimit > Ov > $LLimit (with target $Target"
MCImportanceOverlaps.py --wfn-list $wf1 --ed-list $wf2 --wgt-list  $Catalouge/weight.hdf5 --cft-scale 'log' --ed-scale 'log' --output-prefix $Catalouge/Overlap --bins 10

##Extras 6th column of last (and only) line
Overlap=`tail -n 1 $Catalouge/Overlap.csv  | awk 'BEGIN { FS = "," } ; {print $6}'`
echo Overlap: $Overlap
if (( $(bc <<< "$Overlap > $ULimit") )) ; then
    echo "ERROR: While testing $wf1 vs $wf2"
    echo "Error: Overlap is larger than $ULimit!"
    exit -1
fi   
if (( $(bc <<< "$Overlap < $LLimit") )) ; then
    echo "ERROR: While testing $wf1 vs $wf2"
    echo "Error: Overlap is smaller than $LLimit!"
    exit -1
fi

}

test_overlap_in_limit $wf_x_1_rot $wf_y_2
test_overlap_in_limit $wf_x_2_rot $wf_y_1
test_overlap_in_limit $wf_x_3_rot $wf_y_3

###Cleaning afterwards
rm -r $Catalouge
