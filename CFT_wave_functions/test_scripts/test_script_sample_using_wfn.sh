#!/bin/bash

##This script runs CFT code and tests that the pobability discribution can use the CFT wnf itself.

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_sample_data
mkdir -p $Catalouge
cd $Catalouge

N=100
Ne=5
Kmom=0

pos=$Catalouge/pos.hdf5
wfn=$Catalouge/wf.hdf5
wgt=$Catalouge/weight.hdf5

####--------------------------------------------------------------

echo TRY WGT AGAINS LAUGHLIN

### Run the cft again and this time chack that the weight is proportional to the laughlin state
$dir_CFT/Run_CFT_Wfn -qL 3 -D 1 --kappa 3 -Ne $Ne -KL $Kmom -K $Kmom -N $N -pos $pos -wfn $wfn -wgt $wgt > /dev/null

###Test that the wave function are different that the weights
python $MyDir/check_identical_vectors.py $wfn $wgt loglog prop prop

####--------------------------------------------------------------
Ne=4
Kmom=0

echo TRY WGT AGAINS NU 2 5

###Run the torus code oce (turn of antisymmetrize for speedup)
$dir_CFT/Run_CFT_Wfn -A -D 2 --kappa 3 2 2 3 -Ne $Ne -K $Kmom -N $N -pos $pos -wfn $wfn -wgt $wgt > /dev/null

###Test that the wave function are different that the weights
python $MyDir/check_identical_vectors.py $wfn $wgt loglog nonprop nonprop

####--------------------------------------------------------------
echo TRY WGT of NU 2 5 AGAINS NU 2 5

### Run the cft again (using the cft as weight this time) (turn of antisymmetrize for speedup)
$dir_CFT/Run_CFT_Wfn -A -D 2 --kappa 3 2 2 3 -Ne $Ne -K $Kmom -N $N -pos $pos -wfn $wfn -wgt $wgt --use-cft-as-wgt #> /dev/null

###Test that the wave function are different that the weights
python $MyDir/check_identical_vectors.py $wfn $wgt loglog same same




###Cleaning afterwards
rm -r $Catalouge
