#!/bin/bash

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:EvaluateStatesRealSpace.py
###MAKEDEP:test_scripts/check_identical_vectors.py

set -e ##Exit on error

## Changes direcotry ot the location of the spript
MyName=$(basename $0)
cd $(dirname $0)
MyDir=`pwd`

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
dir_CFT=$MyDir/../../CFT_wave_functions
cd $dir_CFT
dir_CFT=$(pwd)
echo $dir_CFT


Denom=3
Cells=5
Kmom=-1
MCPoints=20
Tau_Im=1.0
Tau_Re=0.0

###Make 4 digit names
Tau_Re=$(printf %.4f $Tau_Re)
Tau_Im=$(printf %.4f $Tau_Im)

### Automatically determine K-sector if is not explicitly set
if [[ "$Kmom" == -1 ]]; then ##Assume K=-1 means choose momentum automatically
    if [ $((Cells%2)) -eq 0 ]; then
	Kmom=$((Cells / 2))
    else
	Kmom=0
    fi
fi

Catalouge="$MyDir/Test_same_with_fortran_and_hammer_v0"

PosH=$Catalouge/pos_hammer.hdf5
WgtH=$Catalouge/probs_hammer.hdf5
WfnH=$Catalouge/wfn_hammer.hdf5
Wfn0H=$Catalouge/wfn_0_hammer.hdf5
Wfn1H=$Catalouge/wfn_1_hammer.hdf5
PosC=$Catalouge/pos_cft.hdf5
WgtC=$Catalouge/probs_cft.hdf5
WfnC=$Catalouge/wfn_cft.hdf5
Wfn0C=$Catalouge/wfn_0_cft.hdf5
Wfn1C=$Catalouge/wfn_1_cft.hdf5
echo Catalouge: $Catalouge
echo Catalouge: $Catalouge

mkdir -vp $Catalouge
rm -r $Catalouge
mkdir -vp $Catalouge
cd $Catalouge


MC_Sampling="-dl 10 -th 200"
###Generate the coordinates used (sample ordinary laughlin with no quasiholes)
MCSampleLaughlin.py -q $Denom -Ne $Cells $MC_Sampling -N $MCPoints -K $Kmom -R $Tau_Re -I $Tau_Im -pos  $PosH -wfn $WfnH -wgt $WgtH --Sample-at-Ns $((Denom*Cells)) 
#> $Catalouge_MC/output.log 2>&1

###Sample the laughlin state again
MCSampleLaughlin.py -q $Denom -Ne $Cells -N $MCPoints -K $Kmom -R $Tau_Re -I $Tau_Im -pos $PosH -wfn $Wfn0H  -r --pos-style=lattice
#> $Catalouge_MC/output.log 2>&1

####Sample the laughlin ste using the fortan code
$dir_CFT/Run_CFT_Wfn -D 1 --Kmatrix $Denom -Ne $Cells -N $MCPoints -K $Kmom -R $Tau_Re -I $Tau_Im -pos  $PosH -wfn $Wfn0C -r 


#### Compare the differnt sets of data
python $dir_CFT/test_scripts/check_identical_vectors.py $WfnH $Wfn0H loglog prop prop --Precision -6

python $dir_CFT/test_scripts/check_identical_vectors.py $WfnH $Wfn0C loglog prop prop --Precision -6


###Clean by removing the calouge
rm -r $Catalouge
