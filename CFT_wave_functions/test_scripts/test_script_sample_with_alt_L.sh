#!/bin/bash

##This script runs CFT code and tests that the pobability discribution for altenative laugjhlin is correct.

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_alt_laluglin
mkdir -p $Catalouge
cd $Catalouge

N=100
Ne=5
Kmom=0

pos=pos.hdf5
wfn=wf.hdf5
wgt=weight.hdf5

####--------------------------------------------------------------

echo TRY WGT AGAINS LAUGHLIN

q=3
d=2

### Run the cft again and this time chack that the weight is proportional to the laughlin state
$dir_CFT/Run_CFT_Wfn -qL $q -d_alt $d -D 1 --Kmatrix $q --kappa_bar $d -Ne $Ne -KL $Kmom -K $Kmom -N $N -pos $pos -wfn $wfn -wgt $wgt 

###Test that the wave function are different that the weights
python $MyDir/check_identical_vectors.py $wfn $wgt loglog prop prop


###Cleaning afterwards
rm -r $Catalouge
