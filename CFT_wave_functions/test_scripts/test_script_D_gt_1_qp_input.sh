#!/bin/bash

##test that the cft gives the correct values

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:MCImportanceOverlaps.py

## Changes direcotry ot the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd) ###SHould be lovated in the test_script folder

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit -1

###Set the directories usefull
dir_CFT=$MyDir/../../CFT_wave_functions
cd $dir_CFT
dir_CFT=$(pwd)
echo $dir_CFT

N=100
c=2
Kmat="3 2 2 3"
Ksec=0

echo '-----------------------'
echo ' Test that multi-component wave functions can be generated'
echo '-----------------------'

#Define cataluge name and create folders
Catalouge=$MyDir/Test_muli_comp_qp_input
echo "Files will be stored in $Catalouge"
mkdir -v -p $Catalouge
##Remove any files that might be lingering from an older run
rm -f $Catalouge/*

echo ' .-.-.-.Generating Laughlin qp data .-.-.-.'
###Put two quasi-particles in each group (increases raw flux by 2)
./Run_CFT_Wfn -D 2 --kappa $Kmat -K $Ksec -Ne $c -N $N -wfn $Catalouge/wfn_cft.hdf5 -pos $Catalouge/pos.hdf5 -Nq 2 2 --qp-pos-x 0.0 0.2 0.7 0.5 --qp-pos-y 0.0 0.7 0.2 0.5 -wgt $Catalouge/prob.hdf5

./Run_CFT_Wfn -D 2 --kappa $Kmat -K $Ksec -Ne $c -N $N -wfn $Catalouge/wfn_reuse.hdf5 -pos $Catalouge/pos.hdf5 -Nq 2 2 --qp-pos-x 0.0 0.2 0.7 0.5 --qp-pos-y 0.0 0.7 0.2 0.5  -r

./Run_CFT_Wfn -D 2 --kappa $Kmat -K $Ksec -Ne $c -N $N -wfn $Catalouge/wfn_qp_static.hdf5 -pos $Catalouge/pos.hdf5 -Nq 2 2 --qp-pos-x 0.0 0.2 0.7 0.5 --qp-pos-y 0.0 0.7 0.2 0.5 -r  --qp-static && echo "-------------------------------" && echo "    --qp-static should not be implmented for D>0 !"  && echo "-------------------------------" && exit -1 || echo this should not work! 

###Test that the qp-static flag gives differnt results (so something is done)
echo " Comparing the qp-ouput can re reproduced with the -r flag"
	python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft.hdf5 $Catalouge/wfn_reuse.hdf5 loglog same same

#echo " Comparing the qp-ouput with or without the --qp-static flag"
#	python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft.hdf5 $Catalouge/wfn_reuse.hdf5 loglog same same

##Clean the catalouge at the en
rm -r $Catalouge
