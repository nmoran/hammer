#!/bin/bash

##This script runs CFT code and tests that it can sample wieghts withour sampling cft.

set -e
set -u

###MAKEDEP:Run_CFT_Wfn


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

###Initalize the catalouge to be empty
Catalouge=$MyDir/Test_no_wfn
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge

N=100
Ne=5
Kmom=0
q=3

pos=$Catalouge/pos_tmp.hdf5
wfn=$Catalouge/wf_tmp.hdf5
wgt=$Catalouge/weight_tmp.hdf5

####--------------------------------------------------------------



### Run the theorus laughlin and make sure that no wfn-output is made
$dir_CFT/Run_CFT_Wfn -D 1 --Kmatrix $q -Ne $Ne -KL $Kmom -K $Kmom -N $N -pos $pos -wfn $wfn -wgt $wgt --no-wfn

###test if the file  $wfn is present
if [ -e $wfn ] ; then
    echo
    echo ERROR: No output file \"$wfn\" should exist when flag '--no-wfn' is used!
    exit -1
fi

####--------------------------------------------------------------

###Cleaning afterwards
rm -r $Catalouge
