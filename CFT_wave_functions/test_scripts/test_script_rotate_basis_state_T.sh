#!/bin/bash

##This script runs CFT code, generates a 

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:ModularTransformState.py
###MAKEDEP:MCImportanceOverlaps.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_basis_state
mkdir -p $Catalouge

N=100
Ne=2
Kmom=1
q=3
Ns=$((q*Ne))

pos=$Catalouge/pos.hdf5
pos_rot=$Catalouge/pos_rot.hdf5
wf_x=$Catalouge/wfn_x.hdf5
wf_x_rot=$Catalouge/wfn_x_rot.hdf5
wf_y=$Catalouge/wfn_y.hdf5

###Run the torus code once
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -N $N -dl 100 -pos $pos -wfn $wf_x -wgt $Catalouge/weight.hdf5  #> /dev/null

###Perform an T-transform
echo "Perform T-transform on the wfn"
ModularTransformState.py --wfn-in $wf_x --coord-in $pos --wfn-out $wf_x_rot --coord-out $pos_rot --transformation T --fluxes $Ns  #> /dev/null

###Run the torus code once again with the new T-transformed 
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -N $N -pos $pos_rot -wfn $wf_y  -R 1.0 -r  #> /dev/null

ULimit=1.001
LLimit=0.999
Target=1.0
####Check that the overlap between the wave functions is 1 again
echo "Check that the T-transformed overlap between the wave functions is in the range $ULimit > Ov > $LLimit (with target $Target"
MCImportanceOverlaps.py --wfn-list $wf_x_rot --ed-list $wf_y --wgt-list  $Catalouge/weight.hdf5 --cft-scale 'log' --ed-scale 'log' --output-prefix $Catalouge/Overlap --bins 10

##Extras 6th column of last (and only) line
Overlap=`tail -n 1 $Catalouge/Overlap.csv  | awk 'BEGIN { FS = "," } ; {print $6}'`
echo Overlap: $Overlap
if (( $(bc <<< "$Overlap > $ULimit") )) ; then
    echo "Error: Overlap is larger than $ULimit!"
    exit -1
fi   
if (( $(bc <<< "$Overlap < $LLimit") )) ; then
    echo "Error: Overlap is smaller than $LLimit!"
    exit -1
fi

###Cleaning afterwards
rm -r $Catalouge
