#!/bin/bash


set -e
set -u

File1Re=$1
File1Im=$2
File2Re=$3
File2Im=$4

echo Files:
echo File1Re: $File1Re
echo File1Im: $File1Im
echo File2Re: $File2Re
echo File2Im: $File2Im

LogType=$5 ###Are the vectors loglog, linlog,or linlin
DiffType=$6 ####Do we check if the vectors are the same or different?

if [ "$LogType" == loglog ] ; then
    LogCode="[0,0]"
elif [ "$LogType" == loglin ] ; then
    LogCode="[0,1]"
elif [ "$LogType" == linlog ] ; then
    LogCode="[1,0]"
elif [ "$LogType" == linlin ] ; then
    LogCode="[1,1]"
else
    echo "Error:Unknown log type"
    exit -1
fi

if [ "$DiffType" == same ] ; then
    DiffCode="[1,1]"
elif [ "$DiffType" == diff ] ; then
    DiffCode="[-1,-1]"
elif [ "$DiffType" == samescale ] ; then
    DiffCode="[1,0]"
elif [ "$DiffType" == diffscale ] ; then
    DiffCode="[-1,0]"
elif [ "$DiffType" == samephase ] ; then
    DiffCode="[0,1]"
elif [ "$DiffType" == diffphase ] ; then
    DiffCode="[0,-1]"
else
    echo "Error:Unknown diff type"
    exit -1
fi

echo TypeCode : $LogCode
echo DiffCode : $DiffCode

###File assumes it is called from the CFT to directory.. so it starts by adding its own directory do octave
  octave -q --eval "addpath('./test_scripts');check_identical_vectors('$File1Re','$File1Im','$File2Re','$File2Im',$LogCode,$DiffCode)"

