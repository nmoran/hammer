from __future__ import print_function
import h5py
import numpy as np
from numpy import pi as pi
import math
import argparse
import sys

def __main__():
    parser = argparse.ArgumentParser(description="Test if two wave functions stored in hdf5-format are the same")
    parser.add_argument("File1", type=str, help="The first input hdf5-file")
    parser.add_argument("File2", type=str, help="The second input hdf5-file")
    parser.add_argument("LogType", type=str, help="The type of linear or log scale the files are in. Valid options are 'loglog', 'linlog', 'loglin' or 'linlin'.")
    parser.add_argument("DiffTypeScale", type=str, help="The type of comparison to per performed. Valid options are 'same', 'prop','nonprop','diff', '-'.")
    parser.add_argument("DiffTypeAngle", type=str, help="The type of comparison to per performed. Valid options are 'same', 'prop','nonprop','diff', '-'.\n The meaning is:\n'same'=all values are the same\n'diff'=all values are different\n'prop'=all values are proportional\n'nonprop'=values not proportional\n'-'=Do not test this at all")
    parser.add_argument("--Precision", type=int, help="The presision (10^{X}) at which different vectors are the same of different. Default is X=-11", default=-11)

    args = parser.parse_args()

    print("Precision set to : ",args.Precision) 
    pres=10**(args.Precision)
    print("Precision is thus : ",pres) 


    print("LogType is: ",args.LogType)
    if args.LogType == "loglog":
        LogType=(0,0)
    elif args.LogType == "loglin":
        LogType=(0,1)
    elif args.LogType == "linlog":
        LogType=(1,0)
    elif args.LogType == "linlin":
        LogType=(1,1)
    else:
        raise NameError(args.LogType,"is not a valid input")
    print("LogType is: ",LogType) 

    np.set_printoptions(linewidth=200)
    DiffTypeScale=getDiffType(args.DiffTypeScale,"DiffTypeScale")
    DiffTypeAngle=getDiffType(args.DiffTypeAngle,"DiffTypeSAngle")

    raw_wfval1=read_wnf(args.File1)
    raw_wfval2=read_wnf(args.File2)


    WF1=logarize(raw_wfval1,LogType[0])
    WF2=logarize(raw_wfval2,LogType[1])
    

    ##The difference between the two is now divided into scale differences and angle differences
    ScaleDiff=np.real(WF1-WF2);

    
    MedianScale1=np.median(np.real(WF1))
    MedianScale2=np.median(np.real(WF2))
    print("MedianScale1:",MedianScale1)
    print("MedianScale2:",MedianScale2)
    ZeroScale=20.0
    Zero1=MedianScale1>np.real(WF1)+ZeroScale
    Zero2=MedianScale2>np.real(WF2)+ZeroScale
    print("Num Zero1:",np.sum(Zero1))
    print("Num Zero2:",np.sum(Zero2))
    print("exp(-"+str(ZeroScale)+"):",np.exp(-ZeroScale))
    IsNonZero=np.logical_not(np.logical_or(Zero1,Zero2))
    #print("IsNonZero:",IsNonZero)

    ###Compute the differnce in angle
    AngleDiff=np.mod(np.imag(WF1-WF2)[IsNonZero],2*pi);
    ###Then take mod 2*pi of the ange differnce...
    ###Now, work out the average positon of the data...
    ### Then make the branch-cut hafway between the average angle and 2*pi or 0, depending on wich is larger...
    ##This way, if all the data is clumped somewhere, it will not be moved, and if
    ##it happens to clump precisely at 0,2*pi this, will be taken care of.
    MeanAngle=np.mean(AngleDiff)
    Shift1=2*np.pi-(2.0*np.pi+MeanAngle)/2
    Shift2=MeanAngle/2
    if Shift1>Shift2:
        Shift=Shift1
    else:
        Shift=2*np.pi-Shift2
    print("MeanAngle:",MeanAngle)
    print("Shift1:",Shift1)
    print("Shift2:",Shift2)
    print("Shift:",Shift)
    #print("AngleDiff:",AngleDiff)
    AngleDiff=np.mod(AngleDiff+Shift,2*pi)-Shift
    #print("AngleDiff:",AngleDiff)

    if DiffTypeScale==1: ###Test if the same
        test_difftype_1(ScaleDiff[IsNonZero],pres,WF1[IsNonZero],WF2[IsNonZero],'scale')
    elif DiffTypeScale==2:###Test if prop
        test_difftype_2(ScaleDiff[IsNonZero],pres,WF1[IsNonZero],WF2[IsNonZero],'scale')
    elif DiffTypeScale==3:###Test if different
        test_difftype_3(ScaleDiff[IsNonZero],pres,WF1[IsNonZero],WF2[IsNonZero],'scale')
    elif DiffTypeScale==4:###Test if different
        test_difftype_4(ScaleDiff[IsNonZero],pres,WF1[IsNonZero],WF2[IsNonZero],'scale')
    elif DiffTypeScale==0:###Do not test
        print('No scale test performed')
    else:
        raise Exception('Unknown test')


    if DiffTypeAngle==1: ###Test if the same
        test_difftype_1(AngleDiff,pres,WF1[IsNonZero],WF2[IsNonZero],'angle')
    elif DiffTypeAngle==2:###Test if prop
        test_difftype_2(AngleDiff,pres,WF1[IsNonZero],WF2[IsNonZero],'angle')
    elif DiffTypeAngle==3:###Test if different
        test_difftype_3(AngleDiff,pres,WF1[IsNonZero],WF2[IsNonZero],'angle')
    elif DiffTypeAngle==4:###Test if different
        test_difftype_4(AngleDiff,pres,WF1[IsNonZero],WF2[IsNonZero],'angle')
    elif DiffTypeAngle==0:###Do not test
        print('No Angle test performed')
    else:
        raise Exception('Unknown test')


    assert not math.isnan(sum(ScaleDiff)), 'Differnce WF1 and WF2 is NaN'
    assert not math.isnan(sum(AngleDiff)), 'Differnce WF1 and WF2 is NaN'
    print("The vectors passed the test")
    print("")



def getDiffType(DiffType,DiffTypeName):
    print(DiffTypeName+" is:",DiffType)
    if DiffType == "same":
        return 1
    elif DiffType == "prop":
        return 2
    elif DiffType == "diff":
        return 3
    elif DiffType == "nonprop":
        return 4
    elif DiffType == "-":
        return 0
    else:
        raise NameError(DiffType,"is not a valid input")



def logarize(raw_wfval,LogType):
    ####Construct the wave function
    if LogType:
        WF=raw_wfval[:,0]+1j*raw_wfval[:,1]
        WF=np.log(WF) ##Take the log if the functions is in linear scale
    else:
        WF=raw_wfval[:,0]+1j*np.mod(raw_wfval[:,1],2*pi)
    return WF
    
def read_wnf(File):
    print("Open file ",File," for read")
    with h5py.File(File, 'r') as wf:
        return np.array(wf.get('wf_values'))


def test_difftype_1(Diff,pres,WF1,WF2,Type): ###Same
    SumDiff=sum(abs(Diff)>pres)
    print('Number of different elements:',SumDiff)
    if SumDiff:
        print("WF1      WF2    WF1 - WF2 ")
        print(np.vstack((WF1,WF2,Diff)).T)
        print("Diff")
        print(Diff)
        Freq=np.histogram(Diff)
        print("Frequencies")
        print(Freq[0]) ##Frequencies
        ##print(Freq[1]) ##Bin edges
        raise Exception('WF1 and WF2 vectors are not the same '+Type)


def test_difftype_2(Diff,pres,WF1,WF2,Type): ####prop
    PropConst=np.mean(Diff)
    ReducedDiff=Diff-PropConst ###remove mean number (so sums to zero)
    SumDiff=sum(abs(ReducedDiff)>pres)
    print('Number of non-proportional elements:',SumDiff)
    if SumDiff:
        print("WF1      WF2       WF1-WF2")
        print(np.vstack((WF1,WF2,Diff)).T)
        print("Diff after removing prop constant")
        print(ReducedDiff)
        print("Prop-constat:",PropConst)
        Freq=np.histogram(ReducedDiff)
        print("Frequencies ReducedDiff:")
        print(Freq[0]) ##Frequencies
        print("Vals ReducedDiff:")
        print(Freq[1]) ##Frequencies
        raise Exception('WF1 and WF2 vectors are not proportional in '+Type)

def test_difftype_3(Diff,pres,WF1,WF2,Type): ###Diff
    SumDiff=sum(abs(Diff)<pres)
    print('Number of identical elements:',SumDiff)
    if SumDiff:
        print("WF1      WF2       WF1-WF2")
        print(np.vstack((WF1,WF2,Diff)).T)
        Freq=np.histogram(Diff)
        print("Frequencies")
        print(Freq[0]) ##Frequencies
        print("Vals Diff:")
        print(Freq[1]) ##Frequencies
        raise Exception('At least one element in WF1 and WF2 vectors have the same '+Type+' (when they shouldn\'t have)')


def test_difftype_4(Diff,pres,WF1,WF2,Type): ###Nonprop
    ReducedDiff=Diff-np.mean(Diff) ###remove mean number (so sums to zero)
    SumDiff=sum(abs(ReducedDiff)<pres)
    print('Number of identical elements:',SumDiff)
    if SumDiff:
        print("WF1      WF2       WF1-WF2")
        print(np.vstack((WF1,WF2,Diff)).T)
        Freq=np.histogram(Diff)
        print("Frequencies")
        print(Freq[0]) ##Frequencies
        print("Vals Diff:")
        print(Freq[1]) ##Frequencies
        raise Exception('At least one element in WF1 and WF2 vectors are proportional in '+Type+' (when they shouldn\'t have)')
    
if __name__ == '__main__' :
    __main__()
