import h5py
import argparse
import numpy as np
import sys


parser = argparse.ArgumentParser()
parser.add_argument('-f1', type=str, help='File1')
parser.add_argument('-f2', type=str, help='File2')

opts = parser.parse_args(sys.argv[1:])

f1=opts.f1
f2=opts.f2

f1 = h5py.File(f1, 'r')
dset1 = f1['wf_values']

f2 = h5py.File(f2, 'r')
dset2 = f2['wf_values']

# calculate maximum difference
max_diff = np.max(np.abs(dset2[:] - dset1[:]))
print('Maximum difference is ' + str(max_diff))

