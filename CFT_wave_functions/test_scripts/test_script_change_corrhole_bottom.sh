#!/bin/bash

##This scrips test that a flag that conrols the corrhole-depth can be set.

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_set_corrhole
mkdir -p $Catalouge
cd $Catalouge

N=1000
Ne=5
Kmom=0
q=3

pos_start=pos-here.hdf5

###Run the torus code with random seed
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf.hdf5 -wgt $Catalouge/weight.hdf5 -chL 0.01

rm -r $Catalouge
