#!/bin/bash

##test that the cft gives the correct values

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py

## Changes direcotry ot the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd) ###SHould be lovated in the test_script folder

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit -1

###Set the directories usefull
dir_CFT=$MyDir/../../CFT_wave_functions
cd $dir_CFT
dir_CFT=$(pwd)
echo $dir_CFT


N=1000
c=2
q=3
Ksec=1

echo '-----------------------'
echo ' Generate wave functiond with the qps moved a full revolution'
echo '-----------------------'

#Define cataluge name and create folders
Catalouge=$MyDir/Test_qp_input,q=$q,c=$c
echo "Files will be stored in $Catalouge"
mkdir -v -p $Catalouge
###We do not clean as any files will be deposited in the same catalouge


echo ' .-.-.-.Generating Laughlin qp data .-.-.-.'
./Run_CFT_Wfn -D 1 --kappa $q -K $Ksec -Ne $c -N $N -KL $Ksec -wfn $Catalouge/wfn_cft.hdf5 -qL $q -pos $Catalouge/pos.hdf5 -Nq 2 --qp-pos-x 0.0 0.5 --qp-pos-y 0.0 0.5 -wgt $Catalouge/prob.hdf5

#cp -v result/chiral_re.rnd result/test_re_00.rnd

for n in $( seq 1 $q) ; do
    echo '.-.-.-.Generating Laughlin qp data.-.-.-.' $n 0
    ./Run_CFT_Wfn -D 1 --kappa $q -K $Ksec -Ne $c -N $N -KL $Ksec -wfn $Catalouge/wfn_cft_${n}0.hdf5 -qL $q -pos $Catalouge/pos.hdf5 -Nq 2 --qp-pos-x ${n}.0 0.5 --qp-pos-y 0.0 0.5 -r
    
    echo '.-.-.-.Generating Laughlin qp data.-.-.-.' 0 $n
    ./Run_CFT_Wfn -D 1 --kappa $q -K $Ksec -Ne $c -N $N -KL $Ksec -wfn $Catalouge/wfn_cft_0${n}.hdf5 -qL $q -pos $Catalouge/pos.hdf5 -Nq 2 --qp-pos-x 0.0 0.5 --qp-pos-y ${n}.0 0.5 -r

    echo '.-.-.-.Generating Laughlin qp data.-.-.-.' 0 $n
    ./Run_CFT_Wfn -D 1 --kappa $q -K $[ $Ksec + $c*$n ] -Ne $c -N $N -KL $Ksec -wfn $Catalouge/wfn_cft_shift_${n}.hdf5 -qL $q -pos $Catalouge/pos.hdf5 -Nq 2 --qp-pos-x 0.0 0.5 --qp-pos-y 0.0 0.5 -r
    
done

for n in $( seq 1 $q) ; do 
    echo " Comparing the difference ${n} qp-translations in x-direction Expect proportional"
    echo FIRST TEST!!!
    python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft.hdf5 $Catalouge/wfn_cft_${n}0.hdf5 loglog prop prop

    echo SECOND TEST!!!
    echo " Comparing the difference ${n} qp-translations in y-direction with ${n}. Expect proportional"
    python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft_shift_${n}.hdf5 $Catalouge/wfn_cft_0${n}.hdf5 loglog prop prop

    echo THIRD TESTS!!!
    if [ "$n" != 1 ] ; then
	echo " Comparing the difference ${n} qp-translations in y-direction with 1. Expect differnt"
	python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft_shift_1.hdf5 $Catalouge/wfn_cft_0${n}.hdf5 loglog diff diff
    fi
    
    if [ "$n" != 2 ] ; then
	echo " Comparing the difference ${n} qp-translations in y-direction with 2. Expect differnt"
	python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft_shift_2.hdf5 $Catalouge/wfn_cft_0${n}.hdf5 loglog diff diff
	
    fi
    
    if [ "$n" != 3 ] ; then
	echo " Comparing the difference ${n} qp-translations in y-direction with 3. Expect differnt"
	python test_scripts/check_identical_vectors.py $Catalouge/wfn_cft_shift_3.hdf5 $Catalouge/wfn_cft_0${n}.hdf5 loglog diff diff
    fi
    
done

##Clean the catalouge at the en
rm -r $Catalouge
