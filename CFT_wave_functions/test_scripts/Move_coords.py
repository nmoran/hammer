#! /usr/bin/env python

""" ModularTransformState.py: Script that applies a modular transformation (or serries thereof) to coordinates and also applies the corresponding gauge transformation to the wave function. """

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"

import sys
import argparse
import numpy as np
import h5py
import time

def __main__():

    """
    Main driver function, if script called from the command line it will use the command line arguments as the vector filenames.
    """
    full_start = time.time()
    Parser = argparse.ArgumentParser(
        description='Utility to perform translations on coordinates in x and y directions.')
    Parser.add_argument('--coord-in', type=str, help='input file for (x,y) coordinates, z=x+tau*y.')
    Parser.add_argument('--coord-out', type=str, help='output file for (x\',y\') coordinates, z\'=x\'+tau*y\'.')
    Parser.add_argument('-x', type=float, help='Steps in x direction.')
    Parser.add_argument('-y', type=float, help='Steps in y direction.')
    Parser.add_argument('--fluxes',type=int,help='Number of fluxes. Required to know size of lattice of translations.')

    Args = Parser.parse_args()
    coord_in=Args.coord_in
    coord_out=Args.coord_out
    Nphi=Args.fluxes
    x=Args.x
    y=Args.y
    if x!=None:
        x_shift=x/Nphi
    else:
        x_shift=0
    if y!=None:
        y_shift=y/Nphi
    else:
        y_shift=0
    print('xshift=',x_shift)
    print('yshift=',y_shift)
    

    ### Load the Wave-functions and the coordinates
    ensure_flag_dep(coord_out,'--coord-out', coord_in,'--coord-in')
    ensure_flag_dep(coord_out,'--coord-out', Nphi,'--fluxes')

    if coord_in!=None:
        print(("Open file '"+coord_in+"' for reading.."))
        with h5py.File(coord_in, 'r') as coord:
            xcoord_in=np.array(coord.get('xvalues'))
            ycoord_in=np.array(coord.get('yvalues'))
            
        ###Compute the transforfmation one at a time
        xcoord_out=xcoord_in+x_shift
        ycoord_out=ycoord_in+y_shift

    if coord_out!=None:
        print("Write the transformed coordinates to file:")
        print(coord_out)
        try:
            of = h5py.File(coord_out, 'w')
        except IOError as ioerror:
            print(("ERROR: Problem writing to file " + coord_out  + ': ' + str(ioerror)))
            sys.exit(-1)
        of.create_dataset("xvalues", data=xcoord_out)
        of.create_dataset("yvalues", data=ycoord_out)
        of.close()

def ensure_flag_dep(F1,FName1,F2,FName2):
    if F1!=None:
        ##Flag 1 has a value
        if F2==None:
            print(("ERROR: Flag "+FName1+" depends on flag "+FName2+", which has not been set!"))
            sys.exit(-1)

if __name__ == '__main__' :
    __main__()
