#!/bin/bash

##This script runs CFT code, generates a 

set -e
set -u

###MAKEDEP:ModularTransformState.py
###MAKEDEP:test_scripts/do_transform_tau.sh



## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)
./do_transform_tau.sh 0.000 1.000 0.000 1.000 S
./do_transform_tau.sh 0.000 1.000 1.000 1.000 T
./do_transform_tau.sh 0.000 1.000 -1.000 1.000 R

./do_transform_tau.sh 0.000 2.000 0.000 0.500 S
./do_transform_tau.sh 0.500 2.000 1.500 2.000 T
./do_transform_tau.sh 0.500 2.000 -0.500 2.000 R


./do_transform_tau.sh 0.560 3.450 0.560 3.450 STSTST
./do_transform_tau.sh 0.560 3.450 0.560 3.450 TSTSTS
./do_transform_tau.sh 0.560 3.450 0.560 3.450 SRSRSR
./do_transform_tau.sh 0.560 3.450 0.560 3.450 RSRSRS

