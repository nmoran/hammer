#!/bin/bash

##This script runs CFT code and tests that the pobability discribution can use the CFT wnf itself.

set -euf -o pipefail

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_add_cft_scale
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge

N=100

pos=$Catalouge/pos.hdf5
wfn=$Catalouge/wf.hdf5
wfn_scale=$Catalouge/wf_scale.hdf5
wfn_scale2=$Catalouge/wf_scale2.hdf5
wgt=$Catalouge/weight.hdf5

####--------------------------------------------------------------
Ne=4
Kmom=0

cd $Catalouge
###Run the torus code oce (turn of antisymmetrize for speedup)
$dir_CFT/Run_CFT_Wfn -A -D 2 --kappa 3 2 2 3 -Ne $Ne -K $Kmom -N $N -pos $pos -wfn $wfn #> /dev/null

###Run the torus code oce (turn of antisymmetrize for speedup)
$dir_CFT/Run_CFT_Wfn -A -D 2 --kappa 3 2 2 3 -Ne $Ne -K $Kmom -N $N -pos $pos -wfn $wfn_scale -r --use-cft-scale #> /dev/null

###Test that the wave function are note the same but proportinaol
python $MyDir/check_identical_vectors.py $wfn $wfn_scale loglog prop prop
python $MyDir/check_identical_vectors.py $wfn $wfn_scale loglog diff diff

###Cleaning afterwards
rm -r $Catalouge

exit

###FIXME These test should be added at some point

###Run the torus code oce (turn of antisymmetrize for speedup)
$dir_CFT/Run_CFT_Wfn -A -D 2 --kappa 3 2 2 3 -Ne $Ne -K $Kmom -N $N -pos $pos -wfn $wfn_scale2 -wgt $wgt -r --use-cft-scale --cft-sign 0 0 #> /dev/null


###Test that the wave function are note the same but proportinaol
python $MyDir/check_identical_vectors.py $wfn_scale $wfn_scale2 loglog prop prop
python $MyDir/check_identical_vectors.py $wfn_scale $wfn_scale2 loglog diff diff


###Cleaning afterwards
rm -r $Catalouge
