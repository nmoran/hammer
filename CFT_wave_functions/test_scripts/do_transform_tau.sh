#!/bin/bash

##This script runs CFT code, generates a 

set -e
set -u

###MAKEDEP:ModularTransformState.py

tau1=$1
tau2=$2
tau1t=$3
tau2t=$4
TList=$5

##### test for a set of tau that the modular transformation utility can can computed the transformed tau.

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_tau
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge


echo $tau1 > tau.txt
echo $tau2 >> tau.txt


###Perform an S-transform
echo "Perform Modular transform"
ModularTransformState.py --transformation $TList  --tau-in tau.txt --tau-out tauprime.txt 

if [ ! -f tauprime.txt ] ; then
    echo "ERROR: The file tauprime.txt has not been created!">&2
    echo "       For the transformations '$TList'.">&2
    exit -1
fi
    
##Extras 6th column of last (and only) line
cat tauprime.txt
tau1p=`head tauprime.txt -n 1`
tau2p=`head tauprime.txt -n 2 | tail -n 1`
echo tau1:  $tau1
echo tau2:  $tau2
echo tau1p: $tau1p
echo tau2p: $tau2p
echo tau1t: $tau1t
echo tau2t: $tau2t
echo TList: $TList

if [ "$tau1t" != "$tau1p" ] ; then
    echo "Error:">&2
    echo "Error: tau1=$tau1p is not the expected value tau1=$tau1t !" >&2
    echo "       For the transformations '$TList'.">&2
    exit -1
fi

if [ "$tau2t" != "$tau2p" ] ; then
    echo "Error:">&2
    echo "Error: tau2=$tau2p is not the expected value tau2=$tau2t !" >&2
    echo "       For the transformations '$TList'.">&2
    exit -1
fi


###Cleaning afterwards
rm -r $Catalouge
