#!/bin/bash


##This test that the K-sector for the generating Laughlin state need not be set.
##Instead it should be computed if needed.
## NB - we still need to set the K-sector for the real Laughlin state here (but that's not what we are testing).
set -e
set -u

###MAKEDEP:Run_CFT_Wfn

## Changes direcotry ot the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd) ###Should be located in the test_script folder

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
dir_CFT=$MyDir/../../CFT_wave_functions
cd $dir_CFT
dir_CFT=$(pwd)
echo $dir_CFT

Tau_Im=1.0
Nomin=1
Samples=100
Denom=3

#Define cataluge name and create folders
Catalouge=$MyDir/test_no_set_ksector
echo "Files will be stored in $Catalouge"
###We do not clean as any files will be deposited in the same catalouge
mkdir -v -p $Catalouge

for cells in `seq 2 6` ; do 
    Ne=$cells
    Ns=$[ $cells*$Denom ]
    LPow=$Denom

    ### Set K-sector for Laughlin
    if [[ $(($cells%2)) == "1" ]]; then
	Ksector=0;
    else
	Ksector=$(($cells/2));
    fi

    ###Run the chiral_wave_function to check that K-sector need not be set
    cd $dir_CFT
    ./Run_CFT_Wfn -D 1 --kappa $Denom -Ne $cells -N $Samples  -qL $LPow -K $Ksector -wgt $Catalouge/wgt_cft.hdf5 -wfn $Catalouge/wfn_cft.hdf5 -pos $Catalouge/pos.hdf5

    
    ###Run the chiral_wave_function again to check that K-sector=-1 is the same a not specifying it
    ./Run_CFT_Wfn -D 1 --kappa $Denom -Ne $cells -N $Samples  -qL $LPow -K $Ksector -wgt $Catalouge/wgt_cft.hdf5 -wfn $Catalouge/wfn_cft.hdf5 -pos $Catalouge/pos.hdf5 -KL -1
    
done
rm -r $Catalouge



