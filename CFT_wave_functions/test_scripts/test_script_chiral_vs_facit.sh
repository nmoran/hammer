#!/bin/bash

##test that the cft gives the correct values

set -e
set -u

###MAKEDEP:cft_wfn
###MAKEDEP:test_scripts/check_identical_vectors.py


N=1000
c=2

echo '-----------------------'
echo ' Extract the coorinates from the facit database '
echo '-----------------------'

mkdir -p result ###Create the catalogue if it doe's not exist
rm -f result/*
cp -v test_data/2_5_square_xx.rnd result/square_xx.rnd
cp -v test_data/2_5_square_yy.rnd result/square_yy.rnd

echo ' Generating CFT data'
./cft_wfn -D 2 --kappa 3 2 2 3 -c $c -N $N -r # > /dev/null

echo ' Comparing CFT and CFT'
##Load the two vectors, normalize them, align them and compare them
##If the sum(abs(CFT-ED)) > 10^-10 then they are note the same
./test_scripts/check_identical_vectors.sh 'result/chiral_re.rnd' 'result/chiral_im.rnd' 'test_data/2_5_chiral_re.rnd' 'test_data/2_5_chiral_im.rnd' loglog same

###Clean the catalogue afterwards
rm -r result
