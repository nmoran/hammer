#!/bin/bash

##This script runs CFT code with qps: and extracs the 1p correlation functions

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:test_scripts/check_identical_vectors.py

## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_set_random_seed
mkdir -p $Catalouge
cd $Catalouge

N=1000
Ne=5
Kmom=0
q=3

pos_start=pos-here.hdf5

###Run the torus code with random seed
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf.hdf5 -wgt $Catalouge/weight.hdf5

### Run with a random seed (set explictly)
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf_rnd1.hdf5 -wgt $Catalouge/weight.hdf5 --seed -1  ## -1 meanst random seed

### Run with a random seed again (set explictly again)
$dir_CFT/Run_CFT_Wfn -D 1 --kappa $q -Ne $Ne -K $Kmom  -KL $Kmom -N $N -pos $Catalouge/$pos_start -wfn $Catalouge/wf_rnd2.hdf5 -wgt $Catalouge/weight.hdf5 --seed -1 ## -1 meanst random seed


###Test that the two iterations with seed -1 sets calls the random seed 
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_rnd1.hdf5 loglog diff diff

###Test that the two iterations with seed -1 sets calls the random seed 
python $MyDir/check_identical_vectors.py $Catalouge/wf.hdf5 $Catalouge/wf_rnd2.hdf5 loglog diff diff

###Test that the two iterations with seed -1 sets calls the random seed 
python $MyDir/check_identical_vectors.py $Catalouge/wf_rnd1.hdf5 $Catalouge/wf_rnd2.hdf5 loglog diff diff

###Cleaning afterwards
rm -r $Catalouge
