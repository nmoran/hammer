#!/bin/bash

##This script runs CFT code, generates a 

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:ModularTransformState.py
###MAKEDEP:MCImportanceOverlaps.py
###MAKEDEP:test_scripts/do_rotate_coord.sh



## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)
./do_rotate_coord.sh 100 5 0 3 SS
./do_rotate_coord.sh 100 5 0 3 TR
./do_rotate_coord.sh 100 5 0 3 RT
./do_rotate_coord.sh 100 5 0 3 STSTST
./do_rotate_coord.sh 100 5 0 3 TSTSTS
./do_rotate_coord.sh 100 5 0 3 SRSRSR
./do_rotate_coord.sh 100 5 0 3 RSRSRS

./do_rotate_coord.sh 100 6 3 3 SS
./do_rotate_coord.sh 100 6 3 3 TR
./do_rotate_coord.sh 100 6 3 3 RT
./do_rotate_coord.sh 100 6 3 3 STSTST
./do_rotate_coord.sh 100 6 3 3 TSTSTS
./do_rotate_coord.sh 100 6 3 3 SRSRSR
./do_rotate_coord.sh 100 6 3 3 RSRSRS
