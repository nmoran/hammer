#!/bin/bash


set -e
set -u

###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:TransformState.py
###MAKEDEP:Overlap.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_states_ph
#### "Uggly" ways of cleaning catalouge
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge

echo "Testing PH conjugation in transform utility"
TorusDiagonalise.py -p 4 -l 12 -y 2 -c 2 --eigenstate --nbr-eigvals 1 --nbr-states 1 --tau2 2.0 --inversion-sector 0
ConvertBinToASCII.py -mr Torus_p_4_Ns_12_K_2_P_2_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0.vec

TransformState.py -c Torus_p_4_Ns_12_K_2_P_2_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0.vec -o Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0_hole.vec
ConvertBinToASCII.py -mr Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0_hole.vec

TorusDiagonalise.py -p 8 -l 12 -y 4 -c 0 --eigenstate --nbr-eigvals 1 --nbr-states 1 --tau2 2.0 --inversion-sector 0
ConvertBinToASCII.py -mr Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0.vec

echo "Following overlaps should be one."
Ov=$(Overlap.py -a Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0_hole.vec Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0.vec)
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after PH conjugattion is not 1.0 (but $Ov )!"
    exit -1
fi

Ov=$(Overlap.py -a Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0_hole.vec Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0_hole.vec)
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after PH conjugattion is not 1.0 (but $Ov )!"
    exit -1
fi


Ov=$(Overlap.py -a Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0.vec Torus_p_8_Ns_12_K_4_P_0_I_0_tau1_0.000_tau2_2.000_maglen_1.00_LL0Coulomb_State_0.vec)
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after PH conjugattion is not 1.0 (but $Ov )!"
    exit -1
fi


###Cleaning afterwards
rm -r $Catalouge
