#!/bin/bash


##This script first computes the Lalughlin state in real space,
##It then runt the Hammer and computes the real space wave function from thre as well,
##using the electron positions. It then compares the two to see it they give proportional output

set -e
set -u

###MAKEDEP:Run_CFT_Wfn
###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:EvaluateStatesRealSpace.py
###MAKEDEP:FQHFileNames.py
###MAKEDEP:test_scripts/check_identical_vectors.py


## Changes direcotry ot the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd) ###SHould be lovated in the test_script folder

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
dir_CFT=$MyDir/../../CFT_wave_functions
cd $dir_CFT
dir_CFT=$(pwd)
echo $dir_CFT

Samples=10
Tau_Re=0.6
Tau_Im=2.6
q=3
K1=0
K2=0
Ne=5
Ns=$((Ne*q))
	

Catalouge=$MyDir/Test_Hammer_Names
echo "Files will be stored in $Catalouge"
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -v -p $Catalouge
cd $Catalouge

$dir_CFT/Run_CFT_Wfn -D 1 --Kmatrix $q -K $K1 -I $Tau_Im -R $Tau_Re -Ne $Ne -N $Samples -KL $K1 -wgt wgt_cft.hdf5 -wfn wfn_cft.hdf5 -qL $q -pos pos.hdf5 

####The last two are the CoM momentum and the inversion sector
###Use Hammer (assumed to be in path) and Get realspace wave-functions
TorusDiagonalise.py -Ne $Ne -Ns $Ns -K1 $K1 -K2 $K2 -i 0 --tau1 $Tau_Re --tau2 $Tau_Im  --nbr-states 1 --nbr-eigval 1 --interaction Delta --eigenstate 

###Stat that the basis files exist
BasisFile=$(FQHFileNames.py -Ne $Ne -Ns $Ns -K1 $K1 -K2 $K2 -i 0 --type 0)_Basis.basis
if [ -e "$BasisFile" ]
then
    echo File $BasisFile exists!
else
    echo Files present:
    ls
    echo ERROR! File $BasisFile does not exist!
    exit -1
fi
    
echo "DIAGNONALIZED HAMILTONIAN"
###The name of the output vector
GS_NAME=$(FQHFileNames.py -Ne $Ne -Ns $Ns -K1 $K1 -K2 $K2 -i 0 --tau1 $Tau_Re --tau2 $Tau_Im  --interaction Delta --type 2)_State_0.vec
echo GS_NAME $GS_NAME

###Stat that the ground state vector files exists
if [ -e "$GS_NAME" ]
then
    echo File $BasisFile exists!
else
    echo Files present:
    ls
    echo ERROR! File $BasisFile does not exist!
    exit -1
fi



###Evaluate the real space wave-functions 
EvaluateStatesRealSpace.py -v --hdf5-positions pos.hdf5 $GS_NAME --output wfn_exdiag --hdf5-output --hdf5-nometa 
###Clean any other data not used




#### Check that the vectors are proportional
#python $dir_CFT/test_scripts/check_identical_vectors.py wfn_cft.hdf5 wfn_exdiag.hdf5 loglin prop prop --Precision -6


	
###Clean by removing the calouge
rm -r $Catalouge


