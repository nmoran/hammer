#!/bin/bash


set -e
set -u

###MAKEDEP:TorusDiagonalise.py
###MAKEDEP:TransformState.py
###MAKEDEP:Overlap.py


## Changes direcotry t0 the location of the spript
MyName=$(basename $0)
cd $(dirname $0)

MyDir=$(pwd)

## Make sure the file existsd in the path just changd to
[ ! -e  $MyName ] && echo "Program $MyName does not exist in path" && pwd && exit

###Set the directories usefull
cd ../../CFT_wave_functions
dir_CFT=$(pwd)
echo $dir_CFT

Catalouge=$MyDir/Test_transform_states_not_ph
#### "Uggly" ways of cleaning catalouge
mkdir -p $Catalouge
rm -r $Catalouge
mkdir -p $Catalouge
cd $Catalouge

Ne=3
Nec=4
Ns=7
K1=2
K1c=5


State1=$(FQHFileNames.py -p ${Ne} -l ${Ns} -y ${K1} --tau2 2.0  --interaction 3Body --type 2)_State_0.vec
State2=$(FQHFileNames.py -p ${Nec} -l ${Ns} -y ${K1c} --tau2 2.0  --interaction 3Body --type 2)_State_0_ph.vec
State3=$(FQHFileNames.py -p ${Ne} -l ${Ns} -y ${K1} --tau2 2.0  --interaction 3Body --type 2)_State_0_ph_ph.vec
State4=$(FQHFileNames.py -p ${Nec} -l ${Ns} -y ${K1c} --tau2 2.0  --interaction 3Body --type 2)_State_0.vec


echo ".........................................................................."
echo "Testing PH conjugation on state that is not expected to the PH invariant"
echo ".........................................................................."
TorusDiagonalise.py -p ${Ne} -l ${Ns} -y ${K1} --eigenstate --nbr-eigvals 1 --nbr-states 1 --tau2 2.0  --interaction 3Body
ConvertBinToASCII.py -mr $State1 -v

echo ".........................................................................."
echo "  Make PH conjugation"
echo ".........................................................................."
TransformState.py -c $State1  -v --id _ph
ConvertBinToASCII.py -mr $State2 -v

echo ".........................................................................."
echo "  Make PH conjugation back"
echo ".........................................................................."
TransformState.py -c $State2  -v --id _ph
ConvertBinToASCII.py -mr $State3 -v

echo ".........................................................................."
echo " Make the PH conjugated state by diagonalization"
echo ".........................................................................."

TorusDiagonalise.py -p ${Nec} -l ${Ns} -y ${K1c} --eigenstate --nbr-eigvals 1 --nbr-states 1 --tau2 2.0  --interaction 3Body

ConvertBinToASCII.py -mr $State4 -v


echo "This overlap should be one."
Ov=$(Overlap.py -a $State1 $State3)
if [ "$Ov" != "1.0" ]; then
    echo "ERROR: Overlap after two PH conjugates is not 1.0 (but $Ov )!"
    exit -1
fi


echo "This overlap should not be one."
Ov=$(Overlap.py -a $State2 $State4)
if [ "$Ov" == "1.0" ]; then
    echo "ERROR: Overlap between PH conjugate is 1.0 when it should not be!"
    exit -1
else
    OvTarget=0.197491892
    Ov=${Ov:0:11} ##Drop last digits..
    if [ "$Ov" == ${OvTarget:0:11} ]; then
	echo "GOOD: Overlap after two PH conjugates is not 1.0 (but $Ov )!"
    else
	echo "ERROR: Overlap between PH conjugate is is not 1.0 ( but $Ov ) but also not the expected $OvTarget"
	exit -1
    fi
fi

###Cleaning afterwards
rm -r $Catalouge
