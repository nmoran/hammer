###This dependence should be added to all o-files and programs, to they are rebuilt properly.
MAKEFILES=$(MAKEFILE) $(SPECIFICS) includes.mk Programs.mk tests.mk tests.mk clean.mk graph.mk build_f90.mk make_torus.mk deps.mk

include Programs.mk ##The programs and objects: NB inlcude this first

include $(SPECIFICS)

include deps.mk  ##Include the file that build the dependecies for the programs
include $(DEPFILE) ###Include the file that conains the dependencies for the programs
include tests.mk  ##Rules for tests
include clean.mk  ##Rules for cleaning
include graph.mk  ##Rules for making dependecy-graphs
include debug.mk  ##Rules for making profiling
include build_f90.mk  ##Rules for making f95 .o files
include dependencies ##If the file does not exist.. make will create is using the rules bellow

### C code - production
include make_torus.mk
