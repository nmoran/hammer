module center_of_mass

  use typedef
  use jacobitheta
  use k_matrix
  use channels
  use dedekind_eta_function
  implicit none

  logical :: verbose_CM=.FALSE.
  !!These integers control the periodic or antiperiodic pbc
  !!They start out as H=T=0 but need to be set otherwise depending on the number of particles
  integer :: CoM_D=0,CoM_kdenom=1,CoM_CoMindx=0,CoM_delta=1,CoM_q_nu=0
  logical :: BC_SET=.FALSE.,Is_Hierarchy=.FALSE.,Is_Chiral=.FALSE.,Is_Balanced=.FALSE.
  logical :: cft_com_verbose=.FALSE.

  real(KIND=dp) :: CoM_sval
  real(KIND=dp), allocatable, dimension(:) :: Hlist,Tlist,CoM_H0
  integer, allocatable, dimension(:,:) :: CoM_Kmat,CoM_kappa_bar,scaled_kminus
  real(Kind=dp), allocatable, dimension(:,:) :: scaled_kplus
  
  private
  public Center_of_mass_function
  public verbose_CM
  public set_CM_BC
  public force_raw_computation
  public set_trivial_CM_BC
  public unset_CM_BC
  public add_exponentially
  public diag_matrix_entry
  public set_cft_com_verbose
  
  
contains
  !!Use this to sett the verbosenes-level
  subroutine set_cft_com_verbose(Verbose_ON)
    logical, intent(IN) :: Verbose_ON
    cft_com_verbose=Verbose_ON
    write(*,*) '-------------------------------'
    write(*,*) 'Setting verboseness to:',cft_com_verbose
  end subroutine set_cft_com_verbose

  subroutine set_trivial_CM_BC(D,Kmat,kappa_bar,dual_index,Nq,hout,tout,k_denom,sval)
    integer, intent(IN) :: D !The size of the kappa-matrix
    integer, intent(IN) :: Kmat(D,D) !The kappa-matrix
    integer, intent(in),optional :: kappa_bar(D,D)
    integer, intent(in),optional :: dual_index(D) !!*2 of the true index, to preserve integerness
    integer, intent(in),optional :: Nq(D) !Number of qps (changes effecive flux)
    integer, intent(in),optional :: k_denom !Denominaor of k-matrices
    real(Kind=dp), intent(out), optional :: hout(D),tout(D) !Number of qps (changes effecive flux)
    real(Kind=dp), intent(in), optional :: sval !Papameter dictating how wave-fns are glued together.
    
    call set_CM_BC(0,0,D,Kmat,kappa_bar,dual_index,Nq,hout,tout,k_denom,sval,trivial_BC=.TRUE.)

  end subroutine set_trivial_CM_BC
  
  subroutine set_matrix_stats(D)
    integer, intent(In) :: D
    Is_Hierarchy=is_hierarchy_k_matrix(D,CoM_Kmat)
    Is_Chiral=is_matrix_zero(CoM_kappa_bar,D)

    !!Check if the diagonal elements are the same
    !!in that case the charge lattice is rectangular with a basis.
    Is_Balanced=.FALSE. !!!Initial setting
    if(D.eq.2)then !!By the diagonal here we mean the diagonal of the Kplus element.
       if((CoM_Kmat(1,1)+CoM_kappa_bar(1,1)).eq.(CoM_Kmat(2,2)+CoM_kappa_bar(2,2)))then
          Is_Balanced=.TRUE.
       end if
    end if

  end subroutine set_matrix_stats

  subroutine force_raw_computation
    !!Force the evaluation of CoM to be brute force
    !!Mainly usefull to test that more elaborate calculation agrees with naive one
    
    !!Reset this option by calling set_CM_BC again
    Is_Hierarchy=.FALSE.
  end subroutine force_raw_computation
  
  subroutine get_h_t_list(Ns,K_sector,Kmat_in, Hlist_out, Tlist_out) bind(C)
    USE ISO_C_BINDING
    !! NB Use as a entypoint for c-code. Used to extract the H,T values for the laughlin CoM.
    integer(kind=c_int), intent(IN) :: Ns !Number of fluxes
    integer(kind=c_int), intent(IN) :: K_sector !The desired K_sector (integer if periodic)
    integer(kind=c_int), intent(IN) :: Kmat_in !The desired kappa_matrix value
    REAL(KIND=c_double), DIMENSION(1:1), INTENT(OUT) :: Hlist_out, Tlist_out
    integer :: Kmat(1,1) !!Only works for loaughlin type, so D=1 implicitly
    Kmat(1,1) = Kmat_in
    call set_CM_BC(Ns,K_sector,1,Kmat,hout=Hlist_out,tout=Tlist_out)
  end subroutine get_h_t_list

  subroutine set_CM_BC(Ns_in,K_sector,D,Kmat,kappa_bar,dual_index,Nq,hout,tout,k_denom,sval,trivial_BC,CoMindx)
    !! Sets the boundary conditions such that the wave-function is periodic,
    !! and in a given K_sector.

    integer, intent(IN) :: Ns_in !Number of fluxes
    integer, intent(IN) :: K_sector !The desired K_sector (integer if periodic)
    integer, intent(IN) :: D !The size of the kappa-matrix
    integer, intent(IN) :: Kmat(D,D) !The kappa-matrix
    integer, intent(in),optional :: kappa_bar(D,D)
    integer, intent(in),optional :: dual_index(D) !!*2 of the true index, to preserve integerness
    integer, intent(in),optional :: Nq(D) !Number of qps (changes effecive flux)
    integer, intent(in),optional :: k_denom !Denominaor of k-matrices
    integer, intent(in),optional :: CoMindx !Sets the eigenvalue of T_2^q
    real(Kind=dp), intent(out), optional :: hout(D),tout(D) !Number of qps (changes effecive flux)
    real(Kind=dp), intent(in), optional :: sval !Papameter dictating how wave-fns are glued together.
    logical, intent(IN), optional :: trivial_BC
    integer :: q_inv,q_cell,p,plist(D),Kminus_inv(D,D) !The inverse kappa-matrix, and accociated denominator
    integer :: H_parameter,T_parameter
    integer :: indx_a,Ns,p_nu
    integer :: shift !!The shift needed to set a certain K_sector
    integer :: raw_K_sector !!The K-sector that will happen if noting is done
    integer :: K_diff,cells,Ne,Input_K_sector
    logical :: found_shift,CoM_TBC

    !write(*,*) '-----------------------------------'
    !write(*,*) 'Set boundary conditions for CFT wfn'
    !write(*,*) '-----------------------------------'
    
    !!NB/FIXME: Adding localized qps breaks the translational invariance
    !! Thus K-sector is not a good meassure. Instead we compute the K-sector that we would have had if there where no qps present.
    !!FIXME: If the number of qps, in the different groups are not the same, this will be a problem.

    if(present(Nq))then
       Ns=Ns_in-Nq(1)
    else
       Ns=Ns_in
    end if
    
    if(present(k_denom))then
       CoM_kdenom=k_denom
    else
       CoM_kdenom=1
    end if

    if(present(sval))then
       CoM_sval=sval
    else
       CoM_sval=0.d0
    end if

    if(present(trivial_bc))then
       CoM_TBC=trivial_bc
    else
       CoM_TBC=.FALSE.
    end if

    if(present(CoMindx))then
       CoM_CoMindx=CoMindx
    else
       CoM_CoMindx=0
    end if


    if(CoM_TBC)then
       Input_K_sector=0
    else
       !!Set the K-sector to work with
       Input_K_sector=modulo(K_sector,Ns)
    end if
    
    if(BC_SET)then !!If it was earlier set,unset it now
       call unset_CM_BC
    end if
    !write(*,*) 'allocate matrices'
    allocate(CoM_Kmat(D,D),CoM_kappa_bar(D,D),scaled_kminus(D,D),scaled_kplus(D,D))
    allocate(Hlist(D),Tlist(D),CoM_H0(D))
    
    CoM_D=D
    CoM_Kmat=Kmat
    if(present(kappa_bar))then
       CoM_kappa_bar=kappa_bar  !!In the Non-chiral case
    else
       CoM_kappa_bar=0  !!In the chiral case
    end if
    Scaled_kminus = CoM_Kmat - CoM_kappa_bar!!Contruct k_minus
    Scaled_kplus = CoM_Kmat + CoM_kappa_bar!!Contruct k_plus
    call check_k_minus_is_integer(scaled_Kminus,CoM_kdenom,D)
    Scaled_kminus=Scaled_kminus/CoM_kdenom!!divide by scale
    !write(*,*) 'scaled k_plus before  division',Scaled_kplus
    Scaled_kplus=Scaled_kplus/CoM_kdenom!!divide by scale
    !write(*,*) 'scaled k_plus after  division',Scaled_kplus

    call set_matrix_stats(D)
    
    !write(*,*) 'Set the BC:s with',Ns,' fluxes and Input_K_sector', Input_K_sector


    call kappa_matrix_inverse(D,scaled_Kminus,Kminus_inv,q_inv)
    !write(*,*) 'q_inv:',q_inv
    call  kappa_matrix_group_sizes(D,scaled_Kminus,plist,p,q_cell)
    call  Filling_fraction(D,plist,q_cell,p_nu,CoM_q_nu)
    !write(*,*) 'q_cell:',q_cell
    cells = Ns/q_cell
    Ne = cells * p
    CoM_delta=q_cell/CoM_q_nu
    CoM_H0=real(matmul(Kminus_inv,CoM_H0*0+1),dp)/q_inv !!Sets CoM using the dimention of H0
    !!write(*,*) 'CoM_H0:',CoM_H0


    if(CoM_TBC)then
       Tlist=0d0
       Hlist=0d0
    else
       
       if(present(dual_index))then
          !write(*,*) 'Specific dual vectors have been set, construct K-sector from them'
          !write(*,*) 'Input dual_indx*2:',dual_index
          !!Test that the dual_index corresponts to correct bc:s
          do indx_a=1,D
             !!Parity test
             if(modulo(Ns+scaled_Kminus(indx_a,indx_a)-dual_index(indx_a),2).ne.0)then
                write(*,*) '........sbc.........'
                write(*,*) '        ERROR:'
                write(*,*) 'Input dual_indx*2:',dual_index
                write(*,*) 'does not have correct parity for periodic bc:s.'
                write(*,*) 'When there are Ns=',Ns,'fluxes and Kmarix:'
                call print_int_matrix(scaled_Kminus)
                write(*,*) 'the parity should be:',&
                     modulo(Ns+diag_matrix_entry(D,scaled_Kminus),2)
                call exit(-2)
             end if
          end do
          
          !!The K-sector mapped put by the dual-index is 
          !! K = sum_a * l_a * N_a
          raw_K_sector = dot_product(dual_index,plist)*cells
          !write(*,*) 'Targeted_K-sector*2 is;',raw_K_sector
          if(modulo(raw_K_sector,2).eq.0)then
             raw_K_sector=modulo(raw_K_sector/2,Ns)
          else
             write(*,*) '----Error:----'
             write(*,*) 'Raw_K_sector is not an integer.'
             write(stderr,*) 'Boundary conditions can not be periodic!'
             call exit(-2)
          end if
          !write(*,*) 'Targeted_K-sector is;',raw_K_sector
          !write(*,*) 'Input-Ksector is;',Input_K_sector
          if(raw_K_sector.ne.Input_K_sector)then
             write(stderr,*) '----scm----'
             write(stderr,*) 'Error:'
             write(stderr,*) 'Input K_sector',Input_K_sector,&
                  'does not match target K_sector',raw_K_sector
             write(*,*) 'There are Ns=',Ns,'fluxes and Kmarix:'
             call print_int_matrix(scaled_Kminus)          
             call exit(-1)
          end if
          !write(*,*) 'Correct K-sector. Now need to compute H-t-vectors'
          Hlist=real(matmul(Kminus_inv,dual_index),dp)/(2*q_inv)
          Tlist=Hlist
          !write(*,*) 'Hlist:',Hlist
          !write(*,*) 'Tlist:',Tlist
       else
          !write(*,*) 'No dual index hs been set, assume abelian and deduce correct choice.'
          
          raw_K_Sector=modulo(compute_raw_K_sector(D,Ne,Ns,scaled_Kminus,plist,q_cell),Ns)
          !write(*,*) 'Set the raw K_sector is', raw_K_sector
          
          !!By shifting the H/T parameter the K_sector gets shifted by Ne*shift
          !! Thus re require that shift =  (Input_K_sector - raw_K_sector)/Ne
          !!which has to be an integer
          K_diff = mod(Input_K_sector-raw_K_sector,Ns)
          
          !write(*,*) 'The desired shift is',K_diff
          
          !!Compute the corret shift to use
          found_shift=.FALSE.
          do shift=0,q_cell !!There is only q-fold degeneracy
             if(mod(Ne*shift-K_diff,Ns).eq.0)then
                !!Found correct shift
                found_shift=.TRUE.
                exit
             end if
          end do
          if(.not.found_shift)then
             write(stderr,*) '----scbc-----'
             write(stderr,*) 'ERROR: Wrong Boundary conditions for CFT'
             write(stderr,*) 'The chosen K-sector=',Input_K_sector,'is not avaliable for kappa-matrix'
             call print_int_matrix(scaled_Kminus)
             write(stderr,*) 'Sectors has to be multiples of ',Ne,'offset from',raw_K_sector
             write(stderr,*) 'NB: This is only true for abelian states'
             write(stderr,*) '    If the state is non-abelain the relation is different'
             write(stderr,*) '    Use the dual_index option to specify K-sector.'
             write(stderr,*) '    Use the program k_matrix_info to get more info.'
             call exit(-2)
          end if
          
          !write(*,*) 'The shift is',shift
          
          H_parameter=Ns+2*shift
          T_parameter=Ns+2*shift
          
          !!NB/FIXME: Here we put back the contribution from the quasi-particles.
          !!This will cause the h/h to be slightly shifted from the raw expected
          !!It will however, make sure that we quasi,paricle wave functions are periodic
          if(present(Nq))then
             H_parameter=H_parameter+Nq(1)
             T_parameter=T_parameter+Nq(1)
          end if
          !!write(*,*) 'set H,T',H_parameter,T_parameter

          
          !!We use the relation Z=sum q_a * z_a
          !!  ---||----         h=sum q_a * h_a
          !!  ---||----         t=sum q_a * t_a
          !!The boundary condition (for periodic bcs) that 
          !!  N_phi + K_aa = 2* h * q_a   (mod 2)
          !!  N_phi + K_aa = 2* t * q_a   (mod 2)
          !!
          !! For simple hierarchy states this is solved by
          !!  h = sum \tilde h_a l_a 
          !!  t = sum \tilde t_a l_a where l_a * q_b = delta_a,b
          !! and \tilde h_a=\tilde t_a = ( N_phi + K_aa ) / 2  (mod 1)
          !!
          !! We wish to expand this in the non-ortogonal basis given by the kappa_matrix.
          !! This leads to the equation
          !!     \tilde h_a = sum_b h_b K_ab
          !!     \tilde t_a = sum_b t_b K_ab
          !! wich is solved by
          !!     h_a = sum_b \tilde h_b K_ab**(-1)
          !!         = sum_b K_ab**(-1)(N_phi + K_bb)/2
          
          !!Compute the H & T lists from the H and T parameters
          !write(*,*) 'H & T parameters are',H_parameter,T_parameter
          !write(*,*) 'The kappa-matrix is:'
          !call print_int_matrix(Kminus)
          !write(*,*) 'The Inv-kappa-matrix is:'
          !call print_int_matrix(Kminus_inv)
          !write(*,*) 'Divided by',q_inv
          
          Hlist=real(matmul(Kminus_inv,H_parameter+diag_matrix_entry(D,scaled_Kminus)),dp)/(2*q_inv)
          Tlist=real(matmul(Kminus_inv,T_parameter+diag_matrix_entry(D,scaled_Kminus)),dp)/(2*q_inv)
       end if
    end if
    

    call set_s_and_trim_HT(D,sval)
    BC_SET=.TRUE.
    !!Read h and t to output variables if present
    if(present(hout))then
       hout=Hlist
    end if
    if(present(tout))then
       tout=Tlist
    end if
  end subroutine set_CM_BC

  
  subroutine set_s_and_trim_HT(D,sval)
    integer, intent(in) :: D
    real(Kind=dp), intent(in), optional :: sval !Parameter dictating how wave-fns are glued together.
    if(present(sval))then
       if(sval.ne.0.d0)then
          if(D.eq.1)then
             Hlist=Hlist-sval !!remove the svalue from the Hlist
          else
             write(stderr,*) 'ERROR: svalue=',sval,' not implemented for BC with D>1'
             call exit(-1)
          end if
       end if
    end if
    !!Redicing Hlist and Tlist to be as close of 0 as possible
    Hlist=Hlist-nint(Hlist)
    Tlist=Tlist-nint(Tlist)
    !write(*,*) 'Set: Hlist',Hlist
    !write(*,*) 'Set: Tlist',Tlist
  end subroutine set_s_and_trim_HT
  

  function diag_matrix_entry(Size,Matrix)
    integer, intent(IN) :: Size
    integer :: diag_matrix_entry(Size)
    integer, intent(IN) :: Matrix(Size,Size)
    integer :: n
    do n=1,Size
       diag_matrix_entry(n)=Matrix(n,n)
    end do
  end function diag_matrix_entry

  subroutine unset_CM_BC()
    !!Reset the CM_BC
    CoM_D=0
    Is_Hierarchy=.FALSE.
    Is_Chiral=.FALSE.
    Is_Balanced=.FALSE.
    if(BC_SET)then !!If it was earlier set,unset it now
       !!write(*,*) 'Dealocate Matrix and Hlist'
       deallocate(Hlist,Tlist,Com_H0)
       deallocate(CoM_Kmat,CoM_kappa_bar,scaled_kminus,scaled_kplus)
    end if
    !write(*,*) 'Unset the BC:s'
    BC_SET=.FALSE.
  end subroutine unset_CM_BC

  complex(KIND=dpc) function Center_of_Mass_Function(D,Kmat,kappa_bar,Xlist,Ylist,tau,k_denom) result(weight)
    integer, intent(IN) :: D,Kmat(D,D),kappa_bar(D,D)
    integer, optional, intent(IN) :: k_denom
    real(KIND=dp), intent(IN) :: Xlist(D),Ylist(D)
    complex(KIND=dpc), intent(IN) :: tau
    if(.not.BC_SET)then
       write(*,*) '---CoMf----'
       write(*,*) 'ERROR: Chiral BC:s have not been set'
       write(*,*) 'First call: center_of_mass_MOD_set_CM_BC'
       write(*,*) 'stopping....'
       call exit(-3)
    end if
    if(CoM_D.ne.D)then
       write(*,*) '---CoMf----'
       write(*,*) 'ERROR: Wrong size on D.'
       write(*,*) 'Expecting D=',CoM_D,'but got D=',D
       write(*,*) 'Call center_of_mass_MOD_set_CM_BC to reset'
       write(*,*) 'stopping....'
       call exit(-3)
    end if
    if(.not.is_matrices_equal(CoM_D,CoM_Kmat,Kmat))then
       write(*,*) '---CoMf----'
       write(*,*) 'ERROR: Wrong input kappa-matrix'
       write(*,*) 'Expecting kappa_matrix:'
       call print_int_matrix(CoM_Kmat)
       write(*,*) 'But got kappa_matrix:'
       call print_int_matrix(Kmat)
       write(*,*) 'Call center_of_mass_MOD_set_CM_BC to reset'
       write(*,*) 'stopping....'
       call exit(-3)
    end if
    if(.not.is_matrices_equal(CoM_D,CoM_kappa_bar,kappa_bar))then
       write(*,*) '---CoMf----'
       write(*,*) 'ERROR: Wrong input K-bar-matrix'
       write(*,*) 'Expecting K-bar-matrix:'
       call print_int_matrix(CoM_kappa_bar)
       write(*,*) 'But got K-bar-matrix:'
       call print_int_matrix(kappa_bar)
       write(*,*) 'Call center_of_mass_MOD_set_CM_BC to reset'
       write(*,*) 'stopping....'
       call exit(-3)
    end if
    if(present(k_denom))then
       if(CoM_kdenom.ne.k_denom)then
          write(*,*) '---CoMf----'
          write(*,*) 'ERROR: Wrong denominator  for k-matrix.'
          write(*,*) 'Expecting k_denom=',CoM_kdenom,'but got k_denom=',k_denom
          write(*,*) 'Call center_of_mass_MOD_set_CM_BC to reset'
          write(*,*) 'stopping....'
          call exit(-3)
       end if
    else
       if(CoM_kdenom.ne.1)then
          write(*,*) '---CoMf----'
          write(*,*) 'ERROR: Wrong denominator  for k-matrix.'
          write(*,*) 'Expecting k_denom=',CoM_kdenom,'but got k_denom=',1
          write(*,*) 'Call center_of_mass_MOD_set_CM_BC to reset'
          write(*,*) 'stopping....'
          call exit(-3)
       end if
    end if
    
    !write(*,*) 'Entering CoM, X:',Xlist
    !write(*,*) 'Entering CoM, Y:',Ylist
    
    !!Adding the eta-normalization to the wave_function with correct bc:s
    weight=add_bc_make_sum_index(Xlist,Ylist,tau)-D*log_dedekind_eta(tau)
  end function Center_of_Mass_Function
  


  complex(KIND=dpc) function add_bc_make_sum_index(Xlist,Ylist,tau) result(weight)
    real(KIND=dp), intent(IN) :: Xlist(COM_D),Ylist(CoM_D)
    complex(KIND=dpc), intent(IN) ::tau
    complex(KIND=dpc) ::tmp_wgt,tmp_phase1,tmp_phase2
    integer :: k
    !!Here we add differnt CoM functions such as to be an eigenstate of T2^q
    !!This is not guaranteed if the state is non-abelian


    !!Call the piece that computes the CoM
    weight=add_bc_ht_make_sum_index(Xlist,Ylist,tau,Hlist,Tlist)
    if(CoM_delta.gt.1)then
       !write(*,*) 'Hlist:',Hlist
       !write(*,*) 'Tlist:',Tlist
       !write(*,*) 'k,tmp:',0,mod_2pi(weight)
       do k=1,(CoM_delta-1)
          tmp_wgt=add_bc_ht_make_sum_index(Xlist,Ylist,tau,Hlist+k*CoM_q_nu*CoM_h0,Tlist)
          tmp_phase1=iunit*2*pi*k*CoM_q_nu*dot_product(CoM_h0,matmul(scaled_Kminus,Tlist))
          tmp_phase2=-iunit*2*pi*k*CoM_CoMindx/real(CoM_delta,dp)
          !write(*,*) 'k,tmp:',k,mod_2pi(tmp_wgt)
          !write(*,*) '  phase1:',tmp_phase1
          !write(*,*) '  phase2:',tmp_phase2
          weight=add_exponentially(weight,tmp_wgt+tmp_phase1+tmp_phase2)
       end do
    end if
  end function add_bc_make_sum_index

  
  
  complex(KIND=dpc) function add_bc_ht_make_sum_index(Xlist,Ylist,tau,h_list,t_list) result(weight)
    real(KIND=dp), intent(IN) :: Xlist(COM_D),Ylist(CoM_D),h_list(CoM_D),t_list(CoM_D)
    complex(KIND=dpc), intent(IN) ::tau
    real(KIND=dp) :: X2list(COM_D),Y2list(CoM_D)
    complex(KIND=dpc) :: extra_phase,tilde_tau(CoM_D,CoM_D),psi_sval
    !!In this step we use the relation that
    !!F_h,t(Z) = F_0_0(Z+t+tau*h)*exp(i*pi*tau*h**2)*exp(i*2*pi*Z*h*)
    !!
    
    !write(*,*) 'Adding the boudnary condition vectors. These are:'
    !write(*,*) 'T_list:',T_list
    !write(*,*) 'H_list:',H_list

    !!Computing the extra phase from the s-vale
    psi_sval=compute_sval_phase(Xlist,Ylist,tau)
    
    tilde_tau=(tau*CoM_Kmat-dconjg(tau)*CoM_kappa_bar)/CoM_kdenom
       
    !!Adding the boundary condition vectors
    X2list=Xlist+T_list
    Y2list=Ylist+H_list       

    !!FIXME: Procedure of adding a nonzero s-value as a X_com_shift will only work for D=1 states.
    X2list=X2list+com_sval*real(tau,dp)
    !write(*,*) 's-value:',com_sval

    
    extra_phase=pi*iunit*dot_product(H_list,matmul(tilde_tau,H_list))&
         +2*pi*iunit*dot_product(H_list,matmul(tilde_tau,Ylist))&
         +2*pi*iunit*dot_product(H_list,matmul(scaled_Kminus,Xlist))
    
    
    !!Call the piece that computes the CoM
    weight=coordinates_shifts(X2list,Y2list,tau)+extra_phase+psi_sval
    
  end function add_bc_ht_make_sum_index

  complex(kind=dpc) function compute_sval_phase(X_com,Y_com,tau) result(phase)
    real(kind=dp), intent(in) :: X_com(com_d),Y_com(com_d)
    complex(kind=dpc), intent(in) :: tau
    !!Throw error before initialization if something is wrong....
    if(com_d.ne.1)then !!only do if D=1
       if(com_sval.ne.0.d0)then !!only do if sval!=1
          write(stderr,*) 'ERROR: sval=',com_sval,'.ne.0 not implemented for D>0'
          write(stderr,*) 'ERROR: please implement this'
          call exit(-1)
       end if
    end if
    !!Initialize to the phase used for Laughlin
    phase=iunit*2*pi*scaled_kminus(1,1)*com_sval*(X_com(1)+real(tau,dp)*Y_com(1))
  end function compute_sval_phase
 


  complex(KIND=dpc) function coordinates_shifts(Xlist,Ylist,tau) result(weight)
    real(KIND=dp), intent(IN) :: Xlist(COM_D),Ylist(CoM_D)
    complex(KIND=dpc), intent(IN) :: tau
    real(KIND=dp) :: X2list(COM_D),Y2list(COM_D)
    integer :: Xshift(COM_D), Yshift(COM_D)
    complex(KIND=dpc) :: Extra_phase
    complex(KIND=dpc) :: tilde_tau(COM_D,COM_D)
    real(KIND=dp) :: reslinearX
    complex(KIND=dpc) :: gauss_res,reslinearY
    
    !write(*,*) 'The CoM coordinates are:'
    !write(*,*) 'Xlist:',Xlist
    !write(*,*) 'Ylist:',Ylist
    Xshift=nint(Xlist)
    Yshift=nint(Ylist)
    !write(*,*) 'The possible shifts are:'
    !write(*,*) 'Xshift:',Xshift
    !write(*,*) 'Yshift:',Yshift
    X2list=Xlist-XShift
    Y2list=Ylist-YShift
    !write(*,*) 'Now the CoM coordinates are:'
    !write(*,*) 'X2list:',X2list
    !write(*,*) 'Y2list:',Y2list
       
    !!Here we shift the Ylist
    !!The extra pahse is: e^(-2i\pi\sum_a\tilde{q_a}\delta_a*\tilde{Z})
    !!  *      e^(-i\pi\tilde{tau}(\sum_a\tilde{q_a}\delta_a)^2)
    !!WE have \tilde{tau*q_a*q_b}=tau*K-\bar{tau*K}=tau1*K^-+i*tau2*K^+
    
    !!FIXME - this could be computed at higher level    
    tilde_tau=(tau*CoM_Kmat-dconjg(tau)*CoM_kappa_bar)/CoM_kdenom
    !write(*,*) 'Tilde tau:'
    !call print_cmplx_matrix(CoM_D,tilde_tau)
    
    
    !!Compute matrix product for the quadratic term
    gauss_res=dot_product(Yshift,matmul(tilde_tau,Yshift))
    !Write(*,*) 'gauss_res:',gauss_res    
    !!Compute matrix product for the linear term
    reslinearX=dot_product(Yshift,matmul(scaled_kminus,X2list))
    reslinearY=dot_product(Yshift,matmul(tilde_tau,Y2list))
    !Write(*,*) 'res lin X:',reslinearX
    !Write(*,*) 'res lin Y:',reslinearY
    
    
    Extra_Phase=-iunit*pi*(gauss_res+2*(reslinearX+reslinearY))
    !write(*,*) 'Extra_Phase:',Extra_Phase
     
    weight=compute_cm(X2list,Y2list,tau)+Extra_phase
    !!write(*,*) 'Retunred weight2:',weight
         
  end function coordinates_shifts
  
  
  complex(kind=dpc) function compute_cm(Xlist,Ylist,tau) result(weight)
    real(KIND=dp), intent(IN) :: Xlist(COM_D),Ylist(CoM_D)
    complex(KIND=dpc), intent(IN) :: tau

    !write(*,*) 'Xlist',Xlist
    !write(*,*) 'Ylist',Ylist
    !write(*,*) 'Is the kappa-matrix chiral?',Is_Chiral
    !write(*,*) 'Is the kappa-matrix hierarchy?',Is_hierarchy
    !write(*,*) 'Is the kappa-matrix balanced?',Is_Balanced
    !Switch to the computation method appropriate
    if(is_chiral)then
       if(is_balanced)then
          !write(*,*) 'Is the kappa-matrix balanced?',Is_Balanced
          weight=make_balanced_cm(Xlist,Ylist,tau)
       else
          if(Is_hierarchy)then
             weight=make_hierarchy_CM(Xlist+tau*Ylist,tau)
          else
             weight=compute_nc_cm(Xlist,Ylist,tau,dconjg(tau))
          end if
       end if
    else
       if(is_balanced)then
          !write(*,*) 'Is the kappa-matrix balanced?',Is_Balanced
          weight=make_balanced_cm(Xlist,Ylist,tau)
       else
          weight=compute_nc_cm(Xlist,Ylist,tau,dconjg(tau))
       end if
    end if
    
  end function compute_cm

  !!---------------------------------------
  !!         IF The state is Balanced
  !!-------------------------------------
  

  complex(kind=dpc)  function make_balanced_cm(Xlist,Ylist,tau) result(weight)
    real(KIND=dp), intent(IN) :: Xlist(CoM_D),Ylist(CoM_D)
    complex(KIND=dpc), intent(IN) :: tau
    integer :: delta
    real(KIND=dp) :: tau1
    complex(KIND=dpc) :: itau2
    real(KIND=dp) :: Kplus_plus,Kplus_min,Kmin_plus,Kmin_min
    complex(KIND=dp) :: Zplus,Zmin,tauplus,taumin,weight0
    if(com_D.ne.2)then
       write(stderr,*) 'ERROR: Balanced CoM not implemented for D!=2.'
       write(stderr,*) '       Choose another path to evaluate this'
       call exit(-1)
    end if

    tau1=real(tau,dp)
    itau2=iunit*aimag(tau)
    !write(*,*) 'Computing stats:'
    Kplus_plus=scaled_kplus(1,1)+scaled_kplus(1,2)
    Kplus_min=scaled_kplus(1,1)-scaled_kplus(1,2)
    Kmin_plus=scaled_kminus(1,1)+scaled_kminus(1,2)
    Kmin_min=scaled_kminus(1,1)-scaled_kminus(1,2)
    taumin =tau1*2*Kmin_min  +itau2*2*Kplus_min
    tauplus=tau1*2*Kmin_plus +itau2*2*Kplus_plus
    !write(*,*) 'tauplus,taumin'
    !write(*,*) tauplus,taumin

    Zplus=Kmin_plus*(Xlist(1)+Xlist(2))+(Ylist(1)+Ylist(2))*tau1+&
         Kplus_plus*(Ylist(1)+Ylist(2))*itau2
    Zmin=Kmin_min*(Xlist(1)-Xlist(2))+(Ylist(1)-Ylist(2))*tau1+&
         Kplus_min*(Ylist(1)-Ylist(2))*itau2

    !!In this simplest case there is only two terms that need to be summed
    delta=0
    weight=logthetagen(0.d0,0.d0,Zplus,tauplus)+&
         logthetagen(0.d0,0.d0,Zmin,taumin)
    delta=1
    weight0=logthetagen(delta*0.5d0,0.d0,Zplus,tauplus)+&
         logthetagen(delta*0.5d0,0.d0,Zmin,taumin)
    weight=add_exponentially(weight,weight0)
    

  end function make_balanced_cm


  !!---------------------------------------
  !!         IF The state is Non-chiral
  !!-------------------------------------

  
  complex(kind=dpc) function compute_nc_cm(Xlist,Ylist,tau,taubar)  result(weight)
    !!Computes the Non-chiral CoM
    !!Currently this function ignores the difference between chiral and-anti-chiral wave-functions
    real(KIND=dp), intent(IN) :: Xlist(CoM_D),Ylist(CoM_D)
    complex(KIND=dpc), intent(IN) :: tau,taubar
    complex(KIND=dpc) :: Zlist(CoM_D),Zbarlist(CoM_D)
    complex(KIND=dpc) :: combined_tau,combined_Z
    
    if(CoM_D.eq.1)then
       !!arguments
       Zlist=Xlist+tau*Ylist
       Zbarlist=Xlist+taubar*Ylist
       combined_tau=(CoM_Kmat(1,1)*tau - CoM_kappa_bar(1,1)*taubar)/CoM_Kdenom
       combined_Z=(CoM_Kmat(1,1)*Zlist(1) - CoM_kappa_bar(1,1)*Zbarlist(1))/CoM_kdenom
       !!weight
       weight=logthetagen(0.d0,0.d0,combined_Z,combined_tau)
       !if(cft_com_verbose)then
       !   write(*,*) 'Kmat,kappa_bar',Com_Kmat(1,1),CoM_kappa_bar(1,1)
       !   write(*,*) 'tau:   ',tau
       !   write(*,*) 'taubar:',taubar
       !   write(*,*) 'Zlist:',Zlist
       !   write(*,*) 'tot_tau',combined_tau
       !   write(*,*) 'tot_Z',combined_Z
       !   write(*,*) 'com weight:',weight
       !   write(*,*) '-------------------------------------'
       !end if
    elseif(CoM_D.ge.2)then
       !!write(*,*) 'Compute nc_cm_piece:'
       !!verbose_CM=.TRUE.
       weight=compute_nc_cm_dn(Xlist,Ylist,tau)
       !!write(*,*) 'Returned weight',weight
    else 
       write(*,*) 'ERROR: Something is wrigong for nc_cm'
       call exit(-1)
    end if
  end function compute_nc_cm

complex(kind=dpc) function compute_nc_cm_dn(Xlist,Ylist,tau)  result(weight)
    !!Computes the Non-chiral CoM
    !!Currently this function ignores the difference between chiral and-anti-chiral wave-functions
    real(KIND=dp), intent(IN) :: Xlist(CoM_D),Ylist(CoM_D)
    complex(KIND=dpc), intent(IN) :: tau
    complex(KIND=dpc) :: contrib
    integer :: K,Nlist(CoM_D),Min_Loop
    !!FIXME: The method of really adding upp all the terms 
    !! is verry inneficient. Need to importve this
    !! NB - carbon copy of chrial calculation
    
    !!Loop through iteration steps
    weight=log_zero() !!Reset the output weight
    K=0 !!Start by taking zero steps
    !!verbose_CM=.TRUE.
    Min_Loop=2
    
    do
       if(verbose_CM)then
          write(*,*) 'create indexes for ',K,'steps and',CoM_D,'variables'
       end if
       contrib=log_zero() !!Resetting contribution
       call do_make_nc_sum_index(Nlist,1,.FALSE.,K,contrib,Xlist,Ylist,tau)
       if(verbose_CM)then
          write(*,*) 'At step:', K
          write(*,*) 'The contribution is:',contrib
       end if
       weight=add_exponentially(weight,contrib)
       if(abs(weight).ge.real_infinity())then
          write(*,*) '-----cncd-----'
          write(*,*) 'ERROR: Internal'
          write(*,*) 'Recieved infinity while computing CoM'
          write(*,*) 'at step:',K
          write(*,*) 'tau:',tau
          write(*,*) 'Kmat:'
          call print_int_matrix(CoM_Kmat)
          write(*,*) 'kappa_bar:'
          call print_int_matrix(CoM_kappa_bar)
          write(*,*) 'K-denominator:',CoM_kdenom
          write(*,*) 'Xlist:',Xlist
          write(*,*) 'Ylist:',Ylist
          write(*,*) 'quitting...'
          call exit(-2)
       end if
       if(verbose_CM)then
          write(*,*) 'Cumulated weight:   ',weight
       end if
       if((weight.eq.add_exponentially(weight,contrib)).and.(K.ge.Min_Loop))then
          !!Stop when the contribuution does not change the value
          if(verbose_CM)then
             write(*,*) '--------Converged--------'
             write(*,*) 'weight:',weight
          end if
          exit
       end if
       K=K+1 !!Next take one more step
    end do
    
    
  end function compute_nc_cm_dn
  
  recursive subroutine do_make_nc_sum_index(Nlist,Current,Corner,Steps,&
       contrib,Xlist,Ylist,tau)
    
    integer, intent(IN) :: Steps
    real(KIND=dp), intent(IN) :: Xlist(CoM_D),Ylist(CoM_D)
    complex(KIND=dpc), intent(IN) :: tau
    integer :: NList(CoM_D) , Current,Iter
    logical :: Corner
    complex(KIND=dpc) :: contrib
    !!write(*,*) 'Level',Current,'of',CoM_D
    if(Steps.eq.0)then !!IF Steps=0 all steps are zero
       NList=0
       call nc_cm_summation_term(Nlist,contrib,Xlist,Ylist,tau)
    elseif(Current.eq.CoM_D)then !!If this is the last variable...
       if(Corner)then  !!And another varaible is int he corner
          do Iter=-Steps,Steps !!assign any variable
             NList(CoM_D)=Iter
             call nc_cm_summation_term(Nlist,contrib,Xlist,Ylist,tau)
          end do
       else !!And no other varaible is int he corner
          NList(CoM_D)=-Steps !! only assign corner values
          call nc_cm_summation_term(Nlist,contrib,Xlist,Ylist,tau)
          NList(CoM_D)=Steps !! only assign corner values
          call nc_cm_summation_term(Nlist,contrib,Xlist,Ylist,tau)
       end if
    else !!If this is not the last variable 
       NList(Current)=-Steps !!and cal recursiverly start over
       call do_make_nc_sum_index(Nlist,Current+1,.TRUE.,Steps&
            ,contrib,Xlist,Ylist,tau)
       do Iter=(-Steps+1),(Steps-1)  !!assign it to anything in range
          NList(Current)=Iter !!and call recursiverly to start over
          call do_make_nc_sum_index(Nlist,Current+1,Corner,Steps&
               ,contrib,Xlist,Ylist,tau)
       end do
       NList(Current)=Steps !!and cal recursiverly start over
       call do_make_nc_sum_index(Nlist,Current+1,.TRUE.,Steps&
            ,contrib,Xlist,Ylist,tau)
    end if
    
  end subroutine do_make_nc_sum_index
  
 
  subroutine nc_cm_summation_term(Nlist,contrib,Xlist,Ylist,tau)
    integer, intent(IN) :: NList(CoM_D)
    complex(KIND=dpc), intent(IN) :: tau
    real(KIND=dp), intent(IN) :: Xlist(CoM_D),Ylist(CoM_D)
    real(KIND=dp) :: reslinearX
    complex(KIND=dpc) :: tautilde_ab(CoM_D,CoM_D),gauss_res,reslinearY
    complex(KIND=dpc) :: contrib ,weight,expweight
    
    !write(*,*) '....................'
    !write(*,*) 'Summation indexes: ',Nlist
    !write(*,*) '....................'
    
    !write(*,*) 'Xlist:',Xlist
    !write(*,*) 'Ylist:',Ylist
    
    !!FIXME - this could be computed at higher level
    tautilde_ab=(tau*CoM_Kmat-dconjg(tau)*CoM_kappa_bar)/CoM_kdenom
    !Write(*,*) '\tilde{tau}_ab:'
    !call print_cmplx_matrix(CoM_D,tautilde_ab)
    !!Compute matrix product for the quadratic term
    gauss_res=dot_product(Nlist,matmul(tautilde_ab,Nlist))
    !Write(*,*) 'gauss_res:',gauss_res    
    !!Compute matrix product for the linear term
    reslinearX=dot_product(Nlist,matmul(scaled_Kminus,Xlist))
    reslinearY=dot_product(Nlist,matmul(tautilde_ab,Ylist))
    !Write(*,*) 'res lin X:',reslinearX
    !Write(*,*) 'res lin Y:',reslinearY
    
    expweight=gauss_res+2*(reslinearX+reslinearY)
    !write(*,*) 'result tot:',expweight
    
    weight=iunit*pi*expweight
    
    !if(cft_com_verbose)then
    !   write(*,*) 'com weight:',weight
    !end if

    !write(*,*) 'contrib old:',contrib
    !write(*,*) 'weight     :',weight
    contrib=add_exponentially(contrib,weight)
    !write(*,*) 'contrib new:',contrib
    
  end subroutine nc_cm_summation_term
  



  !!--------------------------------------
  !!      IF The State is Hierarchy 
  !!--------------------------------------
  
  complex(KIND=dpc) function make_hierarchy_CM(Zlist,tau) result(weight)
    complex(KIND=dpc), intent(IN) :: Zlist(CoM_D),tau
    integer :: ClistTrans(CoM_D,CoM_D),Qlist(0:CoM_D),Rlist(CoM_D),tlist(CoM_D)
    integer :: Mlist(CoM_D-1)
    complex(KIND=dpc) :: CZlist(CoM_D)
    complex(KIND=dpc), allocatable :: Theta_Vals(:,:)
    logical, allocatable :: Theta_Store(:,:)
    
    !!!WE assume ther incomping kappa is a hierarchy k-matrix
    !!That is the off-diagonals is "diagonal-1"
    
    !!write(*,*) 'Chiral hierarchy kmat:'
    !!call print_int_matrix(Kmat)
    
    call K_matrix_to_T_Q_R_list(Scaled_kminus,CoM_D,Tlist,Qlist,Rlist)    
    
    call Q_list_to_CmatTrans(CoM_D,Qlist,ClistTrans)
    
    !!Prepare the function that calculates the CM-function
    !write(*,*) 'Z-list in'
    !write(*,*) Zlist
    CZlist=matmul(ClistTrans,Zlist)
    !write(*,*) 'CZ-list out'
    !write(*,*) CZlist
    MList=0
    weight=czero
    if(CoM_D.le.1)then
       call do_make_hierarchy_CM(CoM_D,Qlist,Rlist,CZlist,tau,weight,MList,1)
    else
       !!write(*,*) 'D:',D
       !!write(*,*) 'Qlist:',Qlist
       !!write(*,*) 'Mult:',Qlist(0:(D-1))*Qlist(1:D)
       !!write(*,*) 'Allocate theta storage:',D-1,'x',Qlist(D-1)
       allocate(Theta_store(CoM_D-1,Qlist(CoM_D-1)))
       Theta_store=.FALSE. !!Set nothing to stored
       allocate(Theta_vals(CoM_D-1,Qlist(CoM_D-1)))
       weight=recursicve_hierarchy_CM(CoM_D,Qlist,Rlist,CZlist,tau,1,Theta_store,Theta_vals,0)
    end if

    !!Make logscale for upstream transformations
    weight=log(weight)
  end function make_hierarchy_CM

  recursive subroutine do_make_hierarchy_CM(D,Qlist,Rlist,Zlist,tau,weight,Mlist,index)
    integer, intent(IN) :: D,Qlist(0:D),Rlist(D)
    complex(KIND=dpc), intent(IN) :: Zlist(D),tau
    integer :: Mlist(D-1),index,iter
    complex(KIND=dpc) :: weight,contr
    !write(*,*) 'Entering the construction function'
    !write(*,*) 'M-list in:',Mlist
    !write(*,*) 'index:',index
    if(d.eq.1)then
       !write(*,*) 'No extra index needed'
       weight=theta_function_multiplier(D,Qlist,Rlist,Zlist,tau,Mlist)
    else
       do Iter=1,Qlist(index) !!assign a variable in the sum
          MList(index)=Iter
          if(index.eq.(D-1))then
             !write(*,*) 'index to end of sum'
             !write(*,*) 'M-list:',Mlist
             contr=theta_function_multiplier(D,Qlist,Rlist,Zlist,tau,Mlist)
             weight=weight+contr
          elseif(index.eq.D)then
             write(*,*) 'overshot: index:',index
             call exit(-2)
          else
             call do_make_hierarchy_CM(D,Qlist,Rlist,Zlist&
                  ,tau,weight,Mlist,index+1)
          end if
       end do
    end if
  end subroutine do_make_hierarchy_CM
  
  recursive function recursicve_hierarchy_CM(D,Qlist,Rlist,Zlist,tau,index,Theta_store,Theta_Vals,Iter_in) result(weight)
    !!Tis function assumes D>2 so noo need for smaller 
    integer, intent(IN) :: D,Qlist(0:D),Rlist(D),Iter_in
    complex(KIND=dpc), intent(IN) :: Zlist(D),tau
    complex(KIND=dpc) :: Theta_vals(D-1,Qlist(D-1))
    logical :: Theta_store(D-1,Qlist(D-1))
    integer :: index,iter
    complex(KIND=dpc) :: weight,contr
    !write(*,*) 'Entering the construction function'
    !write(*,*) 'index:',index
    if(d.lt.2)then
       write(*,*) 'ERROR: This functions is not defined for D<2'
       call exit(-1)
    else
       !write(*,*) '     Qllist:', Qlist
       !write(*,*) 'Index, D,iter_in', index,D,iter_in
       !write(*,*) 'Basis case: sum over mj and call recusively (j+1)'
       if(theta_exists(D,index,Iter_in,Qlist,Theta_store))then
          !!Extract values
          weight = extract_theta(D,index,Iter_in,Qlist,Theta_vals)
       else
          !!Compute values
          weight=0.d0 !!Initiate
          if(index.lt.D)then !!Sum over the recusive steps
             do Iter=1,Qlist(index)
                contr=theta_factor(D,Qlist,Rlist,Zlist,tau,index,Iter_in,Iter)&
                     *recursicve_hierarchy_CM(D,Qlist,Rlist,Zlist&
                     ,tau,index+1,Theta_store,Theta_Vals,Iter)
                weight = weight + contr
             end do
          elseif(index.eq.D)then !!The last factor is just the theta factor
             do Iter=1,Qlist(index)
                contr=theta_factor(D,Qlist,Rlist,Zlist,tau,index,Iter_in,Iter)
              weight = contr
           end do
        else
           write(*,*) 'ERROR: Index has wrong size.'
           call exit(-1)
        end if
        
        !! Store values if index > 1
        if(index.gt.1)then
           call  insert_theta(D,index,Iter_in,Qlist,Theta_vals,Theta_store,weight)
        end if
     end if

    end if
  end function recursicve_hierarchy_CM
  
  logical function theta_exists(D,index,Iter_in,Qlist,Theta_store) result(exists)
    integer, intent(IN) :: D,Qlist(0:D),Iter_in,index
    logical, intent(IN)  :: Theta_store(D-1,Qlist(D-1))

    !!write(*,*) 'check_theta:D,index,Iter_in:',D,index,Iter_in
    !!write(*,*) Theta_store
    if(index.eq.1)then
       exists=.FALSE. !!The first level is never (can  not be)  stored. 
    else
       exists =  Theta_store(index-1,Iter_in)
    end if
  end function theta_exists


  subroutine  insert_theta(D,index,Iter_in,Qlist,Theta_vals,Theta_store,weight)
    integer, intent(IN) :: D,Qlist(0:D),Iter_in,index
    logical  :: Theta_store(D-1,Qlist(D-1))
    complex(kind=dpc), intent(IN) :: weight
    complex(kind=dpc)  :: Theta_vals(D-1,Qlist(D-1))

    !!write(*,*) 'insert_theta:D,index,Iter_in:',D,index,Iter_in
    !!write(*,*) 'insert_val:',weight
    if(index.eq.1)then
       !!The first level is never (can  not be)  stored. 
       write(*,*) 'ERROR: The first level is never (can  not be)  stored.'
       call exit(-1)
    else
       Theta_store(index-1,Iter_in)=.TRUE.
       Theta_vals(index-1,Iter_in)=weight
    end if
    !!write(*,*) 'Stored theta:',Theta_store
    
  end subroutine insert_theta

  complex(kind=dpc) function extract_theta(D,index,Iter_in,Qlist,Theta_vals) result(weight)
    integer, intent(IN) :: D,Qlist(0:D),Iter_in,index
    complex(kind=dpc), intent(IN)  :: Theta_vals(D-1,Qlist(D-1))

    !!write(*,*) 'extract_theta:D,index,Iter_in:',D,index,Iter_in
    if(index.eq.1)then
       write(*,*) 'ERROR: The first level is never (can  not be)  stored.'
       call exit(-1)
    else
       weight =  Theta_vals(index-1,Iter_in)
    end if


  end function extract_theta
  
complex(KIND=dpc) function theta_factor(D,Qlist,Rlist,Zlist,tau,index,m1,m2) result(contribution)
    integer, intent(IN) :: D,Qlist(0:D),Rlist(D),index,m1,m2
    complex(KIND=dpc), intent(IN) :: Zlist(D),tau
    !!write(*,*) 'Theta func for index,m1,m2:',index,m1,m2
    if(index.eq.1)then
       contribution=thetagen(-m2/(1.d0*Qlist(1)),0.d0,&
            Zlist(1),Rlist(1)*tau)
    elseif(index.eq.D)then
       contribution=thetagen(m1/(1.d0*Qlist(d-1)),0.d0,&
            Zlist(d),Rlist(d)*tau)
    else
       contribution=thetagen(&
            m1/(1.d0*Qlist(index-1))&
            -m2/(1.d0*Qlist(index)),0.d0,&
            Zlist(index),Rlist(index)*tau)
    end if
    !!write(*,*) 'Factor:',contribution
    
  end function theta_factor



  complex(KIND=dpc) function theta_function_multiplier(D,Qlist,Rlist,Zlist,tau,Mlist) result(contribution)
    integer, intent(IN) :: D,Qlist(0:D),Rlist(D),Mlist(D-1)
    complex(KIND=dpc), intent(IN) :: Zlist(D),tau
    integer :: index
    
    if(D.eq.1)then
       !write(*,*) 'ordinary theta-function'
       !write(*,*) 'Zlist:',Zlist
       !write(*,*) 'Rlist:',Rlist
       contribution=thetagen(0.d0,0.d0,Zlist(1),Rlist(1)*tau)
    else
       contribution=thetagen(-Mlist(1)/(1.d0*Qlist(1)),0.d0,&
            Zlist(1),Rlist(1)*tau)&
            *thetagen(Mlist(d-1)/(1.d0*Qlist(d-1)),0.d0,&
            Zlist(d),Rlist(d)*tau)
       do index=2,(d-1)
          contribution=contribution*thetagen(&
               Mlist(index-1)/(1.d0*Qlist(index-1))&
               -Mlist(index)/(1.d0*Qlist(index)),0.d0,&
               Zlist(index),Rlist(index)*tau)
       end do
    end if

    !write(*,*) 'M-list',Mlist
    !write(*,*) 'cont',abs(contribution)
  end function theta_function_multiplier

  real(kind=dp) function real_infinity() result(infinity)
    !!Returns infinity
    infinity=0.d0
    infinity=-log(infinity)
  end function real_infinity
  
  complex(kind=dpc) function log_zero() result(res)
    !!Resturns log(0) = -infinity (as complex)
    res=czero
    res=log(res)
  end function log_zero
  


  complex(kind=dpc) function add_exponentially(x,y) result(z)
    implicit none
    complex(kind=dpc) , intent(in) :: x,y
    
    !! we add the smaller number as a pertubation to the larger one
    if(real(x).gt.real(y)) then
       z = x + log(1+exp(y-x))
    elseif(real(x).le.real(y)) then
       z = y + log(1+exp(x-y))
    else
       !!FIXME - is this the correct way to handle is
       !! (can the situation even happen?)
       write(*,*) 'Trying to add x=',x,' and y=',y
       write(*,*) 'x is neither larger than, smaller than or equal to y'
       call exit(-1)
    end if
  end function add_exponentially

end module center_of_mass
