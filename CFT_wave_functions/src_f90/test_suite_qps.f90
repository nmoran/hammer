program test_qps
  
  USE typedef     ! types and definitions
  use test_utilities
  use k_matrix
  use center_of_mass
  use chiral_wave_function
  use combinations
  use misc_random
  
  !!Variables
  IMPLICIT NONE  
  
  
  !!TEST STARTS HERE!!
    write(*,*) '         Test that quasiparticles are implemented correcly'
    !! Testing the symmetry properties of the many-body wave-function
    !! Is more or less a carbon copy of the chiral version
    call test_pbc_laughlin
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_pbc_nc_laughlin
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_pbc_laughlin_on_qps
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_pbc_nc_laughlin_on_qps
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    
contains 
  
  !!----------------------------------------------
  !!     Thest the manu body wave-functions
  !!---------------------------------------------
  subroutine test_pbc_XX_laughlin(NC)
    logical, intent(IN) :: NC
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:,:) :: Kminus
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_bar
    integer,ALLOCATABLE, DIMENSION(:) :: plist
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: XQcoord,YQcoord
    integer :: q,cells,D,p,indx,Ne,Ns,raw_k_sector,n2,qps,Kmin,NC_max,NC_val,qp_denom
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,tau_x,tau_y
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_bc
    write(*,*) 'Test that correct periodic boundary conditions can be set and are satisfied'
    !!This is really a test that of eqn (A34) in PRB 89, 125303 (2014) get shiftet by Nq
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    call set_cft_wfn_verbose(.FALSE.)    

    !!IF we consider NC then NC_val is higher than 0
    if(NC)then
       NC_max=4
    else
       NC_max=0
    end if
    
    
    !!WE start with chiral laughlin states!!
    D=1
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(kappa_bar(D,D))
    ALLOCATE(Kminus(D,D))
    ALLOCATE(plist(D))
    
    do Kmin=1,5
       Kminus(1,1)=Kmin
       write(*,*) 'Kminus-matrix is:'
       call print_int_matrix(Kminus)
       call kappa_matrix_group_sizes(D,Kminus,plist,p,q) 
       do NC_val=0,NC_max
          kappa_bar(1,1)=NC_val
          do qp_denom=0,kappa_bar(1,1)
             do cells=1,3 !!Loop over different number of fundamental cells
                !write(*,*) 'No. Cells:',cells
                Ne=p*cells
                ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))    
                do qps=0,3 !!Loop over different number of qps
                   Ns=q*Ne+qps
                   !write(*,*) 'No. qps:',qps
                   !write(*,*) 'qp_denom,qp_nomin:',qp_denom,Kminus(1,1)
                   !write(*,*) 'Ne,Nq,Ns:',Ne,qps,Ns
                   ALLOCATE(XQcoord(qps),YQcoord(qps))
                   !!Set kappa_bar,kappa_matrix
                   kappa_matrix=Kminus+kappa_bar
                   !!Initialize the cooridinates (use unit length in X).
                   !!The length scale Lx is thus implicit.
                   call RANDOM_NUMBER(tau_x)
                   call RANDOM_NUMBER(tau_y)
                   !Put tau_x in range -1/2 < tau_x < 1/2
                   tau_x=tau_x-.5
                   !Put tau_y in range 1/2 < tau_x < 1+1/2
                   tau_y=tau_y+.5
                   
                   !!Force tau to be trvial (not in use)
                   !!tau_x=0.d0
                   !!tau_y=1.d0
                   
                   tau=tau_x+iunit*tau_y
                   call RANDOM_NUMBER(Xcoord)
                   call RANDOM_NUMBER(Ycoord)
                   !!Make the random number in the range -1/2 < x < 1/2
                   Xcoord=Xcoord-.5d0
                   Ycoord=Ycoord-.5d0
                   
                   call RANDOM_NUMBER(XQcoord)
                   call RANDOM_NUMBER(YQcoord)
                   !!Make the random number in the range -1/2 < x < 1/2
                   XQcoord=XQcoord-.5d0
                   YQcoord=YQcoord-.5d0
                   
                   
                   !write(*,*) 'Compute K-sector'
                   !!Set the boudnary conditions to periodic
                   raw_k_sector=compute_raw_K_sector(D,Ne,Ns,Kminus,plist,q,(/qps/))
                   !write(*,*) 'TEST: raw k_sector is',raw_k_sector
                   call set_CM_bc(Ns,raw_k_sector,D,kappa_matrix,kappa_bar,Nq=(/qps/))
                   
                   !!Compute first wave function
                   !write(*,*) '   ----Raw wave function ----'
                   !!Initialize the wave-function
                   call initialize_wave_function(D,kappa_matrix,Ne,Ns&
                        ,(/(0, n2=1,D)/),(/(0, n2=1,D)/),kappa_bar,qps=(/qps/),&
                        qp_pow=(/qp_denom,Kminus(1,1)/))
                   psi=chiral_raw_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                   
                   do indx=1,Ne
                      !!Change the coordinates one by one in x-direction
                      Xcoord2=Xcoord
                      Ycoord2=Ycoord
                      Xcoord2(indx)=Xcoord2(indx)+1.d0
                      !write(*,*) '   ----Change in x-coordinates for particle no.',indx
                      psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                      !!Change the coordinates one by one in y-direction
                      Xcoord2=Xcoord
                      Ycoord2=Ycoord
                      Ycoord2(indx)=Ycoord2(indx)+1.d0
                      !!Compute the gaguge phase
                      !write(*,*) '   ----Change in y-coordinates for particle no.',indx
                      gauge_phase=Xcoord(indx)*2*pi*Ns
                      psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)&
                           +iunit*gauge_phase !!Add the gauge phase
                      
                      !!Reduce to smallest mod_2pi
                      psi_2x=mod_2pi(psi_2x)
                      psi_2y=mod_2pi(psi_2y)
                      
                      !!Test the periodic boundary conditions             
                      if(test_diff_exp_cmplx(psi,psi_2x,1.d-10)&
                           .or.&
                           test_diff_exp_cmplx(psi,psi_2y,1.d-10))then
                         write(*,'(A,A,I3,A)') '.-.-.-.-.-.-.'&
                              ,'ERROR for change in coordinates for particle no.',indx&
                              ,'.-.-.-.-.-.-'
                         write(*,*) 'Not-periodic'
                         write(*,*) 'The matrices are:'
                         write(*,*) 'Kminus'
                         call print_int_matrix(Kminus)
                         write(*,*) 'kappa_matrix'
                         call print_int_matrix(kappa_matrix)
                         write(*,*) 'kappa_bar'
                         call print_int_matrix(kappa_bar)
                         write(*,*) 'No. qps:',qps
                         write(*,*) 'qp_denom,qp_nomin:',qp_denom,Kminus(1,1)
                         write(*,*) 'Ne,Nq:',Ne,qps
                         write(*,*) 'tau:',tau
                         write(*,*) 'cells:',cells
                         write(*,*) 'Ns:',Ns
                         call print_the_psis(psi,psi_2x,psi_2y)
                         call print_scaled_psis(psi,psi_2x,psi_2y,Xcoord(indx),'x(i)')
                         call print_scaled_psis(psi,psi_2x,psi_2y,Ycoord(indx),'y(i)')
                         write(*,*) 'Coordinates Z:'
                         write(*,*) Xcoord+tau*Ycoord
                         write(*,*) 'Coordinates Zq:'
                         write(*,*) XQcoord+tau*YQcoord
                         call exit(-2)
                      end if
                   end do !!Next particle
                   DEALLOCATE(XQcoord,YQcoord)
                end do !!Next number of qps
                !!Delaocate the coordinates
                DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
             end do !!Next cell size
          end do !!Next qp_denom
       end do !!Neext NC laughlin parameter
    end  do !!Next Laughlin parameter
    DEALLOCATE(Kminus)
    DEALLOCATE(kappa_bar)
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist)
    !!unset the pbcs at the end
    call unset_CM_bc
  end subroutine test_pbc_XX_laughlin
  
  
  subroutine test_pbc_nc_laughlin
    call test_pbc_XX_laughlin(.FALSE.)
  end subroutine test_pbc_nc_laughlin
  
  subroutine test_pbc_laughlin
    call test_pbc_XX_laughlin(.TRUE.)
  end subroutine test_pbc_laughlin
  
  
  
  subroutine test_pbc_XX_laughlin_on_qps(NC)
    logical, intent(IN) :: NC
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:,:) :: Kminus
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_bar
    integer,ALLOCATABLE, DIMENSION(:) :: plist
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,XQcoord2,YQcoord2
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: XQcoord,YQcoord
    integer :: q,cells,D,p,indx,Ne,Ns,raw_k_sector,n2,qps,Kmin,NC_max,NC_val
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y,psi_plus,psi_minus
    real(kind=dp) :: gauge_phase,tau_x,tau_y
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_bc
    write(*,*) 'Test for periodic pbs. I.e that qps also have correct pbc:s'
    !!We test that only a phase i accumulated when a qp revolves around the torus in the x-direction
    !!However if if revolves in the y-direction then the state gets translated into the next degenerate groundstateparent
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    call set_cft_wfn_verbose(.FALSE.)

    !!IF we consider NC then NC_val is higher than 0
    if(NC)then
       NC_max=4
    else
       NC_max=0
    end if
    
    
    !!WE start with chiral laughlin states!!
    D=1
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(kappa_bar(D,D))
    ALLOCATE(Kminus(D,D))
    ALLOCATE(plist(D))
    
    do Kmin=1,5
       Kminus(1,1)=Kmin
       write(*,*) 'Kminus-matrix is:'
       call print_int_matrix(Kminus)
       call kappa_matrix_group_sizes(D,Kminus,plist,p,q) 
       do NC_val=0,NC_max
          kappa_bar(1,1)=NC_val
          do cells=1,3 !!Loop over different number of fundamental cells
             !write(*,*) 'No. Cells:',cells
             Ne=p*cells
             ALLOCATE(Xcoord(Ne),Ycoord(Ne))    
             do qps=0,3 !!Loop over different number of qps
                write(*,*) 'No. qps:',qps
                Ns=q*Ne+qps
                write(*,*) 'Ne,Nq,Ns:',Ne,qps,Ns
                ALLOCATE(XQcoord(qps),YQcoord(qps),XQcoord2(qps),YQcoord2(qps))
                !!Set kappa_bar,kappa_matrix
                kappa_matrix=Kminus+kappa_bar
                !!Initialize the cooridinates (use unit length in X).
                !!The length scale Lx is thus implicit.
                call RANDOM_NUMBER(tau_x)
                call RANDOM_NUMBER(tau_y)
                !Put tau_x in range -1/2 < tau_x < 1/2
                tau_x=tau_x-.5
                !Put tau_y in range 1/2 < tau_x < 1+1/2
                tau_y=tau_y+.5
                
                !!Force tau to be trvial (not in use)
                !!tau_x=0.d0
                !!tau_y=1.d0
                
                tau=tau_x+iunit*tau_y
                call RANDOM_NUMBER(Xcoord)
                call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
                Xcoord=Xcoord-.5d0
                Ycoord=Ycoord-.5d0
                
                call RANDOM_NUMBER(XQcoord)
                call RANDOM_NUMBER(YQcoord)
                !!Make the random number in the range -1/2 < x < 1/2
                XQcoord=XQcoord-.5d0
                YQcoord=YQcoord-.5d0
                
                
                !write(*,*) 'Compute K-sector'
                !!Set the boudnary conditions to periodic
                raw_k_sector=compute_raw_K_sector(D,Ne,Ns,Kminus,plist,q,(/qps/))
                !write(*,*) 'TEST: raw k_sector is',raw_k_sector
                
                !!Set bcs
                call unset_CM_bc 
                call set_CM_bc(Ns,raw_k_sector-Ne,D,kappa_matrix,kappa_bar,Nq=(/qps/))
                !!Initialize the wave-function
                call initialize_wave_function(D,kappa_matrix,Ne,Ns&
                     ,(/(0, n2=1,D)/),(/(0, n2=1,D)/),kappa_bar,qps=(/qps/),qp_pow=(/kappa_bar(1,1),&
                     Kminus(1,1)/),qp_static=.TRUE.)
                !!Compute first wave function
                !!write(*,*) '   ----Raw wave function ----'
                psi_minus=chiral_raw_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                
                !!Set bcs
                call unset_CM_bc
                call set_CM_bc(Ns,raw_k_sector+Ne,D,kappa_matrix,kappa_bar,Nq=(/qps/))
                !!Initialize the wave-function
                call initialize_wave_function(D,kappa_matrix,Ne,Ns&
                     ,(/(0, n2=1,D)/),(/(0, n2=1,D)/),kappa_bar,qps=(/qps/),qp_pow=(/kappa_bar(1,1),&
                     Kminus(1,1)/),qp_static=.TRUE.)
                !!Compute first wave function
                !!write(*,*) '   ----Raw wave function ----'
                psi_plus=chiral_raw_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                
                !!Set bcs
                call unset_CM_bc
                call set_CM_bc(Ns,raw_k_sector,D,kappa_matrix,kappa_bar,Nq=(/qps/))
                !!Initialize the wave-function
                call initialize_wave_function(D,kappa_matrix,Ne,Ns&
                     ,(/(0, n2=1,D)/),(/(0, n2=1,D)/),kappa_bar,qps=(/qps/),qp_pow=(/kappa_bar(1,1),&
                     Kminus(1,1)/),qp_static=.TRUE.)
                !!Compute first wave function
                !!write(*,*) '   ----Raw wave function ----'
                psi=chiral_raw_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                
                
                
                do indx=1,qps
                   !!Change the coordinates of the qps one by one in x-direction
                   XQcoord2=XQcoord
                   YQcoord2=YQcoord
                   XQcoord2(indx)=XQcoord2(indx)+1.d0
                   !!write(*,*) '   ----Change in x-coordinates for particle no.',indx
                   psi_2x=chiral_raw_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord2,inject_Yqps=YQcoord2)
                   !!Change the coordinates one by one in y-direction
                   XQcoord2=XQcoord
                   YQcoord2=YQcoord
                   YQcoord2(indx)=YQcoord2(indx)+1.d0
                   !!Compute the gaguge phase
                   !write(*,*) '   ----Change in y-coordinates for particle no.',indx
                   gauge_phase=XQcoord(indx)*2*pi*Ns
                   psi_2y=chiral_raw_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord2,inject_Yqps=YQcoord2)&
                        +iunit*gauge_phase !!Add the gauge phase
                   
                   !!Reduce to smallest mod_2pi
                   psi_2x=mod_2pi(psi_2x)
                   psi_2y=mod_2pi(psi_2y)
                   
                   !!Test the periodic boundary conditions             
                   if(test_diff(real(psi),real(psi_2x),1.d-10)&
                        .or.&
                        test_diff(real(psi_plus),real(psi_2y),1.d-10))then
                      write(*,'(A,A,I3,A)') '.-.-.-.-.-.-.'&
                           ,'ERROR for change in coordinates for QUASI-particle no.',indx&
                           ,'.-.-.-.-.-.-'
                      write(*,*) 'Not-periodic'
                      write(*,*) 'The matrices are:'
                      write(*,*) 'Kminus'
                      call print_int_matrix(Kminus)
                      write(*,*) 'kappa_matrix'
                      call print_int_matrix(kappa_matrix)
                      write(*,*) 'kappa_bar'
                      call print_int_matrix(kappa_bar)
                      write(*,*) 'tau:',tau
                      write(*,*) 'cells:',cells
                      write(*,*) 'Ns:',Ns
                      write(*,*) 'psi+ :',psi_plus
                      write(*,*) 'psi- :',psi_minus
                      call print_the_psis(psi,psi_2x,psi_2y)
                      call print_scaled_psis(psi,psi_2x,psi_2y,XQcoord(indx),'x(i)')
                      call print_scaled_psis(psi,psi_2x,psi_2y,YQcoord(indx),'y(i)')
                      write(*,*) 'Coordinates Z:'
                      write(*,*) Xcoord+tau*Ycoord
                      write(*,*) 'Coordinates Zq:'
                      write(*,*) XQcoord+tau*YQcoord
                      call exit(-2)
                   end if
                end do !!Next particle
                DEALLOCATE(XQcoord,YQcoord,XQcoord2,YQcoord2)
             end do !!Next number of qps
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord)
          end do !!Next cell size
       end do !!Neext NC laughlin parameter
    end  do !!Next Laughlin parameter
    DEALLOCATE(Kminus)
    DEALLOCATE(kappa_bar)
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist)
    !!unset the pbcs at the end
    call unset_CM_bc
  end subroutine test_pbc_XX_laughlin_on_qps
  
  
  subroutine test_pbc_nc_laughlin_on_qps
    call test_pbc_XX_laughlin_on_qps(.FALSE.)
  end subroutine test_pbc_nc_laughlin_on_qps
  
  subroutine test_pbc_laughlin_on_qps
    call test_pbc_XX_laughlin_on_qps(.TRUE.)
  end subroutine test_pbc_laughlin_on_qps
  
  
  subroutine print_the_psis(psi,psi_2x,psi_2y)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-x:',mod_2pi(psi_2x)
    write(*,*) 'psi-y:',mod_2pi(psi_2y)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi):',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi):',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_psis
  
  subroutine print_scaled_psis(psi,psi_2x,psi_2y,coord,type)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y
    real(kind=dpc), intent(in) :: coord
    character(*), intent(in) :: type
    write(*,*) 
    write(*,*) 'dixff-x/(',type,'*2*pi):',(mod_2pi(psi_2x)-mod_2pi(psi))/(coord*2*pi)
    write(*,*) 'diff-y/(',type,'*2*pi):',(mod_2pi(psi_2y)-mod_2pi(psi))/(coord*2*pi)
  end subroutine print_scaled_psis
  
end program test_qps
