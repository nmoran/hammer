MODULE hamiltonian

  use timer
  USE typedef
  USE global_ed
  use global_torus
  USE fock_basis
  USE quantum_numbers
  use bits_manipulation
  use channels
  
  IMPLICIT NONE
  
  !===================================================
  ! Pair-interaction matrix elements at LLL on a torus
  !===================================================
  complex(kind=dpc), PRIVATE, ALLOCATABLE :: Vkp(:,: ) ! intreaction matrix elements (in asymetric hoppnig form)
  complex(kind=dpc), PRIVATE, ALLOCATABLE :: Vnd(:,: ) ! intreaction matrix elements (in symmetric hopping form)
  
  
CONTAINS
  
  !***********
  !HAMILTONIAN
  !***********
  
  ! Define here your Hamiltonian, split into
  ! one- and two-particle parts H1 and H2.
  ! Quantum numbers are given in the matrix qns(:,:).
  
  ! Two-particle matrix element
  ! ============
  
  SUBROUTINE init_hamiltonian()
    ! Coulomb matrix elements
    CALL tabulate_Vkp()
  END SUBROUTINE init_hamiltonian
  
  FUNCTION H2(rx,sx,tx,ux) ! Order makes difference!
    INTEGER, INTENT(IN) :: rx, sx, tx, ux
    ! NOTE: this combines the direct and exchange contributions
    !       note a corresponding modification in matrixelements.f90
    INTEGER :: k1,k2,k3,k4,k14,k13
    complex(KIND=dpc) :: H2
    
    H2 = 0.d0 ! by default
    
    ! matrixelements.f90 has a different convention with indices
    k1 = qns(rx,qmom) ! quantum numbers
    k2 = qns(sx,qmom) ! if don't have spin
    k4 = qns(tx,qmom) ! these are trivially
    k3 = qns(ux,qmom) ! the x momenta
    
    ! momentum and spin must be conserved
    IF ( MODULO(k1+k2-k3-k4,working_ns) /= 0 .OR. qns(rx,qsz)/=qns(tx,qsz) &
         .OR. qns(sx,qsz)/=qns(ux,qsz)) RETURN 
    
    k13 = MODULO(k1-k3,working_ns) ! have to take modulos as we are about to
    k14 = MODULO(k1-k4,working_ns) ! address an array
    
    !!This is where the antisymmetrization of V_kp takes place
    H2 = Vkp(k14,k13)-Vkp(k13,k14) ! V_1234=V_2143 so only two terms nececarry.
    
  END FUNCTION H2
  
  SUBROUTINE tabulate_Vkp()
    INTEGER :: k,p,n,d
    
    ! V_{k1k2k3k4} = V_{k1-k4,k1-k3} 
    
    ! NOTE that k=k1-k4 and k=k1-k3
    ! NOTE that Vkp does NOT include the exchange contribution
    ! Direct + exchange are given by V_kp +- V_pk
    IF (.NOT. ALLOCATED(Vkp) ) ALLOCATE(Vkp(0:working_ns-1,0:working_ns-1))
    IF (.NOT. ALLOCATED(Vnd) ) ALLOCATE(Vnd(0:working_ns-1,0:working_ns-1))
    
    write(*,*) 'Ordering V_kp'
    DO k=0,working_ns-1
       DO p=0,working_ns-1
          Vkp(k,p) = Vijkl(k,-p, k-p,0)  ! modulos are taken in Vijkl
       END DO
    END DO
    
    ! NOTE that n=k4-k3 and d=k1-k4
    ! NOTE that Vnd does NOT include the exchange contribution
    ! Direct + exchange are given by ??V_nd +- V_n,-n-d??
    
    write(*,*) 'Ordering V_nd'
    DO n=0,working_ns-1
       DO d=0,working_ns-1
          Vnd(n,d) = Vijkl(d,-n-d,-n,0)  ! modulos are taken in Vijkl
       END DO
    END DO
    
    !Note also that V_1234 = V_2143 by symmetry
    
    CALL exportVkp()
  END SUBROUTINE tabulate_Vkp
  
  SUBROUTINE exportVkp()
    INTEGER :: n, d,k,p
    INTEGER :: chre = 836
    INTEGER :: chim = 835
    !Export Vkp
    OPEN(chre,FILE=TRIM('result/Vkp.re.dat'),STATUS='unknown')
    OPEN(chim,FILE=TRIM('result/Vkp.im.dat'),STATUS='unknown')
    DO k = 0, working_ns-1
       DO p = 0, working_ns-1
          WRITE(chre,'(E16.7)',advance='no') trim_V(real(Vkp(k,p),dp))
          WRITE(chim,'(E16.7)',advance='no') trim_V(aimag(Vkp(k,p)))
       END DO
       write(chre,*)
       write(chim,*)
    END DO
    WRITE(*,*) 'Exported Vkp to file Vkp.re.dat and Vkp.im.dat'
    
    CLOSE(chre)
    CLOSE(chim)
    !Also export Vnd
    OPEN(chre,FILE=TRIM('result/Vnd.re.dat'),STATUS='unknown')
    OPEN(chim,FILE=TRIM('result/Vnd.im.dat'),STATUS='unknown')
    DO n = 0, working_ns-1
       DO d = 0, working_ns-1
          WRITE(chre,'(E16.7)',advance='no') trim_V(real(Vnd(n,d),dp))
          WRITE(chim,'(E16.7)',advance='no') trim_V(aimag(Vnd(n,d)))
       END DO
       write(chre,*)
       write(chim,*)
    END DO
    
    CLOSE(chre)
    CLOSE(chim)
    WRITE(*,*) 'Exported Vnd to file Vnd.re.dat and Vnd.im.dat'
    
  END SUBROUTINE exportVkp
  
  real(kind=dpc) function trim_V(s_in) result(s_out)
    !!Sanity: Put s_in to zero it is really small"
    !!FIXME: We do this because when fortran outputs these small numberes
    !!then xE-yyy -> x-yyy (The E is lost)
    real(kind=dpc), intent(in) :: s_in
    s_out=s_in
    if(abs(s_in).le.1.d-90)then
       s_out=0.d0
    end if
  end function trim_V
  
  FUNCTION Vijkl(k1,k2,k3,k4) RESULT(s)
    INTEGER, INTENT(IN) :: k1,k2,k3,k4
    ! this returns the pair matrix element
    ! V_{k1k2k3k4} = \int d^2x \int d^2x' 
    !        \psi_{k_1}^*(x)\psi_{k_4}(x)V(x-x')\psi_{k_2}^*(x')\psi_{k_3}(x')
    ! of LL single-particle states in level L.
    ! Vijkl( 1, 2, 2, 1) gives the direct and (1,2,1,2) the exchange
    complex(KIND=dpc) :: w1,w2,s,s_old
    INTEGER :: j1
    INTEGER :: j1_max=300,j1_min=5
    logical :: not_conv
    
    !! The basis functions \eta_n are implemeted such that 
    !!    \eta_{n+working_ns} = \eta_n.
    
    !! This makes the Vijkl elements non-invariant under \tau -> \tau +1
    
    s = 0.d0
    IF ( MODULO(k1+k2-k3-k4,working_ns) /=0 ) RETURN  ! momentum conservation
    !! Returun zero if the momentum is not conserved !!
    
    if(VOnly)then    
       write(*,*) "Compute hopping V_ijkl:",MODULO(k1,working_ns),&
            MODULO(k2,working_ns),MODULO(k3,working_ns),MODULO(k4,working_ns)
    end if
    
    !!We can safetly assume that abs(s1-s2)<working_ns/2
    !!by the min_module contruction in Vevaluate.
    
    !!WE approach the problem by obtaining convergence for each row 
    !! of j2 individually.
    !! The problem is the reduced to two coupled one-dimentional problems.
    !!We sum symmetrically around (j1,j2)=(0,0),
    !!since the major wight will be there.
    
    !!The middle term j1=0
    j1=0
    w1=j1_converge(j1,k1,k2,k3) 
    s=w1
    !!The wings uppward and downward
    not_conv=.TRUE. !!Set the default value for the converegence
    do j1=1,j1_max  !!Iterate parwise over the rest of the steps
       s_old=s !!We redod the old value su that we may check convergence
       w1=j1_converge(j1,k1,k2,k3) 
       w2=j1_converge(-j1,k1,k2,k3) 
       s=s+w1+w2  !!New values added to old
       if((s_old.eq.s).and.(j1.ge.j1_min))then !!Check that value converged,
          !! and minimal amound of steps are  tekien
          if(VOnly)then    
             write(*,*) 'Converged after ',j1,' steps, on',s
          end if
          not_conv=.FALSE.
          exit
       end if
    end do
    if(not_conv)then !!In case more steps are taken
       write(*,*) 'ERROR: Did not converge after ',j1_max,'steps'
       call exit(-1)
    end if
    
  END FUNCTION Vijkl
  
  
  complex(KIND=dpc) function j1_converge(j1,s1,s2,s3) result(res)
    INTEGER, intent(IN) :: j1,s1,s2,s3
    integer :: j2,j2_max=300,j2_min=10,j2shift
    complex(kind=dpc) :: w1,w2,res_old
    logical :: not_conv=.TRUE.
    res=0.d0 !!Set the output variable
    j2=0  !!Set the staring step
    if(T1Basis)then !!The shift has different sizes
       !!depending on the size of n2
       j2shift=nint((j1*working_ns+min_mod(s2-s3,working_ns))*tau_re_dp)
    else
       j2shift=nint(j1*tau_re_dp/working_ns_dp)
    end if
    
    !!Evalualte the j2=0 effective center
    w1=Vevaluate(j1,j2+j2shift,s1,s2,s3)
    res=w1
    do j2=1,j2_max  !!Terate parwise over the rest of the steps
       res_old=res !!Store the old value for convergence comparisson
       w1=Vevaluate(j1,j2+j2shift,s1,s2,s3)
       w2=Vevaluate(j1,-j2+j2shift,s1,s2,s3)
       res=res+w1+w2 !!Add new values to the old one
       if((res_old.eq.res).and.(j2.ge.j2_min))then
          not_conv=.FALSE.
          exit
       end if
    end do
    if(not_conv)then !Erro if not converged in time
       write(*,*) 'ERROR: Did not converge after ',j2_max,'steps'
       call exit(-1)
    end if
  end function J1_converge
  
  
  complex(KIND=dpc) function Vevaluate(t1,t2,s1,s2,s3) result(res)
    INTEGER, intent(IN) :: t1,t2,s1,s2,s3
    INTEGER :: n1,n2,nspecial,s23,s13
    real(KIND=dp) :: x1,y1,w1,y1x1
    complex(KIND=dpc) :: z_m
    
    !! FIXME check that the coulomb-elements are correctly implemented (i.e that all the exponentials have the right signs etc. then comparing t1 basis and t2 basis)
    !! FIXME- check that the phase factors on the t2 basis are correct.
    s23=min_mod(s2-s3,working_ns)
    s13=min_mod(s1-s3,working_ns)
    
    if(T1BASIS)then
       n1=s23+working_ns*t1
       n2=t2
       nspecial=n2
    else
       n1=t1
       n2=-s23+working_ns*t2
       nspecial=n1
    end if
    
    
    !! This is where we caclucalte the term that we want to sum over.
    y1 = Ly/working_ns_dp
    x1 = Lx/working_ns_dp
    w1 = Ldelta/working_ns_dp
    y1x1=2.d0*pi/working_ns_dp
    
    !! The reciprocal vector is 
    z_m=iunit*x1*n2-iunit*tau_dpc*x1*n1
    
    res = exp(-0.5d0 * (abs(z_m))**2 )&
         *exp(iunit*y1x1*nspecial*s13)&
         *LLDependence(z_m)&
         *LLDependence(-z_m)&
         *Vdiscrete(n1,n2)
  end function Vevaluate
  
  function min_mod(s1,mod_num) result(smod)
    !!This function chooses the modular value with the smalles absolute value.
    integer, intent(IN) :: s1,mod_num
    integer :: smod,m1,m2
    m1=modulo(s1,mod_num)
    m2=-modulo(-s1,mod_num)
    if(abs(m1).le.abs(m2))then
       smod=m1
    else
       smod=m2
    end if
  end function min_mod
  
  function LLDependence(z_m) result(res)
    complex(KIND=dpc) :: z_m,res
    if(LanLev.eq.0)then
       res=1.0d0
    elseif(LanLev.eq.1)then
       res=1.0d0-0.5d0*(abs(z_m))**2
    else !!Not implemented yet
       write(*,*) 'Error: Landau Level ',LanLev,' not implemented yet'
       call exit(1)
    end if
  end function LLDependence
  
  
  function Vdiscrete(n1,n2) result(Vres)
    INTEGER, intent(IN) :: n1,n2
    real(KIND=dp) :: x1,y1,w1
    real(KIND=dp) :: Vres
    
    y1 = Ly/working_ns_dp
    x1 = Lx/working_ns_dp
    w1 = LDelta/working_ns_dp
    
    !!The constant contribution  is set  to zero
    if((n1.eq.0).and.(n2.eq.0))then
       Vres=0.0d0
    else
       Vres=Vfourier(n1*y1,n2*x1-n1*w1)
    end if
  end function Vdiscrete
  
  real(kind=dp) function Vfourier(k1,k2) result(Vfour)
    real(KIND=dp),intent(In)  :: k1,k2
    real(KIND=dp) :: ksq,mscreen
    ksq = k1**2+k2**2
    

    select case(Potential)
    case(0)     !The Lauglin 1/3 pseudopeotential
       Vfour = -ksq
    case(1)     !! Unscrened coulomb
       Vfour = 1/SQRT(ksq)
    case(2) !! Screened coulomb
       !!FIXME this is a potential variable to set
       mscreen = 0.1d0 !!Screening length
       Vfour = exp(-mscreen*SQRT(ksq))/SQRT(ksq)
    case default
       ! dV1 interpolates between 0LL and 1LL
       ! dV1 = 0 <- 0LL
       ! dV1 = 1 -> 1LL
       !!Vfour = (1.d0-dV1*ksq+ 0.25d0*(dV1*ksq)**2)/SQRT(ksq)
       write(stderr,*) 'ERROR: Potential'
       write(stderr,*) 'No interaction mapped no number:',Potential
       call exit(-4)
    end select
    
  end function Vfourier
  
  SUBROUTINE sp_hamiltonian( hs, N_basis )
    USE sparse
    TYPE(spa), INTENT(OUT) :: hs         ! hamiltonian matrix
    INTEGER, INTENT(IN) :: N_basis       ! basis size
    ! 
    ! This constructs a real Hamiltonian matrix in the
    ! basis specified in the module physics. 
    ! matrix in sparse storage format. The basis size is specified
    ! in N_basis (or SIZE(vect) ).
    ! This is how many kind=dp reals that fit into one GB.
    INTEGER(8), PARAMETER :: one_G=2**27 
    !!Allow the matrix to use up to 16 GB of data (One complex matrix and an integer keeping track of which states are used)
    INTEGER(kind=8) :: h_max_elements=int(8*one_G,kind=8)
    INTEGER(kind=8) :: Max_Elements,iter_elem
    REAL :: time_start, time_stop
    INTEGER :: ierr,i1,i2
        complex(KINd=dpc) :: c  !!Temporaty stirage for elements
    
    !!Mickes test
    integer :: unique_hopping((working_ns*Nele*(Nele-1))/2)
    integer :: hoppings,max_size_hoppings,hopping_num
    max_size_hoppings=(working_ns*Nele*(Nele-1))/(2)
    
    ! OK, hs below is going to be the Hamiltonian matrix stored as a
    ! sparse matrix. See Spa_module.f90 for more details how this is done.
    CALL cpu_time(time_start)
    ! h_max_elements tells us how many elements we can have. This is limited
    ! by the computer memory. If the problem is small, we allocate space for
    ! all elements, setting h_max_elements=N_basis**2.
    !
    !!Max elements is so large it might orverflow,
    !!computatoin mus be done in stages
    Max_Elements=N_basis !!Hopping from each state
    Max_Elements=Max_Elements*working_ns  !!To one of the states
    Max_Elements=Max_Elements*Nele !!By one of the electrons
    Max_Elements=Max_Elements*(Nele-1) !!And a partner
    Max_Elements=Max_Elements/2 !!But they are identical (a pair)
    Max_Elements=Max_Elements/2 !!so is the place they end up
    Max_Elements=Max_Elements+1 !!Add an extra state for those rare cases
    write(*,*) 'The basis size is                              :         '&
         ,N_basis
    write(*,*) 'The maximum (sparce) size of the hamiltonian is:',Max_Elements
    write(*,*) 'The maximum (hard coded) size of the memory is :',h_max_elements
    
    !!Never allocate more than the maximum theoretical amount of elements
    IF (h_max_elements<Max_Elements) then
       write(*,*) 'increase h_max_elements, if you can.'
       call exit(-1)
    else
       h_max_elements=Max_Elements
    end IF
    
    ALLOCATE(hs%a(h_max_elements),stat=ierr)
    IF (ierr/=0) then
       write(*,*) ' PROBLEM in allocate a!'
       call exit(-1)
    end IF
    
    ALLOCATE(hs%ja(h_max_elements),stat=ierr)
    IF (ierr/=0) then
       write(*,*)  ' PROBLEM in allocate ja!'
       call exit(-1)
    end IF
    
    ALLOCATE(hs%ia(N_basis+1),stat=ierr)
    IF (ierr/=0) then
       write(*,*)  ' PROBLEM in allocate ia!'
       call exit(-1)
    end IF
    
    !!Specify starting conditions
    hs%n=N_basis
    hs%nz=0
    hs%ia(1)=1
    
    !!Record the time ti takes to constuct the matrix
    CALL setup_write_progress()
    iter_elem=0
    !! Since the matix is two-body;
    !! the maximum of elements needed is N_e*N_s*N_basis = O(N) operations
    !! These loops are over the basis, and the matrix elements are stored to hs.
    DO i1=1, N_basis
       !write(*,*) 'i1',i1
       !!This lists all the unique hoppings
       call compute_hopping_list(unique_hopping,hoppings,i1,max_size_hoppings)
       !write(*,*) 'unique hoppings:',unique_hopping
       !write(*,*) 'hoppings:',hoppings
       !write(*,*) 'max_hoppings:',max_size_hoppings
       
       !!Loop over all the defived hoppings
       !write(*,*) 'i2',i1
       if(max_size_hoppings.lt.hoppings)then
          write(*,*) 'SERIOUS MISCALULATION IN NUMBER OF STATES'
          call exit(-1)
       end if
       
       DO hopping_num=1,hoppings
          i2=unique_hopping(hopping_num)
          
          IF (i2==i1) THEN   ! Diagonal elements:
             c=O2rdiag_elem(i1, H2) !! This calculates the hopping elements
          elseif(i2<i1) THEN  ! Off-diagonal elements: 
             ! OK, if i2 is smaller than i1, we have done this before. No point
             ! in doing again (H(i,j)=conjugate(H(j,i))),
             ! just search the element from hs:
             c=dconjg(spa_get(hs,i2,i1))
          ELSE !! This calculates the hopping elements
             c = O2roff_diag_elem(i1,i2, H2)
          END IF
          call save_matrix_element(hs,c,i2)
       END DO
       IF (h_max_elements-hs%nz<hs%n .AND. i1<N_basis) THEN 
          WRITE(*,*) h_max_elements, ' is the maximum number of Ham. elements.'
          WRITE(*,*) 'and',hs%nz, ' is too close to that, sorry...'
          write(*,*) 'quitting...'
          call exit(-1)
       END IF
       hs%ia(i1+1)=hs%nz+1
       call write_progress(i1,N_basis)
    END DO
    
    WRITE(*,*) 'Done Hamiltonian'
    ! Now we know how many of the elements are non-zero
    WRITE(*,*) 100.d0*REAL(hs%nz)/REAL(N_basis*N_basis), '% nonzero.'
    CALL cpu_time(time_stop)
    WRITE(*,*) 'Took ', time_stop-time_start, ' seconds to construct H'
  END SUBROUTINE sp_hamiltonian
  
  
  subroutine test_hoppings(i1,hopping_num,hoppings,unique_hoppings)
    integer, intent(in) ::  i1,hoppings,unique_hoppings(hoppings)
    integer, intent(inout) ::  hopping_num
    
    hopping_num=hopping_num+1    
    if(i1.ne.unique_hoppings(hopping_num))then
       write(*,*) 'i1,hopp',i1,hopping_num,unique_hoppings(hopping_num)
       write(*,*) 'error'
       call exit(-1)
    end if
    
    
  end subroutine test_hoppings
  
  
  
  subroutine compute_hopping_list(sorted_basis,unique_hoppings,i1,max_size)
    use sort
    integer,intent(in)  :: max_size
    integer :: hoppings
    integer,intent(out) :: unique_hoppings
    integer(kind=8) :: unique_hopping(max_size)
    integer,intent(in) :: i1
    integer(kind=8) :: bbase1,bbase2,i2,rbase1
    integer(kind=8) :: hopping_list(max_size)
    integer(kind=8) :: sort_hopping(max_size)
    integer :: order_hopping(max_size)
    integer :: basis_order(max_size)
    integer,intent(OUT) :: sorted_basis(max_size)
    integer :: ibase(Nele)
    integer :: e1,e2,steps

    if(max_size.eq.0)then !!This happens for the trivial case of one electron
       unique_hoppings=0
    else
    !write(*,*) 'From Basis item:',i1
    bbase1=bbasis(i1)
    ibase(:)=ibasis(i1,:)
    !write(*,*) '  ************************** '
    !write(*,*) 'Starting bbasis',bbase1
    !write(*,*) 'Starting ibase',ibase
    !call write_bits(bbase1,working_ns)
    !write(*,*) 'Starting Bits set',bists_set(bbase1,working_ns)
    !!Compute and list all the possible hoppings
    hoppings=0
    hopping_list=0
    
    
    !!Fist we have no hopping at all
    hoppings=1
    hopping_list(1)=bbase1
    !write(*,*) '-.-.-.-.start hopping-.-.-.-'
    do e1=1,(nele-1)
       do e2=(e1+1),nele
          do steps=1,(working_ns-1)
             !write(*,*) 'Hop electron',e1,'and',e2,'by',steps,'steps'
             !call write_bits(bbase1,working_ns)
             bbase2=ibclr(bbase1,ibase(e1)) !!Remove el-1
             !call write_bits(bbase2,working_ns)
             bbase2=ibclr(bbase2,ibase(e2)) !!Remove el-2
             !call write_bits(bbase2,working_ns)
             bbase2=ibset(bbase2,modulo(ibase(e1)+steps,working_ns))!!Hop el-1
             !call write_bits(bbase2,working_ns)
             bbase2=ibset(bbase2,modulo(ibase(e2)-steps,working_ns))!!Hop el-2
             !call write_bits(bbase2,working_ns)
             !write(*,*) 'Bits set',bists_set(bbase2,working_ns)
             if(bists_set(bbase2,working_ns).eq.Nele)then
                !write(*,*) 'OK hoppnig',bbase2
                !call write_bits(bbase2,working_ns)
                hoppings=hoppings+1
                hopping_list(hoppings)=bbase2
             end if
          end do
       end do
    end do
    !write(*,*) 'All the',hoppings,'hopping elements'
    !write(*,*) hopping_list(1:hoppings)
    
    sort_hopping=0    
    call sort_long_int(hopping_list(1:hoppings),sort_hopping(1:hoppings)&
         ,order_hopping(1:hoppings),hoppings)
    !write(*,*) 'All the',hoppings,'ordered hopping elements'
    !write(*,*) sort_hopping(1:hoppings)
    
    unique_hopping=0    
    call unique_long_int(sort_hopping(1:hoppings),hoppings&
         ,unique_hopping(1:hoppings),unique_hoppings)
    !write(*,*) 'All the',unique_hoppings,'unique hopping elements'
    !write(*,*) unique_hopping(1:unique_hoppings)
    
    !!Do reverse lookup to find the basis numbers
    basis_order=0
    sorted_basis=0
    !write(*,*) '---------------'
    do i2=1,unique_hoppings
       !write(*,*) 'bit no',unique_hopping(i2)
       !call write_bits(unique_hopping(i2),working_ns)
       rbase1=reverse_bits(unique_hopping(i2),working_ns)
       !call write_bits(rbase1,working_ns)
       basis_order(i2)=lookup_basis_num(rbase1)
       !write(*,*) 'Basis num',basis_order(i2)
    end do
    !write(*,*) '---------------'
    
    !!Sort the obtained sereves list
    call sort_int(basis_order(1:unique_hoppings)&
         ,sorted_basis(1:unique_hoppings)&
         ,order_hopping(1:unique_hoppings),unique_hoppings)
 end if
  end subroutine compute_hopping_list
  
  integer function shift_modulo(A,M)
    integer, intent(IN) :: M
    integer, intent(IN) :: A
    !!Same as module exept that 1<= A <= M
    !! instead of 0<= A <= M
    shift_modulo=modulo(A-1,M)+1
  end function shift_modulo
  
  subroutine save_matrix_element(hs,Matrix_Element,To_Basis_State)
    USE sparse
    complex(kind=dpc), intent(IN) :: Matrix_Element
    integer, intent(IN) :: To_Basis_State
    TYPE(spa), INTENT(INOUT) :: hs   ! hamiltonian matrix
    
    !write(*,*) 'Trying element',To_Basis_State
    IF (ABS(Matrix_Element)>0.d0) THEN ! Only non-zeros are stored.
       !write(*,*) '  Saving element',To_Basis_State
       !call write_bits(bbasis(To_Basis_State),working_ns)
       
       hs%nz=hs%nz+1 !step up number of non-zero
       hs%a(hs%nz)=Matrix_Element !store value
       hs%ja(hs%nz)=To_Basis_State !store ja_value
    END IF
  end subroutine save_matrix_element
  
  ! Two-particle matrix elements:
  ! ********************************
  
  FUNCTION O2rdiag_elem(c, O2) RESULT(elem)
    complex(KIND=dpc) :: elem
    INTEGER, INTENT(IN) :: c
    INTERFACE !!O2 is the complex tw-body-element
       FUNCTION O2(ix,jx,kx,lx)
         USE typedef
         IMPLICIT NONE
         INTEGER, INTENT(IN) :: ix,jx,kx,lx
         complex(KIND=dp) :: O2
       END FUNCTION O2
    END INTERFACE
    INTEGER :: i1,i2
    
    elem = 0.d0
    
    DO i1=1, Nele
       DO i2=i1+1, Nele
          ! MOD specific to translation invariant systems
          elem = elem + O2(ibasis(c,i1),ibasis(c,i2),ibasis(c,i1),ibasis(c,i2))
          ! the exchange is included in O2
       END DO
    END DO
  END FUNCTION O2rdiag_elem
  
  FUNCTION O2roff_diag_elem(c1,c2,O2) RESULT(elem)
    INTEGER, INTENT(IN) :: c1, c2
    complex(dpc) :: elem
    INTERFACE !!O2 is the complex twp-body matrix element
       FUNCTION O2(ix,jx,kx,lx)
         USE typedef
         IMPLICIT NONE
         INTEGER, INTENT(IN) :: ix,jx,kx,lx
         complex(dpc) :: O2
       END FUNCTION O2
    END INTERFACE
    INTEGER(KIND=8) :: conf1, conf2, same, diff1, diff2
    !LOGICAL, DIMENSION(0:N1-1) :: conf1, conf2, same, diff1, diff2
    INTEGER :: p1(2), p2(2), fack, k, pcnt, oc(Nele-1)
    
    conf1 = bbasis(c1)
    conf2 = bbasis(c2)
    
    same = IAND(conf1, conf2)
    diff1 =  IEOR(conf1,same)
    pcnt = POPCNT(diff1)
    
    elem=0.d0
    
    IF (pcnt == 1) THEN
       diff2 =  IEOR(conf2, same)
       !Determine occupied states
       CALL bocs( same, oc )
       CALL bpos1( same, diff1, diff2, p1, fack )
       DO k = 1, SIZE(oc)
          !MOD specific to transl.inv
          ! USE ONLY FOR THE HAMILTONIAN MATRIX ELEMS
          elem = elem+(O2( p1(1),oc(k),p1(2),oc(k)))
          !          elem = elem+(O2( p1(1),oc(k),p1(2),oc(k))&
          !               -O2( p1(1),oc(k),oc(k),p1(2)))
       END DO
       elem = fack*elem 
    ELSEIF (pcnt==2) THEN
       diff2 =  IEOR(conf2, same)
       CALL bpos2( same, diff1, diff2, p1, p2, fack )
       ! MOD
       elem=fack*(O2(p1(1),p1(2),p2(1),p2(2)))
       !       elem=fack*(O2(p1(1),p1(2),p2(1),p2(2))&
       !          &-O2(p1(1),p1(2),p2(2),p2(1)))
    END IF
  END FUNCTION O2roff_diag_elem
  
  SUBROUTINE bocs( cf, ioc )
    ! returns an integer array ioc with the occupations of the Fock state cf
    ! Note: we don't really need this in the ibasis formulation, since
    ! the occupations are directly stored.
    INTEGER, INTENT(OUT) :: ioc(:)
    INTEGER(KIND=8), INTENT(IN) :: cf
    INTEGER :: u, v
    u = 0
    v = 0
    DO WHILE( u < Nele )
       IF ( BTEST(cf,v) ) THEN
          u = u+1
          ioc(u) = v
       END IF
       v = v+1
    END DO
  END SUBROUTINE bocs
  
  SUBROUTINE bpos1( same, diff1, diff2, ipos1, sgn )
    INTEGER, INTENT(OUT) :: ipos1(2), sgn
    INTEGER(KIND=8), INTENT(IN) :: same, diff1, diff2
    INTEGER :: ii,jj
    
    jj=1
    DO ii=0, working_ns-1
       IF (BTEST(diff1,ii) .OR. BTEST(diff2,ii)) THEN
          ipos1(jj)=ii
          jj=jj+1
          IF (jj==3) EXIT
       END IF
    END DO
    sgn=1
    IF ( BTEST(COUNT( (/ (BTEST(same,jj), jj=ipos1(1), ipos1(2)) /) ),0) )&
         & sgn=-sgn
  END SUBROUTINE bpos1
  
  SUBROUTINE bpos2( same, diff1, diff2, ipos1, ipos2, sgn )
    INTEGER, INTENT(OUT) :: ipos1(2), ipos2(2), sgn
    INTEGER(KIND=8), INTENT(IN) :: same, diff1, diff2
    INTEGER :: ii,jj
    
    jj=1
    DO ii=0, working_ns-1
       IF (BTEST(diff1,ii)) THEN
          ipos1(jj)=ii
          jj=jj+1
          IF (jj==3) EXIT
       END IF
    END DO
    jj=1
    DO ii=0, working_ns-1
       IF (BTEST(diff2,ii)) THEN
          ipos2(jj)=ii
          jj=jj+1
          IF (jj==3) EXIT
       END IF
    END DO
    sgn=1
    IF ( BTEST( COUNT( (/ (BTEST(same,jj), jj=ipos1(1), ipos1(2)) /) ),0) )&
         & sgn=-sgn
    IF ( BTEST( COUNT( (/ (BTEST(same,jj), jj=ipos2(1), ipos2(2)) /) ),0) )&
         & sgn=-sgn
  END SUBROUTINE bpos2
  
  !FUNCTION POPCNT(i) RESULT(res)
  !  INTEGER(8), INTENT(IN) :: i
  ! intel compiler has this as intrinsic. gfortran does not... :(
  ! MICKE: my gfortran has
  ! INTEGER :: res, jj
  ! res = COUNT( (/  (BTEST(i,jj),jj=0,BIT_SIZE(i)-1) /) )
  !END FUNCTION POPCNT
  
END MODULE hamiltonian
