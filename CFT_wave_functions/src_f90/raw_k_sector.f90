program raw_k_sector
  !! A mini program that only returns the raw k_sector for a given kminus matrix and cells size
  
  use input_misc
  use k_matrix
  
  implicit none
  !!Global variables
  integer :: Groups,q_group,p_group,cells,Ns,The_K_sector
  integer, allocatable, Dimension(:,:) :: Kminus
    
  logical::set_groups,set_kminus,set_cells
  
  !!Set all the flags to negative
  set_groups=.FALSE.
  set_kminus=.FALSE.
  set_cells=.FALSE.
 
  
  call retreive_input()
  call check_kmatrix_consistency()
  call compute_k_sector()
  
  
contains
  
  subroutine retreive_input()
    integer::narg,cptArg
    character(len=100)::name
    !!Number of inputs to skip
    integer::skip_next
    
    !!Initally this sytem will barf (in an uncontrolled way) if the input is wrong
    
    !Check if arguments are found
    narg=command_argument_count()
    
    skip_next=0
    if(narg>0)then
       !loop across options
       do cptArg=1,narg
          if(skip_next.gt.0)then
             !!Skip the next skip_next arguments
             !write(*,*) 'skip an argument'
             skip_next=skip_next-1
             cycle
          end if
          !!Get the comand argument
          call get_command_argument(cptArg,name)
          !!Select from all the caes
          select case(adjustl(name))
             !!First we test against flags
          case("--version")
             write(*,*) "This is program K_matrix_stat : Version 0.1"
             call exit(0)
          case("--help","-h")
             call print_help()
             call exit(0)
          case("-D","--groups")
             call set_unset_flag(set_groups,'-D',print_help)
             call recieve_integer_input(groups,'groups',cptArg+1)
             call check_positive_integer(groups,'-D',print_help)
             !!Allocate all the K-matrices
             allocate(Kminus(groups,groups))
             skip_next=1
          case("--Kminus","--K-minus","--kminus","--k-minus",&
               "--kappa","--kappa_matrix","--k-matrix","--kappa-matrix","-K")
             if(set_groups)then 
                call import_set_matrix(set_kminus,'--kappa_matrix'&
                     ,Kminus,cptArg,'kappa-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--Kminus','-D',print_help)
             end if
          case("-c","--cells")
             call set_unset_flag(set_cells,'-c',print_help)
             call recieve_integer_input(cells,'Cells',cptArg+1)
             call check_positive_integer(cells,'-c',print_help)
             skip_next=1
          case default
             call print_help()
             write(stderr,*)"ERROR: Input"
             write(stderr,*)"Option: ",name," at position",cptArg,"is unknown"
             write(*,*)
             call exit(-2)
          end select
       end do
    endif
    
  end subroutine retreive_input
  
  
  subroutine print_help()
    write(*,'(A)') 'Usage: raw_k_sector [OPTION]...'
    write(*,'(A)') 'Compute the raw_k_sector of a k-matrix'
    write(*,*) ''
    write(*,*) '------- kappa-matrix specifics -------'
    write(*,*) '                            All of the bellow flags need to be given -D --kappa_matrix -c'
    write(*,*) ''
    write(*,*) ' -D,  --groups  <D>         Dimention of the kappa-matrix,'
    write(*,*) '                            equals the number of groups.'
    write(*,*) ' --kappa_matrix <k11 ... kDD>    Entries to the K--matrix'
    write(*,*) ''
    write(*,*) ' -c, --cells                The number of unit cells'
    write(*,*)
    write(*,*) ' -h, --help                Print this help and exit' 
    write(*,*) ''
    write(*,*)
    write(*,'(A)') 'Report raw_k_sector bugs to fremling@fysik.su.se'
    write(*,*) 
  end subroutine print_help
  
  subroutine compute_k_sector()
    integer :: Pminus(Groups,Groups),ne
    call kappa_matrix_group_sizes(Groups,Kminus,pminus,p_group,q_group)
    Ns = q_group * cells
    Ne = sum(pminus*cells)
    the_k_sector=compute_raw_k_sector(Groups,Ne,Ns,Kminus,pminus,q_group)
    !!Print the k-sector
    write(*,*) the_k_sector
    
  end subroutine compute_k_sector
  
  subroutine check_kmatrix_consistency()
    if(set_Kminus)then !!The chiral piece is set...
       !!FIXME test symetric?
    else !! Nor kappa-matrix set
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'No kappa-matrix has been supplied, so nothing can be done'
       write(*,*)
       call print_help()
       call exit(-2)
    end if

    if(.not.set_cells)then !!No cells size given
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'No number of cells have been supplied, so nothing can be done'
       write(*,*)
       call print_help()
       call exit(-2)
    end if

  end subroutine check_kmatrix_consistency


end program Raw_k_sector
