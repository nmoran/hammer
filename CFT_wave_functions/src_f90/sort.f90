module sort
  use typedef

  implicit none
contains
  SUBROUTINE sort_dp (array_in, array_out,order_out, datasize)
    !!Compute the ordered array, using a simpel bubble sort
    !!Global Definitions
    INTEGER,intent(in) :: datasize
    REAL(kind=dp),intent(in)  :: array_in(datasize)
    REAL(kind=dp),intent(out) :: array_out(datasize)
    INTEGER,intent(out) :: order_out(datasize)
    integer:: y_temp,i
    !!Local
    REAL(kind=dp) :: x_temp
    LOGICAL :: inorder
    !!Set the data
    array_out=array_in
    do i=1,datasize
       order_out(i)=i
    end do
       
    inorder = .false.
    do while(inorder.eqv..false.)
       inorder = .true.       
       do i=1,(datasize-1)
          !!If x needs to be swapped, do so 
          if (array_out(i).gt.array_out(i+1) )then
             x_temp = array_out(i)
             y_temp = order_out(i)
             array_out(i) = array_out(i+1)
             order_out(i) = order_out(i+1)
             array_out(i+1) = x_temp
             order_out(i+1) = y_temp
             inorder = .false.
          end if
       end do
    end do
  END SUBROUTINE sort_dp


  SUBROUTINE sort_long_int (array_in, array_out,order_out, datasize)
    !!Compute the ordered array, using a simpel bubble sort
    !!Global Definitions
    INTEGER,intent(in) :: datasize
    integer(kind=8),intent(in) :: array_in(datasize)
    integer(kind=8),intent(out) :: array_out(datasize)
    INTEGER,intent(out) :: order_out(datasize)
    integer:: y_temp,i
    !!Local
    integer(kind=8) :: x_temp
    LOGICAL :: inorder
    !!Set the data
    array_out=array_in
    do i=1,datasize
       order_out(i)=i
    end do
    
    inorder = .false.
    do while(inorder.eqv..false.)
       inorder = .true.       
       do i=1,(datasize-1)
          !!If x needs to be swapped, do so 
          if (array_out(i).gt.array_out(i+1) )then
             x_temp = array_out(i)
             y_temp = order_out(i)
             array_out(i) = array_out(i+1)
             order_out(i) = order_out(i+1)
             array_out(i+1) = x_temp
             order_out(i+1) = y_temp
             inorder = .false.
          end if
       end do
    end do
  END SUBROUTINE sort_long_int


  SUBROUTINE sort_int (array_in, array_out,order_out, datasize)
    !!Compute the ordered array, using a simpel bubble sort
    !!Global Definitions
    INTEGER,intent(in) :: datasize
    integer,intent(in) :: array_in(datasize)
    integer,intent(out) :: array_out(datasize)
    INTEGER,intent(out) :: order_out(datasize)
    integer:: y_temp,i
    !!Local
    integer :: x_temp
    LOGICAL :: inorder
    !!Set the data
    array_out=array_in
    do i=1,datasize
       order_out(i)=i
    end do
    
    inorder = .false.
    do while(inorder.eqv..false.)
       inorder = .true.       
       do i=1,(datasize-1)
          !!If x needs to be swapped, do so 
          if (array_out(i).gt.array_out(i+1) )then
             x_temp = array_out(i)
             y_temp = order_out(i)
             array_out(i) = array_out(i+1)
             order_out(i) = order_out(i+1)
             array_out(i+1) = x_temp
             order_out(i+1) = y_temp
             inorder = .false.
          end if
       end do
    end do
  END SUBROUTINE sort_int

  

  SUBROUTINE unique_long_int (array_in, datasize,array_out,unique_out)
    !!Remove duplicates that follow each other
    !!NB: Only guarantees all elements are unique if the list is ordered
    !!Global Definitions
    INTEGER,intent(in) :: datasize
    integer(kind=8),intent(in) :: array_in(datasize)
    integer(kind=8),intent(out) :: array_out(datasize)
    !!A working coput of the array_in
    integer(kind=8) :: array_work(datasize)
    INTEGER,intent(out) :: unique_out
    integer:: i
    
    !!Local
    integer(kind=8) :: x_temp
    !!Set the data
    array_work=0

    
    unique_out=1    
    x_temp=array_in(1)
    array_work(1)=array_in(1)
    do i=2,datasize
       !write(*,*) 'array,ref',array_in(i),x_temp,i
       if(array_in(i).ne.x_temp)then
          unique_out=unique_out+1
          x_temp=array_in(i)
          array_work(unique_out)=array_in(i)
          !write(*,*) 'unique:',unique_out,i
       end if
    end do
    !!Set the output at the end (in case array in and array aout are the same matrix)
    array_out=array_work
  END SUBROUTINE unique_long_int
  



end module sort
