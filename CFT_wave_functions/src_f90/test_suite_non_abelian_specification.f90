program test_non_abelian_specification
  
  USE typedef     ! types and definitions
  use test_utilities
  use k_matrix
  use chiral_wave_function
  use center_of_mass
  use combinations
  use misc_random
  use permutations

  
  !!Variables
  IMPLICIT NONE  
  
    !!!Test starts here!!!
    write(*,*) '         Test the Chiral wave-functions'
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_many_body_T1_eigs
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_many_body_T2_q_eigs
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
contains  
  
  !!----------------------------------------------
  !!     Thest the manu body wave-functions
  !!---------------------------------------------


  subroutine set_matrix(kappa_matrix,kindx,D)
    integer, intent(IN) :: D,kindx
    integer, intent(OUT) :: kappa_matrix(D,D)
    if(kindx.eq.1)then
       kappa_matrix(1,1)=3
       kappa_matrix(1,2)=1
       kappa_matrix(2,1)=1
       kappa_matrix(2,2)=3
    elseif(kindx.eq.2)then
       kappa_matrix(1,1)=2
       kappa_matrix(1,2)=0
       kappa_matrix(2,1)=0
       kappa_matrix(2,2)=2
    elseif(kindx.eq.3)then
       kappa_matrix(1,1)=3
       kappa_matrix(1,2)=2
       kappa_matrix(2,1)=2
       kappa_matrix(2,2)=3
    else
       write(*,*) 'ERROR:Not more matrices!'
       call exit(-1)
    end if
  end subroutine set_matrix


  subroutine test_many_body_T1_eigs
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist
    integer :: q_group,q_nu,p_nu,cells,D,p_group,Ne,Ns
    integer :: eigval,k1_sector,k2_sector,n,det,kindx,delta,CoMindx
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,raw1_shift,raw2_shift,tau_x,tau_y
    
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that many body T1 and T2^q_group eigs are satisfied for non-abelian states'
    write(*,*) 'This is really a test of eqn (A35) in PRB 89, 125303 (2014) (generalized to non-abelian states)'
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    D=2
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(plist(D),nlist(D))
    !!Initialize the n-list 
    do kindx=1,3
       !!Initialize kappa-matrix (pfaffian matrix)
       call set_matrix(kappa_matrix,kindx,D)
       det=determinant_int_mat(kappa_matrix)
       call kappa_matrix_group_sizes(D,kappa_matrix,plist,p_group,q_group)          
       write(*,*) 'kappa-matrix'
       call print_int_matrix(kappa_matrix)
       write(*,*) 'p_group,q_group:',p_group,q_group
       call Filling_fraction(D,plist,q_group,p_nu,q_nu)
       write(*,*) 'p_group,q_group:',p_nu,q_nu
       delta=q_group/q_nu
       write(*,*) 'plist:',plist
       do cells=1,2 !!Loop over different number of fundamental cells
          Ns=q_group*cells
          Ne=p_group*cells
          write(*,*) 'Try for cells,Ne,Ns:',cells,Ne,Ns
          call initialize_wave_function(D,kappa_matrix,Ne,Ns,(/(0, n=1,D)/),(/(0, n=1,D)/),asym=.FALSE.)
          !!Initialize the cooridinates (use unit length in X).
          !!The length scale Lx is thus implicit.
          ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
          
          call get_first_nlist_entry(kappa_matrix,cells,nlist,D)
          do eigval=0,(det-1) !!Loop over the possible eigenvalues      
          do CoMindx=0,(delta-1) !!Loop over the possible CoMomenta
             write(*,*)
             write(*,*)
             write(*,*)
             write(*,*)
             write(*,*) 'nlist:',nlist
             write(*,*) 'CoMindx:',CoMindx
             call RANDOM_NUMBER(tau_x)
             call RANDOM_NUMBER(tau_y)
             !Put tau_x in range -1/2 < tau_x < 1/2
             tau_x=tau_x-.5
             !Put tau_y in range 1/2 < tau_x < 1+1/2
             tau_y=tau_y+.5
             tau=tau_x+iunit*tau_y
             !!tau=iunit
             call RANDOM_NUMBER(Xcoord)
             call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
             Xcoord=Xcoord-.5d0
             Ycoord=Ycoord-.5d0
             !!Ycoord=0.d0
             !!Xcoord=0.d0
             
             !!Set the boundary conditions to periodic
             k1_sector=get_k1(plist,cells,nlist,D,q_group)
             k2_sector=delta*get_k2(plist,cells,nlist,D,q_group,CoMindx)
             !write(*,*) 'TEST: k1_sector is',k1_sector
             call set_CM_BC(Ns,k1_sector,D,kappa_matrix,dual_index=nlist,CoMindx=CoMindx)
             !write(*,*) 'The raw1 shift is',raw_shift
             raw1_shift=real(k1_sector,dp)/Ns
             raw2_shift=real(k2_sector,dp)/(delta*cells)
             write(*,*) 'The raw1 shift*(2*pi) is',raw1_shift
             write(*,*) 'The raw2 shift*(2*pi) is',raw2_shift
             raw1_shift=2*pi*raw1_shift
             raw2_shift=2*pi*raw2_shift
             write(*,*) 'The raw1 shift is',raw1_shift
             write(*,*) 'The raw2 shift is',raw2_shift

             !!Compute first wave function
             psi=chiral_raw_psi(Xcoord,Ycoord,tau)
             
             !!Change all coordinates one step in the x-direction
             !! This tests that correct momentum sector is set
             Xcoord2=Xcoord+1.d0/Ns
             Ycoord2=Ycoord
             psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau)
             
             !!Change all coordinates q_group steps in the y-direction
             !!This tests the \tilde(q_group)-fold degeracy
             Xcoord2=Xcoord
             Ycoord2=Ycoord+real(q_group,dp)/Ns
             
             !!Compute the gaguge phase
             gauge_phase=sum(Xcoord2)*2*pi*q_group
             psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau)&
                     +iunit*gauge_phase !!Add the gauge phase
             !!Reduce to smallest mod_2pi
             psi_2x=mod_2pi(psi_2x-iunit*raw1_shift)
             psi_2y=mod_2pi(psi_2y-iunit*raw2_shift)
             
             !write(*,*) 'Transformations performed'

             !!Test the periodic boundary conditions             
             if(test_diff_exp_cmplx(psi,psi_2x,1.d-7)&
                  .or.&
                  test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                write(*,'(A,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                     ,'Change global coorinates for',Ne&
                     ,' particles and eigenvalue',eigval&
                     ,'.-.-.-.-.-.-'
                write(*,*) 'Not-periodic'
                write(*,*) 'For kappa-matrix'
                call print_int_matrix(kappa_matrix)
                write(*,*) 'With indx:',nlist
                write(*,*) 'With CoM:',CoMindx
                write(*,*) 'With k1:',k1_sector
                write(*,*) 'With T2^q_group:',k2_sector
                write(*,*) 'tau:',tau
                write(*,*) 'cells:',cells
                call print_the_psis(psi,psi_2x,psi_2y)
                write(*,*) 'Coordinates Z:'
                write(*,*) Xcoord+tau*Ycoord
                call exit(-2)
             end if
             call get_next_nlist_entry(kappa_matrix,plist,q_group,cells,nlist,D)
          end do !!!Next CoMomenta
          end do !!!Next states
          !!Delaocate the coordinates
          write(*,*) 'Deallocate'
          DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
       end do !!Next cell size
    
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
    
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist,nlist)
  end subroutine test_many_body_T1_eigs



  subroutine test_many_body_T2_q_eigs
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist
    integer :: q_group,q_nu,p_nu,cells,D,p_group,Ne,Ns
    integer :: eigval,k1_sector,k2_sector,n,det,kindx,delta,CoMindx
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,raw1_shift,raw2_shift,tau_x,tau_y
    
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that many body T2^q_nu eigs are satisfied for non-abelian states'
    write(*,*) 'This is really a test of eqn (A35) in PRB 89, 125303 (2014) (generalized to non-abelian states)'
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    D=2
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(plist(D),nlist(D))
    !!Initialize the n-list 
    do kindx=1,3
       !!Initialize kappa-matrix (pfaffian matrix)
       call set_matrix(kappa_matrix,kindx,D)
       det=determinant_int_mat(kappa_matrix)
       call kappa_matrix_group_sizes(D,kappa_matrix,plist,p_group,q_group)          
       write(*,*) 'kappa-matrix'
       call print_int_matrix(kappa_matrix)
       write(*,*) 'p_group,q_group:',p_group,q_group
       call Filling_fraction(D,plist,q_group,p_nu,q_nu)
       write(*,*) 'p_group,q_group:',p_nu,q_nu
       delta=q_group/q_nu
       write(*,*) 'plist:',plist
       do cells=1,2 !!Loop over different number of fundamental cells
          Ns=q_group*cells
          Ne=p_group*cells
          write(*,*) 'Try for cells,Ne,Ns:',cells,Ne,Ns
          call initialize_wave_function(D,kappa_matrix,Ne,Ns,(/(0, n=1,D)/),(/(0, n=1,D)/),asym=.FALSE.)
          !!Initialize the cooridinates (use unit length in X).
          !!The length scale Lx is thus implicit.
          ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
          
          call get_first_nlist_entry(kappa_matrix,cells,nlist,D)
          do eigval=0,(det-1) !!Loop over the possible eigenvalues      
          do CoMindx=0,(delta-1) !!Loop over the possible CoMomenta
             write(*,*)
             write(*,*)
             write(*,*)
             write(*,*)
             write(*,*) 'nlist:',nlist
             write(*,*) 'CoMindx:',CoMindx
             call RANDOM_NUMBER(tau_x)
             call RANDOM_NUMBER(tau_y)
             !Put tau_x in range -1/2 < tau_x < 1/2
             tau_x=tau_x-.5
             !Put tau_y in range 1/2 < tau_x < 1+1/2
             tau_y=tau_y+.5
             tau=tau_x+iunit*tau_y
             !!tau=iunit
             call RANDOM_NUMBER(Xcoord)
             call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
             Xcoord=Xcoord-.5d0
             Ycoord=Ycoord-.5d0
             !!Ycoord=0.d0
             !!Xcoord=0.d0
             
             !!Set the boundary conditions to periodic
             k1_sector=get_k1(plist,cells,nlist,D,q_group)
             k2_sector=get_k2(plist,cells,nlist,D,q_group,CoMindx)
             !write(*,*) 'TEST: k1_sector is',k1_sector
             call set_CM_BC(Ns,k1_sector,D,kappa_matrix,dual_index=nlist,CoMindx=CoMindx)
             !write(*,*) 'The raw1 shift is',raw_shift
             raw1_shift=real(k1_sector,dp)/Ns
             raw2_shift=real(k2_sector,dp)/(delta*cells)
             write(*,*) 'The raw1 shift*(2*pi) is',raw1_shift
             write(*,*) 'The raw2 shift*(2*pi) is',raw2_shift
             raw1_shift=2*pi*raw1_shift
             raw2_shift=2*pi*raw2_shift
             write(*,*) 'The raw1 shift is',raw1_shift
             write(*,*) 'The raw2 shift is',raw2_shift

             !!Compute first wave function
             psi=chiral_raw_psi(Xcoord,Ycoord,tau)
             
             !!Change all coordinates one step in the x-direction
             !! This tests that correct momentum sector is set
             Xcoord2=Xcoord+1.d0/Ns
             Ycoord2=Ycoord
             psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau)
             
             !!Change all coordinates q_group steps in the y-direction
             !!This tests the \tilde(q_group)-fold degeracy
             Xcoord2=Xcoord
             Ycoord2=Ycoord+real(q_nu,dp)/Ns
             
             !!Compute the gaguge phase
             gauge_phase=sum(Xcoord2)*2*pi*q_nu
             psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau)&
                     +iunit*gauge_phase !!Add the gauge phase
             !!Reduce to smallest mod_2pi
             psi_2x=mod_2pi(psi_2x-iunit*raw1_shift)
             psi_2y=mod_2pi(psi_2y-iunit*raw2_shift)
             
             !write(*,*) 'Transformations performed'

             !!Test the periodic boundary conditions             
             if(test_diff_exp_cmplx(psi,psi_2x,1.d-7)&
                  .or.&
                  test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                write(*,'(A,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                     ,'Change global coorinates for',Ne&
                     ,' particles and eigenvalue',eigval&
                     ,'.-.-.-.-.-.-'
                write(*,*) 'Not-periodic'
                write(*,*) 'For kappa-matrix'
                call print_int_matrix(kappa_matrix)
                write(*,*) 'With indx:',nlist
                write(*,*) 'With CoM:',CoMindx
                write(*,*) 'With k1:',k1_sector
                write(*,*) 'With T2^q_nu:',k2_sector
                write(*,*) 'tau:',tau
                write(*,*) 'cells:',cells
                call print_the_psis(psi,psi_2x,psi_2y)
                write(*,*) 'Coordinates Z:'
                write(*,*) Xcoord+tau*Ycoord
                call exit(-2)
             end if
             call get_next_nlist_entry(kappa_matrix,plist,q_group,cells,nlist,D)
          end do !!!Next CoMomenta
          end do !!!Next states
          !!Delaocate the coordinates
          write(*,*) 'Deallocate'
          DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
       end do !!Next cell size
    
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
    
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist,nlist)
  end subroutine test_many_body_T2_q_eigs

  

  !!----------------------------------------------------
  !!                  MISC
  !!....................................................
  
  subroutine print_the_psis(psi,psi_2x,psi_2y)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-x:',mod_2pi(psi_2x)
    write(*,*) 'psi-y:',mod_2pi(psi_2y)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi):',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi):',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_psis

  subroutine print_the_1psis(psi,psi_2)
    complex(kind=dpc), intent(in) :: psi,psi_2
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-2:',mod_2pi(psi_2)
    write(*,*) 
    write(*,*) 'diff/(2*pi):',(mod_2pi(psi_2)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_1psis


  subroutine print_the_3psis(psi,psi_2x,psi_2y,psi_2xy)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y,psi_2xy
    write(*,*) 'psi  : ',mod_2pi(psi)
    write(*,*) 'psi-x: ',mod_2pi(psi_2x)
    write(*,*) 'psi-y: ',mod_2pi(psi_2y)
    write(*,*) 'psi-xy:',mod_2pi(psi_2xy)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi): ',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi): ',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-xy/(2*pi):',(mod_2pi(psi_2xy)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_3psis

  
  logical function next_nlist(D,nlist,maxlist,minlist)
    integer, intent(IN) :: D,maxlist(D),minlist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_nlist=.FALSE.
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          !!Not-max (incement)
          nlist(n)=nlist(n)+1
          next_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
  end function next_nlist
  
  
  logical function next_parity_nlist(D,nlist,maxlist,minlist,inclist)
    integer, intent(IN) :: D,maxlist(D),minlist(D),inclist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_parity_nlist=.FALSE.
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          !!Not-max (incement)
          nlist(n)=nlist(n)+inclist(n)
          next_parity_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
  end function next_parity_nlist

  logical function same_group(N1,N2,plist,cells) result(same)
    !!!Tesst if N1 and N2 are fromt he same group
    integer, intent(in) :: N1,N2,plist(:), cells
    integer :: Nlist(size(plist)),G1,G2,n,D,Ntmp
    !!Initate G1,G2
    G1=0
    G2=0
    Nlist=plist*cells
    D=size(plist)
    Ntmp=0
    do n=1,D
       if(N1.gt.Ntmp.and.N1.le.(Ntmp+Nlist(n)))then
          G1=n !!This gets set eventually
       end if
       if(N2.gt.Ntmp.and.N2.le.(Ntmp+Nlist(n)))then
          G2=n !!This gets set eventually
       end if
       Ntmp=Ntmp+Nlist(n)
    end do
    !write(*,*) "N1,N2:",N1,N2
    !write(*,*) "G1,G2:",G1,G2
    same=(G1.eq.G2)
  end function same_group
  
end program test_non_abelian_specification

