module bits_manipulation

use typedef

implicit none

public

contains

  integer(kind=8) function list_to_bit(List,Size) result(Bit)
    integer, intent(IN) :: Size, List(Size)
    integer :: n

    !!List is assumed to enumerate from 1
    Bit=0
    do n=1,Size
       Bit=IBSET(Bit,List(n)-1)
    end do
  end function list_to_bit
    

integer function bists_set(Bit,Size)
    !!Count the number of bits set (upt to Size) in Bit
    integer(kind=8), intent(IN) :: Bit
    integer, intent(IN) :: Size
    integer :: n
    bists_set=0
    do n=0,(Size-1)
       if(btest(Bit,n))then
          bists_set=bists_set+1
       end if
    end do
  end function bists_set
  
  subroutine write_bits(Bit,Size)
    !!Print the bits in Bit ut to size Size.
    integer(kind=8), intent(IN) :: Bit
    integer, intent(IN) :: Size
    integer :: n
    do n=0,(Size-1)
       if(btest(Bit,n))then
          write(*,'(A)',advance='no') '1'
       else
          write(*,'(A)',advance='no') '0'
       end if
    end do
    write(*,*)
  end subroutine write_bits

  !!Reverse the bit order
  integer(kind=8) function reverse_bits(BitIN,Size) result(BitOUT)
    !!Reverses the order of bits
    integer(kind=8), intent(IN) :: BitIn
    integer, intent(IN) :: Size            
    integer :: n
    !write(*,*) 'Reverse bitnum', BitIN
    !call write_bits(BitIN,Size)
    BitOut=0
    !!call write_bits(BitOUT,Size)
    do n=0,(Size-1)
       if(btest(Bitin,n))then
          !write(*,*) 'Setting:',n
          BitOut=ibset(BitOut,Size-1-n)
          !call write_bits(BitOUT,Size)
       else
          BitOut=ibclr(BitOut,Size-1-n)
       end if
    end do
  end function reverse_bits


  integer function search_list_decending_integer8(Target_int,Int_List,List_Size) &
       result(position)
    !!Finds the number Target_int in the list List of size List_Size.
    !!This function assumes the list is already sorted in decending order
    !!Subdivision of intervall to search
    integer(kind=8), intent(in) ::Target_int,Int_List(List_Size)
    integer, intent(in) ::List_Size
    integer look_high,look_low,look_middle
    integer(kind=8) ::rmiddle,rlow,rhigh
    
    !write(*,*) '--.--.--SLDI--.--.--'
    !write(*,*) 'Looking for:',Target_int
    !write(*,*) 'In a list of size:',List_size
    !write(*,*) 'Target list is:',Int_List !!NB: Huge output

    !write(*,*) 'Basis size is', List_size
    look_high=List_size
    look_middle=nint(real(List_size,dp)/2)
    look_low=1
    
    !!Assumes the list is reverse ordered
    do
       !write(*,*) 'Looking for',Target_int
       !write(*,*) 'The look bounds are',look_low,look_middle,look_high
       rlow=Int_list(look_low)
       rmiddle=Int_list(look_middle)
       rhigh=Int_list(look_high)
       !write(*,*) 'Low,Middle,High is',rlow,rmiddle,rhigh
       if(Target_int.eq.rlow)then
          !write(*,*) 'Found it low'
          position=look_low
          return
       elseif(Target_int.eq.rmiddle)then
          !write(*,*) 'Found it middle'
          position=look_middle
          return
       elseif(Target_int.eq.rhigh)then
          !write(*,*) 'Found it high'
          position=look_high
          return
       elseif(Target_int.gt.rmiddle)then
          !write(*,*) 'Look bellow'
          if(look_middle.eq.look_high)then
             write(*,*) 'ERROR -- SLDI --'
             write(*,*) 'Intervall can NOT be further subdivided.'
             write(*,*) 'Element does not exist'
             call exit(-2)
          end if
          look_high=look_middle
          look_middle=nint(real(look_low+look_middle)/2)
       else
          !write(*,*) 'Look above'
          if(look_middle.eq.look_low)then
             write(*,*) 'ERROR -- SLDI --'
             write(*,*) 'Intervall can NOT be further subdivided.'
             write(*,*) 'Element does not exist'
             call exit(-2)
          end if
          look_low=look_middle
          look_middle=nint(real(look_high+look_middle)/2)
       end if
    end do
    
  end function search_list_decending_integer8



end module bits_manipulation
