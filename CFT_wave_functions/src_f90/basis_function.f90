MODULE basis_function

  use jacobitheta      !  theta functions
  use typedef
  use channels
  
  IMPLICIT NONE
  
  !!Global parameters usefull for this module
  integer :: Ns, Ne  !!NUmber of fluxes and electrons
  complex(kind=dpc) :: tau  !!Geometry parameter
  real(kind=dp) :: Lx,basis_norm !!Length scale and normalization
  logical :: T1Basis !!t1 och t2 basis
  
  complex(KIND=dpc), PRIVATE, DIMENSION(:), POINTER :: vect
  logical :: basis_wfn_initialized=.FALSE.
  
  private
  public init_ed_basis_geometry,test_initialized_basis
  public single_particle_matrix,basisfunction
  
CONTAINS
  
  
  SUBROUTINE init_ed_basis_geometry(Ns_in,Ne_in,tau_in,T1Basis_in)
    integer, intent(IN) :: Ns_in, Ne_in
    complex(kind=kind((1.0d0,1.0d0))), intent(IN) :: tau_in
    logical :: T1Basis_in
    
    Ne = Ne_in
    Ns = Ns_in
    tau = tau_in
    !!Length scale
    Lx = sqrt(2*pi*Ns/aimag(tau))
    T1Basis=T1Basis_in
    if(T1Basis)then
       basis_norm=1.0d0/sqrt(Lx*sqrt(pi))
    else !!T2Basis
       !!Add an extra factor compared to the t1-normalization
       basis_norm=1.0d0/sqrt(Ns*Lx*sqrt(pi))
    end if
    
    basis_wfn_initialized=.TRUE.
    
  END SUBROUTINE init_ed_basis_geometry

  function single_particle_matrix(xlist,ylist,Ns_in) result(psi_tab)
    REAL(KIND=kind(1.0d0)), DIMENSION(:), INTENT(IN) :: ylist, xlist
    integer, intent(IN) :: Ns_in
    complex(kind=kind((1.0d0,1.0d0))) :: psi_tab(size(xlist),0:(Ns_in-1))
    integer :: j,i
    !!Compute a matrix containing all the basis function values
    !!for all the input coordiantes
    !!This matrix can then be used to compute slater determinants

    !!Trivial input checking
    if(Ns.ne.Ns_in)then
       write(stderr,*) '----ep---'
       write(stderr,*) 'ERROR:'
       write(stderr,*) 'The size of Ns_in in non-compliant with in initialized value'
       call exit(-1)
    end if

    !!Trivial input checking
    if(size(Xlist).ne.size(Ylist))then
       write(stderr,*) '----ep---'
       write(stderr,*) 'ERROR:'
       write(stderr,*) 'The dimention of input vector X, differs from dimention of input vector Y'
       call exit(-1)
    end if
    
    call test_initialized_basis !!Check initialization
    !! tabulate rows that appear in all the determinants
    DO j = 0, Ns-1
       !! Tabulates the single particle wave functions
       do i=1,size(xlist)
          psi_tab(i,j) = basisfunction(j,xlist(i),ylist(i))
       end do
    END DO
  end function single_particle_matrix
  
  function basisfunction(n,x,y) result(res)
    complex(KIND=kind((1.0d0,1.0d0))) :: res
    REAL(KIND=kind(1.0d0)), INTENT(IN) :: x, y
    integer, intent(IN) :: n
    complex(KIND=dpc) theta,tau_gauge
    
    !! The basis functions \eta_n are implemeted such that 
    !!    \eta_{n+Ns} = \eta_n.
    !! This makes the states non-invariant under \tau -> \tau + 1

    call test_initialized_basis !!Check initialization


    !!Gaussian factor for the tau-gauge
    !!(needed to compare states at different tau)    
    tau_gauge=iunit*pi*Ns*tau*y**2
    
    if(T1Basis)then
       !!Generalized Theta function
       theta=logthetagen(1.d0*n/Ns,0.d0,Ns*(x+tau*y),Ns*tau)
    else 
       !!FIXME - check that the basis functions are correct,
       !!I.e. that n/Ns has the right sign compared to the V_ijkl
       !!Generalized Theta function
       theta=logthetagen(0.d0,-1.d0*n/Ns,x+tau*y,tau/Ns)
    end if
    res=exp(tau_gauge+theta)*basis_norm
  end function basisfunction

  subroutine test_initialized_basis
    if(.not.basis_wfn_initialized)then !!Check initialization
       write(stderr,*) 'ERROR: ED basis geometry not initialized'
       write(stderr,*) 'Call inite_ed_basis_geometry first'
       call exit(-1)
    end if
  end subroutine test_initialized_basis
  
  
END MODULE Basis_function
