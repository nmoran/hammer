module permutations
  !! Computes all the permutaions of n groups of length S1,S2,...Sn
  use channels
  use combinations
  use typedef

  implicit none

  integer, parameter :: max_loop=3
  
  Type permutation
     integer :: my_level,max_level,ToT,Rn,Sn,Tn
     logical :: status,active
     integer,pointer,dimension(:) :: SList,Rlist
     integer,pointer,dimension(:) :: SRange,RRange,TRange
     type(permutation), pointer :: sub_perm
     type(combination), pointer :: comb
  end type permutation
  
contains
  
  subroutine perm_initialize(P)
    type(permutation), intent(INOUT) :: P
    !! SOMETHING SHOULD PRABABLY BE DONE HERE, UNSURE OF WHAT
    P%active=.FALSE.
    NULLIFY(P%sub_perm)
  end subroutine perm_initialize
  
  logical function perm_ok(P) result(ok)
    !!Return the status of the permutation
    type(permutation), intent(IN) :: P
    ok=P%comb%ok
  end function perm_ok

  subroutine perm_create(P,SList)
    !!Create the first permutation,middle layer
    type(permutation), intent(INOUT) :: P
    integer, intent(IN) :: SList(:)
    integer :: Rlist(size(SList))
    !!write(*,*) 'Create permutations of groups:',SList
    call set_R_list(Rlist,SList)
    call do_perm_create(P,1,SList,Rlist,sum(Slist))
    !write(*,*) 'Create completed'
  end subroutine perm_create
  
  RECURSIVE subroutine do_perm_create(P,Current_Lev,SL_in,RL_in,ToT_in)
    !!Create the first permutation
    type(permutation), intent(INOUT) :: P
    integer, intent(IN) :: Current_Lev,SL_in(:),RL_in(:),ToT_in
    integer :: Target_Lev,i
    Target_Lev = size(SL_in)
    if(size(SL_in).ne.size(RL_in))then
       write(stderr,*) '----dpc-----'
       write(stderr,*) 'ERROR: SL and RL have differnt sizes'
    end if
    !!write(*,*) 'Do Create on level :',Current_Lev,'of',Target_Lev
    if(P%active)then
       write(stderr,*) 'ERROR: Permutation already created. Destroy first'
       call exit(-1)
    end if
    !!write(*,*) 'Initializing:',Current_Lev,Target_Lev
    P%my_level=Current_Lev
    P%max_level=Target_lev
    P%status=.TRUE.
    P%active=.TRUE. 
    !!Alocate group info
    allocate(P%SList(Target_lev))
    allocate(P%RList(Target_lev))
    allocate(P%comb)
    P%SList=SL_in
    P%Rlist=RL_in
    P%Sn=P%SList(P%my_level)
    P%Rn=P%RList(P%my_level)
    P%Tn=P%Sn+P%Rn

    !Write(*,*) 'Sn,Rn,Tn:',P%Sn,P%Rn,P%Tn
    !write(*,*) 'Allocate at level',current_Lev
    allocate(P%SRange(P%Sn))
    allocate(P%RRange(P%Rn))
    allocate(P%TRange(P%Tn))

    !write(*,*) 'Set range at level',current_Lev    
    P%ToT = ToT_in
    P%SRange = (/(i,i=P%ToT-P%Tn+1,P%ToT-P%Rn)/)
    P%RRange = (/(i,i=P%ToT-P%Rn+1,P%ToT)/)
    P%TRange = (/(i,i=P%ToT-P%Tn+1,P%ToT)/)
    
    !write(*,*) 'Create comb at level',current_Lev    
    call createCombination(P%comb)
    call initializeCombination(P%comb,P%Tn,P%Sn)  
    !!If we are just above the last level, there is no need to allocate
    !!for the last, since it is the identity
    if(P%my_level.lt.(P%max_level-1))then
       !!Allocate the next level
       allocate(P%sub_perm)
       call perm_initialize(P%sub_perm)!!Allocate the memory
       !!Set the memory recursively
       call do_perm_create(P%sub_perm,P%my_level+1,P%SList,P%RList,P%ToT)
    end if
    !!write(*,*) 'Done at level',current_Lev    
  end subroutine do_perm_create

  subroutine perm_print(P)
    type(permutation), intent(IN) :: P
    integer :: order(P%ToT) !!Output variable
    integer ::n
    !!write(*,*) 'write permutation:',P%Slist
    order = (/(n,n=1,P%ToT)/)
    call do_perm_get_order(P,order)
    call write_compactly(order)
    !!write(*,*) 'write done:'
  end subroutine perm_print
  
  function perm_get_order(P) result(order)
    type(permutation), intent(IN) :: P
    integer :: order(P%ToT) !!Output variable
    integer ::n
    !!write(*,*) 'write permutation:',P%Slist
    order = (/(n,n=1,P%ToT)/)
    call do_perm_get_order(P,order)
    !!call write_compactly(order)
    !!write(*,*) 'write done:'
  end function perm_get_order

  function perm_get_parity(P) result(ord_par)
    type(permutation), intent(IN) :: P
    integer :: ord_par !!Output variable
    ord_par = 0 !!Even parity at the start
    call do_perm_get_parity(P,ord_par)
  end function perm_get_parity

  RECURSIVE subroutine do_perm_get_order(P,Ordering)
    type(permutation), intent(IN) :: P
    integer,intent(INOUT) :: Ordering(:)
    integer :: Sorder(P%Sn),Rorder(P%Rn)
    !!Extract the combinations
    Sorder = P%comb%C
    Rorder = Pcomp2(P%Tn,P%comb%C)
    !write(*,*) 'Sorder:',Sorder
    !write(*,*) 'Rorder:',Rorder
    !!Permute the indexes
    Ordering(P%TRange) = Ordering(P%TRange(concat_array(Sorder,Rorder)))
    if(P%my_level.lt.(P%max_level-1))then 
       !!If the are levels further down, look at them
       call do_perm_get_order(P%sub_perm,Ordering)
    end if
  end subroutine do_perm_get_order


  RECURSIVE subroutine do_perm_get_parity(P,Ord_par)
    type(permutation), intent(IN) :: P
    integer,intent(INOUT) :: Ord_par
    integer :: Sorder(P%Sn)
    !!Parity at  max_level=1 is the trivail one
    if(P%max_level.gt.1)then 
       !!Extract the combinations
       Sorder = P%comb%C
       !write(*,*) 'Sorder:',Sorder
       !!Compute new partity
       ord_par=modulo(ord_par+SUM(Sorder),2)
       !!Do not look at the parity att the bottom level
       if(P%my_level.lt.(P%max_level-1))then 
          !!If the are levels further down, look at them
          call do_perm_get_parity(P%sub_perm,ord_Par)
       end if
    end if
  end subroutine do_perm_get_parity
  

  RECURSIVE subroutine perm_next(P)
    type(permutation), intent(INOUT) :: P
    !!The last level is really the next to last level
    !!write(*,*) 'Next at level:',P%my_level
    if(P%my_level.eq.(P%max_level-1))then
       !!Step up the current counter
       CALL nextCombination(P%Comb)
    elseif(P%my_level.eq.P%max_level)then 
       !!Should only happen when max_level=1
       !!Step up the current counter
       CALL nextCombination(P%Comb)
    else
       !!write(*,*) 'Go down a level'
       !!Loop through the permutation one level down
       !! call perm_print(P%sub_perm) 
       call perm_next(P%sub_perm)
       if(.not.perm_ok(P%sub_perm))then !If it is not ok
          !!write(*,*) 'Perm has reached its final state'
          !!Step up the current counter
          CALL nextCombination(P%Comb)
          !!reset the old
          call perm_reset(P%sub_perm)
       end if
    end if
  end subroutine perm_next
  

  RECURSIVE subroutine perm_reset(P)
    !!!Reset the remutation instead of destroying it!
    !!Most of the data is the same anyway
    type(permutation), intent(INOUT) :: P
    call destroyCombination(P%comb)
    call initializeCombination(P%comb,P%Tn,P%Sn)  
    !!If we are just above the last level, there is no need to allocate
    !!for the last, since it is the identity
    if(P%my_level.lt.(P%max_level-1))then
       !!Reset the next level
       call perm_reset(P%sub_perm)!!Reset the permutation
    end if
    !!write(*,*) 'Done at level',current_Lev    



  end subroutine perm_reset


  RECURSIVE subroutine perm_destroy(P)
    !!Clean up after the permuations
    !!Need to destroy combinations lower down, to avoid memory leaks
    type(permutation), intent(INOUT) :: P
    !!write(*,*) 'Destroy at level',P%my_level,'of',P%max_level
    P%status=.false.
    P%active=.False. 
    if(P%my_level.lt.(P%max_level-1))then
       call perm_destroy(P%sub_perm)  !!destory the perutations recursively
       deallocate(P%sub_perm) !!Delete them to
    end if
    call destroyCombination(P%comb) !!destroy the local permutmation
    deallocate(P%SList,P%RList,P%comb,P%Srange,P%Rrange,P%Trange)
  end subroutine perm_destroy
  
  subroutine set_R_list(Rlist,Slist)
    !!Assumes size(SList) = size(Rlist)
    integer, intent(In) :: Slist(:)
    integer, intent(OUT) :: Rlist(:)
    integer :: n,tot,levs
    levs=size(Slist)
    tot=sum(Slist)
    Rlist(levs)=0
    do n=1,(levs-1)
       Rlist(levs-n)=Rlist(levs-n+1)+Slist(levs-n+1)
    end do
    !write(*,*) 'Slist:',Slist
    !write(*,*) 'Rlist:',Rlist
  end subroutine set_R_list
  

  function Pcomp2(Elements, c1) result(c2)
    INTEGER, INTENT(IN) :: c1(:),Elements
    INTEGER :: c2(Elements-size(c1))
    integer :: nums(Elements),k
    ! Returns in c2 a complement of the combination c1
    LOGICAL :: mask(Elements)
    nums = (/(k, k=1,Elements)/)
    mask = .TRUE.
    mask(c1) = .FALSE.
    c2 = PACK(nums,mask)
  end function Pcomp2
  
  subroutine  Pcomp(Size, c1, c2 )
    INTEGER, DIMENSION(:), INTENT(IN) :: c1
    INTEGER, DIMENSION(:), INTENT(OUT) :: c2
    integer, intent(IN) :: Size
    integer :: nums(Size),k
    ! Returns in c2 a complement of the combination c1
    LOGICAL :: mask(Size)
    nums = (/(k, k=1,Size)/)
    mask = .TRUE.
    mask(c1) = .FALSE.
    c2 = PACK(nums,mask)
  END SUBROUTINE Pcomp


  subroutine write_compactly(int_list)
    integer, intent(IN) :: int_list(:)
    integer :: n,D
    D=size(int_list)
    do n=1,D
       if(n.ne.D)then
          write(*,'(I3)',advance='no') int_list(n)
       else
          write(*,'(I3)') int_list(n)
       end if
    end do
  end subroutine write_compactly


  integer function multinom(Slist) result(res)
    integer, intent(in) :: SList(:)
    integer :: Rlist(size(SList)),ToT,n
    ToT=sum(Slist)
    call set_R_list(Rlist,Slist)
    !!Compute the number of multinomials
    if(size(SList).eq.1)then
       res=1
    else !!Add up the partial multinomials !!
       res=binom(ToT,Slist(1))
       do n=2,(size(SList)-1)
          res = res * binom(Rlist(n-1),Slist(n))
       end do
    end if
  end function multinom


  real(kind=dp) function lnmultinom(Slist) result(res)
    integer, intent(in) :: SList(:)
    integer :: Rlist(size(SList)),ToT,n
    ToT=sum(Slist)
    call set_R_list(Rlist,Slist)
    !!Compute the number of multinomials
    if(size(SList).eq.1)then
       res=0d0
    else !!Add up the partial multinomials !!
       res=lnbinom(ToT,Slist(1))
       do n=2,(size(SList)-1)
          res = res + lnbinom(Rlist(n-1),Slist(n))
       end do
    end if
  end function lnmultinom


  real(kind=dp)  function lnfactorial(n) result(lnfac)
    !!Computes logarithmic factorial
    integer,intent(in) :: n
    integer k
    if(n.le.0)then
       write(stderr,*) 'ERROR: n is to small'
       call exit(-1)
    elseif(n.eq.1)then
       lnfac=0
    else
       lnfac=0
       do k=2,n
          lnfac = lnfac + log(1.0*k)
       end do
    end if
  end function lnfactorial


  real(kind=dp) function lnbinom(n,k) result(res)
    integer, intent(in) :: n,k
    if(k.gt.n)then
       write(stderr,*) 'ERRROR: k is larger than n'
       call exit(-1)
    end if
    res=lnfactorial(n)-lnfactorial(n-k)-lnfactorial(k)
    !!Round off to nearest integer
  end function lnbinom
  
  integer function binom(n,k) result(res)
    integer, intent(in) :: n,k
    if(k.gt.n)then
       write(stderr,*) 'ERRROR: k is larger than n'
       call exit(-1)
    end if
    res=nint(exp(lnfactorial(n)-lnfactorial(n-k)-lnfactorial(k)))
    !!Round off to nearest integer
  end function binom

  function concat_array(A1,A2) result(B)
    integer, intent(IN) :: A1(:),A2(:)
    integer :: B(size(A1)+size(A2))
    integer :: n,S1,S2
    S1=size(A1)
    S2=size(A2)
    do n=1,S1
       B(n)=A1(n)
    end do
    do n=1,S2
       B(S1+n)=A2(n)
    end do
  end function concat_array
  
  
  
end module permutations

