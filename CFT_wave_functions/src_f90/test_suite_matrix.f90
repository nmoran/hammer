program test_matrix
  
  USE typedef     ! types and definitions
  use misc_matrix
  use test_utilities
  
  
  !!Variables
  IMPLICIT NONE  
  
  !!Test starts here!!
    write(*,*) '         Test the misc matrix functions'
    !! Testing the misc matrix functions
    call test_int_determinant
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
contains 


  subroutine test_int_determinant()
    integer :: D
    
    write(*,*) 'Test that correct determinant can be computed'
    D=2    
    call do_test_int_determinant(reshape((/1,0,0,1/),(/D,D/)),1)
    call do_test_int_determinant(reshape((/0,1,1,0/),(/D,D/)),-1)
    call do_test_int_determinant(reshape((/3,2,2,3/),(/D,D/)),5)
    call do_test_int_determinant(reshape((/3,0,2,3/),(/D,D/)),9)
    call do_test_int_determinant(reshape((/0,2,2,0/),(/D,D/)),-4)
    call do_test_int_determinant(reshape((/3,1,1,3/),(/D,D/)),8)
    
    D=3    
    call do_test_int_determinant(reshape((/2,0,0,0,2,0,0,0,2/),(/D,D/)),8)
    call do_test_int_determinant(reshape((/0,2,2,2,0,2,2,2,0/),(/D,D/)),16)
    call do_test_int_determinant(reshape((/0,0,1,0,1,0,1,0,0/),(/D,D/)),-1)
    call do_test_int_determinant(reshape((/0,1,0,0,0,1,1,0,0/),(/D,D/)),1)
    call do_test_int_determinant(reshape((/3,1,3,1,3,5,3,5,-1/),(/D,D/)),-80)
    call do_test_int_determinant(reshape((/3,2,2,2,3,2,2,2,3/),(/D,D/)),7)

  end subroutine test_int_determinant
  
  subroutine do_test_int_determinant(Matrix,Det)
    integer, intent(IN) :: Matrix(:,:),Det
    integer :: Det_calc
    Det_calc=determinant_int_mat(matrix)
    if(Det.ne.Det_calc)then
       write(*,*) '.,.,.,.,tm:tid.,.,.,.,'
       write(*,*) 'ERROR:'
       write(*,*) 'The computed determinant=',Det_calc
       write(*,*) 'For Matrix'
       call print_int_matrix(matrix)
       write(*,*) 'does not match the expected determinant=',Det
       call exit(-1)
    end if
  end subroutine do_test_int_determinant
  
end program test_matrix
