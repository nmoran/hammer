program laughlin_coeffs

  use input_misc
  use channels
  use typedef
  use fock_coefficients
  implicit none
  
  !!Global variables
  integer ::Ne,Ns,Kvalue
  real(KIND=dp) ::tau_im,tau_re
  !!Flags to check if the global variables are set
  logical::set_Ne,set_Ns,set_Kvalue
  logical::set_I,set_R
    
  !!Derived quntities
  complex(kind=dpc) :: tau

  !!Torus specifics
  real(kind=dp), parameter :: I_default=1.d0,R_default=0.d0

  !!Set all the flags to negative
  set_Ne=.FALSE.
  set_Ns=.FALSE.
  set_Kvalue=.FALSE.
  set_I=.FALSE.
  set_R=.FALSE.
  
  call print_status()
  call retreive_input()
  call print_status()
  call compute_kmatrix_stat()
  call print_status()
  call create_laughlin_basis

contains
  
  subroutine print_status()
    write(*,*)
    write(*,*) 'The global (input) variables'
    call print_variable_status(set_Ne,Ne,'Ne')
    call print_variable_status(set_Ns,Ns,'Ns')
    call print_variable_status(set_Kvalue,Kvalue,'K')
    call print_float_status(set_I,tau_im,'I')
    call print_float_status(set_I,tau_re,'R')
  end subroutine print_status
  

  
!!!--------------------------------------------------
!!!        Reterive the input to the program
!!!--------------------------------------------------
  
  
  
  subroutine retreive_input()
    integer::narg,cptArg
    character(len=20)::name
    !!Number of inputs to skip
    integer::skip_next
    
    !!Initally this sytem will barf (in an uncontrolled way) if the input is wrong
    
    !Check if arguments are found
    narg=command_argument_count()
    
    skip_next=0
    if(narg>0)then
       !loop across options
       do cptArg=1,narg
          if(skip_next.gt.0)then
             !!Skip the next skip_next arguments
             !write(*,*) 'skip an argument'
             skip_next=skip_next-1
             cycle
          end if
          !!Get the comand argument
          call get_command_argument(cptArg,name)
          !!Select from all the caes
          select case(adjustl(name))
             !!First we test against flags
          case("--version")
             write(*,*) "This is program Laughlin_coeffs : Version 0.1"
             call exit(0)
          case("--help","-h")
             call print_help()
             call exit(0)
          case("-Ns","--fluxes")
             call set_unset_flag(set_Ns,'-Ns',print_help)
             call recieve_integer_input(Ns,'Ns',cptArg+1)
             call check_positive_integer(Ns,'-Ns',print_help)
             skip_next=1
          case("-Ne",'--particles')
             call set_unset_flag(set_Ne,'-Ne',print_help)
             call recieve_integer_input(Ne,'Ne',cptArg+1)
             call check_positive_integer(Ne,'-Ne',print_help)
             skip_next=1
          case("-K","--Kvalue")
             call set_unset_flag(set_Kvalue,'-K',print_help)
             call recieve_integer_input(Kvalue,'K',cptArg+1)
             skip_next=1
          case("-I","--tau_im")
             call set_unset_flag(set_I,'-I',print_help)
             call recieve_float_input(tau_im,'tau_im',cptArg+1)
             call check_positive_float(tau_im,'-I',print_help)
             skip_next=1
          case("-R","--tau_re")
             call set_unset_flag(set_R,'-R',print_help)
             call recieve_float_input(tau_re,'tau_re',cptArg+1)
             skip_next=1
          case default
             write(stderr,*)"ERROR: Input"
             write(stderr,*)"Option: ",name," unknown"
             write(*,*)
             call print_help()
             call exit(-2)
          end select
       end do
    endif
    
  end subroutine retreive_input

  subroutine print_help()
    write(*,'(A)') 'Usage: Laughlin_coeffs [OPTION]...'
    write(*,'(A)') 'Compute the coefficients of the Laughlin wave function.'
    write(*,*) '------- K--matrix -------'
    write(*,*) ' -K, --Kvalue  <Kval>     The power of the jastro factor.'
    write(*,*) '                          Also nu=1/Kval'
    write(*,*) ''
    write(*,*) '------- System size -------'
    write(*,*) '  Only one of -Ne and -Ns needs to be supplied.'
    write(*,*) '  If more than one flag is set, they will be compared'
    write(*,*) '  for internal consistency.'
    write(*,*) ''
    write(*,*) ' -Ne, --particles  <Ne>     Number of particles.'
    write(*,*) ' -Ns, --fluxes     <Ns>     Number of flux quanta = states.'
    write(*,*) ''
    write(*,*) '------- Torus Info -------'
    write(*,*) ''
    write(*,*) ' -I,  --tau_im      <I>     Imaginary part of tau.'
    write(*,*) '                            Default tau_im=',I_default
    write(*,*) ' -R,  --tau_re      <R>     Imaginary part of tau.'
    write(*,*) '                            Default tau_re=',R_default
    write(*,*) ''
    write(*,*) ''
    write(*,*) '  -h, --help                Print this help and exit' 
    write(*,*) ''
    write(*,'(A)') 'Report chiral_wfn bugs to fremling@fysik.su.se'
  end subroutine print_help
    

  subroutine compute_kmatrix_stat()
    integer :: local_Ns
    !!Make sure the kappa_matrix has been supplied
    if(.not.set_Kvalue)then
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'No kappa-matrix has been supplied, so nothing can be done'
       write(*,*)
       call print_help()
       call exit(-2)
    end if
        
    !!!Compute the System size
    if(set_Ne)then
       local_Ns=Kvalue*Ne
       call test_set_compability(set_Ns,Ns,local_Ns,'Ns','Ne',Ne,print_help)
    elseif(set_Ns)then !!FIXME is Ns is given instead of Ne
       !!! It has to be checked for compablility with the Kvalue
       write(stderr,*) 'ERROR: THIS OPTION IS NOT IMPLEMENTED'
       call exit(-2)
    else
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'One of the flags -Ns, -Ne must be supplied to give the system size.'
       write(*,*)
       call print_help()
       call exit(-2)
    end if



    !!Decide the value of tau
    if(.not.set_I)then
       write(stderr,*) 'WARNING: No tau_im have been specified.'
       write(stderr,*) '         Defaulting to tau_im=',I_default
       set_I=.TRUE.
       tau_im=I_default
    end if
    if(.not.set_R)then
       write(stderr,*) 'WARNING: No tau_re have been specified.'
       write(stderr,*) '         Defaulting to tau_re=',R_default
       set_R=.TRUE.
       tau_re=R_default
    end if
    !!Set tau
    tau=tau_re+iunit*tau_im
  end subroutine compute_kmatrix_stat


  subroutine create_laughlin_basis
    USE Combinations
    TYPE(Combination) :: Combo
    integer :: N,Target_Momentum,Combo_Momentum


    Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
    write(*,*) 'Target momentum is:',Target_Momentum

    CALL createCombination(Combo)    
    CALL initializeCombination(Combo,Ns,Ne)

    N=0
    DO ! First, let's find the number of basis states
       IF ( .NOT. Combo%ok ) EXIT
       ! This checks that the quantum numbers make up good states:
       
       Combo_Momentum=modulo(sum(Combo%C),Ns)
       IF ( Combo_Momentum .eq. Target_Momentum  ) THEN
          write(*,*) Combo%C
          N=N+1
       END IF
       CALL nextCombination(Combo)
    END DO
    WRITE(*,*) ' Many-body basis size is ', N
    WRITE(*,*) ' '
    
    

  end subroutine create_laughlin_basis

  
end program Laughlin_coeffs

