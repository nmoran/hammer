module K_matrix
  use misc_frac_sqrt
  use misc_matrix
  use channels
  
  !!This module contains method related to computing groupsizes
  !! and similar things from K-matrices
  
  implicit none
    
contains
  

  integer function compute_raw_K_sector(D,Ne,Ns_in,K_minus,plist,q,qps) result(K_sector)
    !!Computes the raw K_sector that is set to fix periodic bc
    !!This might not be the only sector avaiable, but its a start
    integer, intent(IN) :: D,Ns_in,Ne,K_minus(D,D), plist(D),q
    integer, intent(IN), optional :: qps(D) !!If there are qps
    integer :: Ns,n,cells,diff
    !!Here the raw K_sector really is 
    !! sum_a (Ns + K_aa)N_a/2
    !!Since it is the solution to Ns-K_aa = 2*\vec{h}*q_a
    !!put into the momentum \vec{h}*Q
    !!Works for both chiral and non-chiral vectors

    !!NB/FIXME: Adding localized qps breaks the translational invariance
    !! Thus K-sector is not a good meassure. Instead we compute the K-sector that we would have had if there where no qps present.
    !!FIXME: If the there are different number of qps in the different groups (This will need to be refined)
    if(present(qps))then
       Ns=Ns_in-qps(1)
    else
       Ns=Ns_in
    end if

    !!Some test that Ns,Ne,is compaticle qith q, and plist
    cells=Ns/q
    if(Ns.Ne.(cells*q))then
       write(*,*) '...crKs....'
       write(*,*) 'ERROR: The number Ns,q does not give an integer number of cells'
       write(*,*) 'Ns,q,cells:',Ns,q,cells
       call exit(-1)
    end if
    if(Ne.Ne.sum(cells*plist))then
       write(*,*) '...crKs....'
       write(*,*) 'ERROR: The number Ne,plist does not give an integer number of cells'
       write(*,*) 'Ne,cells,plist:',Ne,cells,plist
       call exit(-1)
    end if

    !!Compute the raw K-sector
    K_sector=0
    do n=1,D
       diff=(Ns+K_minus(n,n))*plist(n)*cells
       K_sector = K_sector + diff
    end do
    if(gcd(K_sector,2).ne.2)then
       write(*,*) '-----crKs----'
       write(*,*) 'The K_sector*2=',K_sector,'is not even.... problem!'
       call exit(-3)
    end if
    !!Divide by two
    K_sector = K_sector/2
    !!Reduce to a size apropirate the the nubmer of states Ns.
    K_sector = mod(K_sector,Ns)
    
  end function compute_raw_K_sector
  
  
  subroutine K_matrix_to_T_Q_R_list(kappa_matrix,Size,Tlist,Qlist,Rlist)
    !! Computes the T-, Q- and R- lists that characterise a chiral hierachy state.
    integer,  intent(IN) :: Size,kappa_matrix(Size,Size)
    integer  :: Qlist(0:Size),Rlist(Size),tlist(Size),n
    
    tlist(1)=kappa_matrix(1,1)-1
    Qlist(0)=1
    Qlist(1)=kappa_matrix(1,1)
    Rlist(1)=kappa_matrix(1,1)
    do n=2,Size
       tlist(n)=kappa_matrix(n,n)-kappa_matrix(n-1,n-1)+2
       Qlist(n)=tlist(n)*Qlist(n-1)-Qlist(n-2)
       Rlist(n)=Qlist(n)*Qlist(n-1)
    end do
    
  end subroutine K_matrix_to_T_Q_R_list
  
  subroutine Q_list_to_CmatTrans(Size,Qlist,CmatTrans)
    !!Computes the transpose of C-matrix, needed to compute the hierarchy CM-function
    !!The c-matrix are the integer pieces of the charge lattice
    integer,  intent(IN) :: Size,Qlist(0:Size)
    integer  :: CmatTrans(Size,Size),n,m
    CmatTrans=0
    do n=1,Size
       do m=1,n
          if(n.eq.m)then
             CmatTrans(m,n)=Qlist(m)
          else
             CmatTrans(m,n)=Qlist(m)-Qlist(m-1)
          end if
       end do
    end do
  end subroutine Q_list_to_CmatTrans
  
  subroutine Q_list_to_Lmatrix(Size,Qlist,Lmatrix)
    !!Computes the L_matrix,for a chiral hierarchy state,
    !!which is the integer pices of the quasi-particle charge lattice 
    integer,  intent(IN) :: Size,Qlist(0:Size)
    integer  :: Lmatrix(Size,Size),n,m
    Lmatrix=0
    do n=1,Size
       do m=n,Size
          if(n.eq.m)then
             Lmatrix(m,m)=Qlist(m-1)
          else
             Lmatrix(n,m)=Qlist(n-1)-Qlist(n)
          end if
       end do
    end do
  end subroutine Q_list_to_Lmatrix
  
  
  subroutine kappa_charge_lattice(Size,kappa_matrix,CMatrix,Rlist)
    !!Computes a charge lattive for a gereic kappa-matrix.
    !!The charge lattive is given in a lower traingular form
    integer,  intent(IN) :: Size,kappa_matrix(Size,Size)
    integer,  intent(OUT) :: Cmatrix(Size,Size),Rlist(Size)
    integer :: n,m,Rmatrix(Size,Size),nomin,denom,sqden

    !write(*,*)
    !write(*,*) '--------------------------------------'
    !write(*,*) 'Computing Charge lattice for kappa'
    !call print_int_matrix(kappa_matrix)
    !!write(*,*)
    if(Size.eq.1)then
       Cmatrix=kappa_matrix
       Rmatrix=kappa_matrix
       if(kappa_matrix(1,1).lt.1)then
          call charge_lattice_error()
       end if
    else
       Cmatrix=0
       Rmatrix=1
       !!write(*,*) 'Starting matrices'
       !!call print_charge_progres(Size,Cmatrix,Rmatrix)

       do n=1,Size
          do m=1,n
             !write(*,*) 'Element:',n,m
             if(n.eq.m)then !!The diagonal element
                if(n.eq.1)then
                   Cmatrix(1,n)=kappa_matrix(1,1)
                   Rmatrix(1,n)=kappa_matrix(1,1)
                else
                   call compute_euclidian_norm(n-1,Cmatrix(n,1:(n-1)),&
                        Rmatrix(n,1:(n-1)),nomin,denom)
                   !!Subtract the norm from the kappa-matrix entry
                   call add_fractions(kappa_matrix(n,n),1,-nomin,1*denom,nomin,denom)
                   !!Take the square root
                   if(nomin.eq.0)then
                      Cmatrix(n,n)=0
                      Rmatrix(n,n)=1
                   elseif(nomin.lt.0)then
                      call charge_lattice_error()
                   else
                      call do_reduce_sqrt_fraction(nomin,denom*nomin,&
                           Cmatrix(n,n),Rmatrix(n,n))
                   end if
                end if
             else
                if(m.eq.1)then !!On ther frist row, we know that 
                   !write(*,*) 'm=1'
                   !! c/sqrt(R) = K(n,1)/sqrt(K(1,1))
                   Cmatrix(n,1)=kappa_matrix(n,1)
                   Rmatrix(n,1)=kappa_matrix(1,1)
                else !!Compute the overlaps so far
                   !write(*,*) 'm/=1'
                   !!Add upp all the earlier contributions
                   call compute_euclidian_overlap(n-1,Cmatrix(n,1:(n-1)),&
                        Rmatrix(n,1:(n-1)),Cmatrix(m,1:(n-1)),&
                        Rmatrix(m,1:(n-1)),nomin,sqden)
                   !!Take the square root of sqden
                   !! since we expect it to be integer
                   !!write(*,*) 'sqden:',sqden
                   denom=isqrt(sqden)
                   
                   !!write(*,*) 'Subtract'
                   !!Subtract them from the kappa-matrix entry
                   call add_fractions(kappa_matrix(n,m),1,-nomin,1*denom,nomin,denom)  
                   !!Divide by the other pieces
                   !!Here Cmatrix and Rmatrix plays the roles as inverese (as a middle step)
                   call do_reduce_sqrt_fraction(Rmatrix(m,m),&
                        Cmatrix(m,m)**2*Rmatrix(m,m),&
                        Cmatrix(n,m),Rmatrix(n,m))
                   !!M;Ultply together
                   call multiply_sqrt_number(nomin,denom*denom,Cmatrix(n,m),&
                        Rmatrix(n,m),Cmatrix(n,m),Rmatrix(n,m))
                   !!write(*,*) 'The fraction is',Cmatrix(n,m),'/sqrt(',Rmatrix(n,m),')'
                end if
             end if
             call reduce_sqrt_fraction(Cmatrix(n,m),Rmatrix(n,m))
             !!Make all the roots the same
             call make_root_same_as_diag(Cmatrix,Rmatrix,n,m,Size)
             !call print_charge_progres(Size,Cmatrix,Rmatrix)
             !write(*,*) 
             !write(*,*) 
          end do
       end do
    end if


    !!If all the Rmatrix entries are the same, then we may chose any entry
    !!In all other cases we must scale the differententries
    call rescale_Rmatrix(Size,Cmatrix,Rmatrix)
    do n=1,Size
       !!Now the differnentries are the same
       Rlist(n)=Rmatrix(n,n)
    end do



    !!write(*,*) 'Done with computation'
    !!write(*,*) '--------------------------------------'
    !!write(*,*)
  end subroutine kappa_charge_lattice

  subroutine rescale_Rmatrix(Size,Cmatrix,Rmatrix)
    !!Rescales the Rmatrix enties such that the compactifiaction raduii 
    !!is the same for all rows
    integer, intent(IN) :: Size
    integer, intent(INOUT) ::Cmatrix(Size,Size),Rmatrix(Size,Size)
    integer :: n,m1,m2,scale2,scale
    logical :: nonequal

    do n=1,Size
       nonequal=.false.
       do m1=n+1,Size !!All entries in upper right are 1 and uninteresting
          !!If two ellemtns differ
          !!then all elemts will not be equal to the first
          if(Rmatrix(n,n).ne.Rmatrix(m1,n))then 
             nonequal=.TRUE. 
             !write(*,*) 'All collumn entries not the same in collumn',n
             !write(*,*) 'Rmatrix:'
             !call print_int_matrix(Rmatrix)             
          end if
       end do
       if(nonequal)then !!The elements needs do the scaled
          !!We scale so that the first elemtn is scaled apropriately 
          !!to the rest and then we work our way down
          do m1=n,Size-1 !!All entries in upper right are 1 and uninteresting
             do m2=n+1,Size 
                if(Rmatrix(m1,n).gt.Rmatrix(m2,n))then
                   scale2=Rmatrix(m1,n)/Rmatrix(m2,n)
                   scale=isqrt(scale2)
                   Rmatrix(m2,n)=Rmatrix(m2,n)*scale2
                   Cmatrix(m2,n)=Cmatrix(m2,n)*scale
                elseif(Rmatrix(m1,n).lt.Rmatrix(m2,n))then
                   scale2=Rmatrix(m2,n)/Rmatrix(m1,n)
                   scale=isqrt(scale2)
                   Rmatrix(m1,n)=Rmatrix(m1,n)*scale2
                   Cmatrix(m1,n)=Cmatrix(m1,n)*scale
                else !!do nothing
                end if
             end do
          end do
          !write(*,*) 'Now fixed, and is:'
          !call print_int_matrix(Rmatrix)             
       end if
    end do
  end subroutine rescale_Rmatrix
    
  subroutine charge_lattice_to_inverse_lattice(Size,Cmatrix,Rlist,Lmatrix)
    !!Computes the inverse lattice to the C_matrix
    !!Care is takes such that the compactification radii are the same 
    !!Do the direct and the dual lattice.
    !!For that reason, there may be changes to Cmatrix, and Rlist
    !!That need to propagate back!
    integer, intent(IN) :: Size
    integer, intent(INOUT) :: Cmatrix(Size,Size),Rlist(Size)
    integer, intent(OUT) :: Lmatrix(Size,Size)
    integer :: DlistW(Size),n,RlistW(Size)
    integer :: Cgcd,Lgcd,Lgcd_new,Scale_factor
    
    !write(*,*) 
    !write(*,*) '....................'
    !write(*,*) '   inverse charge lattive'
    !write(*,*) '....................'
    if(Size.eq.1)then !!Laughlin qp-lattice is simple
       Lmatrix=1
    else
       call integer_inverse(Size,Cmatrix,Lmatrix,DlistW)
       Lmatrix=transpose(Lmatrix)
       RlistW=DlistW**2
       !write(*,*) 'DlistW:',DlistW
       !write(*,*) 'Rlist :',Rlist

       !! We now need to rescale C_duall and DlistW by
       !!the initial scales Rtestlist
       !! This means that DlistW -> DlistW/sqrt(Rlist)
       !!                 RlistW -> RlistW**2/Rlist
       !! If Rlist == DlistW then RlistW -> Rlist but 
       !!this is in general not true
       do n=1,Size
          if(DlistW(n).ne.Rlist(n))then
             !!If the lists are thhe same
             !!then the sqrt denominators are the same also
             !!...
             !!but if not... something need to be done
             
             !write(*,*) 'The sqrt denominators are not the same for charge lattice and inverse lattice'
             !write(*,*) 'Need to take a closer look'
             !write(*,*) 'Cmatrix is:'
             !call print_int_matrix(Cmatrix)
             !write(*,*) 'Rlist :',Rlist
             !write(*,*) 'Lmatrix is:'
             !call print_int_matrix(Lmatrix)
             !write(*,*) 'RlistW:',RlistW

             Cgcd=abs(gcd_vector(Size,Cmatrix(:,n)))
             Lgcd=abs(gcd_vector(Size,Lmatrix(:,n)))
             !write(*,*) 'The gcd to work with is Cgcd:',Cgcd,'Lgcd:',Lgcd
             call multiply_sqrt_number(Lgcd,RlistW(n),Rlist(n),Rlist(n),&
                  Lgcd_new,RlistW(n))
             !write(*,*) 'The reduced fraction is ',Lgcd_new,'/sqrt(',RlistW(n),')'
             if(Lgcd.ne.Lgcd_new)then !!Check for rescaling
                write(*,*) '---cltil---'
                write(*,*) 'Some rescaling of Lgcd needed....'
                call exit(-2)
             end if

             !write(*,*) 'Now the Lmatrix is:'
             !call print_int_matrix(Lmatrix)
             !write(*,*) 'RlistW:',RlistW

             
             !!Rescale C and Rlist
             if(RlistW(n).gt.Rlist(n))then
                Scale_factor=isqrt(RlistW(n)/Rlist(n))
                !write(*,*) 'The scale factor is:',Scale_factor
                Rlist(n)=Rlist(n)*Scale_factor**2
                Cmatrix(:,n)=Cmatrix(:,n)*Scale_factor
             else
                Scale_factor=isqrt(Rlist(n)/RlistW(n))
                !write(*,*) 'The scale factor is:1/',Scale_factor
                RlistW(n)=RlistW(n)*Scale_factor**2
                Lmatrix(:,n)=Lmatrix(:,n)*Scale_factor
             end if

             if(RlistW(n).ne.Rlist(n))then
                write(*,*) '---cltil---'
                write(*,*) 'Now something has gone wrong again!'
                call exit(-2)
             end if

             !write(*,*) 'Cmatrix is:'
             !call print_int_matrix(Cmatrix)
             !write(*,*) 'Rlist :',Rlist

             !write(*,*) 'Now the Lmatrix is:'
             !call print_int_matrix(Lmatrix)
             !write(*,*) 'RlistW:',RlistW

             
          end if
       end do
    end if
    
  end subroutine charge_lattice_to_inverse_lattice




  subroutine charge_lattice_to_null_lattice(Size,Cmatrix,Cbar,Rlist,Rbar,DualNullvector,RsqrtW)
    integer, intent(IN) :: Size,Cmatrix(Size,Size),Rlist(Size)
    integer, intent(IN) :: Cbar(Size,Size),Rbar(Size)
    integer, intent(OUT) :: DualNullvector(2*Size,2*Size)
    integer, intent(OUT) :: RsqrtW(2*Size,2*Size)
    integer :: RlistW(2*Size)
    integer :: Cin(2*Size,2*Size)
    integer :: Rscalelist(2*Size)
    integer :: n,m
    
    !write(*,*) 
    !write(*,*) '....................'
    !write(*,*) '   null lattice of the  charge lattive'
    !write(*,*) '....................'
    !! WE can have an ortogonal lattice since the lattice is underdetermined
    !! We solve for the dual lattice and the orthogonal lattice at the same time by inverting the lattice formed by
    !! {tilde q_alpha} and an identiy lattice
    
    
    Cin=0
    !!The charge vectors
    do n=1,Size
       do m=1,Size
          Cin(n,m)=Cmatrix(n,m)
          Cin(n,m+Size)=-Cbar(n,m)
       end do
    end do
    !! The extra diagonal piece (will fix the orthogonal vectors)
    do n=1,Size
       Cin(n+Size,n+Size)=1
    end do

    Rscalelist=0 !!Construct the lsit con compatcifiacfion radi
    do n=1,Size
       Rscalelist(n)=Rlist(n)
       Rscalelist(n+Size)=Rbar(n)
    end do
    
    !write(*,*) 'The C_in matrix'
    !call print_int_matrix(Cin)
    
    call integer_inverse(2*Size,Cin,DualNullvector,RlistW)
    DualNullvector=transpose(DualNullvector)
    !!write(*,*) 'The C_dual_null matrix'
    !!call print_int_matrix(DualNullvector)
    !!write(*,*) 'RlistW:',RlistW
    !!write(*,*) 'Rlistt:',Rscalelist
    !! We now need to rescale C_duall and RlistW by in initial scales Rscalelist
    !! This means that RlistW-> RlistW/sqrt(Rscalelist)
    !! If Rlist == Rscalelist then RlistW-> RlistW but this is in general not true
    !!In practive we let C_ij/Rw_j -> C_ij*sqrt(R_j)/Rw_j 
    !!!                       = C_ij*R_j/sqrt(Rw_j^2*R_j)
    !!! so C_ij -> C_ij*R_j  and Rw_j -> Rw_j^2*R_j
    
    do n=1,2*Size
       do m=1,2*Size
          call multiply_sqrt_number(DualNullvector(m,n),RlistW(n)**2,Rscalelist(n),Rscalelist(n),DualNullvector(m,n),RsqrtW(m,n))
       end do
    end do
    !!write(*,*) 'RlistW:',RlistW
    
  end subroutine charge_lattice_to_null_lattice

  subroutine make_root_same_as_diag(Cmatrix,Rmatrix,n,m,Size)
    !!Makes the root expression (n,m) the same as for the diagonal pieces (m,m)
    integer,  intent(in) :: n,m,Size
    integer,  intent(OUT) :: Rmatrix(Size,Size),Cmatrix(Size,Size)
    integer :: gcd1,mul1
    if(n.eq.m)then
       !!Do nothing
    elseif(Cmatrix(n,m).eq.0)then !!Can be triviallt rescaled
       Rmatrix(n,m)=Rmatrix(m,m)
    else
       !!Assuming n>m
       gcd1=gcd(Rmatrix(n,m),Rmatrix(m,m))
       if(gcd1.ne.Rmatrix(m,m))then
          mul1=Rmatrix(m,m)/gcd1
          Rmatrix(n,m)=Rmatrix(n,m)*mul1
          Cmatrix(n,m)=Cmatrix(n,m)*isqrt(mul1)
       end if
    end if
  end subroutine make_root_same_as_diag

  subroutine charge_lattice_error()
    write(*,*) '-----ERROR----'
    write(*,*) 'This kappa-matrix does not have a charge lattice'
    write(*,*) 'quitting.....'
    call exit(-3)
  end subroutine charge_lattice_error
   
  subroutine print_charge_progres(Size,Cmatrix,Rmatrix)
    integer,  intent(IN) :: Size,Rmatrix(Size,Size),Cmatrix(Size,Size)
    write(*,*) 'Current C-matrix'
    call print_int_matrix(Cmatrix)
    write(*,*) 'Current R-matrix'
    call print_int_matrix(Rmatrix)
  end subroutine print_charge_progres
  
  
  function is_hierarchy_k_matrix(Size,kappa_matrix) result(is_hier)
    !!Tests is a k-matrix describes a state in the chiral hierarchy
    integer, intent(IN) :: size, kappa_matrix(Size,Size)
    logical :: is_hier
    integer :: n,m
    
    !!Start by assuming it's hierachy
    is_hier=.TRUE.
    do n=1,Size
       do m=n,Size !!!kappa-matrix is symmetrix (tested later)
          if(n.eq.m)then !!Diagonal elements
             if(kappa_matrix(n,n).le.0)then
                is_hier=.FALSE. !!Diagonal elements should be positive
             elseif(n.gt.1)then
                if(kappa_matrix(n,n).lt.kappa_matrix(n-1,n-1))then
                   is_hier=.FALSE. !!Diagonal elements should be growing
                end if
             end if
          else !!Off-diagonal elements
             if(kappa_matrix(n,m).ne.kappa_matrix(m,n))then
                is_hier=.FALSE. !!kappa-matrix is symmetrix
             elseif(kappa_matrix(m,n).ne.(kappa_matrix(n,n)-1))then !!n < m by construction
                is_hier=.FALSE. !!off-diagonal = diagonal -1
             end if
          end if
       end do
    end do
  end function is_hierarchy_k_matrix
  
  
  
  subroutine kappa_matrix_group_sizes(Size,kappa_matrix,plist,p,q)
    integer, intent(IN) :: Size, kappa_matrix(Size,Size)
    integer, intent(OUT) :: plist(Size),q,p
    integer :: n,m,Pmatrix(Size,Size),max_gcd
    
    call kappa_matrix_inverse(Size,kappa_matrix,Pmatrix,q)
    !!write(*,*) 'q_out:',q

    !!Plist is the row/collum sumation of pmatrix.
    do n=1,Size
       plist(n)=0 !!Sum all rows
       do m=1,Size
          plist(n)=plist(n)+Pmatrix(m,n)
       end do
    end do
    !!write(*,*) 'plist_out',plist
    
    max_gcd=q !!Can not be larger than the denominator
    do n=1,Size  
       max_gcd=gcd(plist(n),max_gcd) !! cumulative gcd of the plist values
    end do
    !!write(*,*) 'max_gcd',max_gcd
    !!Divide away the cumulative gcd
    !!To get relatively prime factors
    q=q/max_gcd
    plist=plist/max_gcd
    !!Nominator should be positive
    if(q.lt.0)then
       q=-q
       plist=-plist
    end if
    !!Add all the p-contributions together
    p=0
    do n=1,Size
       p=p+plist(n)
    end do
  end subroutine kappa_matrix_group_sizes


  subroutine kappa_matrix_inverse(Size,kappa_matrix,Pmatrix,q)
    integer, intent(IN) :: Size, kappa_matrix(Size,Size)
    integer, intent(OUT) :: Pmatrix(Size,Size),q !!Inverse matrix, and denominator
    integer :: n,m,max_gcd,Qlist(Size)
    
    if(Size.eq.1)then !!Ordinary Laughlin function
       Pmatrix(1,1)=1
       q=kappa_matrix(1,1)
       if(q.eq.0)then
          call stop_univertable_matrix(size,kappa_matrix)
       end if
    elseif(Size.eq.2)then !!Here we know explicitly what the inverse matrix is
       q=kappa_matrix(1,1)*kappa_matrix(2,2)-kappa_matrix(1,2)*kappa_matrix(2,1)!!2x2 determinant
       if(q.eq.0)then
          call stop_univertable_matrix(size,kappa_matrix)
       end if
       !!The inverse matrix
       Pmatrix(1,1)=kappa_matrix(2,2)
       Pmatrix(2,2)=kappa_matrix(1,1)
       Pmatrix(1,2)=-kappa_matrix(1,2)
       Pmatrix(2,1)=-kappa_matrix(2,1)
       !!Raduce it a far as possible
       max_gcd=q !!Can not be greates than the denominator
       do n=1,Size
          do m=1,Size
             max_gcd=gcd(max_gcd,Pmatrix(n,m))
          end do
       end do
       if(max_gcd.ne.1)then
          q=q/max_gcd
          Pmatrix=Pmatrix/max_gcd
       end if
    else
       !!!Invert the integer matrix of it is 3x3 or larger
       call integer_inverse(Size,kappa_matrix,Pmatrix,Qlist)
       !!Now the matrix is diagonal (but not with ones)
       !!Multiply all rows such that they have the same value on the diagonal
       !!This is done by first finding the gratest comon divisor for each pair
       !! and then multiplying witht re remainder
       !!!write(*,*) 'Equalize the rows'
       do n=1,(Size-1)
          do m=(n+1),Size
             call scale_lines_the_same(Size,Qlist,PMatrix,n,m)
          end do
       end do
       !!write(*,*) 'Qlist:',Qlist
       !!write(*,*) 'Pmatrix:'
       !!call print_int_matrix(Pmatrix)
       !!Set the denominator to the first elemetn of the list
       !!(they are all the same)
       q=Qlist(1)
       !!write(*,*) 'q:',q
       
    end if

    !!Make denominator positive
    if(q.lt.0)then
       q=-q
       Pmatrix=-Pmatrix
    end if
  end subroutine kappa_matrix_inverse
  
  subroutine stop_univertable_matrix(size,Matrix)
    integer, intent(in) :: size,matrix(size,size)
    write(*,*) 'ERROR: kappa-matrix is un-invertable!!'
    call print_int_matrix(matrix)
    write(*,*) 'Stopping....'
    call exit(-2)
  end subroutine stop_univertable_matrix

  
  subroutine  Filling_fraction(Size,plist,q_group,p,q)
  !! Computes the filling fraction for a given set of groups-sizes and a minimal cell
  integer, intent(IN) :: Size, plist(Size),q_group
  integer, intent(OUT) :: p,q
  integer :: p_group,local_gcd
  p_group=sum(plist)
  local_gcd=gcd(p_group,q_group)
  p=p_group/local_gcd
  q=q_group/local_gcd
  !!Denominator is always positive
  if(q.lt.0)then
     p=-p
     q=-q
  end if
end subroutine Filling_fraction
  


  subroutine integer_inverse(Size,InMatrix,OutMatrix,DenomList)
    integer, intent(IN) :: Size, InMatrix(Size,Size)
    integer, intent(OUT) :: OutMatrix(Size,Size),DenomList(Size)
    integer :: WorkMatrix(Size,Size),n
    WorkMatrix=InMatrix
    !!Set OutMatrix To the identity matrix
    OutMatrix=0
    do n=1,Size
       OutMatrix(n,n)=1
    end do
    !write(*,*) '-------------------'
    !write(*,*) 'Begin inverse procedure'
    call do_integer_inverse(Size,WorkMatrix,OutMatrix)
    !write(*,*) '*************'
    !call print_progress(Size,WorkMatrix,OutMatrix)
    !write(*,*) 'Inverse completed'
    !write(*,*) '--------------------'
    
    
    !!Set the denominatorlist to the diagonal of the working matrix!!
    do n=1,Size
       DenomList(n)=WorkMatrix(n,n)  
       if(DenomList(n).lt.0)then
          !!No-negavie signs please
          DenomList(n)=-DenomList(n)
          OutMatrix(n,:)=-OutMatrix(n,:)
       end if
    end do
    !!This gives the rational scaling for all the elements
    
  end subroutine integer_inverse

  

  subroutine do_integer_inverse(Size,InMat,OutMat)
    integer, intent(IN) :: Size
    integer, intent(OUT) :: InMat(Size,Size),OutMat(Size,Size)
    integer :: n,m,det
    logical :: shuffled
    
    !!!Algorithm diagonalizes integes matrices using Gauss elimination
    !!Extra care is taken as all entries have to be integers
    !!Thus after each operation, to avoid to large numbers,
    !!the operated line is deduced by its global gcd.
    
    !!call print_progress(Size,Inmat,Outmat)
    !!   Check lower triangle
    do m=1,(Size-1) !!Loop over lower triangle
       if(InMat(m,m).eq.0)then
          shuffled=.FALSE.
          do n=(m+1),Size !!Loop over the entries bellow
             write(*,*) 'for(n,m)=',n,m,'gives Mat(m,n)=',InMat(n,m)
             if(InMat(n,m).ne.0)then
                write(*,*) 'Shuffle with line n=',n
                InMat =swap_matrix_rows(InMat ,n,m)
                OutMat=swap_matrix_rows(OutMat,n,m)
                call print_int_matrix(InMat)
                shuffled=.TRUE.
                exit
             end if
          end do
          if(.not.shuffled)then
             write(*,*) ',,,,,,,,dii,,,,,,,'
             write(*,*) 'ERROR: Matrix Inverse'
             write(*,*) 'The determinant is zero. No inverse!'
             write(*,*) 'quitting....'
             call exit(-1)
          end if
       end if
       do n=(m+1),Size
          if(InMat(n,m).ne.0)then
             !write(*,*) '***Lower Not zero, make same value'
             call add_row_m_to_row_n(Size,InMat,OutMat,n,m,m,m)
          end if
       end do
    end do
    !! Now the matrix is traiangular. Therefore we can cheack that the determinant is not zero
    det=1
    do n=1,Size
       det=det*InMat(n,n)
    end do
    if(det.eq.0)then
       call stop_univertable_matrix(size,inmat)
    end if


    !!!Here we reduce all rows to have as small integers as possible
    !write(*,*) "Reduce all rows:"
    do n=1,Size
       call reduce_row(Size,InMat,OutMat,n)
    end do


    !!   Check upper triangle
    do n=1,(Size-1)
       do m=(n+1),Size !!Loop over upper triangle
          if(InMat(n,m).ne.0)then
             !write(*,*) '***Upper Not zero, make same value'
             call add_row_m_to_row_n(Size,InMat,OutMat,n,m,m,m)
          end if
       end do
    end do

    !!!Here we reduce all rows to have as small integers as possible
    !write(*,*) "Reduce all rows:"
    do n=1,Size
       call reduce_row(Size,InMat,OutMat,n)
    end do
  end subroutine do_integer_inverse
  

  subroutine add_row_m_to_row_n(Size,InMat,OutMat,n1,n2,m1,m2)
    integer, intent(in) :: n1,n2,m1,m2,Size
    integer, intent(out) :: InMat(Size,Size), OutMat(Size,Size)
    integer :: local_gcd,mul1,mul2
    local_gcd=gcd(InMat(n1,n2),InMat(m1,m2))
    mul1=InMat(n1,n2)/local_gcd
    mul2=InMat(m1,m2)/local_gcd
    !!Subtract the upper from the lower
    !write(*,*) '---Subtracting'
    InMat(n1,:)=mul2*InMat(n1,:)-mul1*InMat(m1,:)
    OutMat(n1,:)=mul2*OutMat(n1,:)-mul1*OutMat(m1,:)
    !call print_progress(Size,Inmat,Outmat)    
    !write(*,*) '---Reducing rows'
    call reduce_row(Size,InMat,OutMat,n1)
  end subroutine add_row_m_to_row_n


  subroutine make_lines_the_same(Size,InMat,OutMat,n1,n2,m1,m2)
    !!Makes the elements n1,n2 of Inmat the same as m1,m2 of Inmat
    !!Outmat is scaled the same way
    integer, intent(in) :: n1,n2,m1,m2,Size
    integer, intent(out) :: InMat(Size,Size), OutMat(Size,Size)
    integer :: local_gcd,mul1,mul2
    local_gcd=gcd(InMat(n1,n2),InMat(m1,m2))
    mul1=InMat(n1,n2)/local_gcd
    mul2=InMat(m1,m2)/local_gcd
    !write(*,*) 'n1,n2',n1,n2
    !write(*,*) 'm1,m2',m1,m2
    !write(*,*) 'mul1,mul2',mul1,mul2
    if(mul1.ne.1)then
       !write(*,*) 'mul1'
       InMat(m1,:)=mul1*InMat(m1,:)
       OutMat(m1,:)=mul1*OutMat(m1,:)
       !call print_progress(Size,Inmat,Outmat)
    end if
    if(mul2.ne.1)then
       !write(*,*) 'mul2'
       InMat(n1,:)=mul2*InMat(n1,:)
       OutMat(n1,:)=mul2*OutMat(n1,:)
       !call print_progress(Size,Inmat,Outmat)
    end if
  end subroutine make_lines_the_same



  subroutine scale_lines_the_same(Size,InList,OutMat,n,m)
    !!Makes the elements n of InList the same as n of InList
    !!Outmat is scaled the same way
    integer, intent(in) :: n,m,Size
    integer, intent(out) :: InList(Size), OutMat(Size,Size)
    integer :: local_gcd,mul1,mul2
    local_gcd=gcd(InList(n),InList(m))
    mul1=InList(n)/local_gcd
    mul2=InList(m)/local_gcd
    !write(*,*) 'n,m',n,m
    !write(*,*) 'mul1,mul2',mul1,mul2
    if(mul1.ne.1)then
       !write(*,*) 'mul1'
       InList(m)=mul1*InList(m)
       OutMat(m,:)=mul1*OutMat(m,:)
       !call print_progress(Size,InList,Outmat)
    end if
    if(mul2.ne.1)then
       !write(*,*) 'mul2'
       InList(n)=mul2*InList(n)
       OutMat(n,:)=mul2*OutMat(n,:)
       !call print_progress(Size,InList,Outmat)
    end if
  end subroutine scale_lines_the_same




  subroutine reduce_row(Size,InMat,OutMat,n)
    integer, intent(in) :: n,Size
    integer, intent(out) :: InMat(Size,Size), OutMat(Size,Size)
    integer :: max_gcd,m
    max_gcd=0 !!This is the "highest gcd possible"
    do m=1,Size
       max_gcd=gcd(max_gcd,InMat(n,m))
       max_gcd=gcd(max_gcd,OutMat(n,m))
    end do
    if((max_gcd.ne.0).and.(max_gcd.ne.1))then
       !write(*,*) 'max_gcd',max_gcd
       !write(*,*) 'Reducing:'
       InMat(n,:)=InMat(n,:)/max_gcd
       OutMat(n,:)=OutMat(n,:)/max_gcd
       !call print_progress(Size,Inmat,Outmat)
    end if
  end subroutine reduce_row
  


  subroutine print_progress(Size,Inmat,Outmat)
    integer, intent(IN) :: Size, INmat(Size,Size), Outmat(Size,size)
    write(*,*) 'Working Inverse'
    call print_int_matrix(InMat)
    write(*,*) 'Target Inverse'
    call print_int_matrix(OutMat)
  end subroutine print_progress
  
  
  
  logical function is_matrix_zero(input_kappa,size) result(is_zero)
    !! Returns .TRUE. if the matrix is zero and .FALSE. othervise
    integer, intent(IN) :: Size,input_kappa(size,size)
    integer :: n1,n2
    is_zero=.TRUE.
    do n1=1,size
       do n2=1,size
          if(input_kappa(n1,n2).ne.0)then 
             is_zero=.FALSE. !! Matrix is not all zero
          end if
       end do
    end do
  end function is_matrix_zero


  subroutine orthonormalize_then_null_vectors(Groups,Cnull,Rnull,SpaceLikeNull)
    !!Orthonormalized the null-vectors
    integer, intent(IN) :: Groups
    integer, intent(INOUT) :: Cnull(Groups,2*Groups),Rnull(Groups,2*Groups)
    logical, intent(OUT) :: SpaceLikeNull(Groups)
    integer :: n,m,nomin,denom,sqden,tmpC(2*Groups),tmpR(2*Groups)
    !!Compute the norms
    !!write(*,*) 'Next we compute the norm'
    do n=1,Groups
       !write(*,*) 'Vector no:',n
       !write(*,*) '  Orthogonalize agains the other null-vectors'
       do m=1,n-1
          !write(*,*) '  Remove vector no:',m
          !!Compute the overlap to remove it
          call compute_lorenz_overlap(groups,Cnull(n,:),Rnull(n,:),&
               Cnull(m,:),Rnull(m,:),nomin,sqden)
          !write(*,*) 'Overlap',nomin,'/sqrt(',sqden,')'
          !!Vector to subtract is 
          call scale_sqrt_vector_by_sqrt(2*Groups,Cnull(m,:),Rnull(m,:),nomin,sqden,&
               tmpC,tmpR)
          !write(*,*) 'Scaled vector to remove is:'
          !write(*,*) tmpC
          !write(*,*) 'With radii:'
          !write(*,*) tmpR
          if(SpaceLikeNull(m))then
             !!IF spacelike the minus sign becomes a plus
             call add_sqrt_vector_to_sqrt_vector(2*Groups,Cnull(n,:),Rnull(n,:),&
                  tmpC,tmpR,Cnull(n,:),Rnull(n,:))
          else
             !!Otherwise it the usual orthogonalization proces
             call add_sqrt_vector_to_sqrt_vector(2*Groups,Cnull(n,:),Rnull(n,:),&
                  -tmpC,tmpR,Cnull(n,:),Rnull(n,:))
          end if
          
       end do
       !write(*,*) '  Orthoginalized'
       !write(*,*) 'Non-Normalized function is:'
       !write(*,*) Cnull(n,:)
       !write(*,*) 'With radii:'
       !write(*,*) Rnull(n,:)

       call compute_lorenz_norm(Groups,Cnull(n,:),Rnull(n,:),nomin,denom)
       !write(*,*) 'Norm is:', nomin,'/',denom
       if(nomin.gt.0)then
          !write(*,*) 'Lightlike'
          SpaceLikeNull(n)=.FALSE.
       elseif(nomin.lt.0)then
          !write(*,*) 'spacelike'
          nomin=-nomin
          SpaceLikeNull(n)=.TRUE.
       else
          write(*,*) 
          write(*,*) 'ERROR:'
          write(*,*) 'The null vector'
          call print_int_vector(Cnull(n,:))
          write(*,*) '/sqrt'
          call print_int_vector(Rnull(n,:))
          write(*,*) 'is a true null vector'
          write(*,*) 'The naive Gram-shmidt otrogoanliaztion does not work here'
          write(*,*) 'FIXME: This problem is solved by tacking a step back'
          write(*,*) '       and starting again by adding in other null-vector'
          write(*,*) '       components that are not true null-vectors.'
          write(*,*) 'We`ll fix this some other time'
          write(*,*) 'quitting....'
          call exit(-1)
       end if
       !write(*,*) 'Normalize by dividing by the sqrt norm'
       call scale_sqrt_vector_by_sqrt(2*groups,Cnull(n,:),Rnull(n,:),denom,nomin*denom,&
            Cnull(n,:),Rnull(n,:))
       !write(*,*) 'Normalized function is:'
       !write(*,*) Cnull(n,:)
       !write(*,*) 'With radii:'
       !write(*,*) Rnull(n,:)
       !!Sanity check!
       call compute_lorenz_norm(Groups,Cnull(n,:),Rnull(n,:),nomin,denom)
       if((abs(nomin).ne.1).or.(denom.ne.1))then
          write(*,*) 'Norm for null',n,'is:', nomin,'/',denom
          write(*,*) 'ERROR: The norm of the null vector is not 1...'
          call exit(-1)
       end if
    end do

  end subroutine orthonormalize_then_null_vectors
  

  subroutine remove_null_from_dual_vectors(Groups,Cnull,Rnull,SpaceLikeNull,Cdual,Rdual)
    integer, intent(IN) :: Groups
    integer, intent(IN) :: Cnull(Groups,2*Groups),Rnull(Groups,2*Groups)
    integer, intent(INOUT) :: Cdual(Groups,2*Groups),Rdual(Groups,2*Groups)
    logical, intent(OUT) :: SpaceLikeNull(Groups)
    integer :: n,m,nomin,sqden,tmpC(2*Groups),tmpR(2*Groups)

    !!Next we remove the null-pice from the dual vectors
    !!First we compute the overlap ad the we remove it
    !!write(*,*) '  --- Compute overlap  ---'
    do n=1,Groups
       do m=1,Groups
          !write(*,*) 'm,n:',m,n
          !!Compute the overlap to remove it
          call compute_lorenz_overlap(groups,Cdual(n,:),Rdual(n,:),&
               Cnull(m,:),Rnull(m,:),nomin,sqden)
          !write(*,*) 'Overlap',nomin,'/sqrt(',sqden,')'
          !!Vector to subtract is 
          call scale_sqrt_vector_by_sqrt(2*Groups,Cnull(m,:),Rnull(m,:),nomin,sqden,&
               tmpC,tmpR)
          if(SpaceLikeNull(m))then
             !!IF spacelike the minus sign becomes a plus
             call add_sqrt_vector_to_sqrt_vector(2*Groups,Cdual(n,:),Rdual(n,:),&
                  tmpC,tmpR,Cdual(n,:),Rdual(n,:))
          else
             !!Otherwise it the usual orthogonalization proces
             call add_sqrt_vector_to_sqrt_vector(2*Groups,Cdual(n,:),Rdual(n,:),&
                  -tmpC,tmpR,Cdual(n,:),Rdual(n,:))
          end if
       end do
    end do
    
  end subroutine remove_null_from_dual_vectors


  subroutine check_k_minus_is_integer(K_minus,k_denom,Size)
    integer, intent(IN) :: Size,K_minus(Size,Size),k_denom
    integer :: i,j
    do i=1,Size
       do j=1,Size
          if((K_minus(i,j)/k_denom)*k_denom.ne.K_minus(i,j))then
             write(stderr,*) 'ERROR: The values of K_minus/k_denom are not integer'
             write(*,*) 'K-minus matrix:'
             call print_int_matrix(K_minus)
             write(*,*) 'K-denominator:',K_denom
             call exit(-1)
          end if
       end do
    end do
  end subroutine check_k_minus_is_integer
  

  subroutine gen_hierarchy_k_matrix(D,kappa_matrix,Slist)
    !!Constructs generic chiral kappa-matrix from the Slist, that labesl the differens at each step
    integer, intent(in) :: D,Slist(D)
    integer :: kappa_matrix(D,D),Iter1,Iter2,DiagList(D)
    !!The Slist contains the starting element and then the incerements for each diagonal step

    !!Add up all the Slist elements
    DiagList(1)=Slist(1)
    do Iter1=2,D
       DiagList(Iter1)=DiagList(Iter1-1)+Slist(Iter1)
    end do
    !write(*,*) 'DiagList:',DiagList
    
    do Iter1=1,D
       do Iter2=Iter1,D
          If(Iter1.eq.Iter2)then
             kappa_matrix(Iter1,Iter1)=DiagList(Iter1)
          else
             kappa_matrix(Iter1,Iter2)=DiagList(Iter1)-1
             kappa_matrix(Iter2,Iter1)=DiagList(Iter1)-1
          end If
       end do
    end do
    
    !write(*,*) 'the k-matrix is'
    !call print_int_matrix(kappa_matrix)
  end subroutine gen_hierarchy_k_matrix


  subroutine get_first_nlist_entry(Kmat,cells,nlist,D)
    integer, intent(IN) :: D, Kmat(D,D),cells
    integer, intent(OUT) :: nlist(D)
    integer:: Ns,p,q,plist(D),eps,eps0
    integer:: n !!Loop
    call kappa_matrix_group_sizes(D,Kmat,plist,p,q)          
    Ns=q*cells
    eps=mod(Ns+kmat(1,1),2)
    if(D.gt.1)then
       do n = 2,D 
          eps0=mod(Ns+kmat(n,n),2)
          if(eps.ne.eps0)then !Check for well defined parity
             write(*,*) 'ERROR: kappa-matrix is not bosonic or fermionic.'
             write(*,*) '       could handle this, but confused.'
             call exit(-1)
          end if
       end do
    end if
    nlist=eps
  end subroutine get_first_nlist_entry

subroutine get_next_nlist_entry(Kmat,plist,q_group,cells,nlist,D)
    integer, intent(IN) :: D, Kmat(D,D),plist(D),q_group,cells
    integer, intent(INOUT) :: nlist(D)
    integer :: n1,n2,res(D),r1(D),n0list(D),eps(D),Kinv(D,D),q_inv,determinant,delta,rows,shift(D)
    logical ::same
    !! Compute the inverse kappa-matrix
    call kappa_matrix_inverse(D,Kmat,Kinv,q_inv)
    eps=modulo(nlist,2) !!This is the reference state
    n0list=0 !!This is the reference state
    res=(nlist-eps)/2
    !!Regarding the state
    delta=gcd(sum(plist),q_group)
    determinant=determinant_int_mat(Kmat)
    rows=abs(determinant)/q_group
    !write(*,*) 'delta=',delta
    !write(*,*) 'q=',q_group
    !write(*,*) 'determinant=',determinant
    !write(*,*) 'rows=',rows
    if(rows.eq.1)then
       r1=res+1
       same=equivalent_points(r1,n0list,Kinv,q_inv,D)
       if(same)then
          write(*,*) 'List of states exhausted'
       end if
       nlist=2*r1+eps
    elseif(rows.gt.1)then
       if(D.eq.1)then !!Should not happen
          write(*,*) 'ERROR: rows>1 for D=1. Does not make sense!'
          call exit(-1)
       elseif(D.eq.2)then !!Should not happen
          shift=res
          shift(1)=res(2)
          r1=shift+1
          same=equivalent_points(r1,n0list,Kinv,q_inv,D)
          r1(1)=r1(1)-res(2)+res(1)
          if(same)then
             write(*,*) 'Row of states exhausted'
             r1(1)=r1(1)+1
             r1(1)=r1(1)-r1(2)
             r1(2)=0
          end if
          nlist=2*r1+eps
       elseif(D.gt.2)then
          write(*,*) 'ERROR: rows>1 not implemented yet for giving next state for d>2'
          write(*,*) '       Bug someone maintaining the code for this option'
          write(*,*) '       The problem here is how to switch between the rows'
          write(*,*) '       IF is turns out number rows is a power of 2, then things are probably easy'
          call exit(-1)
       end if
    end if
  end subroutine get_next_nlist_entry

  logical function equivalent_points(n1,n2,Kinv,q,D) result(same)
    integer, intent(IN) :: D, Kinv(D,D),n1(D),n2(D),q
    integer :: diff(D),scaled(D),n
    scaled=n2-n1
    !!write(*,*) 'diff:  ',scaled
    scaled=matmul(Kinv,scaled)
    !!write(*,*) 'scaled:',scaled
    scaled=modulo(scaled,q)
    !!write(*,*) 'reduced:',scaled
    same=.TRUE.
    do n=1,D
       if(scaled(n).ne.0)then
          same=.FALSE.
          EXIT
       end if
    end do
    
    
  end function equivalent_points

  integer function get_k1(plist,cells,nlist,D,q) result(k1)
    integer, intent(IN) :: D, cells,nlist(D),plist(D),q
    integer :: Ns
    Ns=q*cells
    k1=not_half_integer(cells*dot_product(plist,nlist),&
         Ns,'Momentum k1')
  end function get_k1
  
  integer function get_k2(plist,cells,nlist,D,q,r) result(k2)
    integer, intent(IN) :: D, cells,nlist(D),plist(D),q,r
    integer :: delta
    delta=gcd(sum(plist),q)
    k2=not_half_integer(cells*(2*r-dot_product(plist,nlist)),&
         delta*cells,'Co-momentum k2')
  end function get_k2
  
  integer function not_half_integer(dX,M,text) result(X)
    integer, intent(IN) :: dX,M
    character(*) :: text
    if(mod(dX,2).ne.0)then
       write(*,*) 'ERROR: ',text,' is not an integer!'
       write(*,*) text,'=',dX
       write(*,*) '       Should not happen!'
       call exit(-1)
    else
       X=modulo(dX/2,M)
    end if
  end function not_half_integer


end module K_matrix
