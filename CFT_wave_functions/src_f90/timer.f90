module timer
  !! This module contains routine usefull when computing the time a prodecure will take
  
  use typedef
  implicit none
  
  real :: StartWallTime,LastWallTime
  real :: StartCPUTime,LastCPUTime
  
  
  private
  public setup_write_progress,write_progress
  
  
contains
  
  subroutine setup_write_progress() bind(C)
    CALL wall_clock(StartWallTime) !!Start the clock (by getting the time)
    call cpu_time(StartCPUTime) !!Start the clock (by getting the time)
    LastWallTime = 0.0
    LastCPUTime = 0.0
  end subroutine setup_write_progress
  
  subroutine wall_clock(my_time)
    real, intent(out) :: my_time
    integer :: clck_counts, clck_rate,clck_max
    call system_clock ( clck_counts, clck_rate ,clck_max)
    my_time = clck_counts/real(clck_rate)
  end subroutine wall_clock
  
  subroutine write_progress(Nr,NSamples) bind(C)
    use ISO_C_BINDING
    integer(kind=C_INT) , INTENT(IN) :: Nr,Nsamples
    real ::  TimeWallElapsed, TimeWallLeft
    real ::  TimeCPUElapsed, TimeCPUTotal
    real ::  TimeElFmt,TimeLeFmt
    real ::  TimeElFmtCPU,TimeTotCPUFmt
    integer,save ::  steps=0
    integer ::  steps_el,steps_le
    character(len=4),save :: dimention_el=' sec'
    character(len=4),save :: dimention_le='  hr'
    character(len=4),save :: dimention_el_CPU=' sec'
    character(len=4),save :: dimention_tot_CPU='  hr'
    integer :: SamplesLeft
    integer, parameter :: min_len=60, hr_len=3600, sec_len=1
    
    real :: EndWallTime,EndCPUTime
    !initialize time steps
    steps_el=sec_len
    steps_le=hr_len
    

    CALL wall_clock(EndWallTime)
    call cpu_time(EndCPUTime)

    !write(*,*)     
    !write(*,*) 'Clock Time:',EndWallTime
    !write(*,*) 'CPU Time:',EndCPUTime
    
    !write(*,*) 'Last Wall Time:',LastWallTime
    !write(*,*) 'Last CPU Time:',LastCPUTime
    
    TimeWallElapsed = EndWallTime - StartWallTime  
    TimeCPUElapsed = EndCPUTime - StartCPUTime  
    !write(*,*) 'Wall Time Elapsed:',TimeWallElapsed
    !write(*,*) 'CPU Time Elapsed:',TimeCPUElapsed
    IF (TimeWallElapsed .gt. LastWallTime+steps)then
       
       !!Computes samples left
       SamplesLeft = Nsamples - Nr +1
       !!Compute time left
       TimeWallLeft = nint((real(SamplesLeft,dp)/real(Nr,dp))*real(TimeWallElapsed,dp))
       TimeCPUTotal = nint((real(Nsamples,dp)/real(Nr,dp))*real(TimeCPUElapsed,dp))
       
       !!Step up the time as the elapsed time goes upp
       if(TimeWallElapsed.gt.min_len)then
          !write(*,*) 'set min up'
          dimention_el=' min'
          steps_el=min_len
       end if
       if(TimeWallElapsed.gt.hr_len)then
          !write(*,*) 'set hr up'
          dimention_el='  hr'
          steps_el=hr_len
       end if


       !!Step up the time as the elapsed time goes upp
       if(TimeCPUElapsed.gt.min_len)then
          !write(*,*) 'set min up'
          dimention_el_CPU=' min'
       end if
       if(TimeCPUElapsed.gt.hr_len)then
          !write(*,*) 'set hr up'
          dimention_el_CPU='  hr'
       end if

       
       !!Step down the time as the time left ges down
       if(TimeWallLeft.lt.hr_len)then
          !write(*,*) 'set min down'
          dimention_le=' min'
          steps_le=min_len
       end if
       if(TimeWallLeft.lt.min_len)then
          !write(*,*) 'set sec down'
          dimention_le=' sec'
          steps_le=sec_len
       end if

       !!Step down the time as the time left ges down
       if(TimeCPUTotal.lt.hr_len)then
          !write(*,*) 'set min down'
          dimention_tot_CPU=' min'
       end if
       if(TimeCPUTotal.lt.min_len)then
          !write(*,*) 'set sec down'
          dimention_tot_CPU=' sec'
       end if

       
       select case(dimention_el)
       case(' sec')
          TimeElFmt=TimeWallElapsed
       case(' min')
          TimeElFmt=nint(real(TimeWallElapsed)/min_len)
       case('  hr')
          TimeElFmt=nint(real(TimeWallElapsed)/hr_len)
       case default
          write(*,*) 'unknown dimention: set to sec'
          TimeElFmt=TimeWallElapsed
          dimention_el=' sec'
          steps_el=sec_len
       end select


       select case(dimention_el_CPU)
       case(' sec')
          TimeElFmtCPU=TimeCPUElapsed
       case(' min')
          TimeElFmtCPU=nint(real(TimeCPUElapsed)/min_len)
       case('  hr')
          TimeElFmtCPU=nint(real(TimeCPUElapsed)/hr_len)
       case default
          write(*,*) 'unknown dimention: set to sec'
          TimeElFmtCPU=TimeCPUElapsed
          dimention_el_cpu=' sec'
       end select

       
       select case(dimention_le)
       case(' sec')
          TimeLeFmt=TimeWallLeft
       case(' min')
          TimeLeFmt=nint(real(TimeWallLeft)/min_len)
       case('  hr')
          TimeLeFmt=nint(real(TimeWallLeft)/hr_len)
       case default
          write(*,*) 'unknown dimention: set to sec'
          TimeLeFmt=TimeWallLeft
          dimention_le=' sec'
          steps_le=sec_len
       end select


       select case(dimention_tot_CPU)
       case(' sec')
          TimeTotCPUFmt=TimeCPUTotal
       case(' min')
          TimeTotCPUFmt=nint(real(TimeCPUTotal)/min_len)
       case('  hr')
          TimeTotCPUFmt=nint(real(TimeCPUTotal)/hr_len)
       case default
          write(*,*) 'unknown dimention: set to sec'
          TimeTotCPUFmt=TimeCPUTotal
          dimention_tot_CPU=' sec'
       end select

       
       WRITE(*,'(I10,A,I10,A,I7,A,A,I7,A)') &
            (Nr-1), ' done ', SamplesLeft, &
            ' to go. Time: ', nint(TimeElFmt),dimention_el, &
            ' Time left: ', nint(TimeLeFmt),dimention_le
       WRITE(*,'(A,I7,A,A,I7,A)') &
            '                      Elapsed CPU Time: ', nint(TimeElFmtCPU),dimention_el_CPU, &
            '  of Total: ', nint(TimeTotCPUFmt),dimention_tot_CPU
       LastWallTime = TimeWallElapsed
       steps=min(steps_el,steps_le)
    END if
  end subroutine write_progress
  
  
end module timer
