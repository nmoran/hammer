program test_com

  USE typedef     ! types and definitions
  use test_utilities
  use center_of_mass
  use dedekind_eta_function
  use misc_random
  
  !!Variables
  IMPLICIT NONE  

  !!Test starts here!!
    write(*,*) '         Test the center of mass function'
    !! Testing center of mass function
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_plain_cm
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_whole_z_cm
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_half_z_cm
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_z_arguments_cm
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'

contains 
  

!!!----------------------------------------
!!! Test CM
!!!----------------------------------------
  subroutine test_plain_cm()
    complex(kind=dpc) :: tau=iunit
    complex(kind=dpc), ALLOCATABLE, DIMENSION(:) :: Zlist
    integer :: D,iter,listvar
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    complex(kind=dpc) :: Weight_hier,weight_raw,CM_Facit
    real(KIND=dp) :: CMList(12)
    
    write(*,*) 'Test the CM-function with no arguments'
    call unset_CM_BC
    !!D=1
    CMList(1)=1.0001613990351406940d0
    CMList(2)=1.0179665950670831287d0
    CMList(3)=1.0864348112133080146d0
    CMList(4)=1.1897218499539461726d0
    !!D=2
    CMList(5)=1.0040576846837732841d0
    CMList(6)=1.1224354061131408390d0
    CMList(7)=1.4222975466726096489d0
    CMList(8)=1.8161324493531778915d0
    !!D=3
    CMList(9)=1.011689800230747610892d0
    CMList(10)=1.31622024027879240527d0
    CMList(11)=2.0508591818467041545d0
    CMList(12)=3.0541005910888048873d0

    !!Wave-functions are in log scale   
    CMList=log(CMList)

    listvar=1
    do D=1,3
       do iter=1,4
          !!Initialize kappa-matrix, and other varaibles
          ALLOCATE(kappa_matrix(D,D),ZList(D))
          Zlist=czero
          tau=iunit/iter
          !!Contruct the kappa-matrix
          call hierarchy_k_matrix(D,kappa_matrix)
          call set_trivial_CM_BC(D,kappa_matrix)
          
          !verbose_cm=.true. !!set verbosity
          weight_hier=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,real(Zlist,dp),real(Zlist,dp),tau)
          call force_raw_computation
          weight_raw=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,real(Zlist,dp),real(Zlist,dp),tau)

          CM_facit=CMList(listvar)-log_dedekind_eta(tau)*D
          
          if(test_diff_cmplx(weight_hier,CM_Facit,1.d-11).or.&
               test_diff_cmplx(weight_raw,CM_Facit,1.d-11))then
             write(*,*) 'The two alternatives for D=',d,'tau=I*',aimag(tau)
             write(*,*) 'weight_raw:  ',weight_raw
             write(*,*) 'weight_hier: ',weight_hier
             write(*,*) 'weight_Facit:',CM_Facit
             write(*,*) 'DEdekind_eta:',log_dedekind_eta(tau)
             write(*,*) 'NOT the same'
             call exit(-2)
          end if
          
          DEALLOCATE(kappa_matrix,Zlist)
          listvar=listvar+1
          !write(*,*) '----------NEXT---------------'
       end do
    end do
    
  end subroutine test_plain_cm
  


  subroutine test_whole_z_cm()
    complex(kind=dpc) :: tau=iunit
    complex(kind=dpc), ALLOCATABLE, DIMENSION(:) :: Zlist
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: xlist,ylist
    integer :: D,iter,listvar
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    complex(kind=dpc) :: Weight_hier,weight_raw,CMList(12),CM_facit
    
    write(*,*) 'Test the CM-function with z=tau'
    call unset_CM_BC
    !!D=1
    CMList(1)=12393.647807916698533d0
    CMList(2)=2.0000000000000000848d0
    CMList(3)=1.0000806995175703046d0
    CMList(4)=1.0000000065124121361d0
    !!D=2
    CMList(5)=4.4210171827566189192d13
    CMList(6)=573504.62629423065363d0
    CMList(7)=47.281385278583386957d0
    CMList(8)=1.0037348854877390910d0
    !!D=3  
    CMList(9)=4.5389019606781385740d28
    CMList(10)=4.9335961403544883342d11
    CMList(11)=1.9906873133267526486d7
    CMList(12)=1607.4749665939044154d0

    !!Wave-functions are in log scale       
    CMList=log(CMList)

    listvar=1
    do D=1,3
       do iter=1,4
          !!Initialize kappa-matrix, and other varaibles
          ALLOCATE(kappa_matrix(D,D),ZList(D),XList(D),YList(D))
          Zlist=iunit
          
          tau=iunit*iter
          !!Contruct the kappa-matrix
          call hierarchy_k_matrix(D,kappa_matrix)
          call set_trivial_CM_BC(D,kappa_matrix)

          !verbose_cm=.true. !!set verbosity

          Xlist=real(Zlist,dp)-real(tau,dp)*aimag(Zlist)/aimag(tau)
          Ylist=aimag(Zlist)/aimag(tau)

          weight_hier=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,Xlist,Ylist,tau)
          call force_raw_computation                    
          weight_raw=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,Xlist,Ylist,tau)


          
          CM_facit=CMList(listvar)-log_dedekind_eta(tau)*D

          if(test_diff_cmplx(weight_hier,CM_facit,1.d-13).or.&
               test_diff_cmplx(weight_raw,CM_facit,1.d-13))then
             write(*,*) 'The two alternatives for D=',d,'tau=I*',aimag(tau)
             write(*,*) 'weight_raw:  ',weight_raw
             write(*,*) 'weight_hier: ',weight_hier
             write(*,*) 'weight_Facit:',CM_facit
             write(*,*) 'DEdekind_eta:',log_dedekind_eta(tau)
             write(*,*) 'NOT the same'
             call exit(-2)
          end if
          
          DEALLOCATE(kappa_matrix,Zlist,XList,YList)
          listvar=listvar+1
          !write(*,*) '----------NEXT---------------'
       end do
    end do
    
  end subroutine test_whole_z_cm

  subroutine test_half_z_cm()
    complex(kind=dpc) :: tau=iunit
    complex(kind=dpc), ALLOCATABLE, DIMENSION(:) :: Zlist
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: xlist,ylist
    integer :: D,iter,listvar
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    complex(kind=dpc) :: Weight_hier,weight_raw,CMList(12),CM_facit
    
    write(*,*) 'Test the CM-function with z=tau/2'
    call unset_CM_BC
    !!D=1
    CMList(1)=2.0000000130248242722d0
    CMList(2)=1.0000806995180958531d0
    CMList(3)=1.0000000065124121785d0
    CMList(4)=1.0000000000005255485d0
    !!D=2
    CMList(5)=1072.9945157060170590d0
    CMList(6)=1.0864348112133326886d0
    CMList(7)=1.0000069877095366903d0
    CMList(8)=1.0000000005871768049d0
    !!D=3
    CMList(9)=1.7237348512043467911d6
    CMList(10)=70.422340942170521061d0
    CMList(11)=1.0056023673060814535d0
    CMList(12)=1.0000004521781519628d0
    
    !!Wave-functions are in log scale
    CMList=log(CMList)

    listvar=1
    do D=1,3
       do iter=1,4
          !!Initialize kappa-matrix, and other varaibles
          ALLOCATE(kappa_matrix(D,D),ZList(D),XList(D),YList(D))
          Zlist=iunit/2.d0
         
          tau=iunit*iter
          !!Contruct the kappa-matrix
          call hierarchy_k_matrix(D,kappa_matrix)
          call set_trivial_CM_BC(D,kappa_matrix)

          !verbose_cm=.true. !!set verbosity

          Xlist=real(Zlist,dp)-real(tau,dp)*aimag(Zlist)/aimag(tau)
          Ylist=aimag(Zlist)/aimag(tau)

          weight_hier=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,Xlist,Ylist,tau)
          call force_raw_computation                                        
          weight_raw=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,Xlist,Ylist,tau)


          
          CM_facit=CMList(listvar)-log_dedekind_eta(tau)*D
          
          if(test_diff_cmplx(weight_hier,CM_facit,1.d-14).or.&
               test_diff_cmplx(weight_raw,CM_facit,1.d-14))then
             write(*,*) 'The two alternatives for D=',d,'tau=I*',aimag(tau)
             write(*,*) 'weight_raw:  ',weight_raw
             write(*,*) 'weight_hier: ',weight_hier
             write(*,*) 'weight_Facit:',CM_facit
             write(*,*) 'NOT the same'
             call exit(-2)
          end if
          
          DEALLOCATE(kappa_matrix,Zlist,XList,YList)
          listvar=listvar+1
          !write(*,*) '----------NEXT---------------'
       end do
    end do
    
  end subroutine test_half_z_cm

  
  
  subroutine test_z_arguments_cm()
    complex(kind=dpc) :: tau=iunit
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xlist,Ylist
    complex(kind=dpc), ALLOCATABLE, DIMENSION(:) :: Zlist
    integer :: D,iter
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    complex(kind=dpc) :: Weight_hier,weight_raw

    write(*,*) 'Test the CM-function with random-Z arguments'
    call unset_CM_BC
    
    do D=1,4
       do iter=1,5
          !!Initialize kappa-matrix, and other varaibles
          ALLOCATE(kappa_matrix(D,D))
          ALLOCATE(XList(D),YList(D),ZList(D))
          
          
          CALL INIT_RANDOM_SEED()  !!get new seed every time
          !!Set the center of mass parameters
          call RANDOM_NUMBER(XList)
          call RANDOM_NUMBER(YList)
          XList=(2*XList-1)*D
          YList=(2*YList-1)*D
          Zlist=XList+tau*YList
          
          !!Contruct the kappa-matrix
          call hierarchy_k_matrix(D,kappa_matrix)
          call set_trivial_CM_BC(D,kappa_matrix)

          !verbose_cm=.true. !!set verbosity

          weight_hier=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,Xlist,Ylist,tau)
          call force_raw_computation                                        
          weight_raw=Center_of_Mass_Function(D,kappa_matrix,kappa_matrix*0,Xlist,Ylist,tau)
          
          !verbose_cm=.true. !!set verbosity
          
          if(test_diff_exp_cmplx(weight_raw,weight_hier,1.d-11))then
             write(*,*) 'The two alternatives for D=',D
             write(*,*) 'weight_raw: ',weight_raw
             write(*,*) 'weight_hier:',weight_hier
             write(*,*) 'NOT the same'
             write(*,*) 'Diff size:',abs((weight_raw-weight_hier)/weight_raw)
             write(*,*) 'Input:',Zlist
             call exit(-2)
          end if
          !!FIXME here we test id they are the same
          
          DEALLOCATE(kappa_matrix)
          DEALLOCATE(XList,YList,ZList)
          
       end do
    end do
  end subroutine test_z_arguments_cm



!!!------------------------
!!!       Auxiallry functions
!!!-------------------------

  
  subroutine hierarchy_k_matrix(D,kappa_matrix)
    integer, intent(in) :: D
    integer :: kappa_matrix(D,D),Iter1,Iter2
    do Iter1=1,D
       do Iter2=1,D
          If(Iter1.eq.Iter2)then
             kappa_matrix(Iter1,Iter2)=3
          else
             kappa_matrix(Iter1,Iter2)=2
          end If
       end do
    end do
  end subroutine hierarchy_k_matrix

end program test_com


