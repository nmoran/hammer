program test_chiral_wave_function
  
  USE typedef     ! types and definitions
  use test_utilities
  use k_matrix
  use chiral_wave_function
  use center_of_mass
  use combinations
  use misc_random
  
  !!Variables
  IMPLICIT NONE  
  
!!!Test starts here!!!
  write(*,*) '         Test the Chiral wave-functions'
  !! Testing the many-body wave-function
  call test_non_anti_symmetrization
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  call test_pbc_non_anti_sym(.FALSE.)
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  call test_pbc_non_anti_sym(.TRUE.)
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  call test_many_body_T1_eigs(.FALSE.)
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  call test_many_body_T1_eigs(.TRUE.)
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  
contains  
  
  
  !!----------------------------------------------
  !!     Thest the manu body wave-functions
  !!---------------------------------------------
  subroutine test_pbc_non_anti_sym(TEST_PBCS)
    logical, intent(IN) :: TEST_PBCS
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,kappa_matrix_large,kappa_bar_mat
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer :: q,cells,D,indx,Ne,Ns,raw_k_sector,snum,kappa_bar
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,extra_phase,sval
    
    !!This function serves a double purpouses.
    !!We both test with boundary conditions, and without boundary conditions
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    if(TEST_PBCS)then
       write(*,*) 'Test that correct periodic boundary conditions can be set and are satisfied'
       !!This is really a test of eqn (A34) in PRB 89, 125303 (2014)
       !!with h=t=0
       !! but for the non-chiral case
    else
       write(*,*) 'Test that h=t=0 periodic boundary conditions are satisfied'
       !!This is really a test of eqn (A34) in PRB 89, 125303 (2014)
       !!with h,t,chosen to obtain periodic bcs
       !! but for the non-chiral case
    end if
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    D=1
    ALLOCATE(kappa_matrix(D,D),kappa_bar_mat(D,D))
    do q=1,6
       do cells=1,6 !!Loop over different number of fundamental cells
          Ns=q*cells
          Ne=cells
          
          
          !!Initialize the cooridinates (use unit length in X).
          !!The length scale Lx is thus implicit.
          ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
          allocate(kappa_matrix_large(Ne,Ne))
          
          do kappa_bar=0,3
             !!Initialize kappa-matrix
             kappa_matrix(1,1)=q+kappa_bar
             kappa_bar_mat(1,1)=kappa_bar
             
             do snum=0,5
                sval=snum+rand_num() !!s should be any floating point value
                
                !!Initialize the wave-function
                call initialize_wave_function(D,kappa_matrix,Ne,Ns,(/0/),(/0/),kappa_bar=kappa_bar_mat)
                
                tau=set_tau()
                call RANDOM_NUMBER(Xcoord)
                call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
                Xcoord=Xcoord-.5d0
                Ycoord=Ycoord-.5d0
                
                if(TEST_PBCS)then
                   !!Set the boudnary conditions to periodic
                   raw_k_sector=compute_raw_K_sector(D,Ne,Ns,kappa_matrix-kappa_bar_mat,(/1/),q)
                   !!write(*,*) 'TEST: raw k_sector is',raw_k_sector
                   call set_CM_BC(Ns,raw_k_sector,D,kappa_matrix,kappa_bar_mat,sval=sval)
                else
                   call set_trivial_CM_BC(D,kappa_matrix,kappa_bar_mat,sval=sval)
                   kappa_matrix_large=q
                end if
                !!Compute first wave function
                psi=chiral_raw_psi(Xcoord,Ycoord,tau)
                do indx=1,Ne
                   !!Change the coordinates one by one in x-direction
                   Xcoord2=Xcoord
                   Ycoord2=Ycoord
                   Xcoord2(indx)=Xcoord2(indx)+1.d0
                   psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau)
                   !!Change the coordinates one by one in y-direction
                   Xcoord2=Xcoord
                   Ycoord2=Ycoord
                   Ycoord2(indx)=Ycoord2(indx)+1.d0
                   !!Compute the gaguge phase
                   gauge_phase=Xcoord(indx)*2*pi*Ns
                   psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau)&
                        +iunit*gauge_phase !!Add the gauge phase
                   if(.not.TEST_PBCS)then
                      !!Compute the extra phase (the expected phase)
                      extra_phase=pi*(Ns-kappa_matrix_large(indx,indx))
                      psi_2x=psi_2x+iunit*extra_phase
                      psi_2y=psi_2y+iunit*extra_phase
                   end if
                   !!Reduce to smallest mod_2pi
                   psi_2x=mod_2pi(psi_2x)
                   psi_2y=mod_2pi(psi_2y)
                   
                   !!Test the periodic boundary conditions             
                   if(test_diff_exp_cmplx(psi,psi_2x,1.d-08)&
                        .or.&
                        test_diff_exp_cmplx(psi,psi_2y,1.d-08))then
                      write(*,'(A,A,I3,A)') '.-.-.-.-.-.-.'&
                           ,'Change coordinates for particle no.',indx&
                           ,'.-.-.-.-.-.-'
                      write(*,*) 'Not-periodic'
                      write(*,*) 'For kappa-matrix'
                      call print_int_matrix(kappa_matrix)
                      write(*,*) 'svalue:',sval
                      write(*,*) 'tau:',tau
                      write(*,*) 'cells:',cells
                      call print_the_psis(psi,psi_2x,psi_2y)
                      write(*,*) 'Coordinates Z:'
                      write(*,*) Xcoord+tau*Ycoord
                      call exit(-2)
                   end if
                end do !!Next particle
             end do !!Next svalue
          end do !!Next bar-value
          !!Delaocate the coordinates
          DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
          deallocate(kappa_matrix_large)
       end do !!Next cell size
       !!unset the pbcs at the end
       call unset_CM_BC
    end do !!Next q
    !!Compute next combinations
    DEALLOCATE(kappa_matrix)
  end subroutine test_pbc_non_anti_sym
  
  
  subroutine test_many_body_T1_eigs(TEST_PBCS)
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,kappa_bar_mat
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer :: q,cells,D,Ne,Ns,eigval,raw_k_sector,snum,kappa_bar
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,raw_shift,sval
    logical, intent(IN) :: TEST_PBCS
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    if(TEST_PBCS)then
       write(*,*) 'Test that correct many body T1 eigs can be set and are satisfied'
       !!This is really a test of eqn (A35) in PRB 89, 125303 (2014)
       !!with h=t=0
    else
       write(*,*) 'Test that h=t=0 many body T1 eigs are satisfied'
       !!This is really a test of eqn (A35) in PRB 89, 125303 (2014)
       !!with h,t, set to obatin periodic bcs.
    end if
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    D=1
    ALLOCATE(kappa_matrix(D,D),kappa_bar_mat(D,D))
    do q=1,6
       !write(*,*) 'q:       ',q
       
       do cells=1,6 !!Loop over different number of fundamental cells
          Ns=q*cells
          Ne=cells
          !write(*,*) 'q,Ne:    ',q,Ne
          
          ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
          
          do kappa_bar=0,3
             !!Initialize kappa-matrix
             kappa_matrix(1,1)=q+kappa_bar
             kappa_bar_mat(1,1)=kappa_bar
             
             do snum=0,5
                sval=snum+rand_num()!!s should be any floating point value
                
                call initialize_wave_function(D,kappa_matrix,Ne,Ns,(/0/),(/0/),kappa_bar=kappa_bar_mat)
                !!Initialize the cooridinates (use unit length in X).
                !!The length scale Lx is thus implicit.
                
                
                do eigval=0,q !!Loop over the possible eigenvalues      
                   !write(*,*) 'q,Ne,eig:',q,Ne,eigval
                   tau=set_tau()
                   call RANDOM_NUMBER(Xcoord)
                   call RANDOM_NUMBER(Ycoord)
                   !!Make the random number in the range -1/2 < x < 1/2
                   Xcoord=Xcoord-.5d0
                   Ycoord=Ycoord-.5d0
                   
                   if(TEST_PBCS)then
                      !!Set the boundary conditions to periodic
                      raw_k_sector=compute_raw_K_sector(D,Ne,Ns,kappa_matrix-kappa_bar_mat,(/1/),q)
                      !write(*,*) 'TEST: raw k_sector is',raw_k_sector
                      call set_CM_BC(Ns,raw_k_sector+Ne*eigval,D,kappa_matrix,kappa_bar_mat,sval=sval)
                      !write(*,*) 'The raw1 shift is',raw_shift
                      raw_shift=real(raw_k_sector,dp)/Ns
                      !write(*,*) 'The raw2 shift((2*pi) is',raw_shift
                      raw_shift=raw_shift+eigval*real(1,dp)/q
                      !write(*,*) 'The raw3 shift((2*pi) is',raw_shift
                      raw_shift=2*pi*raw_shift
                   else
                      call set_trivial_CM_BC(D,kappa_matrix,kappa_bar_mat,sval=sval)
                      raw_shift=0.d0
                      !write(*,'(A,A,A)') '...............'&
                      !     ,'we now test that the boudnary conditison work'&
                      !,'....................'
                   end if
                   
                   
                   
                   !!Compute first wave function
                   psi=chiral_raw_psi(Xcoord,Ycoord,tau)
                   
                   !!Change all coordinates one step in the x-direction
                   Xcoord2=Xcoord+1.d0/Ns
                   Ycoord2=Ycoord
                   psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau)
                   
                   !!Change all coordinates q steps in the y-direction
                   !!This tests the q-fold degeracy
                   Xcoord2=Xcoord
                   Ycoord2=Ycoord+real(q,dp)/Ns
                   
                   !!Compute the gaguge phase
                   gauge_phase=sum(Xcoord2)*2*pi*q
                   psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau)&
                        +iunit*gauge_phase !!Add the gauge phase
                   !!Reduce to smallest mod_2pi
                   psi_2x=mod_2pi(psi_2x-iunit*raw_shift)
                   psi_2y=mod_2pi(psi_2y-iunit*q*raw_shift)
                   
                   !!Test the periodic boundary conditions             
                   if(test_diff_exp_cmplx(psi,psi_2x,1.d-7)&
                        .or.&
                        test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                      write(*,'(A,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                           ,'Change global coorinates for',Ne&
                           ,' particles and eigenvalue',eigval&
                           ,'.-.-.-.-.-.-'
                      write(*,*) 'Not-periodic'
                      write(*,*) 'For kappa-matrix'
                      call print_int_matrix(kappa_matrix)
                      write(*,*) 'tau:',tau
                      write(*,*) 'cells:',cells
                      call print_the_psis(psi,psi_2x,psi_2y)
                      write(*,*) 'Coordinates Z:'
                      write(*,*) Xcoord+tau*Ycoord
                      call exit(-2)
                   end if
                end do !!Next eigenvalue
             end do !next svalue
          end do !!Next kappa_bar value
          DeALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
       end do !!Next cell size
       !!unset the pbcs at the end
       call unset_CM_BC
    end do !!Next q
  end subroutine test_many_body_T1_eigs
  
  
  subroutine test_non_anti_symmetrization
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,kappa_bar_mat
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer :: q,cells,D,Ne,Ns,snum,kappa_bar
    integer :: part1,part2
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: sval
    
    call set_cft_wfn_verbose(.FALSE.)
    
    !!This function test that anti-symetrization of the wave-functions can be done
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that the wave function with correct symetries even though no (anti)-symetrization is present'
    
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    D=1 !!!Laugjlin has D=1
    ALLOCATE(kappa_matrix(D,D),kappa_bar_mat(D,D))
    do q=1,6
       !write(*,*) 'q=',q
       do cells=1,5 !!Loop over different number of fundamental cells
          Ns=q*cells
          Ne=cells
          !write(*,*) 'cells:', cells
          ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
          
          do kappa_bar=0,3
             !write(*,*) 'kappa_bar:', kappa_bar
             kappa_matrix(1,1)=q+kappa_bar
             kappa_bar_mat(1,1)=kappa_bar
             !!Here there are three main considerations T_Ns,0,T_0,Ns,and T_0,0             
             do snum=0,5
                sval=snum+rand_num()!!s should be any floating point value
                call set_trivial_CM_BC(D,kappa_matrix,kappa_bar_mat,sval=sval)
                tau=set_tau()
                !!Initialize the cooridinates (use unit length in X).
                !!The length scale Lx is thus implicit.
                call RANDOM_NUMBER(Xcoord)
                !!Make the random number in the range -1/2 < x < 1/2
                Xcoord=Xcoord-.5d0
                !!Place the y-coordiante on a line (so they will not overlap)
                Ycoord = (/(part1/real(Ne,dp), part1=1,Ne)/)-.5d0
                !!write(*,*) 'Ycoord',Ycoord
                
                !!Compute first wave function
                !!psi=chiral_sym_psi(Xcoord,Ycoord,tau,Ne,Ns,kappa_matrix&
                !!,D,shiftlist_x,shiftlist_y)
                call initialize_wave_function(D,kappa_matrix,Ne,Ns,&
                     (/0/),(/0/),asym=.FALSE.,kappa_bar=kappa_bar_mat)
                psi=chiral_sym_psi(Xcoord,Ycoord,tau)
                
                do part1=1,(Ne-1)
                   do part2=(part1+1),Ne
                      !!write(*,*) 'testing',part1,'vs',part2
                      
                      !!Set the new coodinates
                      Xcoord2=Xcoord
                      Ycoord2=Ycoord
                      !!Trade palces opn aprticel one and two
                      Xcoord2(part1)=Xcoord(part2)
                      Ycoord2(part1)=Ycoord(part2)
                      Xcoord2(part2)=Xcoord(part1)
                      Ycoord2(part2)=Ycoord(part1)
                      
                      !!Compute the wfn again
                      !!psi_2x=chiral_sym_psi(Xcoord2,Ycoord2,tau,Ne,Ns&
                      !!,kappa_matrix,D,shiftlist_x,shiftlist_y)
                      psi_2x=chiral_sym_psi(Xcoord2,Ycoord2,tau)
                      
                      !!We expect a shift of pi from the (anti)symetrization
                      !!Reduce to smallest mod_2pi
                      psi_2y=mod_2pi(psi_2x+iunit*pi*q)
                      !!it's sufficient to look at frist kappa-matrix entrry,
                      !!as the diagonals should have the same parity
                      
                      !!Test the periodic boundary conditions             
                      if(test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                         write(*,'(A,A,I3,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                              ,'Not (anti)-summetric for particle no ',part1&
                              ,' and ',part2,' of ',Ne&
                              ,' particles .-.-.-.-.-.-'
                         write(*,*) 'Not-periodic'
                         write(*,*) 'For kappa-matrix'
                         call print_int_matrix(kappa_matrix)
                         write(*,*) 'sval:',sval
                         write(*,*) 'tau:',tau
                         write(*,*) 'cells:',cells
                         write(*,*) 'Output phase different'
                         call print_the_1psis(psi,psi_2x)
                         write(*,*) 'Corrected phase different'
                         call print_the_1psis(psi,psi_2y)
                         write(*,*) 'Coordinates Z:'
                         write(*,*) Xcoord+tau*Ycoord
                         call exit(-2)
                      end if
                   end do !!!Next pair
                end do !!!Next pair
             end do !!! NExt s-value
          end do !!next kappa_bar value
          !!Delaocate the coordinates
          DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
       end do !!Next cell size
       !!Compute next combinations
    end do !!Next q
    DEALLOCATE(kappa_matrix,kappa_bar_mat)
    !!unset the pbcs at the end
    call unset_CM_BC
  end subroutine test_non_anti_symmetrization
  
  !!----------------------------------------------------
  !!                  MISC
  !!....................................................
  
  subroutine print_the_psis(psi,psi_2x,psi_2y)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-x:',mod_2pi(psi_2x)
    write(*,*) 'psi-y:',mod_2pi(psi_2y)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi):',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi):',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_psis
  
  subroutine print_the_1psis(psi,psi_2)
    complex(kind=dpc), intent(in) :: psi,psi_2
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-2:',mod_2pi(psi_2)
    write(*,*) 
    write(*,*) 'diff/(2*pi):',(mod_2pi(psi_2)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_1psis
  
  
  subroutine print_the_3psis(psi,psi_2x,psi_2y,psi_2xy)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y,psi_2xy
    write(*,*) 'psi  : ',mod_2pi(psi)
    write(*,*) 'psi-x: ',mod_2pi(psi_2x)
    write(*,*) 'psi-y: ',mod_2pi(psi_2y)
    write(*,*) 'psi-xy:',mod_2pi(psi_2xy)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi): ',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi): ',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-xy/(2*pi):',(mod_2pi(psi_2xy)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_3psis
  
  
  logical function next_nlist(D,nlist,maxlist,minlist)
    integer, intent(IN) :: D,maxlist(D),minlist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_nlist=.FALSE.
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          !!Not-max (incement)
          nlist(n)=nlist(n)+1
          next_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
  end function next_nlist
  
  
  logical function next_parity_nlist(D,nlist,maxlist,minlist,inclist)
    integer, intent(IN) :: D,maxlist(D),minlist(D),inclist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_parity_nlist=.FALSE.
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          !!Not-max (incement)
          nlist(n)=nlist(n)+inclist(n)
          next_parity_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
  end function next_parity_nlist
  
  logical function same_group(N1,N2,plist,cells) result(same)
!!!Tesst if N1 and N2 are fromt he same group
    integer, intent(in) :: N1,N2,plist(:), cells
    integer :: Nlist(size(plist)),G1,G2,n,D,Ntmp
    !!Initate G1,G2
    G1=0
    G2=0
    Nlist=plist*cells
    D=size(plist)
    Ntmp=0
    do n=1,D
       if(N1.gt.Ntmp.and.N1.le.(Ntmp+Nlist(n)))then
          G1=n !!This gets set eventually
       end if
       if(N2.gt.Ntmp.and.N2.le.(Ntmp+Nlist(n)))then
          G2=n !!This gets set eventually
       end if
       Ntmp=Ntmp+Nlist(n)
    end do
    !write(*,*) "N1,N2:",N1,N2
    !write(*,*) "G1,G2:",G1,G2
    same=(G1.eq.G2)
  end function same_group
  
  real(kind=dp) function rand_num() result(my_rand)
    call RANDOM_NUMBER(my_rand)
  end function rand_num
  
  complex(kind=dpc) function set_tau() result(my_tau)
    real(kind=dp) :: tau_x,tau_y
    !Put tau_x in range -1/2 < tau_x < 1/2
    tau_x=rand_num()-.5
    !Put tau_y in range 1/2 < tau_x < 1+1/2
    tau_y=rand_num()+.5
    my_tau=tau_x+iunit*tau_y
  end function set_tau
  
end program test_chiral_wave_function
