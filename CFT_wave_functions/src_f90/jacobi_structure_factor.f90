module jacobi_structure_factor
  USE typedef     ! types and definitions
  use jacobitheta
  implicit none
  
  !! Allocatalbe variables for the function
  !! Will store the values of the Z_factor up to the called level
  !! Will only store for one value of \tau at a time.
  complex(KIND=dpc) :: tau_value
  !! Before any allocation has taken place, the allocation level is zero
  integer :: allocation_level=0
  !! The matrix that stores the Z_factor values
  complex(KIND=dpc), dimension(:,:), allocatable :: Z_values
  logical, dimension(:,:), allocatable :: stored_Z_values
  
  private
  public :: Z_factor,reset_memory_usage,print_memory_status
  
contains


  !!-----------------------------------------
  !!   Miscelaneous functions
  !!----------------------------------------

  !! The Z-factor for fourier expanding theta-functions
  !! The Z-factor is realted to theta3 as
  !!
  !! theta3(z,tau)^N = sum_K e^(i*pi*tau K^2/N) *
  !!                     * e^(2*i*pi*K*z) * Z_factor(K,N,tau)
  complex(KIND=dpc) recursive function Z_factor(K_in,N,tau) result(Z_res)
    integer, intent(IN) :: K_in,N
    complex(KIND=dpc), intent(IN) :: tau
    integer :: K
    !!!Shift K to the desired range 1=<K=<N
    K=modulo(K_in-1,N)+1

    !write(*,*) ' -  -  -  -  -  - '
    !write(*,*) 'Resquesting data on',N,K
    !write(*,*) 'for tau=',tau

    !! And the value of \tau is correct
    if(tau.eq.tau_value)then
       !write(*,*) 'Correct tau value:',tau_value
       !!Test if valeus are allocated to the desired level
       if(allocation_level.ge.N)then
          !write(*,*) 'Level ',N,'allocated'
          !! And the particuall Z_K(N) has been allocated
          if(stored_Z_values(N,K))then
             !write(*,*) 'Z_value stored for (',N,',',K,')'
             !! Read the desired values from the table
             Z_res=Z_values(N,K)
          else !! The desired value is not stored
             !write(*,*) 'No stored value for (',N,',',K,')'
             !!Compute and store Z_value
             Z_res = compute_store_Z(K,N,tau)
          end if
       else
          !!Compute the desired value
          !write(*,*) 'Allocation level ',allocation_level,'<',N,'to small'
          !!Extend the alocated memory to the desired desired level
          call exted_memory_level(N,allocation_level)
          !!Compute and store Z_value
          Z_res = compute_store_Z(K,N,tau)
       end if
    else !!Wrong tau value - reset and try from the start
       !write(*,*) 'Wrong tau value -- reset'
       call reset_memory_usage
       tau_value=tau
       !!Call the function again
       z_res=Z_factor(K,N,tau)
    end if
  end function Z_factor


  complex(KIND=dpc) recursive function compute_store_Z(K,N,tau) result(Z_res)
    integer, intent(IN) :: N,K
    complex(KIND=dpc), intent(IN) :: tau
    !!Compute the desired value
    Z_res = compute_Z_factor(K,N,tau)
    !write(*,*) 'Computed Z_factor',N,K,tau
    !write(*,*) 'Z_res:',Z_res
    !!Store the value in memory
    Z_values(N,K)=Z_res
    stored_Z_values(N,K)=.TRUE.
    !call print_memory_status
  end function compute_store_Z



  complex(KIND=dpc) recursive function compute_Z_factor(K,N,tau) result(Z_res)
    integer, intent(IN) :: K,N
    complex(KIND=dpc), intent(IN) :: tau
    integer :: int_k
    complex(KIND=dpc) :: theta
    !write(*,*) 'Computing Z_factor',N,K,tau

    if(N.eq.1)then
       Z_res=(1.0d0,0.d0)
    else
       Z_res=0.d0
       do int_k=1,N-1
          theta=thetagen(1.d0*K/N+1.d0*int_K/(N-1),&
               0.d0,czero,N*(N-1)*tau)
          if(isnan(abs(theta)))then
             write(*,*) 'N,K,theta:',N,K,theta
             call exit(-1)
          end if
          Z_res=Z_res+Z_factor(int_k,N-1,tau)*theta
       end do
    end if
  end function Compute_Z_factor

  subroutine reset_memory_usage()
    !!Reset the memory usage
    !write(*,*) 'Reseting memory'
    if(allocation_level.ne.0)then
       deallocate(Z_values,stored_Z_values)
       allocation_level=0
    end if
    !write(*,*) 'Deallocation complete!'
  end subroutine reset_memory_usage


  subroutine exted_memory_level(Desired_level,current_level)
    !!Extend the current allocation of memory to the desired level
    integer, intent(IN) :: Desired_level,current_level
    complex(KIND=dpc) :: Z_values_temp(current_level,current_level)
    logical :: stored_Z_values_temp(current_level,current_level)
    integer :: N,K
    
    if(current_level.eq.0)then
       !write(*,*) 'Memory allocated'
       !!Data should not be allocated
       allocate(Z_values(Desired_level,Desired_level))
       allocate(stored_Z_values(Desired_level,Desired_level))
       !!Reset the newly allcoated memory
       Z_values=0.d0
       stored_Z_values=.FALSE.
    else
       !write(*,*) 'Memory extended'
       !!!Temporary store the existing data
       Z_values_temp=Z_values
       stored_Z_values_temp=stored_Z_values
       !!Deallocate the memory
       deallocate(Z_values,stored_Z_values)
       !!Allocate the new memory space
       allocate(Z_values(Desired_level,Desired_level))
       allocate(stored_Z_values(Desired_level,Desired_level))
       !!Reset the newly allcoated memory
       Z_values=0.d0
       stored_Z_values=.FALSE.
       !!Store the temporay values in the new memory space
       do N=1,current_level
          do K=1,current_level
             Z_values(N,K)=Z_values_temp(N,K)
             stored_Z_values(N,K)=stored_Z_values_temp(N,K)
          end do
       end do
    end if
    allocation_level=desired_level   
    !write(*,*) 'Allocation complete!'
    !call print_memory_status
    
  end subroutine exted_memory_level

  
  subroutine print_memory_status
    !!Print the memory value usage
    integer :: N
    
    write(*,*) '..................'
    do N=1,allocation_level
       write(*,*) stored_Z_values(N,:)
    end do
    write(*,*) '..................'
    
    write(*,*) '.-.-.-.-.-.-.-.-.-.-.'
    do N=1,allocation_level
       write(*,*) Z_values(N,:)
    end do
    write(*,*) '.-.-.-.-.-.-.-.-.-.-.'
    
  end subroutine print_memory_status



  
  
end module jacobi_structure_factor
