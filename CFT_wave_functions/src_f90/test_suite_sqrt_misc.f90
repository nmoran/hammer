program test_sqrt_misc

  USE typedef     ! types and definitions
  use test_utilities
  use misc_frac_sqrt
  use permutations
  use channels
  
  implicit none


  !!Test start here!!
      write(*,*) '        Test the misc sqrt module'
      
      !! Testing msic sqrt
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_number_of_permutations
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_the_permutations
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_gcd_0
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_sqrt_reduction
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  
  contains
  
  subroutine test_number_of_permutations
    integer :: n,k,NumPerms,count,tot
    type(permutation) :: perm
    integer, allocatable, dimension(:) :: order
    
    write(*,*) 'test that permutations gives the desired list'
    call perm_initialize(perm)
    do n=1,4
       ToT=sum((/(k,k=1,n)/))
       allocate(order(ToT)) !!Allocate the expected order
       NumPerms=multinom((/(k,k=1,n)/))
       count = 0
       write(*,*) 'Create perm level',n
       write(*,*) 'Expecting',NumPerms,'permutations'
       call perm_create(perm,(/(k,k=1,n)/))
       do 
          if(.not.perm_ok(perm)) exit
          call perm_next(perm)
          count = count +1 !!Step up counter
       end do
       call perm_destroy(perm)
       if(count.ne.NumPerms)then
          write(stderr,*) '----------tp----------'
          write(stderr,*) 'ERROR: Number of permutations does now match expected'
          write(stderr,*) 'Computed:',count,'Expected:',NumPerms
          call exit(-1)
       end if
       deallocate(order)
    end do
  end subroutine test_number_of_permutations
  
  
  subroutine test_the_permutations
    integer :: k,ToT,n
    write(*,*) 'test that permutations gives the desired list'
    do n=1,4
!!!ON this side we can expand the test by adding more combinations, but lets be happy with thsi for now...
       ToT=sum((/(k,k=1,n)/))
       call test_perm(ToT,(/(k,k=1,n)/))
    end do
  end subroutine test_the_permutations
  

  subroutine test_perm(ToT,Slist)
    integer, intent(In) :: Slist(:),ToT
    integer :: i, Ordering(ToT)
    type(permutation) :: perm    
    integer :: wSn(size(Slist)),wRn(size(Slist)),wTn(size(Slist))
    wSn=Slist
    call set_R_list(wRn,wSn)
    wTn=wSn+wRn
    
    call perm_initialize(perm)
    call perm_create(perm,wSn)
    
    Ordering = (/(i,i=1,ToT)/)
    call do_test_perm(1,Ordering,0,perm,wSn,wRn,wTn,ToT)
  end subroutine test_perm
  
  recursive subroutine do_test_perm(level,Ordering,ord_par,perm,wSn,wRn,wTn,ToT)
    !!This code contins a loop variant of creating permutations...
    !!We use it to evaluate the perm struct....
    
    type(permutation),intent(INOUT) :: perm
    integer, intent(in) :: ToT,level,Ordering(ToT),ord_par
    integer, intent(in) :: wSn(:),wRn(:),wTn(:)
    integer :: wSize
    integer :: SRange(wSn(level))
    integer :: RRange(wRn(level))
    integer :: TRange(wTn(level))
    integer :: Sorder(wSn(level))
    integer :: Rorder(wRn(level))
    integer :: New_Ordering(ToT)
    
    integer :: Tn,Sn,Rn,i,new_ord_par
    type(combination) :: comb

    !!For testing
    integer :: test_order(ToT),test_ord_par,n,m,raw_order(ToT)
    
    wSize = size(wSn)
    
    Rn = wRn(level)
    Sn = wSn(level)
    Tn = Sn + Rn

    !!write(*,*) '---------------'
    !!write(*,*) 'level:',level
    !!write(*,*) 'wSn:',wSn
    !!write(*,*) 'ToT:',ToT

    SRange = (/(i,i=ToT-Tn+1,ToT-Rn)/)
    RRange = (/(i,i=ToT-Rn+1,ToT)/)
    TRange = (/(i,i=ToT-Tn+1,ToT)/)
    !!write(*,*) 'SRange:',Srange
    !!write(*,*) 'RRange:',Rrange
    !!write(*,*) 'TRange:',Trange
    
    if((wSize.eq.1).or.(level.eq.wSize))then
       !!NO reoering neede at this, the last,  level
       !!Testing!!!
       test_order = perm_get_order(perm)
       test_ord_par = perm_get_parity(perm)
       call perm_next(perm)
       !!Test parity!!
       if(test_ord_par.ne.ord_par)then
          write(stderr,*) 'ERROR: wrong_parity'
          write(stderr,*) 'ord_par:',ord_par,'test_ord_par:',test_ord_par
          write(stderr,*) 'order:     ',Ordering
          write(stderr,*) 'test_order:',test_order
          call exit(-1)
       end if
       !!Test_ordering
       do n=1,ToT
          if(test_order(n).ne.Ordering(n))then
             write(stderr,*) 'ERROR: wrong_Order'
             write(stderr,*) 'ord_par:',ord_par,'test_ord_par:',test_ord_par
             write(stderr,*) 'order:     ',Ordering
             write(stderr,*) 'test_order:',test_order
             raw_order = (/(m,m=1,ToT)/)
             raw_order(test_order)=raw_order
             write(stderr,*) 'raw_order1:',raw_order
             raw_order = (/(m,m=1,ToT)/)
             raw_order=raw_order(test_order)
             write(stderr,*) 'raw_orde21:',raw_order
             call exit(-1)
          end if
       end do
    else !!!start permuting
       call createCombination(comb)
       call initializeCombination(comb,Tn,Sn)  
       do !!If the combination computed is ok, do stuff
          IF ( .NOT. Comb%ok ) EXIT
          !!write(*,*) 'Old order:'
          !!call write_compactly(Ordering)
          Sorder = comb%C
          Rorder = Pcomp2(Tn,comb%C)
          !!write(*,*) 'S-Combination:',Sorder
          !!write(*,*) 'R-Combination:',Rorder          
          New_Ordering = Ordering
          New_Ordering(SRange) = Ordering(TRange(Sorder))
          New_Ordering(RRange) = Ordering(TRange(Rorder))
          !!write(*,*) 'New order:'
          !!call write_compactly(New_Ordering)
          
          new_ord_par=modulo(ord_par+SUM(Sorder),2)
          call do_test_perm(level+1,New_Ordering,new_ord_par,perm,&
               wSn,wRn,wTn,ToT)
          CALL nextCombination(Comb)
       end do
       CALL destroyCombination(Comb)
    end if
    
  end subroutine do_test_perm
    

  subroutine test_gcd_0
    integer :: n,gcd_res
    write(*,*) 'test that n=gcd(n,0)'
    do n=-10,10
       gcd_res=gcd(n,0)
       if(gcd_res.ne.n)then
          write(*,*) '-----tg0---'
          write(*,*) 'ERROR:'
          write(*,*) 'Computerd gcd(',n,',0)'
          write(*,*) 'And expected',n,' but got',gcd_res
          call exit(-2)
       end if
    end do
  end subroutine test_gcd_0

  subroutine test_sqrt_reduction
    integer, parameter :: size=2
    integer :: test_c(size),test_R(size)
    integer :: res_c(size),res_R(size)
    integer :: facit_c(size),facit_R(size)
    integer :: n
    
    write(*,*) 'test that sqrt reduction is possible'    
    test_c=(/160,-160/)
    test_r=(/2100,2100/)
    facit_c=(/16,-16/)
    facit_r=(/21,21/)
    do n=1,size
       !!perform the reduction
       call do_reduce_sqrt_fraction(test_c(n),test_r(n),res_c(n),res_r(n))
       if((res_c(n).ne.facit_c(n)).or.(res_r(n).ne.facit_r(n)))then
          write(*,*) '-----tsr---'
          write(*,*) 'ERROR:'
          write(*,*) 'Tried reduction of',test_c(n),'/sqrt(',test_r(n),')'
          write(*,*) 'to ',facit_c(n),'/sqrt(',facit_r(n),')'
          write(*,*) 'but got:',res_c(n),'/sqrt(',res_r(n),')'
          call exit(-2)
       end if
    end do
       
    
  end subroutine test_sqrt_reduction
  
end program test_sqrt_misc
