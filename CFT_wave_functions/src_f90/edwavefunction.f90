MODULE EDWaveFunction
  
  use basis_function !  Module containing the basis functions
  use typedef
  use channels
  
  IMPLICIT NONE
  
  !!Global parameters usefull for this module
  integer :: Ns, Ne  !!NUmber of fluxes and electrons
  
  complex(KIND=dpc), PRIVATE, DIMENSION(:), POINTER :: vect
  logical :: ed_wfn_initialized=.FALSE.
  
  private
  public ed_psi, init_edwf
  
CONTAINS
  
  
  SUBROUTINE init_edwf(ivect,Ns_in,Ne_in)
    complex(KIND=dpc), DIMENSION(:), TARGET :: ivect
    integer, intent(IN) :: Ne_in,Ns_in
    call test_initialized_basis
    vect=>ivect          !Communicate the global variables to big_psi
    Ne=Ne_in
    Ns=Ns_in
    ed_wfn_initialized=.TRUE.
  END SUBROUTINE init_edwf
  
  FUNCTION ed_psi( xlist, ylist ,tau_in,Xqps,Yqps) result(ibig_psi)
    ! Calculates the value of the many-body wave function
    ! using tabulated data
    complex(kind=dpc) :: ibig_psi !!Result
    REAL(kind=dp), INTENT(IN) :: xlist(:),ylist(:)
    complex(kind=dpc), INTENT(IN) :: tau_in !!NB: Input put not used
    real(KIND=dp), optional, intent(In),dimension(:) :: Xqps,Yqps!!NB, input not used, added for compatibility with chiral_sym_psi

    COMPLEX(KIND=dpc) :: psi_tab(Ne,0:(Ns-1))
    INTEGER :: i
    
    if(.not.ed_wfn_initialized)then !!Check initialization
       write(stderr,*) 'ERROR: ED wave function fock_coeffs not intiialized'
       write(stderr,*) 'Call init_edwf first'
       call exit(-1)
    end if
    
    !!Trivial input checking
    if(size(Xlist).ne.Ne)then
       write(stderr,*) '----ep---'
       write(stderr,*) 'ERROR:'
       write(stderr,*) 'The dimention of input vector X, differs fromt the initialized size'
       call exit(-1)
    end if
    if(size(Ylist).ne.Ne)then
       write(stderr,*) '----ep---'
       write(stderr,*) 'ERROR:'
       write(stderr,*) 'The dimention of input vector Y, differs fromt the initialized size'
       call exit(-1)
    end if
    
    !! tabulate the large matrix containing all the single particle orbitals

    psi_tab = single_particle_matrix(xlist,ylist,Ns)
    
    !! Add all antisymetrized multi-electron components together
    ibig_psi=0.d0
    DO i=1, SIZE(vect)
       ibig_psi = ibig_psi + vect(i)*ifock(i,psi_tab)
       !! FIXME -- TEST that no nan:s are produced - to be removed
       if(isnan(abs(ibig_psi)))then
          write(*,*) 'vect,fock:',vect(i),ifock(i,psi_tab)
       end if
    END DO
  END FUNCTION ed_psi
  
    
    FUNCTION ifock( ind , psi_tab)
    USE fock_basis
    USE matrix
    COMPLEX(KIND=dpc), intent(IN) :: psi_tab(Ne,0:Ns-1)
    INTEGER, INTENT(IN) :: ind
    ! returns the wave function of the ind:th Fock basis state
    ! evaluated at x
    COMPLEX(KIND=dpc) :: ifock
    INTEGER :: oc(Ne)
    INTEGER :: k
    COMPLEX(dpc) :: Sup(Ne,Ne)
    
    oc = ibasis(ind,:)
    DO k=1, Ne
       ! fill in the rows with tabulated data
       Sup(k,:)= psi_tab(:, oc(k))
    END DO
    ! normalized slater determinant
    ifock=det(Sup)
    if(isnan(abs(ifock)))then
       !!FIXME --- is 
       write(stderr,*) '------WARNIGN:----------'
       write(stderr,*) 'Singular matrix, replacing the determinant with zero'
       ifock=0.0d0
    end if
    
  END FUNCTION ifock
  
END MODULE EDWaveFunction
