MODULE global_torus

  USE typedef     ! types and definitions

  IMPLICIT NONE

  !****************************************************************************
  !
  ! Global variables.
  !
  !****************************************************************************

  REAL(KIND=dp), PUBLIC :: sqrt_pi ! double prec
  REAL(KIND=dp), PUBLIC :: two_pi  ! double prec
  COMPLEX(KIND=dpc), PUBLIC :: itwo_pi   ! double prec

  
  ! Geometry
  REAL(KIND=dp) :: tau_im_dp  ! Ly/Lx
  REAL(KIND=dp) :: tau_re_dp  ! Ldelta/Lx
  REAL(KIND=dp) :: Lx              ! torus generator Lx ( Lx*Ly=Ns/2\pi )
  REAL(KIND=dp) :: Ly              ! torus generator Lx ( Lx*Ly=Ns/2\pi )
  REAL(KIND=dp) :: ldelta          ! torus generator LDelta = 0 (on the rectangle)
  REAL(KIND=dp) :: one_over_lx     ! 1/Lx
  REAL(KIND=dp) :: two_pi_over_lx
  REAL(KIND=dp) :: two_pi_over_ly
  
  COMPLEX(KIND=dpc) :: tau_dpc     ! (Ldelta+i*Ly)/Lx  OBS: Do not confuse with tau_dp
  
  ! Quantum numbers
  INTEGER :: working_Ns                ! Number of fluxes 
  REAL(KIND=dp) :: working_Ns_dp       ! Number of fluxes (real number)
 
  
CONTAINS
  
  ! OBS: Call this before you do anything:
  
  SUBROUTINE input_torus(Ns,tau_re,tau_im)
    integer, intent(IN) ::  Ns
    real(kind=dp), intent(in) :: tau_re,tau_im
    ! Read parameters for the torus, define some constants
    
    ! define some pi's
    sqrt_pi=SQRT(pi)
    two_pi = 2.d0*pi
    itwo_pi = iunit*two_pi
    
    
    !!Set Ns
    working_Ns=Ns
    working_Ns_dp=REAL(working_Ns,dp)
    !!Set torsu specifics
    tau_im_dp=tau_im
    tau_re_dp=tau_re
    
    Lx = SQRT(two_pi*working_Ns_dp/tau_im_dp)
    Ly = tau_im_dp*Lx
    Ldelta = tau_re_dp*Lx
    
    WRITE(*,'(/,TR5,A)') '*******************************************'
    WRITE(*,'(TR5,A)') '          Lx          Ly      Ldelta'
    WRITE(*,'(TR5,3F12.5)') Lx,Ly,LDelta
    WRITE(*,'(TR5,A,/)') '*******************************************'
    
    !! some usefull constants
    one_over_lx = 1.d0/lx
    two_pi_over_lx = two_pi/lx
    two_pi_over_ly = two_pi/ly
    tau_dpc = tau_re_dp+iunit*tau_im_dp
  END SUBROUTINE input_torus

END MODULE global_torus
