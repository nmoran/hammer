module fock_coefficients_storage
  
  use typedef
  use bits_manipulation
  use sort
  
  implicit none
  
  !! Allocatalbe fock coefficents for the laughlin states
  !! Will store the values of the Laughlin_states up to the called level
  !! Will only store for one value of \tau at a time.
  complex(KIND=dpc) :: tau_stored
  !! Before any allocation has taken place, the allocation level is zero
  integer :: allocation_level=0,Kvalue_stored
  !! The matrix that stores the Z_factor values
  complex(KIND=dpc), dimension(:), allocatable :: Fock_coeffs
  logical, dimension(:), allocatable :: stored_Fock_coeffs
  integer(kind=8), dimension(:), allocatable :: state_num_to_binary
  integer, dimension(:), allocatable :: basis_sizes,basis_sizes_cumulative
  
  private
  public :: reset_memory_usage, get_fock_coeff, set_fock_coeff, memory_status
  
contains
  
  !!------------------------------------------
  !! Public functions
  !!------------------------------------------
  subroutine reset_memory_usage()
    !!Reset the memory usage
    write(*,*) '-.-RMU-.-'
    write(*,*) 'Resetting fock coeff memory!'
    if(allocation_level.ne.0)then
       deallocate(Fock_coeffs,stored_Fock_coeffs,state_num_to_binary,basis_sizes,basis_sizes_cumulative)
       allocation_level=0
       tau_stored=czero
       Kvalue_stored=0
    end if
    write(*,*) 'Deallocation complete!'
  end subroutine reset_memory_usage
  
  subroutine memory_status()
    !!Print the memory usage
    write(*,*) '-.-.-.-MS-.-.-.-'
    write(*,*) 'Current Memory status'
    write(*,*) 'allocation_level:',allocation_level
    write(*,*) 'tau_stored:',tau_stored
    write(*,*) 'Kvalue_stored:',Kvalue_stored
    write(*,*) 'basis sizes:',basis_sizes
    write(*,*) 'State allocation:',stored_Fock_coeffs
    write(*,*) 'Binary representation:',state_num_to_binary
    write(*,*) '-.-.-.-.-.-.-.-'
  end subroutine memory_status
  
  
  
  subroutine get_fock_coeff(Ne,tau,DT_list,Kvalue,fock_value,succesfull)
    integer, intent(IN) :: Ne,DT_list(Ne),Kvalue
    complex(kind=dpc),intent(In) :: tau
    logical, intent(OUT) :: succesfull
    complex(kind=dpc),intent(OUT) :: fock_value
    integer :: mom_list(Ne)
    
    !!NB!
    !!The function assumes that DT has propper balance and parity, so no such checks are made
    mom_list=modulo((DT_list+(Ne-1)*Kvalue)/2,Ne*Kvalue)
    
    !write(*,*) '-------GFC--------(1)'
    !write(*,*) 'Requesting Data for:'
    !write(*,*) 'Kval:',Kvalue,'tau:',tau
    !write(*,*) 'Momentum:',mom_list
    
    if(Kvalue.ne.Kvalue_stored)then !!Wrong K-value
       succesfull=.FALSE.
       fock_value=czero
    elseif(Ne.gt.allocation_level)then !!Allocation not done to desired level
       succesfull=.FALSE.
       fock_value=czero
    elseif(tau.ne.tau_stored)then !!Wrong tau_value
       succesfull=.FALSE.
       fock_value=czero
    else
       !write(*,*) '-------GFC--------(2)'
       !write(*,*) 'Looking up data'
       !!Get the fock state to lookup
       call extract_data(mom_list,fock_value,Ne,Kvalue,succesfull)
    end if
    
    !write(*,*) '-------GFC--------(3)'
    !write(*,*) 'Data lookuc sucess state:',succesfull
    
  end subroutine get_fock_coeff
  
  subroutine set_fock_coeff(Ne,tau,DT_list,Kvalue,fock_value)
    integer, intent(IN) :: Ne,DT_list(Ne),Kvalue
    complex(kind=dpc),intent(In) :: tau,fock_value
    integer :: mom_list(Ne),Ne_counter
    !!write(*,*) 'store the value',fock_value
    
    mom_list=modulo((DT_list+(Ne-1)*Kvalue)/2,Ne*Kvalue)
    
    !write(*,*) '-------SFC--------'
    !write(*,*) 'Storing data for:'
    !write(*,*) 'Kval:',Kvalue,'tau:',tau
    !write(*,*) 'Momentum:',mom_list
    !write(*,*) 'Value:', fock_value
    
    
    !!Reset the memory if Kvalue or Tau are wrong
    if(Kvalue.ne.Kvalue_stored)then !!Wrong K-value
       call reset_memory_usage()
       call initilize_memory_usage(Kvalue,tau)
    elseif(tau.ne.tau_stored)then !!Wrong tau_value
       call reset_memory_usage()
       call initilize_memory_usage(Kvalue,tau)
    end if
    
    
    !!Check theere is enough data allocated
    if(Ne.gt.allocation_level)then
       !!Extend the allocated memory
       write(*,*) '-------SFC--------'
       write(*,*) 'Extend allocated memory'
       do Ne_counter=(allocation_level+1),Ne !!Extend by one level at a time
          call extend_allocated_memory(Ne,Kvalue)
       end do
    end if

    !write(*,*) '-------SFC--------'
    !write(*,*) 'Enough allocated memory'
    
    !!Insert the desired value
    !!FIXME -- implement from here
    !write(*,*) '-------SFC--------'
    !write(*,*) 'Insert the desired value'
    call insert_data(mom_list,fock_value,Ne,Kvalue)
    !write(*,*) 'Data insertion completed'
    
  end subroutine set_fock_coeff
  
  !!---------------------------------------------
  !! Private functions
  !!---------------------------------------------
  
  
  
  integer function get_fock_state(mom_list,Ne,Kvalue) result(state_num)
    integer, intent(IN) :: Ne,mom_list(Ne),Kvalue
    integer :: Ns,shift_mom_list(Ne),i,Ns_effective
    integer :: sort_order(Ne) !!Dummy variable
    integer(kind=8) :: binary_state

    Ns=Ne*Kvalue
    !write(*,*) ',,,,,,,,,GFS,,,,,,,,,,,'
    !write(*,*) 'Get the number of the fock state'
    !write(*,*) 'Mom_list: ',mom_list
    !!Make sue nubmering is 1...Ns
    shift_mom_list=modulo(mom_list-1,Ns)+1
    if(modulo(Kvalue,2).eq.0)then
       !write(*,*) ',,,,,,,,,GFS,,,,,,,,,,,'
       !!write(*,*) 'Even K-value'
       !!Order the state in acending order
       call sort_int(1*shift_mom_list,shift_mom_list,sort_order,Ne)
       !! Add extra momentum for binary construction
       do i=2,Ne
          shift_mom_list(i)=shift_mom_list(i)+i-1
       end do
       Ns_effective=Ns+Ne-1
    else
       Ns_effective=Ns
    end if
    binary_state=reverse_bits(list_to_bit(shift_mom_list,Ne),Ns_effective)
    !write(*,*) ',,,,,,,,,GFS,,,,,,,,,,,'
    !write(*,*) 'Mom_list2:',shift_mom_list
    !write(*,*) 'Binary state:', binary_state
    !!call write_bits(binary_state,Ns_effective)
    
    state_num=search_list_decending_integer8(&
         binary_state,&
         state_num_to_binary((basis_sizes_cumulative(Ne-1)+1):basis_sizes_cumulative(Ne)),basis_sizes(Ne))
    !write(*,*) ',,,,,,,,,GFS,,,,,,,,,,,'
    !write(*,*) 'Found the state to be:',state_num
  end function get_fock_state
  
  integer function get_basis_size(Ne,Kvalue) result(basis_size)
    USE Combinations
    integer, intent(IN) :: Ne,Kvalue
    integer :: Ns,Ns_effective,Perm_mom,target_mom,n
    TYPE(Combination) :: Permutation
    !!Computes the basis size needed to store for Ne particles
    Ns=Ne*Kvalue
    target_mom=modulo(Kvalue*(Ne*(Ne-1))/2,Ns)
    
    if(modulo(Kvalue,2).eq.1)then !!Odd K-value
       Ns_effective=Ns
    else !!Even K-value
       Ns_effective=Ns+Ne-1
    end if
    
    !!Code inspired from fock_basis.f90 (rev 1531)
    !!Create a basis to work with
    CALL createCombination(Permutation)
    CALL initializeCombination(Permutation,Ns_effective,Ne)
    
    !!Look through all combinations
    basis_size=0
    !write(*,*) '--------GBS---------'
    !write(*,*) 'Target momentum:',target_mom
    DO ! First, let's find the number of basis states
       IF ( .NOT. Permutation%ok ) EXIT
       !write(*,*) Permutation%C
       if(modulo(Kvalue,2).eq.1)then !!Odd K-value
          perm_mom=modulo(sum(Permutation%C),Ns)
       else !!Even K-value
          perm_mom=0
          do n=1,Ne
             perm_mom=perm_mom+Permutation%C(n)-n+1
          end do
          perm_mom=modulo(perm_mom,Ns)
       end if
       
       ! This checks that the quantum numbers make up good states:
       if(perm_mom.eq.target_mom)then
          !write(*,*) '--------GBS---------'
          !write(*,*) 'ok!'
          !write(*,*) Permutation%C
          basis_size=basis_size+1
       END IF
       CALL nextCombination(Permutation)
    END DO
    write(*,*) '--------GBS---------'
    WRITE(*,*) ' Many-body basis size is ', basis_size
    
  end function get_basis_size
  
  subroutine insert_data(mom_list,fock_value,Ne,Kvalue)
    integer, intent(IN) :: Ne,mom_list(Ne),Kvalue
    integer :: fock_state,parity_sign
    complex(kind=dpc), intent(IN) :: fock_value
    
    fock_state=get_fock_state(mom_list,Ne,Kvalue)
    parity_sign=permutation_parity(mom_list,Ne)**Kvalue
    
    if(Ne.eq.1)then
       Fock_coeffs(fock_state)=fock_value*parity_sign
       stored_Fock_coeffs(fock_state)=.true.
    else
       Fock_coeffs(fock_state+basis_sizes_cumulative(Ne-1))=fock_value*parity_sign
       stored_Fock_coeffs(fock_state+basis_sizes_cumulative(Ne-1))=.true.
    end if
    !write(*,*) '-.-.-.-.-ID-.-.-.-.-'
    !write(*,*) 'Data inserted i memory'
    !write(*,*) 'The allocation of the library currently'
    !write(*,*) stored_Fock_coeffs
    
  end subroutine insert_data
  
  
  subroutine extract_data(mom_list,fock_value,Ne,Kvalue,Sucess)
    integer, intent(IN) :: Kvalue,Ne,mom_list(Ne)
    integer :: fock_state,parity_sign
    complex(kind=dpc), intent(OUT) :: fock_value
    logical, intent(OUT) :: Sucess
    
    fock_state=get_fock_state(mom_list,Ne,Kvalue)
    parity_sign=permutation_parity(mom_list,Ne)**Kvalue
    
    if(Ne.eq.1)then
       fock_value=Fock_coeffs(fock_state)*parity_sign
       Sucess=stored_Fock_coeffs(fock_state)
    else
       fock_value=Fock_coeffs(fock_state+basis_sizes_cumulative(Ne-1))*parity_sign
       Sucess=stored_Fock_coeffs(fock_state+basis_sizes_cumulative(Ne-1))
    end if
    !write(*,*) '-.-.-.-.-ED-.-.-.-.-'
    !write(*,*) 'Data retrieved from memory'
    
  end subroutine extract_data
  
  
  
  
  !!--------------------------------------
  !!        Memory manipulation
  !!--------------------------------------
  
  subroutine initilize_memory_usage(Kvalue,tau)
    integer, intent(IN) :: Kvalue
    complex(kind=dpc),intent(In) :: tau
    
    !!Initalize the data
    tau_stored=tau
    Kvalue_stored=Kvalue
    allocation_level=1
    !!Allocate memoty for level 1
    allocate(Fock_coeffs(1),stored_Fock_coeffs(1),state_num_to_binary(1))
    allocate(basis_sizes(1),basis_sizes_cumulative(1))
    !!Set the varaibles for level 1
    Fock_coeffs=czero
    stored_Fock_coeffs=.false.
    basis_sizes=1  !!One state at Ne=1
    basis_sizes_cumulative=1
    state_num_to_binary=1  !!Momentum labeled 1 - Ns
  end subroutine initilize_memory_usage
  
  subroutine extend_allocated_memory(Ne,Kvalue)
    integer, intent(IN) :: Kvalue,Ne
    complex(KIND=dpc) :: tmp_Fock_coeffs(basis_sizes_cumulative(allocation_level))
    logical :: tmp_stored_Fock_coeffs(basis_sizes_cumulative(allocation_level))
    integer(kind=8) :: tmp_state_num_to_binary(basis_sizes_cumulative(allocation_level))
    integer :: tmp_basis_sizes(allocation_level),tmp_basis_sizes_cumulative(allocation_level)
    
    if(Ne.ne.(allocation_level+1))then
       write(*,*) '..........EAM.........'
       write(*,*) 'ERROR:'
       write(*,*) 'Memmory must be extended sequentially'
       call exit(-1)
    end if
    
    !!Copy the data
    !write(*,*) '..........EAM.........'
    !write(*,*) ' copying data   '
    tmp_Fock_coeffs=Fock_coeffs
    tmp_stored_Fock_coeffs=stored_Fock_coeffs
    tmp_state_num_to_binary=state_num_to_binary
    tmp_basis_sizes=basis_sizes
    tmp_basis_sizes_cumulative=basis_sizes_cumulative
    
    !write(*,*) '..........EAM.........'
    !write(*,*) ' deallocating data'
    deallocate(Fock_coeffs,stored_Fock_coeffs,state_num_to_binary,&
         basis_sizes,basis_sizes_cumulative)
    !write(*,*) '..........EAM.........'
    !write(*,*) ' r-eallocating data (step 1)'
    allocate(basis_sizes(Ne),basis_sizes_cumulative(Ne))
    basis_sizes(1:(Ne-1))=tmp_basis_sizes
    basis_sizes_cumulative(1:(Ne-1))=tmp_basis_sizes_cumulative
    !!Compute the next basis size
    basis_sizes(Ne)=get_basis_size(Ne,Kvalue)
    basis_sizes_cumulative(Ne)=basis_sizes_cumulative(Ne-1)+basis_sizes(Ne)
    !write(*,*) '..........EAM.........'
    !write(*,*) ' r-eallocating data (step 2)'
    allocate(Fock_coeffs(basis_sizes_cumulative(Ne)))
    allocate(stored_Fock_coeffs(basis_sizes_cumulative(Ne)))
    allocate(state_num_to_binary(basis_sizes_cumulative(Ne)))
    !write(*,*) '..........EAM.........'
    !write(*,*) ' copying data back'
    Fock_coeffs(1:basis_sizes_cumulative(Ne-1))=tmp_Fock_coeffs
    stored_Fock_coeffs(1:basis_sizes_cumulative(Ne-1))=tmp_stored_Fock_coeffs
    state_num_to_binary(1:basis_sizes_cumulative(Ne-1))=tmp_state_num_to_binary
    !write(*,*) '..........EAM.........'
    !write(*,*) ' resetting the new memory'
    Fock_coeffs((basis_sizes_cumulative(Ne-1)+1):basis_sizes_cumulative(Ne))=czero
    stored_Fock_coeffs((basis_sizes_cumulative(Ne-1)+1):basis_sizes_cumulative(Ne))=.false.
    call enumerate_basis(state_num_to_binary((basis_sizes_cumulative(Ne-1)+1):basis_sizes_cumulative(Ne)),basis_sizes(Ne),Ne,Kvalue)
    
    allocation_level=Ne
    !write(*,*) '..........EAM.........'
    !write(*,*) 'memory extention complete'
    !call memory_status  
  end subroutine extend_allocated_memory
  
  
  subroutine enumerate_basis(state_list,size,Ne,Kvalue)
    USE Combinations
    integer, intent(IN) :: size, Ne,Kvalue
    integer(kind=8), intent(OUT) :: state_list(size) !!A bit-list
    integer :: Ns,Ns_effective,Perm_mom,target_mom,n,basis_size
    TYPE(Combination) :: Permutation
    
    !write(*,*) ',,,,,,,,,,,EB,,,,,,,,,,,,,,,,'
    !write(*,*) 'Enumerating the new basis'
    !! Number of states
    Ns=Ne*Kvalue
    !! Target momentum
    target_mom=modulo(Kvalue*(Ne*(Ne-1))/2,Ns)
    
    !!Different code depending on K od or even
    if(modulo(Kvalue,2).eq.1)then !!Odd K-value
       Ns_effective=Ns
    else !!Even K-value
       Ns_effective=Ns+Ne-1
    end if
    
    !!Code inspired from fock_basis.f90 (rev 1531)
    !!Create a basis to work with
    CALL createCombination(Permutation)
    CALL initializeCombination(Permutation,Ns_effective,Ne)
    
    !!Look through all combinations
    !! We now know that the basis size is equal to size
    basis_size=0
    !write(*,*) '--------EB---------'
    !write(*,*) 'Target momentum:',target_mom
    DO ! First, let's find the number of basis states
       IF ( .NOT. Permutation%ok ) EXIT
       !write(*,*) Permutation%C
       if(modulo(Kvalue,2).eq.1)then !!Odd K-value
          perm_mom=modulo(sum(Permutation%C),Ns)
       else !!Even K-value
          perm_mom=0
          do n=1,Ne
             perm_mom=perm_mom+Permutation%C(n)-n+1
          end do
          perm_mom=modulo(perm_mom,Ns)
       end if
       
       ! This checks that the quantum numbers make up good states:
       if(perm_mom.eq.target_mom)then
          !write(*,*) '--------EB---------'
          !write(*,*) 'ok!'
          !!Store the basis information in the state list
          basis_size=basis_size+1
          !! binary (reverse) representation
          state_list(basis_size)=reverse_bits(list_to_bit(Permutation%C,Ne),Ns_effective)
          !write(*,*) '--------EB---------'
          !write(*,*) 'Bit num:', state_list(basis_size)
          !call write_bits(state_list(basis_size),Ns_effective)
          !! The list of states will be in revese order
       END IF
       CALL nextCombination(Permutation)
    END DO
    !write(*,*) '--------EB---------'
    !WRITE(*,*) ' Many-body basis size is ', basis_size
  end subroutine enumerate_basis
  
  
  integer function permutation_parity(Permutation,Ne) result(part)
    integer, intent(IN) :: Permutation(Ne),Ne
    integer :: n1,n2
    
    part=1
    do n1=1,(Ne-1)
       do n2=(n1+1),Ne
          if(Permutation(n1).lt.Permutation(n2))then
             part=-part
          end if
       end do
    end do
  end function permutation_parity
  
end module fock_coefficients_storage
