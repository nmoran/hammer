MODULE Combinations
  
  USE Channels

  IMPLICIT NONE

! Typically, you need to do an operation for all $k$-combinations of 
! $N$ objects.
! You are thus tempted to write something like
! \begin{verbatim}
! ...
! DO C(1) = 1,N-k+1
!  DO C(2) = C(1)+1,N-k+2
!   ...
!   DO C(k) = C(k-1)+1,N
!    ...
!   END DO
!  END DO
! END DO
! ...
! \end{verbatim}
! Unfortunately, it is not possible to write an undetermined number of nested 
! \texttt{DO}-loops. Luckily, using this module, you can do the job easily:
! \begin{verbatim}
! ...
! USE Combinations
! Type(Combination) :: C
! ...
! CALL createCombination(C)
! CALL initializeCombination(C,n,k)
! DO
!   IF ( .NOT. C%ok ) EXIT
!   ... ! Here C%C(1:k) contains the current combination 
!   CALL nextCombination(C)
! END DO
! ...
! \end{verbatim} 
! This module gives the combinations in lexicographic order, e.g.,
! 123, 124, 134, 234. \\

! The algorithm was written by Tuomas Torsti 
! (\texttt{tto@hugo.hut.fi}) in 1998.
! User interface was later modified, and the \texttt{binom} function 
! written by Sami Siljam�ki (\texttt{sps@hugo.hut.fi}).
! 
! Binom deleted by aph, some minor changes.

  TYPE Combination
    INTEGER :: N                          ! Number of possible values
    INTEGER :: k                          ! This is a $k$-combination 
    INTEGER :: INDEX                      ! Bookkeeping... 
    INTEGER,POINTER, DIMENSION(:) :: C    ! The actual combination  
    LOGICAL :: ok                         ! TRUE if last oper. succeeded.
    LOGICAL :: status                     ! TRUE if allocated
  END TYPE Combination
  
CONTAINS

  subroutine printcombination(C)
    TYPE(Combination) :: C
    integer :: i,j

    do j=1,(C%C(1)-1)
       write(*,'(A)',advance='no') 'O '
    end do
    write(*,'(A)',advance='no') '1 '
    DO i=2,C%k
       do j=(C%C(i-1)+1),(C%C(i)-1)
          write(*,'(A)',advance='no') 'O '
       end do
       write(*,'(A)',advance='no') '1 '
    end DO
    do j=(C%C(C%k)+1),C%N
       write(*,'(A)',advance='no') 'O '
    end do
    write(*,*)

  end subroutine printcombination

    


  SUBROUTINE createCombination(C)
    TYPE(Combination) :: C

    C%status = .FALSE.
    NULLIFY(C%C)
    
  END SUBROUTINE createCombination


  SUBROUTINE destroyCombination(C)
    TYPE(Combination) :: C
    INTEGER :: deallocstat

    C%status = .FALSE.
    DEALLOCATE(C%C, STAT=deallocstat )
    IF ( deallocstat > 0 ) THEN
      WRITE(STDERR,*)'ERROR (module Combinations): failed to deallocate memory,'
      WRITE(STDERR,*)'                             STATUS =',deallocstat
      RETURN
    END IF
    
  END SUBROUTINE destroyCombination

  SUBROUTINE initializeCombination(C,N,k)
    TYPE(Combination) :: C
    INTEGER :: N,k

    INTEGER :: i
    INTEGER :: allocstat, deallocstat

    IF ( ASSOCIATED( C%C ) ) THEN 
      DEALLOCATE( C%C , STAT=deallocstat )
      IF ( deallocstat > 0 ) THEN
        WRITE(STDERR,*)'ERROR (module Combinations): failed to deallocate memory,'
        WRITE(STDERR,*)'                             STATUS =',deallocstat
        RETURN
      END IF
    END IF

    C%status = .TRUE.
    C%ok = .TRUE.

!aph    ALLOCATE(C%C(k+1), STAT=allocstat)
    ALLOCATE(C%C(k), STAT=allocstat)
    IF ( allocstat > 0 ) THEN
      C%status = .FALSE.
      C%ok = .FALSE.
      WRITE(STDERR,*)'ERROR (module Combinations): failed to allocate memory,'
      WRITE(STDERR,*)'                             STATUS =',allocstat
      RETURN
    END IF

    C%N = N
    C%k = k
    C%index = k
    
    DO i=1,k
      C%C(i)=i
    END DO
!aph    C%C(k+1)=N+1

  END SUBROUTINE initializeCombination

  SUBROUTINE nextCombination(C)
    TYPE(Combination) :: C
    INTEGER :: i
    LOGICAL :: nextFound
    !call printcombination(C)
    IF ( C%C(1) == C%N - C%k + 1 ) THEN ! The last combination:
      C%ok = .FALSE.
      RETURN
    ELSE ! This is NOT the last combination.
      nextFound=.FALSE.
      DO WHILE ( .NOT. nextFound)
         IF (C%C(C%index) == C%N - C%k + C%index) THEN
            DO i=C%index,C%k
               C%C(i) = C%C(C%index-1) + 2 + (i - C%index)
            END DO
            C%index = C%index-1
            C%C(C%index)=C%C(C%index)+1
            nextFound=.TRUE.
         else if(C%index == C%k)then 
            if(C%C(C%index) .ne. C%N)then
               C%C(C%index) = C%C(C%index) + 1
               nextFound=.TRUE.
            end if
         ELSE IF (C%C(C%index) == C%C(C%index+1)-1) THEN
            C%index = C%index+1
         ELSE
            C%C(C%index) =C%C(C%index)+1
            nextFound=.TRUE.
         END IF
      END DO
   END IF
   
  END SUBROUTINE nextCombination
  
END MODULE Combinations
