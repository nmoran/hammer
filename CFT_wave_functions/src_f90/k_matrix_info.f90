program K_matrix_info
  
  use input_misc
  use k_matrix
  use misc_frac_sqrt
  use misc_matrix
  use test_utilities
  
  implicit none
  !!Global variables
  integer :: Groups,n,q_group,p_group,p,q,cells_group
  integer :: q_mat_group,p_mat_group,p_mat,q_mat
  integer :: q_bar_group,p_bar_group,p_bar,q_bar
  integer, allocatable, Dimension(:,:) :: Imatrix,Cmatrix,Lmatrix
  integer, allocatable, Dimension(:,:) :: Ibar,Cbar,Lbar
  integer, allocatable, Dimension(:,:) :: Iminus,Cminus,Lminus
  integer, allocatable, Dimension(:,:) :: kappa_matrix,kappa_bar,Kplus,Kminus
  integer, allocatable, Dimension(:) :: Pminus,Plist_mat,Plist_bar
  integer, allocatable, Dimension(:) :: Rlist,Rminus,Rbar
  
  logical::set_groups,set_kappa,set_kappa_bar,set_kplus,set_kminus,chiral_kappa,set_cells_group
  
  !!Set all the flags to negative
  set_groups=.FALSE.
  set_kappa=.FALSE.
  set_kappa_bar=.FALSE.
  set_kplus=.FALSE.
  set_kminus=.FALSE.
  set_cells_group=.FALSE.

  chiral_kappa=.FALSE.
  
  
  call retreive_input()
  call check_kmatrix_consistency()
  call print_and_compute_inverse()
  call compute_filling_fraction_and_sizes()
  call compute_and_plot_charge_vectors()
  call determine_quantum_numbers()
  
  
contains
  
  subroutine retreive_input()
    integer::narg,cptArg
    character(len=20)::name
    !!Number of inputs to skip
    integer::skip_next
    
    !!Initally this sytem will barf (in an uncontrolled way) if the input is wrong
    
    !Check if arguments are found
    narg=command_argument_count()
    
    skip_next=0
    if(narg>0)then
       !loop across options
       do cptArg=1,narg
          if(skip_next.gt.0)then
             !!Skip the next skip_next arguments
             !write(*,*) 'skip an argument'
             skip_next=skip_next-1
             cycle
          end if
          !!Get the comand argument
          call get_command_argument(cptArg,name)
          !!Select from all the caes
          select case(adjustl(name))
             !!First we test against flags
          case("--version")
             write(*,*) "This is program K_matrix_stat : Version 0.1"
             call exit(0)
          case("--help","-h")
             call print_help()
             call exit(0)
          case("-D","--groups")
             call set_unset_flag(set_groups,'-D',print_help)
             call recieve_integer_input(groups,'groups',cptArg+1)
             call check_positive_integer(groups,'-D',print_help)
             !!Allocate all the K-matrices
             allocate(kappa_matrix(groups,groups))
             allocate(kappa_bar(groups,groups))
             allocate(Kplus(groups,groups))
             allocate(Kminus(groups,groups))
             !!Allocate all the Stat-matrices
             allocate(Imatrix(Groups,Groups))
             allocate(Cmatrix(Groups,Groups))
             allocate(Lmatrix(Groups,Groups))
             allocate(Ibar(Groups,Groups))
             allocate(Cbar(Groups,Groups))
             allocate(Lbar(Groups,Groups))
             allocate(Iminus(Groups,Groups))
             allocate(Cminus(Groups,Groups))
             allocate(Lminus(Groups,Groups))
             !!Allocate the lists
             allocate(Plist_mat(Groups))
             allocate(Plist_bar(Groups))
             allocate(Rlist(Groups))
             allocate(Pminus(Groups))
             allocate(Rminus(Groups))
             allocate(Rbar(Groups))
             skip_next=1
          case("-c","--cells_group")
             call set_unset_flag(set_cells_group,'-c',print_help)
             call recieve_integer_input(cells_group,'cells_group',cptArg+1)
             call check_positive_integer(cells_group,'-c',print_help)
             skip_next=1
          case("--kappa_matrix","--kappa-matrix","--kappa")
             if(set_groups)then 
                call import_set_matrix(set_kappa,'--kappa_matrix'&
                     ,kappa_matrix,cptArg,'kappa-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--kappa_matrix','-D',print_help)
             end if
          case("--kappa_bar","--K-bar","--kbar","--k-bar")
             if(set_groups)then 
                call import_set_matrix(set_kappa_bar,'--kappa_bar'&
                     ,kappa_bar,cptArg,'K-bar-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--kappa_bar','-D',print_help)
             end if
          case("--kappa_bar-diag","--kbar-diag")
             if(set_groups)then 
                call import_set_diag_matrix(set_kappa_bar,'--kappa_bar-diag'&
                     ,kappa_bar,cptArg,'K-bar-matrix',groups,print_help)
                skip_next=groups
             else
                call  error_flag_dependency('--kappa_bar','-D',print_help)
             end if
          case("--Kplus","--K-plus","--kplus","--k-plus")
             if(set_groups)then 
                call import_set_matrix(set_kplus,'--Kplus'&
                     ,Kplus,cptArg,'K-plus-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--Kplus','-D',print_help)
             end if
          case("--Kmatrix","--K-matrix","--kmatrix","--k-matrix")
             if(set_groups)then 
                call import_set_matrix(set_kminus,'--Kminus'&
                     ,Kminus,cptArg,'K-minus-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--Kminus','-D',print_help)
             end if
          case default
             call print_help()
             write(stderr,*)"ERROR: Input"
             write(stderr,*)"Option: ",name," at position",cptArg,"is unknown"
             write(*,*)
             do n=max(cptArg-4,1),min(cptArg+4,narg)
                if(n.eq.cptArg)then
                   write(stderr,*) '                  ----|  |-----'
                   write(stderr,*) '                  ----v  v-----'
                end if
                call get_command_argument(n,name)
                   write(stderr,*)'arg no:',n,': "',trim(name),'"'
                if(n.eq.cptArg)then
                   write(stderr,*) '                  ----^  ^-----'
                   write(stderr,*) '                  ----|  |-----'
                end if
             end do

             write(*,*)
             call exit(-2)
          end select
       end do
    endif
    
  end subroutine retreive_input
  
  
  subroutine print_help()
    write(*,'(A)') 'Usage: k_matrix_stats [OPTION]...'
    write(*,'(A)') 'Compute information for a k-matrix'
    write(*,*) ''
    write(*,*) '------- kappa-matrix specifics -------'
    write(*,*) '  Only -D and --kappa_matrix has to be supplied, then -p and -q'
    write(*,*) '  will be compared to the internally computed values.'
    write(*,*) ''
    write(*,*) ' -D,  --groups  <D>         Dimention of the kappa-matrix,'
    write(*,*) '                            equals the number of groups.'
    write(*,*) ' --Kmatrix <k11 ... kDD>    Entries to the K--matrix    -- || --'
    write(*,*) ' --kappa <k11 ... kDD>      Entries to the kappa-matrix    row by row.'
    write(*,*) ' --kappa_bar <k11 ... kDD>  Entries to the kappa_bar-matrix  -- || --'
    write(*,*) ' --Kplus <k11 ... kDD>      Entries to the K+-matrix    -- || --'
    write(*,*) ' --K???-diag <k1 kD>        Only the diagonal entries'
    write(*,*) '                            Flag -D has to be set bofore any kappa-matrix is given.'
    write(*,*) '                            All K-matrices have to be symetric.'
    write(*,*) '                            The internal consistency'
    write(*,*) '                            of the differnt K-matrices is:'
    write(*,*) '                            Kplus   = kappa_matrix + kappa_bar'
    write(*,*) '                            Kmatrix = kappa_matrix - kappa_bar'
    write(*,*) '                            Default kappa_bar = 0,'
    write(*,*) '                            equivalent to chiral kappa-matrix'
    write(*,*) ''
    write(*,*) ' -c,  --cells  <c>          Number of unit cells in TT-limit'
    write(*,*) '                            use to compute many-body quantum numbers'   
    write(*,*) ''
    write(*,*) '  -h, --help                Print this help and exit' 
    write(*,*) ''
    write(*,*) "....................................."
    write(*,*) "           kappa-matrix stats            "
    write(*,*) "....................................."
    write(*,*) "This programs takes a given kappa-matrix and computes:"
    write(*,*) "  * The filling fraction"
    write(*,*) "  * Disposition of grups"
    write(*,*) "  * Minimal Unit Cell"
    write(*,*) "  * If the kappa-matrix is a chiral hierarchy state"
    write(*,*) "  * The charge and dual latice of the kappa-matrix"
    write(*,*) "    Provided it can be constructed"
    write(*,*) "  * Also gives the quantum number of the different states"
    write(*,*) "------------------------------------"
    write(*,*)
    write(*,'(A)') 'Report k_matrix_start bugs to micke.fremling@gmail.com'
    write(*,*) 
  end subroutine print_help
  
  
  
  
  subroutine plot_charge_vectors()
    logical, allocatable :: Gamma_lattice(:,:),Dual_lattice(:,:)
    integer :: size_x,size_y,cells_x,cells_y
    integer :: m,n !!dummu indexes
    integer :: x_coord,y_coord
    !!If the charge lattice is 2-dimentional,
    !!we plot the charge lattice and dual lattice
    !!in units of the compactification radii.
    if(groups.ne.2)then
       write(*,*) "Lattice has wrong dimention to plot"
       return
    end if
    write(*,*) 'Plotting the charge lattice, and dual lattice'
    
    !!Draw a lattice on a grid first, then plot the grid
    !!Decide the size of the charge lattice as 2*longest vector in that direction.
    
    cells_x=2
    cells_y=2
    size_x=cells_x*max(Cmatrix(1,1),Cmatrix(2,1))
    size_y=cells_y*max(Cmatrix(1,2),Cmatrix(2,2))
    write(*,*) 'Lattice dimentions:',size_x,'x',size_y
    !!Allocate (with natural indexation), and initalize
    allocate(Gamma_lattice(-size_x:size_x,-size_y:size_y))
    allocate(Dual_lattice(-size_x:size_x,-size_y:size_y))
    Gamma_lattice=.FALSE.
    Dual_lattice=.FALSE.
    !!Populate the gamma_lattice in a simple wave
    do m=-3*size_x,3*size_x
       do n=-3*size_y,3*size_y
          !!First version (of cells_x=cells_y we only need this)
          x_coord=m*Cmatrix(1,1)+n*Cmatrix(2,1)
          y_coord=m*Cmatrix(1,2)+n*Cmatrix(2,2)
          !write(*,*) 'm,n,x,y',m,n,x_coord,y_coord
          if((x_coord.ge.(-size_x)).and.(x_coord.le.size_x).and.&
               (y_coord.ge.(-size_y)).and.(y_coord.le.size_y))then
             !write(*,*) 'Store point'
             Gamma_lattice(x_coord,y_coord)=.TRUE.
          end if
          
          !! Do the same for the dual lattice
          x_coord=m*Lmatrix(1,1)+n*Lmatrix(2,1)
          y_coord=m*Lmatrix(1,2)+n*Lmatrix(2,2)
          !write(*,*) 'm,n,x,y',m,n,x_coord,y_coord
          if((x_coord.ge.(-size_x)).and.(x_coord.le.size_x).and.&
               (y_coord.ge.(-size_y)).and.(y_coord.le.size_y))then
             !write(*,*) 'Store point'
             Dual_lattice(x_coord,y_coord)=.TRUE.
          end if
          
       end do
    end do
    
    !!"Plot the lattice"
    do m=-size_y,size_y
       do n=-size_x,size_x
          if(Gamma_lattice(n,-m))then
             write(*,'(A)',advance='no') 'x '
          elseif(Dual_lattice(n,-m))then
             write(*,'(A)',advance='no') '* '
          else
             write(*,'(A)',advance='no') '. '
          end if
       end do
       !write(*,*) ''
       write(*,*) ''
    end do
    
    
  end subroutine plot_charge_vectors
  
  subroutine plot_unit_charge_vectors()
    logical, allocatable :: Gamma_lattice(:,:),Dual_lattice(:,:)
    integer :: size_x_min,size_x_max,size_y_min,size_y_max,size_x,size_y
    integer :: m,n !!dummu indexes
    integer :: x_coord,y_coord
    !!If the charge lattice is 2-dimentional,
    !!we plot the charge lattice and dual lattice
    !!in units of the compactification radii.
    if(groups.ne.2)then
       write(*,*) "Lattice has wrong dimention to plot"
       return
    end if
    write(*,*) 'Plotting the unit cell of the charge lattice, and dual lattice'
    
    !!Draw a lattice on a grid first, then plot the grid
    
    !!The size of the grid is from the origin to the vector (q1+q2)
    
    size_x_min=min(0,Cmatrix(1,1),Cmatrix(2,1),Cmatrix(1,1)+Cmatrix(2,1))
    size_x_max=max(0,Cmatrix(1,1),Cmatrix(2,1),Cmatrix(1,1)+Cmatrix(2,1))
    size_y_min=min(0,Cmatrix(1,2),Cmatrix(2,2),Cmatrix(1,2)+Cmatrix(2,2))
    size_y_max=max(0,Cmatrix(1,2),Cmatrix(2,2),Cmatrix(1,2)+Cmatrix(2,2))

    write(*,*) 'x_min,x_max,y_min,y_max:'
    write(*,*) size_x_min,size_x_max,size_y_min,size_y_max

    size_x=size_x_max-size_x_min
    size_y=size_y_max-size_y_min
    write(*,*) 'Lattice dimentions:',size_x,'x',size_y
    !!Allocate (with natural indexation), and initalize
    allocate(Gamma_lattice(size_x_min:size_x_max,size_y_min:size_y_max))
    allocate(Dual_lattice(size_x_min:size_x_max,size_y_min:size_y_max))
    Gamma_lattice=.FALSE.
    Dual_lattice=.FALSE.
    !!Populate the gamma_lattice in a (stupid but effective) way
    do m=-3*size_x,3*size_x
       do n=-3*size_y,3*size_y
          !!First version (of cells_x=cells_y we only need this)
          x_coord=m*Cmatrix(1,1)+n*Cmatrix(2,1)
          y_coord=m*Cmatrix(1,2)+n*Cmatrix(2,2)
          if((x_coord.ge.size_x_min).and.(x_coord.le.size_x_max).and.&
               (y_coord.ge.size_y_min).and.(y_coord.le.size_y_max))then
             !write(*,*) 'Store gamma point'
             !write(*,*) 'm,n,x,y',m,n,x_coord,y_coord

             Gamma_lattice(x_coord,y_coord)=.TRUE.
          end if
          
          !! Do the same for the dual lattice
          x_coord=m*Lmatrix(1,1)+n*Lmatrix(2,1)
          y_coord=m*Lmatrix(1,2)+n*Lmatrix(2,2)
          if((x_coord.ge.size_x_min).and.(x_coord.le.size_x_max).and.&
               (y_coord.ge.size_y_min).and.(y_coord.le.size_y_max))then
             !write(*,*) 'Store dual point'
             !write(*,*) 'm,n,x,y',m,n,x_coord,y_coord
          
             Dual_lattice(x_coord,y_coord)=.TRUE.
          end if
          
       end do
    end do
    
    !!"Plot the lattice"
    do m=-size_y_max,-size_y_min !!Count backwards from the top
       do n=size_x_min,size_x_max
          !!Check if point is in the unit cell.
          !! Here we use that all entries are positive, so matrix unit cell is leaning toward right.
          !!We also use that q1 = (q1,0)  so we need only consider inclusion in the x-direction.
          !! Inclusion in y-direction is trivial.
          !! It is important that the point is to the right of q2 but to the left of q2+q1.
          !! This happens if the stepens of the point (x,y) is steper that the vector q2=(n,m).
          !! Thus include if n/m > x/y or ny > mx.
          if(((n*Cmatrix(2,2)).ge.((-m)*Cmatrix(2,1))).and.&
               (((n-Cmatrix(1,1))*Cmatrix(2,2)).le.((-m)*Cmatrix(2,1))))then
             if(Gamma_lattice(n,-m))then
                write(*,'(A)',advance='no') 'x '
             elseif(Dual_lattice(n,-m))then
                write(*,'(A)',advance='no') '* '
             else
                write(*,'(A)',advance='no') '. '
             end if
          else
             write(*,'(A)',advance='no') '  '
          end if
       end do
       !write(*,*) ''
       write(*,*) ''
    end do
    
    
  end subroutine plot_unit_charge_vectors
  
  
  subroutine print_and_compute_inverse()
    integer :: determinant
    write(*,*) 'Your kappa-matrix is:'
    call print_int_matrix(Kminus)
    if(.not.chiral_kappa)then
       write(*,*) 'with chiral kappa-matrix:'
       call print_int_matrix(kappa_matrix)
       write(*,*) 'and anti-chiral kappa-matrix:'
       call print_int_matrix(kappa_bar)
    end if
    
    !! Compute the inverse kappa-matrix
    call kappa_matrix_inverse(Groups,Kminus,Imatrix,q_group)
    
    write(*,*)
    write(*,'(A,I3,A)') 'The inverese kappa-matrix (divided by ',q_group,') is:'
    call print_int_matrix(Imatrix)
    
    write(*,*) 
    if(is_hierarchy_k_matrix(Groups,Kminus))then
       write(*,'(A)') 'The matrix IS a hierarchy kappa-matrix'
    else
       write(*,'(A)') 'The matrix IS NOT a hierarchy kappa-matrix'
    end if


    !!Compute the determiant also
    determinant=determinant_int_mat(Kminus)
    write(*,*) 'The determinant is:',determinant

  end subroutine print_and_compute_inverse
  
  subroutine compute_filling_fraction_and_sizes  
    !! Compute the group groups
    write(*,*) '-----------------------------'
    write(*,*) '      K-minus groups size '
    write(*,*) '-----------------------------'

    call kappa_matrix_group_sizes(Groups,Kminus,pminus,p_group,q_group)
    write(*,*) 
    call Filling_fraction(Groups,pminus,q_group,p,q)
    write(*,'(A,I3,A,I3)') 'The filling fraction is: ',p,'/',q
    
    write(*,*) 
    write(*,'(A,I3)') 'The minimal cell size is: ',q_group
    
    write(*,*) 
    write(*,'(A,I3,A,I1,A)') 'The minimal cell has ',p_group,' paricles distributed on ',Groups,' groups as:'
    do n=1,Groups
       write(*,'(A,I3,A,I3,A)') 'Group ',n,': ',pminus(n),' particle(s)'
    end do


    if(.not.chiral_kappa)then !!If there are both mat and K-bar components
       !! Compute the group groups
       write(*,*) '-----------------------------'
       write(*,*) '      kappa-matrix non chiral groups size '
       write(*,*) '-----------------------------'
       call kappa_matrix_group_sizes(Groups,kappa_matrix,plist_mat,p_mat_group,q_mat_group)
       
       write(*,*) 
       call Filling_fraction(Groups,plist_mat,q_mat_group,p_mat,q_mat)
       write(*,'(A,I3,A,I3)') 'The filling fraction is: ',p_mat,'/',q_mat
       
       write(*,*) 
       write(*,'(A,I3)') 'The minimal cell size is: ',q_mat_group
       
       write(*,*) 
       write(*,'(A,I3,A,I1,A)') 'The minimal cell has ',p_mat_group,' paricles distributed on ',Groups,' groups as:'
       do n=1,Groups
          write(*,'(A,I3,A,I3,A)') 'Group ',n,': ',plist_mat(n),' particle(s)'
       end do
       
       
       !! Compute the group groups
       write(*,*) '-----------------------------'
       write(*,*) '      K-bar groups size '
       write(*,*) '-----------------------------'
       call kappa_matrix_group_sizes(Groups,kappa_bar,plist_bar,p_bar_group,q_bar_group)
       
       write(*,*) 
       call Filling_fraction(Groups,plist_bar,q_bar_group,p_bar,q_bar)
       write(*,'(A,I3,A,I3)') 'The filling fraction is: ',p_bar,'/',q_bar
       
       write(*,*) 
       write(*,'(A,I3)') 'The minimal cell size is: ',q_bar_group
       
       write(*,*) 
       write(*,'(A,I3,A,I1,A)') 'The minimal cell has ',p_bar_group,' paricles distributed on ',Groups,' groups as:'
       do n=1,Groups
          write(*,'(A,I3,A,I3,A)') 'Group ',n,': ',plist_bar(n),' particle(s)'
       end do


       !!Test groups are of equal size!
       if((.not.all(pminus.eq.plist_bar)).or.(.not.all(pminus.eq.plist_mat)))then
          write(*,*) 'ERROR: Group sizes'
          write(*,*) 'The group size of the differen matrixes are:'
          write(*,*) 'K-minus: ',pminus
          write(*,*) 'kappa-matrix:',plist_mat
          write(*,*) 'K-bar:   ',plist_bar
          write(*,*) '  which are not equal!'
          write(*,*) '  This will not constitute a good wave function'
          write(*,*) '  quitting...'
          call exit(-2)
       else
          write(*,*) 'All group divisions are of equal size'
       end if
       
    end if

  end subroutine compute_filling_fraction_and_sizes
  
  subroutine compute_and_plot_charge_vectors
    if(chiral_kappa)then
       write(*,*) 
       write(*,*) 'The charge lattice of the kappa-matrix is'
       call kappa_charge_lattice(Groups,kappa_matrix,Cmatrix,Rlist)  
       call print_int_matrix(Cmatrix)
       write(*,*) 'with compactification radii:'
       write(*,*) Rlist
       
       write(*,*)
       write(*,*) 'The dual charge lattice of the kappa-matrix is'
       call charge_lattice_to_inverse_lattice(Groups,Cmatrix,Rlist,Lmatrix)
       call print_int_matrix(Lmatrix)
       write(*,*) 'with compactification radii:'
       write(*,*) Rlist
       
       call plot_charge_vectors()
       call plot_unit_charge_vectors()
    else
       write(*,*) 
       write(*,*) 'The charge lattice of the chiral kappa-matrix is'
       call kappa_charge_lattice(Groups,kappa_matrix,Cmatrix,Rlist)  
       call print_int_matrix(Cmatrix)
       write(*,*) 'with compactification radii:'
       write(*,*) Rlist

       write(*,*) 
       write(*,*) 'The charge lattice of the anti-chiral kappa-matrix is'
       call kappa_charge_lattice(Groups,kappa_bar,Cbar,Rbar)  
       call print_int_matrix(Cbar)
       write(*,*) 'with compactification radii:'
       write(*,*) Rbar

       call compute_dual_lattice()

    end if
    
  end subroutine compute_and_plot_charge_vectors

  subroutine compute_dual_lattice()
    !!Compute the dual lattice of the non-chiral charge lattice,
    !!This has two kinds of vectors, the dual vectors, and null-vectors
    !!We first compute a set of change vectors and a set of null-vectors
    !!These will not all be othogonal, so next we orthogonalize
    integer :: Ccharge(Groups,2*Groups),Rcharge(2*Groups)
    integer :: Cdual(Groups,2*Groups),Cnull(Groups,2*Groups)
    integer :: Cdualnull(2*Groups,2*Groups),Rdualnull(2*Groups,2*Groups)
    integer :: Rdual(Groups,2*Groups),Rnull(Groups,2*Groups)
    integer :: tmpRmat(Groups,Groups),tmpCmat(Groups,Groups)
    integer :: tmpMat(Groups,Groups)
    integer :: n,m
    logical :: SpaceLikeNull(Groups)
    
    
    !!Compute the naive null-vectors
    
    call charge_lattice_to_null_lattice(Groups,Cmatrix,Cbar,Rlist,Rbar,&
         Cdualnull,Rdualnull)
    !!Extract the dual and null parts
    do n=1,Groups
       Cdual(n,:)=Cdualnull(n,:)
       Cnull(n,:)=Cdualnull(n+Groups,:)
       Rdual(n,:)=Rdualnull(n,:)
       Rnull(n,:)=Rdualnull(n+Groups,:)
       !!The joint charge lattice
       Ccharge(n,1:groups)=Cmatrix(n,:)
       Ccharge(n,(groups+1):(2*groups))=Cbar(n,:)
       Rcharge(1:groups)=Rlist(:)
       Rcharge((groups+1):(2*groups))=Rbar(:)
    end do
    
    
    !write(*,*) 
    !write(*,*) 'The joint charge lattice'
    !call print_int_rec_matrix(Groups,2*Groups,Ccharge)
    !write(*,*) "wich compactification radius"
    !write(*,*) Rcharge

    !write(*,*) 
    !write(*,*) 'Naive Dual lattice'
    !call print_int_rec_matrix(Groups,2*Groups,Cdual)
    !write(*,*) "wich compactification radius"
    !call print_int_rec_matrix(Groups,2*Groups,Rdual)
    
    !write(*,*) 
    !write(*,*) 'Naive Null lattice'
    !call print_int_rec_matrix(Groups,2*Groups,Cnull)
    !write(*,*) "wich compactification radius"
    !call print_int_rec_matrix(Groups,2*Groups,Rnull)



    !!Sanity check null dual vectors have no null vectors components!!
    call test_lorenz_orthogonal_charge(Groups,Cnull,Rnull,Ccharge,Rcharge,'  null','charge')
    tmpMat=kroenecker_matrix(Groups)
    !!Sanity check dual vectors have right properties!!
    call test_lorenz_matrix_charge(Groups,Cdual,Rdual,Ccharge,Rcharge,'  dual','charge',tmpMat)
    
    call orthonormalize_then_null_vectors(Groups,Cnull,Rnull,SpaceLikeNull)
    
    
    !!Summarize: Orthoginalized null vectors  are 
    write(*,*)
    write(*,*) ' True null lattice'
    call print_int_rec_matrix(Cnull)
    write(*,*) "wich compactification radius"
    call print_int_rec_matrix(Rnull)
    
    !!Sanity check - null vectros are orthogonal!!
    call test_lorenz_orthogonal_internal(Groups,Cnull,Rnull,'null')
    !!Sanity check - null vectors are still nullvectors!!
    call test_lorenz_orthogonal_charge(Groups,Cnull,Rnull,Ccharge,Rcharge,'  null','charge')
    
    
    call remove_null_from_dual_vectors(Groups,Cnull,Rnull,SpaceLikeNull,Cdual,Rdual)    
    
    !!Summarize: Dual vectors with null-vectors removed 
    write(*,*)
    write(*,*) 'True Dual lattice'
    call print_int_rec_matrix(Cdual)
    write(*,*) "wich compactification radius"
    call print_int_rec_matrix(Rdual)


    !!Sanity check null dual vectors have no null vectors components!!
    call test_lorenz_orthogonal_external(Groups,Cnull,Rnull,Cdual,Rdual,'  null','  dual')
    
    
    tmpMat=kroenecker_matrix(Groups)
    !!Sanity check dual vectors have right properties!!
    call test_lorenz_matrix_charge(Groups,Cdual,Rdual,Ccharge,Rcharge,'  dual','charge',tmpMat)
    
    
    
    !!Compute the euclidian scalar product!!
    do n=1,Groups
       do m=1,Groups
          call compute_euclidian_overlap(2*groups,Ccharge(n,:),Rcharge(:),&
               Cdual(m,:),Rdual(m,:),tmpCmat(n,m),tmpRmat(n,m))
       end do
    end do
    write(*,*)
    write(*,*) 'The euclidian scalar product beween charge and dual is'
    call print_int_matrix(tmpCmat)
    write(*,*) 'divided by sqrt'
    call print_int_matrix(tmpRmat)

  end subroutine compute_dual_lattice


  subroutine merge_matrices_to_one(Size,Merge,MA,MB)
    !!Merge two matrices of Size x Size to a matrix of Size x 2*Size
    integer, intent(IN) :: Size,MA(Size,Size),MB(Size,Size)
    integer, intent(OUT) :: Merge(Size,2*Size)
    integer::  i,j
    do i=1,Size
       do j=1,Size
          Merge(i,j)=MA(i,j)
          Merge(i,j+Size)=MB(i,j)
       end do
    end do
  end subroutine merge_matrices_to_one


  subroutine check_kmatrix_consistency()
    integer :: local_kappa_matrix(groups,groups),local_kappa_bar(groups,groups)
    integer :: local_Kplus(groups,groups),local_Kminus(groups,groups)
    
    write(*,*) ' ---- Testing the internal kappa-matrix consistency ---- '

    if(set_kappa)then !!The chiral piece is set...
       if(set_kappa_bar)then !! .. and the non-chiral piece is set
          !!Computing the combinded version
          local_Kplus=kappa_matrix+kappa_bar
          local_Kminus=kappa_matrix-kappa_bar
          call test_set_matrix(set_Kplus,Kplus,local_Kplus,'Kplus-matrix','kappa-matrix','kappa_bar-matrix',groups,print_help)
          call test_set_matrix(set_Kminus,Kminus,local_Kminus,'Kminus-matrix','kappa-matrix','kappa_bar-matrix',groups,print_help)
       elseif(set_Kplus)then !! .. and the additive piece is set
          local_kappa_bar=Kplus-kappa_matrix
          local_Kminus=2*kappa_matrix-Kplus
          call test_set_matrix(set_kappa_bar,kappa_bar,local_kappa_bar,'kappa_bar-matrix',&
               'kappa-matrix','Kplus-matrix',groups,print_help)
          call test_set_matrix(set_Kminus,Kminus,local_Kminus,'Kminus-matrix','kappa-matrix',&
               'Kplus-matrix',groups,print_help)
       elseif(set_Kminus)then !! .. and the subtractive piece is set
          local_kappa_bar=kappa_matrix-Kminus
          local_Kplus=2*kappa_matrix-Kminus
          call test_set_matrix(set_kappa_bar,kappa_bar,local_kappa_bar,'kappa_bar-matrix',&
               'kappa-matrix','Kminus-matrix',groups,print_help)
          call test_set_matrix(set_Kplus,Kplus,local_Kplus,'Kplus-matrix','kappa-matrix',&
               'Kminus-matrix',groups,print_help)
       else !!None of the other matrices set
          !!By default then kappa_bar=0
          call set_kappa_bar_zero(kappa_matrix)
       end if
    elseif(set_kappa_bar)then !!The nont-chiral piece is set...
       if(set_Kplus)then !! .. and the additive piece is set
          local_kappa_matrix=Kplus-kappa_bar
          local_Kminus=Kplus-2*kappa_bar
          call test_set_matrix(set_kappa,kappa_matrix,local_kappa_matrix,'kappa-matrix',&
               'kappa_bar-matrix','Kplus-matrix',groups,print_help)
          call test_set_matrix(set_Kminus,Kminus,local_Kminus,'Kminus-matrix','kappa_bar.matrix',&
               'Kplus-matrix',groups,print_help)
       elseif(set_Kminus)then !! .. and the subtractive piece is set
          local_kappa_matrix=Kminus+kappa_bar
          local_Kplus=Kminus+2*kappa_bar
          call test_set_matrix(set_kappa,kappa_matrix,local_kappa_matrix,'kappa-matrix',&
               'kappa_bar-matrix','Kminus-matrix',groups,print_help)
          call test_set_matrix(set_Kplus,Kplus,local_Kplus,'Kplus-matrix','kappa_bar.matrix',&
               'Kminus-matrix',groups,print_help)
       else !!Only K_bar set
          write(stderr,*) 'ERROR: Input'
          write(stderr,*) 'If --kappa_bar is given, one of --kappa_matrix, --Kplus, --Kmins'
          write(stderr,*) 'has to be supplied to determine the chiral sector'
          call exit(-2)
       end if
    elseif(set_Kplus)then !!The additice piece is set...
       if(set_Kminus)then !! .. and the subtractive piece is set
          !!K+ +/- K- has to be even
          local_kappa_matrix=Kplus+Kminus
          local_kappa_bar=Kplus-Kminus
          call test_set_int_matrix_x2(set_kappa,kappa_matrix,local_kappa_matrix,&
               'kappa_matrix','Kplus-matrix','Kminus-matrix',groups,print_help)
          call test_set_int_matrix_x2(set_kappa_bar,kappa_bar,local_kappa_bar,'kappa_bar',&
               'Kplus-matrix','Kminus-matrix',groups,print_help)
       else !!Only K_plus set
          !!By default then kappa_bar=0
          call set_kappa_bar_zero(Kplus)
       end if
    elseif(set_Kminus)then !!Only K_minus set
       !!By default then kappa_bar=0
       call set_kappa_bar_zero(Kminus)
    else !! Nor kappa-matrix set
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'No kappa-matrix has been supplied, so nothing can be done'
       write(*,*)
       call print_help()
       call exit(-2)
    end if

    !!Now that all the different matrices have been set. We also check that
    !!K and kappa_bar have only positive entries.
    !call check_positive_matrix(kappa_matrix,groups,'kappa-matrix',print_help)
    !call check_positive_matrix(kappa_bar,groups,'kappa_bar-matrix',print_help)
    !!If kbar is zero, then the matrix is chiral
    chiral_kappa=is_matrix_zero(kappa_bar,groups)
    if(chiral_kappa)then
       write(*,*) 'The kappa-matrix is chiral'
    else
       write(*,*) 'The kappa-matrix is NOT chiral'
    end if
  end subroutine check_kmatrix_consistency


  subroutine set_kappa_bar_zero(input_kappa)
    integer, intent(IN) :: input_kappa(groups,groups)
    kappa_bar   =0
    kappa_matrix=input_kappa
    Kplus  =input_kappa
    Kminus =input_kappa
    set_kappa_bar   =.TRUE.
    set_kappa=.TRUE.
    set_Kplus  =.TRUE.
    set_Kminus =.TRUE.
    write(stderr,*) 'WARNING: No --kappa_bar information has been given'
    write(stderr,*) '         Defaulting to kappa_bar=0'
  end subroutine set_kappa_bar_zero
  

  subroutine determine_quantum_numbers()
    integer :: epsilon,fluxes,Ne,k1,k2,delta,r,rows,determinant,row,cells
    integer :: nlist(groups),n0list(groups)

    write(*,*) '-----------------------------'
    write(*,*) '      Quantum numbers        '
    write(*,*) '-----------------------------'

    if(set_cells_group)then
       determinant=determinant_int_mat(Kminus)
       fluxes=q_group*cells_group
       Ne=p_group*cells_group
       
       epsilon=mod(fluxes+kminus(1,1),2)
       write(*,*) 'Parity flux-K_11:',epsilon
       delta=q_group/q
       cells=cells_group*delta
       write(*,*) '  electrons,     Fluxes, CoMomentum range'
       write(*,*) Ne,fluxes,cells

       rows=determinant/q_group
       write(*,*) 'delta=',delta
       write(*,*) 'rows=',rows
       if(delta.eq.1)then !!Abelian (one-dimentional)
          if(rows.eq.1)then !!Abelian (one-dimentional)
             write(*,*) 'Abelian state:'
          else 
             write(*,*) 'Abelian (multirow) state:'
          end if
       else
          write(*,*) 'Non-Abelian state:'
       end if
       write(*,*) '         k1          k2 :     CoMindx :            Dual-index (x2)   '
       write(*,*) '       ----------------------------------'
       call get_first_nlist_entry(kappa_matrix,cells_group,nlist,groups)
       do row=1,determinant !!!Loop through all states
          do r=0,(delta-1)
             k1=get_k1(pminus,cells_group,nlist,groups,q_group)
             k2=get_k2(pminus,cells_group,nlist,groups,q_group,r)
             write(*,*) k1,k2,':',r,':',nlist
          end do
          call get_next_nlist_entry(kappa_matrix,pminus,q_group,cells_group,nlist,groups)
       end do
    else
       write(*,*) 'No cell information given, so nothing can be done regarding quantum numbers.'
    end if
    

  end subroutine determine_quantum_numbers
  
end program K_matrix_info
