module chiral_wfn
  
  use input_misc
  use k_matrix
  use chiral_wave_function !!to initalize the wave-functions
  use center_of_mass   !!to initalize the boundary conditions
  use channels
  use typedef
  use DiscretizedWaveFunctions
  use cft_help
  use misc_matrix
  implicit none
  
  !!Global variables
  integer ::Ne,Ns,groups,cells,p,q,Ksector,K2sector,MC,xtrans,ytrans,qps
  integer ::k_denom,qp_pow_nomin,qp_pow_denom,COMI
  real(KIND=dp) ::tau_im,tau_re,sval
  real(KIND=dp),allocatable ::qp_posx(:),qp_posy(:)
  integer,allocatable :: kappa_matrix(:,:),kappa_bar(:,:),Kplus(:,:),Kmatrix(:,:)
  integer,allocatable :: xlist(:),ylist(:),dual_index(:),qp_list(:)
  !!Flags to check if the global variables are set
  logical::set_Ne,set_Ns,set_groups,set_cells,set_p,set_q,set_k_denom,set_no_wfn
  logical::set_k,set_MC,set_I,set_R,set_x,set_y,set_no_asym,set_sval,set_K2
  logical::set_qp_pow,set_qp_posx,set_qp_posy,set_qp_filex,set_qp_filey
  logical::set_kappa_matrix,set_kappa_bar_matrix,set_kplus,set_kmatrix,set_qps,set_COMI
  logical::set_xlist,set_ylist,set_reuse,set_dual_index,set_log,set_qp_static,set_cft_scale
  
  !!Derived quantities
  complex(kind=dpc) :: tau
  integer:: p_group,q_group
  integer,allocatable :: p_list(:),group_sizes(:)
  logical :: chiral_kappa
  
  !!The default catalouge to write to
  character(len=100) :: output_dir="result"
  character(len=100) :: qp_filex,qp_filey
  

  !!NB: Important
  !!From the calling C-code, this module should be called in the order 
  !call set_input()
  !call retreive_input()
  !call check_kmatrix_consistency()
  !call compute_kmatrix_stat()
  !call init_wfn()
  !!

  private
  public set_input,retreive_input,check_kmatrix_consistency
  public compute_kmatrix_stat,init_wfn,print_status,compute_wave_function
  

  
contains
  
  subroutine set_input()  bind(C)
    USE ISO_C_BINDING
    !!Set all the flags to negative
    set_COMI=.FALSE.
    set_Ne=.FALSE.
    set_Ns=.FALSE.
    set_groups=.FALSE.
    set_cells=.FALSE.
    set_p=.FALSE.
    set_q=.FALSE.
    set_K=.FALSE.
    set_MC=.FALSE.
    set_I=.FALSE.
    set_R=.FALSE.
    set_x=.FALSE.
    set_y=.FALSE.
    set_sval=.FALSE.
    set_xlist=.FALSE.
    set_ylist=.FALSE.
    set_kappa_matrix=.FALSE.
    set_kappa_bar_matrix=.FALSE.
    set_kplus=.FALSE.
    set_kmatrix=.FALSE.
    set_reuse=.FALSE.
    set_cft_scale=.FALSE.
    set_dual_index=.FALSE.
    set_no_asym=.FALSE.
    set_log=.FALSE.
    set_qps=.FALSE.
    set_qp_pow=.FALSE.
    set_qp_posx=.FALSE.
    set_qp_posy=.FALSE.
    set_qp_filex=.FALSE.
    set_qp_filey=.FALSE.
    set_k_denom=.FALSE.
    set_qp_static=.FALSE.
    set_no_wfn=.FALSE.
  end subroutine set_input
  
  
  subroutine print_status()
    write(*,*)
    write(*,*) '(Some of) the global (input) variables'
    call print_variable_status(set_Ne,Ne,'Ne')
    call print_variable_status(set_Ns,Ns,'Ns')
    call print_variable_status(set_qps,qps,'qps:s')
    call print_variable_status(set_groups,groups,'groups')
    call print_variable_status(set_cells,cells,'cells')
    call print_variable_status(set_p,p,'p')
    call print_variable_status(set_q,q,'q')
    call print_variable_status(set_K,Ksector,'K')
    call print_variable_status(set_MC,MC,'MC')
    call print_matrix_status(set_kappa_matrix,kappa_matrix,'kappa-matrix',groups)
    call print_matrix_status(set_kappa_bar_matrix,kappa_bar,'kappa_bar-matrix',groups)
    call print_matrix_status(set_kplus,kplus,'Kplus-matrix',groups)
    call print_matrix_status(set_kmatrix,kmatrix,'Kmatrix',groups)
    call print_float_status(set_I,tau_im,'I')
    call print_float_status(set_I,tau_re,'R')
    call print_variable_status(set_x,xtrans,'x')
    call print_variable_status(set_y,ytrans,'y')
    call print_list_status(set_xlist,xlist,'x-list',groups)
    call print_list_status(set_ylist,ylist,'y-list',groups)
    call print_list_status(set_dual_index,dual_index,'dual-index-list',groups)
  end subroutine print_status
  
  
!!!--------------------------------------------------
!!!        Reterive the input to the program
!!!--------------------------------------------------
  
  
  SUBROUTINE flush_args(Narg,arguments)
    INTEGER, INTENT(IN) ::Narg
    CHARACTER(len=100), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: arguments
    INTEGER :: i
    
    ALLOCATE (arguments(narg))
    DO i=1,Narg
       call get_command_argument(i,arguments(i))
       !!write(*,*) 'Arg',i,':',arguments(i)
    END DO
    
  END SUBROUTINE flush_args
  
  
  subroutine retreive_input()
    integer::narg
    CHARACTER(len=100), DIMENSION(:), ALLOCATABLE :: arguments
    
    !!Initally this system will barf (in an uncontrolled way) if the input is wrong
    
    !Check if arguments are found
    narg=command_argument_count()
    
    call flush_args(Narg,arguments)
    
    call init_variables(Narg,arguments)
    
    
  end subroutine retreive_input
  
  
  
!!!--------------------------------------------------
!!!        Reterive the input to the program
!!!--------------------------------------------------
  
  
  SUBROUTINE splitargs(str, ks, n, n_M, arguments)
    INTEGER, INTENT(IN) :: n, n_M
    INTEGER, INTENT(IN) :: ks(1:n)
    CHARACTER(len=n_M), INTENT(IN) :: str
    CHARACTER(len=100), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: arguments
    INTEGER :: sbegin=1, send, i
    
    ALLOCATE (arguments(n))
    DO i=1,n
       send=sbegin+ks(i)-1
       arguments(i)=str(sbegin:send)
       sbegin=send+1
    END DO
    
  END SUBROUTINE splitargs
  
  
  subroutine retreive_input_alt(inarguments, inlength, no_arguments, ks) bind(C)
    USE ISO_C_BINDING
    INTEGER(kind=C_INT), INTENT(IN) :: no_arguments, inlength
    !CHARACTER(len=inlength), INTENT(IN) :: inarguments
    CHARACTER(kind=C_CHAR), DIMENSION(inlength), INTENT(IN) :: inarguments
    INTEGER(kind=C_INT), INTENT(IN) :: ks(1:no_arguments)
    CHARACTER(kind=C_CHAR,len=100), DIMENSION(:), ALLOCATABLE :: arguments
    integer :: narg
    
    ! WRITE(*,*) 'inarguments ',inarguments,' end of arguments'
    ! CALL splitargs(inarguments, ks, no_arguments, 100, arguments);
    CALL splitargs(inarguments(1), ks, no_arguments, 100, arguments);
    
    !!Initally this sytem will barf (in an uncontrolled way) if the input is wrong
    
    !Check if arguments are found
    ! narg=command_argument_count()
    narg=no_arguments
    
    call init_variables(Narg,arguments)
  end subroutine retreive_input_alt
  
  
  
  subroutine init_variables(Narg,arguments)
    integer, intent(IN)::Narg
    CHARACTER(len=100), DIMENSION(:),intent(IN) :: arguments
    integer::skip_next,cptArg,n
    character(len=100)::name
    
    
    write(*,*) "Parsing CFT arguments"

    skip_next=0
    if(narg>0)then
       !loop across options
       do cptArg=1,narg
          !write(*,*) 'Argno:',cptArg
          if(skip_next.gt.0)then
             !!Skip the next skip_next arguments
             !!write(*,*) 'skip an argument'
             skip_next=skip_next-1
             cycle
          end if
          !!Get the comand argument
          name=arguments(cptArg)
          !!write(*,*) 'Argument:',adjustl(name),":"
          !!Select from all the cases
          select case(adjustl(name))
             
          case("--help","-h")
             call print_help()
             call exit(0)
             !!NB: Theses two catches depends on which flags are used in the C-part of the program
             !!First we test against the C-implementation flags (these take 1 argument)
          CASE("--seed","-th","-dl","-st","-qL","-KL","-pos","-wfn","-wgt","-d_alt","-chL")
             skip_next=1
             !!First we test against the C-implementation flags (these take 0 arguments)
          CASE("-csv","--use-cft-as-wgt")
             skip_next=0
          CASE("--no-wfn") !!!Special c-implementation as it deisbles the need to -scpecify D!!!
             call set_unset_flag(set_no_wfn,'--no-wfn',print_help)
             skip_next=0
          case("-r","--reuse")
             call set_unset_flag(set_reuse,'-r',print_help)
          case("--use-cft-scale")
             call set_unset_flag(set_cft_scale,'--use-cft-scale',print_help)
          case("-A")
             call set_unset_flag(set_no_asym,'-A',print_help)
          case("-l","--log")
             call set_unset_flag(set_log,'-l',print_help)
          case("--qp-static")
             call set_unset_flag(set_qp_static,'--qp-static',print_help)
          case("-Nq","--qps")
             call set_unset_flag(set_qps,'-Nq',print_help)
             call recieve_integer_list_input_alt(qp_list,groups,'Nq',arguments,cptArg+1)
             do n=1,groups
                call check_non_negative_integer(qp_list(n),'-Nq',print_help)
             end do
             qps=sum(qp_list)
             allocate(qp_posx(qps),qp_posy(qps))
             skip_next=groups
          case("--qp-power","--qp-pow")
             call set_unset_flag(set_qp_pow,'--qp-power',print_help)
             call recieve_integer_input_alt(qp_pow_nomin,'qp_nomin',arguments,cptArg+1)
             call recieve_integer_input_alt(qp_pow_denom,'qp_denom',arguments,cptArg+2)
             call check_positive_integer(qp_pow_denom,'--qp_power',print_help)
             skip_next=2             
          case("--qp-pos-x","--qp-posx")
             if(set_qps)then
                if((.not.set_qp_filex))then
                   call set_unset_flag(set_qp_posx,'--qp-pos-x',print_help)
                   call recieve_float_list_input_alt(qp_posx,qps,'qp-pos-x',arguments,cptArg+1)
                   skip_next=qps             
                else
                   call  error_flag_clash('--qp-pos-x','--qp-file-x',print_help)
                end if
             else
                call  error_flag_dependency('--qp-pos-x','-Nq',print_help)
             end if
          case("--qp-pos-y","--qp-posy")
             if(set_qps)then
                if((.not.set_qp_filey))then
                   call set_unset_flag(set_qp_posy,'--qp-pos-y',print_help)
                   call recieve_float_list_input_alt(qp_posy,qps,'qp-pos-y',arguments,cptArg+1)
                   skip_next=qps             
                else
                   call  error_flag_clash('--qp-pos-y','--qp-file-y',print_help)
                end if
             else
                call  error_flag_dependency('--qp-pos-y','-Nq',print_help)
             end if
          case("--qp-file-x","--qp-filex")
             if(set_qps)then
                if((.not.set_qp_posx))then
                   call set_unset_flag(set_qp_filex,'--qp-file-x',print_help)
                   call recieve_string_input_alt(qp_filex,'qp-file-x',arguments,cptArg+1)
                   skip_next=1
                else
                   call  error_flag_clash('--qp-file-x','--qp-pos-x',print_help)
                end if
             else
                call  error_flag_dependency('--qp-pos-x','-Nq',print_help)
             end if
          case("--qp-file-y","--qp-filey")
             if(set_qps)then
                if((.not.set_qp_posy))then
                   call set_unset_flag(set_qp_filey,'--qp-file-y',print_help)
                   call recieve_string_input_alt(qp_filey,'qp-file-y',arguments,cptArg+1)
                   skip_next=1
                else
                   call  error_flag_clash('--qp-file-y','--qp-pos-y',print_help)
                end if
             else
                call  error_flag_dependency('--qp-pos-y','-Nq',print_help)
             end if
          case("-s","--svalue")
             call set_unset_flag(set_sval,'-s',print_help)
             call recieve_float_input_alt(sval,'sval',arguments,cptArg+1)
             skip_next=1
          case("-p","--nomin")
             call set_unset_flag(set_p,'-p',print_help)
             call recieve_integer_input_alt(p,'p',arguments,cptArg+1)
             call check_positive_integer(p,'-p',print_help)
             skip_next=1
          case("-q","--denomin")
             call set_unset_flag(set_q,'-q',print_help)
             call recieve_integer_input_alt(q,'q',arguments,cptArg+1)
             call check_positive_integer(q,'-q',print_help)
             skip_next=1
          case("-Ns","--fluxes")
             call set_unset_flag(set_Ns,'-Ns',print_help)
             call recieve_integer_input_alt(Ns,'Ns',arguments,cptArg+1)
             call check_positive_integer(Ns,'-Ns',print_help)
             skip_next=1
          case("-Ne",'--particles')
             call set_unset_flag(set_Ne,'-Ne',print_help)
             call recieve_integer_input_alt(Ne,'Ne',arguments,cptArg+1)
             call check_positive_integer(Ne,'-Ne',print_help)
             skip_next=1
          case("-c","--cells")
             call set_unset_flag(set_cells,'-c',print_help)
             call recieve_integer_input_alt(cells,'Cells',arguments,cptArg+1)
             call check_positive_integer(cells,'-c',print_help)
             skip_next=1
          case("--COMI",'--CoM-index')
             call set_unset_flag(set_COMI,'--COMI',print_help)
             call recieve_integer_input_alt(COMI,'COMI',arguments,cptArg+1)
             skip_next=1
          case("-D","--groups")
             call set_unset_flag(set_groups,'-D',print_help)
             call recieve_integer_input_alt(groups,'groups',arguments,cptArg+1)
             call check_positive_integer(groups,'-D',print_help)
             call allocate_variables()
             skip_next=1
          case("-MC","--MC-points","-N")
             call set_unset_flag(set_MC,'-MC',print_help)
             call recieve_integer_input_alt(MC,'MC-points',arguments,cptArg+1)
             call check_non_negative_integer(MC,'-MC',print_help)
             skip_next=1
          case("-K","-K1","--momentum")
             call set_unset_flag(set_K,'-K',print_help)
             call recieve_integer_input_alt(Ksector,'K',arguments,cptArg+1)
             skip_next=1
          case("-K2","--co-momentum")
             call set_unset_flag(set_K2,'-K2',print_help)
             call recieve_integer_input_alt(K2sector,'K2',arguments,cptArg+1)
             skip_next=1
          case("-x")
             call set_unset_flag(set_x,'-x',print_help)
             call recieve_integer_input_alt(xtrans,'x',arguments,cptArg+1)
             skip_next=1
          case("-y")
             call set_unset_flag(set_y,'-y',print_help)
             call recieve_integer_input_alt(ytrans,'y',arguments,cptArg+1)
             skip_next=1
          case("--x-list","--xlist")
             if(set_groups)then 
                call set_unset_flag(set_xlist,'--x-list',print_help)
                call recieve_integer_list_input_alt(xlist,groups,'xlist',arguments,cptArg+1)
                skip_next=groups
             else
                call print_help()
                write(stderr,*) 'ERROR: Input'
                write(stderr,*) 'Flag -D has to be set before --x-list'
                write(*,*)
                call exit(-2)
             end if
          case("--y-list","--ylist")
             if(set_groups)then 
                call set_unset_flag(set_ylist,'--y-list',print_help)
                call recieve_integer_list_input_alt(ylist,groups,'ylist',arguments,cptArg+1)
                skip_next=groups
             else
                call print_help()
                write(stderr,*) 'ERROR: Input'
                write(stderr,*) 'Flag -D has to be set before --y-list'
                write(*,*)
                call exit(-2)
             end if
          case("--kappa_matrix","--kappa")
             if(set_groups)then 
                call import_set_matrix_alt(set_kappa_matrix,'--kappa_matrix'&
                     ,kappa_matrix,cptArg,arguments,'kappa-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--kappa_matrix','-D',print_help)
             end if
          case("--kappa_matrix-diag","--kappa-diag")
             if(set_groups)then 
                call import_set_diag_matrix_alt(set_kappa_matrix,'--kappa_matrix-diag'&
                     ,kappa_matrix,cptArg,arguments,'kappa_matrix',groups,print_help)
                skip_next=groups
             else
                call  error_flag_dependency('--kappa_matrix','-D',print_help)
             end if
          case("--kappa_bar","--kbar")
             if(set_groups)then 
                call import_set_matrix_alt(set_kappa_bar_matrix,'--kappa_bar'&
                     ,kappa_bar,cptArg,arguments,'K-bar-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--kappa_bar','-D',print_help)
             end if
          case("--kappa_bar-diag","--kbar-diag")
             if(set_groups)then 
                call import_set_diag_matrix_alt(set_kappa_bar_matrix,'--kappa_bar-diag'&
                     ,kappa_bar,cptArg,arguments,'K-bar-matrix',groups,print_help)
                skip_next=groups
             else
                call  error_flag_dependency('--kappa_bar','-D',print_help)
             end if
          case("--Kplus","--K-plus","--kplus","--k-plus")
             if(set_groups)then 
                call import_set_matrix_alt(set_kplus,'--Kplus'&
                     ,Kplus,cptArg,arguments,'K-plus-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--Kplus','-D',print_help)
             end if
          case("--Kmatrix","--kmatrix")
             if(set_groups)then 
                call import_set_matrix_alt(set_kmatrix,'--Kmatrix'&
                     ,Kmatrix,cptArg,arguments,'K-minus-matrix',groups,print_help)
                skip_next=groups**2
             else
                call  error_flag_dependency('--Kmatrix','-D',print_help)
             end if
          case("-I","--tau_im")
             call set_unset_flag(set_I,'-I',print_help)
             call recieve_float_input_alt(tau_im,'tau_im',arguments,cptArg+1)
             skip_next=1
          case("-R","--tau_re")
             call set_unset_flag(set_R,'-R',print_help)
             call recieve_float_input_alt(tau_re,'tau_re',arguments,cptArg+1)
             skip_next=1
          case("--dual-index","--DI")
             if(set_groups)then 
                call set_unset_flag(set_dual_index,'--dual-index',print_help)
                call recieve_integer_list_input_alt(dual_index,groups,'dual-index',arguments,cptArg+1)
                skip_next=groups
             else
                call print_help()
                write(stderr,*) 'ERROR: Input'
                write(stderr,*) 'Flag -D has to be set before --dual-index'
                write(*,*)
                call exit(-2)
             end if
          case('--k_denomin','--k_denom')
             call set_unset_flag(set_k_denom,'--k_denominator',print_help)
             call recieve_integer_input_alt(k_denom,'K_kdenom',arguments,cptArg+1)
             skip_next=1
          case('--output_dir')
             call recieve_string_input(output_dir,'--output_dir',cptArg+1)
             skip_next=1
          case default
             !!FIXME: Maybe print the whole input data here
             call print_help()
             write(stderr,*)"ERROR: Input"
             write(stderr,*)'Option: "',trim(name),'" unknown at position',cptArg
             write(*,*)
             do n=max(cptArg-4,1),min(cptArg+4,narg)
                if(n.eq.cptArg)then
                   write(stderr,*) '                  ----|  |-----'
                   write(stderr,*) '                  ----v  v-----'
                end if
                   write(stderr,*)'arg no:',n,': "',trim(arguments(n)),'"'
                if(n.eq.cptArg)then
                   write(stderr,*) '                  ----^  ^-----'
                   write(stderr,*) '                  ----|  |-----'
                end if

             end do
             
             call exit(-2)
          end select
          !!write(*,*) 'Done with argno:',cptArg
       end do
       write(*,*) 'Done!'
    endif
    
    write(*,*) 'Post Checks:'
    !!Check that Dflag was set (unless the --no-wfn was set)!!
    if(.not.set_groups)then
       if(set_no_wfn)then 
          !!Set D=1 artificially        
          groups=1
          call set_unset_flag(set_groups,'-D',print_help)
          call allocate_variables()
          !!Set kappa_matrix=1 artificially
          call set_unset_flag(set_kappa_matrix,'--kappa_matrix',print_help)
          kappa_matrix(1,1)=1
       else 
          call error_flag_missing('-D',print_help)
       end if
    end if
    
    !!Do some post settings!
    !!Test if the quasiparticles are set
    !!If not, set them to zero.
    if(.not.set_qps)then
       qps=0
       qp_list=0
       set_qps=.TRUE.
    end if
   
    write(*,*) "Parsing CFT arguments complete"
    
  end subroutine init_variables

  subroutine allocate_variables()
    allocate(kappa_matrix(groups,groups))
    allocate(kappa_bar(groups,groups))
    allocate(Kplus(groups,groups))
    allocate(Kmatrix(groups,groups))
    allocate(xlist(groups))
    allocate(ylist(groups))
    allocate(dual_index(groups))
    allocate(qp_list(groups))
  end subroutine allocate_variables

    
!!!--------------------------------------------------
!!!        Computing relevant stuff from the kappa-matrix
!!!--------------------------------------------------
  
  subroutine check_kmatrix_consistency() bind(C)
    USE ISO_C_BINDING
    integer(kind=C_INT) :: local_kappa_matrix(groups,groups),local_kappa_bar(groups,groups)
    integer(kind=C_INT) :: local_Kplus(groups,groups),local_Kmatrix(groups,groups)
    
    write(*,*) ' ---- Testing the internal kappa-matrix consistency ---- '
    
    if(set_kappa_matrix)then !!The chiral piece is set...
       if(set_kappa_bar_matrix)then !! .. and the non-chiral piece is set
          !!Computing the combinded version
          local_Kplus=kappa_matrix+kappa_bar
          local_Kmatrix=kappa_matrix-kappa_bar
          call test_set_matrix(set_Kplus,Kplus,local_Kplus,'Kplus-matrix','kappa-matrix','kappa_bar-matrix',groups,print_help)
          call test_set_matrix(set_Kmatrix,Kmatrix,local_Kmatrix,'Kmatrix','kappa-matrix',&
               'kappa_bar-matrix',groups,print_help)
       elseif(set_Kplus)then !! .. and the additive piece is set
          local_kappa_bar=Kplus-kappa_matrix
          local_Kmatrix=2*kappa_matrix-Kplus
          call test_set_matrix(set_kappa_bar_matrix,kappa_bar,local_kappa_bar,'kappa_bar-matrix',&
               'kappa-matrix','Kplus-matrix',groups,print_help)
          call test_set_matrix(set_Kmatrix,Kmatrix,local_Kmatrix,'Kmatrix','kappa-matrix',&
               'Kplus-matrix',groups,print_help)
       elseif(set_Kmatrix)then !! .. and the subtractive piece is set
          local_kappa_bar=kappa_matrix-Kmatrix
          local_Kplus=2*kappa_matrix-Kmatrix
          call test_set_matrix(set_kappa_bar_matrix,kappa_bar,local_kappa_bar,'kappa_bar-matrix',&
               'kappa-matrix','Kmatrix',groups,print_help)
          call test_set_matrix(set_Kplus,Kplus,local_Kplus,'Kplus-matrix','kappa-matrix',&
               'Kmatrix',groups,print_help)
       else !!None of the other matrices set
          !!By default then kappa_bar=0
          call set_kappa_bar_zero(kappa_matrix)
       end if
    elseif(set_kappa_bar_matrix)then !!The nont-chiral piece is set...
       if(set_Kplus)then !! .. and the additive piece is set
          local_kappa_matrix=Kplus-kappa_bar
          local_Kmatrix=Kplus-2*kappa_bar
          call test_set_matrix(set_kappa_matrix,kappa_matrix,local_kappa_matrix,'kappa-matrix',&
               'kappa_bar-matrix','Kplus-matrix',groups,print_help)
          call test_set_matrix(set_Kmatrix,Kmatrix,local_Kmatrix,'Kmatrix','kappa_bar.matrix',&
               'Kplus-matrix',groups,print_help)
       elseif(set_Kmatrix)then !! .. and the subtractive piece is set
          local_kappa_matrix=Kmatrix+kappa_bar
          local_Kplus=Kmatrix+2*kappa_bar
          call test_set_matrix(set_kappa_matrix,kappa_matrix,local_kappa_matrix,'kappa-matrix',&
               'kappa_bar-matrix','Kmatrix',groups,print_help)
          call test_set_matrix(set_Kplus,Kplus,local_Kplus,'Kplus-matrix','kappa_bar.matrix',&
               'Kmatrix',groups,print_help)
       else !!Only K_bar set
          write(stderr,*) 'ERROR: Input'
          write(stderr,*) 'If --kappa_bar is given, one of --kappa_matrix, --Kplus, --Kmatrix'
          write(stderr,*) 'has to be supplied to determine the chiral sector'
          call exit(-2)
       end if
    elseif(set_Kplus)then !!The additice piece is set...
       if(set_Kmatrix)then !! .. and the subtractive piece is set
          !!K+ +/- K- has to be even
          local_kappa_matrix=Kplus+Kmatrix
          local_kappa_bar=Kplus-Kmatrix
          call test_set_int_matrix_x2(set_kappa_matrix,kappa_matrix,local_kappa_matrix,&
               'kappa_matrix','Kplus-matrix','Kmatrix',groups,print_help)
          call test_set_int_matrix_x2(set_kappa_bar_matrix,kappa_bar,local_kappa_bar,'kappa_bar',&
               'Kplus-matrix','Kmatrix',groups,print_help)
       else !!Only K_plus set
          !!By default then kappa_bar=0
          call set_kappa_bar_zero(Kplus)
       end if
    elseif(set_Kmatrix)then !!Only K_minus set
       !!By default then kappa_bar=0
       call set_kappa_bar_zero(Kmatrix)
    else !! Nor kappa-matrix set
       call print_help()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'No kappa-matrix has been supplied, so nothing can be done'
       write(*,*)       
       call exit(-2)
    end if

    !!Now that all the different matrices have been set. We also check that
    !!K and kappa_bar have only positive entries.
    
    !!FIXME: It's only important to ensure that the eigenvalues are all positive.
    !!For 2x2 matrices this means Kplus(d,d)>0 and det(Kplus)=Kplus(1,1)*Kplus(2,2)-Kplus(1,2)^2>0
    !!call check_positive_matrix(kappa_matrix,groups,'kappa-matrix',print_help)
    !!call check_positive_matrix(kappa_bar,groups,'kappa_bar-matrix',print_help)

    !!If kbar is zero, then the matrix is chiral
    chiral_kappa=is_matrix_zero(kappa_bar,groups)
    if(chiral_kappa)then
       write(*,*) 'The kappa-matrix is chiral'
    else
       write(*,*) 'The kappa-matrix is NOT chiral'
    end if
  end subroutine check_kmatrix_consistency
  
  subroutine set_kappa_bar_zero(input_kappa)
    integer, intent(IN) :: input_kappa(groups,groups)
    kappa_bar   = 0
    kappa_matrix=input_kappa
    Kplus  =input_kappa
    Kmatrix =input_kappa
    set_kappa_bar_matrix   =.TRUE.
    set_kappa_matrix=.TRUE.
    set_Kplus  =.TRUE.
    set_Kmatrix =.TRUE.
    write(stderr,*) 'WARNING: No --kappa_bar information has been given'
    write(stderr,*) '         Defaulting to kappa_bar=0'
  end subroutine set_kappa_bar_zero
  
  subroutine compute_kmatrix_stat() bind(C)
    integer :: local_p,local_q,local_Ns,local_Ne,local_k2,NeByp
    integer :: local_gcd,local_cells,qp_ns_shift, nlist(groups)
    !!Make sure the kappa_matrix has been supplied
    if(.not.set_Kmatrix)then
       call print_help()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'No kappa-matrix has been supplied, so nothing can be done'
       write(*,*)
       call exit(-2)
    end if
    
    if(.not.set_k_denom)then
       !!Set to default values
       k_denom=1
       set_k_denom=.TRUE.
    end if
    
    call check_k_minus_is_integer(Kmatrix,k_denom,groups)
    
    write(*,*) ' ---- Compute all things needed ----'
    allocate(p_list(groups))
    allocate(group_sizes(groups))
    write(*,*) 'Compute the differnt group sizes'
    call kappa_matrix_group_sizes(groups,Kmatrix/k_denom,p_list,p_group,q_group)
    call kappa_matrix_group_sizes(groups,Kmatrix/k_denom,p_list,p_group,q_group)

    write(*,*) 'The input kappa-matrix'
    call print_int_matrix(Kmatrix/k_denom)
    write(*,*) 'has a smallest cell of',q_group,'with',p_group,'particles'
    call Filling_fraction(groups,p_list,q_group,local_p,local_q)
    write(*,*) 'this corresponds to fillnig fraction nu=',local_p,'/',local_q
    !!Compare with input numbers
    call test_set_compability2(set_p,p,local_p,'p',print_help)
    call test_set_compability2(set_q,q,local_q,'q',print_help)
    
    !!compute the flux shift form the qps
    !!FIXME here we assume the same number of qps in each group
    qp_ns_shift=qp_list(1)
    
    !!Compute the system size that will be used in generating MC-points
    if(set_cells)then !!If number of cells are provided we start there
       local_Ne=p_group*cells
       local_Ns=q_group*cells
       call test_set_compability(set_Ns,Ns,local_Ns+qp_ns_shift,'Ns','cells',cells,print_help)
       call test_set_compability(set_Ne,Ne,local_Ne,'Ne','cells',cells,print_help)
    elseif(set_Ns)then
       !!Compute the grates common divisor of Ns and q_group
       local_gcd=gcd(q_group,Ns)
       if(local_gcd.eq.q_group)then !!If equal  Ns is compatible
          local_cells=Ns/q_group
          local_Ne=p_group*local_cells
          call test_set_compability(set_cells,cells,local_cells,'cells','Ns',Ns,print_help)
          call test_set_compability(set_Ne,Ne,local_Ne,'Ne','Ns',Ns,print_help)
       else
          !!If gcd.ne.q_group then NS is not compatible with the kappa-matrix
          call test_set_compability(set_Ns,Ns,Ns+1,'Ns','group-q',q_group,print_help)
       end if
    elseif(set_Ne)then
       !!Compute the grates common divisor of Ne and p_group
       local_gcd=gcd(p_group,Ne)
       if(local_gcd.eq.p_group)then !!If equal  Ne is compatible
          local_cells=Ne/p_group
          local_Ns=q_group*local_cells
          call test_set_compability(set_cells,cells,local_cells,'cells','Ne',Ne,print_help)
          call test_set_compability(set_Ns,Ns,local_Ns+qp_ns_shift,'Ns','Ne',Ne,print_help)
       else
          !!If gcd.ne.q_group then NS is not compatible with the kappa-matrix
          call test_set_compability(set_Ns,Ns,Ns+1,'Ns','group-q',q_group,print_help)
       end if
    else
       call print_help()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'One of the flags -c, -Ns, -Ne must be supplied to give the system size.'
       write(*,*)
       call exit(-2)
    end if


    
    !!Compute the groups size one the number of cells is known
    group_sizes=cells*p_list
    
    
    !!Set the quasi-particle powers
    if(.not.set_qp_pow)then
       set_qp_pow=.TRUE.
       if(qps.eq.0)then!!If no quasi particles this is not needed
          qp_pow_denom=0
          qp_pow_nomin=1
       else !!This is the fundamental quasi-hole for laughlin (D=kappa_bar/q)
          call multiply_fractions(kappa_bar(1,1),k_denom,1,q_group,qp_pow_denom,qp_pow_nomin)
       end if
    end if

    !!!Logic regarding the co-momentum
    if(.not.set_COMI)then
       set_COMI=.TRUE.
       COMI=0
       write(stderr,*) 'WARNING: No --COMI information has been given'
       write(stderr,*) '         Defaulting to COMI=0'
    end if


    !!Test the K2 specification
    if(set_k2)then
       call get_first_nlist_entry(Kmatrix/k_denom,cells,nlist,groups)
       local_k2=get_k2(p_list,cells,nlist,groups,q_group,COMI)
       NeByp=Ne/local_p
       if(mod(k2sector-local_k2,NeByp).ne.0)then
          write(stderr,*) 'ERROR: The K2 sector is not what you expect it to be'
          write(*,*) 'Set COMI:',COMI
          write(*,*) 'Derived K2 sector:',mod(local_k2,NeByp)
          write(*,*) 'Input K2:',mod(K2sector,NeByp)
          write(*,*) 'Diff K2:',mod(k2sector-local_k2,NeByp)
          write(stderr,*) '  ...quitting'
          call exit(-1)
       end if
    end if


    
  end subroutine compute_kmatrix_stat
  
  subroutine get_wfn_ne(ne_out) bind(c)
    USE ISO_C_BINDING
    integer(kind=C_INT), intent(out) :: ne_out
    if(set_Ne)then
       ne_out=Ne
    else
       write(*,*) 'ERROR: Ne not set yet'
       call exit(-1)
    end if
  end subroutine get_wfn_ne
  
  subroutine get_wfn_ns(Ns_out) bind(c)
    USE ISO_C_BINDING
    integer(kind=C_INT), intent(out) :: ns_out
    if(set_Ns)then
       ns_out=Ns
    else
       write(*,*) 'ERROR: Ns not set yet'
       call exit(-1)
    end if
  end subroutine get_wfn_ns
  
  subroutine get_wfn_k(k_out) bind(c)
    USE ISO_C_BINDING
    integer(kind=C_INT), intent(out) :: k_out
    if(set_k)then
       k_out=ksector
    else
       write(*,*) 'ERROR: k-sector not set yet'
       call exit(-1)
    end if
  end subroutine get_wfn_k
  
  
  subroutine get_wfn_tau(tau_out) bind(c)
    USE ISO_C_BINDING
    complex(kind=C_DOUBLE_COMPLEX), intent(out) :: tau_out
    if(set_I.and.set_R)then
       tau_out=tau_re+iunit*tau_im
    else
       write(*,*) 'ERROR: I or R not set yet'
       call exit(-1)
    end if
  end subroutine get_wfn_tau
  
  subroutine init_wfn() bind(C)
    USE ISO_C_BINDING
    integer :: K_sector_shift,n
    
    !!Compute the x-y lists
    if(set_x)then
       call test_set_translation_list(set_xlist,xlist,xtrans,'x-list','x',groups,print_help)
    end if
    if(set_y)then
       call test_set_translation_list(set_ylist,ylist,ytrans,'y-list','y',groups,print_help)
    end if
    if((.not.set_xlist).and.(.not.set_ylist))then
       !!If non of them is set, set to default
       if(groups.ne.1)then !!Waring only needed with gore groups than one
          write(stderr,*) 'WARNING: No -x/y or --x/y-list have been specified.'
          write(stderr,*) '         Defaulting to (x,y)=(',x_default,',',y_default,')'
       end if
       set_x=.true.
       set_y=.true.
       xtrans=x_default
       ytrans=y_default
       call test_set_translation_list(set_xlist,xlist,xtrans,'x-list','x',groups,print_help)
       call test_set_translation_list(set_ylist,ylist,ytrans,'y-list','y',groups,print_help)       
    elseif(.not.set_xlist)then
       set_x=.true.
       xtrans=0
       call test_set_translation_list(set_xlist,xlist,xtrans,'x-list','x',groups,print_help)
    elseif(.not.set_ylist)then
       set_y=.true.
       ytrans=0
       call test_set_translation_list(set_ylist,ylist,ytrans,'y-list','y',groups,print_help)
       !! else Both variables are set
    end if
    
    if(.not.set_sval)then
       sval=sval_default
       set_sval=.true.
    end if
    !!Initialize the wave-function (we have everything we need)
    call initialize_wave_function(groups,kappa_matrix,Ne,Ns,xlist,ylist,kappa_bar,&
         .not.set_no_asym,qp_list,k_denom,qp_pow=(/qp_pow_denom,qp_pow_nomin/),qp_static=set_qp_static,use_cft_scale=set_cft_scale)
    
    !!As a result of the translations, the K-sector changes by 
    !!K_sector_shift. This we need to shift back
    
    K_sector_shift=0
    do n=1,groups
       !!Add upp all the shifts induces by y-translation of the different groups
       K_sector_shift=K_sector_shift+group_sizes(n)*ylist(n)
    end do
    write(*,*) 'The K-sector shift induced is',mod(K_sector_shift,Ns)
    
    !!Compute the K-value and initalize the wave-function boundary conditions
    if(.not.set_K)then !!WE set the default value of K=0 
       write(stderr,*) 'WARNING: No K-sector has been specified.'
       write(stderr,*) '         Defaulting to Ksector=',K_default
       set_K=.TRUE.
       Ksector=K_default
    end if
    
    !!write(*,*) 'Setting BC:s'
    if(set_dual_index)then
       call set_CM_BC(Ns,Ksector-K_sector_shift,groups,kappa_matrix,kappa_bar,Dual_index,&
            Nq=qp_list,k_denom=k_denom,sval=sval,CoMindx=COMI)
    else !!If the dual index are not-set, don't use them
       call set_CM_BC(Ns,Ksector-K_sector_shift,groups,kappa_matrix,kappa_bar,Nq=qp_list,&
            k_denom=k_denom,sval=sval,CoMindx=COMI)
    end if
    !!write(*,*) 'BC:s set'

    
    if(set_I.and.set_log)then !!Convert from log scale
       tau_im=exp(tau_im)
    end if
    
    call set_default_real(set_R,tau_re,R_default,'tau_re','tau_re')
    call set_default_real(set_I,tau_im,I_default,'tau_im','tau_im')
    call set_default_integer(set_MC,MC,MC_default,'Monte-Calo ponts','MC')
    
    !!Test that tau_im > 0. 
    call check_positive_float(tau_im,'-I',print_help)
    
    !!Set tau
    tau=tau_re+iunit*tau_im
    
    if(qps.gt.0)then!!Only need to load qps if there are any
       call load_qps() !!Sent qp_posision to cft wfn memory
    end if
    write(*,*) '---Wfn initialized ----'


  end subroutine init_wfn


  subroutine load_qps()
    !!Here we look and see if the qps have beeen suplied either as comand line or as separate file
    Integer  :: qpxx = 468, qpyy = 469
    if(set_qp_posx.and.set_qp_filex)then
       write(stderr,*) 'ERROR: Both comandline and file inpus for qp x-position has been supplied'
       call exit(-1)
    elseif(set_qp_filex)then !!Load the qp-positions from file
       OPEN(qpxx,FILE=TRIM(qp_filex),STATUS='old')
       read(qpxx,*)  qp_posx(:)
       write(*,*) 'qp coordinates xx:',qp_posx(:)
       CLOSE(qpxx)
    elseif(set_qp_posx)then !!Load the qp-positions from comand line
       !!NOthig to do actaully
    else !!!DEfault to x-pos=0
       qp_posx=0.d0 
       write(stderr,*) 'WARNING: No qp x-position information has been given'
       write(stderr,*) '         Defaulting to qp x-position=',qp_posx
    end if

    if(set_qp_posy.and.set_qp_filey)then
       write(stderr,*) 'ERROR: Both comandline and file inpus for qp y-position has been supplied'
       call exit(-1)
    elseif(set_qp_filey)then !!Load the qp-positions from file
       OPEN(qpyy,FILE=TRIM(qp_filey),STATUS='old')
       read(qpyy,*)  qp_posy(:)
       write(*,*) 'qp coordinates yy:',qp_posy(:)
       CLOSE(qpyy)
    elseif(set_qp_posy)then !!Load the qp-positions from comand line
       !!NOthig to do actaully
    else !!!DEfault to y-pos=0
       qp_posy=0.d0 
       write(stderr,*) 'WARNING: No qp y-position information has been given'
       write(stderr,*) '         Defaulting to qp y-position=',qp_posy
    end if
    
    call load_qp_positions(qp_posx,qp_posy)
   
  end subroutine load_qps


  subroutine compute_wave_function()
    character(100) :: out_dir   
    
    call set_output_dir(output_dir)
    call get_output_dir(out_dir)
    call SampleTheWaveFunction(MC,set_reuse,Ne,&
         TRIM(out_dir)//'/chiral_re.rnd',TRIM(out_dir)//'/chiral_im.rnd',&
         tau,chiral_sym_psi,.TRUE.,Ns)
    
  end subroutine compute_wave_function
  
  
end module chiral_wfn

