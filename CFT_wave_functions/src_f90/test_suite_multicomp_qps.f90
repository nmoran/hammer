program test_chiral_wave_function
  
  USE typedef     ! types and definitions
  use test_utilities
  use k_matrix
  use chiral_wave_function
  use center_of_mass
  use misc_random
  
  !!Variables
  IMPLICIT NONE  
  
  !!Test starts here!!!
  write(*,*) '         Test the Chiral wave-functions'
  !! Testing the many-body wave-function
  call test_pbc_non_anti_sym(.FALSE.)
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  call test_pbc_non_anti_sym(.TRUE.)
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  call test_anti_symmetrization
  write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
contains  
  
  !!----------------------------------------------
  !!     Test the manu body wave-functions
  !!---------------------------------------------
  subroutine test_pbc_non_anti_sym(TEST_PBCS)
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,kappa_matrix_large
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist,qps_list
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: XQcoord,YQcoord
    integer :: qps, fluxes,n2
    integer :: q,cells,D,p,indx,Ne,Ns,raw_k_sector
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,extra_phase,tau_x,tau_y
    logical, intent(IN) :: TEST_PBCS
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    call set_cft_wfn_verbose(.FALSE.)
    if(TEST_PBCS)then
       write(*,*) 'Test that correct periodic boundary conditions can be set and are satisfied'
       !!This is really a test of eqn (A34) in PRB 89, 125303 (2014)
       !!with h=t=0
       !! but for the non-chiral case
    else
       write(*,*) 'Test that h=t=0 periodic boundary conditions are satisfied'
       !!This is really a test of eqn (A34) in PRB 89, 125303 (2014)
       !!with h,t,chosen to obtain periodic bcs
       !! but for the non-chiral case
    end if
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    do D=1,3
       ALLOCATE(kappa_matrix(D,D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D),qps_list(D))
       !!Initialize the n-list 
       maxlist=2
       maxlist(1)=3
       minlist=0
       minlist(1)=1
       nlist=minlist
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          do cells=1,3 !!Loop over different number of fundamental cells
             !!Sets number of electrons
             Ne=p*cells
             
             !!Initialize the cooridinates (use unit length in X).
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             allocate(kappa_matrix_large(Ne,Ne))
             
             do fluxes=0,3 !!Loop over different number of extra fluxes
                !write(*,*) 'System stats: D,Ne,fluxes'
                !write(*,*) D,Ne,fluxes
                !!NB: We do the simpest thing here, we add one pq in each layer.
                !! There are more advances setups one can trye
                !! FIXME: implement some specific test-cases for these bellow
                Ns=q*cells+fluxes !!Fluxes add to number of states
                qps=D*fluxes !!One qp per flux per layer (simplest setup)
                qps_list=(/(fluxes, n2=1,D)/)
                ALLOCATE(XQcoord(qps),YQcoord(qps))
                
                call RANDOM_NUMBER(tau_x)
                call RANDOM_NUMBER(tau_y)
                !Put tau_x in range -1/2 < tau_x < 1/2
                tau_x=tau_x-.5
                !Put tau_y in range 1/2 < tau_x < 1+1/2
                tau_y=tau_y+.5
                tau=tau_x+iunit*tau_y
                call RANDOM_NUMBER(Xcoord)
                call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
                Xcoord=Xcoord-.5d0
                Ycoord=Ycoord-.5d0
                
                call RANDOM_NUMBER(XQcoord)
                call RANDOM_NUMBER(YQcoord)
                !!Make the random number in the range -1/2 < x < 1/2
                XQcoord=XQcoord-.5d0
                YQcoord=YQcoord-.5d0
                

                if(TEST_PBCS)then
                   !!Set the boudnary conditions to periodic
                   !write(*,*) 'TEST: Compute the Raw k-sector:'
                   raw_k_sector=compute_raw_K_sector(D,Ne,Ns,kappa_matrix,plist,q,qps_list)
                   !write(*,*) 'TEST: raw k_sector is',raw_k_sector
                   call set_CM_BC(Ns,raw_k_sector,D,kappa_matrix,Nq=qps_list)
                   !write(*,*) 'TEST: BC:s set'
                else
                   call set_trivial_CM_BC(D,kappa_matrix)
                   call construct_large_K_matrix(D,Ne,kappa_matrix,&
                        kappa_matrix_large,cells*plist)
                   !write(*,*) 'Large kappa-matrix'
                   !call print_int_matrix(kappa_matrix_large)
                end if
                
                call initialize_wave_function(D,kappa_matrix,Ne,Ns&
                     ,(/(0, n2=1,D)/),(/(0, n2=1,D)/),qps=qps_list)
                !!Compute first wave function
                psi=chiral_raw_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                do indx=1,p
                   !!Change the coordinates one by one in x-direction
                   Xcoord2=Xcoord
                   Ycoord2=Ycoord
                   Xcoord2(indx)=Xcoord2(indx)+1.d0
                   psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                   !!Change the coordinates one by one in y-direction
                   Xcoord2=Xcoord
                   Ycoord2=Ycoord
                   Ycoord2(indx)=Ycoord2(indx)+1.d0
                   !!Compute the gaguge phase
                   gauge_phase=Xcoord(indx)*2*pi*Ns
                   psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)&
                        +iunit*gauge_phase !!Add the gauge phase
                   if(.not.TEST_PBCS)then
                      !!Compute the extra phase (the expected phase)
                      extra_phase=pi*(Ns-kappa_matrix_large(indx,indx))
                      psi_2x=psi_2x+iunit*extra_phase
                      psi_2y=psi_2y+iunit*extra_phase
                   end if
                   !!Reduce to smallest mod_2pi
                   psi_2x=mod_2pi(psi_2x)
                   psi_2y=mod_2pi(psi_2y)
                   
                   !!Test the periodic boundary conditions             
                   if(test_diff_exp_cmplx(psi,psi_2x,1.d-08)&
                        .or.&
                        test_diff_exp_cmplx(psi,psi_2y,1.d-08))then
                      write(*,'(A,A,I3,A)') '.-.-.-.-.-.-.'&
                           ,'Change coordinates for particle no.',indx&
                           ,'.-.-.-.-.-.-'
                      write(*,*) 'Not-periodic'
                      write(*,*) 'For kappa-matrix'
                      call print_int_matrix(kappa_matrix)
                      write(*,*) 'tau:',tau
                      write(*,*) 'cells:',cells
                      call print_the_psis(psi,psi_2x,psi_2y)
                      write(*,*) 'Coordinates Z:'
                      write(*,*) Xcoord+tau*Ycoord
                      call exit(-2)
                   end if
                end do
                DEALLOCATE(XQcoord,YQcoord)
             end do  !!Next flux size
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
             deallocate(kappa_matrix_large)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_nlist(D,nlist,maxlist,minlist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist,qps_list)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_pbc_non_anti_sym
  
  
  

  
  subroutine test_anti_symmetrization
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist
    integer :: q,cells,D,p,Ne,Ns
    integer :: part1,part2
    complex(KIND=dpc) :: tau,psi,psi_2x,psi_2y
    real(kind=dp) :: tau_x,tau_y
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix_large
    integer, ALLOCATABLE, DIMENSION(:) :: qps_list
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: XQcoord,YQcoord
    integer :: qps, fluxes,n2
    
    !!This function test that anti-symetrization of the wave-functions can be done
    write(*,*) 'Test that the (anti)-symetrization works'
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    call set_cft_wfn_verbose(.FALSE.)

    !!WE start with chiral hierarchy states!!
    do D=1,3
       ALLOCATE(kappa_matrix(D,D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D),qps_list(D))
       !!Initialize the n-list 
       maxlist=2
       maxlist(1)=3
       minlist=0
       minlist(1)=1
       nlist=minlist
       if(D.eq.3)then
          maxlist=(/2,0,0/)
       end if
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          write(*,*) '---------------------'
          call print_int_matrix(kappa_matrix)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          do cells=1,3 !!Loop over different number of fundamental cells
             !!Sets number of electrons
             Ne=p*cells
             
             !!Initialize the cooridinates (use unit length in X).
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             allocate(kappa_matrix_large(Ne,Ne))
             
             do fluxes=0,3 !!Loop over different number of extra fluxes
                !write(*,*) 'System stats: D,Ne,fluxes'
                !write(*,*) D,Ne,fluxes
                
                !!NB: We do the simpest thing here, we add one pq in each layer.
                !! There are more advances setups one can trye
                !! FIXME: implement some specific test-cases for these bellow
                Ns=q*cells+fluxes !!Fluxes add to number of states

                !!FIXME: There are occations when Ne>Ns
                !! this will cause the antisymmetrized wave fucntoin to be zero.
                !! WE therefore test if Ne>Ns
                if(Ne.gt.Ns)then
                   !!If it is larger, try next test
                   !write(*,*) '---------------------'
                   !write(*,*) 'Ne was greater than Ns'
                   !call print_int_matrix(kappa_matrix)
                   !write(*,*) '---------------------'
                   CYCLE
                else
                   !write(*,*) '---------------------'
                   !write(*,*) 'Ne was smaller than Ns'
                   !call print_int_matrix(kappa_matrix)
                   !write(*,*) '---------------------'
                end if


                qps=D*fluxes !!One qp per flux per layer (simplest setup)
                qps_list=(/(fluxes, n2=1,D)/)
                ALLOCATE(XQcoord(qps),YQcoord(qps))
                
                call RANDOM_NUMBER(tau_x)
                call RANDOM_NUMBER(tau_y)
                !Put tau_x in range -1/2 < tau_x < 1/2
                tau_x=tau_x-.5
                !Put tau_y in range 1/2 < tau_x < 1+1/2
                tau_y=tau_y+.5
                tau=tau_x+iunit*tau_y
                call RANDOM_NUMBER(Xcoord)
                call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
                Xcoord=Xcoord-.5d0
                Ycoord=Ycoord-.5d0
                
                call RANDOM_NUMBER(XQcoord)
                call RANDOM_NUMBER(YQcoord)
                !!Make the random number in the range -1/2 < x < 1/2
                XQcoord=XQcoord-.5d0
                YQcoord=YQcoord-.5d0
                
                call set_trivial_CM_BC(D,kappa_matrix)
                call construct_large_K_matrix(D,Ne,kappa_matrix,&
                     kappa_matrix_large,cells*plist)
                
                call initialize_wave_function(D,kappa_matrix,Ne,Ns&
                     ,(/(n2-1, n2=1,D)/),(/(n2-1, n2=1,D)/),qps=qps_list)
                !!Compute first wave function
                psi=chiral_sym_psi(Xcoord,Ycoord,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)

                do part1=1,(Ne-1)
                   do part2=(part1+1),Ne
                      !!Set the new coordinates
                      Xcoord2=Xcoord
                      Ycoord2=Ycoord
                      !!Trade places of particle one and two
                      Xcoord2(part1)=Xcoord(part2)
                      Ycoord2(part1)=Ycoord(part2)
                      Xcoord2(part2)=Xcoord(part1)
                      Ycoord2(part2)=Ycoord(part1)
                      
                      !!Compute the wfn again
                      psi_2x=chiral_sym_psi(Xcoord2,Ycoord2,tau,inject_Xqps=XQcoord,inject_Yqps=YQcoord)
                      !!We expect a shift of pi from the (anti)symetrization
                      !!Reduce to smallest mod_2pi
                      psi_2y=mod_2pi(psi_2x+iunit*pi*kappa_matrix(1,1))
                      !!it's sufficient to look at first kappa-matrix entrry,
                      !!as the diagonals should have the same parity
                      
                      !!Test the anti-symmetry             
                      if(test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                         write(*,'(A,A,I3,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                              ,'Not (anti)-summetric for particle no ',part1&
                              ,' and ',part2,' of ',Ne&
                              ,' particles .-.-.-.-.-.-'
                         write(*,*) 'Num of qps:',qps_list
                         write(*,*) 'For kappa-matrix'
                         call print_int_matrix(kappa_matrix)
                         write(*,*) 'tau:',tau
                         write(*,*) 'cells:',cells
                         write(*,*) 'Output phase different'
                         call print_the_1psis(psi,psi_2x)
                         write(*,*) 'Corrected phase different'
                         call print_the_1psis(psi,psi_2y)
                         write(*,*) 'Coordinates Z:'
                         write(*,*) Xcoord+tau*Ycoord
                         call exit(-2)
                      end if
                   end do
                end do
                DEALLOCATE(XQcoord,YQcoord)
             end do  !!Next flux size
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
             deallocate(kappa_matrix_large)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_nlist(D,nlist,maxlist,minlist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist,qps_list)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_anti_symmetrization

  !!----------------------------------------------------
  !!                  MISC
  !!....................................................
  
  subroutine print_the_psis(psi,psi_2x,psi_2y)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-x:',mod_2pi(psi_2x)
    write(*,*) 'psi-y:',mod_2pi(psi_2y)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi):',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi):',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_psis

  subroutine print_the_1psis(psi,psi_2)
    complex(kind=dpc), intent(in) :: psi,psi_2
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-2:',mod_2pi(psi_2)
    write(*,*) 
    write(*,*) 'diff/(2*pi):',(mod_2pi(psi_2)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_1psis


  subroutine print_the_3psis(psi,psi_2x,psi_2y,psi_2xy)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y,psi_2xy
    write(*,*) 'psi  : ',mod_2pi(psi)
    write(*,*) 'psi-x: ',mod_2pi(psi_2x)
    write(*,*) 'psi-y: ',mod_2pi(psi_2y)
    write(*,*) 'psi-xy:',mod_2pi(psi_2xy)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi): ',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi): ',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-xy/(2*pi):',(mod_2pi(psi_2xy)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_3psis

  
  logical function next_nlist(D,nlist,maxlist,minlist)
    integer, intent(IN) :: D,maxlist(D),minlist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_nlist=.FALSE.
    !write(*,*) 'maxlist',maxlist
    !write(*,*) 'minlist',minlist
    !write(*,*) 'nlist_in',nlist
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          if(n.eq.1)then 
             nlist(n)=nlist(n)+1
          else
             nlist(n)=nlist(n)+2
          end if
          next_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
    !write(*,*) 'nlist_out',nlist
    !write(*,*) 
  end function next_nlist
  
  
  logical function next_parity_nlist(D,nlist,maxlist,minlist,inclist)
    integer, intent(IN) :: D,maxlist(D),minlist(D),inclist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_parity_nlist=.FALSE.
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          !!Not-max (incement)
          nlist(n)=nlist(n)+inclist(n)
          next_parity_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
  end function next_parity_nlist

  logical function same_group(N1,N2,plist,cells) result(same)
    !!!Tesst if N1 and N2 are fromt he same group
    integer, intent(in) :: N1,N2,plist(:), cells
    integer :: Nlist(size(plist)),G1,G2,n,D,Ntmp
    !!Initate G1,G2
    G1=0
    G2=0
    Nlist=plist*cells
    D=size(plist)
    Ntmp=0
    do n=1,D
       if(N1.gt.Ntmp.and.N1.le.(Ntmp+Nlist(n)))then
          G1=n !!This gets set eventually
       end if
       if(N2.gt.Ntmp.and.N2.le.(Ntmp+Nlist(n)))then
          G2=n !!This gets set eventually
       end if
       Ntmp=Ntmp+Nlist(n)
    end do
    !write(*,*) "N1,N2:",N1,N2
    !write(*,*) "G1,G2:",G1,G2
    same=(G1.eq.G2)
  end function same_group
  
end program test_chiral_wave_function

