MODULE matrix
  ! 
  ! This module defines some matrix operations that are not directly found
  ! from f95. Most important ones are the determinants and inverses of 
  ! matrices of different KINDs.
  ! 
  
  IMPLICIT NONE
  
  INTERFACE det2
     MODULE PROCEDURE r_det
     MODULE PROCEDURE c_det
  END INTERFACE det2
  
  INTERFACE sum2
     MODULE PROCEDURE d_sum2
  END INTERFACE sum2
  
  INTERFACE mat_inv
     MODULE PROCEDURE d_inv
     MODULE PROCEDURE z_inv
  END INTERFACE mat_inv
  
  INTERFACE det
     MODULE PROCEDURE s_det2
     MODULE PROCEDURE d_det2
     MODULE PROCEDURE c_det2
     MODULE PROCEDURE z_det2
     ! Using LAPACK's LU
     !       sgetrf            Computes an LU factorization of a
     !       dgetrf            general matrix, using partial pivoting
     !       cgetrf            with row interchanges.
     !       zgetrf
  END INTERFACE det
  INTERFACE write_mat
     MODULE PROCEDURE r_write_mat1
     MODULE PROCEDURE r_write_mat
     MODULE PROCEDURE c_write_mat
     MODULE PROCEDURE i_write_mat
     MODULE PROCEDURE l_write_mat
  END INTERFACE write_mat
  
CONTAINS
  SUBROUTINE z_inv (mat,matinv)
    
    CHARACTER,PARAMETER :: trans='N'
    COMPLEX(KIND=KIND(1.d0)), INTENT(IN) :: mat(:,:)
    !    COMPLEX(KIND=KIND(1.d0)), DIMENSION(SIZE(mat(:,1)),SIZE(mat(1,:))) ::&
    COMPLEX(KIND=KIND(1.d0)), DIMENSION(SIZE(mat,1),SIZE(mat,2)) ::&
         & matinv, matdum
    INTEGER, DIMENSION(SIZE(mat,1)) :: IPIV
    INTEGER :: nsize,msize,i,INFO
    
    msize=SIZE(mat,1)
    nsize=SIZE(mat,2)
    matdum=mat
    matinv=0.0d0
    DO i=1,msize
       matinv(i,i)=1.0d0
    END DO
    INFO=-1
    CALL zGETRF(msize,nsize,matdum,nsize,IPIV,INFO)
    IF (INFO /= 0) then
       write(*,*) 'Error with z_inv 1'
       call exit(-1)
    end IF
    INFO=-1
    CALL zGETRS(trans,msize,nsize,matdum,nsize,IPIV,matinv,msize,INFO)
    IF (INFO /= 0)then
       write(*,*) 'Error with z_inv 2'
       call exit(-1)
    end IF
  END SUBROUTINE z_inv
  
  SUBROUTINE d_inv (mat,matinv)
    
    CHARACTER,PARAMETER :: trans='N'
    REAL(KIND=KIND(1.d0)), INTENT(IN) :: mat(:,:)
    !    REAL(KIND=KIND(1.d0)), DIMENSION(SIZE(mat(:,1)),SIZE(mat(1,:))) ::&
    REAL(KIND=KIND(1.d0)), DIMENSION(SIZE(mat,1),SIZE(mat,2)) ::&
         & matinv, matdum
    INTEGER, DIMENSION(SIZE(mat,1)) :: IPIV
    INTEGER :: nsize,msize,i,INFO
    
    msize=SIZE(mat,1)
    nsize=SIZE(mat,2)
    matdum=mat
    matinv=0.0d0
    DO i=1,msize
       matinv(i,i)=1.0d0
    END DO
    INFO=-1
    CALL dGETRF(msize,nsize,matdum,nsize,IPIV,INFO)
    IF (INFO /= 0) THEN
       WRITE(*,*) 'Warning from d_inv 1', INFO
       call exit(-1)
    END IF
    INFO=-1
    CALL dGETRS(trans,msize,nsize,matdum,nsize,IPIV,matinv,msize,INFO)
    IF (INFO /= 0) then
       write(*,*) 'Error with d_inv 2'
       call exit(-1)
    end IF
  END SUBROUTINE d_inv
  
  FUNCTION d_sum2(A) RESULT(sum2)
    
    REAL(KIND=KIND(0.d0)) :: sum2, A(:)
    REAL(KIND=KIND(0.d0)), DIMENSION(SIZE(a(:))) :: Orig
    INTEGER :: N
    REAL(KIND=KIND(0.d0)) :: dnrm2
    
    N=SIZE(A(:))
    
    IF (N==0) THEN
       sum2=1.d0
    ELSE
       Orig=A
       sum2=dnrm2(N,Orig,1)
    END IF
  END FUNCTION d_sum2
  
  FUNCTION mat_inv2(a)
    
    INTEGER :: N, i, j
    REAL(KIND=KIND(0.d0)) :: a(:,:)!!, d
    REAL(KIND=KIND(0.d0)), DIMENSION(SIZE(A,1),SIZE(a,2)) ::Orig, mat_inv2
    !!INTEGER, DIMENSION(SIZE(a,1)) :: indx
    N=SIZE(a(:,1))
    
    Orig=A
    mat_inv2=0.d0
    DO I=1, N
       mat_inv2(I,I)=1.d0
    END DO
    write(*,*) 'NOT implemented'
    call exit(-1)
    !CALL ludcmp(a,N,N,indx,d)
    DO J=1, N
       write(*,*) 'NOT implemented, but you never get here'
       call exit(-1)
       !CALL lubksb(a,N,N,indx,mat_inv2(1,J))
    END DO
    A=Orig
    
  END FUNCTION mat_inv2
  FUNCTION r_det(A)
    
    IMPLICIT NONE
    REAL(KIND=KIND(0.d0)) :: r_det, A(:,:)!!, d
    REAL(KIND=KIND(0.d0)), DIMENSION(SIZE(A,1),SIZE(a,2)) :: Orig
    INTEGER :: j, N
    !!INTEGER, DIMENSION(SIZE(A,1)) :: indx
    N=SIZE(A(:,1))
    
    Orig=A
    write(*,*) 'not implemented'
    call exit(-1)
    !CALL ludcmp(A,N,N,indx,r_det)
    DO j=1, N
       r_det=r_det*A(j,j)
    END DO
    A=Orig
  END FUNCTION r_det
  
  FUNCTION c_det(A)
    
    IMPLICIT NONE
    COMPLEX(KIND=KIND(0.d0)) :: c_det, A(:,:)
    COMPLEX(KIND=KIND(0.d0)), DIMENSION(SIZE(A,1),SIZE(a,2)) :: Orig
    INTEGER :: j, N
    !!INTEGER, DIMENSION(SIZE(A,1)) :: indx
    N=SIZE(A(:,1))
    
    Orig=A
    write(*,*) 'not implemented'
    call exit(-1)
    !CALL c_ludcmp(A,N,N,indx,c_det)
    DO j=1, N
       c_det=c_det*A(j,j)
    END DO
    A=Orig
  END FUNCTION c_det
  
  FUNCTION z_det2(A) RESULT(det)
    
    IMPLICIT NONE
    COMPLEX(KIND=KIND(0.d0)) :: det, A(:,:)
    COMPLEX(KIND=KIND(0.d0)), DIMENSION(SIZE(A,1),SIZE(A,2)) :: B
    INTEGER :: j, N, err, p
    INTEGER, DIMENSION(SIZE(A,1)) :: indx
    N=SIZE(A(:,1))
    
    IF (N==0) THEN
       det=(1.d0,0.d0)
    ELSE
       !CALL zgemt('N',N,N,(1.d0,0.d0),A,N,B,N)
       B=A
       CALL zgetrf(N,N,B,N,indx,err)
       IF (err < 0) THEN 
          WRITE(*,*) err,' number'
          write(*,*) 'Error with det z_det2'
          call exit(-1)
       END IF
       p=0
       det=(1.d0,0.d0)
       DO j=1, N
          det=det*B(j,j)/SQRT(REAL(j,KIND=KIND(1.d0)))
          IF (indx(j)/=j) THEN
             !	  p=p+1
             det=-det
          END IF
       END DO
       !      det=det*(-1)**p
    END IF
  END FUNCTION z_det2
  FUNCTION d_det2(A) RESULT(det)
    
    IMPLICIT NONE
    REAL(KIND=KIND(0.d0)) :: det, A(:,:)
    REAL(KIND=KIND(0.d0)), DIMENSION(SIZE(A,1),SIZE(a,2)) :: B
    INTEGER :: j, N, err, p
    INTEGER, DIMENSION(SIZE(A,1)) :: indx
    N=SIZE(A(:,1))
    
    IF (N==0) THEN
       det=1.d0
    ELSE
       B=A
       CALL dgetrf(N,N,B,N,indx,err)
       IF (err /= 0) THEN 
          WRITE(*,*) 'Number ', err
          write(*,*) 'Error with d_det2'
          call exit(-1)
       END IF
       !	  
       !THEN
       !STOP 'Not tested yet'
       !     ELSE
       !
       !      END IF
       p=0
       det=1.d0
       DO j=1, N
          det=det*B(j,j)/SQRT(REAL(j,KIND=KIND(1.d0)))
          IF (indx(j)/=j) THEN
             p=p+1
             !	  det=-det
          END IF
       END DO
       IF (MOD(p,2)==1) det=-det
       !      det=det*(-1)**p
       !      A=Orig
    END IF
  END FUNCTION d_det2
  FUNCTION s_det2(A) RESULT(det)
    
    IMPLICIT NONE
    REAL(KIND=KIND(0.e0)) :: det, A(:,:)
    REAL(KIND=KIND(0.e0)), DIMENSION(SIZE(A,1),SIZE(a,2)) :: Orig
    INTEGER :: j, N, err, p
    INTEGER, DIMENSION(SIZE(A,1)) :: indx
    N=SIZE(A(:,1))
    
    IF (N==0) THEN
       det=1.e0
    ELSE
       Orig=A
       CALL sgetrf(N,N,A,N,indx,err)
       !    IF (err == 0) STOP 'Error with det'
       IF (err == 0) THEN
          write(*,*) 'Not tested yet'
          call exit(-1)
       ELSE
          write(*,*) 'Error with det'
          call exit(-1)
       END IF
       p=0
       det=1.e0
       DO j=1, N
          det=det*A(j,j)
          IF (indx(j)/=j) THEN
             p=p+1
          END IF
       END DO
       det=det*(-1)**p
       A=Orig
    END IF
  END FUNCTION s_det2
  FUNCTION c_det2(A) RESULT(det)
    
    IMPLICIT NONE
    COMPLEX(KIND=KIND(0.e0)) :: det, A(:,:)
    COMPLEX(KIND=KIND(0.e0)), DIMENSION(SIZE(A,1),SIZE(a,2)) :: Orig
    INTEGER :: j, N, err, p
    INTEGER, DIMENSION(SIZE(A,1)) :: indx
    N=SIZE(A(:,1))
    
    IF (N==0) THEN
       det=(1.e0,1.e0)
    ELSE
       Orig=A
       CALL cgetrf(N,N,A,N,indx,err)
       !    IF (err == 0) STOP 'Error with det'
       IF (err == 0) THEN
          write(*,*) 'Not tested yet'
          call exit(-1)
       ELSE
          write(*,*) 'Error with det'
          call exit(-1)
       END IF
       p=0
       det=(1.e0,0.e0)
       DO j=1, N
          det=det*A(j,j)
          IF (indx(j)/=j) THEN
             p=p+1
          END IF
       END DO
       det=det*(-1)**p
       A=Orig
    END IF
  END FUNCTION c_det2
  SUBROUTINE r_write_mat(A,to)
    IMPLICIT NONE
    REAL(KIND=KIND(0.d0)) :: A(:,:)
    INTEGER :: i, j, N, to
    N=SIZE(A(1,:))
    WRITE(to,'(A)',advance='no') '#------------'
    DO i=2, N-1
       WRITE(to,'(A)',advance='no') '-------------'
    END DO
    WRITE(to,'(A)',advance='yes') '-------------'
    DO i=1, SIZE(A(:,1))
       DO j=1, N-1
          WRITE(to,'(ES14.5E3)',advance='no') A(i,j)
       END DO
       j=N
       WRITE(to,'(ES14.5E3)',advance='yes')  A(i,j)
    END DO
    WRITE(to,'(A)',advance='no') '#------------'
    DO i=2, N-1
       WRITE(to,'(A)',advance='no') '-------------'
    END DO
    WRITE(to,'(A)',advance='yes') '-------------'
  END SUBROUTINE r_write_mat
  SUBROUTINE c_write_mat(A,to)
    IMPLICIT NONE
    COMPLEX(KIND=KIND(0.d0)) :: A(:,:)
    INTEGER :: i, j, N, to
    N=SIZE(A(:,1))
    !  DO i=1, N-1
    !    WRITE(to,'(A)',advance='no') '-------------'
    !  END DO
    !  WRITE(to,'(A)',advance='yes') '-------------'
    DO i=1, SIZE(A(1,:))
       DO j=1, N-1
          WRITE(to,'(A,ES12.5,ES13.5,A)',advance='no') '(',A(i,j),') '
       END DO
       j=N
       WRITE(to,'(A,ES12.5,ES13.5,A)',advance='yes')  '(',A(i,j),') '
    END DO
    DO i=1, N-1
       WRITE(to,'(A)',advance='no') '-------------'
    END DO
    WRITE(to,'(A)',advance='yes') '-------------'
  END SUBROUTINE c_write_mat
  SUBROUTINE i_write_mat(A,to)
    IMPLICIT NONE
    INTEGER :: A(:,:)
    INTEGER :: i, j, N, to
    N=SIZE(A(:,1))
    DO i=1, N-1
       WRITE(to,'(A)',advance='no') '-------------'
    END DO
    WRITE(to,'(A)',advance='yes') '-------------'
    DO i=1, SIZE(A(1,:))
       DO j=1, N-1
          WRITE(to,'(i10)',advance='no') A(i,j)
       END DO
       j=N
       WRITE(to,'(i10)',advance='yes') A(i,j)
    END DO
    DO i=1, N-1
       WRITE(to,'(A)',advance='no') '-------------'
    END DO
    WRITE(to,'(A)',advance='yes') '-------------'
  END SUBROUTINE i_write_mat
  SUBROUTINE l_write_mat(A,to)
    IMPLICIT NONE
    LOGICAL :: A(:,:)
    INTEGER :: i, j, N, to
    N=SIZE(A(:,1))
    DO i=1, N-1
       WRITE(to,'(A)',advance='no') '-------------'
    END DO
    WRITE(to,'(A)',advance='yes') '-------------'
    DO i=1, SIZE(A(1,:))
       DO j=1, N-1
          WRITE(to,'(l6)',advance='no') A(i,j)
       END DO
       j=N
       WRITE(to,'(l6)',advance='yes') A(i,j)
    END DO
    DO i=1, N-1
       WRITE(to,'(A)',advance='no') '-------------'
    END DO
    WRITE(to,'(A)',advance='yes') '-------------'
  END SUBROUTINE l_write_mat
  SUBROUTINE r_write_mat1(A,to)
    IMPLICIT NONE
    REAL(KIND=KIND(0.d0)) :: A(:)
    INTEGER :: j, N, to
    N=SIZE(A(:))
    DO j=1, N-1
       WRITE(to,'(ES13.5)',advance='no') A(j)
    END DO
    j=N
    WRITE(to,'(ES13.5)',advance='yes')  A(j)
  END SUBROUTINE r_write_mat1
  
END MODULE matrix
