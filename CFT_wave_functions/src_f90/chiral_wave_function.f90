module chiral_wave_function
  use typedef
  use k_matrix
  use jacobitheta
  use center_of_mass
  use combinations
  use dedekind_eta_function
  use misc_matrix
  use permutations
  use omp_lib
  use jastrow_factor
  !!This module computes the chiral wave functions
  !!In this class fall the chiral hierarchy, but also other states
  
  implicit none
  
  integer :: working_Ns,working_raw_Ns,working_Ne,working_size
  integer :: working_q_group,working_p_group
  integer :: working_tot_qps,working_k_denom
  integer :: working_qp_pow(2),working_inv_denom
  integer, ALLOCATABLE, DIMENSION(:) :: working_qp_list
  integer, ALLOCATABLE, DIMENSION(:) :: working_qp_start
  integer, ALLOCATABLE, DIMENSION(:) :: working_qp_stop
  integer, ALLOCATABLE, DIMENSION(:,:) :: working_inv_k_matrix
  integer, ALLOCATABLE, DIMENSION(:,:) :: working_kappa_matrix
  integer, ALLOCATABLE, DIMENSION(:,:) :: working_k_bar_matrix
  real(kind=dp), ALLOCATABLE, DIMENSION(:,:) :: working_kappa_matrix_dp
  real(kind=dp), ALLOCATABLE, DIMENSION(:,:) :: working_k_bar_matrix_dp
  integer, ALLOCATABLE, DIMENSION(:,:) :: working_k_plus
  integer, ALLOCATABLE, DIMENSION(:,:) :: working_k_minus
  integer, ALLOCATABLE, DIMENSION(:,:) :: working_kappa_matrix_large
  integer, ALLOCATABLE, DIMENSION(:,:) :: working_k_bar_matrix_large
  real(kind=dp), ALLOCATABLE, DIMENSION(:,:) :: working_kappa_matrix_large_dp
  real(kind=dp), ALLOCATABLE, DIMENSION(:,:) :: working_k_bar_matrix_large_dp
  integer, ALLOCATABLE, DIMENSION(:) :: working_group_size,working_p_list
  integer, ALLOCATABLE, DIMENSION(:) :: working_groups_stop
  integer, ALLOCATABLE, DIMENSION(:) :: working_groups_start
  integer, ALLOCATABLE, DIMENSION(:) :: work_tx,work_ty
  integer, ALLOCATABLE, DIMENSION(:) :: working_lorenz_gaussian
  real(kind=dp), ALLOCATABLE, DIMENSION(:) :: working_euclid_gaussian
  real(kind=dp), ALLOCATABLE, DIMENSION(:) :: load_Xqps,load_Yqps

  complex(kind=dpc) :: log_zero
  logical :: working_use_cft_scale,working_asym,working_qp_static
  logical :: initialized=.FALSE.
  logical :: chiral_kappa=.FALSE.
  logical :: cft_wfn_verbose=.TRUE.
  
  logical, parameter :: default_asym=.TRUE.
  logical, parameter :: default_use_cft_scale=.FALSE.
  logical, parameter :: default_qp_static=.FALSE.
  integer, parameter :: default_k_denom=1
  integer, parameter :: default_kappa_bar_matrix=0
  integer, parameter :: default_qps=0
  !!FIXME-not the default in the outer layer!
  !!There it is the more dynamic - pow=kappa_bar/q
  integer, parameter :: default_qp_pow(2)=(/0,1/) 
  
  private
  public chiral_raw_psi
  public chiral_sym_psi
  public chiral_sym_psi_alt
  public chiral_translated_psi
  public construct_large_k_matrix
  public initialize_wave_function
  !!FIXME: There is a good reason this fuction is external,
  !!       But i can't think of it now
  public add_exponentially !!Exported further
  public load_qp_positions
  !! set verboseness level
  public set_cft_wfn_verbose
  public get_lambda_mn
  
contains

  !! ----------------------------------------------
  !!   Initalization
  !!-----------------------------------------------
  !! This functions initalizes all the usefull stats
  !!     about the kappa-matrix that the different sub-functions need
  
  !!Use this to sett the verbosenes-level
  subroutine set_cft_wfn_verbose(Verbose_ON)
    logical, intent(IN) :: Verbose_ON
    cft_wfn_verbose=Verbose_ON
  end subroutine set_cft_wfn_verbose
    
  
  subroutine initialize_wave_function(Size,kappa,Ne,Ns,t_x_in,t_y_in,&
       kappa_bar,asym,qps,k_denom,qp_pow,qp_static,use_cft_scale)
    integer, intent(in) :: Ne,Ns,Size,kappa(Size,Size)
    integer, intent(in),optional :: kappa_bar(Size,Size),qps(Size),k_denom
    integer, intent(in),optional :: qp_pow(2)
    logical, intent(in),optional :: asym,qp_static,use_cft_scale
    integer, intent(in) :: t_x_in(Size),t_y_in(Size)
    
    if(initialized)then !!Test if initialized
       if(same_static_input(Size,kappa,Ne,Ns, kappa_bar,asym,qps,k_denom,qp_pow,qp_static,use_cft_scale))then
          !!Just update the t_x and t_y (as these are allowed to change)
          !!Set the translations
          work_tx=t_x_in
          work_ty=t_y_in
       else !!If the statistics do not match!
          !write(*,*) 'Re-Initialize'
          call deinitialize_wave_function
          initialized=.FALSE.
       end if
    end if
    if(.not.initialized)then !!Test again for initialization, as it may have been unitialized
       call do_initialize_wave_function(Size,kappa,Ne,Ns,t_x_in,t_y_in,&
            kappa_bar,asym,qps,k_denom,qp_pow,qp_static,use_cft_scale)
    end if
  end subroutine initialize_wave_function
  
  logical function same_static_input(Size,kappa,Ne,Ns, kappa_bar,asym,qps,k_denom,&
       qp_pow,qp_static,use_cft_scale) result(same)
    integer, intent(in) :: Ne,Ns,Size,kappa(Size,Size)
    integer, intent(in),optional :: kappa_bar(Size,Size),qps(size),k_denom
    integer, intent(in),optional :: qp_pow(2)
    logical, intent(in),optional :: asym,qp_static,use_cft_scale
    integer :: n1,n2

    same = .TRUE.
    call test_if_same_integer(same,Ne,working_Ne)
    call test_if_same_integer(same,Ns,working_Ns)
    call test_if_same_integer(same,Size,working_Size)
    if(present(asym))then
       call test_if_same_logical(same,asym,working_asym)
    else
       call test_if_same_logical(same,default_asym,working_asym)
    end if
    if(present(use_cft_scale))then
       call test_if_same_logical(same,use_cft_scale,working_use_cft_scale)
    else
       call test_if_same_logical(same,default_use_cft_scale,working_use_cft_scale)
    end if
    if(present(qp_static))then
       call test_if_same_logical(same,qp_static,working_qp_static)
    else
       call test_if_same_logical(same,default_qp_static,working_qp_static)
    end if
    if(present(qps))then
       do n1=1,size
          call test_if_same_integer(same,qps(n1),working_qp_list(n1))
       end do
    else
       do n1=1,size
          call test_if_same_integer(same,default_qps,working_qp_list(n1))
       end do
    end if
    if(present(k_denom))then
       call test_if_same_integer(same,k_denom,working_k_denom)
    else
       call test_if_same_integer(same,default_k_denom,working_k_denom)
    end if
    if(present(qp_pow))then
       call test_if_same_integer(same,qp_pow(1),working_qp_pow(1))
       call test_if_same_integer(same,qp_pow(2),working_qp_pow(2))
    else
       call test_if_same_integer(same,default_qp_pow(1),working_qp_pow(1))
       call test_if_same_integer(same,default_qp_pow(2),working_qp_pow(2))
    end if

    !!Test kappa
    do n1=1,size
       do n2=1,size
          call test_if_same_integer(same,kappa(n1,n2),working_kappa_matrix(n1,n2))
          if(present(kappa_bar))then
             call test_if_same_integer(same,kappa_bar(n1,n2),working_K_bar_matrix(n1,n2))
          else
             call test_if_same_integer(same,default_kappa_bar_matrix,working_K_bar_matrix(n1,n2))
          end if
       end do
    end do
    

    if(.not.same)then
       if(cft_wfn_verbose)then
          write(stderr,*) 'WARNING: Reinitialization of wave function does not match stored values'
          write(stderr,*) '         Resetting with new values'
          write(stderr,*) 'No warning: call set_cft_wfn_verbose(.FALSE.)'
       end if
    end if
    
  end function same_static_input

  subroutine do_initialize_wave_function(Size,kappa_matrix,Ne,Ns,t_x_in,t_y_in,&
       kappa_bar_matrix,asym,qps,k_denom,qp_pow,qp_static,use_cft_scale)
    integer, intent(in) :: Ne,Ns,Size,kappa_matrix(Size,Size)
    integer, intent(in),optional :: kappa_bar_matrix(Size,Size),qps(size),k_denom
    logical, intent(in),optional :: asym,qp_static,use_cft_scale
    integer, intent(in),optional :: qp_pow(2)
    integer, intent(in) :: t_x_in(Size),t_y_in(Size)
    integer :: n
    
    if(present(asym))then !!If the asym variable is present
       working_asym=asym !!save it
    else !!If it's not present
       working_asym=default_asym !!Set it to the dafault value
    end if
    if(present(use_cft_scale))then !!If the use_cft_scale variable is present
       working_use_cft_scale=use_cft_scale !!save it
    else !!If it's not present
       working_use_cft_scale=default_use_cft_scale !!Set it to the dafault value
    end if


    if(present(k_denom))then !!IF the k_denom wariable is present
       working_k_denom=k_denom !!save it
    else !!If it's not present
       working_k_denom=default_k_denom  !!Set it to default value
    end if

    if(present(qp_pow))then !!IF the k_denom wariable is present
       working_qp_pow=qp_pow !!save it
    else !!If it's not present
       working_qp_pow=default_qp_pow  !!Set it to default value
    end if

       
    !!Initialize the working_kappa_matrix
    working_size=size
    working_Ne=Ne
    working_Ns=Ns
    !!NB: We have to remove one flux per quasiaparticle to make the filling fraction equatons work out.
    !!FIXME: This might only work for the laughlin like states, though.
    allocate(working_inv_k_matrix(working_size,working_size))
    allocate(working_kappa_matrix(working_size,working_size))
    allocate(working_k_bar_matrix(working_size,working_size))
    allocate(working_kappa_matrix_dp(working_size,working_size))
    allocate(working_k_bar_matrix_dp(working_size,working_size))
    allocate(working_k_plus(working_size,working_size))
    allocate(working_k_minus(working_size,working_size))
    allocate(working_kappa_matrix_large(working_Ne,working_Ne))
    allocate(working_k_bar_matrix_large(working_Ne,working_Ne))
    allocate(working_kappa_matrix_large_dp(working_Ne,working_Ne))
    allocate(working_k_bar_matrix_large_dp(working_Ne,working_Ne))
    allocate(working_group_size(working_size))
    allocate(working_groups_stop(working_size))
    allocate(working_groups_start(working_size))
    allocate(working_p_list(working_size))
    allocate(working_lorenz_gaussian(working_size))
    allocate(working_euclid_gaussian(working_size))
    allocate(work_tx(working_size))
    allocate(work_ty(working_size))
    allocate(working_qp_list(working_size))
    allocate(working_qp_start(working_size))
    allocate(working_qp_stop(working_size))
    
    !!Set the translations
    work_tx=t_x_in
    work_ty=t_y_in

    if(present(qp_static))then !!IF the quasiparicle wariable is present
       working_qp_static=qp_static !!save it
    else !!If it's not present
       working_qp_static=default_qp_static  !!Set it to default value
    end if


    
    if(present(qps))then !!IF the quasiparicle wariable is present
       working_qp_list=qps !!save it
    else !!If it's not present
       working_qp_list=default_qps  !!Set it to default value
    end if
    !!Compute the total number of qps
    working_tot_qps=sum(working_qp_list)
!    if(abs(working_tot_qps).gt.100)then
!       write(stderr,*) 'cwf: ERROR:'
!       write(stderr,*) 'Number of qps=',working_tot_qps,'is reidcously high!'
!       write(stderr,*) 'You have probably initiated you function erroneously'
!       write(stderr,*) 'The working_qp_list is:',working_qp_list
!       call exit(-1)
!    end if
       
    
    !!The nubmer of fluxes if there are were no qps present
    working_raw_Ns=working_Ns-flux_from_qps(working_size,working_qp_list) 
    
    working_kappa_matrix=kappa_matrix
    if(present(kappa_bar_matrix))then !!If the kappa_bar matrix is present
       working_k_bar_matrix=kappa_bar_matrix !!save it
    else !!If it's not present
       working_k_bar_matrix=default_kappa_bar_matrix !!Set it to zero as default
    end if
    
    !!The difference of the two matrices defines the filling fraction
    working_K_minus=working_kappa_matrix-working_k_bar_matrix
    working_K_plus=working_kappa_matrix+working_k_bar_matrix

    call check_k_minus_is_integer(working_K_minus,working_k_denom,working_size)

    !write(*,*) 'work_K_minus:'
    !call print_int_matrix(working_size,working_K_minus)
    !write(*,*) 'work_K_plus:'
    !call print_int_matrix(working_size,working_K_plus)
    
    !!Test that k_matrix is symmetric
    if((.not.is_matrix_symmetric(working_size,working_kappa_matrix)).or.&
         (.not.is_matrix_symmetric(working_size,working_k_bar_matrix)))then
       write(stderr,*) '---------IWF------'
       write(stderr,*) 'ERROR:'
       write(stderr,*) 'kappa_matrix is not symmetric, and can not be used'
       write(stderr,*) 'stopping....'
       call exit(-2)
    end if



    call check_positive_eigenvalues(working_k_plus)


    !!write(*,*) 'K-minus-input'
    !!call print_int_matrix(working_K_minus)
    !!write(*,*) 'K-plus-input'
    !!call print_int_matrix(working_K_plus)

    !!Compute the sizes of the different groups
    call kappa_matrix_group_sizes(working_size,working_K_minus/working_k_denom,working_p_list&
         ,working_p_group,working_q_group)
    call make_group_size(working_size,working_group_size,working_p_list&
         ,working_p_group,working_q_group,Working_Ne,working_raw_ns)

    call make_cumulative_group_size(working_group_size,working_groups_start,working_groups_stop)
    call make_cumulative_group_size(working_qp_list,working_qp_start,working_qp_stop)
    
    call construct_large_K_matrix(working_Size,working_Ne,working_kappa_matrix,&
         working_kappa_matrix_large,working_group_size)
    call construct_large_K_matrix(working_Size,working_Ne,working_k_bar_matrix,&
         working_K_bar_matrix_large,working_group_size)
    
    !!Compute the double precision version of the k-matrices
    !!small
    working_kappa_matrix_dp=real(working_kappa_matrix,dp)/working_K_denom
    working_K_bar_matrix_dp=real(working_K_bar_matrix,dp)/working_K_denom
    !!large
    working_kappa_matrix_large_dp=real(working_kappa_matrix_large,dp)/working_K_denom
    working_K_bar_matrix_large_dp=real(working_K_bar_matrix_large,dp)/working_K_denom

    !!Test if the kappa-matrix is chiral (the K-bar piece is zero)
    chiral_kappa=is_matrix_zero(working_k_bar_matrix,working_size)
    
    
    !!Compute in inverse k_matrix (used for qp:s)
    call kappa_matrix_inverse(working_size,working_K_minus/working_k_denom,working_inv_k_matrix,working_inv_denom)
    !write(*,*) 'kappa_matrix inverse:'
    !call print_int_matrix(working_inv_k_matrix)
    !write(*,*) 'with denominator:',working_inv_denom
    
    !!FIXME: These computations should really be made with the quasiparticles included
    !!As it stands we simply remove the quasiparicles contributions in the thest check.
    working_lorenz_gaussian=compute_gaussian(working_group_size,&
         working_k_minus/working_k_denom,working_size)
    working_euclid_gaussian=real(compute_gaussian(working_group_size,&
         working_k_plus,working_size),dp)/working_k_denom

    !write(*,*) 'Denom:', working_k_denom
    !write(*,*) 'Euclid:',working_euclid_gaussian
    !write(*,*) 'Lorenz:',working_lorenz_gaussian
    
    do n=1,working_size
       if(working_raw_Ns.ne.working_lorenz_gaussian(n))then
          write(stderr,*) '----iwf----'
          write(stderr,*) 'ERROR: The Lorenz gaussian piece is not equal to Ns'
          write(*,*) 'The gaussian weights are:'
          write(*,*) 'Euclid:',working_euclid_gaussian
          write(*,*) 'Lorenz:',working_lorenz_gaussian
          write(*,*) 'Number of fluxes',working_Ns
          write(*,*) 'Number of raw fluxes',working_raw_Ns
          write(stderr,*) 'quitting....'
          call exit(-2)
       end if
    end do
    
    !!Set initialized to true
    !write(*,*) 'Initalization Done'
    log_zero=0.d0*iunit
    log_zero=log(log_zero)
    initialized=.TRUE.
  end subroutine do_initialize_wave_function

  function compute_cft_scale(tau) result(log_cft_scale)
    !!!This funciton looks at the induced scalings and computes and apropriate  cft_scaling constant
    !!!NB: If the translation is set to zero, the weight is assumed to be unity. This is since a zero translation is i'll defined (and not present in the sum in eqn (D13) in PRB 89, 125303 (2014).
    complex(KIND=dpc), intent(IN) :: tau
    complex(KIND=dpc) :: log_cft_scale
    real(KIND=dp) :: lfactor,lvalue
    integer :: grp
    if(working_use_cft_scale)then
       if(mod(working_q_group,2).eq.0)then
          !!FIXME NB the relations ins egn (D13) only holds for odd q.
          write(*,*) 'INTERNAL ERROR: Even denominator not impelemted yet for cft scale!'
          call exit(-1)
       end if
       if(working_size.gt.2)then
          !!FIXME For D>2 the weight for \mathbb(D)^2 are not computed
          write(*,*) 'INTERNAL ERROR: For D>2 the weight for \mathbb(D)^2 are not known'
          call exit(-1)
       end if
       if(work_tx(1).ne.0.or.work_ty(1).ne.0)then
          !!FIXME NOt sure how to handle the first translation beeing non-zero
          write(*,*) 'INTERNAL ERROR: Not sure how to handle when translaton on first group is not zero.'
          call exit(-1)
       end if
       if(work_tx(2).ne.0.and.work_ty(2).ne.0)then
          !!FIXME Not sure how to comptue the Lambda parameter when the second translation is both x and y
          write(*,*) 'INTERNAL ERROR: Not sure how to comptue the Lambda parameter when the second translation is both x and y.'
          !! FIXME Need to know values for \Lambda * q = 1+p*p**(-1) mod 2
          !! Or was it \Lambda = p_alpha*(1-p*p**(-1)) mod 2 ?
          call exit(-1)
       end if

       !!FIXME assumes only two groups, and first is zero
       grp=2
       !!FIXME:: Add a flag to change the lvalue!!
       !!        Curently it is set ot the value used for nu=2/5 in PRB 89, 125303 (2014)
       lvalue = working_ns + 1
       lfactor= lvalue*(work_tx(grp)+work_ty(grp)+work_tx(grp)*work_ty(grp))
       log_cft_scale = working_group_size(grp)*get_lambda_mn(work_tx(grp),work_ty(grp),tau,working_Ns) + iunit*pi*lfactor
    else
       log_cft_scale=0.0
    end if
  end function compute_cft_scale

  complex(KIND=dpc) function get_lambda_mn(mindx,nindx,tau,Ns) result(lambda)
    integer, intent(IN) :: mindx,nindx,Ns
    complex(KIND=dpc), intent(IN) :: tau
    real(KIND=dp) :: epsilon
    if(mod(mindx,Ns).eq.0.and.mod(nindx,Ns).eq.0)then
       !!If the translations are 0 mod Ns then the lambda weight is unefined (so we set it to exp(0))
       lambda=czero
    else
       epsilon=1/real(Ns,KIND=dp)
       lambda=0.5d0*log(aimag(tau))+log_dedekind_eta(tau)*3 &
            -iunit*pi*nindx*(mindx+tau*nindx)*epsilon**2 &
            - logtheta1(epsilon*(mindx+tau*nindx),tau)
    end if
  end function get_lambda_mn
     
  subroutine check_positive_eigenvalues(matrix)
    integer, intent(iN) :: matrix(working_size,working_size)
    logical :: positive
    positive=.FALSE.
    if(working_size.eq.1)then
       if(matrix(1,1).gt.0)then
          positive=.TRUE.
       end if
    elseif(working_size.eq.2)then
       !!for matrix K = ( a c c b) the criterian for both ev to be positive is that 
       !!  (a+b)/2 > sqrt{c^2 + (a-b)^2/4}
       !! This means that a>0,b>0,ab>c^2
       if((matrix(1,1).gt.0).and.(matrix(2,2).gt.0).and.&
            ((matrix(1,1)*matrix(2,2)).gt.(matrix(1,2)*matrix(1,2))))then
          positive=.TRUE.
       end if
    else
       !!FIXME: Make this a real calulation
       if(cft_wfn_verbose)then
          write(stderr,*) "WARNING: Don\'t know how ho perform positive eigenvalues calculation for D>2 k-matrices"
          call print_int_matrix(matrix)
       end if
       positive=.TRUE.
       !!call exit(-1)
    end if
    
    if(.not.positive)then
       write(stderr,*) 'ERROR: K-plus matrix'
       call print_int_matrix(matrix)
       write(stderr,*) 'does not have only positive eigenvalues, so CoM calulation will not converge.'
       write(stderr,*) 'quitting...'
       call exit(-1)
    end if
    
  end subroutine check_positive_eigenvalues


  subroutine make_cumulative_group_size(group_size,group_start,group_stop)
    integer, intent(IN) :: group_size(working_size)
    integer, intent(OUT) :: group_start(working_size)
    integer, intent(OUT) :: group_stop(working_size)
    integer :: n
    !!Computing the cumulative group_size
    group_stop(1)=group_size(1)
    group_start(1)=1
    do n=2,working_size
       group_stop(n)=group_stop(n-1)&
            +group_size(n)
       group_start(n)=group_stop(n-1)+1
    end do
  end subroutine make_cumulative_group_size
  

  
  integer function flux_from_qps(D,qp_list) result(flux)
    integer, intent(IN) :: D,qp_list(D)
    integer :: n
    !!Computes the flux contribution by inserting qps.
    !!NB: In the simplest case there is the same number of qps in all groups as the flux
    !!we will assume this bellow.
    !!FIXME: add code to handle non equal qps per group
    
    !!NB: Assume all fluxes the same
    flux=qp_list(1)
    do  n=1,D
       if(flux.ne.qp_list(n))then
          write(stderr,*) 'ERROR: The number of fluxes per sector is not equal:'
          write(stderr,*) 'Qps: ',qp_list
          write(stderr,*) 'ERROR: This case has not been implemented yet.'
          write(stderr,*) 'Quitting....'
          call exit(-2)
       end if
    end do
    
  end function flux_from_qps


  function compute_gaussian(N_a,Matrix,D) result(gaussian)
    integer, intent(IN) :: D,N_a(D),Matrix(D,D)
    integer, dimension(D) :: gaussian
    integer a,b
    
    gaussian=0
    do a=1,D
       do b=1,D
          gaussian(a)=gaussian(a)+Matrix(a,b)*N_a(b)
       end do
    end do
  end function compute_gaussian
  
  subroutine load_qp_positions(Xqps_in,Yqps_in)
    real(KIND=dp), intent(IN) :: Xqps_in(working_tot_qps),Yqps_in(working_tot_qps)
    
    if(.not.initialized)then
       write(stderr,*) 'ERROR: qp-positions can not be loaded before initialization'
       call exit(-1)
    end if
    
    if(working_tot_qps.gt.0)then
       allocate(load_Xqps(working_tot_qps),load_Yqps(working_tot_qps))
    end if
    
    !!!Load the qp-positions to memory
    load_Xqps=Xqps_in
    load_Yqps=Yqps_in
  end subroutine load_qp_positions

  subroutine deinitialize_wave_function
    !!De-Initialize the wave-function specifics
    deallocate(working_inv_k_matrix)
    deallocate(working_kappa_matrix)
    deallocate(working_k_bar_matrix)
    deallocate(working_kappa_matrix_dp)
    deallocate(working_k_bar_matrix_dp)
    deallocate(working_k_plus)
    deallocate(working_k_minus)
    deallocate(working_group_size)
    deallocate(working_groups_stop)
    deallocate(working_groups_start)
    deallocate(working_p_list)
    deallocate(working_kappa_matrix_large)
    deallocate(working_k_bar_matrix_large)
    deallocate(working_kappa_matrix_large_dp)
    deallocate(working_k_bar_matrix_large_dp)
    deallocate(work_tx,work_ty)
    deallocate(working_lorenz_gaussian)
    deallocate(working_euclid_gaussian)
    deallocate(working_qp_list)
    deallocate(working_qp_start)
    deallocate(working_qp_stop)
    working_asym=.FALSE.
    chiral_kappa=.FALSE.
    initialized=.FALSE.
  end subroutine deinitialize_wave_function
    
  complex(KIND=dpc) function chiral_sym_psi(Xlist,Ylist,tau,inject_Xqps,inject_Yqps) result(psi)
    !!Creates an (anti) -- symeetreized wave function over all the coordinatse
    real(KIND=dp), intent(In) :: Xlist(:),Ylist(:)
    complex(KIND=dpc), intent(In) :: tau
    real(KIND=dp), optional, intent(In),dimension(:) :: inject_Xqps,inject_Yqps
    complex(KIND=dpc) :: cft_scale_to_add
    
    if(.not.initialized)then
       write(*,*) 'ERROR:'
       write(*,*) 'The wave-function is not initialized!'
       write(*,*) 'can not proceed, stopping....'
       call exit(-2)
    end if
    
    !!Trivial input checking
    if(size(Xlist).ne.working_Ne)then
       write(stderr,*) '----csp---'
       write(stderr,*) 'ERROR:'
       write(stderr,*) 'The dimention of input vector X, differs fromt the initialized size'
       call exit(-1)
    end if
    if(size(Ylist).ne.working_Ne)then
       write(stderr,*) '----csp---'
       write(stderr,*) 'ERROR:'
       write(stderr,*) 'The dimention of input vector Y, differs fromt the initialized size'
       call exit(-1)
       
    end if

    psi=0.d0 !!Initialize to empty value
    if(working_asym)then
       if(working_size.eq.1)then
          !!No (anti)symetrization necessary - state is allready (anti)symetric
          psi=chiral_translated_psi(Xlist,Ylist,tau,work_tx,work_ty,inject_Xqps=inject_Xqps,inject_Yqps=inject_Yqps)
       elseif(working_size.ge.2)then!!For more groups we need to antisymetrize
          psi=anti_sym_gen(Xlist,Ylist,tau,inject_Xqps=inject_Xqps,inject_Yqps=inject_Yqps)
       end if
    else !!Do not antisymetrize
       !!Directly call the translated psi
       psi=chiral_translated_psi(Xlist,Ylist,tau,work_tx,work_ty,inject_Xqps=inject_Xqps,inject_Yqps=inject_Yqps)
    end if
    cft_scale_to_add=compute_cft_scale(tau)
    !!Reduce phase factor and add cft scale if needed
    psi=mod_2pi(psi+cft_scale_to_add)
  end function chiral_sym_psi
  

  complex(KIND=C_DOUBLE_COMPLEX) function chiral_sym_psi_alt(Xlist,Ylist,tau, vdims) result(psi) bind(C)
    use ISO_C_BINDING
    !!NB: Input for C-code: Calls the proper fortran version without doing anything more
    INTEGER(kind=c_INT), intent(In) :: vdims
    real(KIND=C_DOUBLE), intent(In) :: Xlist(1:vdims),Ylist(1:vdims)
    complex(KIND=C_DOUBLE_COMPLEX), intent(In) :: tau
    psi=chiral_sym_psi(Xlist,Ylist,tau)
  end function chiral_sym_psi_alt
  
  complex(KIND=dpc) function chiral_translated_psi(Xlist,Ylist,tau,&
       trans_x,trans_y,log_theta_z1z2,permutation_order,inject_Xqps,inject_Yqps) result(psi)
    real(KIND=dp), optional, intent(In) :: inject_Xqps(working_tot_qps),inject_Yqps(working_tot_qps)
    real(KIND=dp), intent(In) :: Xlist(working_Ne),Ylist(working_Ne)
    complex(KIND=dpc), intent(In) :: tau
    integer, intent(in) :: trans_x(Working_size),trans_y(Working_size)
    complex(KIND=dpc),optional, intent(in) :: log_theta_z1z2(working_size*working_ne,working_size*working_ne) !!The the sampled theta function
    integer,optional, intent(in) :: permutation_order(working_Ne)
    real(KIND=dpc) :: Xlist_trans(working_Ne), Ylist_trans(working_Ne)
    real(KIND=dpc) :: gauge_factor
    integer :: x_trans_list(working_Ne),y_trans_list(working_Ne)
    
    call make_translation_list(Working_size,working_Ne,working_group_size&
         ,x_trans_list,trans_x)
    call make_translation_list(Working_size,working_Ne,working_group_size&
         ,y_trans_list,trans_y)
    
    !Translate the corrdinates
    Xlist_trans=Xlist+x_trans_list/real(working_Ns,dp)
    Ylist_trans=Ylist+y_trans_list/real(working_Ns,dp)
    !!Compute the accociated gague factor
    !!Two pieces, the gauge transformatoin
    !! and the noncommutativity of ortogonal tanslations
    gauge_factor=dot_product(y_trans_list,Xlist)*2*pi&
         +pi*dot_product(y_trans_list,x_trans_list)/real(working_Ns,dp)
    
    if(present(log_theta_z1z2).and.present(permutation_order))then
       psi=chiral_raw_psi(Xlist_trans,Ylist_trans,tau,&
            log_theta_z1z2,permutation_order,inject_Xqps,inject_Yqps)+iunit*gauge_factor
    else
       psi=chiral_raw_psi(Xlist_trans,Ylist_trans,tau,inject_Xqps=inject_Xqps,inject_Yqps=inject_Yqps)&
            +iunit*gauge_factor
    end if
  end function chiral_translated_psi
  
  subroutine make_translation_list(Size,working_Ne,group_size,trans_list,trans_group)
    !!COnstrung the list of translations
    integer, intent(in) :: Size,working_Ne,group_size(Size),trans_group(Size)
    integer, intent(out) :: trans_list(working_Ne)
    integer :: indx,number,id
    !write(*,*) 'Make the translation list'
    id=1
    do indx=1,Size
       do number=1,group_size(indx)
          trans_list(id)=trans_group(indx)
          id=id+1
       end do
    end do
  end subroutine make_translation_list

  
  subroutine choose_qp_positions(tau,Zlist,Xqps,Yqps,X_CoM_qps,Y_CoM_qps,Psi_qps,inject_Xqps,inject_yqps)
    complex(KIND=dpc),intent(IN) :: tau
    complex(KIND=dpc),intent(out) :: Psi_qps
    real(KIND=dp),intent(OUT) :: Xqps(working_tot_qps),Yqps(working_tot_qps)
    real(KIND=dp),intent(OUT) :: X_CoM_qps(working_size),Y_CoM_qps(working_size)
    complex(KIND=dpc),intent(IN) :: Zlist(working_Ne)
    real(KIND=dp),optional, intent(In) :: inject_Xqps(working_tot_qps),inject_Yqps(working_tot_qps)
    complex(KIND=dpc) :: Zqps(working_tot_qps)
    
    
    !!NB: There are two rotes to get the quasiprarticle positions
    !! into the wave-function.
    !! One is to supply then before evaulation by callong 'load_qps_position'
    !! This gives load_Xqps.
    !! The other is to inject them directly into chiral_raw_psi.
    !! This creates injext_Xqps.
    !! For now the injected coodrdinates have precedence of the loaded ones
    !!write(*,*) 'Adding quasi-particle parts'
    if((.not.present(inject_Xqps).and..not.allocated(load_Xqps)).or.&
         (.not.present(inject_Yqps).and..not.allocated(load_Yqps)))then
       write(*,*) 'Number of quasi-partices=',working_tot_qps,'is larger than 0'
       write(stderr,*) 'ERROR: The qp-positions has not been supplied to the wave function'
       write(stderr,*) "Use 'call load_qp_positions' to load them"
       call exit(-1)
    else !!IF loaded or injected are present, then use them
       if(present(inject_Xqps))then
          Xqps=inject_Xqps
       elseif(allocated(load_Xqps))then
          Xqps=load_Xqps
          !write(*,*) 'loaded xqps:',Xqps
       else !!not present
          write(*,*) 'ERROR: Xqps is not provided!'
          call exit(-1)
       end if
       if(present(inject_Yqps))then
          Yqps=inject_Yqps
       elseif(allocated(load_Yqps))then
          Yqps=load_Yqps
          !write(*,*) 'loaded Yqps:',Yqps
       else !!not present
          write(*,*) 'ERROR: Yqps is not provided!'
          call exit(-1)
       end if
       Zqps=Xqps+tau*Yqps
       !!Construct CoM for Qps
       call sum_XY_com(working_Size,working_tot_qps,working_qp_list,Xqps,X_CoM_qps)
       call sum_XY_com(working_Size,working_tot_qps,working_qp_list,Yqps,Y_CoM_qps)
       
       Psi_qps=compute_gaussian_qps(Yqps,tau)
       !write(*,*) 'Psi_qps gauss',Psi_qps
       Psi_qps=Psi_qps+compute_jastrow_qps(Zlist,Zqps,tau,working_ne,working_tot_qps,working_size,&
            get_qp_pow(),working_kappa_matrix_dp,working_k_bar_matrix_dp,working_qp_pow,&
            chiral_kappa,working_groups_stop,working_group_size,&
            working_qp_stop,working_qp_list,working_qp_static)
       !write(*,*) 'Psi_qps jastrow',Psi_qps
    end if
    !!write(*,*) 'Psi_qps interior',Psi_qps
  end subroutine choose_qp_positions
  
  
  complex(KIND=dpc) function chiral_raw_psi(Xlist,Ylist,tau,log_theta_z1z2,&
       permutation_order,inject_Xqps,inject_Yqps) result(psi)
    real(KIND=dp), intent(In) :: Xlist(working_Ne),Ylist(working_Ne)
    real(KIND=dp), optional, intent(In) :: inject_Xqps(working_tot_qps),inject_Yqps(working_tot_qps)
    complex(KIND=dpc), intent(In) :: tau
    complex(KIND=dpc),optional, intent(in) :: log_theta_z1z2(working_size*working_ne,working_size*working_ne) !!The the sampled theta function
    integer,optional, intent(in) :: permutation_order(working_Ne)
    real(KIND=dp) :: Xqps(working_tot_qps),Yqps(working_tot_qps)
    complex(KIND=dpc) :: Psi_gauss,Psi_jastrow,Psi_Com,Psi_qps
    complex(KIND=dpc) :: Zlist(working_Ne)
    real(KIND=dp) :: X_CoM(working_Size),Y_CoM(working_Size)
    real(KIND=dp) :: X_CoM_qps(working_Size),Y_CoM_qps(working_Size)
    real(KIND=dp) :: X_CoM_qp_eff(working_Size),Y_CoM_qp_eff(working_Size)
    
    !!write(*,*) 'Entering computation of wave-function'
    
    !!The chiral wave-function consists of several parts...
    !!Rember that all things are in log-scale
    
    !!Exept for the exponential all the coordinate dependence is holomorphic
    !!NB:If non-chiral components are present, that will imply non-holomorphic coordinates
    Zlist=Xlist+tau*Ylist
    
    !!Test if/how quasiparticles should be handled
    if(working_tot_qps.gt.0)then
       !write(*,*) 'injected:',present(inject_Xqps),present(inject_Xqps)
       call choose_qp_positions(tau,Zlist,Xqps,Yqps,X_CoM_qps,Y_CoM_qps,Psi_qps,inject_Xqps,inject_yqps)
    else
       !!If there are no quasiparicles thir center of mass is also zero
       X_Com_qps=0.d0
       Y_CoM_qps=0.d0
       !!The total contributions from quasiparticles will likewise be zero
       Psi_qps=0.d0
    end if
    
    !write(*,*) 'Psi_qps:',Psi_qps

    !!Center of mass pieces
    Psi_gauss=compute_gaussian_weight(Ylist,tau)
    
    if(present(log_theta_z1z2).and.present(permutation_order))then
       Psi_jastrow=compute_sampled_jastrow_factor(log_theta_z1z2,permutation_order,&
            working_size,working_ne,chiral_kappa,working_kappa_matrix_dp,&
            working_K_bar_matrix_dp,working_groups_stop,&
       working_group_size)
    else
       Psi_jastrow=compute_raw_jastrow_factor(Zlist,tau,working_ne,chiral_kappa,&
            working_kappa_matrix_large_dp,working_K_bar_matrix_large_dp)
    end if
       
    call sum_XY_com(working_Size,Working_ne,working_group_size,Xlist,X_com)
    call sum_XY_com(working_Size,Working_ne,working_group_size,Ylist,Y_com)

    !write(*,*) '................................................'
    !!FIXME: The procedure here of projecting X_com_qps on X_com 
    !!will only work for Laughlin-like states
    call compute_com_qp_eff(X_com_qp_eff,Y_com_qp_eff,tau,X_com_qps,Y_com_qps)
    
    Psi_CoM=Center_of_Mass_function(working_Size,&
         working_kappa_matrix,working_K_bar_matrix,&
         X_com+X_com_qp_eff,Y_com+Y_com_qp_eff,&
         tau,k_denom=working_k_denom)
    !write(*,*) 'Psi-com:',Psi_CoM+log_dedekind_eta(tau)
    
    !write(*,*) 'CoM part:     ',Psi_CoM
    
    !!Add the contributions together
    psi=Psi_gauss+psi_jastrow+psi_com+Psi_qps
    if(.not.chiral_kappa)then
       !write(*,*) '------------------'
       !write(*,*) 'Total :       ',psi
       !write(*,*) '------------------'
    end if
    
    psi=mod_2pi(psi)
    !!write(*,*) 'Exiting computation of wave-function'

  end function chiral_raw_psi
  
  subroutine compute_com_qp_eff(X_com_qp_eff,Y_com_qp_eff,tau,X_com_qps,Y_com_qps)
    real(kind=dp), intent(out) :: X_com_qps(working_size),Y_com_qps(working_size)
    real(kind=dp), intent(out) :: X_com_qp_eff(working_size),Y_com_qp_eff(working_size)
    complex(kind=dpc), intent(in) :: tau
    if(working_tot_qps.gt.0)then
       if(chiral_kappa)then
          !!FIXME: Implement chiral kappa-matrix factors
          X_com_qp_eff=matmul(working_inv_k_matrix,X_com_qps)/working_inv_denom
          Y_com_qp_eff=matmul(working_inv_k_matrix,Y_com_qps)/working_inv_denom
       else
          if(working_size.gt.1)then
             write(stderr,*) 'ERROR: Com effective qps not implemented for D>1 non-chiral kappa-matrix'
             call exit(-1)
          else
             X_com_qp_eff=(X_com_qps(1)+Y_com_qps(1)*2*real(tau,dp)*&
                  (get_k_bar(1,1)-get_k_minus(1,1)*get_qp_pow())/get_k_plus(1,1))/get_k_minus(1,1)
             Y_com_qp_eff=Y_com_qps(1)*(1+2*get_qp_pow())/get_k_plus(1,1)
          end if
       end if
    else
       X_com_qp_eff=0.d0
       Y_com_qp_eff=0.d0
    end if
  end subroutine compute_com_qp_eff
  
  complex(Kind=dpc) function compute_gaussian_weight(Ylist,tau) result(Psi_gauss)
    real(kind=dp), intent(in) :: Ylist(working_ne)
    complex(kind=dpc), intent(in) :: tau
    integer :: Norm_power,n,alpha
    !!Center of mass pieces
    if(chiral_kappa)then !!CHiral k-matrix means LLL gaussian
       Psi_gauss=iunit*pi*working_Ns*tau*sum(Ylist**2)
    else !!Otherwise the Gaussian depends on the kappa-matrix
       if(working_size.eq.1)then
          !write(*,*) 'K,kappa_bar_matrix,Ne:',working_kappa_matrix(1,1), working_K_bar_matrix(1,1),Working_Ne
          Psi_gauss=iunit*pi*sum(Ylist**2)*(&
               tau*(working_Ne*get_K_mat(1,1)+working_tot_qps*(1+get_qp_pow()))&
               -dconjg(tau)*(working_Ne*get_K_bar(1,1)+working_tot_qps*get_qp_pow()))
!          Psi_gauss=iunit*pi*sum(Ylist**2)&
!               *(working_Ne+(working_tot_qps*1.d0)/get_k_minus(1,1))&
!               *(get_K_mat(1,1)*tau-get_K_bar(1,1)*dconjg(tau))
       else !!for non chiral sizes larger than 1
          
          !!The gaussian factor is computed as:
          !! exp(i*pi*\tilde{tau}*\tilde{Q}*sum_a \tilde{q}_a y_a^2
          !! exp(i*pi*sum_a(tau1*lorenz_gauss_a+i*tau2*euclid_gauss_a ) y_a^2
          
          
          !!Enumerate the different groups
          !write(stderr,*) '------cqw-----'
          Psi_gauss=0
          do alpha=1,working_size
             !write(*,*) 'Group:',alpha
             !write(*,*) 'Lorenz:',working_lorenz_gaussian(alpha)
             !write(*,*) 'Euclid:',working_euclid_gaussian(alpha)
             do n=working_groups_start(alpha),working_groups_stop(alpha)
                !write(*,*) 'n=',n,'y=',Ylist(n)
                Psi_gauss=Psi_gauss+iunit*pi&
                     *(real(tau,dp)*working_lorenz_gaussian(alpha)&
                     +iunit*aimag(tau)*working_euclid_gaussian(alpha))&
                     *Ylist(n)**2
                !write(*,*) 'Psi_gauss:',psi_gauss
             end do
          end do
       end if
    end if
    !!Norm power is the sum of all the K_ii
    Norm_power=get_norm_power()
    !!Normalize with the tau_2 and eta_dependent prefactor
    !!write(*,*) 'Norm_power:',Norm_power
    Psi_gauss=Psi_gauss+Norm_power*log_dedekind_eta(tau)&
         +Norm_power*0.25d0*log(aimag(tau))
    !write(*,*) 'Gaussian part:',psi_gauss    
  end function compute_gaussian_weight
  
  integer function get_norm_power() result(res)
    integer alpha !!Add upp all the K_ii components
    res=0!!Initiate to zero
    do alpha=1,working_size
       !!FIXME for non-chiral matrices this is not the norm actually!!
       res=res+working_group_size(alpha)*get_k_minus(alpha,alpha)
    end do
  end function get_norm_power


  complex(Kind=dpc) function compute_gaussian_qps(Yqps,tau) result(Psi_gauss)
    real(kind=dpc), intent(in) :: Yqps(working_tot_qps)
    complex(kind=dpc), intent(in) :: tau
    !!Center of mass pieces
    if(working_qp_static)then
       !write(*,*) 'cwf: Working out gaussian:'
       if(working_size.eq.1)then
          !write(*,*) 'K,kappa_bar_matrix,Ne:',get_K_mat(1,1), get_K_bar(1,1),Working_Ne
          !write(*,*) 'qp_pow_components:',working_qp_pow
          !write(*,*) 'qp_power:',get_qp_pow()
          !write(*,*) 'working_tot_qps',working_tot_qps
          !write(*,*) 'tau:',tau
          if(working_qp_pow(1).eq.0)then
             Psi_gauss=iunit*pi*sum(Yqps**2)*&
                  tau*(1+get_qp_pow())*(working_Ne+working_tot_qps*(1+get_qp_pow())/get_k_mat(1,1))
          else
             Psi_gauss=iunit*pi*sum(Yqps**2)*(&
                  tau*(1+get_qp_pow())*(working_Ne+working_tot_qps*(1+get_qp_pow())/get_k_mat(1,1))&
                  -dconjg(tau)*get_qp_pow()*(working_Ne+working_tot_qps*get_qp_pow()/get_k_bar(1,1)))
          end if
          !write(*,*) 'psi_gauss;',Psi_gauss
       else 
          if(chiral_kappa)then
             write(stderr,*) 'ERROR: Gaussian function not implemented for (modified) qps'
             write(stderr,*) '       With chiral k-matrix is larger than 1'
             call exit(-1)
          else
             write(stderr,*) 'ERROR: Gaussian function not implemented for qps'
             write(stderr,*) '       With non-chiral k-matrix is larger than 1'
             call exit(-1)
          end if
       end if
    else
       Psi_gauss=0.d0 !!Set trivially zero if not required
    end if
    !!FIXME: No normailzation on the qp gaussian.
  end function compute_gaussian_qps
  
  subroutine make_group_size(Size,group_size,p_list,p,q,Ne,Ns)
    integer, intent(IN) :: p,q,Ne,Ns,Size,p_list(Size)
    integer, intent(OUT) :: group_size(Size)
    integer :: cells,gcd1
    !!Make sure the filling fraction are the same for Ne,Ns as for p,q
    !write(*,*) 'Checking group size'
    !write(*,*) 'Ne,Ns,p,q:',Ne,Ns,p,q
    if((Ne*q).eq.(Ns*p))then
       gcd1=gcd(Ne,p)
       cells=Ne/gcd1
       group_size=p_list*cells
    else
       write(*,*) 'Input Ne,Ns does not match the calulated p,q for this kappa-matrix'
       write(*,*) 'Ne,Ns,p,q:',Ne,Ns,p,q
       write(*,*) 'Kminus:'
       call print_int_matrix(working_K_minus)
       write(*,*) 'Stopping.....'
       call exit(-2)
    end if
  end subroutine make_group_size
  
  
  
  subroutine construct_large_K_matrix(Size,Ne,K_matrix&
       ,K_matrix_large,group_size)
!!!Computes the large kappa-matrix for the set of particles
    integer, intent(IN) :: Size,Ne,group_size(Size),K_matrix(Size,Size)
    integer, intent(OUT) :: K_matrix_large(Ne,Ne)
    integer :: gr_indx,group_list(Ne),n,el_indx,el_indx2
    
    el_indx=1
    do gr_indx=1,Size
       do n=1,group_size(gr_indx)
          group_list(el_indx)=gr_indx
          el_indx=el_indx+1
       end do
    end do
    !!write(*,*) 'List of group affiliation'
    !!write(*,*) group_list
    do el_indx=1,Ne
       do el_indx2=1,Ne
          K_matrix_large(el_indx,el_indx2)=&
               K_matrix(group_list(el_indx),group_list(el_indx2))
       end do
    end do
    !write(*,*) 'Large kappa-matrix'
    !call print_int_matrix(Ne,K_matrix_large)
  end subroutine construct_large_K_matrix
  
  subroutine sum_XY_com(Size,Ne,group_size,XY_coord,XY_com)
    real(KIND=dp), intent(In) :: XY_coord(Ne)
    integer, intent(in) :: Ne,Size,group_size(Size)
    real(KIND=dp), intent(out) :: XY_com(Size)
    integer :: indx1,indx2,com_size(Size)
    
    com_size=0
    com_size(1)=group_size(1)
    do indx1=2,Size
       com_size(indx1)=com_size(indx1-1)+group_size(indx1)
    end do
    !write(*,*) 'sum group size',com_size
    
    XY_com=0d0
    do indx1=1,Size    
       if(indx1.eq.1)then
          do indx2=1,com_size(1)
             XY_com(1)=XY_com(1)+XY_coord(indx2)
          end do
       else
          do indx2=(com_size(indx1-1)+1),com_size(indx1)
             XY_com(indx1)=XY_com(indx1)+XY_coord(indx2)
          end do
       end if
    end do
    
    !write(*,*) 'XY_CoM:'
    !write(*,*) XY_com
  end subroutine sum_XY_com
  
  complex(KIND=dpc) function mod_2pi(logz) result(mod_logz)
    !! This function returns the argument of logz to be withing the range 0 <= Im(logz) < 2*pi.
    complex(KIND=dpc), INTENT(IN) :: logz
    mod_logz = real(logz,dp)+iunit*modulo(aimag(logz),2*pi)
  end function mod_2pi
  
  complex(KIND=dpc) function anti_sym_gen(Xlist,Ylist,tau,inject_Xqps,inject_Yqps) result(psi)
    real(KIND=dp), intent(In) :: Xlist(Working_Ne),Ylist(Working_Ne)
    complex(KIND=dpc), intent(In) :: tau
    real(KIND=dp), optional, intent(In) :: inject_Xqps(working_tot_qps),inject_Yqps(working_tot_qps)
    complex(KIND=dpc) :: log_theta_z1z2(working_size*working_ne,working_size*working_ne)
    
    !!Sample the theta functions -- so antisymmetrixation is faster
    call sample_theta_functions(Xlist,Ylist,tau,work_tx,work_ty,log_theta_z1z2,working_Ne,working_size,working_ns)
    !!FIXME(23/2-16): It would seem the speedup is not that large?
    !!                Why is this??? Is it because computation of the wave function is still much worse??? Presumably.
    !!FIXME(23/2-16): For qps: there might be a speed-up in the jastrow factor to be had.
    !!                Run a profiler to see if there is something to be gained here..
    psi = do_anti_sym_gen(Xlist,Ylist,tau,log_theta_z1z2,inject_Xqps=inject_Xqps,inject_Yqps=inject_Yqps)
  end function anti_sym_gen
  
  complex(KIND=dpc) function do_anti_sym_gen(Xlist,Ylist,tau,&
       log_theta_z1z2,inject_Xqps,inject_Yqps) result(psi)
    real(KIND=dp), intent(In) :: Xlist(Working_Ne),Ylist(Working_Ne)
    complex(KIND=dpc), intent(In) :: tau
    integer :: Ordering(working_Ne),ord_par
    complex(KIND=dpc),intent(in) :: log_theta_z1z2(working_size*working_ne,working_size*working_ne)
    real(KIND=dp), optional, intent(In) :: inject_Xqps(working_tot_qps),inject_Yqps(working_tot_qps)
    real(KIND=dp) :: Xlist_perm(Working_Ne),Ylist_perm(Working_Ne)
    complex(dpc) :: psi_local
    type(permutation) :: perm
    real(dp) :: sym_num_dp
    integer(8) :: long_sym_num,long_counter

    !!Compute number of states, so for loops knows about it.
    sym_num_dp=lnmultinom(working_group_size)
    long_sym_num=nint(exp(sym_num_dp),kind=8)
    
    psi=0.d0
    psi=log(psi)
    
    call perm_initialize(perm)
    call perm_create(perm,working_group_size)
    
    !$OMP PARALLEL DO PRIVATE(Ordering,ord_par,Xlist_perm,Ylist_perm,psi_local)
    do long_counter=1,long_sym_num
       !$OMP Critical !!Read order,parity and update to next permutation
       IF (.not.perm_ok(perm)) then  !!Stop if not ok
          write(stderr,*) 'Overflow and uncontrolled integer in permutation'
       end IF
       Ordering = perm_get_order(perm)
       ord_par = perm_get_parity(perm)
       call perm_next(perm)
       !$OMP END Critical
       !!Reassign the coodinates
       Xlist_perm=Xlist(Ordering)
       Ylist_perm=Ylist(Ordering)
       !!Compute the contribution
       psi_local=chiral_translated_psi(Xlist_perm,Ylist_perm,tau&
            ,work_tx,work_ty,log_theta_z1z2,ordering,inject_Xqps=inject_Xqps,inject_Yqps=inject_Yqps)
       !!Add (anti)-symmetrization factor
       psi_local=psi_local + iunit*pi*ord_par*get_k_minus(1,1)
       !$OMP Critical !!Update the psi-value
       psi=add_exponentially(psi,psi_local)
       !$OMP END Critical
    end do
    !$OMP end PARALLEL do
    call perm_destroy(perm)
    
  end function do_anti_sym_gen
  
  subroutine test_if_same_integer(same,var,working_var)
    integer, intent(IN) :: var,working_var
    logical, intent(INOUT) :: same
    if(var.ne.working_var)then
       same = .False.
    end if
  end subroutine test_if_same_integer
  
  subroutine test_if_same_logical(same,var,working_var)
    logical, intent(IN) :: var,working_var
    logical, intent(INOUT) :: same
    if(var.neqv.working_var)then
       same = .False.
    end if
  end subroutine test_if_same_logical

  !! Fech function for the k_matrix values
  integer function get_k_minus(n,m) result(k_min)
    integer, intent(in) :: m,n
    k_min=working_k_minus(n,m)/working_k_denom
  end function get_k_minus
  
  real(kind=dp) function get_k_plus(n,m) result(k_plus)
    integer, intent(in) :: m,n
    k_plus=real(working_k_plus(n,m),dp)/working_k_denom
  end function get_k_plus
  
  real(kind=dp) function get_k_mat(n,m) result(k_mat)
    integer, intent(in) :: m,n
    k_mat=real(working_kappa_matrix(n,m),dp)/working_k_denom
  end function get_k_mat
  
  real(kind=dp) function get_k_bar(n,m) result(k_bar)
    integer, intent(in) :: m,n
    k_bar=real(working_k_bar_matrix(n,m),dp)/working_k_denom
  end function get_k_bar

  real(kind=dp) function get_qp_pow() result(qp_pow)
    qp_pow=real(working_qp_pow(1),dp)/working_qp_pow(2)
  end function get_qp_pow



end module chiral_wave_function
