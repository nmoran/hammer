program test_chiral_wave_function
  
  USE typedef     ! types and definitions
  use test_utilities
  use k_matrix
  use chiral_wave_function
  use center_of_mass
  use combinations
  use misc_random
  use permutations

  
  !!Variables
  IMPLICIT NONE  
  
    !!!Test starts here!!!
    write(*,*) '         Test the Chiral wave-functions'
    !! Testing the many-body wave-function
    call test_non_anti_symmetrization
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_non_abelian_com_choise
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_pbc_non_anti_sym(.FALSE.)
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_pbc_non_anti_sym(.TRUE.)
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_many_body_T1_eigs(.FALSE.)
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_many_body_T1_eigs(.TRUE.)
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_periodic_translations
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_many_body_translations
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_naive_anti_symmetrization
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_anti_symmetrization
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'


contains  
  
  !!----------------------------------------------
  !!     Thest the manu body wave-functions
  !!---------------------------------------------
  subroutine test_pbc_non_anti_sym(TEST_PBCS)
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,kappa_matrix_large
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist
    integer :: q,cells,D,p,indx,Ne,Ns,raw_k_sector,n
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,extra_phase,tau_x,tau_y
    logical, intent(IN) :: TEST_PBCS
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    if(TEST_PBCS)then
       write(*,*) 'Test that correct periodic boundary conditions can be set and are satisfied'
       !!This is really a test of eqn (A34) in PRB 89, 125303 (2014)
       !!with h=t=0
       !! but for the non-chiral case
    else
       write(*,*) 'Test that h=t=0 periodic boundary conditions are satisfied'
       !!This is really a test of eqn (A34) in PRB 89, 125303 (2014)
       !!with h,t,chosen to obtain periodic bcs
       !! but for the non-chiral case
    end if
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    do D=1,3
       ALLOCATE(kappa_matrix(D,D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D))
       !!Initialize the n-list 
       maxlist=2
       maxlist(1)=3
       minlist=0
       minlist(1)=1
       nlist=minlist
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          do cells=1,3 !!Loop over different number of fundamental cells
             Ns=q*cells
             Ne=p*cells
        
             !!Initialize the wave-function
             call initialize_wave_function(D,kappa_matrix,Ne,Ns,(/(0, n=1,D)/),(/(0, n=1,D)/))
             
             !!Initialize the cooridinates (use unit length in X).
             !!The length scale Lx is thus implicit.
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             allocate(kappa_matrix_large(Ne,Ne))
             call RANDOM_NUMBER(tau_x)
             call RANDOM_NUMBER(tau_y)
             !Put tau_x in range -1/2 < tau_x < 1/2
             tau_x=tau_x-.5
             !Put tau_y in range 1/2 < tau_x < 1+1/2
             tau_y=tau_y+.5
             tau=tau_x+iunit*tau_y
             call RANDOM_NUMBER(Xcoord)
             call RANDOM_NUMBER(Ycoord)
             !!Make the random number in the range -1/2 < x < 1/2
             Xcoord=Xcoord-.5d0
             Ycoord=Ycoord-.5d0
                          

             if(TEST_PBCS)then
                !!Set the boudnary conditions to periodic
                raw_k_sector=compute_raw_K_sector(D,Ne,Ns,kappa_matrix,plist,q)
                !!write(*,*) 'TEST: raw k_sector is',raw_k_sector
                call set_CM_BC(Ns,raw_k_sector,D,kappa_matrix)
             else
                call set_trivial_CM_BC(D,kappa_matrix)
                call construct_large_K_matrix(D,Ne,kappa_matrix,&
                     kappa_matrix_large,cells*plist)
                !write(*,*) 'Large kappa-matrix'
                !call print_int_matrix(kappa_matrix_large)
             end if
             !!Compute first wave function
             psi=chiral_raw_psi(Xcoord,Ycoord,tau)
             do indx=1,p
                !!Change the coordinates one by one in x-direction
                Xcoord2=Xcoord
                Ycoord2=Ycoord
                Xcoord2(indx)=Xcoord2(indx)+1.d0
                psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau)
                !!Change the coordinates one by one in y-direction
                Xcoord2=Xcoord
                Ycoord2=Ycoord
                Ycoord2(indx)=Ycoord2(indx)+1.d0
                !!Compute the gaguge phase
                gauge_phase=Xcoord(indx)*2*pi*Ns
                psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau)&
                     +iunit*gauge_phase !!Add the gauge phase
                if(.not.TEST_PBCS)then
                   !!Compute the extra phase (the expected phase)
                   extra_phase=pi*(Ns-kappa_matrix_large(indx,indx))
                   psi_2x=psi_2x+iunit*extra_phase
                   psi_2y=psi_2y+iunit*extra_phase
                end if
                !!Reduce to smallest mod_2pi
                psi_2x=mod_2pi(psi_2x)
                psi_2y=mod_2pi(psi_2y)
                
                !!Test the periodic boundary conditions             
                if(test_diff_exp_cmplx(psi,psi_2x,1.d-08)&
                     .or.&
                     test_diff_exp_cmplx(psi,psi_2y,1.d-08))then
                   write(*,'(A,A,I3,A)') '.-.-.-.-.-.-.'&
                        ,'Change coordinates for particle no.',indx&
                        ,'.-.-.-.-.-.-'
                   write(*,*) 'Not-periodic'
                   write(*,*) 'For kappa-matrix'
                   call print_int_matrix(kappa_matrix)
                   write(*,*) 'tau:',tau
                   write(*,*) 'cells:',cells
                   call print_the_psis(psi,psi_2x,psi_2y)
                   write(*,*) 'Coordinates Z:'
                   write(*,*) Xcoord+tau*Ycoord
                   call exit(-2)
                end if
             end do
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
             deallocate(kappa_matrix_large)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_nlist(D,nlist,maxlist,minlist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_pbc_non_anti_sym
  
  
  subroutine test_many_body_T1_eigs(TEST_PBCS)
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist,inclist
    integer :: q,cells,D,p,Ne,Ns,eigval,raw_k_sector,n
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,raw_shift,tau_x,tau_y
    logical, intent(IN) :: TEST_PBCS
    
    !!This function serves a double purpouses. We both test with boundary 
    !!conditions, and without boundary conditions
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    if(TEST_PBCS)then
       write(*,*) 'Test that correct many body T1 eigs can be set and are satisfied'
       !!This is really a test of eqn (A35) in PRB 89, 125303 (2014)
       !!with h=t=0
    else
       write(*,*) 'Test that h=t=0 many body T1 eigs are satisfied'
       !!This is really a test of eqn (A35) in PRB 89, 125303 (2014)
       !!with h,t, set to obatin periodic bcs.
    end if
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    do D=1,3
       ALLOCATE(kappa_matrix(D,D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D),inclist(D))
       !!Initialize the n-list 
       maxlist=2
       maxlist(1)=3
       minlist=0
       minlist(1)=1
       inclist=1!!We test center of mass translation also for mixed states!!
       inclist(1)=1
       nlist=minlist
       do 
          !write(*,*) 'Nlist'
          !write(*,*) Nlist
          
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          !write(*,*) 'kappa-matrix'
          !call print_int_matrix(kappa_matrix)
          
          do cells=1,2 !!Loop over different number of fundamental cells
             Ns=q*cells
             Ne=p*cells
             call initialize_wave_function(D,kappa_matrix,Ne,Ns,(/(0, n=1,D)/),(/(0, n=1,D)/))
             !!Initialize the cooridinates (use unit length in X).
             !!The length scale Lx is thus implicit.
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             
             do eigval=0,q !!Loop over the possible eigenvalues      
                call RANDOM_NUMBER(tau_x)
                call RANDOM_NUMBER(tau_y)
                !Put tau_x in range -1/2 < tau_x < 1/2
                tau_x=tau_x-.5
                !Put tau_y in range 1/2 < tau_x < 1+1/2
                tau_y=tau_y+.5
                tau=tau_x+iunit*tau_y
                call RANDOM_NUMBER(Xcoord)
                call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
                Xcoord=Xcoord-.5d0
                Ycoord=Ycoord-.5d0
                
                if(TEST_PBCS)then
                   !!Set the boundary conditions to periodic
                   raw_k_sector=compute_raw_K_sector(D,Ne,Ns,kappa_matrix,plist,q)
                   !write(*,*) 'TEST: raw k_sector is',raw_k_sector
                   call set_CM_BC(Ns,raw_k_sector+Ne*eigval,D,kappa_matrix)
                   !write(*,*) 'The raw1 shift is',raw_shift
                   raw_shift=real(raw_k_sector,dp)/Ns
                   !write(*,*) 'The raw2 shift((2*pi) is',raw_shift
                   raw_shift=raw_shift+eigval*real(p,dp)/q
                   !write(*,*) 'The raw3 shift((2*pi) is',raw_shift
                   raw_shift=2*pi*raw_shift
                else
                   call set_trivial_CM_BC(D,kappa_matrix)
                   raw_shift=0.d0
                   !write(*,'(A,A,A)') '...............'&
                   !     ,'we now test that the boudnary conditison work'&
                   !     ,'....................'
                end if
                
                !!Compute first wave function
                psi=chiral_raw_psi(Xcoord,Ycoord,tau)
                
                !!Change all coordinates one step in the x-direction
                Xcoord2=Xcoord+1.d0/Ns
                Ycoord2=Ycoord
                psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau)
                
                !!Change all coordinates q steps in the y-direction
                !!This tests the q-fold degeracy
                Xcoord2=Xcoord
                Ycoord2=Ycoord+real(q,dp)/Ns
                
                !!Compute the gaguge phase
                gauge_phase=sum(Xcoord2)*2*pi*q
                psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau)&
                     +iunit*gauge_phase !!Add the gauge phase
                !!Reduce to smallest mod_2pi
                psi_2x=mod_2pi(psi_2x-iunit*raw_shift)
                psi_2y=mod_2pi(psi_2y-iunit*q*raw_shift)
                
                !!Test the periodic boundary conditions             
                if(test_diff_exp_cmplx(psi,psi_2x,1.d-7)&
                     .or.&
                     test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                   write(*,'(A,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                        ,'Change global coorinates for',Ne&
                        ,' particles and eigenvalue',eigval&
                        ,'.-.-.-.-.-.-'
                   write(*,*) 'Not-periodic'
                   write(*,*) 'For kappa-matrix'
                   call print_int_matrix(kappa_matrix)
                   write(*,*) 'tau:',tau
                   write(*,*) 'cells:',cells
                   call print_the_psis(psi,psi_2x,psi_2y)
                   write(*,*) 'Coordinates Z:'
                   write(*,*) Xcoord+tau*Ycoord
                   call exit(-2)
                end if
             end do
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_parity_nlist(D,nlist,maxlist,minlist,inclist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist,inclist)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_many_body_T1_eigs
  
  
  subroutine test_many_body_Translations
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:) :: shiftlist_x,shiftlist_y
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist,inclist
    integer :: q,cells,D,p,Ne,Ns,shift_x,shift_y,group_no
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,raw_shift,tau_x,tau_y
    
    !!This function teststs that a globlal (groupwise) translation operator can be added to the hamiltonian.
    !!THis is important as is destinguiges the different groups. 
    !! This will change the boundary condistions, so it needs to be chacked
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that groupwise translation can be added'
    
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    do D=1,3
       !write(*,*) 'D=',D
       ALLOCATE(kappa_matrix(D,D))
       allocate(shiftlist_x(D),shiftlist_y(D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D),inclist(D))
       !!Initialize the n-list (only look at small subset)
       maxlist=1
       maxlist(1)=2
       minlist=0
       minlist(1)=1
       inclist=1!!We test center of mass translation also for mixed states!!
       inclist(1)=1
       nlist=minlist
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          do cells=1,2 !!Loop over different number of fundamental cells
             Ns=q*cells
             Ne=p*cells
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             do shift_x=0,q !!Loop over the possible eigenvalues      
                do shift_y=0,q !!Loop over the possible eigenvalues      
                   do group_no=1,D !!Loop over the possible eigenvalues      
                      !!Set the shift_size
                      shiftlist_x=0
                      shiftlist_y=0
                      shiftlist_x(group_no)=shift_x
                      shiftlist_y(group_no)=shift_y

                      !!Initialize the cooridinates (use unit length in X).
                      !!The length scale Lx is thus implicit.
                      call RANDOM_NUMBER(tau_x)
                      call RANDOM_NUMBER(tau_y)
                      !Put tau_x in range -1/2 < tau_x < 1/2
                      tau_x=tau_x-.5
                      !Put tau_y in range 1/2 < tau_x < 1+1/2
                      tau_y=tau_y+.5
                      tau=tau_x+iunit*tau_y
                      call RANDOM_NUMBER(Xcoord)
                      call RANDOM_NUMBER(Ycoord)
                      !!Make the random number in the range -1/2 < x < 1/2
                      Xcoord=Xcoord-.5d0
                      Ycoord=Ycoord-.5d0
                      
                      !!Set the bc to h=t=0
                      call set_trivial_CM_BC(D,kappa_matrix)
                      call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y)
                      !!The raw_shift will be used to compare egenvalues before
                      !!and after shifts
                      !!The expected shift on group alpha is 2*pi*p_alpha /q
                      raw_shift=2*pi*shift_y*real(plist(group_no),dp)/q
                      
                      !!Compute first wave function
                      psi=chiral_translated_psi(Xcoord,Ycoord,tau&
                           ,shiftlist_x,shiftlist_y)
                      
                      !!Change all coordinates one step in the x-direction
                      Xcoord2=Xcoord+1.d0/Ns
                      Ycoord2=Ycoord
                      psi_2x=chiral_translated_psi(Xcoord2,Ycoord2,tau&
                           ,shiftlist_x,shiftlist_y)
                      
                      !!Change all coordinates q steps in the y-direction
                      !!This tests the q-fold degeracy
                      Xcoord2=Xcoord
                      Ycoord2=Ycoord+real(q,dp)/Ns
                      
                      !!Compute the gaguge phase
                      gauge_phase=sum(Xcoord2)*2*pi*q
                      psi_2y=chiral_translated_psi(Xcoord2,Ycoord2,tau&
                           ,shiftlist_x,shiftlist_y)&
                           +iunit*gauge_phase !!Add the gauge phase
                      !!Reduce to smallest mod_2pi
                      psi_2x=mod_2pi(psi_2x-iunit*raw_shift)
                      psi_2y=mod_2pi(psi_2y-iunit*q*raw_shift)
                      
                      !!Test the periodic boundary conditions             
                      if(test_diff_exp_cmplx(psi,psi_2x,1.d-8)&
                           .or.&
                           test_diff_exp_cmplx(psi,psi_2y,1.d-8))then
                         write(*,'(A,A,I3,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                              ,'Change global coorinates for',Ne&
                              ,' particles and shifts (',shift_x&
                              ,',',shift_y&
                              ,') .-.-.-.-.-.-'
                         write(*,*) 'Not-periodic'
                         write(*,*) 'For kappa-matrix'
                         call print_int_matrix(kappa_matrix)
                         write(*,*) 'tau:',tau
                         write(*,*) 'cells:',cells
                         call print_the_psis(psi,psi_2x,psi_2y)
                         write(*,*) 'Coordinates Z:'
                         write(*,*) Xcoord+tau*Ycoord
                         call exit(-2)
                      end if
                   end do
                end do
             end do
             !!Delaocate the coordinates
                DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
             end do !!Next cell size
             !!Compute next combinations
          if(.not.next_parity_nlist(D,nlist,maxlist,minlist,inclist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist,inclist)
       deallocate(shiftlist_x,shiftlist_y)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_many_body_Translations


  subroutine test_periodic_Translations
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:) :: shiftlist_x,shiftlist_y
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist,inclist
    integer :: q,cells,D,p,Ne,Ns,group_no
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y,psi_2xy
    real(kind=dp) :: tau_x,tau_y
    real(kind=dp) :: raw_shift_x,raw_shift_y,raw_shift_xy
    
    !!This function teststs that a globlal (groupwise) translation operator can be added to the hamiltonian.
    !!THis is important as is destinguiges the different groups. 
    !! This will change the boundary condistions, so it needs to be chacked
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that periodic groupwise translations has pbc'
    
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    do D=1,3
       !write(*,*) 'D=',D
       ALLOCATE(kappa_matrix(D,D))
       allocate(shiftlist_x(D),shiftlist_y(D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D),inclist(D))
       !!Initialize the n-list (only look at small subset)
       maxlist=1
       maxlist(1)=2
       minlist=0
       minlist(1)=1
       inclist=1!!We test center of mass translation also for mixed states!!
       inclist(1)=1
       nlist=minlist
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          do cells=1,3 !!Loop over different number of fundamental cells
             Ns=q*cells
             Ne=p*cells
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             !!Here there are three main considerations T_Ns,0,T_0,Ns,and T_0,0             
             do group_no=1,D !!Loop over the possible eigenvalues      
                !!Initialize the cooridinates (use unit length in X).
                !!The length scale Lx is thus implicit.
                call RANDOM_NUMBER(tau_x)
                call RANDOM_NUMBER(tau_y)
                !Put tau_x in range -1/2 < tau_x < 1/2
                tau_x=tau_x-.5
                !Put tau_y in range 1/2 < tau_x < 1+1/2
                tau_y=tau_y+.5
                tau=tau_x+iunit*tau_y
                call RANDOM_NUMBER(Xcoord)
                call RANDOM_NUMBER(Ycoord)
                !!Make the random number in the range -1/2 < x < 1/2
                Xcoord=Xcoord-.5d0
                Ycoord=Ycoord-.5d0
                
                !!Set the bc to h=t=0
                call set_trivial_CM_BC(D,kappa_matrix)
                
                !!Set the inital translations
                shiftlist_x=0
                shiftlist_y=0
                !!initalize the first wave fucntion
                call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y)
                !!Compute first wave function
                psi=chiral_translated_psi(Xcoord,Ycoord,tau&
                     ,shiftlist_x,shiftlist_y)
                !!Set the translations fro group alpha         
                !!Change all coordinates one step in the x-direction for one group    
                shiftlist_x(group_no)=Ns
                shiftlist_y(group_no)=0
                !!initalize the second wave fucntion
                call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y)
                psi_2x=chiral_translated_psi(Xcoord,Ycoord,tau&
                     ,shiftlist_x,shiftlist_y)
                !!Set the translations fro group alpha
                !!Change all coordinates in the y-direction

                shiftlist_x(group_no)=0
                shiftlist_y(group_no)=Ns
                psi_2y=chiral_translated_psi(Xcoord,Ycoord,tau&
                     ,shiftlist_x,shiftlist_y)
                !!initalize the third wave fucntion
                call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y)
                !!Change all coordinates in the x and y-direction
                shiftlist_x(group_no)=Ns
                shiftlist_y(group_no)=Ns
                !!initalize the fourth wave fucntion
                call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y)
                psi_2xy=chiral_translated_psi(Xcoord,Ycoord,tau&
                     ,shiftlist_x,shiftlist_y)
                !!The raw_shift will be used to compare egenvalues before
                !!and after shifts
                !!The expected shift on group alpha is 2*pi*p_alpha /q
                raw_shift_x=pi*(Ns + kappa_matrix(group_no,group_no))&
                     *cells*plist(group_no)
                raw_shift_y=pi*(Ns + kappa_matrix(group_no,group_no))&
                     *cells*plist(group_no)
                !!Translating in both directions the two pcs cancel
                !!ONly the noncummativity survive
                raw_shift_xy=pi*Ns*cells*plist(group_no)

                !!Reduce to smallest mod_2pi
                psi_2x=mod_2pi(psi_2x  -iunit*raw_shift_x)
                psi_2y=mod_2pi(psi_2y  -iunit*raw_shift_y)
                psi_2xy=mod_2pi(psi_2xy-iunit*raw_shift_xy)
                
                !!Test the periodic boundary conditions             
                if(test_diff_exp_cmplx(psi,psi_2x,1.d-7)&
                     .or.&
                     test_diff_exp_cmplx(psi,psi_2y,1.d-7)&
                     .or.&
                     test_diff_exp_cmplx(psi,psi_2xy,1.d-7))then
                   write(*,'(A,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                        ,'Change global coorinates for',Ne&
                        ,' particles in group ',group_no&
                        ,' .-.-.-.-.-.-'
                   write(*,*) 'Not-periodic'
                   write(*,*) 'For kappa-matrix'
                   call print_int_matrix(kappa_matrix)
                   write(*,*) 'tau:',tau
                   write(*,*) 'cells:',cells
                   call print_the_3psis(psi,psi_2x,psi_2y,psi_2xy)
                   write(*,*) 'Coordinates Z:'
                   write(*,*) Xcoord+tau*Ycoord
                   call exit(-2)
                end if
             end do
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_parity_nlist(D,nlist,maxlist,minlist,inclist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist,inclist)
       deallocate(shiftlist_x,shiftlist_y)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_periodic_Translations


  subroutine test_naive_anti_symmetrization
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:) :: shiftlist_x,shiftlist_y
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: nlist,maxlist,minlist,inclist
    integer, ALLOCATABLE, DIMENSION(:) :: plist,group_size
    integer :: q,cells,D,p,Ne,Ns,group_no
    integer :: part1
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y,psi_local
    real(kind=dp) :: tau_x,tau_y
    INTEGER, DIMENSION(:), ALLOCATABLE :: comp_comb1
    type(combination) :: comb1
    
    !!This function test that anti-symetrization of the wave-function is the same as the naive anti-sysmtization 
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that the sampled (anti)-symetrization  is the same as the naive one'
    
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!We start with chiral hierarchy states!!
    do D=2,2
       !write(*,*) 'D=',D
       ALLOCATE(kappa_matrix(D,D))
       allocate(shiftlist_x(D),shiftlist_y(D))
       ALLOCATE(plist(D),group_size(D),nlist(D),maxlist(D),minlist(D),inclist(D))
       !!Initialize the n-list (only look at small subset)
       maxlist=4
       maxlist(1)=4
       minlist=0
       minlist(1)=2 !!Start counting form 2, otherwise the LLL will be overfilled
       inclist=2!!(Anti-symetrization is only well defined for paricles of same braiding statisics
       inclist(1)=1
       nlist=minlist
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          !write(*,*) 'kappa_matrix'
          !call print_int_matrix(kappa_matrix)
          do cells=1,3 !!Loop over different number of fundamental cells
             Ns=q*cells
             Ne=p*cells
             group_size=cells*plist
             call set_trivial_CM_BC(D,kappa_matrix)

             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             !!Here there are three main considerations T_Ns,0,T_0,Ns,and T_0,0             
             
             !!Initialize the cooridinates (use unit length in X).
             !!The length scale Lx is thus implicit.
             call RANDOM_NUMBER(tau_x)
             call RANDOM_NUMBER(tau_y)
             !Put tau_x in range -1/2 < tau_x < 1/2
             tau_x=tau_x-.5
             !Put tau_y in range 1/2 < tau_x < 1+1/2
             tau_y=tau_y+.5
             tau=tau_x+iunit*tau_y
             call RANDOM_NUMBER(Xcoord)
             !!Make the random number in the range -1/2 < x < 1/2
             Xcoord=Xcoord-.5d0
             !!Place the y-coordiante on a line (so they will now overlap)
             Ycoord = (/(part1/real(Ne,dp), part1=1,Ne)/)-.5d0
             !!write(*,*) 'Ycoord',Ycoord

             !!Set the global shifts lists
             shiftlist_x = (/(group_no-1, group_no=1,D)/)
             shiftlist_y=0
             
             !!Compute first wave function
             !!psi=chiral_sym_psi(Xcoord,Ycoord,tau,Ne,Ns,kappa_matrix&
             !!,D,shiftlist_x,shiftlist_y)
             call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y)
             psi=chiral_sym_psi(Xcoord,Ycoord,tau)
             

             !!Antrisyemtrize by hand for two groups
             !!(make sure the results are the same)
             !!-----------------------------------------------
             !! Copy of the code from chiral_wave_function at rev 1436
             !!-----------------------------------------------
             !!create the first combination
             call createCombination(comb1)
             call initializeCombination(comb1,Ne,group_size(1))
             ALLOCATE(comp_comb1(Ne-group_size(1)))
             !!FIXME - uggly way to set psi = -infty
             psi_2y=0.d0
             psi_2y=log(psi_2y)
             do 
                !!If the combination computed is ok, do stuff
                IF ( .NOT. Comb1%ok ) EXIT
                CALL Pcomp(Ne,comb1%C,comp_comb1)
                !!Set the permuted wave-function
                Xcoord2(1:group_size(1))=Xcoord(Comb1%c)
                Xcoord2((group_size(1)+1):Ne)=Xcoord(comp_comb1)
                Ycoord2(1:group_size(1))=Ycoord(Comb1%c)
                Ycoord2((group_size(1)+1):Ne)=Ycoord(comp_comb1)
                !!Compute the matrix
                psi_local=chiral_translated_psi(Xcoord2,Ycoord2,tau&
                     ,shiftlist_x,shiftlist_y)
                !!Add the combinations, with a possible minus sign if the k-matrix is odd
                psi_2y=add_exponentially(psi_2y,psi_local&
                     +iunit*pi*SUM(comb1%C)*kappa_matrix(1,1))
                !!Make next combination
                CALL nextCombination(Comb1)
             END DO
             call destroyCombination(comb1)
             DEALLOCATE(comp_comb1)
             !!-----------------------------------------------
             !!-----------------------------------------------


             !!Test the periodic boundary conditions             
             if(test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                write(*,'(A,A,I3,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                     ,'Not the same wave-fucntion for ',Ne&
                     ,' particles .-.-.-.-.-.-'
                write(*,*) 'Not-periodic'
                write(*,*) 'For kappa-matrix'
                call print_int_matrix(kappa_matrix)
                write(*,*) 'tau:',tau
                write(*,*) 'cells:',cells
                write(*,*) 'Output phase different'
                call print_the_1psis(psi,psi_2x)
                write(*,*) 'Corrected phase different'
                call print_the_1psis(psi,psi_2y)
                write(*,*) 'Coordinates Z:'
                write(*,*) Xcoord+tau*Ycoord
                call exit(-2)
             end if
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_parity_nlist(D,nlist,maxlist,minlist,inclist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,group_size,nlist,maxlist,minlist,inclist)
       deallocate(shiftlist_x,shiftlist_y)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_naive_anti_symmetrization







  
  subroutine test_anti_symmetrization
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:) :: shiftlist_x,shiftlist_y
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist,inclist
    integer :: q,cells,D,p,Ne,Ns,group_no
    integer :: part1,part2,max_cells
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: tau_x,tau_y
    
    !!This function test that anti-symetrization of the wave-functions can be done
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that the (anti)-symetrization works'
    
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    do D=1,4
       !write(*,*) 'D=',D
       ALLOCATE(kappa_matrix(D,D))
       allocate(shiftlist_x(D),shiftlist_y(D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D),inclist(D))
       !!Initialize the n-list (only look at small subset)
       if(D.lt.3)then!!for small number
          max_cells=2
          maxlist=2
          maxlist(1)=4
          minlist=0
          minlist(1)=2 !!Start counting form 2, otherwise the LLL will be overfilled
       elseif(D.eq.3)then !!Diagonas is enough
          max_cells=2
          maxlist=0
          maxlist(1)=4
          minlist=0
          minlist(1)=2 !!Start counting form 2, otherwise the LLL will be overfilled
       else
          max_cells=1
          maxlist=0 !!Do not makte interations in the other layes
          maxlist(1)=3
          minlist=0
          minlist(1)=2 !!Start counting form 3 and ,look at only one case
       end if
       inclist=2!!(Anti-symetrization is only well defined for paricles of same braiding statisics
       inclist(1)=1
       nlist=minlist
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          !write(*,*) 'kappa_matrix'
          !call print_int_matrix(kappa_matrix)
          do cells=1,max_cells !!Loop over different number of fundamental cells
             Ns=q*cells
             Ne=p*cells
             call set_trivial_CM_BC(D,kappa_matrix)
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             !!Here there are three main considerations T_Ns,0,T_0,Ns,and T_0,0             
             
             !!Initialize the cooridinates (use unit length in X).
             !!The length scale Lx is thus implicit.
             call RANDOM_NUMBER(tau_x)
             call RANDOM_NUMBER(tau_y)
             !Put tau_x in range -1/2 < tau_x < 1/2
             tau_x=tau_x-.5
             !Put tau_y in range 1/2 < tau_x < 1+1/2
             tau_y=tau_y+.5
             tau=tau_x+iunit*tau_y
             call RANDOM_NUMBER(Xcoord)
             !!Make the random number in the range -1/2 < x < 1/2
             Xcoord=Xcoord-.5d0
             !!Place the y-coordiante on a line (so they will now overlap)
             Ycoord = (/(part1/real(Ne,dp), part1=1,Ne)/)-.5d0
             !!write(*,*) 'Ycoord',Ycoord

             !!Set the global shifts lists
             shiftlist_x = (/(group_no-1, group_no=1,D)/)
             shiftlist_y=0
             
             !!Compute first wave function
             !!psi=chiral_sym_psi(Xcoord,Ycoord,tau,Ne,Ns,kappa_matrix&
             !!,D,shiftlist_x,shiftlist_y)
             call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y)
             psi=chiral_sym_psi(Xcoord,Ycoord,tau)
             
             do part1=1,(Ne-1)
                do part2=(part1+1),Ne
                   !!Set the new coodinates
                   Xcoord2=Xcoord
                   Ycoord2=Ycoord
                   !!Trade palces opn aprticel one and two
                   Xcoord2(part1)=Xcoord(part2)
                   Ycoord2(part1)=Ycoord(part2)
                   Xcoord2(part2)=Xcoord(part1)
                   Ycoord2(part2)=Ycoord(part1)
                   
                   !!Compute the wfn again
                   !!psi_2x=chiral_sym_psi(Xcoord2,Ycoord2,tau,Ne,Ns&
                   !!,kappa_matrix,D,shiftlist_x,shiftlist_y)
                   psi_2x=chiral_sym_psi(Xcoord2,Ycoord2,tau)
                                      
                   !!We expect a shift of pi from the (anti)symetrization
                   !!Reduce to smallest mod_2pi
                   psi_2y=mod_2pi(psi_2x+iunit*pi*kappa_matrix(1,1))
                   !!it's sufficient to look at frist kappa-matrix entrry,
                   !!as the diagonals should have the same parity

                   
                   !!Test the periodic boundary conditions             
                   if(test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                      write(*,'(A,A,I3,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                           ,'Not (anti)-summetric for particle no ',part1&
                           ,' and ',part2,' of ',Ne&
                           ,' particles .-.-.-.-.-.-'
                      write(*,*) 'Not-periodic'
                      write(*,*) 'For kappa-matrix'
                      call print_int_matrix(kappa_matrix)
                      write(*,*) 'tau:',tau
                      write(*,*) 'cells:',cells
                      write(*,*) 'Output phase different'
                      call print_the_1psis(psi,psi_2x)
                      write(*,*) 'Corrected phase different'
                      call print_the_1psis(psi,psi_2y)
                      write(*,*) 'Coordinates Z:'
                      write(*,*) Xcoord+tau*Ycoord
                      call exit(-2)
                   end if
                end do
             end do
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_parity_nlist(D,nlist,maxlist,minlist,inclist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist,inclist)
       deallocate(shiftlist_x,shiftlist_y)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_anti_symmetrization
  

  subroutine test_non_anti_symmetrization
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:) :: shiftlist_x,shiftlist_y
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer, ALLOCATABLE, DIMENSION(:) :: plist,nlist,maxlist,minlist,inclist
    integer :: q,cells,D,p,Ne,Ns,group_no
    integer :: part1,part2,max_cells
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: tau_x,tau_y

    call set_cft_wfn_verbose(.FALSE.)
    
    !!This function test that anti-symetrization of the wave-functions can be done
    
    !!Make sure pbs are set to t=h=0 at the beginning
    call unset_CM_BC
    write(*,*) 'Test that the wave function with correct symetries even though no (anti)-symetrization is present'
    
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!WE start with chiral hierarchy states!!
    do D=1,4
       !write(*,*) 'D=',D
       ALLOCATE(kappa_matrix(D,D))
       allocate(shiftlist_x(D),shiftlist_y(D))
       ALLOCATE(plist(D),nlist(D),maxlist(D),minlist(D),inclist(D))
       !!Initialize the n-list (only look at small subset)
       if(D.lt.3)then!!for small number
          max_cells=2
          maxlist=2
          maxlist(1)=4
          minlist=0
          minlist(1)=2 !!Start counting form 2, otherwise the LLL will be overfilled
       elseif(D.eq.3)then !!Diagonas is enough
          max_cells=2
          maxlist=0
          maxlist(1)=4
          minlist=0
          minlist(1)=2 !!Start counting form 2, otherwise the LLL will be overfilled
       else
          max_cells=1
          maxlist=0 !!Do not makte interations in the other layes
          maxlist(1)=3
          minlist=0
          minlist(1)=2 !!Start counting form 3 and ,look at only one case
       end if
       inclist=2!!(Anti-symetrization is only well defined for paricles of same braiding statisics
       inclist(1)=1
       nlist=minlist
       do 
          !!Initialize kappa-matrix
          call gen_hierarchy_k_matrix(D,kappa_matrix,nlist)
          call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
          !write(*,*) 'kappa_matrix'
          !call print_int_matrix(kappa_matrix)
          do cells=1,max_cells !!Loop over different number of fundamental cells
             Ns=q*cells
             Ne=p*cells
             call set_trivial_CM_BC(D,kappa_matrix)
             ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
             !!Here there are three main considerations T_Ns,0,T_0,Ns,and T_0,0             
             
             !!Initialize the cooridinates (use unit length in X).
             !!The length scale Lx is thus implicit.
             call RANDOM_NUMBER(tau_x)
             call RANDOM_NUMBER(tau_y)
             !Put tau_x in range -1/2 < tau_x < 1/2
             tau_x=tau_x-.5
             !Put tau_y in range 1/2 < tau_x < 1+1/2
             tau_y=tau_y+.5
             tau=tau_x+iunit*tau_y
             call RANDOM_NUMBER(Xcoord)
             !!Make the random number in the range -1/2 < x < 1/2
             Xcoord=Xcoord-.5d0
             !!Place the y-coordiante on a line (so they will now overlap)
             Ycoord = (/(part1/real(Ne,dp), part1=1,Ne)/)-.5d0
             !!write(*,*) 'Ycoord',Ycoord

             !!Set the global shifts lists
             shiftlist_x = (/(group_no-1, group_no=1,D)/)
             shiftlist_y=0
             
             !!Compute first wave function
             !!psi=chiral_sym_psi(Xcoord,Ycoord,tau,Ne,Ns,kappa_matrix&
             !!,D,shiftlist_x,shiftlist_y)
             call initialize_wave_function(D,kappa_matrix,Ne,Ns,shiftlist_x,shiftlist_y,asym=.FALSE.)
             psi=chiral_sym_psi(Xcoord,Ycoord,tau)
             
             do part1=1,(Ne-1)
                do part2=(part1+1),Ne
                   !!Set the new coodinates
                   Xcoord2=Xcoord
                   Ycoord2=Ycoord
                   !!Trade palces opn aprticel one and two
                   Xcoord2(part1)=Xcoord(part2)
                   Ycoord2(part1)=Ycoord(part2)
                   Xcoord2(part2)=Xcoord(part1)
                   Ycoord2(part2)=Ycoord(part1)
                   
                   !!Compute the wfn again
                   !!psi_2x=chiral_sym_psi(Xcoord2,Ycoord2,tau,Ne,Ns&
                   !!,kappa_matrix,D,shiftlist_x,shiftlist_y)
                   psi_2x=chiral_sym_psi(Xcoord2,Ycoord2,tau)
                                      
                   !!We expect a shift of pi from the (anti)symetrization
                   !!Reduce to smallest mod_2pi
                   psi_2y=mod_2pi(psi_2x+iunit*pi*kappa_matrix(1,1))
                   !!it's sufficient to look at frist kappa-matrix entrry,
                   !!as the diagonals should have the same parity

                   if(same_group(part1,part2,plist,cells))then
                      !write(*,*) 'Particles are in the same group'
                      !!Test the periodic boundary conditions             
                      if(test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                         write(*,'(A,A,I3,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                              ,'Not (anti)-summetric for particle no ',part1&
                              ,' and ',part2,' of ',Ne&
                              ,' particles .-.-.-.-.-.-'
                         write(*,*) 'Not-periodic'
                         write(*,*) 'For kappa-matrix'
                         call print_int_matrix(kappa_matrix)
                         write(*,*) 'tau:',tau
                         write(*,*) 'cells:',cells
                         write(*,*) 'Output phase different'
                         call print_the_1psis(psi,psi_2x)
                         write(*,*) 'Corrected phase different'
                         call print_the_1psis(psi,psi_2y)
                         write(*,*) 'Coordinates Z:'
                         write(*,*) Xcoord+tau*Ycoord
                         call exit(-2)
                      end if
                   else
                      !write(*,*) 'Particles are not in the same group'
                      if(.not.test_diff_exp_cmplx(psi,psi_2y,1.d-7,&
                           verbose=.false.))then
                         write(*,'(A,A,I3,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                              ,'(anti)-summetric for particle no ',part1&
                              ,' and ',part2,' of ',Ne&
                              ,' particles when it shouldnt be.-.-.-.-.-.-'
                         write(*,*) 'Not-periodic'
                         write(*,*) 'For kappa-matrix'
                         call print_int_matrix(kappa_matrix)
                         write(*,*) 'tau:',tau
                         write(*,*) 'cells:',cells
                         write(*,*) 'Output phase different'
                         call print_the_1psis(psi,psi_2x)
                         write(*,*) 'Corrected phase different'
                         call print_the_1psis(psi,psi_2y)
                         write(*,*) 'Coordinates Z:'
                         write(*,*) Xcoord+tau*Ycoord
                         call exit(-2)
                      end if
                   end if
                end do
             end do
             !!Delaocate the coordinates
             DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
          end do !!Next cell size
          !!Compute next combinations
          if(.not.next_parity_nlist(D,nlist,maxlist,minlist,inclist))then
             !!'reached end of n-list'
             exit
          end if
       end do
       DEALLOCATE(kappa_matrix)
       DEALLOCATE(plist,nlist,maxlist,minlist,inclist)
       deallocate(shiftlist_x,shiftlist_y)
       !!unset the pbcs at the end
       call unset_CM_BC
    end do
  end subroutine test_non_anti_symmetrization
  



  subroutine  test_non_abelian_com_choise
    integer, parameter :: D=2  !!Only consider the K=(3 1,1 3) states here
    integer :: kappa_matrix(D,D),plist(D)
    real(kind=dp), ALLOCATABLE, DIMENSION(:) :: Xcoord,Ycoord,Xcoord2,Ycoord2
    integer :: q,cells,p,Ne,Ns,raw_k_sector,n
    integer :: l1,l2,l1_raw,l2_raw,offset !!Dummy_indexes
    complex(KIND=dpc) :: tau=iunit,psi,psi_2x,psi_2y
    real(kind=dp) :: gauge_phase,raw_shift,tau_x,tau_y
   
    !!This test that a) the wavefucntions ca be set with a predefined choise
    !! of dual vectors, b) That the choise is compliant with expectations    

    !!Reset bc:s
    call unset_CM_BC
    write(*,*) 'Test that correct many body T1 eigs can be set for arbitary dual vector choise'
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    
    !!We only conuder the nu=1/2 non belas state
    !! K = ( 3 1 , 1 3)
    kappa_matrix=reshape((/3,1,1,3/),(/D,D/))
    !write(*,*) 'kappa-matrix'
    !call print_int_matrix(kappa_matrix)
    
    call kappa_matrix_group_sizes(D,kappa_matrix,plist,p,q)          
    !write(*,*) 'p=',p,'q=',q
    !write(*,*) 'p-list=',plist
    

    do cells=1,2 !!Loop over different number of fundamental cells
       Ns=q*cells
       Ne=p*cells
       call initialize_wave_function(D,kappa_matrix,Ne,Ns&
            ,(/(0, n=1,D)/),(/(0, n=1,D)/))
       !!Initialize the cooridinates (use unit length in X).
       !!The length scale Lx is thus implicit.
       ALLOCATE(Xcoord(Ne),Ycoord(Ne),Xcoord2(Ne),Ycoord2(Ne))
       
       do l1_raw=0,kappa_matrix(1,1) !!Loop over the possible eigenvalues      
          do l2_raw=0,kappa_matrix(1,1) !!Loop over the possible eigenvalues      
             !!Initate tau
             call RANDOM_NUMBER(tau_x)
             call RANDOM_NUMBER(tau_y)
             !Put tau_x in range -1/2 < tau_x < 1/2
             tau_x=tau_x-.5
             !Put tau_y in range 1/2 < tau_x < 1+1/2
             tau_y=tau_y+.5
             tau=tau_x+iunit*tau_y
             call RANDOM_NUMBER(Xcoord)
             call RANDOM_NUMBER(Ycoord)
             !!Make the random number in the range -1/2 < x < 1/2
             Xcoord=Xcoord-.5d0
             Ycoord=Ycoord-.5d0
             

             !!Copmute aproproate l1,l2
             offset=modulo(Ns-kappa_matrix(1,1),2)
             l1=offset+2*l1_raw
             l2=offset+2*l2_raw

             !!Twise the desired sector
             raw_K_sector = (dot_product((/l1,l2/),plist)*cells)/2
             !write(*,*) 'raw k_sector is',raw_k_sector
             
              !!Set BC:s
             call set_CM_BC(Ns,raw_k_sector,D,kappa_matrix,&
                  dual_index=(/l1,l2/))
             !write(*,*) 'The raw1 shift is',raw_shift
             raw_shift=2*pi*real(raw_k_sector,dp)/Ns
             
             !!Compute first wave function
             psi=chiral_raw_psi(Xcoord,Ycoord,tau)
             
             !!Change all coordinates one step in the x-direction
             Xcoord2=Xcoord+1.d0/Ns
             Ycoord2=Ycoord
             psi_2x=chiral_raw_psi(Xcoord2,Ycoord2,tau)
             
             !!Change all coordinates q steps in the y-direction
             !!This tests the q-fold degeracy
             Xcoord2=Xcoord
             Ycoord2=Ycoord+real(q,dp)/Ns
             
             !!Compute the gaguge phase
             gauge_phase=sum(Xcoord2)*2*pi*q
             psi_2y=chiral_raw_psi(Xcoord2,Ycoord2,tau)&
                  +iunit*gauge_phase !!Add the gauge phase
             !!Reduce to smallest mod_2pi
             psi_2x=mod_2pi(psi_2x-iunit*raw_shift)
             psi_2y=mod_2pi(psi_2y-iunit*q*raw_shift)
             
             !!Test the periodic boundary conditions             
             if(test_diff_exp_cmplx(psi,psi_2x,1.d-7)&
                  .or.&
                  test_diff_exp_cmplx(psi,psi_2y,1.d-7))then
                write(*,'(A,A,I3,A,I3,A)') '.-.-.-.-.-.-.'&
                     ,'Change global coorinates for',Ne&
                     ,' particles and eigenvalue',raw_K_sector&
                     ,'.-.-.-.-.-.-'
                write(*,*) 'Not-periodic'
                write(*,*) 'For kappa-matrix'
                call print_int_matrix(kappa_matrix)
                write(*,*) 'tau:',tau
                write(*,*) 'cells:',cells
                call print_the_psis(psi,psi_2x,psi_2y)
                write(*,*) 'Coordinates Z:'
                write(*,*) Xcoord+tau*Ycoord
                call exit(-2)
             end if
          end do
       end do
       !!Delaocate the coordinates
       DEALLOCATE(Xcoord,Ycoord,Xcoord2,Ycoord2)
    end do !!Next cell size
    call unset_CM_BC
  end subroutine test_non_abelian_com_choise

  !!----------------------------------------------------
  !!                  MISC
  !!....................................................
  
  subroutine print_the_psis(psi,psi_2x,psi_2y)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-x:',mod_2pi(psi_2x)
    write(*,*) 'psi-y:',mod_2pi(psi_2y)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi):',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi):',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_psis

  subroutine print_the_1psis(psi,psi_2)
    complex(kind=dpc), intent(in) :: psi,psi_2
    write(*,*) 'psi  :',mod_2pi(psi)
    write(*,*) 'psi-2:',mod_2pi(psi_2)
    write(*,*) 
    write(*,*) 'diff/(2*pi):',(mod_2pi(psi_2)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_1psis


  subroutine print_the_3psis(psi,psi_2x,psi_2y,psi_2xy)
    complex(kind=dpc), intent(in) :: psi,psi_2x,psi_2y,psi_2xy
    write(*,*) 'psi  : ',mod_2pi(psi)
    write(*,*) 'psi-x: ',mod_2pi(psi_2x)
    write(*,*) 'psi-y: ',mod_2pi(psi_2y)
    write(*,*) 'psi-xy:',mod_2pi(psi_2xy)
    write(*,*) 
    write(*,*) 'diff-x/(2*pi): ',(mod_2pi(psi_2x)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-y/(2*pi): ',(mod_2pi(psi_2y)-mod_2pi(psi))/(2*pi)
    write(*,*) 'diff-xy/(2*pi):',(mod_2pi(psi_2xy)-mod_2pi(psi))/(2*pi)
  end subroutine print_the_3psis

  
  logical function next_nlist(D,nlist,maxlist,minlist)
    integer, intent(IN) :: D,maxlist(D),minlist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_nlist=.FALSE.
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          !!Not-max (incement)
          nlist(n)=nlist(n)+1
          next_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
  end function next_nlist
  
  
  logical function next_parity_nlist(D,nlist,maxlist,minlist,inclist)
    integer, intent(IN) :: D,maxlist(D),minlist(D),inclist(D)
    integer, intent(OUT) :: nlist(D)
    integer :: n
    next_parity_nlist=.FALSE.
    do n=1,D
       if(nlist(n).lt.maxlist(n))then
          !!Not-max (incement)
          nlist(n)=nlist(n)+inclist(n)
          next_parity_nlist=.TRUE.
          exit
       elseif(n.ne.D)then
          !!Max (reset, and go to next)
          nlist(n)=minlist(n)
       end if
    end do
  end function next_parity_nlist

  logical function same_group(N1,N2,plist,cells) result(same)
    !!!Tesst if N1 and N2 are fromt he same group
    integer, intent(in) :: N1,N2,plist(:), cells
    integer :: Nlist(size(plist)),G1,G2,n,D,Ntmp
    !!Initate G1,G2
    G1=0
    G2=0
    Nlist=plist*cells
    D=size(plist)
    Ntmp=0
    do n=1,D
       if(N1.gt.Ntmp.and.N1.le.(Ntmp+Nlist(n)))then
          G1=n !!This gets set eventually
       end if
       if(N2.gt.Ntmp.and.N2.le.(Ntmp+Nlist(n)))then
          G2=n !!This gets set eventually
       end if
       Ntmp=Ntmp+Nlist(n)
    end do
    !write(*,*) "N1,N2:",N1,N2
    !write(*,*) "G1,G2:",G1,G2
    same=(G1.eq.G2)
  end function same_group
  
end program test_chiral_wave_function

