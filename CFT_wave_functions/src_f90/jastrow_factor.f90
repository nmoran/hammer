module jastrow_factor
  use typedef
  use k_matrix
  use jacobitheta
  use dedekind_eta_function
  !!This module contains the modules need to compute the jastrow factor for the wave functions
  !!It includes all the routines for chasing the jacobi-theta functions and such.
  
  implicit none


  !!private
  public sample_theta_functions

contains

  subroutine sample_theta_functions(xlist,ylist,tau,trans_x,trans_y,log_theta_z1z2,working_Ne,working_size,working_ns)
    !This subroutine samples the theta1 function in advance.
    !This is because the antisymetrization only uses a small number of 
    !!different calls to the theta1 function.
    !! The speadup from the theta1 calls should be from
    !! ( Ne choose (N1,N2,...Nn) ) to Ne^2*n
    integer, INTENT(IN) :: working_size,working_Ne,working_ns
    real(KIND=dpc), INTENT(IN) :: xlist(working_Ne),ylist(working_Ne)
    integer, INTENT(IN) :: trans_x(working_size),trans_y(working_size)
    complex(KIND=dpc), intent(IN) :: tau
    complex(KIND=dpc), intent(OUT) :: log_theta_z1z2(working_size*working_ne,working_size*working_ne)
    complex(KIND=dpc) :: zlist(working_Ne),shift1,shift2,res
    integer :: n,m,n_a,n_b,indx1,indx2
    complex(kind=dpc) :: log_zero
    
    
    !!Define infinity=log_of_zero
    log_zero=0.d0*iunit
    log_zero=log(log_zero)

    !!write(*,*) 'sample......',time()
    !!log_theta_z1z2 consists of a Size x size lattice of Ne x Ne blocks
    
    !!Thus it should be read as getting the jastrow factor of two coodriantes
    !! z_n and z_m which can be in the two groups n_a and n_b
    !!Thus (exp on each element of) this matrix should be anti-symmetric
    
    zlist=xlist+tau*ylist
    !write(*,*) 'z-list',zlist
    
    
    !Initalize all elemets to "zero"
    log_theta_z1z2(:,:) = log_zero
    
    !Need only to iterate over n<m since theta_1(z) is odd.
    !!Here we do a little bit of over-sampling,
    !! but it' shouldnt affect things to much
    do n_a = 1,working_Size
       do n = 1,working_Ne
          do n_b = n_a,working_Size
             !write(*,*) '**Large elements',n_a,n_b
             do m = 1,working_Ne
                !write(*,*) '*small elements',n,m
                indx1=n+(n_a-1)*working_Ne
                indx2=m+(n_b-1)*working_Ne
                if(indx1.ne.indx2)then
                   !write(*,*) 'total elements',indx1,indx2
                   !!The relative part
                   shift1=(trans_x(n_a)+tau*trans_y(n_a))/real(working_Ns,dp)&
                        +zlist(n)
                   shift2=(trans_x(n_b)+tau*trans_y(n_b))/real(working_Ns,dp)&
                        +zlist(m)
                   !write(*,*) 'shift1:', shift1
                   !write(*,*) 'shift2:', shift2
                   res=logtheta1_dedekind(shift1-shift2,tau)
                   log_theta_z1z2(indx1,indx2)=res
                   log_theta_z1z2(indx2,indx1)=res+iunit*pi
                   !write(*,*) 'res',res
                   !write(*,'(A)',advance='no') 'i'
                end if
             end do
          end do
       end do
    end do
    !!write(*,*) 'done sampling',time()
  end subroutine sample_theta_functions
  

  complex(Kind=dpc) function compute_raw_jastrow_factor(Zlist,tau,working_ne,chiral_kappa,&
       get_K_mat_large,get_K_bar_large) result(Psi_jastrow)
    integer, intent(in) :: working_ne
    logical, intent(in) :: chiral_kappa
    real(kind=dp) :: get_K_mat_large(working_ne,working_ne),get_K_bar_large(working_ne,working_ne)
    complex(kind=dpc), intent(in) :: Zlist(working_ne),tau
    complex(kind=dpc) :: jastrow_factor,jastrow_bar_factor,z_diff
    !!For the perumation computation
    integer :: indx1,indx2
    real(kind=dp) :: K_factor,K_bar_factor
    
    Psi_jastrow=0.d0
    !write(*,*) 'Theta functions is not-sampled'
    !write(*,*) 'Compute jastrow factor brute force way'
    !!Jastrow wave function
    do indx1=1,(Working_ne-1)
       do indx2=(indx1+1),Working_ne
          K_factor=get_K_mat_large(indx1,indx2)
          !!write(*,*) 'K_factor:',K_factor
          if(chiral_kappa)then !!Simpler if k-matrix is chiral
             if((K_factor.ne.0))then
                z_diff=Zlist(indx1)-Zlist(indx2)
                jastrow_factor=logtheta1_dedekind(z_diff,tau)
                Psi_jastrow=Psi_jastrow+jastrow_factor*K_factor
             end if
          else
             K_bar_factor=get_K_bar_large(indx1,indx2)
             !!write(*,*) 'K_bar_factor:',K_bar_factor
             !write(*,*) 'K_(bar)_factor',K_factor,K_bar_factor
             if((K_factor.ne.0).or.(K_bar_factor.ne.0))then
                z_diff=Zlist(indx1)-Zlist(indx2)
                jastrow_factor=logtheta1_dedekind(z_diff,tau)
                jastrow_bar_factor=dconjg(jastrow_factor)
                !write(*,*) 'factor:    ',jastrow_factor
                !write(*,*) 'factor bar:',jastrow_bar_factor
                Psi_jastrow=Psi_jastrow&
                     +jastrow_factor*K_factor&
                     +jastrow_bar_factor*K_bar_factor
             end if
          end if
       end do
    end do
  end function compute_raw_jastrow_factor


  complex(Kind=dpc) function compute_sampled_jastrow_factor(log_theta_z1z2,permutation_order,&
       working_size,working_ne,chiral_kappa,get_K_mat,get_K_bar,working_groups_stop,&
       working_group_size) result(Psi_jastrow)
    integer, intent(In) :: working_size,working_ne,working_group_size(working_size),working_groups_stop(working_size)
    complex(KIND=dpc), intent(in) :: log_theta_z1z2(working_size*working_ne,working_size*working_ne) !!The the sampled theta function
    logical, intent(in) :: chiral_kappa
    real(kind=dp) :: get_K_mat(working_size,working_size),get_K_bar(working_size,working_size)
    integer, intent(In) :: permutation_order(working_ne)
    complex(kind=dpc) :: jastrow_factor,jastrow_bar_factor
    !!For the perumation computation
    integer :: indx1,indx2
    real(kind=dp) :: K_factor,K_bar_factor
    integer :: indx1_min, indx1_max,indx2_min, indx2_max
    integer :: group1,group2,perm_indx1,perm_indx2
    
    Psi_jastrow=0.d0
    !write(*,*) 'Theta functions is sampled, use that'
    !write(*,*) 'Group size'
    !call write_compactly(working_size,working_group_size)
    !WRITE(*,*) 'Permutation order (elements from each group)'
    !call write_compactly(working_Ne,Permutation_order)
    !!Loop the index
    do group1=1,Working_size
       do group2=group1,Working_size
          !write(*,*) 'groups:         ',group1,group2
          indx1_min=working_groups_stop(group1)&
               -working_group_size(group1)+1 
          !!Loop range differnt depening on if groups are the same or not
          if(group1.eq.group2)then
             !write(*,*) 'groups the same'
             indx1_max=working_groups_stop(group1)-1
          else
             !write(*,*) 'groups not the same'
             indx1_max=working_groups_stop(group1)
          end if
          do indx1=indx1_min,indx1_max
             indx2_max=working_groups_stop(group2)
             !!Loop range differnt depening on if groups are the same or not
             if(group1.eq.group2)then
                indx2_min=indx1+1
             else
                indx2_min=working_groups_stop(group2)&
                     -working_group_size(group2)+1
             end if
             do indx2=indx2_min,indx2_max
                !write(*,*) 'Indexes:',indx1,indx2
                K_factor=get_K_mat(group1,group2)
                K_bar_factor=get_K_bar(group1,group2)
                !write(*,*) 'Jstw k_fac:',K_factor
                !write(*,*) 'Jstw k_bar:',K_bar_factor
                perm_indx1=Permutation_order(indx1)
                perm_indx2=Permutation_order(indx2)
                !write(*,*) 'Permutation index:               '&
                !     ,perm_indx1,perm_indx2
                jastrow_factor=read_theta_sample(group1,group2,&
                     perm_indx1,perm_indx2,log_theta_z1z2,working_Ne,working_size)
                !write(*,*) 'jastrow_factor',jastrow_factor
                if(chiral_kappa)then 
                   !!With a chiral-kappa the jastrow factor is holomprphic
                   Psi_jastrow=Psi_jastrow+jastrow_factor*K_factor
                else !!But othereise there are non-holomorphic components
                   !write(*,*) 'K_factor,K_bar_factor',K_factor,K_bar_factor
                   jastrow_bar_factor=dconjg(jastrow_factor)
                   Psi_jastrow=Psi_jastrow&
                        +jastrow_factor*K_factor&
                        +jastrow_bar_factor*K_bar_factor
                end if
             end do
          end do
       end do
    end do
  end function compute_sampled_jastrow_factor

  complex(Kind=dpc) function compute_jastrow_qps(Zlist,Zqps,tau,working_ne,working_tot_qps,&
       working_size,get_qp_pow,get_k_mat,get_k_bar,working_qp_pow,chiral_kappa,&
       working_groups_stop,working_group_size,working_qps_stop,working_qps_size,working_qp_static) result(Psi_jastrow)
    integer, intent(in) :: working_ne,working_tot_qps,working_size,working_qp_pow(2)
    complex(kind=dpc), intent(in) :: Zlist(working_ne),Zqps(working_tot_qps),tau
    real(kind=dp), intent(in) :: get_qp_pow,get_k_mat(working_size,working_size),get_k_bar(working_size,working_size)
    logical, intent(in) ::chiral_kappa,working_qp_static
    integer, intent(In) :: working_group_size(working_size),working_groups_stop(working_size)
    integer, intent(In) :: working_qps_size(working_size),working_qps_stop(working_size)
    !!For the perumation computation
    
    Psi_jastrow = compute_jastrow_z_eta(Zlist,Zqps,tau,working_ne,working_tot_qps,&
         working_size,get_qp_pow,chiral_kappa,working_groups_stop,working_group_size,working_qps_stop,working_qps_size)
    
    
    if(working_qp_static)then !!If we want the static jastrow factor we add it here
       !write(*,*) 'jf:Compute eta-eta'
       Psi_jastrow =  Psi_jastrow +&
            compute_jastrow_eta_eta(Zqps,tau,working_tot_qps,&
            working_size,get_qp_pow,get_k_mat,get_k_bar,working_qp_pow,chiral_kappa)
    end if
    
  end function compute_jastrow_qps

  complex(Kind=dpc) function compute_jastrow_z_eta(Zlist,Zqps,tau,working_ne,working_tot_qps,&
       working_size,get_qp_pow,chiral_kappa,working_groups_stop,working_group_size,&
       working_qps_stop,working_qps_size) result(Psi_jastrow)
    integer, intent(in) :: working_ne,working_tot_qps,working_size
    complex(kind=dpc), intent(in) :: Zlist(working_ne),Zqps(working_tot_qps),tau
    real(kind=dp), intent(in) :: get_qp_pow
    logical, intent(in) ::chiral_kappa
    integer, intent(In) :: working_group_size(working_size),working_groups_stop(working_size)
    integer, intent(In) :: working_qps_size(working_size),working_qps_stop(working_size)
    complex(kind=dpc) :: jastrow_factor,jastrow_bar_factor,z_diff
    !!For the perumation computation
    integer :: group,qpindx,elindx
    real(kind=dp) :: K_factor,K_bar_factor
 
   Psi_jastrow=0.d0
    !!Jastrow wave function (z-eta) part   
    !!Implemented for D=1 non-chiral, and D=* chiral
    if(chiral_kappa)then
       !!FIXME: Implement chiral kappa-matrix factors
       !!Loop through qp-groups
       do group=1,working_size
          !!Loops through qp in that group
          do qpindx=(working_qps_stop(group)-working_qps_size(group)+1),working_qps_stop(group)
             !!Loop through electrons-in corresponding group
             do elindx=(working_groups_stop(group)-working_group_size(group)+1),working_groups_stop(group) 
                !write(*,*) 'Group,qno,elno:',group,qpindx,elindx
                !!The argument
                z_diff=Zlist(elindx)-Zqps(qpindx)
                !!The jastrow factors
                jastrow_factor=logtheta1_dedekind(z_diff,tau)
                !write(*,*) 'factor:    ',jastrow_factor
                !write(*,*) 'factor bar:',jastrow_bar_factor
                Psi_jastrow=Psi_jastrow+jastrow_factor
             end do
          end do
       end do
    else
       if(working_size.gt.1)then
          write(stderr,*) 'ERROR: Jastrow z-eta not implemented for D>1 non-chiral kappa-matrix'
          call exit(-1)
       else
          do elindx=1,Working_ne
             do qpindx=1,Working_tot_qps
                K_factor=1+get_qp_pow
                !write(*,*) 'K_factor:',K_factor
                K_bar_factor=get_qp_pow
                !write(*,*) 'K_bar_factor:',K_bar_factor
                !!The argument
                z_diff=Zlist(elindx)-Zqps(qpindx)
                !!The jastrow factors
                jastrow_factor=logtheta1_dedekind(z_diff,tau)
                jastrow_bar_factor=dconjg(jastrow_factor)
                !write(*,*) 'factor:    ',jastrow_factor
                !write(*,*) 'factor bar:',jastrow_bar_factor
                Psi_jastrow=Psi_jastrow&
                     +jastrow_factor*K_factor&
                     +jastrow_bar_factor*K_bar_factor
             end do
          end do
       end if
    end if
  end function compute_jastrow_z_eta
  
  complex(Kind=dpc) function compute_jastrow_eta_eta(Zqps,tau,working_tot_qps,&
       working_size,get_qp_pow,get_k_mat,get_k_bar,working_qp_pow,chiral_kappa) result(Psi_jastrow)
    integer, intent(in) :: working_tot_qps,working_size,working_qp_pow(2)
    complex(kind=dpc), intent(in) :: Zqps(working_tot_qps),tau
    real(kind=dp), intent(in) :: get_qp_pow,get_k_mat(working_size,working_size),get_k_bar(working_size,working_size)
    complex(kind=dpc) :: jastrow_factor,jastrow_bar_factor,z_diff
    !!For the perumation computation
    integer :: indx1,indx2
    real(kind=dp) :: K_factor,K_bar_factor
    logical, intent(in) ::chiral_kappa    

    !!Impletemted for D=1 non-chiral, and D=1 chiral
    !!Jastrow wave function (eta-eta) part
    Psi_jastrow=0.d0
    if(working_size.gt.1)then
       write(stderr,*) 'ERROR: Jastrow eta-eta not implemented for D>1'
       call exit(-1)
    else
       do indx1=1,(Working_tot_qps-1)
          do indx2=(indx1+1),Working_tot_qps
             K_factor=(1+get_qp_pow)**2/get_k_mat(1,1)
             if(working_qp_pow(1).eq.0)then
                K_bar_factor=0.d0
             else
                K_bar_factor=get_qp_pow**2/get_k_bar(1,1)
             end if
             !!The argument
             z_diff=Zqps(indx1)-Zqps(indx2)
             !!The jastrow factors
             jastrow_factor=logtheta1_dedekind(z_diff,tau)
             jastrow_bar_factor=dconjg(jastrow_factor)
             !!write(*,*) 'i,j:',indx1,indx2
             !!write(*,*) 'factor:    ',jastrow_factor
             !write(*,*) 'factor bar:',jastrow_bar_factor
             Psi_jastrow=Psi_jastrow&
                  +jastrow_factor*K_factor&
                  +jastrow_bar_factor*K_bar_factor
          end do
       end do
    end if
  end function compute_jastrow_eta_eta
  
  
  complex(kind=dpc) function logtheta1_dedekind(z,tau) result(res)
    !!Normalizing the jastrow factors with the dedekind eta_function
    complex(kind=dpc) , intent(IN) :: z,tau
    
    res=logtheta1(z,tau)-log_dedekind_eta(tau)
    !!res=logtheta1(z,tau)
  end function logtheta1_dedekind




  complex(kind=dpc) function read_theta_sample(group_a,group_b&
       ,particle_a,particle_b,log_theta_z1z2,working_Ne,working_size) result(theta_res)
    integer, intent(in) :: group_a,group_b,particle_a,particle_b,working_Ne,working_size
    complex(KIND=dpc), intent(in) :: log_theta_z1z2(working_size*working_ne,working_size*working_ne) !!The the sampled theta function
    integer :: indx1,indx2
    
    !!Compute index
    indx1 = particle_a + (group_a-1)*working_Ne
    indx2 = particle_b + (group_b-1)*working_Ne
    !! extract value
    theta_res=log_theta_z1z2(indx1,indx2)
  end function read_theta_sample
  

  
end module jastrow_factor
