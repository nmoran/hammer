MODULE DiscretizedWaveFunctions

  use timer
  use typedef
  use misc_random
  use channels
  use omp_lib
  IMPLICIT NONE
  
  integer ::  Nsamples  ! Number of Monte Carlo Samples
  integer ::  NumCoord    ! Number of Coordiantes
  logical ::  NewCoord  ! Generate New Coordinates
  !!Set the default output catalouge to the current "."
  character(len=100) :: output_dir="."
  REAL(dp), ALLOCATABLE, DIMENSION(:,:) :: xx, yy  

  private
  public :: SampleTheWaveFunction
  public :: set_output_dir
  public :: get_output_dir
  
CONTAINS

  subroutine set_output_dir(dir_to_set)
    character(*), intent(IN) :: dir_to_set
    output_dir=dir_to_set
  end subroutine set_output_dir
  
  subroutine get_output_dir(out_dir)
    character(*), intent(OUT) :: out_dir
    out_dir=output_dir
  end subroutine get_output_dir
  
  
  !!Routine for sampling the wave-functions
  SUBROUTINE SampleTheWaveFunction(samples,reuse,coordinates,File_wfn_re,File_wfn_im,tau,WaveFunction,logscale,Fluxes)
    character(*),intent(In) :: File_wfn_re,File_wfn_im
    integer, intent(in) :: samples,coordinates,Fluxes
    complex(kind=dpc), intent(in) :: tau
    logical, intent(in) :: reuse,logscale
    interface
       function WaveFunction(rx,ry,tau,Xqps,Yqps)
         use typedef !!Needed for the wave function to know typedefs
         complex(kind=dpc) WaveFunction
         real(kind=dp), intent(In), dimension(:) :: rx, ry
         complex(kind=dpc), intent(IN)  :: tau
         real(KIND=dp), optional, intent(In), dimension(:) :: Xqps,Yqps

       end function WaveFunction
    end interface

    !!Set the global parameters
    nsamples=samples
    numcoord=coordinates
        
    WRITE(*,*) 'Number of samples:',nsamples
    if (.not.reuse) then 
       Write(*,*) 'Generates new coordinates'
    else 
       Write(*,*) 'Read ouput coordinates from file'
    end if
    
    WRITE(*,*) 'Generating finite random reps of trial wfs.'
    
    if(nsamples.gt.0)then
       
       ALLOCATE(xx(coordinates,nsamples),yy(coordinates,nsamples))
       
       !!Here is the logic deciding how to and what to generate
       if(.not.reuse)then
          !!call GenerateAndSampleED(File_wfn_re,File_wfn_im)
          call GenerateAndSampleWaveFunction(tau,Fluxes,&
               File_wfn_re,File_wfn_im,WaveFunction,logscale)
       else
          call initialize_old_coordinate()
          call SampleWaveFunction(tau,File_wfn_re,File_wfn_im,WaveFunction)
       end if
       
       DEALLOCATE(xx,yy) !!Deallocate once used
       
    else
       Write(*,*) '*** No sampling ***'
    end if
  end SUBROUTINE SampleTheWaveFunction

  subroutine initialize_old_coordinate()
    Integer  :: chxx = 148, chyy = 149,i
    
    OPEN(chxx,FILE=TRIM(TRIM(output_dir)//'/square_xx.rnd'),STATUS='old')
    OPEN(chyy,FILE=TRIM(TRIM(output_dir)//'/square_yy.rnd'),STATUS='old')
    DO i = 1, nsamples
       read(chxx,*)  xx(:,i)
       read(chyy,*)  yy(:,i)
       !write(*,*) 'coordinates xx:',xx(:,i)
       !write(*,*) 'coordinates yy:',yy(:,i)
    END DO
    CLOSE(chxx)
    CLOSE(chyy)
  end subroutine initialize_old_coordinate
  
  subroutine New_square_coordinates(randx,randy)
    REAL(kind=dp), intent(out), DIMENSION(:) :: randx,randy
    !!Computes random coodinares in the unit square centered around the origin
    CALL RANDOM_NUMBER(randx)
    CALL RANDOM_NUMBER(randy)
    randx = randx-0.5d0
    randy = randy-0.5d0
  end subroutine New_square_coordinates

  subroutine runif_symmetric(rand)
    REAL(kind=dp), intent(out) :: rand
    !!Computes random coodinares from -.5 to .5
    CALL RANDOM_NUMBER(rand)
    rand = rand-0.5d0
  end subroutine Runif_symmetric



  subroutine Move_square_coordinates(randx,randy,randxold,randyold,Numfluxes,tau)
    !!New version where one particle is moved randomly
    REAL(kind=dp), intent(out), DIMENSION(:) :: randx,randy
    REAL(kind=dp), intent(in), DIMENSION(:) :: randxold,randyold
    complex(kind=dp), intent(in) :: tau
    integer, intent(in) :: Numfluxes
    real(kind=dp) :: flux_factor,mag_len_x,mag_len_y,rnd_x,rnd_y
    real(kind=sp) :: num_part
    integer :: particle

    !!Choose particle
    CALL RANDOM_NUMBER(num_part)
    particle = floor(num_part*size(randx))+1
    !!write(*,*) 'Choose particle no:', particle
    
    !!Generate the offset
    call runif_symmetric(rnd_x)
    call runif_symmetric(rnd_y)
    !!Move the offset on the scale mangetic length ~ 1/sqrt(Numfluxes)
    flux_factor=.5d0/sqrt(real(Numfluxes,dp))
    mag_len_x=flux_factor*sqrt(aimag(tau))
    mag_len_y=flux_factor/sqrt(aimag(tau))

    !!New coorinates is old coordinates
    randx=randxOld
    randy=randyOld
    !!Up to the one coordinates that is changed
    randx(particle)=randx(particle)+mag_len_x*rnd_x
    randy(particle)=randy(particle)+mag_len_y*rnd_y
    !!Reduce to the origin
    randx=randx-nint(randx)
    randy=randy-nint(randy)
  end subroutine Move_square_coordinates

  subroutine SampleWaveFunction(tau,File_wfn_re,File_wfn_im,WaveFunction)
    character(*),intent(In) :: File_wfn_re,File_wfn_im
    complex(KIND=dpc), intent(IN) :: tau
    complex(KIND=dpc) :: Psi_list(nsamples) !!List of wave-function values
    REAL(kind=dp), dimension(NumCoord):: randx,randy !!Coordinates
    !!Record the time
    integer :: i,total_num

    !!The current wave function value
    COMPLEX(kind=dpc) :: Psi    
    interface
       function WaveFunction(rx,ry,tau,Xqps,Yqps)
         use typedef !!Needed for the wave function to know typedefs
         complex(kind=dpc) WaveFunction
         real(kind=dp), intent(In), dimension(:) :: rx, ry
         complex(kind=dpc), intent(IN)  :: tau
         real(KIND=dp), optional, intent(In), dimension(:) :: Xqps,Yqps
       end function WaveFunction
    end interface
    
    !StartTime=time()
    CALL setup_write_progress()
   
    total_num = 0 !!start counting
    !$OMP PARALLEL DO PRIVATE(randx, randy,Psi)
    DO i = 1, nsamples
       randx = xx(:,i) !!Extract x-coordinates
       randy = yy(:,i) !!Extract y-coordinates
       Psi=WaveFunction(randx,randy,tau)
       Psi_List(i)=Psi
       !$OMP Critical !!Update the counting index and write timer
       total_num = total_num + 1 !!step up counting
       call write_progress(total_num,nsamples)
       !$OMP END Critical       
    END DO
    !$OMP END PARALLEL DO

    
    WRITE(*,'(/,T8, A,/)') 'Sampling of wave function done.'    
    call print_wave_function(Psi_List,File_wfn_re,File_wfn_im)
    
  end subroutine SampleWaveFunction
  
  subroutine GenerateAndSampleWaveFunction(tau,NumFluxes,File_wfn_re,File_wfn_im,WaveFunction,LogScale)
    character(*),intent(In) :: File_wfn_re,File_wfn_im
    integer, intent(IN) :: NumFluxes
    complex(KIND=dpc), intent(IN) :: tau
    complex(kind=dpc) :: Psi_List(nsamples)
    logical,intent(In)  :: LogScale
    ! initialize the chiral wave functions
    
    integer :: BurnSteps=500,i,Accepted
    COMPLEX(dpc) :: Psi, PsiOld
    REAL(dp), dimension(NumCoord):: randx,randy
    REAL(dp), dimension(NumCoord):: randxOld,randyOld
    REAL(dp) :: xx_scaled(NumCoord,nsamples),yy_scaled(NumCoord,nsamples)
    !!Physical dimention
    real(dp) :: Lx
    interface
       function WaveFunction(rx,ry,tau,Xqps,Yqps)
         use typedef !!Needed for the wave function to know typedefs
         complex(kind=dpc) WaveFunction
         real(kind=dp), intent(In), dimension(:) :: rx, ry
         complex(kind=dpc), intent(IN)  :: tau
         real(KIND=dp), optional, intent(In), dimension(:) :: Xqps,Yqps
       end function WaveFunction
    end interface

    Accepted=0
    Write(*,*) 'Create new coordinates!'
    CALL init_RANDOM_SEED()
    
    write(*,*) 'Burn in, running',BurnSteps,'samples to find good starting number'
    CALL setup_write_progress()

    ! Center coodinates around the origin of the unit square    
    call New_square_coordinates(randxOld,randyOld)
    ! Compute initial wave-funcion value
    PsiOld = WaveFunction(randxold,randyold,tau)
    
    write(*,*) "Start the burn proces"    
    do
       !! Burn in
       DO i = 1, BurnSteps
          call sample_choose_new_coordinate(i,BurnSteps&
               ,randx,randy,randxold,randyold,Numfluxes,tau,LogScale,Psi,&
               PsiOld,WaveFunction,Accepted)
       END DO
       write(*,*) 'Acceptance rate:',100.0*Accepted/BurnSteps,'%'
       if((100.0*Accepted/BurnSteps).gt.90)then
          write(*,*) 'Burnproces not completed, one more turn'
          Accepted=0
       else
          write(*,*) "Burn process completed"
          exit
       end if
    end do
    write(*,*) 'Recording coordinades and psi-values'
    CALL setup_write_progress()
    Accepted = 0 !!Reset acceptance
    
    
    !! Real sampling
    DO i = 1, nsamples
       call sample_choose_new_coordinate(i,nsamples&
            ,randx,randy,randxold,randyold,Numfluxes,tau,LogScale,Psi,&
            PsiOld,WaveFunction,Accepted)
       xx(:,i) = randxOld
       yy(:,i) = randyOld
       Psi_List(i) = PsiOld
    END DO
    write(*,*) 'Acceptance rate:',100.0*Accepted/nsamples,'%'
    WRITE(*,'(/,T8, A,/)') 'Wave function sampling done.'
    
    call print_wave_function(Psi_List,File_wfn_re,File_wfn_im)
    call print_coordinates(xx,yy,TRIM(output_dir)//'/square_xx.rnd',TRIM(output_dir)//'/square_yy.rnd',NumCoord)
    
    !!Compute the scaled version of the square coordinates
    Lx=sqrt(2*pi*Numfluxes/aimag(tau))
    xx_scaled=Lx*(xx+real(tau,dp)*yy)
    yy_scaled=Lx*aimag(tau)*yy
    
    call print_coordinates(xx_scaled,yy_scaled,TRIM(output_dir)//'/coord_xx.rnd',TRIM(output_dir)//'/coord_yy.rnd',NumCoord)

  end subroutine GenerateAndSampleWaveFunction

  subroutine sample_choose_new_coordinate(i,Steps&
       ,randx,randy,randxold,randyold,Numfluxes,tau,LogScale,Psi,PsiOld&
       ,WaveFunction,Accepted)
    integer, intent(IN) :: NumFluxes,steps
    complex(KIND=dpc), intent(IN) :: tau
    logical,intent(In)  :: LogScale
    ! initialize the chiral wave functions
    
    !integer, intent(IN) :: StartTime
    integer, intent(INOUT) :: i,Accepted
    COMPLEX(dpc), intent(INOUT) :: Psi, PsiOld
    REAL(dp), dimension(NumCoord):: randx,randy
    REAL(dp), dimension(NumCoord):: randxOld,randyOld
    interface
       function WaveFunction(rx,ry,tau,Xqps,Yqps)
         use typedef !!Needed for the wave function to know typedefs
         complex(kind=dpc) WaveFunction
         real(kind=dp), intent(In), dimension(:) :: rx, ry
         complex(kind=dpc), intent(IN)  :: tau
         real(KIND=dp), optional, intent(In), dimension(:) :: Xqps,Yqps
       end function WaveFunction
    end interface

    
    call write_progress(i,Steps)
    call Move_square_coordinates(randx,randy,randxold,randyold&
         ,Numfluxes,tau)
    Psi=WaveFunction(randx,randy,tau)
    if(LogScale)then
       call AcceptNewNumberLog(psi,psiold,randxOld,randx,randyOld&
            ,randy,Accepted)
    else
       call AcceptNewNumber(psi,psiold,randxOld,randx,randyOld&
            ,randy,Accepted)
    end if
  end subroutine sample_choose_new_coordinate

  
  subroutine AcceptNewNumber(psi,psiold,randxOld,randx,randyOld,randy,Accepted)
    REAL(dp), DIMENSION(:) :: randxOld,randyOld
    REAL(dp), DIMENSION(:) :: randx,randy
    Complex(dpc) :: psi, psiOld
    REAL(dp) abspsi, abspsiold,tmp
    INTEGER Accepted
    abspsi=abs(psi)
    abspsiOld=abs(psiOld)
    call RANDOM_NUMBER(tmp)
    if ((abspsi/abspsiold)**2.gt.tmp) then
       abspsiOld = abspsi
       randxOld=randx
       randyOld=randy
       PsiOld=Psi
       Accepted=Accepted+1
    end if
  end subroutine AcceptNewNumber

  subroutine AcceptNewNumberLog(psi,psiold,randxOld,randx,randyOld,randy,Accepted)
    REAL(dp), DIMENSION(:) :: randxOld,randyOld
    REAL(dp), DIMENSION(:) :: randx,randy
    Complex(dpc) :: psi, psiOld
    REAL(dp) abspsi, abspsiold,tmp
    INTEGER Accepted
    abspsi=real(psi)
    abspsiOld=real(psiOld)
    call RANDOM_NUMBER(tmp)
    if ((exp(abspsi-abspsiold))**2.gt.tmp) then
       abspsiOld = abspsi
       randxOld=randx
       randyOld=randy
       PsiOld=Psi
       Accepted=Accepted+1
    end if
  end subroutine AcceptNewNumberLog


  subroutine print_wave_function(WaveFunc,RealDir,ImagDir)
    complex(KIND=dpc), INTENT(IN):: WaveFunc(Nsamples)
    integer ::  k, ch_re=591,ch_im=592
    character(Len=*) :: RealDir , ImagDir
    
    write(*,*) 'Printing.....'
    write(*,*) 'Write real part to ', RealDir, ' and imaginary part to ' ,ImagDir, ':'    
    
    OPEN(ch_re,FILE=TRIM(RealDir),STATUS='unknown')
    !Write real part first
    do k = 1,Nsamples
       write(ch_re,*) Real(WaveFunc(k),dp)
    end do
    close(ch_re)
    
    OPEN(ch_im,FILE=TRIM(ImagDir),STATUS='unknown')
    !Then write imaginary part
    do k = 1,Nsamples
       write(ch_im,*) Aimag(WaveFunc(k))
    end do
    close(ch_im)
    
  end subroutine print_wave_function

  subroutine print_coordinates(Coord_xx,Coord_yy,DirX,DirY,nele)
    real(KIND=dp), INTENT(IN):: Coord_xx(nele,Nsamples),Coord_yy(nele,Nsamples)
    integer, INTENT(IN) :: nele
    integer ::  k, ch_xx=593,ch_yy=594
    character(Len=*) :: DirX , DirY
    
    write(*,*) 'Printing.....'
    write(*,*) 'Write x-coordinates to ', DirX, ' and y-coordinates to ' ,DirY, ':'    
    
    OPEN(ch_xx,FILE=TRIM(DirX),STATUS='unknown')
    !Write real part first
    do k = 1,Nsamples
       write(ch_xx,*) Coord_xx(:,k)
    end do
    close(ch_xx)
    
    OPEN(ch_yy,FILE=TRIM(DirY),STATUS='unknown')
    !Then write imaginary part
    do k = 1,Nsamples
       write(ch_yy,*) Coord_yy(:,k)
    end do
    close(ch_yy)
    
  end subroutine print_coordinates

  
END MODULE DiscretizedWaveFunctions
