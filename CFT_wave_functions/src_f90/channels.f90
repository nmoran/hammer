MODULE Channels

  ! Module to define symbolic names for common I/O channels.

  ! $Id: Channels.f90,v 1.1 2003/11/25 05:24:41 sps Exp $
  ! 1999-2003 by Sami.Siljamaki@iki.fi

  IMPLICIT NONE
  
  ! These are universal(?):
  INTEGER, PARAMETER ::  STDIN = 5
  INTEGER, PARAMETER :: STDOUT = 6
  INTEGER, PARAMETER :: STDERR = 0

CONTAINS

  SUBROUTINE init_Channels() ! nothing to do
  END SUBROUTINE init_Channels

END MODULE Channels
