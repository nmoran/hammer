MODULE fock_basis

  USE typedef
  use bits_manipulation
  ! Fermionic fock basis

  IMPLICIT NONE

  INTEGER(kind=8),allocatable :: bbasis(:)  ! binary rep of Fock basis
  INTEGER(kind=8),allocatable :: rbasis(:)  ! binary rep of Fock basis (reverse)
  INTEGER,allocatable :: ibasis(:,:)        ! integer rep
  INTEGER :: Basis_Size
  
  private
  public make_basis
  public bbasis
  public rbasis
  public ibasis
  public reverse_bits
  public lookup_basis_num
  public write_bits

CONTAINS

  ! **********
  ! Fock basis
  ! **********

  integer FUNCTION make_basis(N1,Ne) RESULT(N)
    ! Generates a Fock basis as combinations single-particle states.
    ! States can be filtered in the function check_qns( state ).
    ! Basis is stored both in integer and binary representation.
    USE Combinations
    USE quantum_numbers
    INTEGER, INTENT(IN) :: N1, Ne
    INTEGER :: i
    TYPE(Combination) :: P
    REAL :: time_start, time_stop
    
    CALL cpu_time(time_start)
    WRITE(*,'(/,T5,A,/)') 'Creating Fock basis...'
    
    IF (N1 > BIT_SIZE(bbasis(1)))then
       write(*,*) 'N1 too large. idiot.'
       call exit(-1)
    end IF
    
    CALL createCombination(P)
    CALL initializeCombination(P,N1,Ne)
    N=0
    DO ! First, let's find the number of basis states
      IF ( .NOT. P%ok ) EXIT
      ! This checks that the quantum numbers make up good states:
      IF ( check_qns( P%C-1 )  ) THEN
        N=N+1
      END IF
      CALL nextCombination(P)
    END DO
    WRITE(*,*) ' Many-body basis size is ', N
    WRITE(*,*) ' '

    ALLOCATE( ibasis(N, Ne), bbasis(N) ,rbasis(N))
    bbasis = 0

    N=0

    CALL initializeCombination(P,N1,Ne)
   ! going again over the basis, this time storing the occupations 
    DO 
       IF ( .NOT. P%ok ) EXIT
       IF ( check_qns( P%C-1 )  ) THEN
          N=N+1
          ibasis(N,:) = P%C-1                  ! integer rep
          bbasis(N)=list_to_bit(P%C,Ne)  !!Binary representations
          !write(*,*) 'Ibasis:',ibasis(N,:)
          !write(*,*) 'bbasis:',bbasis(N)
       END IF
       CALL nextCombination(P)
    END DO

    CALL cpu_time(time_stop)
    WRITE(*,*) 'Took ', time_stop-time_start, ' seconds to construct basis'


    !!Also contruct the reverse basis (for reverse lookup)
    do i=1,N !!Loop over the basis
       !write(*,*) 'B-Bitnums', bbasis(i)
       !call write_bits(bbasis(i),working_ns)
       rbasis(i)=reverse_bits(bbasis(i),working_ns)
       !write(*,*) 'R-Bitnums', rbasis(i)
       !call write_bits(rbasis(i),working_ns)
    end do
    
    Basis_Size=N

  END FUNCTION make_basis



  integer function lookup_basis_num(rbase) result(position)
    integer(kind=8), intent(in) ::rbase
    position=search_list_decending_integer8(rbase,rbasis,basis_size)
  end function lookup_basis_num

END MODULE fock_basis
