program test_k_matrix
  
  USE typedef     ! types and definitions
  use k_matrix
  use test_utilities
  
  
  !!Variables
  IMPLICIT NONE  
  
  !!test begins!!
    write(*,*) '         Test the kappa-matrix algebra'
    !! Testing the kappa-matrix function
    call test_is_jain_K_matrix  
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_is_other_K_matrix  
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_is_not_K_matrix  
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_group_sizes
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_Q_R_C_lists
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_chiral_C_matrix
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_charge_lattice
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_null_and_dual_lattice
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
contains 
  
  
  !!------------------------
  !!       Test of kappa_matrix algebra
  !!-------------------------
  
  subroutine test_is_jain_K_matrix()
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer :: D
    
    write(*,*) 'Test that Jain K-matrices can be identified'
    do D=1,10
       ALLOCATE(kappa_matrix(D,D))
       !!Contruct the kappa-matrix
       call hierarchy_k_matrix(D,kappa_matrix)
       !!Test it
       call error_test_kappa(D,kappa_matrix)
       DEALLOCATE(kappa_matrix)
    end do
  end subroutine test_is_jain_K_matrix
  
  
  subroutine test_is_other_K_matrix()
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer :: n1,n2,n3,n4,D
    
    write(*,*) 'Test that non-Jain K-matrices can be identified'
    
    
    D=1    
    ALLOCATE(kappa_matrix(D,D))
    do n1=1,3
       call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1/))
       call error_test_kappa(D,kappa_matrix)
    end do
    DEALLOCATE(kappa_matrix)
    
    D=2    
    ALLOCATE(kappa_matrix(D,D))
    do n1=1,3
       do n2=0,2
          call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2/))
          call error_test_kappa(D,kappa_matrix)
       end do
    end do
    DEALLOCATE(kappa_matrix)
    
    D=3
    ALLOCATE(kappa_matrix(D,D))
    do n1=1,3
       do n2=0,2
          do n3=0,2
             call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2,n3/))
             call error_test_kappa(D,kappa_matrix)
          end do
       end do
    end do
    DEALLOCATE(kappa_matrix)
    
    D=4
    ALLOCATE(kappa_matrix(D,D))
    do n1=2,4
       do n2=0,2
          do n3=0,2
             do n4=0,2
                call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2,n3,n4/))
                call error_test_kappa(D,kappa_matrix)
             end do
          end do
       end do
    end do
    DEALLOCATE(kappa_matrix)
    
  end subroutine test_is_other_K_matrix
  
  subroutine test_is_not_K_matrix()
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    
    write(*,*) 'Test that non-K-matrices can be identified'
    ALLOCATE(kappa_matrix(2,2))
    kappa_matrix=reshape((/2,0,0,2/),(/2,2/))
    call error_test_not_kappa(2,kappa_matrix)
    
    kappa_matrix=reshape((/3,1,2,3/),(/2,2/))
    call error_test_not_kappa(2,kappa_matrix)
    DEALLOCATE(kappa_matrix)
    
    ALLOCATE(kappa_matrix(3,3))
    kappa_matrix=reshape((/3,2,2,2,3,2,2,2,-3/),(/3,3/))
    call error_test_not_kappa(3,kappa_matrix)
    
    kappa_matrix=reshape((/5,4,4,4,3,2,4,2,5/),(/3,3/))
    call error_test_not_kappa(3,kappa_matrix)
    
    DEALLOCATE(kappa_matrix)
    
  end subroutine test_is_not_K_matrix
  
  subroutine test_group_sizes()
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix
    integer, ALLOCATABLE, DIMENSION(:) :: plist,pcalc
    integer :: n1,n2,n3,n4,D,q_group,p_group,p_fac,q_fac
    
    write(*,*) 'Test that correct group size can be computed'
    
    D=1    
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(plist(D))
    ALLOCATE(pcalc(D))
    do n1=1,3
       call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1/))
       call hierarchy_group_sizes(D,plist,(/n1/))
       call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
       call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
       call hierarchy_fraction(D,p_fac,q_fac,(/n1/))
       call test_integers(D,kappa_matrix,q_fac,q_group,'q')
       call test_integers(D,kappa_matrix,p_fac,p_group,'p')
    end do
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist)
    DEALLOCATE(pcalc)
    
    
    D=2    
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(plist(D))
    ALLOCATE(pcalc(D))
    do n1=1,3
       do n2=0,2
          call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2/))
          call hierarchy_group_sizes(D,plist,(/n1,n2/))
          call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
          call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
          call hierarchy_fraction(D,p_fac,q_fac,(/n1,n2/))
          call test_integers(D,kappa_matrix,q_fac,q_group,'q')
          call test_integers(D,kappa_matrix,p_fac,p_group,'p')
       end do
    end do
    
    !!Some non-hierarchy K-matrices
    
    kappa_matrix=reshape((/2,0,0,2/),(/2,2/))
    plist=(/1,1/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
    
    
    kappa_matrix=reshape((/3,1,1,3/),(/2,2/))
    plist=(/1,1/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
    
    kappa_matrix=reshape((/3,-1,-1,3/),(/2,2/))
    plist=(/1,1/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
    
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist)
    DEALLOCATE(pcalc)
    
    D=3
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(plist(D))
    ALLOCATE(pcalc(D))
    do n1=1,3
       do n2=0,2
          do n3=0,2
             call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2,n3/))
             call hierarchy_group_sizes(D,plist,(/n1,n2,n3/))
             call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
             call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
             call hierarchy_fraction(D,p_fac,q_fac,(/n1,n2,n3/))
             call test_integers(D,kappa_matrix,q_fac,q_group,'q')
             call test_integers(D,kappa_matrix,p_fac,p_group,'p')
          end do
       end do
    end do
    
    !!Some non-hierarchy K-matrices
    kappa_matrix=reshape((/2,0,0,0,2,0,0,0,2/),(/3,3/))
    plist=(/1,1,1/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')

    !!Some non-hierarchy K-matrices
    kappa_matrix=reshape((/0,0,1,0,1,0,1,0,0/),(/3,3/))
    plist=(/1,1,1/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')

    !!Some non-hierarchy K-matrices
    kappa_matrix=reshape((/0,1,0,0,0,1,1,0,0/),(/3,3/))
    plist=(/1,1,1/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')

    !!Some non-hierarchy K-matrices
    kappa_matrix=reshape((/0,0,1,0,2,0,1,0,0/),(/3,3/))
    plist=(/2,1,2/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
        
    kappa_matrix=reshape((/1,0,0,0,2,0,0,0,2/),(/3,3/))
    plist=(/2,1,1/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
    
    kappa_matrix=reshape((/1,0,0,0,2,0,0,0,3/),(/3,3/))
    plist=(/6,3,2/)
    call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
    call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
    
    
    
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist)
    DEALLOCATE(pcalc)
    
    D=4
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D))
    ALLOCATE(plist(D))
    ALLOCATE(pcalc(D))
    do n1=2,4
       do n2=0,2
          do n3=0,2
             do n4=0,2
                call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2,n3,n4/))
                call hierarchy_group_sizes(D,plist,(/n1,n2,n3,n4/))
                call kappa_matrix_group_sizes(D,kappa_matrix,pcalc,p_group,q_group)
                call test_integer_lists(D,plist,pcalc,kappa_matrix,'p')
                call hierarchy_fraction(D,p_fac,q_fac,(/n1,n2,n3,n4/))
                call test_integers(D,kappa_matrix,q_fac,q_group,'q')
                call test_integers(D,kappa_matrix,p_fac,p_group,'p')
             end do
          end do
       end do
    end do
    DEALLOCATE(kappa_matrix)
    DEALLOCATE(plist)
    DEALLOCATE(pcalc)
    
  end subroutine test_group_sizes
  
  
  
  subroutine test_Q_R_C_lists()
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,CmatTrans
    integer, ALLOCATABLE, DIMENSION(:) :: Qlist,Rlist,Tlist
    integer, ALLOCATABLE, DIMENSION(:,:) :: QlistF,RlistF
    integer :: n1,n2,n3,step,D
    
    write(*,*) 'Test that correct Q-, R-,C- lists can be generated'
    !!We do not test the T-list
    
    !!Trivial Q=R=K=C
    D=1    
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatTrans(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D))
    do n1=1,10
       kappa_matrix=reshape((/n1/),(/D,D/))
       call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,Rlist)
       call test_integer_lists(D,(/n1/),Qlist(1:D),kappa_matrix,'Q')
       call test_integer_lists(D,(/n1/),Rlist,kappa_matrix,'R')
       call Q_list_to_CmatTrans(D,Qlist,CmatTrans)
       call test_integer_lists(D,(/n1/),CmatTrans(1,1),kappa_matrix,'C')
    end do
    DEALLOCATE(kappa_matrix,CmatTrans)
    DEALLOCATE(Qlist,Rlist,Tlist)
    
    !!Here we make some examples by hand
    D=2
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatTrans(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D))
    ALLOCATE(QlistF(D,4),RlistF(D,4))
    
    !!Set facit for some simple test cases
    QlistF=reshape((/3,5,3,11,5,9,5,19/),(/D,4/))
    RlistF=reshape((/3,15,3,33,5,45,5,95/),(/D,4/))
    
    step=1
    do n1=3,5,2
       do n2=0,2,2
          call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2/))
          call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,Rlist)
          call test_integer_lists(D,QlistF(:,step),Qlist(1:D),kappa_matrix,'Q')
          call test_integer_lists(D,RlistF(:,step),Rlist,kappa_matrix,'R')
          step=step+1
       end do
    end do
    DEALLOCATE(kappa_matrix,CmatTrans)
    DEALLOCATE(Qlist,Rlist,Tlist)
    DEALLOCATE(QlistF,RlistF)
    
    
    !!Here we make some examples by hand
    D=3
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatTrans(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D))
    ALLOCATE(QlistF(D,8),RlistF(D,8))
    
    !!Set facit for some simple test cases
    QlistF=reshape((/3,5,7,&
         3,5,17,&
         3,11,19,&
         3,11,41,&
         5,9,13,&
         5,9,31,&
         5,19,33,&
         5,19,71/),(/D,8/))
    RlistF=reshape((/3,15,35,&
         3,15,85,&
         3,33,209,&
         3,33,451,&
         5,45,117,&
         5,45,279,&
         5,95,627,&
         5,95,1349/),(/D,8/))
    
    step=1
    do n1=3,5,2
       do n2=0,2,2
          do n3=0,2,2
             call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2,n3/))
             call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,Rlist)
             call test_integer_lists(D,QlistF(:,step),Qlist(1:D),kappa_matrix,'Q')
             call test_integer_lists(D,RlistF(:,step),Rlist,kappa_matrix,'R')
             step=step+1
          end do
       end do
    end do
    DEALLOCATE(kappa_matrix,CmatTrans)
    DEALLOCATE(Qlist,Rlist,Tlist)
    DEALLOCATE(QlistF,RlistF)
    
  end subroutine test_Q_R_C_lists
  
  
  
  subroutine test_chiral_C_matrix()
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,CmatTrans
    integer, ALLOCATABLE, DIMENSION(:) :: Qlist,Rlist,Tlist
    integer :: n1,D
    
    write(*,*) 'Test that correct C-matrix can be generated'
    !!We do not test the T-list
    
    !!Trivial Q=R=K=C
    D=2
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatTrans(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D))
    !!Diagonal ones here!!
    do n1=1,10
       call gen_hierarchy_k_matrix(D,kappa_matrix,(/1,n1-1/))
       call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,Rlist)
       call Q_list_to_CmatTrans(D,Qlist,CmatTrans)
       !!tests
       call test_integer_lists(D,(/1,1/),Qlist(0:(D-1)),kappa_matrix,'q')
       call test_integer_lists(D,(/1,n1/),Qlist(1:D),kappa_matrix,'Q')
       call test_integer_matrix(D,reshape((/1,0,0,n1/),(/D,D/))&
            ,CmatTrans,kappa_matrix,'C')
    end do
    DEALLOCATE(kappa_matrix,CmatTrans)
    DEALLOCATE(Qlist,Rlist,Tlist)
    
    D=3
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatTrans(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D))
    !!Diagonal ones here!!
    do n1=1,10
       call gen_hierarchy_k_matrix(D,kappa_matrix,(/1,0,n1-1/))
       call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,Rlist)
       call Q_list_to_CmatTrans(D,Qlist,CmatTrans)
       !!tests
       call test_integer_lists(D,(/1,1,1/),Qlist(0:(D-1)),kappa_matrix,'q')
       call test_integer_lists(D,(/1,1,n1/),Qlist(1:D),kappa_matrix,'Q')
       call test_integer_matrix(D,reshape((/1,0,0,0,1,0,0,0,n1/),(/D,D/))&
            ,CmatTrans,kappa_matrix,'C')
    end do
    DEALLOCATE(kappa_matrix,CmatTrans)
    DEALLOCATE(Qlist,Rlist,Tlist)
    
    
  end subroutine test_chiral_C_matrix
  
  
  
  
  subroutine test_charge_lattice()
    integer, ALLOCATABLE, DIMENSION(:,:) :: kappa_matrix,Cmat,CmatF,Lmat,LmatF
    integer, ALLOCATABLE, DIMENSION(:) :: Qlist,Rlist,Tlist,RListF
    integer :: n1,n2,n3,n4,D
    
    write(*,*) 'Test that the charge lattice is correcly computed'
    
    D=1    
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatF(D,D),Cmat(D,D),LmatF(D,D),Lmat(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D),RlistF(D))
    do n1=1,5
       !!Setup the known varaibles
       call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1/))
       call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,RlistF)
       call Q_list_to_CmatTrans(D,Qlist,CmatF)
       call Q_list_to_Lmatrix(D,Qlist,LmatF)
       CmatF=transpose(CmatF)
       !!Compute the charge lattice
       call kappa_charge_lattice(D,kappa_matrix,Cmat,Rlist)
       call test_charge_lattice_is_correct(D,Cmat,Rlist,CmatF,RlistF,kappa_matrix)

       !!Compute the inverse lattice
       call charge_lattice_to_inverse_lattice(D,Cmat,Rlist,Lmat)
       !!Test
       call test_integer_lists(D,RlistF,Rlist,kappa_matrix,'R')
       call test_integer_matrix(D,CmatF,Cmat,kappa_matrix,'C')
       call test_integer_matrix(D,LmatF,Lmat,kappa_matrix,'L')
    end do
    DEALLOCATE(kappa_matrix,CmatF,Cmat,Lmat,LmatF)
    DEALLOCATE(Qlist,Rlist,Tlist,RlistF)
    
    D=2    
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatF(D,D),Cmat(D,D),LmatF(D,D),Lmat(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D),RlistF(D))
    do n1=1,5
       do n2=0,3
          !!Setup the known varaibles
          call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2/))
          call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,RlistF)
          call Q_list_to_CmatTrans(D,Qlist,CmatF)
          call Q_list_to_Lmatrix(D,Qlist,LmatF)
          CmatF=transpose(CmatF)
          !!Compute the charge lattice
          call kappa_charge_lattice(D,kappa_matrix,Cmat,Rlist)
          call test_charge_lattice_is_correct(D,Cmat,Rlist,CmatF,RlistF,kappa_matrix)

          !!Compute the inverse lattice
          call charge_lattice_to_inverse_lattice(D,Cmat,Rlist,Lmat)
          !!Test
          call test_integer_lists(D,RlistF,Rlist,kappa_matrix,'R')
          call test_integer_matrix(D,CmatF,Cmat,kappa_matrix,'C')
          call test_integer_matrix(D,LmatF,Lmat,kappa_matrix,'L')
       end do
    end do
    DEALLOCATE(kappa_matrix,CmatF,Cmat,Lmat,LmatF)
    DEALLOCATE(Qlist,Rlist,Tlist,RlistF)
    
    D=3    
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatF(D,D),Cmat(D,D),LmatF(D,D),Lmat(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D),RlistF(D))
    do n1=1,5
       do n2=0,3
          do n3=0,3
             !!Setup the known varaibles
             call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2,n3/))
             call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,RlistF)
             call Q_list_to_CmatTrans(D,Qlist,CmatF)
             call Q_list_to_Lmatrix(D,Qlist,LmatF)
             CmatF=transpose(CmatF)
             !!Compute the charge lattice
             call kappa_charge_lattice(D,kappa_matrix,Cmat,Rlist)
             call test_charge_lattice_is_correct(D,Cmat,Rlist,CmatF,RlistF,kappa_matrix)

             !!Compute the inverse lattice
             call charge_lattice_to_inverse_lattice(D,Cmat,Rlist,Lmat)
             !!Test
             call test_integer_lists(D,RlistF,Rlist,kappa_matrix,'R')
             call test_integer_matrix(D,CmatF,Cmat,kappa_matrix,'C')
             call test_integer_matrix(D,LmatF,Lmat,kappa_matrix,'L')
          end do
       end do
    end do
    
    
    !!Some non-hierarchy K-matrices
    !!(that had problems with denominator being different, and thus crashed)
    kappa_matrix=reshape((/4,3,2,3,4,2,2,2,4/),(/3,3/))
    call kappa_charge_lattice(D,kappa_matrix,Cmat,Rlist)
    !!FIXME use the commented test to chech for best reduction
    !!call test_integer_matrix(D,reshape((/4,3,2,0,7,2,0,0,10/),(/3,3/)),Cmat,kappa_matrix,'C')
    call test_charge_lattice_is_correct(D,Cmat,Rlist,&
         reshape((/4,3,2,0,7,2,0,0,20/),(/3,3/)),(/4,28,140/),kappa_matrix)
    
    call test_integer_matrix(D,reshape((/4,3,2,0,7,2,0,0,10/),(/3,3/)),Cmat,kappa_matrix,'C')
    !!FIXME use the commented test to chech for best reduction
    !!call test_integer_lists(D,(/4,28,35/),Rlist,kappa_matrix,'R')
    call test_integer_lists(D,(/4,28,35/),Rlist,kappa_matrix,'R')
    DEALLOCATE(kappa_matrix,CmatF,Cmat,Lmat,LmatF)
    DEALLOCATE(Qlist,Rlist,Tlist,RlistF)
    
    
    D=4    
    !write(*,*) 'D=',D
    ALLOCATE(kappa_matrix(D,D),CmatF(D,D),Cmat(D,D),LmatF(D,D),Lmat(D,D))
    ALLOCATE(Qlist(0:D),Rlist(D),Tlist(D),RlistF(D))
    do n1=1,5
       do n2=0,3
          do n3=0,3
             do n4=0,3
                !!Setup the known varaibles
                call gen_hierarchy_k_matrix(D,kappa_matrix,(/n1,n2,n3,n4/))
                call K_matrix_to_T_Q_R_list(kappa_matrix,D,Tlist,Qlist,RlistF)
                call Q_list_to_CmatTrans(D,Qlist,CmatF)
                call Q_list_to_Lmatrix(D,Qlist,LmatF)
                CmatF=transpose(CmatF)
                !!Compute the charge lattice
                call kappa_charge_lattice(D,kappa_matrix,Cmat,Rlist)
                call test_charge_lattice_is_correct(D,Cmat,Rlist,CmatF,RlistF,kappa_matrix)
                !!Compute the inverse lattice
                call charge_lattice_to_inverse_lattice(D,Cmat,Rlist,Lmat)
                !!Test
                call test_integer_lists(D,RlistF,Rlist,kappa_matrix,'R')
                call test_integer_matrix(D,CmatF,Cmat,kappa_matrix,'C')
                call test_integer_matrix(D,LmatF,Lmat,kappa_matrix,'L')
             end do
          end do
       end do
    end do
    DEALLOCATE(kappa_matrix,CmatF,Cmat,Lmat,LmatF)
    DEALLOCATE(Qlist,Rlist,Tlist,RlistF)
    
    
  end subroutine test_charge_lattice
  

  subroutine test_charge_lattice_is_correct(D,Cmat,Rlist,CmatF,RlistF,kappa_matrix)
    integer,intent(IN) :: D,Cmat(D,D),Rlist(D),CmatF(D,D),RlistF(D),kappa_matrix(D,D)
    integer :: Cred,Rred, m1,m2,Cfac,Rfac
    !!Check that the charge lattice is correct
    !!Even though the radii mith be eroneously scaled
    do m1=1,D
       do m2=1,D
          call do_reduce_sqrt_fraction(Cmat(m1,m2),Rlist(m2),Cred,Rred)
          call do_reduce_sqrt_fraction(CmatF(m1,m2),RlistF(m2),Cfac,Rfac)
          if(Cred.ne.Cfac.or.Rred.ne.Rfac)then
             write(*,*) 'ERROR:'
             write(*,*) 'For kappa-matrix:'
             call print_int_matrix(kappa_matrix)
             write(*,*) 
             write(*,*) 'The computed fraction does not match facit:'
             write(*,*) 'They are computed:'
             call print_int_matrix(Cmat)
             write(*,*) 'Rlist:',Rlist
             write(*,*) 'They are Facit:'
             call print_int_matrix(CmatF)
             write(*,*) 'RlistF:',RlistF
             write(*,*) 
             write(*,*) 'Especially (m,n)=',m1,m2
             write(*,*) '(Cred,Cfac)=',Cred,Cfac
             write(*,*) '(Rred,Rfac)=',Rred,Rfac
             call exit(-2)
          end if
       end do
    end do
  end subroutine test_charge_lattice_is_correct
  


  subroutine test_null_and_dual_lattice()
    !!This functions test that dual an null lattices can be gerarated, and test that they have internal consisteny
    integer,parameter :: Groups=2
    integer :: Cmatrix(Groups,Groups),Cbar(Groups,Groups),Rlist(Groups),Rbar(Groups)
    write(*,*) 'Test that dual and null lattices can be computed and evaluated'
    !!Initialize a nu=2/3 charge lattice
    Cmatrix=reshape((/3,2,0,5/),(/Groups,Groups/))
    Cbar=reshape((/2,0,0,2/),(/Groups,Groups/))
    Rlist=(/3,15/)
    Rbar=(/2,2/)
    call    do_compute_dual_lattice(Groups,Cmatrix,Cbar,Rlist,Rbar)
  end subroutine test_null_and_dual_lattice

  
  subroutine do_compute_dual_lattice(Groups,Cmatrix,Cbar,Rlist,Rbar)
    !!Compute the dual lattice of the non-chiral charge lattice,
    !!This has two kinds of vectors, the dual vectors, and null-vectors
    !!We first compute a set of change vectors and a set of null-vectors
    !!These will not all be othogonal, so next we orthogonalize
    integer, intent(IN) :: Groups,Cmatrix(Groups,Groups),Cbar(Groups,Groups)
    integer, intent(IN) :: Rlist(Groups),Rbar(Groups)
    
    integer :: Ccharge(Groups,2*Groups),Rcharge(2*Groups)
    integer :: Cdual(Groups,2*Groups),Cnull(Groups,2*Groups)
    integer :: Cdualnull(2*Groups,2*Groups),Rdualnull(2*Groups,2*Groups)
    integer :: Rdual(Groups,2*Groups),Rnull(Groups,2*Groups)
    integer :: tmpMat(Groups,Groups)
    integer :: n
    logical :: SpaceLikeNull(Groups)
    
    
    !!Compute the naive null-vectors
    
    call charge_lattice_to_null_lattice(Groups,Cmatrix,Cbar,Rlist,Rbar,&
         Cdualnull,Rdualnull)
    !!Extract the dual and null parts
    do n=1,Groups
       Cdual(n,:)=Cdualnull(n,:)
       Cnull(n,:)=Cdualnull(n+Groups,:)
       Rdual(n,:)=Rdualnull(n,:)
       Rnull(n,:)=Rdualnull(n+Groups,:)
       !!The joint charge lattice
       Ccharge(n,1:groups)=Cmatrix(n,:)
       Ccharge(n,(groups+1):(2*groups))=Cbar(n,:)
       Rcharge(1:groups)=Rlist(:)
       Rcharge((groups+1):(2*groups))=Rbar(:)
    end do
    
    !write(*,*) 
    !write(*,*) 'The joint charge lattice'
    !call print_int_rec_matrix(Groups,2*Groups,Ccharge)
    !write(*,*) "wich compactification radius"
    !write(*,*) Rcharge

    !write(*,*) 
    !write(*,*) 'Naive Dual lattice'
    !call print_int_rec_matrix(Groups,2*Groups,Cdual)
    !write(*,*) "wich compactification radius"
    !call print_int_rec_matrix(Groups,2*Groups,Rdual)
    
    !write(*,*) 
    !write(*,*) 'Naive Null lattice'
    !call print_int_rec_matrix(Groups,2*Groups,Cnull)
    !write(*,*) "wich compactification radius"
    !call print_int_rec_matrix(Groups,2*Groups,Rnull)


    !!Sanity check null dual vectors have no null vectors components!!
    call test_lorenz_orthogonal_charge(Groups,Cnull,Rnull,Ccharge,Rcharge,'  null','charge')
    tmpMat=kroenecker_matrix(Groups)
    !!Sanity check dual vectors have right properties!!
    call test_lorenz_matrix_charge(Groups,Cdual,Rdual,Ccharge,Rcharge,'  dual','charge',tmpMat)
    
    call orthonormalize_then_null_vectors(Groups,Cnull,Rnull,SpaceLikeNull)
    
    
    !!Summarize: Orthoginalized null vectors  are 
    !write(*,*)
    !write(*,*) ' True null lattice'
    !call print_int_rec_matrix(Groups,2*Groups,Cnull)
    !write(*,*) "wich compactification radius"
    !call print_int_rec_matrix(Groups,2*Groups,Rnull)
    
    !!Sanity check - null vectros are orthogonal!!
    !!call test_lorenz_orthogonal_internal(Groups,Cnull,Rnull,'null')
    !!Sanity check - null vectors are still nullvectors!!
    !!call test_lorenz_orthogonal_charge(Groups,Cnull,Rnull,Ccharge,Rcharge,'  null','charge')
    
    call remove_null_from_dual_vectors(Groups,Cnull,Rnull,SpaceLikeNull,Cdual,Rdual)    
    
    !!Summarize: Dual vectors with null-vectors removed 
    !!write(*,*)
    !!write(*,*) 'True Dual lattice'
    !!call print_int_rec_matrix(Groups,2*Groups,Cdual)
    !!write(*,*) "wich compactification radius"
    !!call print_int_rec_matrix(Groups,2*Groups,Rdual)


    !!Sanity check null dual vectors have no null vectors components!!
    call test_lorenz_orthogonal_external(Groups,Cnull,Rnull,Cdual,Rdual,'  null','  dual')
    

    tmpMat=kroenecker_matrix(Groups)
    !!Sanity check dual vectors have right properties!!
    call test_lorenz_matrix_charge(Groups,Cdual,Rdual,Ccharge,Rcharge,'  dual','charge',tmpMat)
        
  end subroutine do_compute_dual_lattice
  
  
  !!------------------------
  !!       Auxiallry functions
  !!-------------------------
  
  
  subroutine hierarchy_k_matrix(D,kappa_matrix)
    integer, intent(in) :: D
    integer :: kappa_matrix(D,D),Iter1,Iter2
    do Iter1=1,D
       do Iter2=1,D
          If(Iter1.eq.Iter2)then
             kappa_matrix(Iter1,Iter2)=3
          else
             kappa_matrix(Iter1,Iter2)=2
          end If
       end do
    end do
  end subroutine hierarchy_k_matrix
  
  subroutine hierarchy_group_sizes(D,plist,Slist)
    !!Constructs generic chiral group-size from the Slist
    integer, intent(in) :: D,Slist(D)
    integer :: N,M,preverse(D),sreverse(D),plist(D)
    
    !!Reverse Slist
    do N=1,D
       sreverse(N)=slist(D-N+1)
    end do
    
    !!Construct the reverse p-list
    preverse(1)=1
    do N=2,D
       preverse(N)=preverse(N-1)
       do M=1,(N-1)
          preverse(N)=preverse(N)+sreverse(N-1)*preverse(M)
       end do
    end do
    
    !!Reverse plist
    do N=1,D
       plist(N)=preverse(D-N+1)
    end do
    
  end subroutine hierarchy_group_sizes
  
  
  subroutine hierarchy_fraction(D,p,q,Slist)
    !!Constructs generic chiral group-size from the Slist
    integer, intent(in) :: D,Slist(D) 
    integer, intent(out) :: p,q
    integer :: n,tlist(D),treverse(D)
    !!write(*,*) 'slist:',slist
    tlist=slist+2
    !!The frist element is the laughlin parameter
    tlist(1)=slist(1)
    !!The coputatin is most easily not backwards
    do N=1,D
       treverse(N)=tlist(D-N+1)
    end do
    
    do n=1,D
       if(n.eq.1)then
          !!The root elements is always the inverse of the last t-value
          p=1
          q=treverse(1)
       else
          !! If D>1 the the procedure is repeted recursively
          call add_fractions(treverse(n),1,-p,1*q,q,p)
       end if
       !!write(*,*) 'Recursive fraction'
       !!write(*,*) p,'/',q
    end do
    
  end subroutine hierarchy_fraction
  
  
  
    subroutine error_test_kappa(D,kappa_matrix)
    integer, intent(in) :: D,kappa_matrix(D,D)
    if(.not.is_hierarchy_k_matrix(D,kappa_matrix))then
       write(*,*) 'Error: Matrix is NOT kappa_matrix:'
       call print_int_matrix(kappa_matrix)
       call exit(-2)
    end if
  end subroutine error_test_kappa
  
  
  subroutine error_test_not_kappa(D,kappa_matrix)
    integer, intent(in) :: D,kappa_matrix(D,D)
    if(is_hierarchy_k_matrix(D,kappa_matrix))then
       write(*,*) 'Error: Matrix is NOT kappa_matrix:'
       call print_int_matrix(kappa_matrix)
       call exit(-2)
    end if
  end subroutine error_test_not_kappa
  
  subroutine test_integers(D,kappa_matrix,int_facit,int_calc,Type)
    integer, intent(in) :: D,kappa_matrix(D,D),int_facit,int_calc
    character :: Type(1)
    if(int_facit.ne.int_calc)then
       write(*,*) '-----------Error--------'
       write(*,*) 'For kappa-matrix:'
       call print_int_matrix(kappa_matrix)
       write(*,*) Type,' facit:',int_facit
       write(*,*) 'does NOT match'
       write(*,*) Type,' calc: ',int_calc
       call exit(-2)
    end if
  end subroutine test_integers
  
  
  subroutine test_integer_lists(D,list_facit,list_calc,kappa_matrix,Type)
    integer, intent(in) :: D,kappa_matrix(D,D),list_facit(D),list_calc(D)
    integer :: diff(D)
    character :: Type(1)
    diff=list_facit-list_calc
    if(dot_product(diff,diff).gt.0)then
       write(*,*) '-----------Error--------'
       write(*,*) 'For kappa-matrix:'
       call print_int_matrix(kappa_matrix)
       write(*,*) Type,'-list facit:',list_facit
       write(*,*) 'does NOT match'
       write(*,*) Type,'-list calc: ',list_calc
       call exit(-2)
    end if
  end subroutine test_integer_lists
  
  
  subroutine test_integer_matrix(D,mat_facit,mat_calc,kappa_matrix,Type)
    integer, intent(in) :: D,kappa_matrix(D,D),mat_facit(D,D),mat_calc(D,D)
    character :: Type(1)
    integer n1,n2
    do n1=1,D
       do n2=1,D
          if(mat_facit(n1,n2).ne.mat_calc(n1,n2))then
             write(*,*) '-----------Error--------'
             write(*,*) 'For kappa-matrix:'
             call print_int_matrix(kappa_matrix)
             write(*,*)
             write(*,*) Type,'-matrix facit:'
             call print_int_matrix(mat_facit)
             write(*,*) 'does NOT match'
             write(*,*) Type,'-matrix calculated:'
             call print_int_matrix(mat_calc)
             call exit(-2)
          end if
       end do
    end do
  end subroutine test_integer_matrix
  
  
end program test_k_matrix

