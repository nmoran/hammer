module misc_matrix
  use typedef
  use misc_frac_sqrt

  !!module with usefull stuff for matrixes
  implicit none
  public

contains
  
  subroutine print_int_matrix(Mat)
    integer, intent(IN) :: Mat(:,:)
    call print_int_rec_matrix(Mat)
  end subroutine print_int_matrix
  
  subroutine print_cmplx_matrix(Size,Mat)
    integer, intent(IN) :: Size
    complex(KIND=dpc), intent(IN) :: Mat(Size,Size)
    call print_cmplx_rec_matrix(Size,Size,Mat)
  end subroutine print_cmplx_matrix
  
  
  subroutine print_int_rec_matrix(Mat)
    integer, intent(IN) :: Mat(:,:)
    integer :: n,m,SizeX,SizeY,maxvalue,digits
    character :: FMTSTRING(4)
    !!Dimentions of matrix
    SizeX=Size(Mat,1)
    SizeY=Size(Mat,2) 
    maxvalue=maxval(abs(Mat))
    !write(*,*) 'absolute max-value',maxvalue
    if(maxvalue.le.9)then
       digits=1
    else
       digits=floor(log10(1.0*maxvalue))+1
    end if
    !write(*,*) 'Need',digits,'digits'
    FMTSTRING(1)='('
    FMTSTRING(2)='I'
    FMTSTRING(3)='3'
    FMTSTRING(4)=')'
    select case(digits)
    case(1)
       !write(*,*) '1'
       FMTSTRING(3)='3'
    case(2)
       !write(*,*) '2'
       FMTSTRING(3)='4'
    case(3)
       !write(*,*) '3'
       FMTSTRING(3)='5'
    case(4)
       !write(*,*) '4'
       FMTSTRING(3)='6'
    case(5)
       !write(*,*) '5'
       FMTSTRING(3)='7'
    case(6)
       !write(*,*) '6'
       FMTSTRING(3)='8'
    case(7)
       !write(*,*) '7'
       FMTSTRING(3)='9'
    case default
       write(*,*) '----pirm....'
       write(*,*) 'ERROR: to large values'
       call exit(-1)
    end select
    !write(*,*) 'Use format:',FMTSTRING
    
    
    if(SizeX.eq.1)then
       write(*,'(A)',advance='NO') "("
       do m=1,SizeY
          write(*,FMTSTRING,advance='NO') Mat(1,m)
       end do
       write(*,'(A)') " )"
    else
       do n=1,SizeX
          if(n.eq.1)then
             write(*,'(A)',advance='NO') "/"
          elseif(n.eq.SizeX)then
             write(*,'(A)',advance='NO') "\"
          else
             write(*,'(A)',advance='NO') "|"
          end if
          do m=1,SizeY
             write(*,FMTSTRING,advance='NO') Mat(n,m)
          end do
          if(n.eq.1)then
             write(*,'(A)') " \"
          elseif(n.eq.SizeX)then
             write(*,'(A)') " /"
          else
             write(*,'(A)') " |"
          end if
          
       end do
    end if
  end subroutine print_int_rec_matrix
  
  subroutine print_int_vector(Vec)
    integer, intent(IN) :: Vec(:)
    write(*,*) '(',Vec,')' !!Print with parenthsis around
  end subroutine print_int_vector
  

  subroutine print_cmplx_rec_matrix(SizeX,SizeY,Mat)
    integer, intent(IN) :: SizeX,SizeY
    complex(kind=dpc), intent(IN) :: Mat(SizeX,SizeY)
    integer :: n
    if(SizeX.eq.1)then
       write(*,*) '(',real(Mat(1,:),dp),') +i (',aimag(Mat(1,:)),')'
    else
       do n=1,SizeX
          if(n.eq.1)then
             write(*,*) '/',real(Mat(n,:),dp),'\'
          elseif(n.eq.SizeX)then
             write(*,*) '\',real(Mat(n,:),dp),'/'
          else
             write(*,*) '|',real(Mat(n,:),dp),'|'
          end if
       end do
       write(*,*) '+i * '
       do n=1,SizeX
          if(n.eq.1)then
             write(*,*) '/',aimag(Mat(n,:)),'\'
          elseif(n.eq.SizeX)then
             write(*,*) '\',aimag(Mat(n,:)),'/'
          else
             write(*,*) '|',aimag(Mat(n,:)),'|'
          end if
       end do

    end if
  end subroutine print_cmplx_rec_matrix
  
  function kroenecker_matrix(Size)
    !!Gives a kroenecker delta function in matrix representation
    !!as an size x size matrix
    integer, intent(in) :: Size
    integer, dimension(Size,Size) :: kroenecker_matrix
    integer :: n
    kroenecker_matrix=0
    do n=1,Size
       kroenecker_matrix(n,n)=1
    end do
  end function kroenecker_matrix


  logical function is_matrix_symmetric(Size,Matrix) result(Symmetric)
    integer, intent(IN) :: Size,Matrix(Size,Size)
    integer :: n,m
    !!Retrun tru if not set to false later
    Symmetric=.TRUE.
    !!Check all the elements
    do n=1,(Size-1)
       do m=(n+1),Size
          if(Matrix(n,m).ne.Matrix(m,n))then
             Symmetric=.FALSE.
             return !!Return from the function
          end if
       end do
    end do
  end function is_matrix_symmetric

  
  logical function is_matrices_equal(Size,MatrixA,MatrixB) result(Equal)
    integer, intent(IN) :: Size,MatrixA(Size,Size),MatrixB(Size,Size)
    integer :: n,m
    !!Retrun true if the matrices are the same
    Equal=.TRUE.
    !!Check all the elements
    do n=1,Size
       do m=1,Size
          if(MatrixA(n,m).ne.MatrixB(n,m))then
             Equal=.FALSE.
             return !!Return from the function
          end if
       end do
    end do
  end function is_matrices_equal


  integer function determinant_int_mat(Matrix) result(det)
    integer, intent(IN) :: Matrix(:,:)
    integer :: Unity(size(Matrix,1),size(Matrix,2))
    integer :: pdet,qdet
    !!write(*,*) 'Compute determinant of integer matrix'
    Unity=1 !!Denominator is 1 for all entries
    call determinant_frac_matrix(Matrix,Unity,pdet,qdet)
    if(qdet.ne.1)then
       write(*,*) ',,,,,,,,dim,,,,,,,'
       write(*,*) 'ERROR: Determinant'
       write(*,*) 'The fractional determinat',pdet,'/',qdet,'does not have denominator 1, for integer matrix'
       call print_int_matrix(Matrix)
       write(*,*) 'qutting.....'
       call exit(-1)
    end if
    det=pdet
  end function determinant_int_mat
  
  
  subroutine determinant_frac_matrix(PMat_in,QMat_in,p_det,q_det)
    integer, intent(IN) :: PMat_in(:,:),QMat_in(:,:)
    integer, intent(OUT) :: p_det,q_det
    integer :: PMat(Size(Pmat_in,1),Size(Pmat_in,1))
    integer :: QMat(Size(Pmat_in,1),Size(Pmat_in,1))
    integer :: D,D2,D3,D4,n,m,n_loop,p_mul,q_mul,p_tmp,q_tmp
    logical :: shuffled
    !write(*,*) 'Compute determinant of fractional matrix'
    !call print_int_matrix(Pmat_in)
    !write(*,*) 'divided by'
    !call print_int_matrix(Qmat_in)
    !!Compute Diemntions and make sure they are equal
    D=Size(Pmat_in,1)
    D2=Size(Pmat_in,2)
    D3=Size(Qmat_in,1)
    D4=Size(Qmat_in,2)
    if(D.ne.D2.or.D.ne.D3.or.D.ne.D4)then
       write(*,*) ',,,,,,,,dfm,,,,,,,'
       write(*,*) 'ERROR: Determinant'
       write(*,*) 'Matrices do not all have the sem dimantions'
       write(*,*) 'Size are:',D,'x',D2,'and',D3,'x',D4
       write(*,*) 'qutting.....'
       call exit(-1)
    end if
    !!Compute determinant using gauassian elimination!!
    Pmat=Pmat_in
    Qmat=Qmat_in
    p_det=1 !!Starting determinant nominator
    q_det=1 !!Starting determinant denominator
    do n=1,D
       if(Pmat(n,n).eq.0)then
          !write(*,*) ',,,,,,,,dfm,,,,,,,'
          !write(*,*) 'Diagonal entries in Matrix is zero... need to reshuffle'
          !call print_int_matrix(Pmat)
          !write(*,*) 'divided by'
          !call print_int_matrix(Qmat)
          shuffled=.FALSE.
          do m=n+1,D !!Loop over the entries bellow
             !write(*,*) 'for(m,n)=',m,n,'gives Mat(m,n)=',Pmat(m,n)
             if(Pmat(m,n).ne.0)then
                !write(*,*) 'Shuffle with line m=',m
                Pmat=swap_matrix_rows(Pmat,n,m)
                Qmat=swap_matrix_rows(Qmat,n,m)
                p_det=-p_det !!The swap of two rown introduces a minus sign!
                !call print_int_matrix(Pmat)
                !write(*,*) 'divided by'
                !call print_int_matrix(Qmat)
                shuffled=.TRUE.
                exit
             end if
          end do
          if(.not.shuffled)then
             write(*,*) ',,,,,,,,mm:dii,,,,,,,'
             write(*,*) 'ERROR: Matrix Inverse'
             write(*,*) 'The determinant is zero for !'
             call print_int_matrix(Pmat_in)
             write(*,*) 'divided by'
             call print_int_matrix(Qmat_in)
             write(*,*) 'So no inverse! Quitting....'
             call exit(-1)
          end if
       end if
       do m=n+1,D
          !! Compute the fraction needed to cancel a row
          call multiply_fractions(-Qmat(n,n),Pmat(n,n),Pmat(m,n),Qmat(m,n),p_mul,q_mul)
          !write(*,*) 'For (m,n)=',m,n,'we need p/q',p_mul,'/',q_mul
          do n_loop=1,D
             call multiply_fractions(Pmat(n,n_loop),Qmat(n,n_loop),p_mul,q_mul,p_tmp,q_tmp)
             call add_fractions(p_tmp,q_tmp,Pmat(m,n_loop),Qmat(m,n_loop),&
                  Pmat(m,n_loop),Qmat(m,n_loop))
             !write(*,*) 'result after addition (m,m_loop)',m,n_loop
             !call print_int_matrix(Pmat)
             !write(*,*) 'divided by'
             !call print_int_matrix(Qmat)
          end do
       end do
    end do
    !!Multiply up the determinant
    do n=1,D
       call multiply_fractions(Pmat(n,n),Qmat(n,n),1*p_det,1*q_det,p_det,q_det)
    end do
    !write(*,*) 'Determinant:',p_det,'/',q_det
  end subroutine determinant_frac_matrix
  
  function swap_matrix_rows(InMat,n,m) result(OutMat)
    integer, intent(in) :: n,m,InMat(:,:)
    integer :: OutMat(size(InMat,1),size(InMat,2)) !!Out Variable
    integer :: tmpVector(size(InMat,2))
    integer :: Size
    
    OutMat=InMat
    tmpVector=OutMat(n,:)
    OutMat(n,:)=InMat(m,:)
    OutMat(m,:)=tmpVector
    
  end function swap_matrix_rows

end module misc_matrix
