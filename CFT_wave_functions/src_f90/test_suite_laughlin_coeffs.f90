program test_laughlin_coeffs
  
  USE typedef     ! types and definitions
  use jacobitheta
  use jacobi_structure_factor
  use test_utilities
  use fock_coefficients
  
  !!Variables
  IMPLICIT NONE  
  
  !!test starts here
  call test_suite_laughlin_coeffs()
  
contains 
  subroutine test_suite_laughlin_coeffs()
    integer :: Ne,Kvalue,Ne_max
    write(*,*) '        Test the generation of laughlin Fock coefficients'
    
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_z_factor
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_computation_time_z_factor
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_jacobi_power
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    do Kvalue=1,4
       write(*,*) '------------------------------------'
       write(*,*) '------------------------------------'
       write(*,*) ' Test all the symmetry properties for Kvalue=',Kvalue
       write(*,*) '------------------------------------'
       write(*,*) '------------------------------------'
       select case (Kvalue) !!Select depending on K-value
       case (1) 
          Ne_max=6
       case (2)
          Ne_max=4
       case(3)
          Ne_max=5
       case(4)
          Ne_max=4
       case default
          Ne_max=4
       end select
       do Ne=1,Ne_max
          write(*,*) '------------------------------------'
          write(*,*) ' Test all the symmetry properties for Ne=',Ne
          write(*,*) '------------------------------------'
          call test_invariance_under_Ns_shift(Ne,Kvalue)
          write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
          call test_semi_global_shifts(Ne,Kvalue)
          write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
          call test_periodic_boundary_conditions(Ne,Kvalue)
          write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
          call test_symmetry_on_index_exchange(Ne,Kvalue)
          write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
          call test_against_the_numeric_data(Ne,Kvalue)
          write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
          call test_wrong_momentum_gives_0(Ne,Kvalue)
          write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
          !!FIXME -- this test is just wrong
          !call test_global_shifts_are_the_same(Ne)
          !write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
       end do
    end do
  end subroutine test_suite_laughlin_coeffs
  
  
  !!-------------------------------------------------
  !!  Test computation of Z-factors
  !!-------------------------------------------------
  
  subroutine test_computation_time_z_factor()
    integer :: K,N, Steps=50
    complex(KIND=dpc) :: tau,test
!!!Testing the lowest N=5 layers of
    !! Z_K(N|tau)
    
    write(*,*) 'Testing z-factor computation time'
!!!This test just runns through and computes the 50 lowest Z_vales
!!! It is done to test that a speedup is really aquired
    
    call reset_memory_usage
    
    tau=iunit
    do N=1,Steps
       do K=0,N
          !write(*,*) 'N,K',N,K
          test=Z_factor(K,N,tau)
          !write(*,*) 'Z_factor:',test
       end do
    end do
    
  end subroutine test_computation_time_z_factor
  
  
  
  subroutine test_z_factor()
    integer :: K,N, Steps=5,Number
    complex(KIND=dpc) :: tau,test
    real(kind=dp) :: thetalist(20),Error
!!!Testing the lowest N=5 layers of
    !! Z_K(N|tau)
    
    
    write(*,*) 'Testing z-factor'
    thetalist(1)=1.0000000000000000000d0!!N=1
    thetalist(2)=1.0000000000000000000d0
    thetalist(3)=1.0037348854877390910d0!!N=2
    thetalist(4)=0.41576060259602703231d0
    thetalist(5)=1.0037348854877390910d0
    thetalist(6)=1.0112046955376900906d0!!N=3
    thetalist(7)=0.37012660699290336006d0
    thetalist(8)=0.37012660699290336006d0
    thetalist(9)=1.0112046955376900906d0
    thetalist(10)=1.0224303932790084735d0!!N=4
    thetalist(11)=0.38124883534928077403d0
    thetalist(12)=0.25993272258044692547d0
    thetalist(13)=0.38124883534928077403d0
    thetalist(14)=1.0224303932790084735d0
    thetalist(15)=1.0374538663819946455d0!!N=5
    thetalist(16)=0.40955950862315174235d0
    thetalist(17)=0.23162224256091284947d0
    thetalist(18)=0.23162224256091284947d0
    thetalist(19)=0.40955950862315174235d0
    thetalist(20)=1.0374538663819946455d0
    
    
    
    tau=iunit
    Number=1
    do N=1,Steps
       do K=0,N
          test=Z_factor(K,N,tau)
          Error=(real(test,kind=dp)-thetalist(Number))/thetalist(Number)
          if(abs(error).gt.1.d-14)then
             write(*,*) 'tau,N,K,num:',tau,N,K,Number
             write(*,*) 'theta_gen-calc:',real(test,kind=dp)
             write(*,*) 'theta_gen-raw :',thetalist(Number)
             write(*,*) 'Error:',Error
             call exit(-2)
          end if
          Number=Number+1
       end do
    end do
    
  end subroutine test_z_factor
  
  subroutine test_jacobi_power()
    integer :: K,N, Steps=6
    complex(KIND=dpc) :: tau,test,test2,test3,z
    real(kind=dp) :: Error
    
    write(*,*) 'Testing power of jacobi functions'
    
    tau=iunit
    do N=1,Steps
       do K=0,N
          z=(1.0d0*K)*(1.d0+2*iunit)/N
          test=theta_power_using_Z(z,tau,N)
          test3=theta3(z,tau)
          test2=test3**N
          Error=abs((test-test2)/test2)
          if(abs(error).gt.1.d-13)then
             write(*,*) 'tau,N:',tau,N
             write(*,*) 'K,z:',K,z
             write(*,*) 'theta_gen-calc:',test
             write(*,*) 'theta_gen-raw :',test2
             write(*,*) 'raw no powers: ',test3
             write(*,*) 'Error:',Error
             call exit(-2)
          end if
       end do
    end do
  end subroutine test_jacobi_power
  
  
  complex(kind=dpc) function theta_power_using_Z(z,tau,Power) result(res)
    integer, intent(IN) :: Power
    complex(KIND=dpc), intent(IN) :: tau,z
    integer :: N
    complex(KIND=dpc) :: qweight
    
    res = Z_factor(0,Power,tau)
    n=0
    do 
       n = n + 1
       !!OBS summing N as +-N
       qweight=Z_factor(N,Power,tau)*exp(iunit*n*pi*(tau*n/Power+2*z))&
            +Z_factor(-N,Power,tau)*exp(iunit*n*pi*(tau*n/Power-2*z))
       res = res + qweight
       !write(*,*) 'qweight :res', qweight,res
       
       !! Terminate on convergence (or NaNs or Zeroes)
       if( isnan(abs(res))) then
          write(*,*) 'Value is infinite in theta function'
          write(*,*) 'z,tau:',z,tau
          call exit(-1)
       elseif( abs(res).eq.0.d0 ) then
          write(*,*) 'Value is zero'
          exit
       elseif((n.ge.3).and.((res+qweight).eq.res)) then
          !write(*,*) 'Converged'
          exit
       end if
       
    end do
  end function theta_power_using_Z
  
  
  !!--------------------------------------------
  !!       Test basic properites of the laughlin coefficeints
  !!--------------------------------------------
  
  subroutine test_wrong_momentum_gives_0(Ne,Kvalue)
    use combinations
    integer,intent(IN) :: Kvalue, Ne
    TYPE(Combination) :: Combo
    integer :: Ns
    integer :: Target_Momentum,Combo_Momentum,momentum_diff
    complex(kind=dpc) :: coefficient,tau
    
    write(*,*) 'Test that adding wrong momentum state gives a 0 coefficient'
    
    tau=iunit
          write(*,*) '   Testing Ne=',Ne
          Ns = Ne*Kvalue
          Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
          write(*,*) '   Target momentum =  ',Target_Momentum
          
          CALL createCombination(Combo)    
          CALL initializeCombination(Combo,Ns,Ne)
          
          DO ! First, let's find the number of basis states
             IF ( .NOT. Combo%ok ) EXIT
             Combo_Momentum=modulo(sum(Combo%C),Ns)
             momentum_diff=Target_Momentum-Combo_Momentum
             coefficient=fock_laughlin(Combo%C,Kvalue,Ne,tau)
             !!Test that only target momenum is non-zero
             IF ( modulo(Momentum_diff,Ne).ne.0 ) THEN
                !write(*,*) '----testing ....'
                !write(*,*) '      Combo momentum =',Combo_Momentum
                !write(*,*) '      momentum diff  =',Momentum_diff
                
                if(coefficient.ne.0d0)then
                   write(*,*) 'ERROR:'
                   write(*,*) 'Target momentum is:',Target_Momentum
                   write(*,*) 'Combo momentum=',Combo_Momentum
                   write(*,*) 'Nonzero coefficient:',coefficient
                   write(*,*) 'For Ne=',Ne,'and Kvalue=',Kvalue
                   write(*,*) 'And the configuration'
                   write(*,*) Combo%C
                   call exit(-2)
                END IF
             end IF
             CALL nextCombination(Combo)
          END DO
          
          CALL destroyCombination(Combo)              
    
  end subroutine test_wrong_momentum_gives_0
  
  
  
  subroutine test_invariance_under_Ns_shift(Ne,Kvalue)
    use combinations
    integer, intent(In) :: Ne,Kvalue
    TYPE(Combination) :: Combo
    integer :: Ns,electron
    integer :: Target_Momentum,Combo_Momentum
    complex(kind=dpc) :: coefficient,coefficient_shift,tau
    integer :: shift_vector(Ne)
    
    write(*,*) 'Test that coefficeint is ivariant under shifting a cordinate by Ns'
    tau=iunit
       write(*,*) 'Testing Kvalue=',Kvalue
       write(*,*) '   Testing Ne=',Ne
       Ns = Ne*Kvalue
       Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
       write(*,*) '   Target momentum =  ',Target_Momentum
       
       CALL createCombination(Combo)    
       CALL initializeCombination(Combo,Ns,Ne)
       
       DO ! First, let's find the number of basis states
          IF ( .NOT. Combo%ok ) EXIT
          Combo_Momentum=modulo(sum(Combo%C),Ns)
          !write(*,*) '      Combo momentum =',Combo_Momentum
          !!Do test for correct momentum state
          IF ( Combo_Momentum .eq. Target_Momentum  ) THEN
             coefficient=fock_laughlin(Combo%C,Kvalue,Ne,tau)
             do electron=1,Ne
                shift_vector=0
                shift_vector(electron)=Ns
                coefficient_shift=fock_laughlin(Combo%C+shift_vector,Kvalue,Ne,tau)
                if(test_diff_cmplx(coefficient,coefficient_shift,1.d-14))then
                   write(*,*) 'ERROR:'
                   write(*,*) 'Combo momentum is:',Target_Momentum
                   write(*,*) 'Shifted electron=',electron
                   write(*,*) 'coefficient:',coefficient
                   write(*,*) 'shift coefficient:',coefficient_shift
                   write(*,*) 'For Ne=',Ne,'and Kvalue=',Kvalue
                   write(*,*) 'And the configurations'
                   write(*,*) Combo%C
                   write(*,*) Combo%C+shift_vector
                   call exit(-2)
                END IF
             end do
          end IF
          CALL nextCombination(Combo)
       END DO
       
       CALL destroyCombination(Combo)              
    
  end subroutine test_invariance_under_Ns_shift
  
  subroutine test_global_shifts_are_the_same(Ne,Kvalue)
    use combinations
    
    integer, intent(In) :: Ne,Kvalue
    TYPE(Combination) :: Combo
    integer :: Ns,shift
    integer :: Target_Momentum,Combo_Momentum
    complex(kind=dpc) :: coefficient,coefficient_shift,tau
    
    write(*,*) 'Test that a global shift changes the coefficeints in the right way'
!!!FIXME 16/6-14 The signs here under shifts are acturallt not at all that triviall
    
    tau=iunit
       write(*,*) 'Testing Kvalue=',Kvalue
       write(*,*) '   Testing Ne=',Ne
       Ns = Ne*Kvalue
       Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
       write(*,*) '   Target momentum =  ',Target_Momentum
       
       CALL createCombination(Combo)    
       CALL initializeCombination(Combo,Ns,Ne)
       
       DO ! First, let's find the number of basis states
          IF ( .NOT. Combo%ok ) EXIT
          Combo_Momentum=modulo(sum(Combo%C),Ns)
          !write(*,*) '      Combo momentum =',Combo_Momentum
          !!Do test for correct momentum state
          IF ( Combo_Momentum .eq. Target_Momentum  ) THEN
             coefficient=fock_laughlin(Combo%C,Kvalue,Ne,tau)
             do shift=1,Ns
                coefficient_shift=fock_laughlin(Combo%C+shift,Kvalue,Ne,tau)
                if(test_diff_cmplx(coefficient/coefficient_shift&
                     ,(-1)**(shift*Kvalue*(Ne-1))+czero,1.d-14))then
                   write(*,*) 'ERROR:'
                   write(*,*) 'Combo momentum is:',Target_Momentum
                   write(*,*) 'Shift momentum=',Combo_Momentum+shift
                   write(*,*) 'coefficient:      ',coefficient
                   write(*,*) 'shift coefficient:',coefficient_shift
                   write(*,*) 'Quitient:',coefficient/coefficient_shift
                   write(*,*) 'For Ne=',Ne,'and Kvalue=',Kvalue
                   write(*,*) 'And the configurations'
                   write(*,*) Combo%C
                   write(*,*) Combo%C+shift
                   write(*,*) 'Expecting a relative sign of:',(-1)**(shift*Kvalue*(Ne-1))
                   
                   !!FIXME - either fix the test to work properly, or remove it
                   !!call exit(-2)
                   !!FIXME - either fix the test to work properly, or remove it
                END IF
             end do
          end IF
          CALL nextCombination(Combo)
       END DO
       
       CALL destroyCombination(Combo)              
    
  end subroutine test_global_shifts_are_the_same
  
  
  
  subroutine test_periodic_boundary_conditions(Ne,Kvalue)
    use combinations
    integer, intent(In) :: Ne,Kvalue
    TYPE(Combination) :: Combo
    integer :: Ns,particle1,particle2
    integer :: Target_Momentum,Combo_Momentum
    complex(kind=dpc) :: coefficient,coefficient_shift,tau
    integer :: bal_d_shift(Ne),bal_d_swap(Ne)
    integer :: raw_double_shift(Ne)
    
    
    write(*,*) 'Test that winding (and counder winding) by Ns is invariant'
    tau=iunit
       write(*,*) 'Testing Kvalue=',Kvalue
       write(*,*) '   Testing Ne=',Ne
       Ns = Ne*Kvalue
       Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
       write(*,*) '   Target momentum =  ',Target_Momentum
       
       CALL createCombination(Combo)    
       CALL initializeCombination(Combo,Ns,Ne)
       
       DO ! First, let's find the number of basis states
          IF ( .NOT. Combo%ok ) EXIT
          Combo_Momentum=modulo(sum(Combo%C),Ns)
          !write(*,*) '      Combo momentum =',Combo_Momentum
          !!Do test for correct momentum state
          IF ( Combo_Momentum .eq. Target_Momentum  ) THEN
             raw_double_shift=2*Combo%C-Kvalue*(Ne-1)
             call balance_combo(raw_double_shift,bal_d_shift,Ne)
             !write(*,*) 'Raw combo',Combo%C
             !write(*,*) 'Bal_d_combo',bal_d_shift
             coefficient=laughlin_reduced_coeff(bal_d_shift,Ne,Kvalue,tau)
             do particle1=1,Ne-1
                do particle2=(particle1+1),Ne
                   bal_d_swap=bal_d_shift
                   bal_d_swap(particle1)=bal_d_swap(particle1)+2*Ns
                   bal_d_swap(particle2)=bal_d_swap(particle2)-2*Ns
                   !write(*,*) 'Bal_d_shift',bal_d_swap
                   coefficient_shift=laughlin_reduced_coeff(bal_d_swap,Ne,Kvalue,tau)
                   if(test_diff_cmplx(coefficient_shift/coefficient&
                        ,1.d0+czero,1.d-10))then
                      write(*,*) 'ERROR:'
                      write(*,*) 'Combo momentum is:',Target_Momentum
                      write(*,*) 'coefficient:     ',coefficient
                      write(*,*) 'swap coefficient:',coefficient_shift
                      write(*,*) 'Quitient:',coefficient/coefficient_shift
                      write(*,*) 'For Ne=',Ne,'and Kvalue=',Kvalue
                      write(*,*) 'For particles:',particle1,particle2
                      write(*,*) 'And the configurations'
                      write(*,*) Combo%C
                      write(*,*) 'Which (doubled) under balance are'
                      write(*,*) bal_d_shift
                      write(*,*) bal_d_swap
                      
                      write(*,*) 'Expecting a relative sign of:',1
                      call exit(-2)
                   END IF
                end do
             end do
          end IF
          CALL nextCombination(Combo)
       END DO
       
       CALL destroyCombination(Combo)              
    
  end subroutine test_periodic_boundary_conditions
  
  
  subroutine test_semi_global_shifts(Ne,Kvalue)
    use combinations
    integer, intent(In) :: Ne,Kvalue
    TYPE(Combination) :: Combo
    integer :: Ns
    integer :: Target_Momentum,Combo_Momentum
    complex(kind=dpc) :: coefficient,coefficient_shift,tau
    integer :: bal_d_shift(Ne),bal_d_swap(Ne)
    integer :: raw_double_shift(Ne)
    
    
    write(*,*) 'Test that moving all particles (exept the last) by K does not change the coefficients expept for (-1)**K*(Ne-1)'
    tau=iunit
       write(*,*) 'Testing Kvalue=',Kvalue
       write(*,*) '   Testing Ne=',Ne
       Ns = Ne*Kvalue
       Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
       write(*,*) '   Target momentum =  ',Target_Momentum
       
       CALL createCombination(Combo)    
       CALL initializeCombination(Combo,Ns,Ne)
       
       DO ! First, let's find the number of basis states
          IF ( .NOT. Combo%ok ) EXIT
          Combo_Momentum=modulo(sum(Combo%C),Ns)
          !write(*,*) '      Combo momentum =',Combo_Momentum
          !!Do test for correct momentum state
          IF ( Combo_Momentum .eq. Target_Momentum  ) THEN
             raw_double_shift=2*Combo%C-Kvalue*(Ne-1)
             call balance_combo(raw_double_shift,bal_d_shift,Ne)
             !write(*,*) 'Raw combo',Combo%C
             !write(*,*) 'Bal_d_combo',bal_d_shift
             coefficient=laughlin_reduced_coeff(bal_d_shift,Ne,Kvalue,tau)
             
             !!It's the doubled values
             bal_d_swap=bal_d_shift+2*Kvalue
             call balance_combo(1*bal_d_swap,bal_d_swap,Ne)
             
             coefficient_shift=laughlin_reduced_coeff(bal_d_swap,Ne,Kvalue,tau)
             if(test_diff_cmplx(coefficient_shift/coefficient&
                  ,(-1)**(Kvalue*(Ne-1))+czero,1.d-8))then
                write(*,*) 'ERROR: Rigid move of Ne-1 coordinates by K'
                write(*,*) 'Recomputing in verbose mode'
                fock_verbose=.TRUE.
                coefficient=laughlin_reduced_coeff(bal_d_shift,Ne,Kvalue,tau)
                coefficient_shift=laughlin_reduced_coeff(bal_d_swap,Ne,Kvalue,tau)
                write(*,*) 'ERROR: Rigid move of Ne-1 coordinates by K'
                write(*,*) 'Combo momentum is:',Target_Momentum
                write(*,*) 'coefficient:     ',coefficient
                write(*,*) 'swap coefficient:',coefficient_shift
                write(*,*) 'Quitient:',coefficient/coefficient_shift
                write(*,*) 'Quitient-1:',coefficient/coefficient_shift-1.d0
                write(*,*) 'For Ne=',Ne,'and Kvalue=',Kvalue
                write(*,*) 'and the configurations'
                write(*,*) Combo%C
                write(*,*) 'Which (doubled) under balance are'
                write(*,*) bal_d_shift
                write(*,*) bal_d_swap
                
                write(*,*) 'Expecting a relative sign of:',(-1)**(Kvalue*(Ne-1))
                call exit(-2)
             END IF
          end IF
          CALL nextCombination(Combo)
       END DO
       
       CALL destroyCombination(Combo)              
    
  end subroutine test_semi_global_shifts
  
  
  
  subroutine test_symmetry_on_index_exchange(Ne,Kvalue)
    use combinations
    integer, intent(In) :: Ne,Kvalue
    TYPE(Combination) :: Combo
    integer :: Ns,particle1,particle2
    integer :: Target_Momentum,Combo_Momentum
    complex(kind=dpc) :: coefficient,coefficient_shift,tau
    integer :: swap_combo(Ne),bal_d_shift(Ne),bal_d_swap(Ne)
    integer :: raw_double_shift(Ne)
    
    
    write(*,*) 'Test that changing the order of two indexed induces a (possible) minus sign'
    tau=iunit
       write(*,*) 'Testing Kvalue=',Kvalue
       write(*,*) '   Testing Ne=',Ne
       Ns = Ne*Kvalue
       Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
       write(*,*) '   Target momentum =  ',Target_Momentum
       
       CALL createCombination(Combo)    
       CALL initializeCombination(Combo,Ns,Ne)
       
       DO ! First, let's find the number of basis states
          IF ( .NOT. Combo%ok ) EXIT
          Combo_Momentum=modulo(sum(Combo%C),Ns)
          !write(*,*) '      Combo momentum =',Combo_Momentum
          !!Do test for correct momentum state
          IF ( Combo_Momentum .eq. Target_Momentum  ) THEN
             raw_double_shift=2*Combo%C-Kvalue*(Ne-1)
             call balance_combo(raw_double_shift,bal_d_shift,Ne)
             !rite(*,*) 'Raw combo',Combo%C
             !write(*,*) 'Bal_d_shift',bal_d_shift
             coefficient=laughlin_reduced_coeff(bal_d_shift,Ne,Kvalue,tau)
             do particle1=1,Ne-1
                do particle2=(particle1+1),Ne
                   swap_combo=Combo%C
                   swap_combo(particle1)=Combo%C(particle2)
                   swap_combo(particle2)=Combo%C(particle1)
                   raw_double_shift=2*swap_combo-Kvalue*(Ne-1)
                   !rite(*,*) 'swap combo',swap_combo
                   call balance_combo(raw_double_shift,bal_d_swap,Ne)
                   !rite(*,*) 'Bal_d_swap',bal_d_swap
                   coefficient_shift=laughlin_reduced_coeff(bal_d_swap,Ne,Kvalue,tau)
                   if(test_diff_cmplx(coefficient_shift/coefficient&
                        ,(-1.d0)**Kvalue+czero,1.d-10))then
                      write(*,*) 'ERROR:index antisymmetry'
                      !!Recompute the value, but thing time verbosely..
                      fock_verbose=.TRUE.
                      coefficient=laughlin_reduced_coeff(bal_d_shift,Ne,Kvalue,tau)
                      coefficient_shift=laughlin_reduced_coeff(bal_d_swap,Ne,Kvalue,tau)
                      !!Print error maessage
                      write(*,*) 'ERROR:index antisymmetry'
                      write(*,*) 'Combo momentum is:',Target_Momentum
                      write(*,*) 'coefficient:     ',coefficient
                      write(*,*) 'swap coefficient:',coefficient_shift
                      write(*,*) 'Quitient:',coefficient/coefficient_shift
                      write(*,*) 'For Ne=',Ne,'and Kvalue=',Kvalue
                      write(*,*) 'For particles:',particle1,particle2
                      write(*,*) 'And the configurations'
                      write(*,*) Combo%C
                      write(*,*) swap_combo
                      write(*,*) 'Which under balance are'
                      write(*,*) bal_d_shift
                      write(*,*) bal_d_swap
                      
                      write(*,*) 'Expecting a relative sign of:',(-1)**Kvalue
                      call exit(-2)
                   END IF
                end do
             end do
          end IF
          CALL nextCombination(Combo)
       END DO
       
       CALL destroyCombination(Combo)              
    
  end subroutine test_symmetry_on_index_exchange
  
  
  subroutine test_against_the_numeric_data(Ne,Kvalue)
    use combinations
    integer, intent(In) :: Ne,Kvalue
    
    if(Kvalue.eq.3)then
       write(*,*) 'Test the the generated compoents agains the exact diagonalization data, for small systems'
    
       write(*,*) '   Testing Ne=',Ne,'Kvalue',3
       call do_test_against_numeric_data(Ne)
    end if
    
  end subroutine test_against_the_numeric_data
  
  
  subroutine balance_combo(combo_in,balanced_combo,Ne)
    integer, intent(IN) :: combo_in(Ne),Ne
    integer, intent(OUT) :: balanced_combo(Ne)
    integer :: combo(Ne)
    
    !!Just in case combo_in and balanced_combo points to the same memory space
    combo=combo_in
    
    balanced_combo=combo
    balanced_combo(Ne)=combo(Ne)-sum(combo)
  end subroutine balance_combo
  
  
  subroutine do_test_against_numeric_data(Ne)
    use combinations
    integer, intent(in) :: Ne
    integer :: Kvalue,Ns,Basis_size,StateNum,max_position,StateNum2
    integer, allocatable, dimension(:,:) :: fock_basis
    complex(kind=dpc), allocatable, dimension(:) :: fock_ED_coeff,fock_La_coeff
    real(kind=dp) :: tmp_coeff(2),ED_Normalization,LA_Normalization
    complex(kind=dpc) :: ED_exp_arg,LA_exp_arg,tau
    Integer  :: ch_basis = 745, ch_coeff = 746
    
    tau=iunit
    Kvalue=3
    Ns=Kvalue*Ne
    basis_size=get_basis_size(Ne,Kvalue)
    write(*,*) 'Basis size is:',basis_size
    write(*,*) 'for Ne=',Ne,'particles'
    write(*,*) 'Allocating'
    allocate(fock_basis(Basis_size,Ne))
    allocate(fock_ED_coeff(Basis_size))
    allocate(fock_La_coeff(Basis_size))
    fock_basis=-1
    fock_ED_coeff=0.d0
    write(*,*) 'Open data'
    select case(Ne)
    case(1)
       OPEN(ch_basis,FILE=TRIM('test_data/fock_basis_c=1.dat'),STATUS='old')
       OPEN(ch_coeff,FILE=TRIM('test_data/fock_coeffs_c=1.dat'),STATUS='old')
    case(2)
       OPEN(ch_basis,FILE=TRIM('test_data/fock_basis_c=2.dat'),STATUS='old')
       OPEN(ch_coeff,FILE=TRIM('test_data/fock_coeffs_c=2.dat'),STATUS='old')
    case(3)
       OPEN(ch_basis,FILE=TRIM('test_data/fock_basis_c=3.dat'),STATUS='old')
       OPEN(ch_coeff,FILE=TRIM('test_data/fock_coeffs_c=3.dat'),STATUS='old')
    case(4)
       OPEN(ch_basis,FILE=TRIM('test_data/fock_basis_c=4.dat'),STATUS='old')
       OPEN(ch_coeff,FILE=TRIM('test_data/fock_coeffs_c=4.dat'),STATUS='old')
    case(5)
       OPEN(ch_basis,FILE=TRIM('test_data/fock_basis_c=5.dat'),STATUS='old')
       OPEN(ch_coeff,FILE=TRIM('test_data/fock_coeffs_c=5.dat'),STATUS='old')
    case(6)
       OPEN(ch_basis,FILE=TRIM('test_data/fock_basis_c=6.dat'),STATUS='old')
       OPEN(ch_coeff,FILE=TRIM('test_data/fock_coeffs_c=6.dat'),STATUS='old')
    case default
       write(*,*) 'ERROR'
       write(*,*) 'No other electron numbers implemented'
       call exit(-2)
    end select
    
    write(*,*) 'Read in the data'
    Do StateNum=1,Basis_size
       read(ch_basis,*) fock_basis(StateNum,:)
       read(ch_coeff,*) tmp_coeff(:)
       fock_ED_coeff(StateNum)=tmp_coeff(1)+iunit*tmp_coeff(2)
       !write(*,*) 'Basis:',fock_basis(StateNum,:)
       !write(*,*) 'EDCoeff:',fock_ED_coeff(StateNum)
       !write(*,*) 'ED weight:',abs(fock_ED_coeff(StateNum))**2
    end Do
    
    write(*,*) 'Close data'
    close(ch_basis)
    close(ch_coeff)
    
    write(*,*) 'Compute Laughlin coeffs'
    Do StateNum=1,Basis_size
       !write(*,*) 'Computing cofficient'
       !call print_occupation(fock_basis(StateNum,:),Ne,Ns)
       fock_LA_coeff(StateNum)=fock_laughlin(fock_basis(StateNum,:),Kvalue,Ne,tau)
       !write(*,*) 'Basis:',fock_basis(StateNum,:)
       !write(*,*) 'La Coeff:',fock_LA_coeff(StateNum)
       !write(*,*) 'La weight:',abs(fock_LA_coeff(StateNum))**2
    end Do
    
    
    ED_Normalization=sum(abs(fock_ED_coeff)**2)
    La_Normalization=sum(abs(fock_LA_coeff)**2)
    write(*,*) 'ED Normalization:',ED_Normalization
    write(*,*) 'LA Normalization:',LA_Normalization
    
    write(*,*) 'Normalize'
    fock_ED_coeff=fock_ED_coeff/sqrt(ED_Normalization)
    fock_La_coeff=fock_La_coeff/sqrt(La_Normalization)
    
    !!Allign the vector by maing the larges element positive real
    max_position=max_list_position(fock_eD_coeff,Basis_size)
    
    !write(*,*) 'Max position:',max_position
    
    write(*,*) 'realligning vector'
    ED_exp_arg=fock_ED_coeff(max_position)/abs(fock_ED_coeff(max_position))
    LA_exp_arg=fock_LA_coeff(max_position)/abs(fock_LA_coeff(max_position))
    
    !write(*,*) 'ED phase:',ED_exp_arg
    !write(*,*) 'LA phase:',LA_exp_arg
    
    fock_ED_coeff=fock_ED_coeff/ED_exp_arg
    fock_LA_coeff=fock_LA_coeff/LA_exp_arg
    
    !!COmpare all the values and make sure they are the same
    Do StateNum=1,Basis_size
       if(test_diff_cmplx(fock_ED_coeff(StateNum)/fock_LA_coeff(StateNum)&
            ,1.d0+czero,1.d-10))then
          write(*,*) 'ERROR MISSMATCH with Laughlin'
          write(*,*) '----------------------------'
          Do StateNum2=1,Basis_size
             !write(*,*) fock_basis(StateNum2,:)
             call print_occupation(fock_basis(StateNum2,:),Ne,Ns)
             !write(*,*) fock_ED_coeff(StateNum2),fock_LA_coeff(StateNum2)
             write(*,*) abs(fock_ED_coeff(StateNum2)),abs(fock_LA_coeff(StateNum2))
          end Do
          write(*,*) '----------------------------'
          write(*,*) 'For Ne=',Ne,'particles and Kvalue=',Kvalue
          write(*,*) 'Basis:',fock_basis(StateNum,:)
          write(*,*) 'ED Coeff:',fock_ED_coeff(StateNum)
          write(*,*) 'La Coeff:',fock_LA_coeff(StateNum)
          write(*,*) 'Quotient:',fock_ED_coeff(StateNum)/fock_LA_coeff(StateNum)
          write(*,*) 'ED weight:',abs(fock_ED_coeff(StateNum))**2
          write(*,*) 'La weight:',abs(fock_LA_coeff(StateNum))**2
          
          
          call exit(-2)
       end if
    end Do
    
    
  end subroutine do_test_against_numeric_data
  
  
  
  integer function max_list_position(List,Size) result(max_pos)
    integer, intent(in) :: Size
    complex(kind=dpc), intent(in) :: List(Size)
    integer :: tmp_pos
    real(kind=dp) :: max_val,tmp_val
    
    max_val=abs(List(1))
    max_pos=1
    
    do tmp_pos=2,Size
       tmp_val=abs(List(tmp_pos))
       if(tmp_val.gt.max_val)then
          !!new largest element
          max_pos=tmp_pos
          max_val=tmp_val
          !write(*,*) 'new max pos',max_pos,max_val
       end if
    end do
  end function max_list_position
  
  
  
  
  integer function get_basis_size(Ne,Kvalue) result(size)
    use combinations
    integer, intent(in) :: Kvalue, Ne
    TYPE(Combination) :: Combo
    integer :: Target_Momentum,Combo_Momentum,Ns
    
    size=0
    Ns = Ne*Kvalue
    Target_Momentum=modulo((Kvalue*Ne*(Ne+1))/2,Ns)
    CALL createCombination(Combo)    
    CALL initializeCombination(Combo,Ns,Ne)
    DO ! First, let's find the number of basis states
       IF ( .NOT. Combo%ok ) EXIT
       Combo_Momentum=modulo(sum(Combo%C),Ns)
       !write(*,*) '      Combo momentum =',Combo_Momentum
       !!Do test for correct momentum state
       IF ( Combo_Momentum .eq. Target_Momentum  ) THEN
          size=size+1
       end IF
       CALL nextCombination(Combo)
    END DO
  end function get_basis_size
  
  
  subroutine print_occupation(List_in,Ne,Ns)
    integer, intent(IN) :: List_in(Ne),Ne,Ns
    integer i,j,List(Ne)
    
    !!List starts at 0. SHift for simplicity
    List=List_in+1
    do j=1,(List(1)-1)
       write(*,'(A)',advance='no') 'O '
    end do
    write(*,'(A)',advance='no') '1 '
    do i=2,Ne
       do j=(List(i-1)+1),(List(i)-1)
          write(*,'(A)',advance='no') 'O '
       end do
       write(*,'(A)',advance='no') '1 '
    end do
    do j=(List(Ne)+1),Ns
       write(*,'(A)',advance='no') 'O '
    end do
    write(*,*) 
  end subroutine print_occupation
  
  
  
end program test_laughlin_coeffs

