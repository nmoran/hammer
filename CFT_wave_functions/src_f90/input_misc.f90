module input_misc
  
  use channels
  use typedef
  use misc_matrix
  implicit none
  
  public

  
contains
    
  subroutine print_variable_status(Flag,Variable,Name)
    integer, intent(in) :: variable
    character(*), intent(in) ::name
    logical, intent(in) :: Flag
    !!print the value of the global variables
    if(Flag)then
       write(*,*) Name,'=',Variable
    else
       write(*,*) Name,'=unset'
    end if
  end subroutine print_variable_status

  subroutine print_list_status(Flag,List,Name,Size)
    integer, intent(in) :: size,List(size)
    character(*), intent(in) ::name
    logical, intent(in) :: Flag
    !!print the value of the global Lists
    if(Flag)then
       write(*,*) Name,'=',List
    else
       write(*,*) Name,'=unset'
    end if
  end subroutine print_list_status



  subroutine print_matrix_status(Flag,Matrix,Name,Size)
    integer, intent(in) :: size, Matrix(size,size)
    character(*), intent(in) ::name
    logical, intent(in) :: Flag
    !!print the value of the global Matrixs
    if(Flag)then
       write(*,*) Name,'='
       call print_int_matrix(Matrix)
    else
       write(*,*) Name,'=unset'
    end if
  end subroutine print_matrix_status

 
  
  subroutine print_float_status(Flag,Variable,Name)
    real(kind=dp), intent(in) :: variable
    character(*), intent(in) ::name
    logical, intent(in) :: Flag
    !!print the value of the global variables
    if(Flag)then
       write(*,*) Name,'=',Variable
    else
       write(*,*) Name,'=unset'
    end if
  end subroutine print_float_status


  
!!!--------------------------------------------------
!!!        Reterive the input to the program
!!!--------------------------------------------------
  

  subroutine import_set_matrix(FLAG,Flag_Name,Matrix,cptArg,Var_Name,Size,help_function)
    logical, intent(INOUT) :: FLAG
    character(*), intent(IN) :: Flag_Name,Var_Name
    integer, intent(IN) :: cptArg,Size
    integer, intent(OUT) :: matrix(Size,Size)
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    call set_unset_flag(FLAG,Flag_Name,help_function)
    call recieve_integer_matrix_input(Matrix,Size,Var_Name,cptArg+1)
    if(.not.is_matrix_symmetric(Size,Matrix))then
       call help_function()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'The given ',Var_Name
       call print_int_matrix(Matrix)
       write(stderr,*) 'is not symmetric => NO ',Var_Name
       write(*,*)
       call exit(-2)
    end if
  end subroutine import_set_matrix


  subroutine import_set_matrix_alt(FLAG,Flag_Name,Matrix,cptArg,arguments,Var_Name,Size,help_function)
    logical, intent(INOUT) :: FLAG
    character(*), intent(IN) :: Flag_Name,Var_Name
    integer, intent(IN) :: cptArg,Size
    integer, intent(OUT) :: matrix(Size,Size)
    character(*), intent(in) :: arguments(:)
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    call set_unset_flag(FLAG,Flag_Name,help_function)
    call recieve_integer_matrix_input_alt(Matrix,Size,Var_Name,arguments,cptArg+1)
    if(.not.is_matrix_symmetric(Size,Matrix))then
       call help_function()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'The given ',Var_Name
       call print_int_matrix(Matrix)
       write(stderr,*) 'is not symmetric => NO ',Var_Name
       write(*,*)
       call exit(-2)
    end if
  end subroutine import_set_matrix_alt


  subroutine import_set_diag_matrix(FLAG,Flag_Name,Matrix,cptArg,Var_Name,Size,help_function)
    logical, intent(INOUT) :: FLAG
    character(*), intent(IN) :: Flag_Name,Var_Name
    integer, intent(IN) :: cptArg,Size
    integer, intent(OUT) :: matrix(Size,Size)
    integer :: diag_in(Size),n
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    call set_unset_flag(FLAG,Flag_Name,help_function)
    call recieve_integer_list_input(diag_in,Size,Var_Name,cptArg+1)
    Matrix=0
    do n=1,Size
       Matrix(n,n)=diag_in(n)
    end do
    if(.not.is_matrix_symmetric(Size,Matrix))then
       call help_function()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'The given ',Var_Name
       call print_int_matrix(Matrix)
       write(stderr,*) 'is not symmetric => NO ',Var_Name
       write(*,*)
       call exit(-2)
    end if
  end subroutine import_set_diag_matrix


  subroutine import_set_diag_matrix_alt(FLAG,Flag_Name,Matrix,cptArg,arguments,Var_Name,Size,help_function)
    logical, intent(INOUT) :: FLAG
    character(*), intent(IN) :: Flag_Name,Var_Name
    character(*), intent(in) :: arguments(:)
    integer, intent(IN) :: cptArg,Size
    integer, intent(OUT) :: matrix(Size,Size)
    integer :: diag_in(Size),n
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    call set_unset_flag(FLAG,Flag_Name,help_function)
    call recieve_integer_list_input_alt(diag_in,Size,Flag_Name,arguments,cptArg+1)
    Matrix=0
    do n=1,Size
       Matrix(n,n)=diag_in(n)
    end do
    if(.not.is_matrix_symmetric(Size,Matrix))then
       call help_function()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'The given ',Var_Name
       call print_int_matrix(Matrix)
       write(stderr,*) 'is not symmetric => NO ',Var_Name
       write(*,*)
       call exit(-2)
    end if
  end subroutine import_set_diag_matrix_alt


  subroutine error_flag_dependency(Flag_Name,Dependency_Flag,help_function)
    character(*), intent(IN) :: Flag_Name,Dependency_Flag
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    call help_function()
    write(stderr,*) 'ERROR: Input flags in wrong order'
    write(stderr,*) 'Flag ',Dependency_Flag,' has to be set before ', Flag_Name
    write(*,*)
    call exit(-2)
  end subroutine error_flag_dependency

  subroutine error_flag_missing(Flag_Name,help_function)
    character(*), intent(IN) :: Flag_Name
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    call help_function()
    write(stderr,*) 'ERROR: Missing flags!'
    write(stderr,*) 'Flag ',Flag_Name,' has to be set!'
    write(*,*)
    call exit(-2)
  end subroutine error_flag_missing


  subroutine error_flag_clash(Flag_Name,Clashing_Flag,help_function)
    character(*), intent(IN) :: Flag_Name,Clashing_Flag
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    call help_function()
    write(stderr,*) 'ERROR: Clashing input flags'
    write(stderr,*) 'Flag ',Clashing_Flag,' can not be set at the sam time as ', Flag_Name
    write(*,*)
    call exit(-2)
  end subroutine error_flag_clash


  subroutine set_unset_flag(Flag,Name,help_function)
    logical, intent(out) :: Flag
    character(*), intent(in)::name
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    if(Flag)then
       call help_function()
       write(stderr,*) 'ERROR: Duplicate input'
       write(stderr,*) 'The flag ',Name,' has already been set'
       write(*,*)
       call exit(-2)
    else
       !write(*,*) 'Setting the flag ',Name
       Flag=.TRUE.
    end if
  end subroutine set_unset_flag
  
  subroutine recieve_integer_matrix_input(matrix,Size,Name,position)
    integer, intent(in) :: position,Size
    integer, intent(out) :: matrix(Size,Size)
    integer :: input_matrix(Size**2),n,pos,io
    character(len=100)::input
    character(*), intent(in)::name
    do n=1,(size**2)
       pos=position+n-1
       call get_command_argument(pos,input)
       read(input,*,IOSTAT=io) input_matrix(n)
       call check_error_input_reading(io,Name,position,input,'integer')
    end do
    matrix=reshape(input_matrix,(/size,size/))
  end subroutine recieve_integer_matrix_input
  
 
  subroutine recieve_integer_matrix_input_alt(matrix,Size,Name,arguments,position)
    integer, intent(in) :: position,Size
    integer, intent(out) :: matrix(Size,Size)
    integer :: input_matrix(Size**2),n,pos,io
    character(len=100)::input
    character(*), intent(in) :: arguments(:)
    character(*), intent(in)::name
  do n=1,(size**2)
       pos=position+n-1
       input=arguments(pos)
       read(input,*,IOSTAT=io) input_matrix(n)
       call check_error_input_reading(io,Name,position,input,'integer')
    end do
    matrix=reshape(input_matrix,(/size,size/))
  end subroutine recieve_integer_matrix_input_alt
  
  
  subroutine recieve_integer_list_input(list,Size,Name,position)
    integer, intent(in) :: position,Size
    integer, intent(out) :: list(Size)
    integer :: n,pos,io
    character(len=100)::input
    character(*), intent(in)::name
    do n=1,size
       pos=position+n-1
       call get_command_argument(pos,input)
       read(input,*,IOSTAT=io) list(n)
       call check_error_input_reading(io,Name,position,input,'integer')
    end do
  end subroutine recieve_integer_list_input
  
  
  subroutine recieve_integer_list_input_alt(list,Size,Name,arguments,position)
    integer, intent(in) :: position,Size
    integer, intent(out) :: list(Size)
    integer :: n,pos,io
    character(len=100)::input
    character(*), intent(in) :: arguments(:)
    character(*), intent(in)::name
    do n=1,size
       pos=position+n-1
       input=arguments(pos)
       read(input,*,IOSTAT=io) list(n)
       call check_error_input_reading(io,Name,position,input,'integer')
    end do
  end subroutine recieve_integer_list_input_alt
  
  subroutine recieve_integer_input(variable,Name,position)
    integer, intent(in) :: position
    integer, intent(out) :: variable
    integer :: io !!Test input is ok
    character(len=100)::input
    character(*), intent(in)::name
    call get_command_argument(position,input)
    read(input,*,IOSTAT=io) variable
    call check_error_input_reading(io,Name,position,input,'integer')
  end subroutine recieve_integer_input


  subroutine recieve_integer_input_alt(variable,Name,arguments,position)
    integer, intent(in) :: position
    character(*), intent(in) :: arguments(:)
    integer, intent(out) :: variable
    character(*), intent(in)::name
    integer :: io !!Test input is ok
    character(len=100)::input
    input=arguments(position)
    read(arguments(position),*,IOSTAT=io) variable
    call check_error_input_reading(io,Name,position,input,'integer')
  end subroutine recieve_integer_input_alt


  subroutine recieve_float_input(variable,Name,position)
    integer, intent(in) :: position
    real(kind=dp), intent(out) :: variable
    character(len=100)::input
    character(*), intent(in)::name
    integer :: io
    call get_command_argument(position,input)
    read(input,*,IOSTAT=io) variable
    call check_error_input_reading(io,Name,position,input,'float')
  end subroutine recieve_float_input
  

  subroutine recieve_float_input_alt(variable,Name,arguments,position)
    integer, intent(in) :: position
    real(kind=dp), intent(out) :: variable
    character(len=100)::input
    character(*), intent(in)::name
    character(*), intent(in) :: arguments(:)
    integer :: io
    input=arguments(position)
    read(arguments(position),*,IOSTAT=io) variable
    call check_error_input_reading(io,Name,position,input,'float')
  end subroutine recieve_float_input_alt


  subroutine recieve_float_list_input_alt(list,Size,Name,arguments,position)
    integer, intent(in) :: position,Size
    real(KIND=dp), intent(out) :: list(Size)
    integer :: n,pos,io
    character(len=100)::input
    character(*), intent(in) :: arguments(:)
    character(*), intent(in)::name
    do n=1,size
       pos=position+n-1
       input=arguments(pos)
       read(input,*,IOSTAT=io) list(n)
       call check_error_input_reading(io,Name,position,input,'integer')
    end do
  end subroutine recieve_float_list_input_alt
  

  subroutine recieve_string_input_alt(variable,Name,arguments,position)
    integer, intent(in) :: position
    character(len=100), intent(out) :: variable
    integer :: io !!Test input is ok
    character(len=100)::input
    character(*), intent(in)::name
    character(*), intent(in) :: arguments(:)
    input=arguments(position)
    read(arguments(position),*,IOSTAT=io) variable
    call check_error_input_reading(io,Name,position,input,'string')
  end subroutine recieve_string_input_alt


  subroutine recieve_string_input(variable,Name,position)
    integer, intent(in) :: position
    character(len=100), intent(out) :: variable
    integer :: io !!Test input is ok
    character(len=100)::input
    character(*), intent(in)::name
    call get_command_argument(position,input)
    read(input,*,IOSTAT=io) variable
    call check_error_input_reading(io,Name,position,input,'string')
  end subroutine recieve_string_input


  subroutine check_error_input_reading(io,Name,position,input,Type)
    integer, intent(in) :: position,io
    character(*), intent(in)::name,input,Type
    IF (io /= 0) THEN
       !!write(*,*) 'IOSTAT:',io
       WRITE(stderr,*) 'ERROR: Check input. Something was wrong'
       write(stderr,*) 'Expecting ',Type,' input.'
       write(stderr,*) 'Input the ',Name,'-value at position',position
       write(stderr,*) 'Input value is: "',TRIM(input),'"'
       call exit(-1)
    end IF
  end subroutine check_error_input_reading

  subroutine check_non_negative_integer(variable,Name,help_function)
    integer, intent(in) :: variable
    character(*), intent(in) ::name
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    if(variable.lt.0)then
       call help_function()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'Variable ',Name,' has to be non-negative'
       write(stderr,*) 'Your input was:',variable
       write(*,*)
       call exit(-2)
    end if
  end subroutine check_non_negative_integer
  
  subroutine check_positive_integer(variable,Name,help_function)
    integer, intent(in) :: variable
    character(*), intent(in) ::name
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    if(variable.le.0)then
       call help_function()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'Variable ',Name,' has to be positive'
       write(stderr,*) 'Your input was:',variable
       write(*,*)
       call exit(-2)
    end if
  end subroutine check_positive_integer

  subroutine check_positive_float(variable,Name,help_function)
    real(kind=dp), intent(in) :: variable
    character(*), intent(in) ::name
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    if(variable.le.0d0)then
       call help_function()
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) 'Variable ',Name,' has to be positive'
       write(stderr,*) 'Your input was:',variable
       write(*,*)
       call exit(-2)
    end if
  end subroutine check_positive_float

  
  

  subroutine check_positive_matrix(input_matrix,size,VariableName,help_function)
    integer, intent(IN) :: size,input_matrix(size,size)
    character(len=*) , intent(IN) :: VariableName
    integer :: n1,n2
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    do n1=1,size
       do n2=1,size
          if(input_matrix(n1,n2).lt.0)then 
             !!Enforce equality
             call help_function()
             write(stderr,*) 'ERROR: Input'
             write(stderr,*) 'Your choise of ',VariableName,':'
             call print_int_matrix(input_matrix)
             write(stderr,*) 'Has negative entries!'
             write(stderr,*) 'This is forbidden! Quitting ....'
             write(*,*)
             call exit(-2)
          end if
       end do
    end do
  end subroutine check_positive_matrix
  
  subroutine test_set_translation_list(Global_Flag,Global_Variable,xyval&
       ,VariableName,TestName,Size,help_function)
    integer, intent(in) :: xyval,size
    integer, intent(OUT) :: Global_Variable(size)
    logical, intent(OUT) :: Global_Flag
    character(*), intent(in) :: VariableName,TestName
    integer :: local_variable(size),n
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    local_variable=(/(xyval*(n-1),n=1,size)/)
    
    if(Global_Flag)then !!Test that the flags are the same
       do n=1,size
          if(Global_Variable(n).ne.Local_Variable(n))then !!Enforce equality
             call help_function()
             write(stderr,*) 'ERROR: Input'
             write(stderr,*) 'Your choise of ',VariableName,'=',Global_Variable
             write(stderr,*) 'does not correspond to you choise of '&
                  ,TestName,'=',xyval
             write(*,*)
             call exit(-2)
          end if
       end do
    else !!Just set the flag
       Global_Flag=.TRUE.
       Global_Variable=Local_Variable
    end if
  end subroutine test_set_translation_list


  subroutine test_set_matrix(Global_Flag,Global_Variable,Local_Variable&
       ,VariableName,TestName,TestName2,Size,help_function)
    integer, intent(in) :: size
    integer, intent(INOUT) :: Global_Variable(size,Size)
    logical, intent(INOUT) :: Global_Flag
    integer, intent(IN)   :: Local_Variable(Size,Size)
    character(*), intent(in) ::VariableName,TestName,TestName2
    integer :: n1,n2
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    if(Global_Flag)then !!Test that the flags are the same
       do n1=1,size
          do n2=1,size
             if(Global_Variable(n1,n2).ne.Local_Variable(n1,n2))then 
                !!Enforce equality
                call help_function()
                write(stderr,*) 'ERROR: Input'
                write(stderr,*) 'Your choise of ',VariableName,':'
                call print_int_matrix(Global_Variable)
                write(stderr,*) 'does not correspond to your choise of '&
                     ,TestName,' and ',TestName2,'and should be:'
                call print_int_matrix(local_Variable)
                write(*,*)
                call exit(-2)
             end if
          end do
       end do
    else !!Just set the flag
       Global_Flag=.TRUE.
       Global_Variable=Local_Variable
    end if
  end subroutine test_set_matrix




  subroutine test_set_int_matrix_x2(Global_Flag,Global_Variable,Local_Variable&
       ,VariableName,TestName,TestName2,Size,help_function)
    integer, intent(in) :: size
    integer, intent(INOUT) :: Global_Variable(size,Size)
    logical, intent(INOUT) :: Global_Flag
    integer, intent(IN)   :: Local_Variable(Size,Size)
    character(*), intent(in) ::VariableName,TestName,TestName2
    integer :: n1,n2
    interface
       subroutine help_function()
       end subroutine help_function
    end interface
    
    if(Global_Flag)then !!Test that the flags are the same
       write(stderr,*) 'ERROR: Internal'
       write(stderr,*) 'This clause should not happen'
    else
       do n1=1,size
          do n2=1,size
             if(modulo(Local_Variable(n1,n2),2).ne.0)then 
                !!Enforce equality
                call help_function()
                write(stderr,*) 'ERROR: Input'
                write(stderr,*) 'Your choise of ',TestName,' and ',TestName2
                write(stderr,*) 'would not yeild an integer ',VariableName
                write(stderr,*) 'since 2 x ',VariableName,' is:'
                call print_int_matrix(local_Variable)
                write(*,*)
                call exit(-2)
             end if
          end do
       end do
       Global_Flag=.TRUE.
       Global_Variable=Local_Variable/2
    end if
  end subroutine test_set_int_matrix_x2

  

  subroutine test_set_compability2(Global_Flag,Global_Variable,Local_Variable&
       ,VariableName,help_function)
    integer, intent(OUT) :: Global_Variable
    logical, intent(OUT) :: Global_Flag
    integer, intent(in) :: local_Variable
    character(*), intent(in) :: VariableName
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    if(Global_Flag)then !!Test taht the flasgs are the same
       if(Global_Variable.ne.Local_Variable)then !!They have to be equal
          call help_function()
          write(stderr,*) 'ERROR: Input'
          write(stderr,*) 'Your choise of ',VariableName,'=',Global_Variable
          write(stderr,*) 'does not correspont the one given by the kappa-matrix'
          write(*,*)
          call exit(-2)
       end if
    else !!Just set the flag
       Global_Flag=.TRUE.
       Global_Variable=Local_Variable
    end if
  end subroutine test_set_compability2
  
  subroutine test_set_compability(Global_Flag,Global_Variable,Local_Variable&
       ,VariableName,TestName,TestValue,help_function)
    integer, intent(OUT) :: Global_Variable
    logical, intent(OUT) :: Global_Flag
    integer, intent(in) :: local_Variable,TestValue
    character(*), intent(in) :: VariableName,TestName
    interface
       subroutine help_function()
       end subroutine help_function
    end interface

    if(Global_Flag)then !!Test that the flags are the same
       if(Global_Variable.ne.Local_Variable)then !!They have to be equal
          call help_function()
          write(stderr,*) 'ERROR: Input'
          write(stderr,*) 'Your choise of ',VariableName,'=',Global_Variable
          write(stderr,*) 'does not correspond to you choise of '&
               ,TestName,'=',TestValue
          write(*,*)
          call exit(-2)
       end if
    else !!Just set the flag
       Global_Flag=.TRUE.
       Global_Variable=Local_Variable
    end if
  end subroutine test_set_compability

  
  subroutine set_default_integer(Flag,Variable,Default_Value,Long_Name,Var_Name)
    logical,intent(inout) :: Flag
    integer,intent(inout) :: Variable
    integer,intent(in) :: Default_Value
    character(*), intent(in) :: Long_Name, Var_name
    if(.not.Flag)then
       write(stderr,*) 'WARNING: No ',Long_Name,' have been specified.'
       write(stderr,*) '         Defaulting to ',Var_Name,'=',Default_Value
       Flag=.TRUE.
       Variable=Default_Value
    end if
  end subroutine set_default_integer

  subroutine set_default_real(Flag,Variable,Default_Value,Long_Name,Var_Name)
    logical,intent(inout) :: Flag
    real(KIND=dp),intent(inout) :: Variable
    real(KIND=dp),intent(in) :: Default_Value
    character(*), intent(in) :: Long_Name, Var_name
    if(.not.Flag)then
       write(stderr,*) 'WARNING: No ',Long_Name,' have been specified.'
       write(stderr,*) '         Defaulting to ',Var_Name,'=',Default_Value
       Flag=.TRUE.
       Variable=Default_Value
    end if
  end subroutine set_default_real

  
end module input_misc

