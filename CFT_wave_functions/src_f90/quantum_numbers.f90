MODULE quantum_numbers
  
  USE global_ed

  IMPLICIT NONE

  INTEGER, PARAMETER :: nqns = 2  ! two quantum numbers
  INTEGER, POINTER :: qns(:,:)    ! quantum numbers of the single-particle states

CONTAINS

  SUBROUTINE init_qns(Nup,Ndn,Ns)
    INTEGER, INTENT(IN) :: Nup,Ndn,Ns
    INTEGER :: N1, k, i1,j

    qmom = 1 ! momentum at first column
    qsz = 2  ! spin at the second

    k = 0
    IF (Nup*Ndn/=0) THEN ! unpolarized
       N1 = 2*Ns
       ALLOCATE(qns(0:N1-1,nqns))
       DO j = -1,1,2
          DO i1 = 0, Ns-1
             qns(k,qmom) = i1
             qns(k,qsz) = j
             k = k+1
          END DO
       END DO
    ELSE                 ! polarized
       N1 = Ns
       ALLOCATE(qns(0:N1-1,nqns))
       DO i1 = 0, Ns-1
          qns(k,qmom) = i1
          qns(k,qsz) = 1
          k = k+1
        END DO
    END IF
    WRITE(*,'(/,A)') 'SP basis is:'
    WRITE(*,*) '************'
    WRITE(*,*) '         #  k-value spin'
    DO j=0,N1-1
       WRITE(*,'(TR9,I2,TR5,I2,TR4,I2)') &
            j, qns(j,1), qns(j,2)
    END DO
    
!    IF (MODULO(Qnomin*(Nele-1),2)==0) THEN
!      WRITE(*,*) 'periodic bcs'
!      bc = 0.d0  ! periodic boundary conditions
!    ELSE      
!      WRITE(*,*) 'antiperiodic bcs'
!      bc = 0.5d0 ! antiperiodic boundary conditions
!    END IF

  END SUBROUTINE init_qns

  FUNCTION check_qns( fockstate )
    INTEGER, INTENT(IN) :: fockstate(:)
    ! enforce constraints on the basis quantum numbers,
    ! conservation laws for additive quantitites in particular
    LOGICAL :: check_qns
    ! FIXME: check if this should be MOD or MODULO
    check_qns = ( SUM(qns(fockstate,qsz))==S_z .AND. MODULO(SUM(qns(fockstate,qmom))-Kmom,working_Ns)==0 )
  END FUNCTION check_qns

END MODULE quantum_numbers
