MODULE sparse
  !
  ! This modules is related to sparse matrices.
  !
  use typedef
  IMPLICIT NONE
  
  TYPE spa 
!!! spa like 'health resort'
!!! general storage by rows
!!!
!!!  a(1:nz)      Non-zero elements of matrix A_original(N,N)
!!!  ia(1:n+1)    Starting indices of each row (also the ending...[n+1])
!!!  ja(1:nz)     Column index for each element
!!!  nz           Number of non-zero elements of matrix A_original(N,N)
!!!  n            The dimension of matrix A_original(N,N)
     complex(kind=dpc), POINTER, DIMENSION(:) :: a
     INTEGER, POINTER, DIMENSION(:) :: ia, ja
     INTEGER :: nz, n
  END TYPE spa
CONTAINS
  SUBROUTINE spa_export(as,filex)
    TYPE(spa) :: as
    CHARACTER(*), INTENT(IN) :: filex
    INTEGER :: i
    INTEGER :: ch1 = 735
    INTEGER :: ch2 = 736
    INTEGER :: ch3 = 737
    OPEN(ch1,FILE=TRIM(filex)//'.a.dat',STATUS='unknown')
    OPEN(ch2,FILE=TRIM(filex)//'.ja.dat',STATUS='unknown')
    OPEN(ch3,FILE=TRIM(filex)//'.ia.dat',STATUS='unknown')
    DO i = 1, as%nz
       WRITE(ch1,*) real(as%a(i),dp),aimag(as%a(i)) ! matrix elements
       WRITE(ch2,*) as%ja(i) ! column indices
    END DO
    DO i = 1, as%n+1
       WRITE(ch3,*) as%ia(i) ! starting indices
    END DO
    CLOSE(ch1)
    CLOSE(ch2)
    CLOSE(ch3)
  END SUBROUTINE spa_export
  
  FUNCTION spa_get(as,i,j)
    ! This finds the element i,j from a sparse matrix as.
    TYPE(spa) :: as
    complex(KIND=dp) :: spa_get
    INTEGER :: i, j, ii
    spa_get=0.d0
    DO ii=as%ia(i), as%ia(i+1)-1
       IF (as%ja(ii)==j) THEN
          spa_get=as%a(ii)
          RETURN !!Fund element, return
       END IF
       IF (as%ja(ii)>j) RETURN !!No element exists, return
    END DO
  END FUNCTION spa_get
  
  subroutine spa_write(as,n) !!Prints the sparse matrix!!!
    TYPE(spa), intent(IN) :: as !Matrix
    INTEGER, intent(IN) :: n !Size of matrix
    integer:: i,j
    complex(KIND=dp), dimension(n) :: vec
    
    do i=1,n
       do j=1,n
          vec(j)=spa_get(as,i,j) !Store in vector to print to one line
       end do
       write(*,*) vec
    end do
    
  end subroutine spa_write
  
  
  FUNCTION spa_mul(as,b) RESULT(r)
    ! This is a sparse matrix 'as' times vector 'b'
    TYPE(spa) :: as
    REAL(KIND=dp) :: b(:)
    REAL(KIND=dp),DIMENSION(SIZE(b)) :: r
    INTEGER :: i, j
    r=0.d0
    DO i=1, SIZE(b,1)
       DO j=as%ia(i), as%ia(i+1)-1
          r(i)=r(i)+real(as%a(j),dp)*b(as%ja(j))
       END DO
    END DO
  END FUNCTION spa_mul
  
  FUNCTION spa_mulz(as,b) RESULT(r)
    ! This is a sparse matrix 'as' times vector 'b'
    TYPE(spa) :: as
    complex(KIND=dpc) :: b(:)
    complex(KIND=dp),DIMENSION(SIZE(b)) :: r
    INTEGER :: i, j
    
    r=0.d0
    DO i=1, SIZE(b,1)
       DO j=as%ia(i), as%ia(i+1)-1
          r(i)=r(i)+as%a(j)*b(as%ja(j))
       END DO
    END DO
  END FUNCTION spa_mulz
  
  SUBROUTINE spa_mul2(as,nx,b,ind1,ind2) !!multiplying a real vector
    ! This is a sparse matrix 'as' times vector 'b'
    ! for ARPACK stuff...
    TYPE(spa) :: as      ! matrix
    INTEGER, INTENT(IN) :: nx ! dimension
    REAL(KIND=dp) :: b(:)  ! working data
    INTEGER, INTENT(IN) :: ind1, ind2 ! starting indices
    INTEGER :: i, j
    b(ind2:ind2+nx-1) = 0.d0
    DO i=1, nx
       DO j=as%ia(i), as%ia(i+1)-1
          b(i+ind2-1)=b(i+ind2-1)+real(as%a(j)*b(ind1-1+as%ja(j)),dp)
       END DO
    END DO
  END SUBROUTINE spa_mul2
  
  SUBROUTINE spa_mul2z(as,nx,b,ind1,ind2)  !multiplying a complex vector
    ! This is a sparse matrix 'as' times vector 'b'
    ! for ARPACK stuff...
    TYPE(spa) :: as      ! matrix
    INTEGER, INTENT(IN) :: nx ! dimension
    complex(KIND=dpc) :: b(:)  ! working data
    INTEGER, INTENT(IN) :: ind1, ind2 ! starting indices
    INTEGER :: i, j
    b(ind2:ind2+nx-1) = 0.d0
    DO i=1, nx
       DO j=as%ia(i), as%ia(i+1)-1
          b(i+ind2-1)=b(i+ind2-1)+as%a(j)*b(ind1-1+as%ja(j))
       END DO
    END DO
  END SUBROUTINE spa_mul2z
  
  SUBROUTINE ArpackSolver(hs, eigList, Nene, Nsize, evects)
    use sort
    TYPE(spa) :: hs                      ! Sparse matrix to be diagonalized
    REAL(KIND=dp), INTENT(OUT) :: eigList(:)      ! ground state energy
    INTEGER, INTENT(IN) :: Nsize         ! basis size 
    INTEGER, INTENT(IN) :: Nene          ! number of levels needed = SIZE(eigList,1)
    complex(KIND=dpc), intent(OUt) :: evects(Nsize,Nene)! Eigenvectors
    
!!!     %------------------------------------------------------%
!!!     | Storage Declarations:                                |
!!!     |                                                      |
!!!     | The maximum dimensions for all arrays are            |
!!!     | set here to accommodate a problem size of N          |
!!!     |                                                      |
!!!     | NEV is the number of eigenvalues requested.          |
!!!     |     See specifications for ARPACK usage below.       |
!!!     |                                                      |
!!!     | NCV is the largest number of basis vectors that will |
!!!     |     be used in the Implicitly Restarted Arnoldi      |
!!!     |     Process.  Work per major iteration is            |
!!!     |     proportional to N*NCV*NCV.                       |
!!!     %------------------------------------------------------%
!!!
    INTEGER :: ldv,ldz
    
!!!     %--------------%
!!!     | Local Arrays |
!!!     %--------------%
    
    complex(KIND=dpc), ALLOCATABLE ::  resid(:), v(:,:),  workd(:),  workl(:), d(:), ax(:) , workev(:),z(:,:),tvec(:)
    REAL(KIND=dp), ALLOCATABLE ::  rwork(:)
    LOGICAL, ALLOCATABLE :: sel(:) 
    INTEGER :: iparam(11), ipntr(14)
    CHARACTER ::     bmat*1, which*2
    INTEGER ::       ido, n, nev, ncv, lworkl, info, ierr
    integer ::       j, ishfts, maxitr, mode1, nconv
    LOGICAL ::       rvec
    REAL(KIND=dp) :: tol
    complex(KIND=dpc) :: sigma
    INTEGER :: i1

!!!     %----------------------------%
!!!     | SORTING OF EIGENVALUES     |
!!!     %----------------------------%
    REAL(KIND=dp) :: energyListSort(Nene) !!The sorted energies
    integer :: Order(Nene)   !!The order of the sorted energies
    

!!!     %-----------------------------%
!!!     | BLAS & LAPACK routines used |
!!!     %-----------------------------%
!!!
    REAL(KIND=dp), EXTERNAL :: dznrm2,dlamch !, daxpy
!!!
!!!     %--------------------%
!!!     | INTRINSIC FUNCTION |
!!!     %--------------------%
!!!
!!!     %-----------------------%
!!!     | Executable Statements |
!!!     %-----------------------%
!!!
!!!     %-------------------------------------------------%
!!!     | The following INCLUDE statement and assignments |
!!!     | initiate trace output from the internal         |
!!!     | actions of ARPACK.  See debug.doc in the        |
!!!     | DOCUMENTS directory for usage.  Initially, the  |
!!!     | most useful information will be a breakdown of  |
!!!     | time spent in the various stages of computation |
!!!     | given by setting msaupd = 1.                    |
!!!     %-------------------------------------------------%
!!!
!!!      INCLUDE '/home/jms/arpack/ARPACK/SRC/debug.h' !!This is obsolete
!!!      ndigit = -3
!!!      logfil = 6
!!!      msgets = 0
!!!      msaitr = 0 
!!!      msapps = 0
!!!      msaupd = 1
!!!      msaup2 = 0
!!!      mseigt = 0
!!!      mseupd = 0
!!!     
!!!     %-------------------------------------------------%
!!!     | The following sets dimensions for this problem. |
!!!     %-------------------------------------------------%
!!!
!!!     %-----------------------------------------------%
!!!     |                                               | 
!!!     | Specifications for ARPACK usage are set       | 
!!!     | below:                                        |
!!!     |                                               |
!!!     |    1) NEV : number of  eigenvalues to be      |  
!!!     |       computed.                               | 
!!!     |                                               |
!!!     |    2) NCV : length of the Arnoldi             |
!!!     |       factorization                           |
!!!     |                                               |
!!!     |    3) This is a standard problem              |
!!!     |         (indicated by bmat  = 'I')            |
!!!     |                                               |
!!!     |    4) Ask for the NEV eigenvalues of          |
!!!     |       largest magnitude                       |
!!!     |         (indicated by which = 'LM')           |
!!!     |       See documentation in DSAUPD for the     |
!!!     |       other options SM, LA, SA, LI, SI.       | 
!!!     |                                               |
!!!     | Note: NEV and NCV must satisfy the following  |
!!!     | conditions:                                   |
!!!     |              NEV <= MAXNEV - 1                |
!!!     |              NEV+1 < NCV <= MAXNCV            |
!!!     %-----------------------------------------------%
!!!      
    
    ! Calculate the eigenvalues and eigenvactors
    rvec = .TRUE.
    
    !FIXME: choise of ncv is problem dependant
    n = Nsize
    nev = Nene
    ncv = min(2*nev+8,n) !recomended in ARPACK documentation
    !! has to fullfill ncv > Nev+1   and ncv <= n 
    ldv = n
    ldz = n
    bmat  = 'I'
    which = 'SR' ! smallest real eigenvalues
    lworkl = ncv*(3*ncv+5)
    
    ! For the specification of the sises opf the
    
    !For techical reasons in ARPACK:
    !number of eigenvalus has to be lower than size of hamiltonian (makes sence)
    !Also the hamiltonian has to be at least three dimentional.
    if(nev.ge.(Nsize-1))then
       write(*,*) 'Number of requested eigenvalues are to many',nev
       write(*,*) 'Hamiltonian has size',Nsize
       !write(*,*) 'Hamiltonian is:'
       !call spa_write(hs,n)
       write(*,*) 'Diagonalize by hand or other method?'
       call exit(-1)
    end if
    
    
    ALLOCATE( &
         & v(n,ncv),    &
         & workl(lworkl),     &
         & workd(3*n),     &
         & d(nev+1),         &
         & resid(n),       &
         & ax(n),          &
         & tvec(n),        &
         & sel(ncv),       &
         & workev(2*ncv),  &
         & z(n,nev),    &
         & rwork(ncv))
    
    
!!!     %-----------------------------------------------------%
!!!     |                                                     |
!!!     | Specification of stopping rules and initial         |
!!!     | conditions before calling DSAUPD                    |
!!!     |                                                     |
!!!     | TOL  determines the stopping criterion.             |
!!!     |                                                     |
!!!     |      Expect                                         |
!!!     |           ABS(lambdaC - lambdaT) < TOL*ABS(lambdaC) |
!!!     |               computed   true                       |
!!!     |                                                     |
!!!     |      IF TOL .LE. 0,  THEN TOL <- macheps            |
!!!     |           (machine PRECISION) is used.              |
!!!     |                                                     |
!!!     | IDO  is the REVERSE COMMUNICATION PARAMETER         |
!!!     |      used to specify actions to be taken on RETURN  |
!!!     |      from DSAUPD. (See usage below.)                |
!!!     |                                                     |
!!!     |      It MUST initially be set to 0 before the first |
!!!     |      CALL to DSAUPD.                                | 
!!!     |                                                     |
!!!     | INFO on ENTRY specifies starting vector information |
!!!     |      and on RETURN indicates error codes            |
!!!     |                                                     |
!!!     |      Initially, setting INFO=0 indicates that a     | 
!!!     |      random starting vector is requested to         |
!!!     |      start the ARNOLDI iteration.  Setting INFO to  |
!!!     |      a nonzero value on the initial CALL is used    |
!!!     |      IF you want to specify your own starting       |
!!!     |      vector (This vector must be placed in RESID.)  | 
!!!     |                                                     |
!!!     | The work array WORKL is used in DSAUPD as           | 
!!!     | workspace.  Its DIMENSION LWORKL is set as          |
!!!     | illustrated below.                                  |
!!!     |                                                     |
!!!     %-----------------------------------------------------%
    
    sel=.true.
    tol = dlamch('EPS') !!machine presihion computed my ARPACK
    write(*,*) 'Machine precision is:',tol
    tol=tol**(3.d0/4.d0) !! wto third of machine rpesiction is enough
    write(*,*) 'Tolerance is set to: ',tol
    info = 0   !! Reset info
    ido = 0 !!First call to reverse comunication
    
    
    
    
!!!     %---------------------------------------------------%
!!!     | Specification of Algorithm Mode:                  |
!!!     |                                                   |
!!!     | This PROGRAM uses the exact shift strategy        |
!!!     | (indicated by setting PARAM(1) = 1).              |
!!!     | IPARAM(3) specifies the maximum number of Arnoldi |
!!!     | iterations allowed.  Mode 1 of DSAUPD is used     |
!!!     | (IPARAM(7) = 1). All these options can be changed |
!!!     | by the user. For details see the documentation in |
!!!     | DSAUPD.                                           |
!!!     %---------------------------------------------------%
    
    ishfts = 1
    maxitr = 300 
    mode1 = 1
    
    iparam(1) = ishfts
    
    iparam(3) = maxitr
    
    iparam(7) = mode1 !!Mode = A*x = lambda*x.
    
    
    
    
    
!!!     %------------------------------------------------%
!!!     | M A I N   L O O P (Reverse communication loop) |
!!!     %------------------------------------------------%
    do 
!!!        %---------------------------------------------%
!!!        | Repeatedly CALL the routine DSAUPD and take | 
!!!        | actions indicated by PARAMETER IDO until    |
!!!        | either convergence is indicated or maxitr   |
!!!        | has been exceeded.                          |
!!!        %---------------------------------------------%
       CALL znaupd ( ido, bmat, n, which, nev, tol, resid,  &
            &                 ncv, v, ldv, iparam, ipntr, workd, workl, &
            &                 lworkl, rwork, info )
       
       
       IF (ido .EQ. -1 .OR. ido .EQ. 1) THEN
!!!           %--------------------------------------%
!!!           | Perform matrix vector multiplication |
!!!           |              y <--- OP*x             |
!!!           | The user should supply his/her own   |
!!!           | matrix vector multiplication routine |
!!!           | here that takes workd(ipntr(1)) as   |
!!!           | the input, and RETURN the RESULT to  |
!!!           | workd(ipntr(2)).                     |
!!!           %--------------------------------------%
          
!!!           CALL av (nx, workd(ipntr(1)), workd(ipntr(2)))
          CALL spa_mul2z(hs,n,workd,ipntr(1),ipntr(2))
          
          !write(*,*) 'Matrix Multiplication done'
          
       ELSE if(ido.eq.99)then
          ! routine finished
          exit
       else
          WRITE(*,*) 'Unrecognized ido:',ido
          write(*,*) 'stopping....'
          call exit(-1)
       END IF
!!!           %-----------------------------------------%
!!!           | L O O P   B A C K to CALL DSAUPD again. |
!!!           %-----------------------------------------%         
    END do
    
    
    
!!!     %----------------------------------------%
!!!     | Either we have convergence or there is |
!!!     | an error.                              |
!!!     %----------------------------------------%
    
    
    !Error check - diagonalization
    IF ( info .LT. 0 ) THEN
!!!        %--------------------------%
!!!        | Error message. Check the |
!!!        | documentation in ZNAUPD. |
!!!        %--------------------------%
       write(*,*)
       write(*,*) ' Error with znaupd, info = ', info
       write(*,*) ' Check documentation in ARPACK:znaupd '
       write(*,*)
       call exit(-1)
    end IF
    
    
    
    
!!!        %-------------------------------------------%
!!!        | No fatal errors occurred.                 |
!!!        | Post-Process using DSEUPD.                |
!!!        |                                           |
!!!        | Computed eigenvalues may be extracted.    |  
!!!        |                                           |
!!!        | Eigenvectors may be also computed now IF  |
!!!        | desired.  (indicated by rvec = .TRUE.)    | 
!!!        |                                           |
!!!        | The routine DSEUPD now called to DO this  |
!!!        | post processing (Other modes may require  |
!!!        | more complicated post processing than     |
!!!        | mode1.)                                   |
!!!        |                                           |
!!!        %-------------------------------------------%
    
    
    
    ! Calculate the eigenvalues and eigenvactors
    CALL zneupd ( rvec, 'A', sel, d, z, ldz, sigma,workev, &
         &         bmat, n, which, nev, tol, resid, ncv, v, ldv, & 
         &         iparam, ipntr, workd, workl, lworkl, rwork,ierr )
    
!!!         %----------------------------------------------%
!!!         | Eigenvalues are returned in the first column |
!!!         | of the two dimensional array D and the       |
!!!         | corresponding eigenvectors are returned in   |
!!!         | the first NCONV (=IPARAM(5)) columns of the  |
!!!         | two dimensional array V IF requested.        |
!!!         | Otherwise, an orthogonal basis for the       |
!!!         | invariant subspace corresponding to the      |
!!!         | eigenvalues in D is returned in V.           |
!!!         %----------------------------------------------%
    
    
    
    ! Error check - eigenvalue extraction
    IF ( ierr .NE. 0) THEN
!!!            %------------------------------------%
!!!            | Error condition:                   |
!!!            | Check the documentation of zNEUPD. |
!!!            %------------------------------------%
       write(*,*) 
       write(*,*) ' Error with _zneupd, info = ', ierr
       write(*,*) ' Check the documentation of _zneupd. '
       write(*,*)
       call exit(-1)
    end IF
    
    
    
    nconv =  iparam(5)
    do j=1, nconv
       
!!!               %---------------------------%
!!!               | Compute the residual norm |
!!!               |                           |
!!!               |   ||  A*x - lambda*x ||   |
!!!               |                           |
!!!               | for the NCONV accurately  |
!!!               | computed eigenvalues and  |
!!!               | eigenvectors.  (iparam(5) |
!!!               | indicates how many are    |
!!!               | accurate to the requested |
!!!               | tolerance)                |
!!!               %---------------------------%
       
       tvec(1:n)=v(1:n,j)
       ax(1:n) = spa_mulz( hs, tvec)
       CALL zaxpy(n, -d(j), tvec, 1, ax, 1)
       write(*,*) 'norm abs & relative:', dznrm2(n, ax, 1)
       !d(j,2) = d(j,2) / ABS(d(j,1))
    end do
    
    
    
!!!            %-----------------------------%
!!!            | Display computed residuals. |
!!!            %-----------------------------%
    
    CALL dmout(6, nconv, 2, d, ncv, -6,&
         &            'Ritz values and relative residuals')
    
    
!!!         %-------------------------------------------%
!!!         | PRINT additional convergence information. |
!!!         %-------------------------------------------%
    
    
    IF ( info .EQ. 1) THEN
       WRITE(*,*) ' '
       WRITE(*,*) ' Maximum number of iterations reached.'
       WRITE(*,*) ' '
    ELSE IF ( info .EQ. 3) THEN
       WRITE(*,*) ' ' 
       WRITE(*,*) ' No shifts could be applied during implicit',&
            &                ' Arnoldi update, try increasing NCV.'
       WRITE(*,*) ' '
    END IF
    
    
    
    WRITE(*,*) ' '
    WRITE(*,*) ' INFO '
    WRITE(*,*) ' ====== '
    WRITE(*,*) ' '
    WRITE(*,*) ' Size of the matrix is ', n
    WRITE(*,*) ' The number of Ritz values requested is ', nev
    WRITE(*,*) ' The number of Arnoldi vectors generated', &
         &             ' (NCV) is ', ncv
    WRITE(*,*) ' What portion of the spectrum: ', which
    WRITE(*,*) ' The number of converged Ritz values is ', & 
         &               iparam(5)
    WRITE(*,*) ' The number of Implicit Arnoldi update', &
         &             ' iterations taken is ', iparam(3)
    WRITE(*,*) ' The number of OP*x is ', iparam(9)
    WRITE(*,*) ' The convergence criterion is ', tol
    WRITE(*,*) ' '
    
    
    
    
!!!     %---------------------------%
!!!     | Transfer eigenvalues      |
!!!     %---------------------------%
    
    !eigenvalues
    !!:Since the matrix is hermitian we know the eigenvalues are real
    eigList(1:nev) = real(d(1:nev),dp)
    WRITE(*,'(/,I3,A,/,T4A,/)') iparam(5), ' converged eigenvalues.',&
         &'Here are the lower ones in one coolumn, shifted such that E_0=0'
    DO i1 = 1,nev
       !!We scale with the lowest energy value
       WRITE(*,'(T8,I3,TR5,F16.12,TR1)') i1,eigList(i1)-minList(EigList,nev)
    END DO
    WRITE(*,*) ''
    
    !The eigenvalues are not allways sorted!!
    !!Sort the Energies before exporting first !!
    call sort_dp(eigList,energyListSort,order,nev)
    
    write(*,*) 'Eigs_ordered   Order   Eigs'
    do i1=1,nev
       write(*,*) energyListSort(i1), order(i1), eigList(i1)
    end do
    
    !!Write the eneriges in correct order!
    eigList=energyListSort    
    
    ! eigenvectors
    write(*,*) 'recovering eigenvectors'
    do i1=1,nev
       !!The eigenvalues are not sorted, so that has to be done...
       evects(1:Nsize,i1) = z(1:Nsize,order(i1))
    end do
    
    
    
  END SUBROUTINE ArpackSolver
  
  real(kind=dp) function minList(RVec,Size) result(min_elem)
    !!Compute the minimal value of an array of elements
    integer :: Size,i
    real(kind=dp), intent(in) :: RVec(Size)
    min_elem=Rvec(1)
    do i=2,Size
       min_elem=min(min_elem,Rvec(i))
    end do
  end function minList
  
END MODULE sparse

