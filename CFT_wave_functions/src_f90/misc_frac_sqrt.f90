module misc_frac_sqrt
  
  implicit none
  
  !!FIXME 8/9-2014 
  !!There should be testcase to the differnt sqrt operations

  public
  
  
contains
  
  integer function gcd_pd(u, v) result(gcd_res)
    !!Compute the positive definite gcd
    integer, intent(in) :: u, v
    gcd_res=remove_possible_minus_sign(gcd(u,v))
  end function gcd_pd
  
  recursive function gcd(u, v) result(gcd_res)
    !!Computes the gretest comon divisor!!
    !!Thanx http://rosettacode.org/wiki/Greatest_common_divisor#Fortran
    integer             :: gcd_res
    integer, intent(in) :: u, v
    if(u.eq.0)then !!Specail case of one being zero
       !write(*,*) 'Zero u'
       gcd_res=v 
    elseif(v.eq.0)then !!Specail case of one being zero
       !write(*,*) 'Zero v'
       gcd_res=u
    elseif(u.lt.0)then !!Negative values givse negative gcd
       !write(*,*) 'Negative u!'
       gcd_res=-gcd(-u,v)
    elseif(v.lt.0)then !!Negative values givse negative gcd
       !write(*,*) 'Negative v!'
       gcd_res=-gcd(u,-v)
    elseif(mod(u, v).ne.0)then
       gcd_res = gcd(v, mod(u, v))
    else
       gcd_res = v
    end if
  end function gcd

  function gcd_vector(Size,V) result(gcd_res)
    !!Computes the gretest comon divisor of the integer vector V!!
    integer             :: gcd_res,n
    integer, intent(in) :: Size,V(Size)
    !!Start bu setting gcd=0 since that has gcd(a,0)=a 
    gcd_res=0
    do n=1,Size
       gcd_res=gcd(V(n),gcd_res)
    end do
  end function gcd_vector
  

  subroutine add_fractions(p1,q1,p2,q2,p_out,q_out)
    integer, intent(IN) :: p1,p2,q1,q2
    integer, intent(OUT) :: p_out,q_out
    !!In case and of the in are any opf the out!
    integer :: p_work,q_work
    !!Adds together two fractional numbers
    !!write(*,'(A,I3,A,I3,A,I3,A,I3)') 'fractions in:',p1,'/',q1,' + ',p2,'/',q2
    p_work=p1*q2+p2*q1
    q_work=q1*q2
    call reduce_fraction(p_work,q_work)
    q_out=q_work
    p_out=p_work
  end subroutine add_fractions

  subroutine multiply_fractions(p1,q1,p2,q2,p_out,q_out)
    !!Multiplies the factors p1/sqrt(q1) and p2/sqrt(q2)
    !!and reduces the content
    integer, intent(IN) :: p1,p2,q1,q2
    integer, intent(OUT) :: p_out,q_out
    integer :: p_work,q_work
    !!write(*,'(A,I10,A,I10,A,I10,A,I10,A)') 'multiplied fractions are:'&
    !!,p1,'/',q1,'  * ',p2,'/',q2
    if((p1.eq.0).or.(p2.eq.0))then !!The result is zero
       p_work=0
       q_work=1
    else
       p_work=p1*p2
       q_work=q1*q2
       call reduce_fraction(p_work,q_work)
    end if
    q_out=q_work
    p_out=p_work
    !!write(*,'(A,I10,A,I10,A)') 'fraction out:',p_out,'/',q_out
  end subroutine multiply_fractions
  
  subroutine reduce_fraction(p,q)
    !!Reduce the fraction p/q by removing the gretest common divisor
    integer, intent(INOUT) :: p,q
    integer :: rel_gcd
    if(p.eq.0)then
       q=1
    else
       rel_gcd=gcd(p,q)
       p=p/rel_gcd
       q=q/rel_gcd
       if(q.lt.1)then !!Denominator should be positive
          q=-q
          p=-p
       end if
    end if
  end subroutine reduce_fraction

  
  
  subroutine add_sqrt_fractions(p1,s1,p2,s2,p_out,s_out)
    !!Computes the addition fo two tersm with square roots in the denominator.
    !!This functions assumes the result can also be written on the form of
    !! an integer divides by a square root.
    integer, intent(IN) :: p1,p2,s1,s2
    integer, intent(OUT) :: p_out,s_out
    !!In case and of the in are any opf the out!
    integer :: p_work,s_work,s1_rel,s2_rel
    integer :: rel_gcd

    if(p1.eq.0)then
       p_work=p2
       s_work=s2
    elseif(p2.eq.0)then
       p_work=p1
       s_work=s1
    else
       !!First we factor way the join part of the denominator
       rel_gcd=gcd(s1,s2)
       s1_rel=s1/rel_gcd
       s2_rel=s2/rel_gcd
       !write(*,*) '------------------'
       !write(*,*) 'Joint factor:',rel_gcd
       !write(*,*) 'Left:,s1_rel,s2_rel:',s1_rel,s2_rel
       !!The adde the numbers together, 
       !!if this wirks they have to be squares of integers
       s1_rel=isqrt(s1_rel)
       s2_rel=isqrt(s2_rel)
       
       call add_fractions(p1,s1_rel,p2,s2_rel,p_work,s_work)
       
       s_work=s_work**2*rel_gcd
    end if


    !!write(*,'(A,I3,A,I3)') 'fraction out:',p_out,'/',q_out
    !!Transfer to the output variables and ccheck they are minimal
    call do_reduce_sqrt_fraction(p_work,s_work,p_out,s_out)
  end subroutine add_sqrt_fractions


  subroutine multiply_sqrt_number(c1,r1,c2,r2,c_out,r_out)
    !!Multiplies the factors c1/sqrt(r1) and c2/sqrt(r2)
    !!and reduces the content
    integer, intent(IN) :: c1,c2,r1,r2
    integer, intent(OUT) :: c_out,r_out
    integer :: c_tmp,r_tmp
    !write(*,'(A,I10,A,I10,A,I10,A,I10,A)') 'multiplied fractions are:'&
    !,c1,'/sqrt(',r1,')  * ',c2,'/sqrt(',r2,')'
    if((c1.eq.0).or.(c2.eq.0))then !!The result is zero
       c_out=0
       r_out=1
    else
       c_tmp=c1
       c_out=c2
       r_tmp=r1
       r_out=r2
       !!Reduce in all possible ways!!
       call reduce_sqrt_fraction(c_tmp,r_tmp)
       call reduce_sqrt_fraction(c_out,r_out)
       call reduce_mult_sqrt_fraction(c_tmp,r_out,r_tmp)
       call reduce_mult_sqrt_fraction(c_out,r_out,r_tmp)
       c_out=c_out*c_tmp
       r_out=r_out*r_tmp
       call reduce_sqrt_fraction(c_out,r_out)
    end if
    !write(*,'(A,I10,A,I10,A)') 'fraction out:',c_out,'/sqrt(',r_out,')'
  end subroutine multiply_sqrt_number


  subroutine do_reduce_sqrt_fraction(cin,rin,cout,rout)
    !!Same as reduce_sqrt_fraction, but in and out are differnt varaibles
    integer, intent(IN) :: cin,rin
    integer, intent(OUT) :: cout,rout
    cout=cin
    rout=rin
    call reduce_sqrt_fraction(cout,rout)
  end subroutine do_reduce_sqrt_fraction

  recursive subroutine reduce_sqrt_fraction(c,r)
    !!Reducec the reaction c/sqrt(r) as faar as possible.
    !!Here we ned to gcd(c,r)=gcd(c,r/gcd(c,r))
    integer, intent(INOUT) :: c,r
    integer :: gcd1,gcd2,gcda,gcdb
    integer :: d,d2
    
    if(c.eq.0)then !!If c=0 the r=1 trivially
       r=1
    end if
    !!Possible things to divide away
    !!Anything that can be dicided away must be at least present in both c and r
    gcd1=gcd_pd(c,r)
    !write(*,*) '---rsf----'
    !write(*,*) 'Reducing',c,'/sqrt(',r,')'
    !write(*,*) 'GCD1 of',c,r,'is',gcd1
    if(gcd1.gt.1)then
       !write(*,*) 'reducition maybe possible'
       gcd2=gcd_pd(gcd1,r/gcd1)
       !write(*,*) 'GCD2 of',gcd1,r/gcd1,'is',gcd2
       !write(*,*) 'reduction is possbile by',gcd2
       if(gcd2.gt.1)then
          !write(*,*) 'Reducing...'
          c=c/gcd2
          r=r/(gcd2*gcd2)
             !!Start the proces over again
          call reduce_sqrt_fraction(c,r)
       else
          !write(*,*) 'Try to find factors in gcd1 present in r/gcd1'
          do d=2,gcd1
             d2=d**2
             !write(*,*) 'd=',d,'d^2=',d2
             gcda=gcd_pd(c,d)
             gcdb=gcd_pd(r,d2)
             !write(*,*) 'gcda=',gcda,'gcdb=',gcdb
             
             if(gcda.eq.d.and.gcdb.eq.d2)then
                !write(*,*) 'Possible to divide by:',d
                c=c/d
                r=r/d2
                   !!Start the proces over again
                call reduce_sqrt_fraction(c,r)
                !!Return from function sine stuff is done
                return
             end if
          end do
       end if
    end if
  end subroutine reduce_sqrt_fraction
  
  integer function remove_possible_minus_sign(n) result(m)
    integer, intent(IN) :: n
    !!Remove possible minus sign
    if(n.lt.0)then
       m=-n
    else
       m=n
    end if
  end function remove_possible_minus_sign
  

  subroutine reduce_mult_sqrt_fraction(c,r1,r2)
    !!Reduce the fraction c/sqrt(r1*r2) as faar as possible.
    !!Here gcd(c,r)=gcd(c,r/gcd(c,r))
    integer, intent(OUT) :: c,r1,r2
    integer :: gcd1
    !!Possible things to divide away
    gcd1=gcd(gcd(c,r1),r2)
    if(abs(gcd1).gt.1)then
       !!write(*,*) 'double reduction is possbile'
       !!Remove possible minus sign""
       if(gcd1.lt.0)then
          gcd1=-gcd1
       end if
       c=c/gcd1
       r1=r1/gcd1
       r2=r2/gcd1
       !!write(*,'(A,I10,A,I10,A,I10,A)') 'reduced double fraction:',c,&
       !!     '/sqrt(',r1,' * ',r2,')'
    else 
       !write(*,*) 'reduciton NOT possible'
    end if
  end subroutine reduce_mult_sqrt_fraction
  

  integer function isqrt(A)
    integer, intent(in) :: A
    !!Compute the integer square root of A, if it exists
    !!Otherwise trow and error
    isqrt=nint(sqrt(real(A)))
    !!write(*,*) 'Found',isqrt,'as square root of',A
    if((isqrt**2).ne.A)then
       write(*,*) 'Error: Illegal use of isqrt(A)'
       write(*,*) A,'is not the square of an integer'
       call exit(-2)
    end if
  end function isqrt



  subroutine compute_lorenz_overlap(Size,V1,SD1,V2,SD2,nomin,sqrde)
    !!Computes the norm in the lorenz Size+Size dimentional minkowski space
    !!The vectors are assumed to be V/sqrt(SD)
    !!Output is on the form integer/sqrt(integer)
    integer, intent(IN) :: Size,V1(2*Size), V2(2*Size), SD1(2*Size),SD2(2*Size)
    integer, intent(OUT) :: nomin,sqrde
    integer :: n
    !!Initialize 
    nomin=0
    sqrde=1
    do n=1,Size
       !write(*,*) 'adding:',V1(n)*V2(n),'/sqrt(',SD1(n)*SD2(n),')'
       call add_sqrt_fractions(V1(n)*V2(n),SD1(n)*SD2(n),1*nomin,1*sqrde,nomin,sqrde)
       !write(*,*) 'result:',nomin,'/sqrt(',sqrde,')'
    end do
    do n=(Size+1),2*Size
       !write(*,*) 'subtracting:',V1(n)*V2(n),'/sqrt(',SD1(n)*SD2(n),')'
       call add_sqrt_fractions(-V1(n)*V2(n),SD1(n)*SD2(n),1*nomin,1*sqrde,nomin,sqrde)
       !write(*,*) 'result:',nomin,'/sqrt(',sqrde,')'
    end do
    call reduce_sqrt_fraction(nomin,sqrde)
    
  end subroutine compute_lorenz_overlap


  subroutine compute_euclidian_overlap(Size,V1,SD1,V2,SD2,nomin,sqden)
    !!Computes the norm in the  Size  dimentional euclidain space
    !!The vectors are assumed to be V/sqrt(SD)
    !!Output is on the form integer/sqrt(integer)
    integer, intent(IN) :: Size,V1(Size), V2(Size), SD1(Size),SD2(Size)
    integer, intent(OUT) :: nomin,sqden
    integer :: n
    !!Initialize 
    nomin=0
    sqden=1
    do n=1,Size
       !write(*,*) 'adding:',V1(n)*V2(n),'/sqrt(',SD1(n)*SD2(n),')'
       call add_sqrt_fractions(V1(n)*V2(n),SD1(n)*SD2(n),1*nomin,1*sqden,nomin,sqden)
       !write(*,*) 'result:',nomin,'/sqrt(',sqden,')'
    end do
    
  end subroutine compute_euclidian_overlap


  subroutine compute_lorenz_norm(Size,Vectors,sqrddenom,nomin,denom)
    !!Computes the norm in the lorenz Size+Size dimentional minkowski space
    !!The vectors are assumed to be V/sqrt(SD)
    !!Output is on the form integer/integer
    integer, intent(IN) :: Size, Vectors(2*Size), sqrddenom(2*Size)
    integer, intent(OUT) :: nomin,denom
    integer :: n
    !!Initialize 
    nomin=0
    denom=1
    do n=1,Size
       !write(*,*) 'adding:',Vectors(n)**2,'/',sqrddenom(n)
       call add_fractions(Vectors(n)**2,sqrddenom(n),1*nomin,1*denom,nomin,denom)
       !write(*,*) 'result:',nomin,'/',denom
    end do
    do n=(Size+1),2*Size
       !write(*,*) 'subracting:',Vectors(n)**2,'/',sqrddenom(n)
       call add_fractions(-Vectors(n)**2,sqrddenom(n),1*nomin,1*denom,nomin,denom)
       !write(*,*) 'result:',nomin,'/',denom
    end do
    
  end subroutine compute_lorenz_norm


  subroutine compute_euclidian_norm(Size,V,SD,nomin,denom)
    !!Computes the norm in the  Size  dimentional euclidain space
    !!The vectors are assumed to be V/sqrt(SD)
    !!Output is on the form integer/integer
    integer, intent(IN) :: Size,V(Size), SD(Size)
    integer, intent(OUT) :: nomin,denom
    integer :: n
    !!Initialize 
    nomin=0
    denom=1
    do n=1,Size
       !write(*,*) 'adding:',V(n)^2,'/',SD(n)
       call add_fractions(V(n)**2,SD(n),1*nomin,1*denom,nomin,denom)
       !write(*,*) 'result:',nomin,'/',denom
    end do
    
  end subroutine compute_euclidian_norm
        

  subroutine scale_sqrt_vector_by_sqrt(Size,C_in,R_in,nomin,sqden,C_out,R_out)
    !!Scales sqrt vector by a sqrt number
    integer,intent(IN) :: Size,C_in(Size),R_in(Size),nomin,sqden
    integer,intent(OUT) :: C_out(Size),R_out(Size)
    integer m
    do m=1,Size
       call multiply_sqrt_number(C_in(m),R_in(m),nomin,sqden,C_out(m),R_out(m))
    end do
    !write(*,*) 'Scaled vector:'
    !write(*,*) C_out
    !write(*,*) '/sqrt(',R_out,')'
  end subroutine scale_sqrt_vector_by_sqrt

  subroutine add_sqrt_vector_to_sqrt_vector(Size,C1,R1,C2,R2,Cout,Rout)
    !!Add to sqrt vectors  of size dimentions to each other
    integer, intent(IN) :: Size,C1(Size),C2(Size),R1(Size),R2(Size)
    integer, intent(OUT) :: Cout(Size),Rout(Size)
    integer :: m
    !write(*,*) 'Add:',C1
    !write(*,*) 'to: ',C2
    !write(*,*) 'with radii:',R1
    !write(*,*) 'and:       ',R2
    do m=1,Size
       call add_sqrt_fractions(C1(m),R1(m),C2(m),R2(m),Cout(m),Rout(m))
    end do
    !write(*,*) 'result:',Cout
    !write(*,*) 'radii: ',Rout
    
  end subroutine add_sqrt_vector_to_sqrt_vector

end module misc_frac_sqrt
