program test_suite_cft_lambda
  
  USE typedef     ! types and definitions
  use jacobitheta
  use test_utilities
  use dedekind_eta_function
  use misc_random
  use chiral_wave_function
  
  !!Variables
  IMPLICIT NONE  
  
  
    !!Program Start here!!
    write(*,*) '        Test the jacobi theta function'
    
    !! Testing jacobi theta
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_lambda_T_transform
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_lambda_S_transform
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'


contains 

  subroutine test_lambda_T_transform
    integer :: m,n,Ns
    complex(KIND=dpc) :: tau,tau_T,lambda,lambda_T
    real(KIND=dp) :: x,y
    write(*,*) 'test that the CFT lambda transform covariantly under T transforms as l(m,n,tau+1)=l(m+n,n,tau)'

    CALL INIT_RANDOM_SEED()  !!get new seed every time
    !!Set the center of mass parameters
    
    do Ns=1,10 !!Test for fluxed from 1 to 10
       do m=-Ns,Ns
          do n=-Ns,Ns !!Thest all values for the m,n lattice
             call RANDOM_NUMBER(x)
             call RANDOM_NUMBER(y)
             tau=x-.5d0+iunit*(y+.5D0) !!set tau around tau=i
             tau_T=tau+1.0d0
             lambda=get_lambda_mn(m,n,tau_T,Ns)
             lambda_T=get_lambda_mn(m+n,n,tau,Ns)
             
             if(test_diff_exp_cmplx(lambda,lambda_T,1.d-12))then
                write(*,*) 'For Ns,m,n=',Ns,m,n
                write(*,*) 'and tau  =',tau
                write(*,*) 'and tau_T=',tau_T
                write(*,*) 'lambda:  ',lambda
                write(*,*) 'lambda_T:',lambda_T
                write(*,*) 'diff:',lambda-lambda_T
                call exit(-2)
             end if
          end do
       end do
    end do
  end subroutine test_lambda_T_transform

    subroutine test_lambda_S_transform
    integer :: m,n,Ns
    complex(KIND=dpc) :: tau,tau_S,lambda,lambda_S,S_phase
    real(KIND=dp) :: x,y
    write(*,*) 'test that the CFT lambda transform covariantly under S transforms as l(m,n,-1/tau)=l(-n,m,tau)*(tau/abs(tau))'

    CALL INIT_RANDOM_SEED()  !!get new seed every time
    !!Set the center of mass parameters
    
    do Ns=1,10 !!Test for fluxed from 1 to 10
       do m=-Ns,Ns
          do n=-Ns,Ns !!Thest all values for the m,n lattice
             if(mod(m,Ns).ne.0.or.mod(n,Ns).ne.0)then
                !!S-transform now well defined when m,n = 0 mod Ns
                call RANDOM_NUMBER(x)
                call RANDOM_NUMBER(y)
                tau=x-.5d0+iunit*(y+.5D0) !!set tau around tau=i
                tau_S=-1.0d0/tau
                lambda=get_lambda_mn(m,n,tau_S,Ns)
                S_phase=log(tau/abs(tau))
                lambda_S=get_lambda_mn(-n,m,tau,Ns)+S_phase

                if(test_diff_exp_cmplx(lambda,lambda_S,1.d-12))then
                   write(*,*) 'For Ns,m,n=',Ns,m,n
                   write(*,*) 'and tau  =',tau
                   write(*,*) 'and tau_S=',tau_S
                   write(*,*) 'lambda:  ',lambda
                   write(*,*) 'lambda_S:',lambda_S
                   write(*,*) 'diff:',lambda-lambda_S
                   call exit(-2)
                end if
             end if
          end do
       end do
    end do
  end subroutine test_lambda_S_transform
  
end program test_suite_cft_lambda


