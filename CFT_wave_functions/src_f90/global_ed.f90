MODULE global_ed

  USE typedef     ! types and definitions
  use global_torus !torus_specifics

  IMPLICIT NONE

  !****************************************************************************
  !
  ! Global variables.
  !
  !****************************************************************************

  
  ! Quantum numbers
  INTEGER :: Nele, Ncells, Qdenom, Qnomin, Nup, Ndn, Nmax
  integer :: Kmom,Kcft, qmom, qsz,S_z, LanLev, Neigs
  
  integer :: Potential
  REAL(KIND=dp) :: Nele_dp

  REAL(KIND=dp) :: dV1

  LOGICAL :: do_spin, do_pairc, do_cwf, do_wind, do_occs, do_cfs, do_mc, ed_cwf, do_cft
  LOGICAL :: T1BASIS,VOnly

CONTAINS

  ! Call this before you do anything:
  
  SUBROUTINE input_reader(p,q,cells,LL,only_v,Ksector,use_T2,Eigs,Int_Pot)
    integer,intent(IN) :: p,q,cells,LL,Ksector,Eigs,Int_Pot
    logical,intent(in) :: only_v,use_T2
    
    !Fillnig fraction
    Qnomin=p
    Qdenom=q
    IF ( Qnomin > Qdenom )then
       write(*,*) 'We are limited to the FQHE reqime.'
       write(*,*) 'Thus p/q <= 1'
       call exit(-1) 
    end IF
    
    !!Cells
    Ncells=cells
    
    !!Which LL?
    LanLev=LL
    
    Nele = Ncells*Qnomin
    if(Working_Ns.ne.Ncells*Qdenom)then
       write(*,*) 'Filling fraction and cells are not consistent with number of fluxes'
       call exit(-1)
    end if
    Nele_dp = REAL(Nele,dp)
    
    WRITE(*,*) 'We have ', Nele, ' particles and ', Working_Ns, 'sites.' 
    
    WRITE(*,*) 'Assuming full spin polarization.'
    S_z = Nele
    Nup=(S_z+Nele)/2
    Ndn=Nele-Nup
    Nmax = MAX(Nup, Ndn)
    IF (Nup+Ndn/=Nele .OR. Nup-Ndn/=S_z) STOP 'fckd s_z'
    
    !!Stop after V_ijkl
    VOnly=only_v
    
    Kmom=Ksector

    !! Do you want to use t1 eigenstate
    T1Basis=.not.use_T2
    if(T1Basis)then
       WRITE(*,*) 'You choose the t1-basis'
    else
       WRITE(*,*) 'You choose the t2-basis'
    end if
    
    !! Pseidopotential paraemter dv1:'
    !! Not in use currectly
    dV1=0.d0
    
    !!How many eigenvalues to compute?'
    !!Max is basis size - 2:'
    Neigs=Eigs
    write(*,*) 'Find the',Neigs,'lowest energy eigenstates'
    
    Potential=int_pot

   END SUBROUTINE input_reader

 END MODULE global_ed
