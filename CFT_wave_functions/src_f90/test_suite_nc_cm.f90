program test_nc_cm

  USE typedef     ! types and definitions
  use center_of_mass
  use chiral_wave_function
  use jacobitheta
  use test_utilities
  use dedekind_eta_function
  use misc_random
  
  !!Variables
  IMPLICIT NONE  

  !!!test starts here!!!
    write(*,*) '         Test the non-chiral center of mass function'
    !! Testing center of mass function
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_alternative_laughlin_cm
    !! Testing full wave function
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_alternative_laughlin_peridoicity
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_alternative_laughlin_peridoicity_fixed_k
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_alternative_laughlin_t1_fixed_k
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_nc_d_2_imag_tau
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
    call test_nc_d_3_infinity_case
    write(*,*) '-.-.-.-.-.-.-.-.-.-.-.-.-.-.'


contains 
  
  
!!!----------------------------------------
!!! Test CM
!!!----------------------------------------
  subroutine test_alternative_laughlin_cm()
    !!Here we test hat the cm-functions for the alternative laughlin functions
    !! (with h=t=0) is given by
    !! F = theta_3( (p+q)Z - p\bar Z | (p+q)\tau - p\bar\tau )
    complex(kind=dpc) :: Z(1),Z_bar(1),tau,tau_bar
    complex(kind=dpc) :: res_facit,res_calc
    real(kind=dp) :: X(1),Y(1),tau_1,tau_2,squares=2.d0
    integer :: n, n_tests=20,q,p
    integer :: K_matrix(1,1),K_bar_matrix(1,1)

    write(*,*) 'testing the alternative laughlin center of mass'
    call unset_CM_bc
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    do q=1,4
       do p=0,3
          do n=1,n_tests
             !!Set the X-Y variables (one the square with length 2*square)
             call RANDOM_NUMBER(X)
             call RANDOM_NUMBER(Y)
             X=2*squares*X-squares !!centered on 0
             Y=2*squares*Y-squares !!centered on 0

             !!-------------------
             call RANDOM_NUMBER(tau_1)
             call RANDOM_NUMBER(tau_2)
             tau_1=tau_1-.5d0 !!centerd on 0
             tau_2=tau_2+.5d0 !!centred on 1
             !!-------------------
             !!Complex tau 
             !tau    =tau_1+iunit*tau_2
             !tau_bar=tau_1-iunit*tau_2

             tau    =iunit*tau_2 !!FIXME test
             tau_bar=-iunit*tau_2 !!FIXE test

             !!Complex coodinates
             Z    =X+Y*tau   
             Z_bar=X+Y*tau_bar
             !!Konstruct the kappa_matrix
             K_matrix(1,1)=p+q
             K_bar_matrix(1,1)=p
             call set_trivial_CM_BC(1,K_matrix,K_bar_Matrix)
             !!Compute the known result
             res_facit=logthetagen(0.d0,0.d0,(p+q)*Z(1) - p*Z_bar(1),&
                  (p+q)*tau - p*tau_bar)-log_dedekind_eta(tau)
             !!Complute the CoM piece
             res_calc=Center_of_Mass_Function(1,K_matrix,K_bar_matrix,&
                  X,Y,tau)
             if(test_diff_exp_cmplx(res_calc,res_facit,1.d-13))then
                write(*,*) 'q,p,n:',q,p,n
                write(*,*) 'tau:   ',tau
                write(*,*) 'taubar:',tau_bar
                write(*,*) 'Z:   ',Z
                write(*,*) 'Zbar:',Z_bar
                write(*,*) 'calc: ',res_calc
                write(*,*) 'facit:',res_facit
                write(*,*) 'ERROR Diff:',res_calc-res_facit
                write(*,*) 'Diff/2*pi: ',(res_calc-res_facit)/(2*pi)

                call exit(-1)
             end if
          end do
       end do
    end do
  end subroutine test_alternative_laughlin_cm


!!!----------------------------------------
!!! Test wave-functions
!!!----------------------------------------
  subroutine test_alternative_laughlin_peridoicity()
    !!Here we test hat the wave functions for the alternative laughlin functions
    !! (with h=t=0) has correct (ant-periodic bc)
    integer, parameter :: D=1
    complex(kind=dpc) :: tau,tau_bar
    complex(kind=dpc) :: res_facit,res_calc_x,res_calc_y
    real(kind=dp) :: tau_1,tau_2
    real(kind=dp), allocatable, dimension(:) :: Xlist,Ylist
    real(kind=dp), allocatable, dimension(:) :: Xlist_shift,Ylist_shift
    integer :: n,q,p,Electrons,Ns,n2
    integer :: K_matrix(1,1),K_bar_matrix(1,1)
    real(kind=dp) :: extra_phase,gauge_phase

    write(*,*) 'testing the alternative laughlin wave function'
    call unset_CM_bc
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    do q=1,4
       do p=0,3
          do Electrons=1,4
             Ns=q*Electrons
             allocate(Xlist(Electrons),Ylist(Electrons))
             allocate(Xlist_shift(Electrons),Ylist_shift(Electrons))
             !!Set the X-Y variables (one the unit square)
             call RANDOM_NUMBER(Xlist)
             call RANDOM_NUMBER(Ylist)
             Xlist=Xlist-.5d0 !!centerd on 0
             Ylist=Ylist-.5d0 !!centerd on 0
             !!-------------------
             call RANDOM_NUMBER(tau_1)
             call RANDOM_NUMBER(tau_2)
             tau_1=tau_1-.5d0 !!centerd on 0
             tau_2=tau_2+.5d0 !!centred on 1
             !!-------------------
             !!Complex tau
             tau    =tau_1+iunit*tau_2
             tau_bar=tau_1-iunit*tau_2

             !!Konstruct the kappa_matrix
             K_matrix(1,1)=p+q
             K_bar_matrix(1,1)=p
             
             if(p.eq.0)then
                call set_trivial_CM_BC(1,K_matrix)
             else
                call set_trivial_CM_BC(1,K_matrix,K_bar_Matrix)
             end if
             call initialize_wave_function(1,K_matrix,Electrons,Ns,(/(0, n2=1,D)/),(/(0, n2=1,D)/),kappa_bar=K_bar_matrix)

             !!Compute wfn before shifting the indices
             res_facit=mod_2pi(chiral_raw_psi(Xlist,Ylist,tau))
             !!Loop over all the electrons
             do n=1,Electrons
                Xlist_shift=Xlist
                Ylist_shift=Ylist
                Xlist_shift(n)=Xlist_shift(n)+1
                Ylist_shift(n)=Ylist_shift(n)+1
                
                !!The periodic boudnary condtions are Ns - q
                extra_phase=pi*(Ns-q)
                
                !!The expected gauge phase is 
                gauge_phase=Xlist(n)*2*pi*Ns

                res_calc_x=mod_2pi(chiral_raw_psi(Xlist_shift,Ylist,tau)&
                     +iunit*extra_phase)
                res_calc_y=mod_2pi(chiral_raw_psi(Xlist,Ylist_shift,tau)&
                     +iunit*extra_phase+iunit*gauge_phase)
                !!Complute the CoM piece
                if((test_diff_exp_cmplx(res_calc_x,res_facit,1.d-06)).or.&
                     (test_diff_exp_cmplx(res_calc_y,res_facit,1.d-06)))then
                   write(*,*) 'Shift in particle no',n
                   write(*,*) 'q,p,N_e:',q,p,Electrons
                   write(*,*) 'tau:   ',tau
                   write(*,*) 'taubar:',tau_bar
                   write(*,*) 'Xlist:',Xlist
                   write(*,*) 'Ylist:',Ylist
                   write(*,*) '------------------------'
                   write(*,*) 'extra pahse:',extra_phase
                   write(*,*) 'gauge pahse:',gauge_phase
                   write(*,*) '------------------------'
                   write(*,*) 'facit: ',res_facit
                   write(*,*) 'calc_x:',res_calc_x
                   write(*,*) 'calc_y:',res_calc_y
                   write(*,*) '------------------------'
                   write(*,*) 'diff_x:',res_calc_x-res_facit
                   write(*,*) 'diff_y:',res_calc_y-res_facit
                   write(*,*) '------------------------'
                   write(*,*) 'ERROR Diff'
                   call exit(-1)
                end if
             end do
             deallocate(Xlist,Ylist,Xlist_shift,Ylist_shift)
          end do
       end do
    end do
  end subroutine test_alternative_laughlin_peridoicity


  subroutine test_alternative_laughlin_peridoicity_fixed_K()
    !!Here we test hat the wave functions for the alternative laughlin functions
    !! (with h=t=0) has correct (ant-periodic bc)
integer, parameter :: D=1
    complex(kind=dpc) :: tau,tau_bar
    complex(kind=dpc) :: res_facit,res_calc_x,res_calc_y
    real(kind=dp) :: tau_1,tau_2
    real(kind=dp), allocatable, dimension(:) :: Xlist,Ylist
    real(kind=dp), allocatable, dimension(:) :: Xlist_shift,Ylist_shift
    integer :: n,q,p,Electrons,Ns,sector_indx,K_sector,n2
    integer :: K_matrix(1,1),K_bar_matrix(1,1)
    real(kind=dp) :: extra_phase,gauge_phase

    write(*,*) 'testing the alternative laughlin wave function with pbc and given K-sector'
    call unset_CM_bc
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    do q=1,4
       do p=0,3
          do Electrons=1,4
             Ns=q*Electrons
             allocate(Xlist(Electrons),Ylist(Electrons))
             allocate(Xlist_shift(Electrons),Ylist_shift(Electrons))
             do sector_indx=0,q
                !write(*,*) 'q,p,Ne,sector',q,p,Electrons,sector_indx
                !!Set the X-Y variables (one the unit square)
                call RANDOM_NUMBER(Xlist)
                call RANDOM_NUMBER(Ylist)
                Xlist=Xlist-.5d0 !!centerd on 0
                Ylist=Ylist-.5d0 !!centerd on 0
                !!-------------------
                call RANDOM_NUMBER(tau_1)
                call RANDOM_NUMBER(tau_2)
                tau_1=tau_1-.5d0 !!centerd on 0
                tau_2=tau_2+.5d0 !!centred on 1
                !!-------------------
                !!Complex tau
                tau    =tau_1+iunit*tau_2
                tau_bar=tau_1-iunit*tau_2
                
                !!Konstruct the kappa_matrix
                K_matrix(1,1)=p+q
                K_bar_matrix(1,1)=p
                
                K_sector = modulo(q*(Electrons*(Electrons-1))/2,Ns)&
                     +Electrons*sector_indx
                !write(*,*) 'K_sector:',K_sector


                call initialize_wave_function(1,K_matrix,Electrons,Ns,(/(0, n2=1,D)/),(/(0, n2=1,D)/),kappa_bar=K_bar_matrix)
                if(p.eq.0)then
                   call set_CM_BC(Ns,K_sector,1,K_matrix)
                else
                   call set_CM_bc(Ns,K_sector,1,K_matrix,K_bar_matrix)
                end if
                !!Compute wfn before shifting the indices
                res_facit=mod_2pi(chiral_raw_psi(Xlist,Ylist,tau))
                !!Loop over all the electrons
                do n=1,Electrons
                   Xlist_shift=Xlist
                   Ylist_shift=Ylist
                   Xlist_shift(n)=Xlist_shift(n)+1
                   Ylist_shift(n)=Ylist_shift(n)+1
                   
                   !!The periodic boudnary condtions are Ns - q
                   extra_phase=0 !!The test  is for periodic boundary conditions
                   
                   !!The expected gauge phase is 
                   gauge_phase=Xlist(n)*2*pi*Ns
                   
                   res_calc_x=mod_2pi(chiral_raw_psi(Xlist_shift,Ylist,tau)&
                        +iunit*extra_phase)
                   res_calc_y=mod_2pi(chiral_raw_psi(Xlist,Ylist_shift,tau)&
                        +iunit*extra_phase+iunit*gauge_phase)
                   !!Complute the CoM piece
                   if((test_diff_exp_cmplx(res_calc_x,res_facit,1.d-5)).or.&
                        (test_diff_exp_cmplx(res_calc_y,res_facit,1.d-5)))then
                      write(*,*) 'Shift in particle no',n
                      write(*,*) 'q,p,N_e:',q,p,Electrons
                      write(*,*) 'tau:   ',tau
                      write(*,*) 'taubar:',tau_bar
                      write(*,*) 'Xlist:',Xlist
                      write(*,*) 'Ylist:',Ylist
                      write(*,*) '------------------------'
                      write(*,*) 'extra pahse:',extra_phase
                      write(*,*) 'gauge pahse:',gauge_phase
                      write(*,*) '------------------------'
                      write(*,*) 'facit: ',res_facit
                      write(*,*) 'calc_x:',res_calc_x
                      write(*,*) 'calc_y:',res_calc_y
                      write(*,*) '------------------------'
                      write(*,*) 'diff_x:',res_calc_x-res_facit
                      write(*,*) 'diff_y:',res_calc_y-res_facit
                      write(*,*) '------------------------'
                      write(*,*) 'ERROR Diff'
                      call exit(-1)
                   end if
                end do
             end do
             deallocate(Xlist,Ylist,Xlist_shift,Ylist_shift)
          end do
       end do
    end do
  end subroutine test_alternative_laughlin_peridoicity_fixed_K


  subroutine test_alternative_laughlin_t1_fixed_K()
    !!Here we test hat the wave functions for the alternative laughlin functions
    !! (with h=t=0) has correct (ant-periodic bc)
    integer, parameter :: D=1
    complex(kind=dpc) :: tau,tau_bar
    complex(kind=dpc) :: res_facit,res_calc_x
    real(kind=dp) :: tau_1,tau_2
    real(kind=dp), allocatable, dimension(:) :: Xlist,Ylist
    real(kind=dp), allocatable, dimension(:) :: Xlist_shift
    integer :: n,q,p,Electrons,Ns,sector_indx,K_sector,n2
    integer :: K_matrix(1,1),K_bar_matrix(1,1)
    real(kind=dp) :: extra_phase

    write(*,*) 'testing the alternative laughlin wave function with given K-sector'
    call unset_CM_bc
    CALL INIT_RANDOM_SEED()  !!get new seed every time
    do q=1,4
       do p=0,3
          do Electrons=1,4
             Ns=q*Electrons
             allocate(Xlist(Electrons),Ylist(Electrons))
             allocate(Xlist_shift(Electrons))
             do sector_indx=0,q
                !write(*,*) '-----------------------------------------'
                !write(*,*) '-----------------------------------------'
                !write(*,*) 'q,p,Ne,sector',q,p,Electrons,sector_indx
                !!Set the X-Y variables (one the unit square)
                call RANDOM_NUMBER(Xlist)
                call RANDOM_NUMBER(Ylist)
                Xlist=Xlist-.5d0 !!centerd on 0
                Ylist=Ylist-.5d0 !!centerd on 0
                !!-------------------
                call RANDOM_NUMBER(tau_1)
                call RANDOM_NUMBER(tau_2)
                tau_1=tau_1-.5d0 !!centerd on 0
                tau_2=tau_2+.5d0 !!centred on 1
                !!-------------------
                !!Complex tau
                tau    =tau_1+iunit*tau_2
                tau_bar=tau_1-iunit*tau_2
                
                !!Konstruct the kappa_matrix
                K_matrix(1,1)=p+q
                K_bar_matrix(1,1)=p
                
                K_sector = modulo(modulo(q*(Electrons*(Electrons-1))/2,Ns)&
                     +Electrons*sector_indx,Ns)
                !write(*,*) 'K_sector:',K_sector

                call initialize_wave_function(1,K_matrix,Electrons,Ns,(/(0, n2=1,D)/),(/(0, n2=1,D)/),kappa_bar=K_bar_matrix)
                if(p.eq.0)then
                   call set_CM_BC(Ns,K_sector,1,K_matrix)
                else
                   call set_CM_bc(Ns,K_sector,1,K_matrix,K_bar_matrix)
                end if
                
                !!Compute wfn before shifting the indices
                res_facit=mod_2pi(chiral_raw_psi(Xlist,Ylist,tau))
                
                !!Make T1 translation!!
                Xlist_shift=Xlist+1d0/Ns
                
                !!The periodic boudnary condtions are Ns - q
                !!The test  is for periodic boundary conditions
                extra_phase=2*pi*K_sector/real(Ns,dp)
                
                res_calc_x=mod_2pi(chiral_raw_psi(Xlist_shift,Ylist,tau)&
                     -iunit*extra_phase)
                
                !!Complute the CoM piece
                if(test_diff_exp_cmplx(res_calc_x,res_facit,1.d-5))then
                   write(*,*) 'Shift in particle no',n
                   write(*,*) 'q,p,N_e,Ns:',q,p,Electrons,Ns
                   write(*,*) 'tau:   ',tau
                   write(*,*) 'taubar:',tau_bar
                   write(*,*) 'Xlist:',Xlist
                   write(*,*) 'Ylist:',Ylist
                   write(*,*) '------------------------'
                   write(*,*) 'Ns*extra pahse/(2*pi):',Ns*extra_phase/(2*pi)
                   write(*,*) 'extra pahse/(2*pi):   ',extra_phase/(2*pi)
                   write(*,*) '------------------------'
                   write(*,*) 'facit: ',res_facit
                   write(*,*) 'calc_x:',res_calc_x
                   write(*,*) '------------------------'
                   write(*,*) 'diff_x:       ',res_calc_x-res_facit
                   write(*,*) 'diff_x/(2*pi):',(res_calc_x-res_facit)/(2*pi)
                   write(*,*) '------------------------'
                   write(*,*) 'ERROR Diff'
                   call exit(-1)
                end if
             end do
             deallocate(Xlist,Ylist,Xlist_shift)
          end do
       end do
    end do
  end subroutine test_alternative_laughlin_t1_fixed_K
  


  subroutine test_nc_d_2_imag_tau
    integer,parameter :: Values=16,D=2
    integer :: N,kappa_bar(D,D),Kmat(D,D)
    complex(kind=dpc) :: tau,FacitList(Values),Calc,Facit
    real(Kind=dp) :: Xlist(Values,D),Ylist(Values,D),Xtmp(D),Ytmp(D)


    write(*,*) 'test that a non-trivial D=2 state can be computed in sector t=h=0'

    call unset_CM_bc !!Unset boundary conditions
    
    Kmat=reshape((/3,1,1,3/),(/D,D/))
    kappa_bar=reshape((/1,0,0,1/),(/D,D/))
    
    tau = iunit
    Xlist=reshape((/0d0,0.5d0,0d0,0d0,0d0,-0.0346031d0,&
         0.9653969444038244d0,0.663936d0,0.730669d0,0.972755d0&
         ,0.0888909d0,0.406061d0,0.11931075835108929d0, 0.0353228d0,0.852567d0,&
         0.4781743509652605d0,&
         !!Row 2
         0d0,0d0,0.5d0,0d0,0d0,0.238677d0,0.23867685116669524d0,0.509814d0,&
         0.524355d0,0.525271d0,0.364502d0,0.573152d0,0.13993540991458886d0,&
         0.0480793d0,0.392801d0,0.2142060204161731d0&
         /),(/Values,D/))
    
    !Xlist=reshape((/0d0,0d0,0.965397d0, 0.238677d0,0.663936d0, 0.509814d0,&
    !     0.730669d0, 0.524355d0,0.972755d0, 0.525271d0,&
    !     0.0888909d0, 0.364502d0, 0.406061d0, 0.573152d0,&
    !     0.119311d0, 0.139935d0, 0.0353228d0, 0.0480793d0,&
    !     0.852567d0, 0.392801d0, 0.478174d0, 0.214206d0/),(/Values,D/))
    !write(*,*) 'Xlist',Xlist
    
    
    Ylist=reshape((/0d0,0d0,0d0,0.5d0,0d0,0.455498d0,0.4554977044531312d0,&
         0.402681d0,0.223967d0,0.542387d0,0.360218d0,&
         0.589625d0,0.3748524179293924d0,0.544854d0,0.532516d0,&
         0.4147405608518455d0,&
         !!Row 2
         0d0,0d0,0d0,0d0,0.5d0,-0.0968215d0,0.9031785198433029d0,&
         0.155359d0,0.71367d0,0.293921d0,0.554875d0,0.998502d0,&
         0.16403490098362594d0,&
         0.91064d0,0.145177d0,0.034134439837775865d0/),(/Values,D/))
    
    !Ylist=reshape((/0d0,0d0,0.455498d0, 0.903179d0,0.402681d0, 0.155359d0,&
    !     0.223967d0, 0.71367d0, 0.542387d0, 0.293921d0,&
    !     0.360218d0, 0.554875d0, 0.589625d0, 0.998502d0,&
    !     0.374852d0, 0.164035d0, 0.544854d0, 0.91064d0,&
    !     0.532516d0, 0.145177d0, 0.414741d0, 0.0341344d0/),(/Values,D/))
    !write(*,*) 'Ylist',Ylist
    FacitList=(/&
         (1.0000139623942945301d0,0d0),&
         (0.999999986975130d0,0d0),&
         (0.999999986975130d0,0d0),&
         (2.00016170046d0,0d0),&
         (2.00016170046d0,0d0),&
         (1.086159258563d0,-0.155352832d0),&
         (-471586.48288005197d0,-104289.0606900006568d0),&
         (1.11951459271070939d0,0.197938945763202434d0),&
         (162.204677899800170d0,862.97292253413379d0),&
         (-16.9228845717382288d0,-3.38320667733283860d0),&
         (15.8164685262422766d0,34.9659760204162355d0),&
         (8.7108793511239069d7,4.40045559926210948d7),&
         (0.91095481706273312d0,-0.084745615709648013d0),&
         (629488.55204557361d0,-2.32448323598755658d6),&
         (5.60077266050237648d0,-3.2510688765246526d0),&
         (1.069690141157567069d0,-0.1275970900308945d0)/)
    !write(*,*) 'FacitList',FacitsList

    
    call set_trivial_CM_BC(D,Kmat,kappa_bar)    
    do N=1,Values
       Xtmp=Xlist(N,:)
       Ytmp=Ylist(N,:)
       Calc=mod_2pi(Center_of_Mass_Function(D,Kmat,kappa_bar,Xtmp,Ytmp,tau)+D*log_dedekind_eta(tau))
       Facit=mod_2pi(log(FacitList(N)))
       !write(*,*) 'facit:    ',Facit
       !write(*,*) 'Calc:     ',Calc
       
       
       if(test_diff_cmplx(Calc,Facit,1.d-5))then
          write(*,*) '     ERROR, no:',N
          write(*,*) 'The Facit does not match the computation for'
          write(*,*) 'kappa_matrix:'
          call print_int_matrix(Kmat)
          write(*,*) 'kappa_bar:'
          call print_int_matrix(kappa_bar)
          write(*,*) 'tau:   ',tau
          write(*,*) 'Xlist:',Xtmp
          write(*,*) 'Ylist:',Ytmp
          write(*,*) '------------------------'
          write(*,*) 'facit:    ',Facit
          write(*,*) 'Calc:     ',Calc
          write(*,*) 'exp facit:',exp(Facit)
          write(*,*) 'exp Calc: ',exp(Calc)
          write(*,*) '------------------------'
          write(*,*) 'diff_x:       ',Calc-Facit
          write(*,*) 'diff_x/(2*pi):',(calc-facit)/(2*pi)
          write(*,*) '------------------------'
          write(*,*) 'ERROR Diff'
          call exit(-1)
       end if
    end do
  end subroutine test_nc_d_2_imag_tau



  subroutine test_nc_d_3_infinity_case
    integer,parameter :: Values=1,D=3
    integer :: Ns,K,kappa_bar(D,D),Kmat(D,D),Kminus(D,D),dual_indx(D)
    complex(kind=dpc) :: tau,Calc,Facit
    real(Kind=dp) :: Xlist(D),Ylist(D)


    write(*,*) 'test that a particular D=3 case (has happened) does not result in NaN:s'

    call unset_CM_bc !!Unset boundary conditions
    
    Kminus=reshape((/3,1,3,1,3,5,3,5,-1/),(/D,D/))
    kappa_bar=reshape((/3,0,0,0,6,0,0,0,6/),(/D,D/))
    Kmat=Kminus+kappa_bar
    
    tau = 40*iunit
    Xlist=(/0.19718911844242681d0,0.56062330754060674d0,0.63813046903739634d0/)
    Ylist=(/-0.57027297047744829d0,-0.024968627198813098d0,0.49822791676864242d0/)
    Facit=(310.43060446851985d0,-1.9584351950439398d0)
    Ns=10
    K=5
    dual_indx=(/1,3,5/)
    call set_CM_BC(Ns,K,D,Kmat,kappa_bar,dual_indx)    
    Calc=mod_2pi(Center_of_Mass_Function(D,Kmat,kappa_bar,Xlist,Ylist,tau))
    !write(*,*) 'facit:    ',Facit
    !write(*,*) 'Calc:     ',Calc
           
    if(test_diff_cmplx(Calc,Facit,1.d-5))then
       write(*,*) '     ERROR:'
       write(*,*) 'The Facit does not match the computation for'
       write(*,*) 'kappa_matrix:'
       call print_int_matrix(Kmat)
       write(*,*) 'kappa_bar:'
       call print_int_matrix(kappa_bar)
       write(*,*) 'tau:   ',tau
       write(*,*) 'Xlist:',Xlist
       write(*,*) 'Ylist:',Ylist
       write(*,*) '------------------------'
       write(*,*) 'facit:    ',Facit
       write(*,*) 'Calc:     ',Calc
       write(*,*) 'exp facit:',exp(Facit)
       write(*,*) 'exp Calc: ',exp(Calc)
       write(*,*) '------------------------'
       write(*,*) 'diff_x:       ',Calc-Facit
       write(*,*) 'diff_x/(2*pi):',(calc-facit)/(2*pi)
       write(*,*) '------------------------'
       write(*,*) 'ERROR Diff'
       call exit(-1)
    end if
  end subroutine test_nc_d_3_infinity_case


end program test_nc_cm


