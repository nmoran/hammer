module cft_help
  !!Module containing the help-function for the CFT.
  !!Usefull if the help need to be called from other modules

  use typedef  
  implicit none
  
  !!Monte-Carlo multiplier
  integer, parameter :: MC_default=1,x_default=1,y_default=0
  integer, parameter :: K_default=0
  
  !! Torus specifics
  real(kind=dp), parameter :: I_default=1.d0,R_default=0.d0,sval_default=0.d0


  public 
  
contains
  
  subroutine print_help()
    write(*,'(A)') 'Usage: chiral_wfn [OPTION]...'
    write(*,'(A)') 'Compute the wave-function for a chiral kappa-matrix.'
    write(*,*) ''
    write(*,*) '------- kappa-matrix specifics -------'
    write(*,*) '  Only -D and --kappa_matrix has to be supplied, then -p and -q'
    write(*,*) '  will be compared to the internally computed values.'
    write(*,*) ''
    write(*,*) ' -p,  --nomin   <p>         Nominator of filling fraction.'
    write(*,*) ' -q,  --denomin <q>         Denominator of filling fraction.'
    write(*,*) ' -D,  --groups  <D>         Dimention of the kappa-matrix,'
    write(*,*) '                            equals the number of groups.'
    write(*,*) ' --kappa     <k11 ... kDD>  Entries to the kappa-matrix    row by row.'
    write(*,*) ' --kappa_bar <k11 ... kDD>  Entries to the kappa_bar-matrix  -- || --'
    write(*,*) ' --Kplus     <k11 ... kDD>  Entries to the K-plus-matrix    -- || --'
    write(*,*) ' --Kmatrix   <k11 ... kDD>  Entries to the K-matrix    -- || --'
    write(*,*) ' --K???-diag <k1 kD>        Only the diagonal entries'
    write(*,*) '                            Flag -D has to be set bofore any kappa-matrix is given.'
    write(*,*) '                            All K-matrices have to be symetric.'
    write(*,*) '                            The internal consistency of the differnt K-matrices is:'
    write(*,*) '                            Kplus   = kappa + kappa_bar'
    write(*,*) '                            Kmatrix = kappa - kappa_bar'
    write(*,*) '                            Default kappa_bar = 0,'
    write(*,*) '                            equivalent to chiral kappa-matrix'
    write(*,*) ' --k_denomin <k_denom>      Specify the denominator of the k-matrx'
    write(*,*) '                            This will be interpreted as kmatrix/k_denomin'  
    write(*,*) '                            Take care to ensure that  kmatrix/k_denomin is integer' 
    write(*,*) ''
    write(*,*) ' -s, --svalue  <svalue>     A real parameter that specifies how the chiral and'
    write(*,*) '                            non-chiral halves are glued together.' 
    write(*,*) '                            Default s=',sval_default
    write(*,*) ''
    write(*,*) '------- System size -------'
    write(*,*) '  Only one of -c, -Ne and -Ns needs to be supplied.'
    write(*,*) '  If more than one flag is set, they will be compared'
    write(*,*) '  for internal consistency.'
    write(*,*) ''
    write(*,*) ' -c,  --cells      <p>      Number of fundamental unit cells.'
    write(*,*) ' -Ne, --particles  <Ne>     Number of particles.'
    write(*,*) ' -Ns, --fluxes     <Ns>     Number of flux quanta = states.'
    write(*,*) ''
    write(*,*) ' -Nq, --qps <Nq1 --- NqD>   Number of quasiparticles in the groups D'
    write(*,*) '                            Assuming simplest type of qps (not compound)'
    write(*,*) '                            The qp coordinates are recoorded in'
    write(*,*) '                             qps_xx.rnd and'
    write(*,*) '                             qps_yy.rnd and'
    write(*,*) '                            NB: coordinates are used without regularizing'
    write(*,*) '                            to the fundamental torus domain,'
    write(*,*) '                            as qps are not single valued (see --qps-static)'
    write(*,*) ' --qp-power <nom> <denom>   The power of the quasiparticle.'
    write(*,*) '                            on the abosule part of the correlator'
    write(*,*) '                            such that |z-eta|^2<nom>/<denom>'
    write(*,*) ' --pq-pos-x <x1> ... <xNq>  The x-postion of the quasiparticles'
    write(*,*) '                            input directly on commad line'
    write(*,*) ' --pq-pos-y <y1> ... <yNq>  The y-postion of the quasiparticles'
    write(*,*) '                            input directly on commad line'
    write(*,*) ' --pq-file-x <x-pos-file>   The path to the file containing the'
    write(*,*) '                            x-postion of the quasiparticles'
    write(*,*) ' --pq-file-y <y-pos-file>   The path to the file containing the'
    write(*,*) '                            y-postion of the quasiparticles'
    write(*,*) '                            NB: --pq-pos-x and --pq-file-x  nor '
    write(*,*) '                            NB: --pq-pos-y and --pq-file-y '
    write(*,*) '                            can be used in parallel'
    write(*,*) ' --qp-static                Toggle ON the computation of static qp-factors'
    write(*,*) '                            This includes the Gaussian and eta-eta Jastrow factor.'
    write(*,*) '                            By default this computation is off (to save time).'
    write(*,*) ''
    write(*,*) '------- Torus Info -------'
    write(*,*) ''
    write(*,*) ' -I,  --tau_im      <I>     Imaginary part of tau.'
    write(*,*) '                            Default tau_im=',I_default
    write(*,*) ' -R,  --tau_re      <R>     Imaginary part of tau.'
    write(*,*) '                            Default tau_re=',R_default
    write(*,*) ' -l,  --log                 Input -I in log-scale'
    write(*,*) ''
    write(*,*) '------- CFT state specifics -------'
    write(*,*) ' -K,-K1,  --momentum <K>    Momentum sector.'
    write(*,*) '                            Eigenvalue of T1 (mod Ns)'
    write(*,*) ' -K2, --co-momentum <K2>    Co-momentum sector.'
    write(*,*) '                            Eigenvalue of T2^q_nu (mod cells_nu)'
    write(*,*) '                            Trivial for abelian states, needed for non-abelian'
    write(*,*) '                            '
    write(*,*) ' --dual-index, --DI <d_1 ... d_D>'
    write(*,*) '                            An alternative to K-sector.'
    write(*,*) '                            Needed for non abelian states.'
    write(*,*) '                            K-sector is K=Sum_a N_a*D_a/2'
    write(*,*) '                            D_a is integer and 2*true-dual-index'
    write(*,*) ' --CoM-index, --COMI <r>    Co-momentum index. Sets the K2 sector.Default=0'
    write(*,*) '                            '
    write(*,*) ' --x-list  <x1 ... xD>      List of group translations in x-direction'
    write(*,*) ' --y-list  <y1 ... yD>      List of group translations in y-direction'
    write(*,*) ' -x                <x>      Translation in x-direction'
    write(*,*) ' -y                <y>      Translation in y-direction'
    write(*,*) '                            Using -x/-y means that the translations'
    write(*,*) '                            in j-direction are set to <0 j ... (D-1)j>'
    write(*,*) '                            Flag -D has to be set first.'
    write(*,'(A,I2,A,I2,A)') '                             Default translations are (x,y)=(',x_default,',',y_default,')'
    write(*,*) '------- Monte-Carlo specifics -------'
    write(*,*) ''
    write(*,*) ' -r,  --reuse               Resue coordinates in "<dir>/coord_xx.rnd"'
    write(*,*) '                            and "<dir>/coord_yy.rnd".'
    write(*,*) ' -N, -MC, --MC-points <MC>  Number of Monte-Carlo points to sample.'
    write(*,'(A,I2)') '                             Default MC=',MC_default
    write(*,*) '                            Genereated Monte Carlo coordiantes are  written to'
    write(*,*) '                            "<dir>/coord_xx.rnd" and "<dir>/coord_yy.rnd"'
    write(*,*) '                            Genereated Monte Carlo wave function is written to'
    write(*,*) '                            "<dir>/chiral_re.rnd" and "<dir>/chiral_im.rnd"'
    write(*,*) ''
    write(*,*) '  --output_dir <dir>        Set the output dir to <dir>.'
    write(*,*) '                            Default directory <dir>=result'
    write(*,*) '                            NB: Only has an effect when program is called through cft_wfn!'
    write(*,*) '                            NB: Do no use trailing slashes'
    write(*,*) '                            NB: Use <dir>="." for current dir'
  
    write(*,*) ''
    write(*,*) '  -A                        Do not antisymetrize'
    write(*,*) '                            Usefull for testing, and for constructing states'
    write(*,*) '                            like the Halperin 331-state'
    write(*,*) ''
    write(*,*) '  -h, --help                Print this help and exit' 
    write(*,*) ''
    write(*,'(A)') 'Report chiral_wfn bugs to mikael.fremling@nuim.ie'
  end subroutine print_help


end module cft_help
