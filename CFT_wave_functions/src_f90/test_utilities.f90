module test_utilities

  USE typedef     ! types and definitions
  use misc_frac_sqrt
  use misc_matrix

  !!Variables
  IMPLICIT NONE  

  public

  contains

!!!------------------------------------------
!!!      Thes the eqivalence of variables
!!!------------------------------------------

  function test_diff(X1,X2,Acc)
    real(KIND=dp), intent(in) :: X1,X2,Acc
    real(KIND=dp) ::  error
    logical :: test_diff
    
    Error=abs((X1-X2)/X2)
    if(abs(error).gt.Acc)then
       test_diff=.true.
    else
       test_diff=.false.
    end if
  end function test_diff

  function test_diff_cmplx(X1,X2,Acc)
    real(KIND=dp), intent(in) :: Acc
    complex(KIND=dpc), intent(in) :: X1,X2
    real(KIND=dp) ::  error
    logical :: test_diff_cmplx
    
    Error=abs((X1-X2)/X2)
    if(abs(error).gt.Acc)then
       test_diff_cmplx=.true.
    else
       test_diff_cmplx=.false.
    end if
  end function test_diff_cmplx

  logical function test_diff_exp_cmplx(X1,X2,Acc,verbose)
    real(KIND=dp), intent(in) :: Acc
    complex(KIND=dpc), intent(in) :: X1,X2
    real(KIND=dp) ::  error,Eff_Acc
    logical, intent(in), optional :: verbose
    logical :: verb
    
    !!Set verbosity
    if(present(verbose))then
       verb=verbose
    else
       verb=.TRUE.
    end if
    
    Error=abs(log(exp(X1-X2)))
    !!Weigh in the fact that if the sace is differnt differnt number of significant numbers are neede
    Eff_Acc = abs(Acc*ceiling(abs(real(X1,dp))+1))
    if(abs(error).gt.Eff_Acc)then
       test_diff_exp_cmplx=.true.
       if(verb)then
          write(*,*) ' --- TDEC (verbose check) --- '
          write(*,*) '            Error:',abs(error),'>',Acc
          write(*,*) ' Effective  Error:',abs(error),'>',Eff_Acc
          write(*,*) 'Input1:',X1
          write(*,*) 'Input2:',X2
          write(*,*) 'Diff:',log(exp(X1-X2))
       end if
    else
       test_diff_exp_cmplx=.false.

    end if
  end function test_diff_exp_cmplx




  !!-------------------------------
  !!          Test Lorenz matrices
  !!-------------------------------
  subroutine test_lorenz_orthogonal_internal(Groups,Cmat,Rmat,Type)
    integer, intent(IN) :: Groups
    integer, intent(IN) :: Cmat(Groups,2*Groups),Rmat(Groups,2*Groups)
    Character(*), intent(IN) :: Type !!null or dual
    integer :: n,m,nomin,sqden


    do n=1,(Groups-1)
       do m=(n+1),Groups
          call compute_lorenz_overlap(groups,Cmat(n,:),Rmat(n,:),&
               Cmat(m,:),Rmat(m,:),nomin,sqden)
          if((abs(nomin).ne.0).or.(sqden.ne.1))then
             write(*,*) 'Testing that the mutual overlap of ',Type,'-vector is 0...'
             write(*,*) 'Overlap between ',Type,'-vector',n,'and',m
             write(*,*) 'is:', nomin,'/sqrt(',sqden,')'
             write(*,*) 'ERROR: The overlap of ',Type,'-vectors is not 0...'
             call exit(-1)
          end if
       end do
    end do
  end subroutine test_lorenz_orthogonal_internal

  
  subroutine test_lorenz_orthogonal_external(Groups,Cmat1,Rmat1,Cmat2,Rmat2,&
       Type1,Type2)
    integer, intent(IN) :: Groups
    integer, intent(IN) :: Cmat1(Groups,2*Groups),Rmat1(Groups,2*Groups)
    integer, intent(IN) :: Cmat2(Groups,2*Groups),Rmat2(Groups,2*Groups)
    Character(*),intent(IN) :: Type1, Type2 !!null, dual or charge
    integer :: n,m,nomin,sqden
    
    do n=1,Groups
       do m=1,Groups
          call compute_lorenz_overlap(groups,Cmat1(n,:),Rmat1(n,:),&
               Cmat2(m,:),Rmat2(m,:),nomin,sqden)
          if((abs(nomin).ne.0).or.(sqden.ne.1))then
             write(*,*) 'Testing that the overlap of ',Type1,'-vector and ',Type2,'-vector is 0...'
             write(*,*) 'Overlap between ',Type1,'-vector',n,'and ',Type2,'-vector',m
             write(*,*) 'is:', nomin,'/sqrt(',sqden,')'
             write(*,*) 'ERROR: The overlap of ',Type1,'-vector and ',Type2,'-vectoris not 0...'
             call exit(-1)
          end if
       end do
    end do
  
  end subroutine test_lorenz_orthogonal_external
  
  subroutine test_lorenz_orthogonal_charge(Groups,Cmat1,Rmat1,Cmat2,Rin,Type1,Type2)
    integer, intent(IN) :: Groups
    integer, intent(IN) :: Cmat1(Groups,2*Groups),Rmat1(Groups,2*Groups)
    integer, intent(IN) :: Cmat2(Groups,2*Groups),Rin(2*Groups)
    integer :: Rmat2(Groups,2*Groups)
    Character(*),intent(IN) :: Type1,Type2 !!null, dual or charge
    integer :: n
    
    !!Construct a R-matrix insead of r-vector
    do n=1,Groups
       Rmat2(n,:)=Rin(:)
    end do
    call test_lorenz_orthogonal_external(Groups,Cmat1,Rmat1,Cmat2,Rmat2,Type1,Type2)
  end subroutine test_lorenz_orthogonal_charge

  
  subroutine test_lorenz_matrix_charge(Groups,Cmat1,Rmat1,Cmat2,Rin,Type1,Type2,Matrix)
    integer, intent(IN) :: Groups
    integer, intent(IN) :: Cmat1(Groups,2*Groups),Rmat1(Groups,2*Groups)
    integer, intent(IN) :: Cmat2(Groups,2*Groups),Rin(2*Groups)
    integer, intent(IN) :: Matrix(Groups,Groups)
    integer :: Rmat2(Groups,2*Groups)
    Character(*),intent(IN) :: Type1,Type2 !!null, dual or charge
    integer :: n
    
    !!Construct a R-matrix insead of r-vector
    do n=1,Groups
       Rmat2(n,:)=Rin(:)
    end do
    call test_lorenz_matrix(Groups,Cmat1,Rmat1,Cmat2,Rmat2,Type1,Type2,Matrix)
  end subroutine test_lorenz_matrix_charge


  
  subroutine test_lorenz_matrix(Groups,Cmat1,Rmat1,Cmat2,Rmat2,&
       Type1,Type2,Matrix)
    integer, intent(IN) :: Groups
    integer, intent(IN) :: Cmat1(Groups,2*Groups),Rmat1(Groups,2*Groups)
    integer, intent(IN) :: Cmat2(Groups,2*Groups),Rmat2(Groups,2*Groups)
    integer, intent(IN) :: Matrix(Groups,Groups)
    Character(*),intent(IN) :: Type1, Type2 !!null, dual or charge
    integer :: n,m,nomin,sqden
    
    !!Sanity check - null vectors are still nullvectors!!
    
    
    do n=1,Groups
       do m=1,Groups
          call compute_lorenz_overlap(groups,Cmat1(n,:),Rmat1(n,:),&
               Cmat2(m,:),Rmat2(m,:),nomin,sqden)
          if((abs(nomin).ne.Matrix(m,n)).or.(sqden.ne.1))then
             write(*,*) 'Testing that the overlap of ',Type1,'-vector and ',Type2,'-vector is '
             call print_int_matrix(Matrix)
             write(*,*) 'Overlap between ',Type1,'-vector',n,'and ',Type2,'-vector',m
             write(*,*) 'is:', nomin,'/sqrt(',sqden,')'
             write(*,*) 'ERROR: The overlap of ',Type1,'-vector and ',Type2,'-vectoris not ',Matrix(m,n)
             call exit(-1)
          end if
       end do
    end do
  end subroutine test_lorenz_matrix
    


  
!!!-------------------------------------
!!!    Complex modifyers
!!!------------------------------------- 
  
  complex(KIND=dpc) function mod_2pi(logz) result(mod_logz)
    !! This function returns the argument of logz to be 
    !! within the range 0 <= Im(logz) < 2*pi.
    complex(KIND=dpc), INTENT(IN) :: logz
    real(KIND=dp) :: im_piece
    im_piece = aimag(logz)/(2*pi)
    im_piece = im_piece - nint(im_piece)
    mod_logz = real(logz,dp) + iunit*2*pi*im_piece
  end function mod_2pi


end module test_utilities
