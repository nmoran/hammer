PROGRAM ed
  
  USE global_torus
  USE global_ed
  USE fock_basis
  use basis_function
  USE hamiltonian
  USE sparse
  USE quantum_numbers
  USE DiscretizedWaveFunctions
  use channels
  use input_misc
  use misc_frac_sqrt
  USE EDWaveFunction
  
  IMPLICIT NONE
  
  ! Exact diagonalization for torus lll. 
  ! Task list: (Largely obsolete though /Micke)
  !   * Find optimal sparse storage for complex hermitian matrices
  !   * Incorporate IETL and ARPACK Lanczos solvers
  !   * Use Ari's Lanczos only for testing
  !   * Figure out how to encode Hamiltonian data conveniently.
  !     Perhaps we write separate programs for calculating Fock expansions of model
  !     Hamiltonians...
  !   * Implement the relevant pseudopotentials
  
  integer ::Ne,Ns,cells,p,q,Ksector,MC,LL,NumEigs,PotNum
  real(KIND=dp) ::tau_im,tau_re
  !!Flags to check if the global variables are set
  logical::set_Ne,set_Ns,set_cells,set_p,set_q,set_only_V,set_Eigs,set_log
  logical::set_k,set_MC,set_I,set_R,set_LL,set_reuse,set_use_T2,set_pot
  logical::set_load
  
  !!The default catalouge to write to
  character(len=100) :: output_dir="result"

  !!Monte-Carlo multiplier
  integer, parameter :: MC_default=0
  !!Landau level 
  integer, parameter :: LL_default=0
  !!MOmentum sector
  integer, parameter :: K_default=0
  !!Eigenvalues
  integer, parameter :: Eigs_default=1
  !!Potentail
  integer, parameter :: Pot_default=0
  !! Torus specifics
  real(kind=dp), parameter :: I_default=1.d0,R_default=0.d0
  
  
  
  !!Set all the flags to negative
  set_Ne=.FALSE.
  set_Ns=.FALSE.
  set_cells=.FALSE.
  set_p=.FALSE.
  set_q=.FALSE.
  set_K=.FALSE.
  set_MC=.FALSE.
  set_I=.FALSE.
  set_R=.FALSE.
  set_LL=.FALSE.
  set_Eigs=.FALSE.
  set_reuse=.FALSE.
  set_only_V=.FALSE.
  set_use_T2=.FALSE.
  set_pot=.FALSE.
  set_log=.FALSE.
  set_load=.FALSE.
  
  !!Retrieve all the input
  call retreive_input()
  !!
  call sanitize_input
  !!Run the main program
  CALL main()
  
CONTAINS
  
  
  SUBROUTINE main()
    INTEGER :: N_basis               ! basis size
    complex(KIND=dpc), ALLOCATABLE :: ARvector(:) ! ground state eigenvector arpack
    complex(KIND=dpc), ALLOCATABLE :: EigenVectors(:,:) ! lowest eigenvectors
    REAL(KIND=dp), ALLOCATABLE :: EigenValues(:) ! lowest eigenvalues
    INTEGER :: ierr                  ! allocation error
    TYPE(spa) :: hs                  ! hamiltonian matrix
    INTEGER :: eigno
    real :: StartTime, LapTime, TmpTime
    character(Len=1024):: File_wfn_Re,File_wfn_Im
    ! *********************
    ! Exact diagonalization
    ! *********************
    
    ! Read input etc
    
    write(*,*) 'Ns:',Ns
    write(*,*) 'tau_re:',tau_re
    write(*,*) 'tau_im:',tau_im
    CALL input_torus(Ns,tau_re,tau_im)
    CALL input_reader(p,q,cells,LL,set_only_V,Ksector,set_use_T2,NumEigs,PotNum)
    
    
    ! StartTime = time()
    ! LapTime   = time()
    CALL cpu_time(StartTime)
    CALL cpu_time(LapTime)
    
    CALL init_qns(Nup,Ndn,Ns)
    
    ! Generate the Fock basis
    N_basis=make_basis(Ns,Nele)
    write(*,*) 'Fock basis generated'
    
    if(set_load)then !!Only import fock_coeffs do not diagonalize
       !!FIXME put stuff here
       
       !!Allocate the vector for the fock coefficients
       ALLOCATE(ARvector(N_basis))
       !!Load the Fock-coeffients
       write(*,*) 'Load the Fock coeficients'
       call load_fock_conf(ARvector)
       
    else !!Diagonalize and do all that stuff
       
       !Construct the sparse Hamiltonian
       write(*,*) 'calculate V_1234 elements'
       CALL init_hamiltonian()
       
       if(VOnly)then
          write(*,*) "Computation finished"
       else
          
          WRITE(*,*) 'Basis size is', N_basis
          
          
          Write(*,*) 'construct hamiltonian'
          CALL sp_hamiltonian( hs, N_basis )
          write(*,*) 
          write(*,*) '---------------------------------------------'
          
          ALLOCATE(ARvector(N_basis),   &
               & EigenValues(neigs),  &
               & EigenVectors(N_basis,neigs),stat=ierr)
          IF (ierr/=0)then
             Write(*,*) ' PROBLEM in allocate!'
             call exit(-1)
          end IF

          
          !!Info about diagonalization
          CALL cpu_time(TmpTime)
          WRITE(*,'(A,I6,A)') ' It took',nint(TmpTime-LapTime),' seconds to do construct hamiltonian' 
          !WRITE(*,'(A,I6,A)') ' It took',time() - LapTime,' seconds to do construct hamiltonian' 
          CALL cpu_time(LapTime)
          !LapTime   = time()
          
          WRITE(*,*) 'Number of eigenvalues to compute:', neigs
          !!Diagonalize the Hamiltonian"
          if(N_basis.eq.1)then
             write(*,*) 'Hamiltonian is 1x1 so no need to diagonalize'
             !!write(*,*) 'Hamiltonian a:',hs%a
             !!write(*,*) 'Hamiltonian ja:',hs%ja
             !!write(*,*) 'Hamiltonian ia:',hs%ia
             if(neigs.eq.1)then
                !!Set Eigenvectors and eigenvalues manually:
                EigenValues=real(hs%a(1),kind=dp)
                EigenVectors=1.d0 !!Trivially normalized
             else
                write(*,*) 'ERROR: You have requeste more more that one eigenvalue!'
                call exit(-1)
             end if
          else
             Write(*,*) 'Diagonalize Hamiltonian'
             CALL ArpackSolver( hs, EigenValues, neigs, N_basis, EigenVectors )
             write(*,*) 'done'
          end if
          !!Extract GS-eigenvector
          ARvector = EigenVectors(:,1)
          
          !!Info about diagonalization
          CALL cpu_time(TmpTime)
          
          !WRITE(*,'(A,I6,A)') ' It took',time()-LapTime,' seconds to diagonalize hamiltonian' 
          WRITE(*,'(A,I6,A)') ' It took',nint(TmpTime-LapTime),' seconds to diagonalize hamiltonian' 
          
          WRITE(*,*) 'Exporing all Enegies and EigenVectors'
          call export_all_energy(EigenValues,EigenVectors,N_basis)
          
          ! export the Hamiltonian for testing (turned off)
          IF (.FALSE.) CALL spa_export(hs,TRIM(output_dir)//'/spaH')
          !!Export the fock basis
          CALL exportFockBasis
          ! export the ground state Fock expansion
          CALL exportVect( ARvector, TRIM(output_dir)//'/arpackvect0.dat')
          CALL exportVect( ARvector, TRIM(output_dir)//'/fock_coeffs.dat')
          
          ! MAIN OUTPUT
          WRITE(*,'(/,TR5,A)') '*********************************************'
          WRITE(*,'(TR5,A)') '       Nele Qnomin Qdenom  L1/L2 LDelta/L1     Nmb   K     dV1      EAR'
          WRITE(*,'(TR5,A,I4,2I7, F7.2, F10.2, I8, I4, F7.3,F10.5)') &
               'Output:', &
               Nele, Qnomin, Qdenom, tau_im_dp, tau_re_dp, N_basis, Kmom, dV1,EigenValues(1)
          WRITE(*,'(TR5,A,/)') '*********************************************'
          
          !!Info about diagonalization
          CALL cpu_time(TmpTime)
          !WRITE(*,'(A,I6,A)') ' It took',time()-StartTime,' seconds to solve the ED problem in full' 
          WRITE(*,'(A,I6,A)') ' It took',nint(TmpTime-StartTime),' seconds to solve the ED problem in full' 
          
       end if
    end if
    
    !!Compute a monte carlo sample of the wave_function
    if(MC.gt.0)then
       write(*,*) "Sample the wave-function"
       !!Initialize the geometry
       call init_ed_basis_geometry(working_Ns,Nele,tau_dpc,T1Basis)
       if(set_load.or.(Neigs.eq.1))then
          !Only use the lowest vector,
          !!which is the same as the loaded vector 
          ! initialize ExactWaveFunctions
          CALL init_edwf(ARvector,working_Ns,Nele)
          !!Sample
          call set_output_dir(output_dir)
          CALL SampleTheWaveFunction(MC,set_reuse,nele,&
               TRIM(output_dir)//'/ed_re.rnd',TRIM(output_dir)//'/ed_im.rnd',&
               tau_re+iunit*tau_im,ed_psi,.FALSE.,working_ns)
       else
          do eigno=1,neigs !!Loop over the energy eigenstates
             CALL init_edwf(EigenVectors(:,eigno),working_Ns,Nele)
             write(*,*) 'Eigenvalue no:',eigno
             !!Construct the file_names
             write(File_wfn_Re,'(A,I0,A)') TRIM(output_dir)//'/ed_re_',eigno,'.rnd'
             write(File_wfn_Im,'(A,I0,A)') TRIM(output_dir)//'/ed_im_',eigno,'.rnd'
             write(*,*) 'write re to file:',trim(File_wfn_Re)
             write(*,*) 'write im to file:',trim(File_wfn_Im)
             call set_output_dir(output_dir)
             if(eigno.eq.1)then
                CALL SampleTheWaveFunction(MC,set_reuse,nele,&
                     trim(File_wfn_Re),trim(File_wfn_Im),&
                     tau_re+iunit*tau_im,ed_psi,.FALSE.,working_ns)
             else !!If more than one wave function
                !!reuse the coordinates from the first run
                CALL SampleTheWaveFunction(MC,.TRUE.,nele,&
                     trim(File_wfn_Re),trim(File_wfn_Im),&
                     tau_re+iunit*tau_im,ed_psi,.FALSE.,working_ns)
             end if
          end do
       end if
    end if
  END SUBROUTINE main
  
  
  subroutine load_fock_conf(Fock_coeff)
    complex(kind=dpc), intent(OUT) :: Fock_coeff(:)
    !!Fist collumn is real part
    !!Second collumn is imaginary part
    real(kind=dp) :: tmp_coeff(2)     
    Integer  :: ch_coeff = 592,i
    
    !!write(*,*) 'Importing Fock coeffs'
    OPEN(ch_coeff,FILE=TRIM(TRIM(output_dir)//'/fock_coeffs.dat'),STATUS='old')
    DO i = 1, size(Fock_coeff)
       !!Read the coefficients
       read(ch_coeff,*)  tmp_coeff(:)
       Fock_coeff(i)= tmp_coeff(1) + iunit*tmp_coeff(2)
       !!write(*,*) i,Fock_coeff(i)
    END DO
    CLOSE(ch_coeff)
  end subroutine load_fock_conf
  
  
  SUBROUTINE exportFockBasis
    USE fock_basis
    ! exports the Fock basis states
    INTEGER :: i
    INTEGER :: ch1 = 875
    OPEN(ch1,FILE=TRIM(output_dir)//'/fock_basis.dat',STATUS='unknown')
    DO i = 1, SIZE(ibasis,1)
       ! write the coeffiecient
       WRITE(ch1,*) ibasis(i,:)
    END DO
    CLOSE(ch1)
    WRITE(*,*) 'Exported a state to file ',TRIM(output_dir),'/fock_basis.dat'
  END SUBROUTINE exportFockBasis
  
  
  SUBROUTINE exportVect( coefs , filex )
    USE fock_basis
    complex(KIND=dpc), DIMENSION(:), INTENT(IN) :: coefs
    CHARACTER(*), INTENT(IN) :: filex
    ! exports the Fock expansion of a ( ground ) state vector in coefs
    INTEGER :: i
    INTEGER :: ch = 874
    OPEN(ch,FILE=TRIM(filex),STATUS='unknown')
    
    DO i = 1, SIZE(ibasis,1)
       ! write the coeffiecient
       WRITE(ch,*) real(coefs(i),dp),aimag(coefs(i))
    END DO
    CLOSE(ch)
    WRITE(*,*) 'Exported a state to file ', filex
  END SUBROUTINE exportVect
  
  subroutine export_gs_energy(energy)
    REAL(KIND=dp), INTENT(IN) :: energy
    INTEGER :: chgs = 739
    OPEN(chgs,FILE=TRIM(TRIM(output_dir)//'/gs_energy.dat'),STATUS='unknown')
    write(chgs,*) energy
    close(chgs)
  end subroutine export_gs_energy
  
  subroutine export_all_energy(energyList,VectorList,Basis_Size)
    use sort
    INTEGER, INTENT(IN) :: Basis_Size
    REAL(KIND=dp), INTENT(IN) :: energyList(neigs)
    COMPLEX(KIND=dpc), INTENT(IN) :: VectorList(neigs,Basis_size)
    COMPLEX(KIND=dpc) :: Ord_Comp(neigs)
    INTEGER :: ch_evec_re=737,ch_evec_im=738,ch_eval=739,i,j
    OPEN(ch_eval,FILE=TRIM(TRIM(output_dir)//'/energies.dat'),STATUS='unknown')
    OPEN(ch_evec_re,FILE=TRIM(TRIM(output_dir)//'/eigenvectors_re.dat'),STATUS='unknown')
    OPEN(ch_evec_im,FILE=TRIM(TRIM(output_dir)//'/eigenvectors_im.dat'),STATUS='unknown')
    
    !!Write the energies
    do i=1,neigs
       write(ch_eval,*) energyList(i)
    end do
    
    !!Write the eigenstates
    do j=1,Basis_Size
       do i=1,neigs
          Ord_Comp(i)=VectorList(i,j)
       end do
       write(ch_evec_re,*) real(Ord_Comp,KIND=dp)
       write(ch_evec_im,*) aimag(Ord_Comp)
    end do
    
    close(ch_eval)
    close(ch_evec_re)
    close(ch_evec_im)
  end subroutine export_all_energy
  
  
  subroutine retreive_input()
    integer::narg,cptArg
    character(len=100)::name
    !!Number of inputs to skip
    integer::skip_next
    
    !!Initally this sytem will barf (in an uncontrolled way) if the input is wrong
    
    !Check if arguments are found
    narg=command_argument_count()
    
    skip_next=0
    if(narg>0)then
       !loop across options
       do cptArg=1,narg
          if(skip_next.gt.0)then
             !!Skip the next skip_next arguments
             !write(*,*) 'skip an argument'
             skip_next=skip_next-1
             cycle
          end if
          !!Get the comand argument
          call get_command_argument(cptArg,name)
          !!Select from all the caes
          select case(adjustl(name))
             !!Forst we test against flags
          case("--version")
             write(*,*) "This is program ed_tours : Version 0.1"
             call exit(0)
          case("--help","-h")
             call print_help()
             call exit(0)
          case("--load")
             call set_unset_flag(set_load,'--load',print_help)
          case("-r","--reuse")
             call set_unset_flag(set_reuse,'-r',print_help)
          case("-V","--V-only")
             call set_unset_flag(set_only_v,'-V',print_help)
          case("-t","--T2")
             call set_unset_flag(set_use_T2,'-T2',print_help)
          case("-l","--log")
             call set_unset_flag(set_log,'-l',print_help)
          case("-E","--Eigs")
             call set_unset_flag(set_Eigs,'-E',print_help)
             call recieve_integer_input(NumEigs,'Eigs',cptArg+1)
             call check_positive_integer(NumEigs,'-E',print_help)
             skip_next=1
          case("-P","--Potential")
             call set_unset_flag(set_pot,'-P',print_help)
             call recieve_integer_input(PotNum,'Potentail',cptArg+1)
             call check_non_negative_integer(PotNum,'-P',print_help)
             skip_next=1
          case("-L","--LL")
             call set_unset_flag(set_LL,'-L',print_help)
             call recieve_integer_input(LL,'LL',cptArg+1)
             call check_non_negative_integer(LL,'-L',print_help)
             skip_next=1
          case("-p","--nomin")
             call set_unset_flag(set_p,'-p',print_help)
             call recieve_integer_input(p,'p',cptArg+1)
             call check_positive_integer(p,'-p',print_help)
             skip_next=1
          case("-q","--denomin")
             call set_unset_flag(set_q,'-q',print_help)
             call recieve_integer_input(q,'q',cptArg+1)
             call check_positive_integer(q,'-q',print_help)
             skip_next=1
          case("-Ns","--fluxes")
             call set_unset_flag(set_Ns,'-Ns',print_help)
             call recieve_integer_input(Ns,'Ns',cptArg+1)
             call check_positive_integer(Ns,'-Ns',print_help)
             skip_next=1
          case("-Ne",'--particles')
             call set_unset_flag(set_Ne,'-Ne',print_help)
             call recieve_integer_input(Ne,'Ne',cptArg+1)
             call check_positive_integer(Ne,'-Ne',print_help)
             skip_next=1
          case("-c","--cells")
             call set_unset_flag(set_cells,'-c',print_help)
             call recieve_integer_input(cells,'Cells',cptArg+1)
             call check_positive_integer(cells,'-c',print_help)
             skip_next=1
          case("-MC","--MC-points","-N")
             call set_unset_flag(set_MC,'-MC',print_help)
             call recieve_integer_input(MC,'MC-points',cptArg+1)
             call check_non_negative_integer(MC,'-MC',print_help)
             skip_next=1
          case("-K","--momentum")
             call set_unset_flag(set_K,'-K',print_help)
             call recieve_integer_input(Ksector,'K',cptArg+1)
             skip_next=1
          case("-I","--tau_im")
             call set_unset_flag(set_I,'-I',print_help)
             call recieve_float_input(tau_im,'tau_im',cptArg+1)
             skip_next=1
          case("-R","--tau_re")
             call set_unset_flag(set_R,'-R',print_help)
             call recieve_float_input(tau_re,'tau_re',cptArg+1)
             skip_next=1
          case('--output_dir')
             call recieve_string_input(output_dir,'--output_dir',cptArg+1)
             skip_next=1
          case default
             call print_help()
             write(stderr,*)"ERROR: Input"
             write(stderr,*)"Option: ",name," unknown at position",cptArg
             write(*,*)
             call exit(-2)
          end select
       end do
    endif
    
  end subroutine retreive_input
  
  
  subroutine print_help()
    write(*,'(A)') 'Usage: ed_torus [OPTION]...'
    write(*,'(A)') 'Compute the exact groundstate as a particular filling fraction.'
    write(*,*) ''
    write(*,*) '------- Filling fraction specifics -------'
    write(*,*) ' -p,  --nomin   <p>         Nominator of filling fraction.'
    write(*,*) ' -q,  --denomin <q>         Denominator of filling fraction.'
    write(*,*) '                            equals the number of groups.'
    write(*,*) '------- System size -------'
    write(*,*) '  Only one of -c, -Ne and -Ns needs to be supplied.'
    write(*,*) '  If more than one flag is set, they will be compared'
    write(*,*) '  for internal consistency.'
    write(*,*) ''
    write(*,*) ' -c,  --cells      <p>      Number of fundamental unit cells.'
    write(*,*) ' -Ne, --particles  <Ne>     Number of particles.'
    write(*,*) ' -Ns, --fluxes     <Ns>     Number of flux quanta = states.'
    write(*,*) ''
    write(*,*) '------- Torus Info -------'
    write(*,*) ''
    write(*,*) ' -I,  --tau_im      <I>     Imaginary part of tau.'
    write(*,*) '                            Default tau_im=',I_default
    write(*,*) ' -R,  --tau_re      <R>     Imaginary part of tau.'
    write(*,*) '                            Default tau_re=',R_default
    write(*,*) ' -l,  --log                 Input -I in log scale'
    write(*,*) ''
    write(*,*) '------- Information about the poential -------'
    write(*,*) ''
    write(*,*) ' --load                     Load configuration coefficents from'
    write(*,*) '                            <dir>/fock_coeffs.dat'
    write(*,*) '                            NB: disables diagonalization'
    write(*,*) ' -V                         Only compute the V_ijkl and the quit'
    write(*,*) ' -P, --Potential            Type of potential'
    write(*,*) '                            0: Laughlin 1/3 pseudopoenteial'
    write(*,*) '                            1: Unscreened coulomb'
    write(*,*) '                            2: Screened coulomb'
    write(*,*) '                            FIXME: screening length is fixed'
    write(*,*) ''
    write(*,*) ' -E, --Eigs                 Number of eigenvalues to compute'
    write(*,*) '                            Max is basis size-2'
    write(*,*) ''
    write(*,*) '------- Monte-Carlo specifics -------'
    write(*,*) ''
    write(*,*) ' -r,  --reuse               Resue coordinates in "<dir>/coord_xx.rnd"'
    write(*,*) '                            and "<dir>/coord_yy.rnd".'
    write(*,*) ' -K,  --momentum   <K>      Momentum sector.'
    write(*,*) ' -N, -MC, --MC-points <MC>  Number of Monte-Carlo points to sample.'
    write(*,'(A,I2)') '                             Default MC=',MC_default
    write(*,*) '                            Genereated Monte Carlo coordiantes are  written to'
    write(*,*) '                            "<dir>/coord_xx.rnd" and "<dir>/coord_yy.rnd"'
    write(*,*) '                            Genereated Monte Carlo wave function is written to'
    write(*,*) '                            "<dir>/chiral_re.rnd" and "<dir>/chiral_im.rnd"'
    write(*,*) '  --output_dir <dir>        Set the output dir to <dir>.'
    write(*,*) '                            Default directory <dir>=result'
    write(*,*) '                            NB: Do no use trailing slashes'
    write(*,*) '                            NB: Use <dir>="." for current dir'
    write(*,*) ''
    write(*,*) '  -h, --help                Print this help and exit' 
    write(*,*) ''
    write(*,'(A)') 'Report chiral_wfn bugs to fremling@fysik.su.se'
  end subroutine print_help
  
  subroutine sanitize_input()
    integer :: local_p,local_q,local_cells
    !!Make sure the kappa_matrix has been supplied


    !!Compute and fill in the derived types..
    
    !!If Ne and Ns are provided, all others can be derived 
    if(set_Ne.and.set_Ns)then !!This gives the filling faction and cells
       local_p=Ne !!Will be reduced
       local_q=Ns !!Will be reduced
       call reduce_fraction(local_p,local_q)
       local_cells=Ne/local_p
       if(set_cells)then
          if(cells.ne.local_cells)then
             call error_single_compability(local_cells,' c',cells)
          end if
       else
          cells=local_cells
          set_cells=.true.
       end if
    end if
    
    if(set_cells)then !!If cells are set
       if(set_p)then  !!Test compliance for p
          if(set_Ne)then
             if(cells*p.ne.Ne)then
                call error_tripple_compability(p,cells,Ne,' p',' c','Ne')
             end if
          else !!if Ne is not set
             Ne=cells*p
             set_Ne=.true.
          end if
       end if
       if(set_q)then !!Test compliance for q
          if(set_Ns)then
             if(cells*q.ne.Ns)then
                call error_tripple_compability(q,cells,Ns,' q',' c','Ns')
             end if
          else !!if Ns is not set
             Ns=cells*q
             set_Ns=.true.
          end if
       end if
       if(set_Ne)then !!P assumed not to be set
          if(Ne.ne.cells*(Ne/cells))then
             call error_double_compability(cells,Ne,' c','Ne')
          else !!if the test is passed
             p=Ne/cells
             set_p=.true.
          end if
       end if
       if(set_Ns)then !!Q assumed not to be set
          if(Ns.ne.cells*(Ns/cells))then
             call error_double_compability(cells,Ns,' c','Ns')
          else !!if the test is passed
             Q=Ns/cells
             set_Q=.true.
          end if
       end if
    end if
    
    if(set_I.and.set_log)then !!Convert from log scale
       tau_im=exp(tau_im)
    end if
    
    call set_default_real(set_R,tau_re,R_default,'tau_re','tau_re')
    call set_default_real(set_I,tau_im,I_default,'tau_im','tau_im')
    call set_default_integer(set_Pot,PotNum,Pot_default,'Potential type','Pot_Type')
    call set_default_integer(set_LL,LL,LL_default,'Landau level','LL')
    if(set_only_V)then
       !!If only V_km is interesting then K, Eigs and MC need not be specified
       set_K=.TRUE.
       Ksector=0
       set_MC=.TRUE.
       MC=0
       set_Eigs=.TRUE.
       NumEigs=1
    else
       call set_default_integer(set_Eigs,NumEigs,Eigs_default,'Eigenvalues','Eigs')
       call set_default_integer(set_MC,MC,MC_default,'Monte-Calo ponts','MC')
    end if
    !!Test that tau_im > 0. 
    call check_positive_float(tau_im,'-I',print_help)
    
    
    
    !!Test that all flags with values are set
    call test_set_flag(set_p,'-p')
    call test_set_flag(set_q,'-q')
    call test_set_flag(set_cells,'-c')
    call test_set_flag(set_Ne,'-Ne')
    call test_set_flag(set_Ns,'-Ns')
    call test_set_flag(set_K,'-K')
    call test_set_flag(set_Eigs,'-E')
    call test_set_flag(set_MC,'-MC')
    call test_set_flag(set_I,'-I')
    call test_set_flag(set_R,'-R')
    call test_set_flag(set_LL,'-LL')
    
    
    
    
  end subroutine sanitize_input
  
  
  
  subroutine test_set_flag(Flag,Name)
    logical,intent(in) :: Flag
    character(*) :: Name
    if(.not.Flag)then
       write(stderr,*) 'ERROR: Input'
       write(stderr,*) Name,' has not been set, directly or indirectly'
       write(stderr,*) 'Try again!'
       write(*,*)
       call exit(-2)                
    end if
  end subroutine test_set_flag
  
  
  subroutine error_tripple_compability(I1,I2,I3,N1,N2,N3)
    integer,intent(in) :: I1,I2,I3
    character(*),intent(in) :: N1,N2,N3
    call print_help()
    write(stderr,*) 'ERROR: Input'
    write(stderr,*) N1,',',N2,',',N3,':',I1,I2,I3
    write(stderr,*) 'Not compliant with each other! Try again'
    write(*,*)
    call exit(-2)                
    
  end subroutine error_tripple_compability
  
  
  subroutine error_double_compability(I1,I2,N1,N2)
    integer,intent(in) :: I1,I2
    character(*),intent(in) :: N1,N2
    call print_help()
    write(stderr,*) 'ERROR: Input'
    write(stderr,*) N1,',',N2,':',I1,I2
    write(stderr,*) 'Not compliant with each other! Try again'
    write(*,*)
    call exit(-2)                
  end subroutine error_double_compability
  
  
  subroutine error_single_compability(I1,N1,F1)
    integer,intent(in) :: I1,F1
    character(*),intent(in) :: N1
    call print_help()
    write(stderr,*) 'ERROR: Input'
    write(stderr,*) 'Derived ',N1,'=',I1
    write(stderr,*) 'Not compliant input ',N1,'=',F1,'! Try again'
    write(*,*)
    call exit(-2)                
  end subroutine error_single_compability




  END PROGRAM ed
