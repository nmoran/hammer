module fock_coefficients

  use typedef
  use jacobi_structure_factor
  use jacobitheta
  use fock_coefficients_storage
  
  implicit none
  logical :: fock_verbose=.FALSE.


private
public fock_laughlin, laughlin_reduced_coeff, fock_verbose


contains
  
  complex(kind=dpc) function fock_laughlin(momentum_in,Kvalue,Ne,tau) result(coeff)
    integer, intent(in) :: Kvalue, Ne, momentum_in(Ne)
    complex(kind=dpc), intent(in) :: tau
    integer :: momentum(Ne)
    integer :: double_reduced_momentum(Ne)
    integer :: Ns,Raw_momentum,Valid_momentum,Current_momentum
    integer :: Degeneracy_counter
    Degeneracy_counter=0
    coeff = 0.d0
    
    Ns=Ne*Kvalue
    momentum=modulo(momentum_in,Ns)
    Raw_momentum=modulo((Ns*(Ne-1))/2,Ns)
    Current_momentum=modulo(sum(momentum),Ns)
    !!write(*,*) 'Momentum: curent,raw',Current_momentum,Valid_momentum

    !!Thest the other K-fold degenerate posibilityes
    do Degeneracy_counter=0,(Kvalue-1)
       !write(*,*) 'Testing degeneracy case:',Degeneracy_counter
       Valid_momentum=modulo(Raw_momentum+Degeneracy_counter*Ne,Ns)
       if(Current_momentum.eq.Valid_momentum)then
          !write(*,*) 'Momentum match!'
          !write(*,*) 'Valid momentum:',Valid_momentum
          !write(*,*) 'Momuentum:',momentum
          momentum=modulo(momentum-Degeneracy_counter,Ns)
          !write(*,*) 'Remove degenracy shift'
          !write(*,*) 'Momuentum:',momentum

          !!The momentum could be **half integer**, so we treat the double value
          !!First reduce by the degeneracy counter
          !write(*,*) 'K,Ne,Ns',Kvalue,Ne,Ns
          double_reduced_momentum=2*momentum-(Ne-1)*Kvalue
          !write(*,*) 'Raw Double Reduced momentum:',double_reduced_momentum
          if(modulo(sum(double_reduced_momentum),Ns).ne.0)then
             write(*,*) 'ERROR: The reduction of momentum has gone wrong'
             write(*,*) 'For Ne,Kval,Ns:',Ne,Kvalue,Ns
             write(*,*) 'momentum in:',momentum_in
             write(*,*) 'reduced momentum',double_reduced_momentum
             call exit(-2)
          end if

          !!Compute the coefficeint
          !!Make sure the T_list is neutral
          double_reduced_momentum(Ne)=double_reduced_momentum(Ne)-sum(double_reduced_momentum)
          coeff = laughlin_reduced_coeff(double_reduced_momentum,Ne,Kvalue,tau)
          exit !!Break out of loop
       end if
    end do
    
  end function fock_laughlin
  
  complex(kind=dpc) recursive function laughlin_reduced_coeff(DT_list,Ne,Kvalue,tau) result(coeff)
    integer, intent(in) :: DT_list(Ne),Ne,Kvalue
    complex(kind=dpc), intent(in) :: tau
    integer :: Ns,particle_no
    complex(kind=dpc):: coeff2
    logical :: is_zero,sucsessfull_lookup
    
    
    !!Check that the DT_list is balanced
    if(sum(DT_list).ne.0)then
       write(*,*) 'ERROR: Input to laughlin is not zero'
       write(*,*) 'Ne=',Ne,'Kvalue=',Kvalue
       write(*,*) 'Tlist:',DT_list
       call exit(-2)
    end if

    !!Check that the DT_list has correct parity
    if(Ne.gt.1)then !!For Ne=1 we know that T=0
       do particle_no=1,Ne
          if(modulo(DT_list(particle_no)-(Ne-1)*Kvalue,2).ne.0)then
             write(*,*) 'ERROR: Offset in input has wrong parity'
             write(*,*) 'Ne=',Ne,'Kvalue=',Kvalue
             write(*,*) 'DTlist:',DT_list
             call exit(-2)
          end if
       end do
    end if

    Ns=Kvalue*Ne
    if(modulo(Kvalue,2).eq.1)then 
       !!For odd k-values the antisymetry makes the result zero
       !! if two indexes are the same
       is_zero=is_dublicate_indexes(DT_list,Ne,Kvalue)
    else
       is_zero=.false.
    end if
    
    
    if(is_zero)then
       !!write(*,*) 'Coefficient is zero by antisymetry'
       coeff=czero
    else !!Value needs to be computed
       !!Try to look up the value in the memory cashe
       call get_fock_coeff(Ne,tau,DT_list,Kvalue,coeff,sucsessfull_lookup)
       !!If the value could not be looked up, compute it
       if(.not.sucsessfull_lookup)then
          !!
          if(Ne.eq.1)then
             coeff=1.d0
             coeff2=coeff
          elseif(Ne.eq.2)then
             coeff=laughlin_reduced_coeff_ne_2(DT_list,Kvalue,tau)
             coeff2=coeff
          elseif(Ne.eq.3)then
             coeff2=laughlin_reduced_coeff_ne_3(DT_list,Kvalue,tau)
             coeff=laughlin_reduced_coeff_ne_gen(DT_list,Ne,Kvalue,tau)
          elseif(Ne.eq.4)then
             coeff2=laughlin_reduced_coeff_ne_4(DT_list,Kvalue,tau)
             coeff=laughlin_reduced_coeff_ne_gen(DT_list,Ne,Kvalue,tau)
          else !FIXME construc the recursion for larger values
             coeff=laughlin_reduced_coeff_ne_gen(DT_list,Ne,Kvalue,tau)
             coeff2=coeff
          end if
          
          if(abs(coeff/coeff2-1.d0).gt.1.d-10)then
             write(*,*) 'ERROR:'
             write(*,*) 'Coeffs different for Ne,Kvalue',Ne,Kvalue
             write(*,*) 'coeff_Ne: ', coeff2
             write(*,*) 'coeff_gen:', coeff
             write(*,*) 'quituient-1:', coeff/coeff2
             write(*,*) 'DT_list:',DT_list
             call exit(-2)
          end if
          
          !!And put it back in the list of computed values
          call set_fock_coeff(Ne,tau,DT_list,Kvalue,coeff)
       end if
    end if
    
  end function laughlin_reduced_coeff
  
  logical function is_dublicate_indexes(DT_list_in,Ne,Kvalue) result(is_same)
    integer, intent(IN) :: DT_list_in(Ne),Ne,Kvalue
    integer :: DT_list(Ne), m1,m2
    
    is_same=.false.
    !!Put all values one the same moduar range
    DT_list=modulo(DT_list_in,Ne*Kvalue*2)

    do m1=1,Ne-1
       do m2=m1+1,Ne
          if(DT_list(m1).eq.DT_list(m2))then
             is_same=.true.
             return
          end if
       end do
    end do
  end function is_dublicate_indexes
  
  complex(kind=dpc) function laughlin_reduced_coeff_ne_2(DT_list,Kvalue,tau) result(coeff)
    !!This is the special solution for Ne=2
    !!It assumes that the DT_list is properly balanced and has correct parity
    integer, parameter :: Ne=2
    integer, intent(in) :: DT_list(Ne),Kvalue
    complex(kind=dpc), intent(in) :: tau

    integer :: T_shift_list(Ne)
    real(kind=dp) :: T_list(Ne)
    
    !!Here the momentum has an offset of Kval(Ne-1)/2 whioch could he half integer, so we need to keep track of that.
    T_list=real(DT_list,dp)/2
    T_shift_list=(DT_list-Kvalue)/2
    
    !write(*,*) 'T-list:',T_list
    !!when Ne=2 there is alon one Z_factor and one phase factor.
    !!We can handle these explicitly
    coeff=exp(iunit*pi*T_list(1))*Z_factor(T_shift_list(1),Kvalue,tau)
  end function laughlin_reduced_coeff_ne_2
  
  
  complex(kind=dpc) function laughlin_reduced_coeff_ne_3(DT_list,Kvalue,tau) result(coeff)
    !!This is the special solution for Ne=3
    !!It assumes that the DT_list is properly balanced and has correct parity
    integer, parameter :: Ne=3
    integer, intent(in) :: DT_list(Ne),Kvalue
    complex(kind=dpc), intent(in) :: tau
    complex(kind=dpc) :: tmp_coeff
    complex(kind=dpc) :: coeff_phase,coeff_weight,coeff_Laughlin,coeff_Z_1,coeff_Z_2
    
    integer :: T_list(Ne),Z_1_arg,Z_2_arg
    
    integer :: t,double_shift_t,ds_t_vector(Ne-1)
    real(kind=dp) :: t_shift,theta_argument

    !!Here the momentum has an offset of Kval(Ne-1)/2 which is always an integer
    !! Thus we can safety divide everything by two
    T_list=DT_list/2
   
    
    !!Initialize the coeffiecnt
    coeff=0.d0
    if(fock_verbose)then
       write(*,*) '................'
       write(*,*)  'Tlist:',T_list
    end if
    !! WE sum over the variable t in Z_2*K + K/2
    do t=1,2*Kvalue

       t_shift=t+real(Kvalue,dp)/2
       double_shift_t=2*t+Kvalue
       ds_t_vector(1)=double_shift_t
       ds_t_vector(2)=-double_shift_t
       !!Compute the pahse factor
       coeff_phase=exp(iunit*pi*(T_list(1)+T_list(2)))
       !! Compute the recursive laughlin contribution
       coeff_laughlin=laughlin_reduced_coeff(ds_t_vector,Ne-1,Kvalue,tau)
       

       !! Compute the theta-weight
       theta_argument=t_shift/(2*Kvalue)&
            +real(T_list(2)-T_list(1),dp)/(6*Kvalue)
       coeff_weight=thetagen(theta_argument,0.d0,czero,tau*12*Kvalue)          
       !!if(fock_verbose)then
       if(.FALSE.)then
          write(*,*) 't,theta_arg:',t,theta_argument
          write(*,*) 't_shift:',t_shift
          write(*,*) 'coeff_weight:',coeff_weight
       end if
       
       Z_1_arg=T_list(1)-t
       Z_2_arg=T_list(2)+t
       
       !! Comptue the Z_factor weights
       coeff_Z_1=Z_factor(Z_1_arg,Kvalue,tau)
       coeff_Z_2=Z_factor(Z_2_arg,Kvalue,tau)
       
       
       tmp_coeff=coeff_phase*coeff_laughlin*coeff_weight*coeff_Z_1*coeff_Z_2
       
       !!if(fock_verbose)then
       if(.FALSE.)then
          write(*,*) ' ---args----'
          write(*,*) ' t:',t          
          write(*,*) ' Theta arg:',theta_argument
          write(*,*) ' Z_1_arg:',Z_1_arg
          write(*,*) ' Z_2_arg:',Z_2_arg
          write(*,*) ' ---values----'
          write(*,*) ' phase:     ',coeff_phase
          write(*,*) ' weight:    ',coeff_weight
          write(*,*) ' Z_laughlin:',coeff_laughlin
          write(*,*) ' Z_value1:  ',coeff_Z_1
          write(*,*) ' Z_value2:  ',coeff_Z_2
          write(*,*) ' Total_tmp_coeff:',tmp_coeff
          write(*,*) '-.-.-.-.-.-.-.-.'
       end if
       !!Add to the accumulated coefficient
       coeff=coeff+tmp_coeff
    end do
    !write(*,*) 'Total coeff:', coeff
    
    
  end function laughlin_reduced_coeff_ne_3
  

  complex(kind=dpc) function laughlin_reduced_coeff_ne_4(DT_list,Kvalue,tau) result(coeff)
    !!This is the special solution for Ne=4
    !!It assumes that the DT_list is properly balanced and has correct parity
    integer, parameter :: Ne=4
    integer, intent(in) :: DT_list(Ne),Kvalue
    complex(kind=dpc), intent(in) :: tau
    complex(kind=dpc) :: tmp_coeff
    
    integer :: T_shift_list(Ne)
    real(kind=dp) :: T_list(Ne)
    
    integer :: t1,t2

    !!Here the momentum has an offset of Kval(Ne-1)/2 which is not always an integer
    !! Thus we can not safetly divide everything by two
    T_list=real(DT_list,dp)/2
    T_shift_list=(DT_list-Kvalue)/2
    
    
    !!Initialize the coeffiecnt
    coeff=0.d0
    !!if(fock_verbose)then
    if(.FALSE.)then
       write(*,*) '.......LRCN4.........'
       write(*,*)  'Tlist:',T_list
       write(*,*)  'T_shiftlist:',T_shift_list
    end if
    !! WE sum over the variable t in Z_2*K + K/2
    do t1=1,3*Kvalue
       do t2=1,3*Kvalue
          tmp_coeff=term_laughlin_reduced_coeff_ne_4(t1,t2,DT_list,Kvalue,tau)
          !!Add to the accumulated coefficient
          coeff=coeff+tmp_coeff
       end do
    end do
    !write(*,*) 'Total coeff:', coeff
    
    
  end function laughlin_reduced_coeff_ne_4
  
  
  complex(kind=dpc) function term_laughlin_reduced_coeff_ne_4(t1,t2,DT_list,Kvalue,tau) result(tmp_coeff)
    integer, parameter :: Ne=4
    integer, intent(in) :: DT_list(Ne),Kvalue
    integer, intent(IN) :: t1,t2
    complex(kind=dpc), intent(in) :: tau
    integer :: ds_t_vector(Ne-1)

    integer :: T_shift_list(Ne),Z_1_arg,Z_2_arg,Z_3_arg,q2
    real(kind=dp) :: T_list(Ne),D1,D2

    complex(kind=dpc) :: coeff_phase,coeff_weight,coeff_Laughlin
    complex(kind=dpc) :: coeff_Z_1,coeff_Z_2,coeff_Z_3


    !! Thus we can not safetly divide everything by two
    T_list=real(DT_list,dp)/2
    T_shift_list=(DT_list-Kvalue)/2

    
    ds_t_vector(1)=2*t1
    ds_t_vector(2)=2*t2
    ds_t_vector(3)=-2*(t1+t2)
    !!if(.FALSE.)then
    if(fock_Verbose)then
       write(*,*) '......LRCN4..........'
       write(*,*) 't_small_indx: ',t1,t2
       write(*,*) 'DT_small_list:',ds_t_vector
    end if
    
    !!Compute the phase factor
    coeff_phase=exp(iunit*pi*(T_list(1)+T_list(2)+T_list(3)))
    !! Compute the recursive laughlin contribution
    coeff_laughlin=laughlin_reduced_coeff(ds_t_vector,Ne-1,Kvalue,tau)
    
    !! Compute the theta-weight
    D1=(real(t1-t2,dp)/3-(T_list(1)-T_list(2))/4)/Kvalue
    D2=(real(t1+t2,dp)-(T_list(1)+T_list(2)-2*T_list(3))/4)/Kvalue
    
    coeff_weight=czero
    do q2=1,2
       coeff_weight=coeff_weight&
            +thetagen(-real(q2,dp)/2-D1/2,0.d0,czero,tau*24*Kvalue)&
            *thetagen(real(q2,dp)/2-D2/6,0.d0,czero,tau*72*Kvalue)
    end do
    
    Z_1_arg=T_shift_list(1)-t1
    Z_2_arg=T_shift_list(2)-t2
    Z_3_arg=T_shift_list(3)+t1+t2
    
    !! Comptue the Z_factor weights
    coeff_Z_1=Z_factor(Z_1_arg,Kvalue,tau)
    coeff_Z_2=Z_factor(Z_2_arg,Kvalue,tau)
    coeff_Z_3=Z_factor(Z_3_arg,Kvalue,tau)
    
    
    tmp_coeff=coeff_phase*coeff_laughlin*coeff_weight&
         *coeff_Z_1*coeff_Z_2*coeff_Z_3
    
    if(fock_verbose)then
    !!if(.FALSE.)then
       write(*,*) '---args----'
       write(*,*) 't1,t2',t1,t2          
       write(*,*) 'D1:',D1
       write(*,*) 'D2:',D2
       write(*,*) 'Z_1_arg:',Z_1_arg
       write(*,*) 'Z_2_arg:',Z_2_arg
       write(*,*) 'Z_3_arg:',Z_3_arg
       write(*,*) '---values----'
       write(*,*) 'phase:     ',coeff_phase
       write(*,*) 'weight:    ',coeff_weight
       write(*,*) 'Z_laughlin:',coeff_laughlin
       write(*,*) 'Z_value1:  ',coeff_Z_1
       write(*,*) 'Z_value2:  ',coeff_Z_2
       write(*,*) 'Z_value3:  ',coeff_Z_3
       write(*,*) 'Total_tmp_coeff:',tmp_coeff
    end if
  end function term_laughlin_reduced_coeff_ne_4
  
  
  complex(kind=dpc) function laughlin_reduced_coeff_ne_gen(DT_large_list,Ne,Kvalue,tau) result(coeff)
    !!This is the gneric solution for genral Ne
    !!It assumes that the DT_large_list is properly balanced and has correct parity
    integer, intent(in) :: DT_large_list(Ne),Kvalue,Ne
    complex(kind=dpc), intent(in) :: tau
    complex(kind=dpc) :: tmp_coeff,tmp_coeff2
    real(kind=dp) :: T_large_real(Ne)
    integer :: t_small_indx(Ne-2)
    
    !!Here the momentum has an offset of Kval(Ne-1)/2 which is not always an integer
    !! Thus we can not safetly divide everything by two
    T_large_real=real(DT_large_list,dp)/2
    
    
    !!Initialize the coeffiecnt
    coeff=0.d0
    !!if(fock_verbose)then
    if((Ne.eq.4).and.fock_verbose)then
       write(*,*) '.......LRCG.........'
       write(*,*)  'T_large_list:',T_large_real
    end if
    !! WE sum over the variable t in Z_2*K + K/2
    t_small_indx=1
    t_small_indx(Ne-2)=0
    do
       call get_next_state(t_small_indx,Ne-2,Kvalue)
       if((Ne.eq.4).and.fock_verbose)then
          write(*,*) '......LRCG..........'
          write(*,*)  't_small_indx:',t_small_indx
       end if
       tmp_coeff=term_laughlin_reduced_coeff_ne_gen(t_small_indx&
            ,DT_large_list,Ne,Kvalue,tau)
       if(Ne.eq.4)then
          tmp_coeff2=term_laughlin_reduced_coeff_ne_4(&
               t_small_indx(1),t_small_indx(2),&
               DT_large_list,Kvalue,tau)
          
          if(abs(tmp_coeff/tmp_coeff2-1.d0).gt.1.d-10)then
             write(*,*) 'ERROR: Different - recomputing in verbose mode'
             fock_verbose=.true.
             write(*,*) '----------------------------------'
             write(*,*) '            GEN                   '
             write(*,*) '----------------------------------'
             tmp_coeff=term_laughlin_reduced_coeff_ne_gen(&
                  t_small_indx,DT_large_list,Ne,Kvalue,tau)
             write(*,*) '----------------------------------'
             write(*,*) '            Ne = 4                '
             write(*,*) '----------------------------------'
             tmp_coeff2=term_laughlin_reduced_coeff_ne_4(&
                  t_small_indx(1),t_small_indx(2),&
                  DT_large_list,Kvalue,tau)
             write(*,*) '----------------------------------'
             write(*,*) '----------------------------------'
             write(*,*) 'Coeffs different for Ne,Kvalue',Ne,Kvalue
             write(*,*) 't_small_indx:',t_small_indx
             write(*,*) 'coeff_4: ', tmp_coeff2
             write(*,*) 'coeff_gen:', tmp_coeff
             write(*,*) 'quotient-1:', tmp_coeff/tmp_coeff2
             write(*,*) 'DT_large_list:',DT_large_list
             call exit(-2)
          end if
       end if
       
       !!Add to the accumulated coefficient
       coeff=coeff+tmp_coeff
       
       if(is_last_t_value(t_small_indx,Ne-2,Kvalue))then
          exit
       end if
       
    end do
    !write(*,*) 'Total coeff:', coeff
    
  end function laughlin_reduced_coeff_ne_gen


  complex(kind=dpc) function term_laughlin_reduced_coeff_ne_gen(t_small_indx,DT_large_list,Ne,Kvalue,tau) result(tmp_coeff)
    integer, intent(in) :: DT_large_list(Ne),Kvalue,Ne
    complex(kind=dpc), intent(in) :: tau
    integer,intent(IN) :: t_small_indx(Ne-2)
    integer :: DT_small_list(Ne-1)
    complex(kind=dpc) :: coeff_phase,coeff_weight,coeff_Laughlin
    complex(kind=dpc) :: coeff_Z_list
    integer :: Z_arg_list(Ne-1)
    real(kind=dp) :: T_large_real(Ne),D_list(Ne-2),T_small_real(Ne-1)
    
    !!Here the momentum has an offset of Kval(Ne-1)/2 which is not always an integer
    !! Thus we can not safetly divide everything by two
    T_large_real=real(DT_large_list,dp)/2
    
    call construct_balanced_DT_small_list(DT_small_list,t_small_indx,Ne,Kvalue)
    
    if((Ne.eq.4).and.fock_verbose)then
    !!if(Ne.eq.4)then
       write(*,*) '.......TLRCG.........'
       write(*,*) 't_small_indx: ',t_small_indx
       write(*,*) 'DT_small_list:',DT_small_list
    end if
    
    T_small_real=real(DT_small_list,dp)/2
    !!Compute the phase factor
    coeff_phase=exp(-iunit*pi*T_large_real(Ne))
    !! Compute the recursive laughlin contribution
    coeff_laughlin=laughlin_reduced_coeff(DT_small_list,Ne-1,Kvalue,tau)
    
    !! Compute the theta-weight
    call construct_D_list(D_list,T_large_real,t_small_real,Ne,Kvalue)
    coeff_weight=compute_weight_factors(D_list,Ne,Kvalue,tau)
    
    !!Get the Z_function_arguments
    Z_arg_list=(DT_large_list(1:Ne-1)-DT_small_list-Kvalue)/2
    !! Comptue the Z_factor weights
    coeff_Z_list=compute_coeff_Z_factor(Z_arg_list,Ne-1,Kvalue,tau)
    
    !!Multiply all the components together
    tmp_coeff=coeff_phase*coeff_laughlin*coeff_weight*coeff_Z_list
    
    !if(fock_verbose)then
    !!if(.TRUE.)then
    !if(Ne.eq.4)then
    !if(.FAlse.)then
    if((Ne.eq.4).and.fock_verbose)then
       write(*,*) ' ---args----'
       write(*,*) ' t_list',t_small_indx
       write(*,*) ' D_list:',D_list
       write(*,*) ' Z_arg_list:',Z_arg_list
       write(*,*) ' ---values----'
       write(*,*) ' phase:     ',coeff_phase
       write(*,*) ' weight:    ',coeff_weight
       write(*,*) ' Z_laughlin:',coeff_laughlin
       write(*,*) ' Z_value_list:  ',coeff_Z_list
       write(*,*) ' Total_tmp_coeff:',tmp_coeff
       write(*,*) '-.-.-.-.-.-.-.-.'
    end if
  end function term_laughlin_reduced_coeff_ne_gen
  
  
  subroutine get_next_state(indx_list,N,Kvalue)
    integer, intent(INOUT) :: indx_list(N)
    integer, intent(IN) :: N,Kvalue
    integer :: indx, m
    logical :: unmodified
    
    !write(*,*) 'indx_list_in:', indx_list
    unmodified=.TRUE.
    do indx=0,N-1
       m=N-indx
       if(indx_list(m).lt.(Kvalue*(N+1)))then
          indx_list(m)=indx_list(m)+1
          indx_list((m+1):N)=1
          unmodified=.FALSE.
          exit
       end if
    end do
    if(unmodified)then
       !!If we reach where the list of integers is q_n=n.
       !!In that case there is no next state...
       write(*,*) 'ERROR: overshooting indx_list'
       write(*,*) 'indx_list:', indx_list
       call exit(-2)
    end if
    
  end subroutine get_next_state
  
  
  complex(kind=dpc) function compute_coeff_Z_factor(Z_arg_list,N,Kvalue,tau) result(weight)
    integer, intent(IN) :: Z_arg_list(N) , N,Kvalue
    complex(kind=dpc), intent(IN) :: tau
    integer :: j
    weight=1.d0
    do j=1,N
       weight=weight*Z_factor(Z_arg_list(j),Kvalue,tau)
    end do
  end function compute_coeff_Z_factor

  subroutine construct_balanced_DT_small_list(DT_small_list,t_list,Ne,Kvalue)
    integer, intent(IN) :: t_list(Ne-2),Ne,Kvalue
    integer, intent(OUT) :: DT_small_list(Ne-1)

    if(modulo(Ne,2).eq.0)then
       DT_small_list(1:(Ne-2))=2*t_list
    else
       DT_small_list(1:(Ne-2))=2*t_list+Kvalue*(Ne-2)
    end if
    DT_small_list(Ne-1)=-sum(DT_small_list(1:(Ne-2)))
  end subroutine construct_balanced_DT_small_list

  real(kind=dp) function compute_weight_factors(D_list,Ne,Kvalue,tau) result(coeff_weight)
    complex(kind=dpc),intent(in) :: tau
    real(kind=dp), intent(in) :: D_list(Ne-2)
    integer, intent(in) :: Ne,Kvalue
    integer :: q_list(Ne-1),n
    real(kind=dp) :: tmp_weight
    

    coeff_weight=0.d0
    !!initializing the q_list
    q_list(1:Ne-3)=1
    q_list(Ne-2:Ne-1)=0
    do !!Loop through the q_values
       call get_next_q_value(q_list(1:Ne-2),Ne-2)
       !write(*,*) 'q_list:',q_list
       tmp_weight=1.d0
       do n=1,Ne-2 !!Multiply all the theta_functions together
          !!This thetagen should return a real number!!
          tmp_weight=tmp_weight*real(thetagen(&
               real(q_list(n),dp)/n&
               -real(q_list(n+1),dp)/(n+1)&
               -D_list(n)/(n*(n+1)),&
               0.d0,czero,tau*(Ne-1)*Ne*(n+1)*n*Kvalue),dp)   
       end do
       !! Att the contributions together
       coeff_weight=coeff_weight+tmp_weight
       !! !! Get the next q_value
       
       if(is_last_q_value(q_list(1:Ne-2),Ne-2))then
          exit
       end if
    end do

  end function compute_weight_factors
    
  logical function is_last_q_value(q_list,N) result(last_value)
    integer, intent(in) :: q_list(N) ,N
    integer :: indx
    last_value=.true.
    do indx=1,N
       if(q_list(indx).lt.indx)then
          last_value=.false.
          exit
       end if
    end do
  end function is_last_q_value

  logical function is_last_T_value(t_list,N,Kvalue) result(last_value)
    integer, intent(in) :: t_list(N) ,N,Kvalue
    integer :: indx
    last_value=.true.
    do indx=1,N
       if(t_list(indx).lt.(Kvalue*(N+1)))then
          last_value=.false.
          exit
       end if
    end do
  end function is_last_T_value
  

  
  subroutine get_next_q_value(q_list,N)
    integer, intent(in) :: N
    integer, intent(inout) :: q_list(N)
    integer :: indx,m
    logical :: unmodified

    unmodified=.TRUE.    
    if(N.ne.1)then
       do indx=0,N-2
          m=N-indx
          if(q_list(m).lt.(m))then
             q_list(m)=q_list(m)+1
             q_list((m+1):N)=1
             unmodified=.FALSE.
             exit
          end if
       end do
    else
       q_list(1)=q_list(1)+1
       unmodified=.FALSE.
    end if
    if(unmodified)then
       !!If we reach where the list of integers is q_n=n.
       !!In that case there is no next state...
       write(*,*) 'ERROR: overshooting q_list'
       write(*,*) 'q_list:', q_list
       call exit(-2)
    end if
  end subroutine get_next_q_value
    
          


  subroutine construct_D_list(D_list,T_large_real,t_small_real,Ne,Kvalue)
    real(kind=dp), intent(IN) :: T_large_real(Ne),t_small_real(Ne-1)
    integer, intent(IN) :: Ne,Kvalue
    real(kind=dp), intent(OUT) :: D_list(Ne-2)
    integer :: n_indx,N,j
    N=Ne-2
    
    do n_indx=1,N-1 !!Dompute the D_n with n<Ne-2
       D_list(n_indx)=0.d0
       do j=1,N !!Sum up all term 1<=j<=N (some are zero)
          D_list(n_indx)=D_list(n_indx)&
               +(v_j_n_coeff(j,n_indx)*t_small_real(j))/(Ne-1)
       end do
       do j=1,N+1 !!Sum up all terms 1<=j<=N+1 (some are zero)
          D_list(n_indx)=D_list(n_indx)&
               -(v_j_n_coeff(j,n_indx)*t_large_real(j))/Ne
       end do
    end do
    D_list(N)=0.d0
    do j=1,N !!Sum up all term 1<=j<=Ne-2
       D_list(N)=D_list(N)&
            +t_small_real(j)&
            -t_large_real(j)/Ne
    end do
    D_list(N)=D_list(N)&
         +N*t_large_real(N+1)/Ne
    
    !!FInally divide everything by Kvalue
    D_list=D_list/Kvalue

    !if(Ne.eq.4)then
    !   D_list(1)=(real(t_small_real(1)-t_small_real(2),dp)/3&
    !        -(T_large_real(1)-T_large_real(2))/4)/Kvalue
    !end if

    if((Ne.eq.4).and.fock_verbose)then
       write(*,*) ',,,,,,,,,CDL,,,,,,,,,,,,'
       write(*,*) 'T large real:',T_large_real
       write(*,*) 'T small real:',t_small_real
    end if

    
  end subroutine construct_D_list

  integer function v_j_n_coeff(j,n) result(coeff)
    integer, intent(IN) :: j,n
    
    if(j.le.0)then
       coeff=0
    elseif(j.le.n)then
       coeff=1
    elseif(j.eq.(n+1))then
       coeff=-n
    else !! j > n+1
       coeff=0
    end if
  end function v_j_n_coeff

end module fock_coefficients
