program cft_wfn
  
  use chiral_wfn
  implicit none

  call set_input()
  !call print_status()
  call retreive_input()
  !call print_status()
  call check_kmatrix_consistency()
  !call print_status()
  call compute_kmatrix_stat()
  !call print_status()
  call init_wfn()
  !call print_status()
  call compute_wave_function()

  
end program cft_wfn
