#!/bin/bash

set -e
set -u

test_dir=$1 ##First input is the test dir
src_dir=$2 ##Second input is the src dir
build_dir=$3 ##Third input is the build dir

for f90_test in $( ls ${src_dir}/test_suite_*.f90) ; do
    o_test=$(echo $f90_test | sed 's|\.f90$|.o|' | sed "s|${src_dir}/|${build_dir}/|")
    prog_test=$(echo $f90_test |  sed 's|\.f90$||' | sed "s|${src_dir}/|${test_dir}/|")
    target_test=$(echo $prog_test |  sed "s|${test_dir}/|${test_dir}/\.|")
    #echo 
    #echo ...........................
    #echo $f90_test
    #echo $o_test
    #echo $prog_test
    #echo $target_test

    ./mk.build.f90.sh $prog_test $o_test '$(OMPFLAG)'
    echo
    echo $target_test:$prog_test
    echo -e "\t-@echo ---------------------"
    echo -e "\t-@echo \"Testing $prog_test\""
    echo -e "\t-@echo ---------------------"
    echo -e "\t./$prog_test"
    echo -e "\t-@echo ---------------------"
    echo -e "\t-@echo \" Test suscessfull\""
    echo -e "\t-@echo ---------------------"
    echo -e "\t-@echo test_succesfull > $target_test"
    echo 

done
