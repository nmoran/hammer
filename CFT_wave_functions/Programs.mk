###Names of the main programs
PROG_ED   =  ed_torus
PROG_KMAT =  K_matrix_stats
PROG_CHIRAL =  cft_wfn
PROG_LAUGH =  laughlin_coeffs
PROG_SECTOR =  raw_k_sector
Prog_Coord = Run_CFT_Wfn
PROGS := $(PROG_ED) $(PROG_KMAT) $(PROG_TEST) $(PROG_CHIRAL) $(PROG_LAUGH) $(PROG_SECTOR) $(Prog_Coord) 

### NOTA BENE ####
###We keep all filesnames in lower case
###(as fortran is insensitive to file cases)

DIR_SRC_F90 := src_f90
DIR_BLD_F90 := build_f90
DIR_SRC_CXX := src_cpp
DIR_BLD_CXX := build_cpp

## Find all *.f90 files and create an o-file for each source-file
SRCS_F90 := $(wildcard $(DIR_SRC_F90)/*.f90) 	
OBJS_F90 :=	$(patsubst $(DIR_SRC_F90)/%.f90, $(DIR_BLD_F90)/%.o, $(SRCS_F90))
SRCS_C := $(wildcard *.cpp *.h) 	


.PHONY : all #all is not the name of a file, but a rule
all: notest test 

.PHONY : notest #notest is not the name of a file, but a rule
notest: graph $(PROGS) $(OBJS_F90)

##FIXME: this should be made more clever (search for relevant modules immediately), but will have to do for now!
$(Prog_Coord): build_f90/chiral_wfn.o build_f90/chiral_wfn.mod
