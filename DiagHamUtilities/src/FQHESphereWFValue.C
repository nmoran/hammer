#include "Architecture/ArchitectureManager.h"
#include "Architecture/AbstractArchitecture.h"
#include "Vector/RealVector.h"
#include "HilbertSpace/FermionOnSphere.h"
#include "FunctionBasis/AbstractFunctionBasis.h"
#include "FunctionBasis/ParticleOnSphereFunctionBasis.h"
#include "MathTools/Complex.h"
#include "Architecture/ArchitectureOperation/QHEParticleWaveFunctionOperation.h"
#include "Options/Options.h"
#include "Tools/FQHEFiles/QHEOnSphereFileTools.h"


#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <stdio.h>
#include <cstring>


using std::ios;
using std::cout;
using std::endl;
using std::ofstream;

int main(int argc, char** argv)
{
  // some running options and help
  OptionManager Manager ("FQHESphereWFValue" , "0.01");
  //OptionGroup* MiscGroup = new OptionGroup ("misc options");
  OptionGroup* SystemGroup = new OptionGroup ("system options");
  //OptionGroup* MonteCarloGroup = new OptionGroup ("Monte Carlo options");
  Manager += SystemGroup;

  ArchitectureManager Architecture;
  Architecture.AddOptionGroup(&Manager);

  (*SystemGroup) += new SingleStringOption  ('\0', "input-file", "input state file name");

  if (Manager.ProceedOptions(argv, argc, cout) == false)
    {
      cout << "see man page for option syntax or type FQHESphereWFValue -h" << endl;
      return -1;
    }

  int NbrParticles = 0, LzMax = 0, TotalLz = 0;
  bool Statistics;

  cout << "Finding system info from name of vector file: " << ((SingleStringOption*) Manager["input-file"])->GetString() << endl;

  if (FQHEOnSphereFindSystemInfoFromVectorFileName(((SingleStringOption*) Manager["input-file"])->GetString(),
						  NbrParticles, LzMax, TotalLz, Statistics) == false)
    {
      cout << "error while retrieving system parameters from file name " << ((SingleStringOption*) Manager["input-file"])->GetString() << endl;
      return -1;
    }

  cout << endl << "System details" << endl << "-------------------------" << endl;
  cout << "Particles: " << NbrParticles << endl;
  cout << "LzMax: " << LzMax << endl;
  cout << "TotalLz: " << TotalLz << endl;

  cout << endl << "Build basis for system" << endl << "-------------------------" << endl;
  FermionOnSphere *ExactSpace = NULL;
  ExactSpace = new FermionOnSphere(NbrParticles, TotalLz, LzMax);
  cout << "Basis build with dimension: " << ExactSpace->GetHilbertSpaceDimension() << endl;

  cout << endl << "Read fock coefficients from vector" << endl << "-------------------------" << endl;
  RealVector ExactState;
  if (ExactState.ReadVector (((SingleStringOption*) Manager["input-file"])->GetString()) == false)
    {
      cout << "can't open vector file " << ((SingleStringOption*) Manager["input-file"])->GetString() << endl;
      return -1;
    }
  cout << "Vector dimension: " << ExactState.GetVectorDimension() << endl;
  cout << "Vector norm: " << ExactState.Norm() << endl;

  // Specify position to evaluate the wave-function
  int NbrPositions = 1;
  double *Positions;
  Positions = new double[2 * NbrParticles * NbrPositions];
  srand((unsigned)time(0));
  for (int i = 0; i < NbrParticles * NbrPositions; i++)
    {
        Positions[i * 2] = ((double)rand()/(double)RAND_MAX) * M_PI;           // this is the theta (polar) coordinate, takes values from 0 to pi
        Positions[i * 2 + 1] = ((double)rand()/(double)RAND_MAX) * 2.0 * M_PI; // this is the azimuthal coordinate, takes values from 0 to 2 pi
    }

  AbstractFunctionBasis *ExactBasis = NULL;
  ExactBasis = new ParticleOnSphereFunctionBasis(LzMax);
  RealVector TmpPositions = RealVector(Positions, 2 * NbrParticles * NbrPositions);

  // We look at the value of single particle wave-functions
  Complex Tmp;
  double *test_positions = new double[2];
  test_positions[0] = 2.0; test_positions[1] = 1.0;
  RealVector TestPositions = RealVector(test_positions, 2);
  cout << "We look at single particle WF values for first position: " << TestPositions[0] << ", " << TestPositions[1] << endl;
  for (int i = 0 ; i <= LzMax; i++)
    {
      ExactBasis->GetFunctionValue(TestPositions, Tmp, i);
      cout << i << ": " << Tmp.Re << ", " << Tmp.Im << "  (" << Norm(Tmp) << ")" << endl;
    }

  // Set positions of two of the particles equal to each other to test if wave-function dissappears when this happens.
  //Positions[0] = Positions[2];
  //Positions[1] = Positions[3];


  cout << endl << "Created vector with dimension: " << TmpPositions.GetVectorDimension() << endl;
  cout << "Contents: " ;
  for(int i = 0; i < TmpPositions.GetVectorDimension(); i++)
    {
      cout << TmpPositions[i] << "   ";
    }
  cout << endl;


  // buffer to store results
  Complex *ValuesExact;
  ValuesExact = new Complex[NbrPositions];

  QHEParticleWaveFunctionOperation Operation(ExactSpace, &ExactState, &TmpPositions, ExactBasis);
  cout << endl << "Created operation to evaluate the wave-function." << endl;
  Operation.ApplyOperation(Architecture.GetArchitecture());

  cout << endl << "Applied operation to evaluate the wave-function. Values are:" << endl;

  for (int i = 0; i < NbrPositions; i++)
    {
      ValuesExact[i] = Operation.GetScalar(i);
      cout << i << ": " << ValuesExact[i] << endl;
    }

  delete []ValuesExact;

  return 0;
}
