#include "Architecture/ArchitectureManager.h"
#include "Architecture/AbstractArchitecture.h"
#include "Vector/RealVector.h"
#include "Matrix/RealMatrix.h"
#include "Matrix/RealAntisymmetricMatrix.h"
#include "HilbertSpace/AbstractQHEParticle.h"
#include "HilbertSpace/ParticleOnSphere.h"
#include "HilbertSpace/FermionOnSphere.h"
#include "HilbertSpace/BosonOnSphereShort.h"
#include "FunctionBasis/AbstractFunctionBasis.h"
#include "FunctionBasis/ParticleOnSphereFunctionBasis.h"
#include "MathTools/Complex.h"
#include "Architecture/ArchitectureOperation/QHEParticleWaveFunctionOperation.h"
#include "Options/Options.h"
#include "Tools/FQHEFiles/QHEOnSphereFileTools.h"


#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <stdio.h>
#include <cstring>

double twopi = 2.0*M_PI;


inline void rotate(double *tp, double *a)
{
    double x, y, z;

    RealVector r = RealVector(3);
    RealVector tmp = RealVector(3);
    // convert to cartesian coordinates
    r[0] = cos(tp[1])*sin(tp[0]);
    r[1] = sin(tp[1])*sin(tp[0]);
    r[2] = cos(tp[0]);

    //cout << "Coord: " << r << endl;

    RealMatrix XRot = RealMatrix(3,3,true);
    RealMatrix YRot = RealMatrix(3,3,true);
    RealMatrix ZRot = RealMatrix(3,3,true);

    XRot.SetMatrixElement(0,0,1.0); XRot.SetMatrixElement(1,1,cos(a[0]));XRot.SetMatrixElement(1,2,-sin(a[0]));
    XRot.SetMatrixElement(2,1,sin(a[0]));XRot.SetMatrixElement(2,2,cos(a[0]));

    YRot.SetMatrixElement(0,0,cos(a[1])); YRot.SetMatrixElement(0,2,sin(a[1]));YRot.SetMatrixElement(1,1,1);
    YRot.SetMatrixElement(2,0,-sin(a[1]));YRot.SetMatrixElement(2,2,cos(a[1]));

    ZRot.SetMatrixElement(0,0,cos(a[2])); ZRot.SetMatrixElement(0,1,-sin(a[2]));ZRot.SetMatrixElement(1,0,sin(a[2]));
    ZRot.SetMatrixElement(1,1,cos(a[2]));ZRot.SetMatrixElement(2,2,1);

    /*cout << "XRot: " << XRot << endl;
    cout << "YRot: " << YRot << endl;
    cout << "ZRot: " << ZRot << endl;*/

    tmp.Multiply(YRot, r);
    //cout << "Rotated: " << tmp << endl;
    r.Copy(tmp);
    tmp.Multiply(XRot, r);
    r.Copy(tmp);
    //cout << "Rotated: " << r << endl;
    tmp.Multiply(ZRot, r);
    r.Copy(tmp);
    //cout << "Rotated: " << r << endl;


    tp[0] = acos(r[2]);
    tp[1] = atan2(r[1],r[0]);
    if (tp[1] < 0) tp[1] += twopi;
}

inline void rotatey(int N, double *tp) {
	double x, y, z, x2, z2, sindelta=sqrt(3.0)/2.0;
	for(int i=0;i<2*N;i+=2) {
		x=cos(tp[i+1])*sin(tp[i]);
		y=sin(tp[i+1])*sin(tp[i]);
		z=cos(tp[i]);
		x2=x*0.5+z*sindelta;
		z2=-x*sindelta+z*0.5;
		tp[i]=acos(z2);
		tp[i+1]=atan2(y, x2);
		if(tp[i+1]<0) tp[i+1]+=twopi;
	}
}
inline void rotatex(int N, double *tp) {
	double x, y, z, y2, z2, sindelta=sqrt(3.0)/2.0;
	for(int i=0;i<2*N;i+=2) {
		x=cos(tp[i+1])*sin(tp[i]);
		y=sin(tp[i+1])*sin(tp[i]);
		z=cos(tp[i]);
		y2=y*0.5-z*sindelta;
		z2=y*sindelta+z*0.5;
		tp[i]=acos(z2);
		tp[i+1]=atan2(y2, x);
		if(tp[i+1]<0) tp[i+1]+=twopi;
	}
}


using std::ios;
using std::cout;
using std::endl;
using std::ofstream;

int main(int argc, char** argv)
{
  // some running options and help
  OptionManager Manager ("FQHESphereSUKToU1MCOverlap" , "0.01");
  //OptionGroup* MiscGroup = new OptionGroup ("misc options");
  OptionGroup* SystemGroup = new OptionGroup ("system options");
  //OptionGroup* MonteCarloGroup = new OptionGroup ("Monte Carlo options");
  Manager += SystemGroup;

  ArchitectureManager Architecture;
  Architecture.AddOptionGroup(&Manager);

  (*SystemGroup) += new SingleStringOption  ('\0', "input-file", "input state file name");

  if (Manager.ProceedOptions(argv, argc, cout) == false)
    {
      cout << "see man page for option syntax or type FQHESphereWFValue -h" << endl;
      return -1;
    }

  int NbrParticles = 0, LzMax = 0, TotalLz = 0;
  bool Statistics = true;

  cout << "Finding system info from name of vector file: " << ((SingleStringOption*) Manager["input-file"])->GetString() << endl;

  if (FQHEOnSphereFindSystemInfoFromVectorFileName(((SingleStringOption*) Manager["input-file"])->GetString(),
						  NbrParticles, LzMax, TotalLz, Statistics) == false)
    {
      cout << "error while retrieving system parameters from file name " << ((SingleStringOption*) Manager["input-file"])->GetString() << endl;
      return -1;
    }

  cout << endl << "System details" << endl << "-------------------------" << endl;
  cout << "Particles: " << NbrParticles << endl;
  cout << "LzMax: " << LzMax << endl;
  cout << "TotalLz: " << TotalLz << endl;
  cout << "Statistics: " << Statistics << endl;

  cout << endl << "Build basis for system" << endl << "-------------------------" << endl;
  ParticleOnSphere *ExactSpace = NULL;

  //cout << "Boson position: " <<  strstr(((SingleStringOption*) Manager["input-file"])->GetAsAString(), "boson") << endl;
  //cout << "Fermion position: " <<  strstr(((SingleStringOption*) Manager["input-file"])->GetAsAString(), "fermion") << endl;


  //if (Statistics == AbstractQHEParticle::FermionicStatistic)
  if (Statistics)
    {
      ExactSpace = new FermionOnSphere(NbrParticles, TotalLz, LzMax);
    }
  else
    {
      ExactSpace = new BosonOnSphereShort(NbrParticles, TotalLz, LzMax);
    }
  cout << "Basis build with dimension: " << ExactSpace->GetHilbertSpaceDimension() << endl;

  cout << endl << "Read fock coefficients from vector" << endl << "-------------------------" << endl;
  RealVector ExactState;
  if (ExactState.ReadVector (((SingleStringOption*) Manager["input-file"])->GetString()) == false)
    {
      cout << "can't open vector file " << ((SingleStringOption*) Manager["input-file"])->GetString() << endl;
      return -1;
    }
  cout << "Vector dimension: " << ExactState.GetVectorDimension() << endl;
  cout << "Vector norm: " << ExactState.Norm() << endl;

  if ( ExactState.GetVectorDimension() != ExactSpace->GetHilbertSpaceDimension() )
    {
        cout << "Vector and Hilbert space dimensions don't match: " << ExactState.GetVectorDimension() << " != " << ExactSpace->GetHilbertSpaceDimension() << endl;
        return 1;
    }

  // Specify position to evaluate the wave-function
  int NbrPositions = 5;
  double *Positions;
  Positions = new double[2 * NbrParticles * NbrPositions];
  srand((unsigned)time(0));
  for (int i = 0; i < NbrParticles * NbrPositions; i++)
    {
        Positions[i * 2] = ((double)rand()/(double)RAND_MAX) * M_PI;           // this is the theta (polar) coordinate, takes values from 0 to pi
        Positions[i * 2 + 1] = ((double)rand()/(double)RAND_MAX) * 2.0 * M_PI; // this is the azimuthal coordinate, takes values from 0 to 2 pi
    }

  // Set positions of two of the particles equal to each other to test if wave-function dissappears when this happens.
  //Positions[0] = Positions[2];
  //Positions[1] = Positions[3];


  cout << "Random coordinates: " ;
  for(int i = 0; i < (NbrParticles * NbrPositions); i++)
    {
      if ((i % NbrParticles) == 0) cout << endl;
      cout << "(" << Positions[i * 2] << ", " << Positions[i * 2 + 1] << ") ";
    }
  cout << endl;

  // buffer to store results
  Complex *ValuesExact;
  ValuesExact = new Complex[NbrPositions];


  cout << endl << "Creating operation to evaluate the wave-function." << endl;
  for ( int i = 0; i < NbrPositions; i++)
    {
      AbstractFunctionBasis *ExactBasis = NULL;
      RealVector *TmpPositions = new RealVector(&Positions[NbrParticles * 2 * i], 2 * NbrParticles);
      ExactBasis = new ParticleOnSphereFunctionBasis(LzMax);
      QHEParticleWaveFunctionOperation Operation(ExactSpace, &ExactState, TmpPositions, ExactBasis);
      Operation.ApplyOperation(Architecture.GetArchitecture());
      ValuesExact[i] = Operation.GetScalar(0);
      cout << i << ": " << ValuesExact[i] << "  (abs:"  << Norm(ValuesExact[i]) << ")" << endl;
    }


  cout << endl << "Apply random rotations around axes of:" << endl;

  double *Positions2;
  Positions2 = new double[2 * NbrParticles * NbrPositions];

  double amount[3];
  amount[0] = ((double)rand()/(double)RAND_MAX) * M_PI / 12.0;
  amount[1] = ((double)rand()/(double)RAND_MAX) * M_PI / 12.0;
  amount[2] = ((double)rand()/(double)RAND_MAX) * M_PI / 12.0;
  cout << "X: " << amount[0] << endl << "Y: " << amount[1] << endl << "Z: " << amount[2] << endl;

   for (int i = 0; i < NbrParticles * NbrPositions; i++)
    {
        Positions2[i * 2] = Positions[i * 2];
        Positions2[i * 2 + 1] = Positions[i * 2 + 1];
        rotate(&Positions2[i * 2], amount);
    }

  cout << "Rotated coordinates: " ;
  for(int i = 0; i < (NbrParticles * NbrPositions); i++)
    {
      if ((i % NbrParticles) == 0) cout << endl;
      cout << "(" << Positions2[i * 2] << ", " << Positions2[i * 2 + 1] << ") ";
    }
  cout << endl;

  Complex *ValuesExact2;
  ValuesExact2 = new Complex[NbrPositions];

  cout << endl << "Creating operation to evaluate the wave-function." << endl;
  for ( int i = 0; i < NbrPositions; i++)
    {
      AbstractFunctionBasis *ExactBasis = NULL;
      RealVector *TmpPositions = new RealVector(&Positions2[NbrParticles * 2 * i], 2 * NbrParticles);
      ExactBasis = new ParticleOnSphereFunctionBasis(LzMax);
      QHEParticleWaveFunctionOperation Operation(ExactSpace, &ExactState, TmpPositions, ExactBasis);
      Operation.ApplyOperation(Architecture.GetArchitecture());
      ValuesExact2[i] = Operation.GetScalar(0);
      cout << i << ": " << ValuesExact2[i] << "  (abs:"  << Norm(ValuesExact2[i]) << ")" << endl;
    }
  cout << endl;


  for ( int i = 0; i < NbrPositions; i++)
    {
      cout << "Phases: " << Arg(ValuesExact[i]) << ", " << Arg(ValuesExact2[i]) << ", Relative phases: " << Arg(ValuesExact2[i]) - Arg(ValuesExact[i]) << endl;
    }

  delete []ValuesExact;
  delete []ValuesExact2;

  return 0;
}
