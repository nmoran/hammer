CMAKE_MINIMUM_REQUIRED (VERSION 2.6)

PROJECT (DiaghamUtils)

SET (DU_VERSION_MAJOR 0)
SET (DU_VERSION_MINOR 1)

SET (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake-modules")

SET (CMAKE_VERBOSE_MAKEFILE "ON")

# Do not modify these varaibles, but set the environment variables DIAGHAM_SRC and DIAGHAM_BUILD
# appropriately.
set (DIAGHAM_SRC  $ENV{DIAGHAM_SRC})
set (DIAGHAM_BUILD $ENV{DIAGHAM_BUILD})

# checks if these folders exist
if (NOT IS_DIRECTORY ${DIAGHAM_SRC})
    MESSAGE(FATAL_ERROR "Diagham source folder does not exist ${DIAGHAM_SRC}. Please set to a valid folder.")
endif (NOT IS_DIRECTORY ${DIAGHAM_SRC})

if (NOT IS_DIRECTORY ${DIAGHAM_BUILD})
    MESSAGE(FATAL_ERROR "Diagham source folder does not exist ${DIAGHAM_BUILD}. Please set to a valid folder.")
endif (NOT IS_DIRECTORY ${DIAGHAM_BUILD})

# option to sepcify that intel compilers are to be used. Default is off. Turn on with -DUSE_INTEL=ON to
# cmake (before path) on command line.
OPTION(USE_INTEL OFF)


set (DIAGHAM_INCLUDE ${DIAGHAM_SRC}/src ${DIAGHAM_SRC}/FQHE/src ${DIAGHAM_BUILD}/src)

# we assume the mpi headers are in the default path /usr/include if not using intel compilers.
if(USE_INTEL)
    # if diagham is built with mpi, we must also give the mpi include path
    set (MPI_INCLUDE $ENV{INTEL_MPI_DIR}/intel64/include)
    list(APPEND DIAGHAM_INCLUDE ${MPI_INCLUDE})
    set (MPI_LIBRARY -L$ENV{INTEL_MPI_DIR}/intel64/lib -lmpi -lmpicxx) # for intel compiler no underscore
    set (CMAKE_CC_COMPILER mpicc)
    set (CMAKE_CXX_COMPILER  mpicxx)
    set (CCFLAGS -O3 -xHost)
    set (CXXFLAGS -O3 -xHost)
else(USE_INTEL)
    set (MPI_INCLUDE /usr/include/mpi)
    list(APPEND DIAGHAM_INCLUDE ${MPI_INCLUDE})
    set (MPI_LIBRARY -lmpi -lmpi_cxx)
endif(USE_INTEL)

set (DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Hamiltonian/libQHEHamiltonian.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/FunctionBasis/libQHEFunctionBasis.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/HilbertSpace/libQHEHilbertSpace.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Tools/FQHEMPS/libFQHEMPS.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Tools/FQHEFiles/libQHEFiles.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Tools/FQHESpectrum/libQHESpectrum.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Tools/FQHEMonteCarlo/libFQHEMonteCarlo.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Tools/FQHEWaveFunction/libQHEWaveFunction.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/MainTask/libQHEMainTask.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Operator/libQHEOperator.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/Architecture/ArchitectureOperation/libQHEArchitectureOperation.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/FQHE/src/QuantumNumber/libQHEQuantumNumber.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Options/libOptions.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/Base/src/BitmapTools/Color/libColor.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Hamiltonian/DMRGHamiltonian/libDMRGHamiltonian.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Hamiltonian/libHamiltonian.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/FunctionBasis/libFunctionBasis.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/HilbertSpace/ManyBodyHilbertSpace/libManyBodyHilbertSpace.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/HilbertSpace/libHilbertSpace.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/HilbertSpace/DMRGHilbertSpace/libDMRGHilbertSpace.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/MathTools/libMathTools.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/MathTools/RandomNumber/libRandomNumber.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/MathTools/NumericalAnalysis/libNumericalAnalysis.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Interaction/libInteraction.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Interaction/InternalInteraction/libInternalInteraction.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/MainTask/libMainTask.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/MCObservables/libMCObservables.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Tensor/libTensor.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Matrix/libArray.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Matrix/libMatrix.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Operator/libOperator.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/DMRGAlgorithm/libDMRGAlgorithm.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Polynomial/libPolynomial.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/LanczosAlgorithm/libLanczosAlgorithm.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Architecture/ClusterArchitecture/libClusterArchitecture.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Architecture/ArchitectureOperation/libArchitectureOperation.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Architecture/libArchitecture.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/QuantumNumber/libQuantumNumber.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/TensorProduct/libTensorProduct.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/MPSObjects/libMPSObjects.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Vector/libVector.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/Output/libOutput.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/GeneralTools/libGeneralTools.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/src/MathTools/libMathTools.a)
list(APPEND DIAGHAM_LIBRARIES ${DIAGHAM_BUILD}/Base/src/BitmapTools/BitmapPicture/libBitmapPicture.a)

if (USE_INTEL)
    set (BLAS_LAPACK_LIBRARY -L$ENV{MKLROOT}/lib/intel64 -lmkl_sequential -lmkl_intel_lp64 -lmkl_core -limf -lirc -lsvml)
else(USE_INTEL)
    set (BLAS_LAPACK_LIBRARY -lblas -llapack -lpthread)
endif(USE_INTEL)
#set (THREAD_LIBRARY -lpthread)

# uncomment for degugging.
#set (CMAKE_CXX_FLAGS "-g -O0 ")


if(DEFINED ENV{HDF5_DIR})
    SET (HDF5_DIR $ENV{HDF5_DIR})
    MESSAGE(STATUS "HDF5 DIR ${HDF5_DIR}")
    SET (HDF5_INCLUDE_DIRS ${HDF5_DIR}/include ${HDF5_DIR}/include/hdf5/serial)
    SET (HDF5_LIBRARIES -L${HDF5_DIR}/lib -L${HDF5_DIR}/lib64 -lhdf5 -lhdf5_cpp)
else(DEFINED ENV{HDF5_DIR})
    MESSAGE(STATUS "HDF5_DIR not set assuming default location.")
    SET (HDF5_INCLUDE_DIRS "") # assume these are in the standard path
    SET (HDF5_LIBRARIES -lhdf5 -lhdf5_cpp) # assume the same for these
endif(DEFINED ENV{HDF5_DIR})

INCLUDE_DIRECTORIES (${HDF5_INCLUDE_DIRS} ${DIAGHAM_INCLUDE})

MESSAGE(STATUS "Setting ${DIAGHAM_INCLUDE}")
MESSAGE(STATUS "Setting ${HDF5_INCLUDE_DIRS}")

ADD_SUBDIRECTORY (src)
