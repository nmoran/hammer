Hammer is a set of numerical tools for treating systems of strongly interacting
quantum many body systems and is aimed primarily at the study of Fractional
Quantum Hall (FQH) systems. These codes were developed within the group of Joost
Slingerland at the department of mathematical physics at Maynooth
University, Ireland. For more details and documentation consult the Hammer website 
at [http://www.thphys.nuim.ie/hammer/](http://www.thphys.nuim.ie/hammer/).

