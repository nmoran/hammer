Some scripts and utilities for diagonalising FQH Hamiltonians on toroidal geometries. Can run in parallel making use of PETSc and SLEPc.

Dependencies
-------------
- PETSc
- SLEPc
- petsc4py
- slepc4py
- mpi4py
- numpy
- scipy
- cython
- h5py


0. Install using pip
------------------------------
##First get the pip-package
sudo apt-get install python-pip python-dev build-essential 
sudo pip install --upgrade pip 
##Then install the hammer requirements
sudo pip install -r requirements.txt



1. Installation instructions
-----------------------------
If all the requirements are installed the pacakge Hammer can be built and installed using the following commands:

> python setup.py build

> python setup.py install

And only necessary if you would like to evaluate wave-functions in real space are to build the following.

> python setup_f2py.py build install

To use intel compiler instead for this enter

> python setup_f2py.py build --fcompiler=intelem install 


2. Installing requirements, PETSc, SLEPc, petsc4py and slepc4py
-------------------------------------------------
Installing PETSc and SLEPc
- Download PETSc from http://www.mcs.anl.gov/petsc/download/index.html
- Extract and set environment variables PETSC_DIR and PETSC_ARCH to the petsc directory and a string to identify the current petsc build. For example
 export PETSC_DIR=`pwd`
 export PETSC_ARCH=linux-gnu-cxx-complex
- Configure with
./configure --with-clanguage=C++ --with-cxx-support=1 --with-gcov=1 --with-scalar-type=complex --with-shared-libraries=1 --with-debugging=0 --with-scientific-python=1
If support for complex numbers is not required set the scalar type to real instead. If there are problems finding mpi, lapack or blas, make sure these are installed
and specify paths. See ./configure --help for further help and options. 
- Once the configure completes successfully. Start building with "make"

Download SLEPc from http://www.grycap.upv.es/slepc/
- Extract and run configure wth "./configure". This should find your PETSc build via PETSC_DIR and PETSC_ARCh.
- Set SLEPC_DIR to the slepc directory.
 export SLEPC_DIR=`pwd`

Download petsc4py from https://bitbucket.org/petsc/petsc4py/
- Extract and build using
 python setup.py build
You will need to have numpy installed and also the PETSc environment variables set.
- Once this has finished install using 
 python setup.py install
If you do not have permissions to install to the system directory you can create a local python 
environment with virtualenv (the --user install from petsc4py appears to give problems).
To do this install virtualenv: pip install --user virtualenv
Setup new python environment: virtualenv --system-site-packages ENV
Activate this: source ENV/bin/activate
Then build and install:
 python setup.py build
 python setup.py install

Download slepc4py from https://bitbucket.org/slepc/slepc4py/
- Extract and build using the same method as for petsc4py above.

Other python modules mpi4py, numpy, scipy and cython may already be installed or can be installed using pip with commands.
 pip install mpi4py
 pip install numpy 
 pip install scipy
 pip install cython


3. Usage
---------------------
After installing the modules should be in the python module path under 'hammer'. The Diagonalise.py script can then be used as an entry point. Calling this without any arguments will diagonalise a quadrilateral torus with 4 electrons, 12 flux quanta using the LLL Coulomb interaction with a total momentum of 0.

> Diagonalise.py

It is advisable to use other symmetries to reduce the computational effort required. The centre of mass momentum can be specified using the -c switch and the inversion sector with the --inversion-sector which takes values 0 and 1 corresponding to symmetric and antisymmetric sectors. The number of unique centre of mass symmetry sectors corresponds to number of fundamental unit cells. I.e. the quotient between the particle number and the nominator of the filling fraction (Ne/p=Ns/q)

Calling the script with '-h' will show help options

> Diagonalise.py -h

The first groups of options are for PETSc and SLEPc. The options specific to the torus diagonalisation are as follows:

optional arguments:
  -h, --help            show this help message and exit
  --particles PARTICLES, -p PARTICLES
  --nbrflux NBRFLUX, -l NBRFLUX
  --momentum MOMENTUM, -y MOMENTUM
  --landau-level LANDAU_LEVEL, -n LANDAU_LEVEL
  --tau1 TAU1, -t1 TAU1
  --tau2 TAU2, -t2 TAU2
  --magnetic-length MAGNETIC_LENGTH
  --eigenstate
  --nbr-states NBR_STATES
  --save-hamiltonian
  --write-coefficients
  --real
  --sparse
  --mpmath
  --coefficients-only
  --show-matrix
  --interaction INTERACTION

To use n processes call the script with mpiexec as 

> mpiexec -np n Diagonalise.py

