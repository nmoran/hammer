#!/bin/bash

p=10
k=0
lstart=14
lend=32
lstep=4

#for k in `seq 0 7` ; do 
for k in 0 ; do 

    filename="DrawPlot_p_${p}_k_${k}_E.plot"
    
    echo 'set datafile separator ","' > $filename
    #echo 'set terminal eps enhanced color' >> $filename
    echo 'set terminal pdfcairo' >> $filename
    echo "set output \"Plot_p_${p}_k_${k}_E.pdf\"" >> $filename
    echo "set title \"Thin Torus ${p} Particles at Momentum ${k}\"" >> $filename
    echo "set xlabel \"T\"" >> $filename
    echo "set ylabel \"E\"" >> $filename
    echo "set key outside" >> $filename
    echo -n 'plot ' >> $filename
    
    for l in `seq $lstart $lstep $lend` ; do
        if [ $l -gt $lstart ]; then
            echo ",\\" >> $filename
        fi
        echo -n "\"Torus_p_${p}_Ns_${l}_K_${k}_tau1_0.000_tau2_100.000_maglen_1.00__Thermal.csv\" using 1:2 with linespoints title \"Nphi=${l}\"" >> $filename 
    done
    
    gnuplot < $filename
    
#    filename="DrawPlot_p_${p}_k_${k}_ESqrd.plot"
#    
#    echo 'set datafile separator ","' > $filename
#    #echo 'set terminal eps enhanced color' >> $filename
#    echo 'set terminal pdfcairo' >> $filename
#    echo "set output \"Plot_p_${p}_k_${k}_ESqrd.pdf\"" >> $filename
#    echo "set title \"Thin Torus ${p} Particles at Momentum ${k}\"" >> $filename
#    echo "set xlabel \"T\"" >> $filename
#    echo "set ylabel \"E^2\"" >> $filename
#    echo -n 'plot ' >> $filename
#    
#    for l in `seq $lstart $lstep $lend` ; do
#        if [ $l -gt $lstart ]; then
#            echo ",\\" >> $filename
#        fi
#        echo -n "\"Torus_p_${p}_Ns_${l}_K_${k}_tau1_0.000_tau2_100.000_maglen_1.00__Thermal.csv\" using 1:3 with linespoints title \"Nphi=${l}\"" >> $filename 
#    done
#    
#    
#    gnuplot < $filename
done

