Creating ubuntu packages
---------------------------
Created launchpad login with GPG key pair.
Launchpad ppa: ppa:niall-moran/hammer

Making custom versions of existing ubuntu packages
---------------------------------------------------
1. Download source with apt-get source <package>

2. Install build dependencies with apt-get build-dep <package>

3. Change version from source folder with
dch -i 

4. Make changes and sign excluding source
debuild -S -sd

4ld-dep. Upload to launchpad
dput ppa:niall-moran/hammer <package>.changes

Build a new package from a distutils python package
-----------------------------------------------
1. Create source tar with
python setup.py sdist 

2. With source tar run
py2dsc -m 'Niall Moran <niall.moran@gmail.com>' package.tar.gz

3. Edit depends and other fields in debian/control

4. Edit changelog with 
dch -i 

5. Make changes and sign including source 
debuild -S -sa

6. Upload to launchpad
dput ppa:niall-moran/hammer <package>.changes


Specific details for hammer
-----------------------------------------------
1. Update from git. 
  git pull origin master
  
2. Update version number in setup.py 

3. Build source distribution
  python setup.py sdist
  
4. Attempt to make into package using py2dsc
py2dsc -m 'Niall Moran <niall.moran@gmail.com>' hammer-<version>.tar.gz

5. Modifications to get working.
  - Add pxd and header files manually.
    cp <hammer_src_dir>/hammer/*.pxd deb-dist/hammer-<version>/hammer
    cp <hammer_src_dir>/hammer/*.h deb-dist/hammer-<version>/hammer
    
  - Remove test parts in setup.py 
  change line
    cmdclass = {'build_ext': build_ext, 'test': test},
  to
    cmdclass = {'build_ext': build_ext},
  and comment lines:
      options = {
	  'test': {
	  'test_dir':['tests']}
      } 
   
   - Commit changes by running:
    dpkg-source --commit
   
6. Add dependencies to debian/control
- To BUild-Depends add: python-cython (>= 0.22), python-petsc4py (>= 3.4), python-slepc4py (>= 3.4), python-numpy (>= 1.0.1)
- To Depends add: , python-numpy (>= 1.0.1),
         python-petsc4py (>= 3.4), python-slepc4py (>= 3.4), libpetsc3.4.2 (>= 3.4.2),
         libslepc3.4.2 (>= 3.4.2), python-scipy (>= 0.13.3), python-sympy (>= 0.7.4), python-mpi4py (>= 1.3.1)
         
7. Enter command:
  dch -i
  to edit the changelog to add a line about changes in this release. Change unreleased to platform target. For ubuntu 14.04 use trusty etc..
  
8. Create source changes file and sign it using
  debuild -S -sa
  
9. Upload to launchpad using command (substituting correct version number):
 dput ppa:niall-moran/hammer hammer_1.0.1-hammer1_source.changes
