import jacobitheta as jt
import numpy as np

tau = np.complex(0.0, 1.0)

points = 10

values = np.zeros((points, points), dtype=np.complex128)

print jt.jacobitheta.logtheta1(np.complex(0.0,0.0), tau)

for i in range(points):
    for j in range(points):
        z = np.complex(j*np.pi/np.float(points), i*np.pi/np.float(points))
        values[i,j] = jt.jacobitheta.theta1(z, tau)

print values

