#! /usr/bin/env python

""" CreateSuperposition.py: Utility to create a superposition of states with the provided phases. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import sys
import argparse
import csv
import numpy as np
import h5py


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to create superposition of MC gernated wave function values with the given coefficients')
    Parser.add_argument('--output', '-o', type=str, default='output.hdf5', help='Output prefix to use.')
    Parser.add_argument('--verbose','-v', action='store_true', help='Flag to enable more verbose output.')
    Parser.add_argument('--coefficients', type=complex, nargs='*', help='Complex coefficients to use when building the superposition. One per state in format a+bj.')
    Parser.add_argument('--coefficients-file', type=str, default='', help='Filename containing coefficients, two comma separated columns for real and imaginary parts.')
    Parser.add_argument('--states', type=str, nargs='+', help='The states to use to build the superposition.')
    Parser.add_argument('--scale', type=str, help='The states to use to build the superposition.',choices={"log","lin"})
    Parser.add_argument('--normalise', action='store_true', help='Flag to indicate that the output vector should be normalised.')
    Parser.add_argument('--DataStruct', type=str, help='Specify the way the wave function is stored in the files. If the data contsin both a weight and a wave fucnction, the order needs to be specified. If WgtWfn or WfnWgt are specified, then the Wgt will not be altered but the Wfn will be. (Default = "Wfn")',choices={"Wfn","WfnWgt","WgtWfn"},default="Wfn")

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    Coefficients = Args.coefficients
    CoefficientsFile = Args.coefficients_file
    Output = Args.output
    States = Args.states
    Normalise = Args.normalise
    IsLog=Args.scale
    

    if Verbose: print("\nCreate superpositions")

    if States is None or len(States) == 0:
        print('At least one state must be specified.')
        return
    NbrStates = len(States)

    if Coefficients is None: # if no coefficients given, use equal superposition or read from file if given
        if CoefficientsFile == '':
            Coefficients = np.ones(NbrStates)/np.sqrt(NbrStates)
        else:
            print(('Reading coefficients from ' + CoefficientsFile))
            Coefficients = list()
            reader = csv.reader(open(CoefficientsFile,'r'))
            for row in reader:
                Coefficients.append(np.complex(np.float(row[0]), np.float(row[1])))

    elif len(Coefficients) != NbrStates:
        raise Exception, 'Number of coefficients must match the number of states.'

    if Normalise:
        if Verbose: print('Normalising...')
        Coefficients=Coefficients/np.sqrt(np.sum(np.abs(Coefficients)**2))
    
    if Verbose: print(('Combining states ' + str(States) + ' with coefficients ' + str(Coefficients) +'.'))


    if Args.DataStruct=="Wfn":
        RealCol=0
        ImagCol=1
    elif Args.DataStruct=="WfnWgt":
        RealCol=0
        ImagCol=1
    elif Args.DataStruct=="WgtWfn":
        RealCol=2
        ImagCol=3
    
    LogCoeffs=np.log(Coefficients)
    counter=0
    for StateFile in States:
        print "Open file ",StateFile," for read"
        with h5py.File(StateFile, 'r') as wf:
            wfval_in=np.array(wf.get('wf_values')[:,RealCol])+1j*np.array(wf.get('wf_values')[:,ImagCol])
            print "wfval_in.shape:",wfval_in.shape
        if counter==0:
            if Verbose: print(("Create the target vector"))
            wfval_out=np.zeros(wfval_in.shape,dtype=np.complex128)
        if not IsLog:
            if Verbose: print(("Add weighted vector to the target vector"))
            wfval_out+=wfval_in*Coefficients[counter]
        else:
            ###Adding in log scale is more difficuylt... to awoid zeros of overflow we first factor our the largest common piece
            Addition=wfval_in+LogCoeffs[counter]
            if counter==0: ###Set tot he first value
                if Verbose: print(("Create the target vector"))
                wfval_out=Addition
            else:
                ###Use that if e^y = e^A+e^B then y = A + log(1+e^(B-A)), if real(A)>real(B)
                ToF=np.real(Addition) > np.real(wfval_out)
                wfval_out=(ToF*(Addition+np.log(1.0+np.exp(wfval_out-Addition)))
                           +np.logical_not(ToF)*(wfval_out+np.log(1.0+np.exp(Addition-wfval_out))))
                if Verbose: print(("Add weighted vector to the target vector in log scale"))

        counter+=1
            
    if Verbose: print(('Creating output vector.'))
    if Args.DataStruct=="Wfn":
        VO=np.vstack((np.real(wfval_out),np.imag(wfval_out))).T
    elif Args.DataStruct=="WfnWgt":
        with h5py.File(StateFile, 'r') as wf:
            VO=np.vstack((np.real(wfval_out),
                          np.imag(wfval_out),
                          wf.get('wf_values')[:,2],
                          wf.get('wf_values')[:,3])).T
    elif Args.DataStruct=="WgtWfn":
        with h5py.File(StateFile, 'r') as wf:
            VO=np.vstack((wf.get('wf_values')[:,0],
                          wf.get('wf_values')[:,1],
                          np.real(wfval_out),
                          np.imag(wfval_out))).T
    print "VO.shape:",VO.shape
        
    if Verbose: print(('Writing vector to ' + Output + '.'))
    try:
        of = h5py.File(Output, 'w')
    except IOError as ioerror:
        print(("ERROR: Problem writing to file " + Output  + ': ' + str(ioerror)))
        sys.exit(-1)
    of.create_dataset("wf_values", data=VO)
    of.close()

    



if __name__ == '__main__' :
    __main__()
