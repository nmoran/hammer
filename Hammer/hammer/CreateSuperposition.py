#! /usr/bin/env python

""" CreateSuperposition.py: Utility to create a superposition of states with the provided phases. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

from petsc4py import PETSc
import sys
import argparse
import csv
import numpy as np
import hammer.vectorutils as vecutils
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to create superposition of states with the given coefficients')
    Parser.add_argument('--output', '-o', type=str, default='output.vec', help='Output prefix to use.')
    Parser.add_argument('--verbose','-v', action='store_true', help='Flag to enable more verbose output.')
    Parser.add_argument('--coefficients', type=complex, nargs='*', help='Complex coefficients to use when building the superposition. One per state in format a+bj.')
    Parser.add_argument('--coefficients-file', type=str, default='', help='Filename containing coefficients, two comma separated columns for real and imaginary parts.')
    Parser.add_argument('--states', type=str, nargs='*', help='The states to use to build the superposition.')
    Parser.add_argument('--normalise', action='store_true', help='Flag to indicate that the output vector should be normalised.')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    Coefficients = Args.coefficients
    CoefficientsFile = Args.coefficients_file
    Output = Args.output
    States = Args.states
    Normalise = Args.normalise

    if States is None or len(States) == 0:
        print('At least one state must be specified.')
        return
    NbrStates = len(States)

    if Coefficients is None: # if no coefficients given, use equal superposition or read from file if given
        if CoefficientsFile == '':
            Coefficients = np.ones(NbrStates)/np.sqrt(NbrStates)
        else:
            print(('Reading coefficients from ' + CoefficientsFile))
            Coefficients = list()
            reader = csv.reader(open(CoefficientsFile,'r'))
            for row in reader:
                Coefficients.append(np.complex(np.float(row[0]), np.float(row[1])))

    elif len(Coefficients) < NbrStates:
        print('Number of coefficients must match the number of states.')
        return

    if Verbose: print(('Combining states ' + str(States) + ' with coefficients ' + str(Coefficients) +'.'))

#    StateVectors = list()
#    for StateIdx in range(NbrStates):
#        StateVectors.append(vecutils.ReadBinaryPETScVector(States[StateIdx]))
#
#    OutputVec = StateVectors[0].duplicate()
#    OutputVec.zeroEntries()
#    for StateIdx in range(NbrStates):
#        OutputVec.axpy(Coefficients[StateIdx], StateVectors[StateIdx])

    OutputVec = vecutils.Superposition(States, Coefficients)

    if Verbose: print(('Combined state has normal ' + str(OutputVec.norm()) + '.'))
    if Normalise:
        if Verbose: print('Normalising...')
        OutputVec.normalize()

    if Verbose: print(('Writing vector to ' + Output + '.'))
    vecutils.writeBinaryPETScFile(OutputVec, Output)


if __name__ == '__main__' :
    __main__()
