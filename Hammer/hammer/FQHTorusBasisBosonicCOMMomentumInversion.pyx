#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusBasisBosonicCOMMomentumInversion.pyx: This is a cython implementation of code to work with the FQH basis on a Torus, implements COMMomentum and inversion symmetries. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
cimport numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
from petsc4py import PETSc
from hammer.FQHTorusBasisBosonic cimport FQHTorusBasisBosonic
from hammer.FQHTorusBasisBosonicCOMMomentum cimport FQHTorusBasisBosonicCOMMomentum


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)
    double ceil(double x)
    double floor(double x)

cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double cimag(double complex x) nogil
    double complex conj(double complex x) nogil


cdef class FQHTorusBasisBosonicCOMMomentumInversion(FQHTorusBasisBosonicCOMMomentum):
    cdef int InversionSector
    cdef int InversionSectors
    cdef int NumTranslations
    cdef int BytesToReverse
    cdef int ShiftAfterReverse
    cdef long OverflowMask1

    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.Distance = 0
        self.COMMomentum = -1
        self.Sectors = 0
        self.InversionSector = -1
        self.InversionSectors = 0
        self.NumTranslations = 0


    def __init__(self, int Particles, int NbrFlux, int Momentum, int COMMomentum, int InversionSector):
        """
        Set the variables. Inputs are:
        Particles: Number of particles in the basis.
        NbrFlux: Number of flux quanta to consider.
        Momentum: The total momentum, sum of the orbital angular momentum of each particle.
        COMMomentum: The center of mass momentum. If -1 then do not use this symmetry.
        InversionSector: The sector for the inversion, translate symmetry 0 to 1.
        """
        super().__init__(Particles, NbrFlux, Momentum, COMMomentum)
        self.InversionSector = InversionSector

        def FindSectorsAndTranslations(Particles, NbrFlux, Momentum):
            k = 0.0
            InversionSectors = 0
            NumTranslations = 0
            while k < 100.0:
                if ((2.0 * np.float(Momentum) + k*np.float(NbrFlux))/np.float(Particles)) % 1.0 == 0.0:
                    NumTranslations = ((2.0 * np.float(Momentum) + k*np.float(NbrFlux))/np.float(Particles)) + 1
                    InversionSectors = 2
                    break
                k += 1.0
            return [NumTranslations, InversionSectors]

        [NumTranslations1, InversionSectors1] = FindSectorsAndTranslations(Particles, NbrFlux, Momentum)
        [NumTranslations2, InversionSectors2] = FindSectorsAndTranslations(Particles, NbrFlux, COMMomentum)

        # if no multiples found then only a single sector and restrict to lowest one.
        if (InversionSectors1 == 0)  or (InversionSectors2 == 0):
            self.InversionSectors = 0
            self.InversionSector = 0
        else:
            self.InversionSectors = 2
            self.NumTranslations = NumTranslations1

        self.BytesToReverse = np.ceil(self.NbrFlux / 8.0)
        self.ShiftAfterReverse = 64 - self.NbrFlux

        self.NumTranslations = self.NumTranslations % self.NbrFlux

        # set overflow mask to top bits that will be rotated over the boundary
        self.OverflowMask1 = ((1l << self.NumTranslations) - 1l) << (self.NbrFlux - self.NumTranslations)

        #print 'NumTranslations: ' + str(self.NumTranslations)
        #print 'Overflow mask 1: ' + bin(self.OverflowMask1)
        #print 'Sectors: ' + str(self.InversionSectors)
        #print 'Current sectors: ' + str(self.InversionSectors)


    cdef int FindBasisDimension(self):
        """
        Calculates the basis dimension for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasisDimension to calculate the dimension of the basis.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int dim = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        cdef int i = 0
        for i in np.arange(0, self.Particles):
            MaxMomentum += (self.EffectiveFlux - 1 -i)
        i = 0
        while (self.EffectiveMomentum + i*(self.NbrFlux + self.FluxOffset)) <= MaxMomentum:
            dim += self.FindSubBasisDimensionCOMMomentumInversion(self.EffectiveMomentum + i*(self.NbrFlux + self.FluxOffset), self.EffectiveFlux, self.Particles, 0l)
            i+=1
        return dim


    cdef int FindSubBasisDimensionCOMMomentumInversion(self, int J, int Nphi, int Ne, long base):
        """
        Recursive function to find the basis dimension for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int dim = 0

        if (Ne == 0):
            if (J == 0) and self.IsBothRepresentativeConfiguration(base):
                return 1
            else:
                return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                dim += self.FindSubBasisDimensionCOMMomentumInversion(J-(Nphi-1), Nphi-1, Ne-1, base + (1l << (Nphi-1)))
            #elif J >= 0:
            #    dim += self.FindSubBasisDimension(J, Nphi-1, Ne)
            dim += self.FindSubBasisDimensionCOMMomentumInversion(J, Nphi-1, Ne, base)
        return dim


    def FindBasis(self):
        """
        Calculates the basis elements for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasis to calculate the basis elements and populate the BasisElements array.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, self.Particles):
            MaxMomentum += (self.EffectiveFlux - 1 - i)

        cdef int MaxTotMomentum = (self.NbrFlux + self.FluxOffset) * (MaxMomentum / (self.NbrFlux + self.FluxOffset)) + self.EffectiveMomentum

        return self.FindSubBasis(MaxTotMomentum, self.NbrFlux + self.FluxOffset, self.EffectiveFlux, self.Particles, idx, 0l)


    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis elements for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0) and self.IsBothRepresentativeConfiguration(base):  # this is used instead of the standard modulus operator
                self.BasisElements[idx] = base
                return 1
            else:
                return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasis(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))
            #elif J >= 0:
            #    idx += offset
            #    offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne, idx, base)
            idx += offset
            offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne, idx, base)
        return offset


    cdef int IsBothRepresentativeConfiguration(self, long State):
        """
        Method to check if the configuration passed is a representative configuration in both the COMMomentum sector and in the Inversion sector
        """
        cdef long TranslatedState
        cdef long InvertedState
        cdef double complex Normal = 0.0
        cdef double complex InvertNormal = 0.0
        cdef int i, j

        j = 0
        while j < self.InversionSectors:
            InvertedState = State
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                InvertedState = self.SimpleCOMInversion(State)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)

                if TranslatedState < State:
                    return 0
                if TranslatedState == State:
                    Normal += self.Phases[i] * InvertNormal
                i += 1
            j += 1

        if cabs(Normal) > 1e-10:
            return 1
        else:
            return 0


    cdef inline long SimpleTranslateAfterInvert(self, long State):
        """
        Function that translates the given configuration by t orbitals.

        For bosons the algoritm is as follows
        1. Iterate starting at lowest bit of fermionic representation until "(self.NumTranslations)" orbitals have been counted (each zero corresponds to orbital).
        2. Translate the state by the amount of bits that have been iterated over.
        """
        cdef int pos = 0
        cdef int orbitals = 0
        cdef int orbitals_target = self.NbrFlux - self.NumTranslations
        cdef long SmallerMask

        while pos < self.EffectiveFlux and orbitals < orbitals_target:
            if (State & (1l << pos)) == 0l:
                orbitals += 1
            pos += 1

        #SmallerMask = ((1l << pos) - 1l) << (self.EffectiveFlux - pos)

        #print "Pos: " + str(pos) + "(" + self.GetFermionicOccupationRepOfElement(State >> pos)  + " + " + self.GetFermionicOccupationRepOfElement(((State << (self.EffectiveFlux - pos + 1))))

        #print self.GetFermionicOccupationRepOfElement(((State << pos) + ((State >> (self.EffectiveFlux - pos)) & SmallerMask))
        #print self.GetFermionicOccupationRepOfElement(((State << pos) + ((State >> (self.EffectiveFlux - pos)) & SmallerMask)) & self.Mask)

        # now translate by pos orbitals
        return ((State >> pos) + ((State << (self.EffectiveFlux - pos + 1)))) & self.Mask


    cpdef long InvertBits(self, long State):
        """
        Reflect the given state, taking into account it is an effective representation for a bosonic state.
        """
        cdef long NewState = 0l
        cdef long Buffer = 0l
        cdef int i, j

        i = 0
        j = 0
        while i < self.EffectiveFlux:
            if (State & (1l << i)) > 0:
                Buffer += (1l << j)
                j+=1
            else:
                NewState += (Buffer << self.EffectiveFlux - i)
                Buffer = 0l
                j = 0
            i += 1

        NewState += (Buffer << self.EffectiveFlux - i)

        return NewState


    cpdef long SimpleCOMInversion(self, long State):
        """
        Function that reflects positions of particles in the provided state around the COM point and retruns the resulting state.
        """
        cdef int TranslationAmount # number of translations to make to representative configuration.
        cdef long TranslatedState, MinState, OriginalState
        cdef int i

        OriginalState = State
        #print "In state: " + self.GetOccupationRepOfElement(State)

        # Translate until minimum is found
        i = 1
        MinState = State
        TranslationAmount = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(State, i)
            if TranslatedState < MinState:
                MinState = TranslatedState
                TranslationAmount = i
            i += 1

        #print "1: " + self.GetOccupationRepOfElement(MinState)

        State = self.InvertBits(MinState)
        #print "2: " + self.GetOccupationRepOfElement(State)
        State = self.SimpleTranslateAfterInvert(State)
        #print "3: " + self.GetOccupationRepOfElement(State)

        # Translate until minimum is found
        MinState = State

        TranslatedState = self.SimpleTranslate(MinState, (self.Sectors - TranslationAmount) % self.Sectors)

        #print "Out state: " + self.GetOccupationRepOfElement(TranslatedState)

        return TranslatedState


    cpdef double GetNormalIdx(self, Idx):
        """
        Given the index of a basis element, calculate the normal
        """
        cdef long TranslatedState
        cdef long InvertedState
        cdef long State
        cdef double complex Normal = 0.0
        cdef double complex InvertNormal = 1.0
        cdef int i, j

        State = self.BasisElements[Idx]

        j = 0
        while j < self.InversionSectors:
            InvertedState = State
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                InvertedState = self.SimpleCOMInversion(State)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState == self.BasisElements[Idx]:
                    Normal += self.Phases[i] * InvertNormal
                i += 1
            j += 1

        return sqrt(creal(Normal))


    cdef double GetNormal(self):
        """
        Get the normal of the configuration stored in self.TmpState2
        """
        cdef long TranslatedState
        cdef long InvertedState
        cdef double complex Normal = 0.0
        cdef double complex InvertNormal = 1.0
        cdef int i, j

        j = 0
        while j < self.InversionSectors:
            InvertedState = self.TmpRepresentative
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                InvertedState = self.SimpleCOMInversion(self.TmpRepresentative)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState == self.TmpRepresentative:
                    Normal += self.Phases[i] * InvertNormal
                i += 1
            j += 1

        return sqrt(creal(Normal))


    cdef long GetRepresentative(self):
        """
        Try to find the index of the representative of element TmpState2
        """
        cdef long RepState = self.TmpState2
        cdef long TranslatedState
        cdef long InvertedState
        cdef int i, j

        j = 0
        while j < self.InversionSectors:
            InvertedState = self.TmpState2
            if j > 0:
                InvertedState = self.SimpleCOMInversion(self.TmpState2)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState < RepState:
                    RepState = TranslatedState
                i += 1
            j += 1

        self.TmpRepresentative = RepState

        return RepState


    cdef double complex GetRepresentativePhase(self):
        """
        Try to find phase relating the configuration in tmpstate2 and its representative configuration.
        """
        cdef double complex Phase = 0.0
        cdef double complex TranslatePhase = 1.0
        cdef double complex InvertNormal = 1.0
        cdef long TranslatedState
        cdef long InvertedState
        cdef long MinTranslatedState
        cdef int i, j

        # Start from representative and get relative phase back to TmpState2
        State = self.TmpRepresentative

        j = 0
        while j < self.InversionSectors:
            InvertedState = State
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                InvertedState = self.SimpleCOMInversion(State)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState == self.TmpState2:
                    Phase += self.Phases[i] * InvertNormal
                i += 1
            j += 1

        return conj(Phase)


    cpdef ConvertToFullBasis(self, State, FullBasis=None):
        """
        This method expands the state in the reduced centre of mass momentum
        and inversion sector basis to the full basis.

        Parameters
        -----------
        State: vector
            Vector object containing Fock coefficients.
        FullBasis: basis object
            Basis object for full basis. If None is recomputed (default=None).

        Returns
        --------
        basis object
            The full basis object.
        vector
            Vector of Fock coefficients for full state.
        """
        if FullBasis is None:
            FullBasis = FQHTorusBasisBosonic(self.Particles, self.NbrFlux, self.Momentum)
            FullBasis.GenerateBasis()

        cdef int BasisDim = self.Dim
        cdef int FullBasisDim = FullBasis.GetDimension()
        cdef int rstart, rend, Idx
        rstart, rend = State.getOwnershipRange()
        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] dnnz = np.ones(LocalDim, dtype=np.int32) * self.Sectors * self.InversionSectors
        cdef np.ndarray[np.int32_t, ndim=1] odnnz = np.ones(LocalDim, dtype=np.int32) * self.Sectors * self.InversionSectors

        # for small systems use a dense matrix and for large enough use a sparse matrix
        if (self.Sectors * self.InversionSectors) > (BasisDim-1):
            Op = PETSc.Mat(); Op.createDense([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            Op.setUp()
        else:
            Op = PETSc.Mat(); Op.create()
            Op.setSizes([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            Op.setPreallocationNNZ((dnnz, odnnz)) # one term per sector
            Op.setUp()

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(FullBasisDim)
        NewState.setUp()

        cdef int j, k, i = 0
        cdef double complex Phase
        cdef double Normal
        cdef long TranslatedState, InverstedState, Element
        cdef double OverallNorm = np.sqrt(self.Sectors * self.InversionSectors)
        cdef double complex InvertNormal

        i = rstart
        while i < rend:
            Element = self.GetElement(i)
            k = 0
            while k < self.InversionSectors:
                InvertedState = Element
                InvertNormal = (-1.0)**(self.InversionSector * k)
                if k > 0:
                    InvertedState = self.SimpleCOMInversion(Element)
                j = 0
                while j < self.Sectors:
                    TranslatedState = self.SimpleTranslate(InvertedState, j)
                    Phase = self.Phases[j] * InvertNormal
                    Normal = self.GetNormalIdx(i)
                    Idx = FullBasis.BinaryArraySearchDescendingElement(TranslatedState)
                    if Idx >= 0 and Idx < FullBasisDim:
                        Op.setValue(i, Idx, Phase/(Normal * OverallNorm), True)
                    j += 1
                k += 1
            i += 1
        Op.assemble()

        Op.multTranspose(State, NewState)

        return [FullBasis, NewState]

