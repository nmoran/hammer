#! /usr/bin/env python

""" FQHBasisManager.py: Module to manage basis objects to prevent duplication in scripts. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


# import basis classes
from hammer.FQHSphereBasis import FQHSphereBasis
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisBosonicCOMMomentum import FQHTorusBasisBosonicCOMMomentum
from hammer.FQHTorusBasisBosonicCOMMomentumInversion import FQHTorusBasisBosonicCOMMomentumInversion
from hammer.FQHTorusWF import FQHTorusWF
from hammer.FQHSphereWF import FQHSphereWF
import hammer.FQHFileManager as FileName

import numpy as np
import argparse
import time
import os

import petsc4py.PETSc as PETSc

class FQHBasisManager:
    """
    Class which provides utility functions for creating and managing basis objects.
    """

    def __init__(self, Particles = 4, NbrFlux = 12, Momentum = 2, COMMomentum = 2,
                 InversionSector = -1, Tau=np.complex(0.0, 1.0), Sphere=False, Bosonic=False):
        """
        Initialise the class instance with provided parameters or defaults if none given.

        Parameters
        ----------
        Particles: int
            Number of particles (default=4).
        NbrFlux: int
            Total number of flux (default=12).
        Momentum: int
            Total momentum number (default=2).
        COMMomentum: int
            Total centre of mass momemtum on torus, -1 if not used (default=-1).
        InversionSector: int
            The inversion sector either 0 or 1 or -1 if not used (default=-1).
        Sphere: flag
            Flag to indicate that it is spherical geometry that is being used (default=False).
        Bosonic: flag
            Flag to indicate that basis is bosonic (default=False).
        """
        self.Sphere = Sphere
        self.Particles = Particles
        self.NbrFlux = NbrFlux
        self.Momentum = Momentum
        self.COMMomentum = COMMomentum
        self.InversionSector = InversionSector
        self.Tau = Tau
        self.Bosonic = False
        self.ID = ''
        # only relevant to the sphere
        self.TwiceMaxLz = self.NbrFlux - 1
        self.TwiceLz = self.Momentum
        # put together basis file name based on parameters.
        self.BasisFile = self.GetBasisFilename()
        self.Bosonic = Bosonic


    def SetTorusDetailsFromFilename(self, filename):
        """
        From the name of a vector file try to discover the number of particles, flux and momentum sector. Return as a dictionary

        Parameters
        -----------
        filename: str
            The name of the filename from which to parse the arguments.
        """
        Details = dict()
        filename = os.path.basename(filename)
        filename = os.path.splitext(filename)[0]
        if 'Sphere' in filename: # we assume the wave-function is for a torus by default unless sphere appears in the name
            self.Sphere= True
        self.Extension = os.path.splitext(filename)[1]
        self.Particles = (-1 if filename.find('_p_') == -1 else int(filename.split('_p_')[-1].split('_')[0]))

        if not self.Sphere:
            # read torus specific fields
            self.NbrFlux = (-1 if filename.find('_Ns_') == -1 else int(filename.split('_Ns_')[-1].split('_')[0]))
            self.Momentum = (-1 if filename.find('_K_') == -1 else int(filename.split('_K_')[-1].split('_')[0]))
            self.COMMomentum = (-1 if filename.find('_P_') == -1 else int(filename.split('_P_')[-1].split('_')[0]))
            self.InversionSector = (-1 if filename.find('_I_') == -1 else int(filename.split('_I_')[-1].split('_')[0]))
            self.Tau = (np.complex(0.0, 1.0) if filename.find('_tau1_') == -1 else np.complex(float(filename.split('_tau1_')[-1].split('_')[0]),
                                                                                              float(filename.split('_tau2_')[-1].split('_')[0])))
            self.ID = ''
            if filename.find('_tau2_') >= 0:
                remainder = filename.split('_tau2_')[-1]
                if remainder.find('_') >= 0:
                    self.ID = remainder[remainder.find('_'):]
        else:
            # read sphere specific fields
            self.TwiceMaxLz = (-1 if filename.find('_2s_') == -1 else int(filename.split('_2s_')[-1].split('_')[0]))
            self.MaxLz = (-1 if filename.find('_lz_') == -1 else int(filename.split('_lz_')[-1].split('_')[0]))
            self.NbrFlux = self.TwiceMaxLz + 1


        # if there is any extra text after the torus dimensions, put into id string.

        if filename.find('Bosons') >= 0:
            self.Bosonic = True
        else:
            self.Bosonic = False

        self.BasisFile = self.GetBasisFilename()


    def SetTorusDetailsFromOpts(self, opts):
        """
        Set the details of the basis from the provided options object. Exception thrown if expected attributes not
        present (Only for torus at the moment).

        Parameters
        -----------
        opts: class instance
            Class instance with attributes describing basis properties.
        """
        if hasattr(opts, 'Particles'): self.Particles = opts.Particles
        if hasattr(opts, 'NbrFlux'): self.NbrFlux = opts.NbrFlux
        if hasattr(opts, 'Momentum'): self.Momentum = opts.Momentum
        if hasattr(opts, 'COMMomentum'): self.COMMomentum = opts.COMMomentum
        if hasattr(opts, 'InversionSector'): self.InversionSector = opts.InversionSector
        if hasattr(opts, 'Tau'): self.Tau = opts.Tau
        if hasattr(opts, 'Bosonic'): self.Bosonic = opts.Bosonic
        if hasattr(opts, 'BasisFile'):
            self.BasisFile = opts.BasisFile
        else:
            self.BasisFile = self.GetBasisFilename()


    def SetTorusDetailsFromOpts(self, args):
        """
        Set basis details from command line arguments.

        Parameters
        -----------
        args: list of str objects
            List of arguments specifying basis.
        """
        Parser = argparse.ArgumentParser(description='Parse basis parameters.')
        Parser.add_argument('--particles','-p',type=int,default=4,
                                 help='Number of particles to consider (default=4).')
        Parser.add_argument('--nbrflux','-l',type=int,default=12,
                                 help='Number of flux quanta to consider (default=12).')
        Parser.add_argument('--momentum','-y',type=int,default=2,
                                 help='Total momentum sector to consider (default=2).')
        Parser.add_argument('--commomentum','-c',type=int,default=2,
                                 help='Centre of mass momentum sector or -1 to not use this symmetry (default=2).')
        Parser.add_argument('--tau1','-t1',type=float,default=0.0,
                                 help='Value of tau1, real component of tau (default=0).')
        Parser.add_argument('--tau2','-t2',type=float,default=1.0,
                                 help='Value of tau2, imaginary component of tau (default=1.0).')
        Parser.add_argument('--inversion-sector', type=int, default=0,
                                 help='Inversion sector to use for diagonalistion 0-1 (default=0).')
        Parser.add_argument('--bosonic',action='store_true',
                                 help='Flag to indicate that calculations should be performed with '
                                 + 'bosonic statistics rather than the default fermionic.')
        Parser.add_argument('--basis',type=str, default='',
                                 help='Name of file to read basis from.')

        [ArgVals, Unknowns] = Parser.parse_known_args(args)

        self.Particles = ArgVals.particles
        self.NbrFlux = ArgVals.nbrflux
        self.Momentum = ArgVals.momentum
        self.COMMomentum = ArgVals.commomentum
        self.InversionSector = ArgVals.inversion_sector
        self.Tau = np.complex(ArgVals.tau1, ArgVals.tau2)
        self.Bosonic = ArgVals.bosonic
        self.BasisFile = ArgVals.basis
        if self.BasisFile == '': self.BasisFile = self.GetBasisFilename()


    def SetTorusDetailsFromDict(self, details):
        """
        Set the basis parameters from dictionary with the parameters of the basis.

        Parameters
        -----------
        details: dict
            Dictionary which contains basis parameters.
        """
        self.Particles = details['Particles']
        self.NbrFlux = details['NbrFlux']
        self.Momentum = details['Momentum']
        self.Bosonic = details['Bosonic']
        self.COMMomentum = (-1 if 'COMMomentum' not in details else details['COMMomentum'])
        self.InversionSector = (-1 if 'InversionSector' not in details else details['InversionSector'])


    def GetDetailsDict(self):
        """
        Function to return parameters as a dictinary
        """
        Details = dict()
        Details['Particles'] = self.Particles
        Details['NbrFlux'] = self.NbrFlux
        Details['Momentum'] = self.Momentum
        Details['COMMomentum'] = self.COMMomentum
        Details['InversionSector'] = self.InversionSector
        Details['Tau'] = self.Tau
        Details['Bosonic'] = self.Bosonic
        Details['Sphere'] = self.Sphere
        Details['TwiceMaxLz'] = self.TwiceMaxLz
        Details['TwiceLz'] = self.TwiceLz

        return Details


    def GetBasisFilename(self):
        """
        Function that returns typical filename for a basis file.

        Returns
        --------
        str
            The filename where a basis object with the given properties is written to by default.
        """
        if not self.Sphere:
            return FileName.TorusBasisFilename(self.Bosonic,self.Particles,self.NbrFlux,
                                               self.Momentum,self.COMMomentum,self.InversionSector)
        else:
            return FileName.SpheresBasisFilename(self.Bosonic,self.Particles,self.TwiceMaxLz,
                                                 self.TwiceLz)


    def GetFilenamePrefix(self, tau=None, ID=None):
        """
        Function that returns typical filename prefix from basis parameters and tau value.

        Parameters
        -----------
        tau: complex
            Parameters that specifies torus shape. If not then note used (default=None).

        Returns
        -------
        str
            The filename prefix that is used when writing quantities relating to this to disk.
        """
        if ID is None:
            ID = self.ID
        if not self.Sphere:
            return FileName.TorusFilenamePrefix(self.Bosonic,self.Particles,
                                                self.NbrFlux,self.Momentum,
                                                self.COMMomentum,self.InversionSector,
                                                tau, ID)
        else:
            return FileName.SphereBasisShort(self.Bosonic,self.Particles,
                                             self.TwiceMaxLz,self.TwiceLz)


    def GetBasis(self, Verbose=False, DimensionOnly=False):
        """
        Function to create and return an instance of the appropriate basis object.

        Parameters
        ----------
        Verbose: bool
            Flag to indicate that verbose output should be used.
        DimensionOnly: bool
            Flag to indicate that we should just calculate the dimension and not the actual configurations.
        """
        # Construction of basis.
        PetscPrint = PETSc.Sys.Print

        start = time.time()
        if not self.Sphere:
            if self.COMMomentum == -1:
                self.MyBasis = (FQHTorusBasisBosonic(self.Particles, self.NbrFlux, self.Momentum) if self.Bosonic
                        else FQHTorusBasis(self.Particles, self.NbrFlux, self.Momentum))
            else:
                if self.InversionSector == -1:
                    self.MyBasis = (FQHTorusBasisBosonicCOMMomentum(self.Particles, self.NbrFlux, self.Momentum, self.COMMomentum) if self.Bosonic
                            else FQHTorusBasisCOMMomentum(self.Particles, self.NbrFlux, self.Momentum, self.COMMomentum))
                else:
                    self.MyBasis = (FQHTorusBasisBosonicCOMMomentumInversion(self.Particles, self.NbrFlux,self.Momentum,
                                                                            self.COMMomentum, self.InversionSector)
                                    if self.Bosonic else
                                    FQHTorusBasisCOMMomentumInversion(self.Particles, self.NbrFlux,self.Momentum,
                                                                    self.COMMomentum, self.InversionSector))

            if Verbose: PetscPrint('Building basis for ' + str(self.Particles) + (' Bosons ' if self.Bosonic else ' Fermions ') + 'with ' + str(self.NbrFlux)
                                + ' flux, momentum ' + str(self.Momentum) + ('' if self.COMMomentum == -1 else ', ' + str(self.COMMomentum) )
                                + ('' if self.InversionSector == -1 else ' and inversion sector ' + str(self.InversionSector )) + '.')
        else:
            self.MyBasis = FQHSphereBasis(self.Particles, self.TwiceMaxLz, self.TwiceLz)
            if Verbose: PetscPrint('Building basis for ' + str(self.Particles) + (' Bosons ' if self.Bosonic else ' Fermions ') + 'with twice Lz max ' + str(self.TwiceMaxLz)
                                + ' and twice total Lz ' + str(self.TwiceLz) +  '.')


        if Verbose: PetscPrint('Generating basis configurations.')
        self.MyBasis.GenerateBasis(filename=self.BasisFile, DimensionOnly=DimensionOnly)
        end = time.time()
        if Verbose: PetscPrint('Basis dimension: ' + str(self.MyBasis.GetDimension()) + ', built in ' + str(end-start) + ' seconds.')

        return self.MyBasis


    def GetSingleParticleWF(self):
        """
        Return an object for calculating single particle wave-functions with current basis settings.

        Returns
        --------
        wf object
            A class instance which has a WFValue(z, orbital) function.
        """
        if not self.Sphere:
            return FQHTorusWF(self.NbrFlux, self.Tau)
        else:
            return FQHSphereWF(self.TwiceMaxLz)
