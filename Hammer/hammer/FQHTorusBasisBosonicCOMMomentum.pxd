from hammer.FQHTorusBasisBosonic cimport FQHTorusBasisBosonic

cdef class FQHTorusBasisBosonicCOMMomentum(FQHTorusBasisBosonic):
    cdef int COMMomentum
    cdef int Distance # amount by which to translate each time.
    cdef int Sectors # number of translations to get all the way around.
    cdef long Mask # mask which covers all the flux in the system.
    cdef double complex *Phases # array to hold pre computed phases to save time. 
    cdef long TmpRepresentative

    cdef int FindBasisDimension(self)
    cdef int FindSubBasisDimensionCOMMomentum(self, int J, int Nphi, int Ne, long base)
    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base)
    cpdef int IsRepresentativeConfiguration(self, long State)           
    cpdef long SimpleTranslate(self, long State, int Steps)
    cdef double GetNormalIdx(self, Idx)
    cdef double GetNormal(self)        
    cdef long GetRepresentative(self)
    cdef double complex GetRepresentativePhase(self)
    cpdef int BinaryArraySearchDescending(self)
    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx)
    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double AA(self, int Orbital1, int Orbital2, int Idx)
    cpdef double complex AdAd(self, int Orbital1, int Orbital2)
    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3)
    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx)
    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4)
    cpdef GetPhases(self)
    cpdef GetNumberSectors(self)
    cpdef ConvertToFullBasis(self, State, FullBasis=?) 

