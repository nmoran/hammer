#! /usr/bin/env python

""" Diagonalise.py: Utility diagonalise torus FQH systems. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys, slepc4py
import string
slepc4py.init(sys.argv)

from petsc4py import PETSc
from slepc4py import SLEPc

# Custom modules
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion
from hammer.FQHTorusBasisBosonicCOMMomentum import FQHTorusBasisBosonicCOMMomentum
from hammer.FQHTorusBasisBosonicCOMMomentumInversion import FQHTorusBasisBosonicCOMMomentumInversion
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients2Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients3Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients4Body
from hammer.FQHTorusHamiltonian import FQHTorusHamiltonian
from hammer.HammerArgs import HammerArgsParser
from hammer.FQHBasisManager import FQHBasisManager
import hammer.vectorutils as vecutils
import hammer.matrixutils as matutils
import hammer.FQHFileManager as FileName

# System modules
import sys
import argparse
import numpy as np
import time
import csv

import petsc4py
PETSc_Major_Version = np.int(petsc4py.__version__.split('.')[0])
PETSc_Minor_Version = np.int(petsc4py.__version__.split('.')[1])

def __main__(argv):
    # Run the argument parser and process command line arguments.
    Opts = HammerArgsParser()
    Opts.parseArgs(argv)
    Opts.WriteEnergies = True

    TorusDiagonalise(Opts, Log=True)

def TorusDiagonalise(Opts, Log=False, BasisObj=None):
    # This print is to be used when working with many processes as will ensure that statement is only printed once.
    PetscPrint = PETSc.Sys.Print

    # Find the number of processes.
    NumProcesses = PETSc.Comm.getSize(PETSc.COMM_WORLD)
    MyRank = PETSc.Comm.getRank(PETSc.COMM_WORLD)

    if MyRank == 0 and Log:
        Opts.writeRunDetailsStart()

    if not Opts.Silent: PetscPrint("Using %d processes." % NumProcesses)

    if Opts.ID == '':
        Opts.ID=FileName.formatInteractionName(Opts.PseudoPotentials,Opts.LL,
                                              Opts.Interaction,Opts.AddThreeBody)

    # Prefix to use for data files.
    if Opts.Prefix == '':
        Prefix=FileName.TorusStatePrefix(Opts.Bosonic,Opts.Particles,Opts.NbrFlux,Opts.Momentum,
                                         Opts.COMMomentum,Opts.InversionSector,
                                         Opts.Tau.real,Opts.Tau.imag,Opts.MagneticLength,Opts.ID)
    else:
        Prefix = Opts.Prefix

    if not Opts.Silent: PetscPrint('Diagonalising Torus wih %d particles, %d flux quanta at momentum %d' % (Opts.Particles, Opts.NbrFlux, Opts.Momentum))
    if not Opts.Silent: PetscPrint('Tau: %.4f + i%.4f' % (Opts.Tau.real, Opts.Tau.imag))


    #### ------------------------------------------------------
    ####        Calculation of interaction coefficients.
    #### ------------------------------------------------------

    if Opts.Interaction not in ['Coulomb', 'Delta', '3Body']:
        if not Opts.Silent: PetscPrint('Interaction ' + Opts.Interaction + ' is not supported.')
        if Opts.CleanExit:
            return None
        else:
            sys.exit(1) ###Quit with an error message

    if not Opts.Silent: PetscPrint('Calculating coefficients for ' + str(Opts.ID) + ' interaction.')
    if Opts.Interaction == '3Body':
        InteractionCoefficients = FQHTorusInteractionCoefficients3Body(Opts.Tau, Opts.NbrFlux, Interaction=Opts.Interaction, LL=Opts.LL,MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic);
    elif Opts.Interaction == '4Body':
        InteractionCoefficients = FQHTorusInteractionCoefficients4Body(Opts.Tau, Opts.NbrFlux, Interaction=Opts.Interaction, LL=Opts.LL,MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic);
    else:
        if Opts.PseudoPotentials is not None:
            if not Opts.Silent: PetscPrint('Pseudopotentials: ' + str(Opts.PseudoPotentials))
        InteractionCoefficients = FQHTorusInteractionCoefficients2Body(Opts.Tau, Opts.NbrFlux, Interaction=Opts.Interaction, LL=Opts.LL,MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic, PseudoPotentials=Opts.PseudoPotentials, Thickness=Opts.Thickness, AppendPseudoPotentials=Opts.AppendPseudoPotentials);
    start = time.time(); InteractionCoefficients.CalculateCoefficients();end = time.time()
    if not Opts.Silent: PetscPrint('L1: ' + str(InteractionCoefficients.L1) + ', L2: ' + str(InteractionCoefficients.L2) + '.')
    if not Opts.Silent: PetscPrint('Coefficients calculated in ' + str(end-start) + ' seconds.')

    # if we are adding a 3 body component, we need to calculate the coefficients for this also.
    if np.abs(Opts.AddThreeBody) > 0.0:
        InteractionCoefficients3Body = FQHTorusInteractionCoefficients3Body(Opts.Tau, Opts.NbrFlux, Interaction=Opts.Interaction, LL=0,MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic);
        start = time.time(); InteractionCoefficients3Body.CalculateCoefficients();end = time.time()
        if not Opts.Silent: PetscPrint('Calculated additional 3body coefficients in ' + str(end-start) + ' seconds.')

    if Opts.WriteCoefficients and MyRank == 0: # Only do this if the fist process.
        InteractionCoefficients.WriteCoefficients(Prefix+'_Coeff.csv')
        InteractionCoefficients.WriteAntiSymCoefficients(Prefix+'_CoeffSym.csv')

    if Opts.CoefficientsOnly:
        return


    #### ------------------------------------------------------
    ####                Construction of basis.
    #### ------------------------------------------------------
    if BasisObj is None:
        MyBasis = BuildBasis(Opts)
    else:
        MyBasis = BasisObj
        if not Opts.Silent: PetscPrint('Basis dimension: ' + str(MyBasis.GetDimension()) + ', using precomputed basis.')

    if Opts.State != '' :
        State = vecutils.ReadBinaryPETScVector(Opts.State)
        StateDimension = State.getSize()
        if StateDimension != MyBasis.GetDimension():
            if not Opts.Silent: PetscPrint('Basis dimension (%d) differs from the dimesion of the sepcified state (%d).' % MyBasis.GetDimension, StateDimension)
            if Opts.CleanExit:
                return None
            else:
                sys.exit(1) ###Quit with an error message

    ###Check if the dimension is smaller than zero. IF it is exit.
    if MyBasis.GetDimension() <= 0:
        if not Opts.Silent: PetscPrint('Basis dimension ' + str(MyBasis.GetDimension()) + ', exiting.')
        if Opts.CleanExit:
            return None
        else:
            sys.exit(1) ###Quit with an error flag


    ###Check if the dimension is smaller than requested number of eigenvalues. If it is exit.
    if MyBasis.GetDimension() < Opts.NbrEigvals:
        if not Opts.Silent: PetscPrint('Basis dimension ' + str(MyBasis.GetDimension()) + ' is smaller than requested number of eigenvalues '+str(Opts.NbrEigvals)+', exiting.')
        if Opts.CleanExit:
            return None
        else:
            sys.exit(1) ###Quit with an error message

    if MyBasis.GetDimension() < Opts.NbrStates:
        if not Opts.Silent: PetscPrint('Basis dimension ' + str(MyBasis.GetDimension()) + ' is smaller than requested number of states '+str(Opts.NbrStates)+', exiting.')
        if Opts.CleanExit:
            return None
        else:
            sys.exit(1) ###Quit with an error message



    # Check whether or not to exempt orbitals.
    if Opts.ExemptContOrbitals > 1:
        if not Opts.Silent: PetscPrint('Excluding configurations with more than ' + str(Opts.ExemptContOrbitals) + ' consecutive '
                                       + 'orbitals filled or empty.')
        MyBasis.ApproximateBasisPy(Opts.ExemptContOrbitals, Opts.ExemptContOrbitals)
        if not Opts.Silent: PetscPrint('Reduded basis dimension: ' + str(MyBasis.GetDimension()))

    # Construct the Hamiltonian matrix
    TorusHamiltonian = FQHTorusHamiltonian(MyBasis, InteractionCoefficients, Opts.KeepReal)
    start = time.time()
    if Opts.Interaction == '4Body':
        HamiltonianMatrix = TorusHamiltonian.CreateSlepc4BodyHamiltonian(Opts.PreallocatedMatrix)
    elif Opts.Interaction == '3Body':
        HamiltonianMatrix = TorusHamiltonian.CreateSlepc3BodyHamiltonian(Opts.PreallocatedMatrix)
    else:
        HamiltonianMatrix = TorusHamiltonian.CreateSlepcHamiltonian(Opts.PreallocatedMatrix)
    end = time.time()
    if not Opts.Silent: PetscPrint( 'Hamiltonian constructed in ' + str(end-start) + ' seconds.')

    if np.abs(Opts.AddThreeBody) > 0.0:
        start = time.time()
        TorusHamiltonian3Body = FQHTorusHamiltonian(MyBasis, InteractionCoefficients3Body, Opts.KeepReal)
        HamiltonianMatrix3Body = TorusHamiltonian3Body.CreateSlepc3BodyHamiltonian(None)
        end = time.time()
        if not Opts.Silent: PetscPrint( 'Constructed additional matrix in ' + str(end-start) + ' seconds.')
        start = time.time()
        HamiltonianMatrix.axpy(Opts.AddThreeBody, HamiltonianMatrix3Body)
        end = time.time()
        if not Opts.Silent: PetscPrint( 'Added ' + str(Opts.AddThreeBody) + ' times 3 body interaction in ' + str(end-start) + ' seconds.')


    # If no state is specified perform diagonalisation, otherwise calculate the expectation value (assuming state is normalised to 1).
    if Opts.State == '' :

        MyMatrix = HamiltonianMatrix

        if Opts.SubSpaceVectors != None:
            if not Opts.Silent: PetscPrint( 'Projecting to provided ' + str(len(Opts.SubSpaceVectors)) + ' dimensional subspace.')
            ProjectedMatrix = matutils.ProjectMatrix(HamiltonianMatrix, Opts.SubSpaceVectors)
            MyMatrix = ProjectedMatrix

        E = SLEPc.EPS()
        E.create()
        #E.setType(E.Type.LANCZOS)
        E.setOperators(MyMatrix)
        E.setProblemType(SLEPc.EPS.ProblemType.HEP)
        E.setWhichEigenpairs(SLEPc.EPS.Which.SMALLEST_REAL)
        E.setDimensions(nev=Opts.NbrEigvals)
        E.setTolerances(tol=Opts.EPSTol, max_it=Opts.MaxIterations)
        #S = E.getST()
        #S.setType(S.Type.SHIFT)
        #S.setShift(Opts.Shift)
        #S.setFromOptions()
        E.setFromOptions()
        if Opts.InitialState is not None:
            if not Opts.Silent: PetscPrint( 'Setting initial state for the solver.')
            E.setInitialSpace(Opts.InitialState)

        if Opts.MatrixInfo:
            NumpyMatrix=matutils.PETScMatToNumpyArray(HamiltonianMatrix)
            DiagMatrix=np.diag(NumpyMatrix)
            NumpyMatrix=NumpyMatrix-np.diag(DiagMatrix)
            SumOfSqaresDiag=np.sum(np.real(DiagMatrix)**2)
            SumOfAbsDiag=np.sum(np.abs(DiagMatrix))
            SumOfSqaresIm=np.sum(np.imag(NumpyMatrix)**2)
            SumOfAbsIm=np.sum(np.abs(np.imag(NumpyMatrix)))
            SumOfSqaresRe=np.sum(np.real(NumpyMatrix)**2)
            SumOfAbsRe=np.sum(np.abs(np.real(NumpyMatrix)))
            NumRe=np.sum(np.abs(np.real(NumpyMatrix))>10**-10)
            NumIm=np.sum(np.abs(np.imag(NumpyMatrix))>10**-10)
            HeaderString="sos diag,sos re,sos im,soa diag,soa re,soa im,num re,num im"
            print HeaderString,SumOfSqaresDiag, SumOfSqaresRe,SumOfSqaresIm,SumOfAbsDiag,SumOfAbsRe,SumOfAbsIm,NumRe,NumIm
            RealImagStats=np.array([SumOfSqaresDiag,SumOfSqaresRe,SumOfSqaresIm,SumOfAbsDiag,SumOfAbsRe,SumOfAbsIm,NumRe,NumIm])
            np.savetxt(Prefix+"_Hamiltonian_Info.csv",RealImagStats,
                       delimiter=",",header=HeaderString)

            
        if Opts.ShowMatrix:
            NumpyMatrix=matutils.PETScMatToNumpyArray(HamiltonianMatrix)
            DiagMatrix=np.diag(NumpyMatrix)
            NumpyMatrix=NumpyMatrix-np.diag(DiagMatrix)
            import matplotlib.pyplot as plt
            MaxReal=np.max(np.abs(np.real(NumpyMatrix)))
            MaxImag=np.max(np.abs(np.imag(NumpyMatrix)))
            MaxTotal=np.max(MaxReal,MaxImag)
            NumpyMatrix=NumpyMatrix+np.diag(DiagMatrix)
            plt.figure()
            plt.imshow(np.abs(NumpyMatrix),interpolation="nearest",
                       cmap="cubehelix_r",vmin=0)
            plt.title("Absolute value of hopping elments")
            plt.colorbar()
            plt.figure()
            plt.imshow(np.angle(NumpyMatrix),interpolation="nearest",
                       cmap="hsv",
                       vmax=np.pi,vmin=-np.pi)
            plt.title("Phase of hopping elments")
            plt.colorbar()
            plt.figure()
            plt.imshow(np.real(NumpyMatrix),interpolation="nearest",
                       cmap="bwr",
                       vmax=MaxTotal,vmin=-MaxTotal)
            plt.title("Real part of hopping elments")
            plt.colorbar()
            plt.figure()
            plt.imshow(np.imag(NumpyMatrix),interpolation="nearest",
                       cmap="bwr",
                       vmax=MaxTotal,vmin=-MaxTotal)
            plt.title("Imaginary part of hopping elments")
            plt.colorbar()
            plt.show(block=False)
            
            #Viewer = PETSc.Viewer()
            #Viewer.createDraw(display=":0.0")
            #Viewer.createASCII(name="foo.mat", format=PETSc.Viewer.Format.ASCII_MATLAB)
            #Viewer.view(HamiltonianMatrix)
            raw_input("Press any key")
            #(Viewer.clear() if PETSc_Minor_Version <= 4 else Viewer.clearDraw())
            #Viewer.destroy()

        if Opts.SaveHamiltonian:
            Viewer = PETSc.Viewer()
            if Opts.SaveASCII:
                #Viewer.createASCII(name=Prefix + "_Matrix.txt", mode=PETSc.Viewer.Mode.W, format=PETSc.Viewer.Format.ASCII_PYTHON)
                Viewer.createASCII(name=Prefix + "_Matrix.txt", mode=PETSc.Viewer.Mode.W)
            else:
                Viewer.createBinary(name=Prefix + "_Matrix.mat", mode=PETSc.Viewer.Mode.W)
            Viewer.view(HamiltonianMatrix)
            (Viewer.clear() if PETSc_Minor_Version <= 4 else Viewer.clearDraw())
            Viewer.destroy()

        start = time.time();E.solve(); end = time.time()
        if not Opts.Silent: PetscPrint( 'Hamiltonian diagonalised in ' + str(end-start) + ' seconds.')

        its = E.getIterationNumber()
        if not Opts.Silent: PetscPrint( "Number of iterations of the method: %d" % its )

        eps_type = E.getType()
        if not Opts.Silent: PetscPrint( "Solution method: %s" % eps_type )

        nev, ncv, mpd = E.getDimensions()
        if not Opts.Silent: PetscPrint( "Number of requested eigenvalues: %d" % nev )

        tol, maxit = E.getTolerances()
        if not Opts.Silent: PetscPrint( "Stopping condition: tol=%.4g, maxit=%d" % (tol, maxit) )

        nconv = E.getConverged()
        if not Opts.Silent: PetscPrint( "Number of converged eigenpairs %d" % nconv )
        if nconv == 0:
            PetscPrint( "No eigenpairs converged, exiting" )
            if Opts.CleanExit:
                return None
            else:
                sys.exit(1) ###Quit with an error message

        if MyRank == 0 and Opts.WriteEnergies:
            if sys.version_info[0] == 3:
                f = open(Prefix + "_Energies.csv", "w", encoding='utf8', newline='') # python 3 version
            elif sys.version_info[0] == 2:
                f = open(Prefix + "_Energies.csv", "wb")
            csv_writer = csv.writer(f)

        EigenValues = []
        EigenVectors = []

        SLEPcVersion = SLEPc.Sys.getVersion()

        if nconv > 0:
            # Create the results vectors
            vr, wr = MyMatrix.getVecs()
            vi, wi = MyMatrix.getVecs()
            #
            if not Opts.Silent: PetscPrint()
            if not Opts.Silent: PetscPrint("   Energy=k        ||Ax-kx||/||kx||   Estimate       ||Ax-kBx||_2 ")
            if not Opts.Silent: PetscPrint("----------------- ------------------  -------------  -------------")
            for i in range(nconv):
                k = E.getEigenpair(i, vr, vi)

                if SLEPcVersion[1] >= 6:
                    relativeError = E.computeError(i, SLEPc.EPS.ErrorType.RELATIVE)
                    residualNorm = E.computeError(i, SLEPc.EPS.ErrorType.BACKWARD)
                    errorEstimate = E.computeError(i, SLEPc.EPS.ErrorType.ABSOLUTE)
                else:
                    relativeError = E.computeRelativeError(i)
                    residualNorm = E.computeResidualNorm(i)
                    errorEstimate = E.getErrorEstimate(i)

                if not Opts.Silent: PetscPrint( "%.12g, %.12g, %.12g, %.12g, %.12g" % (k.real, k.imag, relativeError, errorEstimate, residualNorm) )
                if MyRank == 0 and Opts.WriteEnergies:
                    csv_writer.writerow([k.real, k.imag, relativeError, errorEstimate, residualNorm, Opts.Momentum,Opts.Tau.real, Opts.Tau.imag])


                if i < Opts.NbrEigvals:
                    if Opts.ReturnEigenValues:
                        EigenValues.append([k.real, k.imag, relativeError, errorEstimate, residualNorm, Opts.Momentum,Opts.Tau.real, Opts.Tau.imag])

                    ### Return the eigenvectors if asked for
                    ### But only the number of vectors asked for
                    if i < Opts.NbrStates:
                        EigenState = vr.copy()

                        # Check whether or not to exempt orbitals.
                        if Opts.ExemptContOrbitals > 1:
                            EigenState = MyBasis.ApproximateStateToFullState(EigenState)


                        if Opts.ReturnEigenVectors and i < Opts.NbrStates:
                            EigenVectors.append(EigenState)

                        if Opts.SaveStates:
                            Viewer = PETSc.Viewer()
                            if Opts.SaveASCII :
                                Viewer.createASCII(name=Prefix + "_State_" + str(i) + ".txt", mode=PETSc.Viewer.Mode.W)
                            else:

                                Viewer.createBinary(name=Prefix + "_State_" + str(i) + ".vec", mode=PETSc.Viewer.Mode.W)
                            Viewer.view(EigenState)
                            (Viewer.clear() if PETSc_Minor_Version <= 4 else Viewer.clearDraw())
                            Viewer.destroy()

                        if Opts.OutputFockCoefficients:
                            vecutils.WriteFockCoefficients(vr, Prefix + "_State_" + str(i) + "_fock_coeffs.dat")
            PetscPrint()
        else:
            if not Opts.Silent: PetscPrint('No eigenvalues converged!')
            if Opts.CleanExit:
                return None
            else:
                sys.exit(1) ###

        if MyRank == 0 and Opts.WriteEnergies:
            f.close()

        ReturnSet = dict()
        ReturnSet['EigenValues'] = EigenValues
        ReturnSet['EigenVectors'] = EigenVectors
        if Opts.ReturnBasis:
            ReturnSet['Basis'] = MyBasis
        if Opts.ReturnMatrix:
            ReturnSet['Matrix'] = HamiltonianMatrix
        else:
            ReturnSet['Matrix'] = None
        if MyRank == 0 and Log:
            Opts.writeRunDetailsEnd()
        return ReturnSet

    else:
        ExpectationValue = matutils.ExpectationValue(HamiltonianMatrix, State)

        if MyRank == 0:
            if sys.version_info[0] == 3:
                f = open(Prefix + "_ExpectationValue.csv","w", encoding='utf8', newline='') # python 3 version
            elif sys.version_info[0] == 2:
                f = open(Prefix + "_ExpectationValue.csv","w")
            csv_writer = csv.writer(f)
            csv_writer.writerow([np.real(ExpectationValue), np.imag(ExpectationValue)])
            f.close()

        if not Opts.Silent: PetscPrint("Expectation value:")
        if not Opts.Silent: PetscPrint( str(np.real(ExpectationValue)) + ',' + str(np.imag(ExpectationValue)))
        if MyRank == 0 and Log:
            Opts.writeRunDetailsEnd()

        return {'ExpectationValue': ExpectationValue}


def BuildBasis(Opts):
    """
    This utility function builds a basis accoding to the options specified in the supplied HammerArgs object.
    """
    # This print is to be used when working with many processes as will ensure that statement is only printed once.
    PetscPrint = PETSc.Sys.Print

    if Opts.BasisFile == '':
        if Opts.Bosonic:
            BasisFile=FileName.TorusBasisFilename(
                Opts.Bosonic,Opts.Particles,Opts.NbrFlux,
                Opts.Momentum,-1,-1)
        else:
            BasisFile=FileName.TorusBasisFilename(
                Opts.Bosonic,Opts.Particles,Opts.NbrFlux,
                Opts.Momentum,Opts.COMMomentum,Opts.InversionSector)
    else:
        BasisFile = Opts.BasisFile

    # Construction of basis.
    if not Opts.Bosonic:
        start = time.time()
        if Opts.COMMomentum == -1:
            MyBasis = FQHTorusBasis(Opts.Particles, Opts.NbrFlux, Opts.Momentum)
        else:
            if Opts.InversionSector == -1:
                MyBasis = FQHTorusBasisCOMMomentum(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum)
            else:
                MyBasis = FQHTorusBasisCOMMomentumInversion(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum, Opts.InversionSector)
        if Opts.WriteBasisFlag:
            MyBasis.GenerateBasis(BasisFile)
        else:
            MyBasis.GenerateBasis()
        end = time.time()
        if not Opts.Silent: PetscPrint('Building basis for ' + str(Opts.Particles) + ' Fermions with ' + str(Opts.NbrFlux) + ' flux and momentum ' + str(Opts.Momentum) + ('' if Opts.COMMomentum == -1 else ' and COM momentum ' + str(Opts.COMMomentum) ) + '.')
    else:
        start = time.time()
        if Opts.COMMomentum == -1:
            MyBasis = FQHTorusBasisBosonic(Opts.Particles, Opts.NbrFlux, Opts.Momentum)
        else:
            if Opts.InversionSector == -1:
                MyBasis = FQHTorusBasisBosonicCOMMomentum(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum)
            else:
                MyBasis = FQHTorusBasisBosonicCOMMomentumInversion(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum, Opts.InversionSector)
        MyBasis.GenerateBasis()
        end = time.time()
        if not Opts.Silent: PetscPrint('Building basis for ' + str(Opts.Particles) + ' Bosons with ' + str(Opts.NbrFlux) + ' flux and momentum ' + str(Opts.Momentum) + ('' if Opts.COMMomentum == -1 else ' and COM momentum ' + str(Opts.COMMomentum) ) + '.')
    if not Opts.Silent: PetscPrint('Basis dimension: ' + str(MyBasis.GetDimension()) + ', built in ' + str(end-start) + ' seconds.')

    return MyBasis


def TorusLaughlinGroundState(Particles, Bosonic=False, Tau=np.complex(0.0, 1.0)):
    """
    Function to calculate 1/3 (or 1/2 in bosonic case) Laughlin state by diagonalising model pseudopotential Hamiltonian.

    Parameters
    -----------
    Particles: int
        Number of particles.
    Bosonic: flag
        Flag to indicate that we want the bosonic Laughlin rather than the fermionic Laughlin (default = False).
    Tau: complex
        The complex parameter which specifies the shape of the torus (default=i).

    Returns
    --------
    Basis object
        The basis object of the Hilbert space where the Laughlin is found.
    Vec
        The petsc vector object containing the Fock coefficients of the Laughlin state in the first momentum sector.
    """
    Opts = HammerArgsParser(); Opts.parseArgs('')
    Opts.Particles = Particles
    Opts.Bosonic = Bosonic
    Opts.NbrFlux = Particles * (2 if Bosonic else 3)
    Opts.Tau = Tau
    Opts.Momentum = (0 if (Opts.Particles % 2) == 1 or Bosonic else Opts.Particles/2) # choose the first momentum sector with the Laughlin.
    Opts.COMMomentum = (0 if (Opts.Particles % 2) == 1 or Bosonic else Opts.Particles/2) # choose the first momentum sector with the Laughlin.
    Opts.InversionSector = 0
    Opts.ReturnEigenVectors = True
    Opts.ReturnEigenValues = True
    Opts.ReturnBasis = True
    Opts.PseudoPotentials = [1.0, 1.0] # should work for both Bosonic and fermionic systems
    Opts.NbrEigvals = 1
    Opts.NbrStates = 1

    Data = TorusDiagonalise(Opts)

    # check if lowest state has approximately zero energy
    assert(np.abs(Data['EigenValues'][0][0]) < 1e-10)

    return [Data['Basis'], Data['EigenVectors'][0]]

def TorusPfaffianGroundState(Particles, Bosonic=False, Tau=np.complex(0.0, 1.0),StateNo=0):
    """
    Function to calculate 1/2 Pfaffian state by diagonalising  model tree-body interaction Hamiltonian.

    Parameters
    -----------
    Particles: int
        Number of particles.
    Bosonic: flag
        Flag to indicate that we want the bosonic Pfaffian rather than the fermionic Pfaffian (default = False).
    Tau: complex
        The complex parameter which specifies the shape of the torus (default=i).
    StateNo: int
        Enumerating the Pfaffian states: If Ne=2*N, and Ns=4*N then 0-> (0,N) , 1 -> (N,0), 2 -> (N,N), 3-> (2N,N) , 4 -> (3N,0), 5 -> (3N,N) (default = 0)
    Returns
    --------
    Basis object
        The basis object of the Hilbert space where the Pfaffian is found.
    Vec
        The petsc vector object containing the Fock coefficients of the Pfaffian state in the first momentum sector.
    """

    assert Particles%2==0, "Pfaffian only defined for even number of particles"
    Cells=Particles/2
    Opts = HammerArgsParser(); Opts.parseArgs('')
    Opts.Particles = Particles
    Opts.Bosonic = Bosonic
    Opts.NbrFlux = Particles * (1 if Bosonic else 2)
    Opts.Tau = Tau
    if StateNo==0:
        (Opts.Momentum,Opts.COMMomentum)=(0,Cells)
    elif StateNo==1:
        (Opts.Momentum,Opts.COMMomentum)=(Cells,0)
    elif StateNo==2:
        (Opts.Momentum,Opts.COMMomentum)=(Cells,Cells)
    elif StateNo==3:
        (Opts.Momentum,Opts.COMMomentum)=(2*Cells,Cells)
    elif StateNo==4:
        (Opts.Momentum,Opts.COMMomentum)=(3*Cells,0)
    elif StateNo==5:
        (Opts.Momentum,Opts.COMMomentum)=(3*Cells,Cells)
    else:
        raise IndexError, "Pfaffian State out of bounds"
    Opts.ReturnEigenVectors = True
    Opts.ReturnEigenValues = True
    Opts.ReturnBasis = True
    Opts.Interaction = '3Body' # should work for both Bosonic and fermionic systems
    Opts.NbrEigvals = 1
    Opts.NbrStates = 1

    Data = TorusDiagonalise(Opts)

    # could check if lowest state has approximately zero energy
    # But we know this will be a matter of numerical preciion.
    #assert(np.abs(Data['EigenValues'][0][0]) < 1e-6)

    return [Data['Basis'], Data['EigenVectors'][0]]


def TorusCoulombGroundState(Particles, NbrFlux, LL=0, Momentum=0, COMMomentum=-1, Bosonic=False, Tau=np.complex(0.0, 1.0), InversionSector=-1):
    """
    Function to calculate Coulomb ground state with specified number of particles, flux and Landau levels.

    Parameters
    ----------
    Particles: int
        Number of particles.
    NbrFlux: int
        Number of flux.
    LL: int
        Landau level index, 0 for LLL (default=0).
    Momentum: int
        Total momentum (default=0).
    COMMomentum: int
        Centre of mass momentum, -1 to not use symmetry (default=-1).
    Bosonic: flag
        Flag to indicate that Bosonic system should be considered (default=False).
    Tau: complex
        Torus geometry parameter.
    InversionSector: int
        The inversion sector to diagonalise in, -1 to not use this symmetry (default=-1).

    Returns
    --------
    Basis object
        The basis in the space.
    PETSc vector
        The ground state vector.
    """
    Opts = HammerArgsParser(); Opts.parseArgs('')
    Opts.Particles = Particles
    Opts.Bosonic = Bosonic
    Opts.NbrFlux = NbrFlux
    Opts.Tau = Tau
    Opts.Momentum = Momentum
    Opts.COMMomentum = COMMomentum
    Opts.InversionSector = InversionSector
    Opts.ReturnEigenVectors = True
    Opts.ReturnEigenValues = True
    Opts.ReturnBasis = True
    Opts.LL = LL
    Opts.NbrEigvals = 1
    Opts.NbrStates = 1
    Opts.CleanExit = True

    Data = TorusDiagonalise(Opts)

    if Data is None:
        return None
    else:
        # make sure more than one state resolved
        assert(len(Data['EigenVectors']) > 0)

        return [Data['Basis'], Data['EigenVectors'][0]]


def TorusCoulombStates(Particles, NbrFlux, LL=0, Momentum=0, COMMomentum=-1, Bosonic=False, Tau=np.complex(0.0, 1.0), InversionSector=-1, NStates=-1, Energies=False):
    """
    Function to calculate Coulomb ground state with specified number of particles, flux and Landau levels.

    Parameters
    ----------
    Particles: int
        Number of particles.
    NbrFlux: int
        Number of flux.
    LL: int
        Landau level index, 0 for LLL (default=0).
    Momentum: int
        Total momentum (default=0).
    COMMomentum: int
        Centre of mass momentum, -1 to not use symmetry (default=-1).
    Bosonic: flag
        Flag to indicate that Bosonic system should be considered (default=False).
    Tau: complex
        Torus geometry parameter.
    InversionSector: int
        The inversion sector to diagonalise in, -1 to not use this symmetry (default=-1).
    Nstates: int
        The number of eigenstates to get, if -1 get all (default=-1).
    Energies: bool
        Flag to indicate that the energies should also be returned.

    Returns
    --------
    Basis object
        The basis in the space.
    list of PETSc vectors
        List of eigenstates.
    array
        Array of energy corresponding energy eigen values (only if Energies argument is true).
    """
    Opts = HammerArgsParser(); Opts.parseArgs('')
    Opts.Particles = Particles
    Opts.Bosonic = Bosonic
    Opts.NbrFlux = NbrFlux
    Opts.Tau = Tau
    Opts.Momentum = Momentum
    Opts.COMMomentum = COMMomentum
    Opts.InversionSector = InversionSector
    Opts.ReturnEigenVectors = True
    Opts.ReturnEigenValues = True
    Opts.ReturnBasis = True
    Opts.LL = LL
    Opts.NbrEigvals = NStates
    Opts.NbrStates = NStates
    Opts.CleanExit = True

    if NStates == -1:
        bm = FQHBasisManager(Particles, NbrFlux, Momentum, COMMomentum, InversionSector, Bosonic)
        b = bm.GetBasis(DimensionOnly=True)
        dim = b.GetDimension()
        Opts.NbrStates = dim
        Opts.NbrEigvals = dim

    Data = TorusDiagonalise(Opts)

    if Data is None:
        return None
    else:
        # make sure more than one state resolved
        assert(len(Data['EigenVectors']) >= NStates)

        if Energies:
            return [Data['Basis'], Data['EigenVectors'], list(map(lambda x: Data['EigenValues'][x][0], range(len(Data['EigenValues']))))]
        else:
            return [Data['Basis'], Data['EigenVectors']]


def TorusCoulombHamiltonian(Particles, NbrFlux, Momentum=0, COMMomentum=-1, Bosonic=False, Tau=np.complex(0.0, 1.0), InversionSector=-1, numpy=False,Basis=False):
    """
    Function to return the Coulomb Hamiltonian in the given sector.

    Parameters
    ----------
    Particles: int
        Number of particles.
    NbrFlux: int
        Number of flux.
    Momentum: int
        Total momentum (default=0).
    COMMomentum: int
        Centre of mass momentum, -1 to not use symmetry (default=-1).
    Bosonic: flag
        Flag to indicate that Bosonic system should be considered (default=False).
    Tau: complex
        Torus geometry parameter.
    InversionSector: int
        The inversion sector to diagonalise in, -1 to not use this symmetry (default=-1).
    numpy: bool
        Flag to indicate that it should be returned as a numpy array, other wise a SLEPc sparse matrix is returned (default=False).

    Returns
    --------
    matrix
        The Hamiltonian matrix in slepc sparse matrix format by default or as a numpy ndarray.
    """
    InteractionCoefficients = FQHTorusInteractionCoefficients2Body(Tau, NbrFlux, Interaction='Coulomb', MagneticLength=1.0, Bosonic=Bosonic)
    InteractionCoefficients.CalculateCoefficients()

    BasisManager = FQHBasisManager(Particles, NbrFlux, Momentum, COMMomentum, InversionSector, Bosonic)
    MyBasis = BasisManager.GetBasis()

    TorusHamiltonian = FQHTorusHamiltonian(MyBasis, InteractionCoefficients)

    HamiltonianMatrix = TorusHamiltonian.CreateSlepcHamiltonian(None)

    if numpy:
        if Basis:
            return matutils.PETScMatToNumpyArray(HamiltonianMatrix),MyBasis
        else:
            return matutils.PETScMatToNumpyArray(HamiltonianMatrix)
    else:
        if Basis:
            return HamiltonianMatrix,MyBasis
        else:
            return HamiltonianMatrix

def TorusLaughlinHamiltonian(Particles, NbrFlux, Momentum=0, COMMomentum=-1, Bosonic=False, Tau=np.complex(0.0, 1.0), InversionSector=-1, numpy=False,Basis=False):
    """
    Function to return the Laughlin pseudopotential Hamiltonian in the given sector.

    Parameters
    ----------
    Particles: int
        Number of particles.
    NbrFlux: int
        Number of flux.
    Momentum: int
        Total momentum (default=0).
    COMMomentum: int
        Centre of mass momentum, -1 to not use symmetry (default=-1).
    Bosonic: flag
        Flag to indicate that Bosonic system should be considered (default=False).
    Tau: complex
        Torus geometry parameter.
    InversionSector: int
        The inversion sector to diagonalise in, -1 to not use this symmetry (default=-1).
    numpy: bool
        Flag to indicate that it should be returned as a numpy array, other wise a SLEPc sparse matrix is returned (default=False).

    Returns
    --------
    matrix
        The Hamiltonian matrix in slepc sparse matrix format by default or as a numpy ndarray.
    """
    PseudoPotentials=[1.0, 1.0] # should work for both Bosonic and fermionic systems


    InteractionCoefficients = FQHTorusInteractionCoefficients2Body(Tau, NbrFlux, Interaction="Coulomb", MagneticLength=1.0, Bosonic=Bosonic, PseudoPotentials=PseudoPotentials, AppendPseudoPotentials=False)
    InteractionCoefficients.CalculateCoefficients()

    BasisManager = FQHBasisManager(Particles, NbrFlux, Momentum, COMMomentum, InversionSector, Bosonic)
    MyBasis = BasisManager.GetBasis()

    TorusHamiltonian = FQHTorusHamiltonian(MyBasis, InteractionCoefficients)

    HamiltonianMatrix = TorusHamiltonian.CreateSlepcHamiltonian(None)

    if numpy:
        if Basis:
            return matutils.PETScMatToNumpyArray(HamiltonianMatrix),MyBasis
        else:
            return matutils.PETScMatToNumpyArray(HamiltonianMatrix)
    else:
        if Basis:
            return HamiltonianMatrix,MyBasis
        else:
            return HamiltonianMatrix


    

if __name__ == '__main__':
    __main__(sys.argv)
