#!/usr/bin/python

#
# This class serves to parse command line arguments, set the corresponding options and store these for later retrieval.
#
#
#
import argparse
import numpy as np
import time
import hammer.vectorutils as vectorutils
import sys
import hammer.FQHFileManager as FileName
import hammer.HammerDefaults as HD


class HammerArgsParser:
    """
    Class to parse command line arguments. Also serves to save options and pass between functions and classes.

    TODO: Change to use dict datatype intenally or even to be a subclass of dict to make more extensible.
    """


    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(
            description='Diagonalise a torus with given parameters.',
            parents=[self.BasisParser(),self.GeometryParser(),self.InteractionParser()])
        self.Parser.add_argument('--eigenstate',action='store_true', help='Flag to indicate eigenstates should be written to disk.')
        self.Parser.add_argument('--nbr-eigvals',type=int,default=HD.eigvals(), help='Minimum number of eigenvalues to attempt to calculate (default='+str(HD.eigvals())+').')
        self.Parser.add_argument('--nbr-states',type=int,default=HD.numstates(), help='Number of eigenstates to write to disk (default='+str(HD.numstates())+'). Activate with --eigenstate')
        self.Parser.add_argument('--eps-tol',type=float ,default=HD.eps_tol(), help='Tolerance to use for eigen problem solver (default='+str(HD.eps_tol())+').')
        self.Parser.add_argument('--save-hamiltonian',action='store_true', help='Flag to indicate the Hamiltonian should be written to disk.')
        self.Parser.add_argument('--real',action='store_true', help='Flag to indicate only real matrix elements are considered.')
        self.Parser.add_argument('--mpmath',action='store_true', help='Flag to indicate use of multiprecision math library to calculate interaction coefficients.')
        self.Parser.add_argument('--coefficients-only',action='store_true', help='Flag to indicate that execution should stop after coefficients are calculated.')
        self.Parser.add_argument('--write-coefficients',action='store_true', help='Flag to indicate that coefficient values shoudl be written to disk.')
        self.Parser.add_argument('--show-matrix',action='store_true', help='Flag to indicate matrix sparsity plot should be displayed (display required).')
        self.Parser.add_argument('--matrix-info',action='store_true', help='Flag to indicate matrix information should be written to file (NB: for now uses full rank matrices).')
        self.Parser.add_argument('--save-ascii',action='store_true', help='Flag to indicate that states are to be written in ASCII format rather than the default binary.')
        self.Parser.add_argument('--prefix',default='', help='The prefix to use for output files containing the energies and eigen states.')
        self.Parser.add_argument('--shift', type=float, default=HD.spectral_shift(), help='This is the spectral shift that is used (default = '+str(HD.spectral_shift())+').')
        self.Parser.add_argument('--state', default='', help='If a state is given the code will get the expectation value of this state with respect to the constructed operator rather than diagonalising,')
        self.Parser.add_argument('--basis', default='', help='Provide basis file. Will use basis file if found with name "Torus_(Bosons)_p_<p>_l_<l>_K_<k>.basis".')
        self.Parser.add_argument('--id', default='', help='ID to use to identify output files.')
        self.Parser.add_argument('--initial-state', default='', help='Vector to use as initial state for the solver.')
        self.Parser.add_argument('--max-iterations', type=int, default=HD.max_iterations(), help='Maximum number of iterations of the solver (default='+str(HD.max_iterations())+').')
        self.Parser.add_argument('--append-pseudopotentials', action='store_true', help='With this option the pseudo-potentaisl coefficients are added to the other underlying interaction rather that replacing it.')
        self.Parser.add_argument('--thickness', type=float, default=0.0, help='The finite-thickness parameter which will be added using the ZDS approximation (v(q) = (e^-dq)/q.)')
        self.Parser.add_argument('--lx', type=float, default=0.0, help='Explicitly specify a value for lx (assume right angled torus).')
        self.Parser.add_argument('--no-basis-write', action='store_false', help='Flag to indicate whether or not to write the basis to disk.')
        self.Parser.add_argument('--output-fock-coefficients', action='store_true', help='Output the coefficients of the fock configurations in a two column space separated formate with real part in first column and imaginary part in the second.')

        self.Parser.add_argument('--silent', action='store_true', help='Flag to indicate that no output should be given.')
        self.LogFileName = None

    def InteractionParser(self):
        """
        Flags related to the interaction Hamiltonian.
        """
        parent_interation_parser=argparse.ArgumentParser(add_help=False)
        parent_interation_parser.add_argument('--landau-level','-n',type=int,default=HD.ll(), help='Landau level to consider (default='+str(HD.ll())+').')
        parent_interation_parser.add_argument('--interaction',default=HD.interaction(), help='Interaction type, options currently are Delta, Coulomb or 3Body (default='+HD.interaction()+'). The Coulomb potential is V(k)=2*pi/k <=> V(r)=1/r.')
        parent_interation_parser.add_argument('--pseudopotentials', type=float, nargs='+', help='List of pseudopotentials starting from V_0. Any not specified are automatically set to zero.')
        parent_interation_parser.add_argument('--add-three-body', type=float, default=HD.ThreeBodyStength(), help='Add three body interaction with specified coefficient (default='+str(HD.ThreeBodyStength())+').')
        return parent_interation_parser


    def GeometryParser(self):
        """
        Flags related to the geometry of the torus.
        """
        parent_geometry_parser=argparse.ArgumentParser(add_help=False)
        parent_geometry_parser.add_argument('--tau1','-t1',type=float,default=HD.tau1(), help='Value of tau1, real component of tau (default='+str(HD.tau1())+').')
        parent_geometry_parser.add_argument('--tau2','-t2',type=float,default=HD.tau2(), help='Value of tau2, imaginary component of tau (default='+str(HD.tau2())+').')
        parent_geometry_parser.add_argument('--magnetic-length',type=float,default=HD.magnetic_length(), help='The magnetic length scale (default='+str(HD.magnetic_length())+').')
        return parent_geometry_parser

    def BasisParser(self):
        """
        Flags related to the size of the basis being built.
        """
        parent_basis_parser=argparse.ArgumentParser(add_help=False)
        parent_basis_parser.add_argument('-Ne','-p','--particles',type=int,default=HD.Ne(), help='Number of particles to consider (default='+str(HD.Ne())+').')
        parent_basis_parser.add_argument('-Ns','-l','--nbrflux',type=int,default=HD.Ns(), help='Number of flux quanta to consider (default='+str(HD.Ns())+').')
        parent_basis_parser.add_argument('-K1','-y','--momentum',type=int,default=HD.K1(), help='Total momentum sector to consider (default='+str(HD.K1())+').')
        parent_basis_parser.add_argument('-K2','-c','--commomentum',type=int,default=HD.K2(), help='Centre of mass momentum sector or -1 to not use this symmetry (default='+str(HD.K2())+').')
        parent_basis_parser.add_argument('-i','--inversion-sector', type=int, default=HD.Inversion(), help='Inversion sector to use for diagonalization 0-1 or -1 to not use this symmetry (default='+str(HD.Inversion())+'). In order to use Inversion sector symmetries, also Centre of mass momentum symmetries must be specified')
        parent_basis_parser.add_argument('--bosonic',action='store_true', help='Flag to indicate that calculations should be performed with bosonic statistics rather than the default fermionic.')
        parent_basis_parser.add_argument('--exempt-cont-orbitals', type=int, default=-1, help='Number of consecutive filled or empty orbitals for which a configuration is exempted (default=-1).')
        return parent_basis_parser


    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.ArgVals = ArgVals
        self.Particles = ArgVals.particles
        self.NbrFlux = ArgVals.nbrflux
        self.Momentum = ArgVals.momentum % self.NbrFlux # only valid up to modulo NbrFlux
        self.COMMomentum = ArgVals.commomentum
        self.LL = ArgVals.landau_level
        self.Tau = np.complex(ArgVals.tau1, ArgVals.tau2)
        if ArgVals.tau2 <= 0.0:
            print("Tau2 (imaginary part of tau) must be positive and nonzero. Exiting...")
            sys.exit(1)
        self.MagneticLength = ArgVals.magnetic_length
        self.SaveStates = ArgVals.eigenstate
        self.NbrEigvals = ArgVals.nbr_eigvals
        self.NbrStates = ArgVals.nbr_states
        if self.SaveStates:
            ###Need at least as many eigenvalues as requested states
            ###But only if the flag to wriote to disc is activated
            self.NbrEigvals=max(self.NbrEigvals,self.NbrStates)
        else:
            ###If not states are gonig to be saved. Set state request to zero.
            self.NbrStates=0
        if not(self.NbrEigvals>0 or self.NbrStates>0): 
            raise ValueError("At least one of --nbr-eigvals="+str(self.NbrEigvals)+" and --nbr-states="+str(self.NbrStates)+" but be positive!")
        self.EPSTol = ArgVals.eps_tol
        self.SaveHamiltonian = ArgVals.save_hamiltonian
        self.WriteCoefficients = ArgVals.write_coefficients
        self.KeepReal = ArgVals.real
        self.MPMath = ArgVals.mpmath
        self.CoefficientsOnly = ArgVals.coefficients_only
        self.ShowMatrix = ArgVals.show_matrix
        self.MatrixInfo = ArgVals.matrix_info
        self.Interaction = ArgVals.interaction
        if self.Interaction == 'Coulomb' and self.LL > 1:
            print("Only LL= 0 or LL=1 (lowest two Landau levels) supported for Coulomb. Exiting...")
            sys.exit(1)
        elif self.Interaction == 'Delta' and self.LL > 1:
            print("Only LL= 0 or LL=1 (lowest two Landau levels) supported for Delta. Exiting...")
            sys.exit(1)
        elif self.Interaction == '3Body' and self.LL > 0:
            print("Only LL= 0 is supported for 3Body. Exiting...")
            sys.exit(1)
        self.SaveASCII = ArgVals.save_ascii
        self.Bosonic = ArgVals.bosonic
        self.Prefix = ArgVals.prefix
        self.Shift = ArgVals.shift
        self.State = ArgVals.state
        self.BasisFile = ArgVals.basis
        self.PseudoPotentials = ArgVals.pseudopotentials
        self.ID = ArgVals.id
        self.MaxIterations = ArgVals.max_iterations
        self.Thickness = ArgVals.thickness
        self.AppendPseudoPotentials = ArgVals.append_pseudopotentials
        self.Lx = ArgVals.lx
        self.WriteBasisFlag = ArgVals.no_basis_write
        self.OutputFockCoefficients = ArgVals.output_fock_coefficients
        self.AddThreeBody = ArgVals.add_three_body
        self.InversionSector = ArgVals.inversion_sector
        self.ExemptContOrbitals = ArgVals.exempt_cont_orbitals
        self.Silent = ArgVals.silent
        self.CleanExit = False

        if self.Lx > 0.0: # assume right angled torus
            self.Tau = np.complex(0.0, 2.0 * np.pi * np.float(self.NbrFlux) / (self.Lx**2))

        if ArgVals.initial_state != '':
            self.InitialState = vectorutils.ReadBinaryPETScVector(ArgVals.initial_state)
        else:
            self.InitialState = None

        # Some options that are not set via the command line but from calling scripts.
        # Mostly to do with python objects that are sent and returned.
        # Objects set to none or here and/or default values given for flags.
        self.ReturnEigenValues = False
        self.ReturnEigenVectors = False
        self.ReturnBasis = False
        self.ReturnMatrix = False
        self.PreallocatedMatrix = None
        self.WriteEnergies = False
        # If this is provided then the diagonalisation will take place
        #in the subspace specified by these vectors.
        self.SubSpaceVectors = None

        ###Test that there are no unknown options
        if(len(self.Unknown)>1): ###First unknown unknown option is the filename
            print(("There are unknown options (the first is always present)"))
            print((self.Unknown))
            print(("Please try again. See the help for a list of valid options"))
            sys.exit(1)


    def getEnergiesFilename(self):
        """
        Method which returns the filename that will be used to store the calculated energies.
        """
        return FileName.TorusEnergiesFilename(self.Particles,self.NbrFlux,self.Momentum,
                                              self.Tau.real,self.Tau.imag,self.MagneticLength,
                                              self.LL,self.Interaction)


    def getVectorFilename(self, State):
        """
        Method which returns the filename that will be used to store the calculated state.
        """
        return FileName.TorusVectorFilename(self.Particles,self.NbrFlux,self.Momentum,
                                            self.Tau.real,self.Tau.imag,self.MagneticLength,
                                            self.LL,self.Interaction,State)


    def writeRunDetailsStart(self):
        """
        Method log the details of a run to a file at the start.
        """
        now = time.gmtime()
        self.LogFileName = 'RunLog_' + str(now.tm_year) + str(now.tm_mon).zfill(2) + str(now.tm_mday).zfill(2) + str(now.tm_hour).zfill(2) + str(now.tm_min).zfill(2) + '.log'
        f = open(self.LogFileName, 'w')
        f.write(str(time.ctime()) +  ' Starting job with paramemters:\n')
        f.write('-----------------------------------------\n')
        for key in list(self.ArgVals.__dict__.keys()):
            f.write(str(key) + ': ' + str(self.ArgVals.__dict__[key]) + '\n')
        f.close()


    def writeRunDetailsEnd(self):
        """
        Method log the details of a run to a file at the start.
        """
        if self.LogFileName is not None:
            f = open(self.LogFileName, 'a')
            f.write(str(time.ctime()) + ' Finised running job.\n')
            f.close()
