#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" utils_timer.pyx: Cython routine usefull when computing the time a prodecure will take

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2017"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"
"""

import time

cdef class timer:
    """
    This class represents a timer, that keeps track of how many steps have been taken, and the corresponding time.
    """

    cdef double StartWallTime,LastWallTime
    cdef int steps,min_len,hr_len,sec_len
    cdef str dimention_el,dimention_le

    def __init__(self):
        """
        Construcor. Set the clock and such things
        """
        self.StartWallTime=time.time()
        self.LastWallTime=0.0
        self.steps=0
        self.dimention_el=" sec"
        self.dimention_le="  hr"
        self.min_len=60
        self.hr_len=3600
        self.sec_len=1


    def write_progress(self,int Nr,int NSamples):
        steps_el=self.sec_len
        steps_le=self.hr_len
    
        
        EndWallTime=time.time()

        #print
        #print 'Clock Time:',EndWallTime
    
        #print 'Last Wall Time:',self.LastWallTime
    
        TimeWallElapsed = EndWallTime - self.StartWallTime  
        #print 'Wall Time Elapsed:',TimeWallElapsed
        if TimeWallElapsed > self.LastWallTime+self.steps :
       
           ##Computes samples left
           SamplesLeft = NSamples - Nr
           ##Compute time left
           TimeWallLeft = ((SamplesLeft*1.0)/(1.0*Nr))*(TimeWallElapsed*1.0)

           ##Step up the time as the elapsed time goes upp
           if (TimeWallElapsed>self.min_len):
               #print 'set min up'
               self.dimention_el=" min"
               steps_el=self.min_len
           if (TimeWallElapsed>self.hr_len):
               #print 'set hr up'
               self.dimention_el="  hr"
               steps_el=self.hr_len
            
            ##Step down the time as the time left ges down
           if(TimeWallLeft<self.hr_len):
               #print 'set min down'
               self.dimention_le=" min"
               steps_le=self.min_len
           if(TimeWallLeft<self.min_len):
               #print 'set sec down'
               self.dimention_le=" sec"
               steps_le=self.sec_len

           if self.dimention_el==" sec":
              TimeElFmt=TimeWallElapsed
           elif self.dimention_el==" min":
              TimeElFmt=((1.0*TimeWallElapsed)/self.min_len)
           elif self.dimention_el=="  hr":
              TimeElFmt=((1.0*TimeWallElapsed)/self.hr_len)
           else:
              print 'unknown dimention: set to sec'
              TimeElFmt=TimeWallElapsed
              dimention_el=" sec"
              steps_el=self.sec_len

           if self.dimention_le==" sec":
               TimeLeFmt=TimeWallLeft
           elif self.dimention_le==" min":
              TimeLeFmt=((1.0*TimeWallLeft)/self.min_len)
           elif self.dimention_le=="  hr":
              TimeLeFmt=((1.0*TimeWallLeft)/self.hr_len)
           else:
               print 'unknown dimention: set to sec'
               TimeLeFmt=TimeWallLeft
               self.dimention_le=" sec"
               steps_le=self.sec_len
           self.LastWallTime = TimeWallElapsed
           self.steps=min(steps_el,steps_le)
           print Nr, 'done', SamplesLeft,'to go. Time:',int(TimeElFmt),self.dimention_el,'Time left:',int(TimeLeFmt),self.dimention_le,'of',int(TimeWallElapsed+TimeWallLeft)," sec"
           
