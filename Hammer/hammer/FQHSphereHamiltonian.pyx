#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHSphereHamiltonian.pyx: This is a cython implementation construct Hamiltonian matrices for FQH systems on tori. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
cimport numpy as np
import time
from mpi4py import MPI
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free

try:
    HaveSciPy = True
    import scipy as sp
    import scipy.sparse as sparse
except:
    HaveSciPy = False

try:
    from petsc4py import PETSc
    from slepc4py import SLEPc
    HaveSlepcLibs = True
except ImportError:
    HaveSlepcLibs = False

class FQHSphereHamiltonian:
    """
    Class to group methods and data structures for dealing constructing Hamiltonians for FQH systems on Tori
    """


    def __init__(self, MyBasis, Coefficients, KeepReal=False):
        """
        Constructor to setup class
        """
        self.MyBasis = MyBasis
        self.Coefficients = Coefficients
        self.Dim = MyBasis.GetDimension()
        self.KeepReal = KeepReal


    def CreateSlepcHamiltonian(self, Ham, Assemblies=10):
        """
        Method to create and populate the PETSc matrix data structure.
        """
        PetscPrint = PETSc.Sys.Print
        Coefficients = self.Coefficients

        # Define c datatypes for coefficients to optimise loops
        cdef int rstart, rend, LocalDim, i, j, k, Col, Row, Dim
        cdef double complex Phase1, Phase2
        cdef int NumPairs, NumDensity
        cdef int *J3Vals
        cdef int *J4Vals
        cdef int **J1Vals
        cdef int **J2Vals
        cdef int *NonZeros
        cdef double complex **SymCoefs
        cdef double nearly_zero = np.finfo(PETSc.ScalarType).eps**2
        cdef int assembly_interval
        cdef int assembly_count
        cdef int *nnz_buffer
        cdef np.ndarray[np.int32_t, ndim=1] Py_dnnz, Py_odnnz
        cdef int *N1Vals
        cdef int *N2Vals
        cdef double complex *DensityCoefs
        cdef double complex DiagonalConstant

        # Allocate memory for local copies of interaction coefficients
        Dim = self.Dim
        NumPairs = Coefficients.NumPairs
        NumDensity = Coefficients.NumDensity
        J3Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J4Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J1Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        J2Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        NonZeros = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        SymCoefs = <double complex**>PyMem_Malloc(NumPairs * sizeof(double complex*))
        N1Vals = <int *>PyMem_Malloc(NumDensity * sizeof(int))
        N2Vals = <int *>PyMem_Malloc(NumDensity * sizeof(int))
        DensityCoefs = <double complex *>PyMem_Malloc(NumDensity * sizeof(double complex))
        DiagonalConstant = Coefficients.Diag
        j = 0
        while j < NumPairs:
            NonZeros[j] = Coefficients.NonZeros[j]
            J3Vals[j] = Coefficients.J3J4Vals[j,0]
            J4Vals[j] = Coefficients.J3J4Vals[j,1]
            J1Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            J2Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            SymCoefs[j] = <double complex*>PyMem_Malloc(NonZeros[j] * sizeof(double complex))
            k = 0
            while k < NonZeros[j]:
                J1Vals[j][k] = Coefficients.J1J2Vals[j][k,0]
                J2Vals[j][k] = Coefficients.J1J2Vals[j][k,1]
                SymCoefs[j][k] = Coefficients.AntiSymCoefs[j][k]
                k += 1
            j += 1

        j = 0
        while j < NumDensity:
            N1Vals[j] = Coefficients.N1N2Vals[j][0]
            N2Vals[j] = Coefficients.N1N2Vals[j][1]
            DensityCoefs[j] = Coefficients.DensityCoefs[j]
            j += 1
        # end of assigning c datatypes

        if Ham is None:
            # Creating temporary matrix to get the local dimension as cannot preallocate after doing setUp and cannot
            # get the local dimension without calling setUp.
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([Dim, Dim])
            Ham.setFromOptions()
            Ham.setUp()
            rstart, rend = Ham.getOwnershipRange()
            Ham.destroy()

            # Create final matrix this time
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([Dim, Dim])
            Ham.setFromOptions()

            LocalDim = rend - rstart # dimension of local portion of matrix, by row

            # Create python objects for these to pass to Preallocation function
            Py_dnnz = np.ones(LocalDim, dtype=np.int32)  # Diagonal non zeros
            Py_odnnz = np.zeros(LocalDim, dtype=np.int32) # off diagonal non zeros
            j = 0
            local_elements = np.array(0, dtype=np.int64)
            global_elements = np.array(0, dtype=np.int64)

            # Create C arrays to count the number of non zero entries for each row, distinguish by those that are in the diagonal block and those that are not.
            nnz_buffer = <int *>PyMem_Malloc(Dim * sizeof(int)) # Allocate space for a buffer to count non zero column entries
            j = 0
            while j < Dim:
                nnz_buffer[j] = -1
                j += 1

            start = time.time()
            Row = rstart
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j][k], J2Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0 and Col < Dim:
                                    if nnz_buffer[Col] < Row: # otherwise, this column has already been counted.
                                        if Col >= rstart and Col < rend:
                                            #if Col != Row : dnnz[Row-rstart] += 1
                                            if Col != Row : Py_dnnz[Row-rstart] += 1
                                        else:
                                            Py_odnnz[Row-rstart] += 1
                                        nnz_buffer[Col] = Row # gives last row where this column was occupied
                            k += 1
                    j += 1

                local_elements += <long>(Py_dnnz[Row-rstart] + Py_odnnz[Row-rstart])
                Row += 1

            PETSc.COMM_WORLD.tompi4py().Allreduce([local_elements, MPI.LONG], [global_elements, MPI.LONG], op=MPI.SUM)

            Ham.setPreallocationNNZ((Py_dnnz,Py_odnnz))
            Ham.setUp()
            PyMem_Free(nnz_buffer)
            end = time.time()
            PetscPrint("Preallocation: %.3gs (for %ld non zero values with local count %ld)" % (end-start, global_elements, local_elements))
        else:
            start = time.time()
            rstart, rend = Ham.getOwnershipRange()
            Ham.zeroEntries()
            end = time.time()
            PetscPrint("Preallocation (only clearing): %.3gs" % (end-start))


        assembly_intervals = int((rend - rstart) / Assemblies)
        assembly_count = 0
        start = time.time()
        Row = rstart
        if PETSc.ScalarType == np.complex128:
            while Row < rend:
                Ham.setValue(Row, Row, DiagonalConstant, True)
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j][k], J2Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1 * Phase2 * SymCoefs[j][k], True)
                            k += 1
                    j += 1

                # now look at density density terms
                j = 0
                while j < NumDensity:
                    Phase1 = self.MyBasis.NN( N1Vals[j], N2Vals[j], Row)
                    if Phase1 != 0.0:
                        Ham.setValue(Row, Row, Phase1 * DensityCoefs[j], True)
                    j += 1

                if (Row - rstart) > Row and (Row - rstart) % assembly_intervals == 0 and assembly_count < (Assemblies - 1):
                    Ham.assemble(assembly=PETSc.Mat.AssemblyType.FLUSH)
                    assembly_count += 1
                Row += 1
        else:
            while Row < rend:
                Ham.setValue(Row, Row, DiagonalConstant.real, True)
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j][k], J2Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1.real * Phase2.real * SymCoefs[j][k].real, True)
                            k += 1
                    j += 1

                # now look at density density terms
                j = 0
                while j < NumDensity:
                    Phase1 = self.MyBasis.NN( N1Vals[j], N2Vals[j], Row)
                    #Phase1 = self.MyBasis.AA( N1Vals[j], N2Vals[j], Row)
                    if Phase1 != 0.0:
                        Ham.setValue(Row, Row, Phase1.real * DensityCoefs[j].real, True)
                    j += 1

                if (Row - rstart) > Row and (Row - rstart) % assembly_intervals == 0 and assembly_count < (Assemblies - 1):
                    Ham.assemble(assembly=PETSc.Mat.AssemblyType.FLUSH)
                    assembly_count += 1
                Row += 1

        # keep aseembling until everyone is finished.
        while assembly_count < (Assemblies -1):
            Ham.assemble(assembly=PETSc.Mat.AssemblyType.FLUSH)
            assembly_count += 1
        Ham.assemble(assembly=PETSc.Mat.AssemblyType.FINAL)

        # Deallocate space used for c datatypes during preallocation.
        PyMem_Free(J3Vals)
        PyMem_Free(J4Vals)
        j = 0
        while j < NumPairs:
            PyMem_Free(J1Vals[j])
            PyMem_Free(J2Vals[j])
            PyMem_Free(SymCoefs[j])
            j += 1
        PyMem_Free(NonZeros)
        PyMem_Free(J1Vals)
        PyMem_Free(J2Vals)
        PyMem_Free(SymCoefs)
        # Finished deallocating space

        end = time.time()
        PetscPrint("Construction: %.3gs." % (end-start))

        return Ham


    def mult(self, mat, X, Y):
        """
        Multiply operation, for use with petsc vector data types.

        Parameters
        ----------
        mat: na
            No used in example so unsure of its use.
        X: petsc vec
            Vector to multiply
        Y: petsc vec
            Result of matrix vector multiplication operation.
        """
        # TODO: write function...
        # outline is to operate on local part of x
        # then communicate the results to full system
