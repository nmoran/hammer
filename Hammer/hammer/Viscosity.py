#! /usr/bin/env python

""" Viscosity.py: Utility functions for interfacing with vectors returned by Hammer. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


from hammer.TorusDiagonalise import *
from hammer.HammerArgs import HammerArgsParser
from hammer.utils import *
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusProductWF import BosonicProductWF
import hammer.vectorutils as vectorutils
import numpy as np
import numpy.linalg as linalg
import sys
import argparse
import petsc4py.PETSc as PETSc
import csv
import itertools
import pickle


def __main__(argv):
    """
    Entry function if script is called from command line.
    """
    # Run the argument parser and process command line arguments.
    ViscosityOpts = ViscosityArgsParser()
    ViscosityOpts.parseArgs(argv)
    Opts = HammerArgsParser()
    Opts.parseArgs(ViscosityOpts.Unknown)
    Viscosity(Opts, ViscosityOpts)


def Viscosity(Opts, ViscosityOpts):
    """
    Main viscosity function.
    """
    # This print is to be used when working with many processes as will ensure that statement is only printed once.
    PetscPrint = PETSc.Sys.Print
    MyRank = PETSc.COMM_WORLD.getRank()

    if PETSc.ScalarType == np.float64:
        PetscPrint("Viscosity calculation requires support for complex numbers.")
        return

    if Opts.ID == '':
        if Opts.PseudoPotentials is not None:
            Opts.ID = 'PseudoPotential'
        else:
            Opts.ID = "LL" + str(Opts.LL) + Opts.Interaction
        if np.abs(Opts.AddThreeBody) > 0.0:
            Opts.ID = Opts.ID + '_3B_' + str(Opts.AddThreeBody)

    Radius = ViscosityOpts.Radius
    Centre = ViscosityOpts.Centre
    Steps = ViscosityOpts.Steps
    TorusDetailsString = ("_p_" + str(Opts.Particles) + "_Ns_" + str(Opts.NbrFlux) + "_K_" + str(Opts.Momentum) +
                         ('' if Opts.COMMomentum == -1 else '_P_' + str(Opts.COMMomentum)) +
                         ('' if Opts.InversionSector == -1 else '_I_' + str(Opts.InversionSector)))
    MyPrefix = "Point" + TorusDetailsString + "_r_" + '{:.5f}'.format(Radius) + "_cr_" + '{:.3f}'.format(Centre.real) + "_ci_" + '{:.3f}'.format(Centre.imag)
    LogFiles = list()
    LastVectors = list()
    for i in range(Opts.NbrStates):
        LogFiles.append("ViscosityLog_interaction_" + (str(Opts.Interaction) if Opts.ID == '' else Opts.ID) + TorusDetailsString + "_r_" + '{:.5f}'.format(Radius)
                        + "_cr_" + '{:.3f}'.format(Centre.real) + "_ci_" + '{:.3f}'.format(Centre.imag) +  "_state_" + str(i) + ".csv")
        LastVectors.append("Checkpoint_interaction_" + (str(Opts.Interaction) if Opts.ID == '' else Opts.ID) + TorusDetailsString + "_r_" + '{:.5f}'.format(Radius)
                        + "_cr_" + '{:.3f}'.format(Centre.real) + "_ci_" + '{:.3f}'.format(Centre.imag) +  "_state_" + str(i) + ".vec")

    #Angles = np.arange(0.0, 2.0*np.pi, 2.0*np.pi/Steps)
    RealRadius = Radius * np.abs(Centre)
    InvariantArea = 2.0*np.pi*(1.0/np.sqrt(1.0-(RealRadius/Centre.imag)**2) - 1.0)

    #Opts.Tau = Centre + (RealRadius * np.exp(np.complex(0.0, Angles[0])))
    Opts.Tau = Centre + (RealRadius * np.exp(np.complex(0.0, 0.0)))
    Opts.SaveStates = False
    Opts.ReturnEigenValues = True
    Opts.ReturnEigenVectors = True
    Opts.ReturnMatrix = True
    Opts.WriteEnergies = False
    Opts.Prefix = MyPrefix + "_" +  str(0)
    NbrStates = Opts.NbrStates
    NbrParticles = Opts.Particles

    myoverlaps = list()
    myproducts = list()

    if ViscosityOpts.ProductMomentums is None:
        MyBasis = BuildBasis(Opts)
        if ViscosityOpts.Degeneracies is None:
            ViscosityOpts.Degeneracies = np.ones(NbrStates, dtype=np.int32)
        else:
            NbrStates = np.sum(ViscosityOpts.Degeneracies)
            Opts.NbrStates = NbrStates
    else:
        Opts.Bosonic = True
        NbrProductFactors = len(ViscosityOpts.ProductMomentums)
        MyBasis = FQHTorusBasisBosonic(NbrProductFactors*Opts.Particles, Opts.NbrFlux, np.sum(ViscosityOpts.ProductMomentums)%Opts.NbrFlux)
        NbrParticles = len(ViscosityOpts.ProductMomentums) * Opts.Particles
        if ViscosityOpts.ProductMomentums2 is not None:
            ViscosityOpts.Degeneracies = np.array([2])
            NbrStates = 2
        else:
            ViscosityOpts.Degeneracies = np.array([1])
            NbrStates = 1

    #If subspace dimension is specified, then get the subspace first
    if ViscosityOpts.SubSpaceDimension > 0:
        Opts.SubSpaceVectors = GetSubSpace(Opts, ViscosityOpts.SubSpaceDimension, MyBasis)

    NbrSpaces = len(ViscosityOpts.Degeneracies)
    MaxIndices = np.array([np.zeros(x, dtype=np.int64) for x in ViscosityOpts.Degeneracies])
    Products = [np.matrix(np.eye(x, dtype=np.complex128)) for x in ViscosityOpts.Degeneracies]
    Overlaps = [np.matrix(np.zeros((x,x), dtype=np.complex128)) for x in ViscosityOpts.Degeneracies]
    BerryPhases = [np.matrix(np.zeros(x, dtype=np.float64)) for x in ViscosityOpts.Degeneracies]
    Viscosities = [np.matrix(np.zeros(x, dtype=np.float64)) for x in ViscosityOpts.Degeneracies]
    Data = dict()
    InitialStates = list()
    Vecs1 = list() # Create list to store retrieved vectors of interest.

    # In case we are resuming, will try to read the specified vector and log file here.
    if (ViscosityOpts.ResumeLog != '') and (ViscosityOpts.ResumeVector != ''):
        Opts.InitialState = list()
        for i in range(NbrStates):
            ResumeLog = ReadCSVFile(ViscosityOpts.ResumeLog + '_state_' + str(i) + '.csv')
            ResumeVector = vectorutils.ReadBinaryPETScVector(ViscosityOpts.ResumeVector + '_state_' + str(i) + '.vec')
            MaxIndices[i] = np.int(ResumeLog[-1][2])
            Products[i] = np.complex(ResumeLog[-1][1])
            BerryPhases[i] = np.float(ResumeLog[-1][3])
            Viscosities[i] = np.float(ResumeLog[-1][4])
            if i == 0 : Step = np.int(ResumeLog[-1][0]) + 1 # incrememt step by one.
            Vecs1.append(ResumeVector) # only implement resume for non-degenerate spaces.
            Opts.InitialState.append(ResumeVector.copy()) # start from ground state
    else:
        PetscPrint('\nStep 0:\n---------------\n')
        #Data = Diagonalise(Opts, BasisObj=MyBasis) # Do first diagonalisation.
        Data = GetNextStates(Opts, ViscosityOpts, BasisObj=MyBasis) # Do first diagonalisation.
        Opts.InitialState = Data['EigenVectors'][0].copy()
        for i in range(NbrStates):
            Vecs1.append(Data['EigenVectors'][i])
            InitialStates.append(Vecs1[i].copy())
            # to save space we now use the original vecs to get absolute value and such.

            #Vecs1Abs = Vecs1[i].copy()
            #Vecs1Abs.abs()
            #[MaxIndices[i],MaxValAbs] = Vecs1Abs.max()
            #MaxVal = vectorutils.GetGlobalValue(Vecs1[i], MaxIndices[i])
            #ScaleVal = np.exp(np.complex(0.0, -np.angle(MaxVal)))
            #Vecs1[i].scale(ScaleVal)
            #Vecs1Abs.destroy()

        if MyRank == 0:
            for i in range(NbrSpaces):
                if sys.version_info[0] == 3:
                    f = open(LogFiles[i], "w", encoding='utf8', newline='') # python 3 version
                elif sys.version_info[0] == 2:
                    f = open(LogFiles[i],"wb")
                csv_writer = csv.writer(f)
                csv_writer.writerow(["Step", "Product", "Overlap", "BerryPhase", "Viscosity"])
                csv_writer.writerow([0, str(Products[i]).replace('\n', ' '), 0, BerryPhases[i], Viscosities[i]])
                f.close()

        if ViscosityOpts.SaveStates:
            vectorutils.writeBinaryPETScFile(Vecs1[i], LastVectors[i])

        Step = 1 # Next step

    OriginalStepSize = 2.0 * np.pi / np.float(Steps)
    StepSize = OriginalStepSize
    Angle = 0.0 + StepSize
    LastStep = 0.0
    if Steps > ViscosityOpts.MaximumSteps : ViscosityOpts.MaximumSteps = Steps
    while Angle < (2.0 * np.pi) and Steps < ViscosityOpts.MaximumSteps:
        GoBack = False
        PetscPrint('\nStep ' + str(Step) + ':\n---------------\n')
        #Opts.Tau = Centre + (RealRadius * np.exp(np.complex(0.0, Angles[i])))
        Opts.Tau = Centre + (RealRadius * np.exp(np.complex(0.0, Angle)))
        if 'Matrix' in Data:
            Opts.PreallocatedMatrix = Data['Matrix']
        Opts.Prefix = MyPrefix + "_" +  str(i)
        #Data = Diagonalise(Opts, BasisObj=MyBasis)
        Data = GetNextStates(Opts, ViscosityOpts, BasisObj=MyBasis) # Do first diagonalisation.
        Opts.InitialState = list()
        for j in range(len(Data['EigenVectors'])):
            Opts.InitialState.append(Data['EigenVectors'][j])
        #Opts.InitialState = Data['EigenVectors'][0]

        Vecs2 = list()
        for j in range(NbrStates):
            Vecs2.append(Data['EigenVectors'][j])
        #Vecs2 = DiagonaliseByOverlap(Vecs1, Vecs2, ViscosityOpts.Verbose)
        for j in range(NbrSpaces):
            for k in itertools.product(list(range(ViscosityOpts.Degeneracies[j])), list(range(ViscosityOpts.Degeneracies[j]))):
                Overlaps[j][k[0], k[1]] = vectorutils.Overlap(Vecs1[np.int(np.sum(ViscosityOpts.Degeneracies[:j])) + k[0]], Vecs2[np.int(np.sum(ViscosityOpts.Degeneracies[:j])) + k[1]])

            if ViscosityOpts.AdaptiveStep > 0.0 and np.linalg.norm(np.eye(Overlaps[j].shape[0]) - (Overlaps[j] * Overlaps[j].conj().transpose())) > ViscosityOpts.AdaptiveStep: # if our overlap matrix is not unitary enough.
                print((str(np.linalg.norm(np.eye(Overlaps[j].shape[0]) - (Overlaps[j] * Overlaps[j].conj().transpose()))) + ' too small!!'))
                StepSize = StepSize/2.0
                Angle = Angle - StepSize
                GoBack = True
                continue

        if GoBack:
            continue

        for j in range(NbrSpaces):
            Products[j] = Products[j] * Overlaps[j]
            for k in range(ViscosityOpts.Degeneracies[j]):
                Vecs1[np.int(np.sum(ViscosityOpts.Degeneracies[:j]))+k] = Vecs2[np.int(np.sum(ViscosityOpts.Degeneracies[:j]))+k]
            BerryPhases[j] = np.angle(np.linalg.eig(Products[j])[0])/InvariantArea
            Viscosities[j] = (2.0 * BerryPhases[j] / np.float(NbrParticles)) + 0.5
            if MyRank == 0:
                if sys.version_info[0] == 3:
                    f = open(LogFiles[i], "a", encoding='utf8', newline='') # python 3 version
                elif sys.version_info[0] == 2:
                    f = open(LogFiles[i],"ab")
                csv_writer = csv.writer(f)
                csv_writer.writerow([Step, str(Products[j]).replace('\n', ' '), str(Overlaps[j]).replace('\n', ' '), BerryPhases[j], Viscosities[j]])
                f.close()
            if ViscosityOpts.SaveStates:
                for k in range(ViscosityOpts.Degeneracies[j]):
                    vectorutils.writeBinaryPETScFile(Vecs1[np.int(np.sum(ViscosityOpts.Degeneracies[:j]))+k], LastVectors[np.int(np.sum(ViscosityOpts.Degeneracies[:j]))+k])

            myoverlaps.append(Overlaps[j].copy())
            myproducts.append(Products[j].copy())
            PetscPrint (str(j) + ": Overlap: " + str(Overlaps[j]).replace('\n', ' '))
            PetscPrint (str(j) + ": Product: " + str(Products[j]).replace('\n', ' ') + " arg: " + str(np.angle(np.trace(Products[j]))) + " abs: " + str(np.abs(np.trace(Products[j]))))
            PetscPrint (str(j) + ": BerryPhase: " + str(BerryPhases[j]))
            PetscPrint (str(j) + ": Viscosity: " + str(Viscosities[j]))

        Step += 1
        LastAngle = Angle
        Angle = Angle + StepSize
        if StepSize < OriginalStepSize:
            StepSize = OriginalStepSize

    PetscPrint('\nFinal step\n---------------\n')
    for j in range(NbrSpaces):
        #ScaleVal = np.exp(np.complex(0.0, -np.angle(vectorutils.GetGlobalValue(Vecs2[j], MaxIndices[j]))))
        #`Vecs2[j].scale(ScaleVal)
        for k in itertools.product(list(range(ViscosityOpts.Degeneracies[j])), list(range(ViscosityOpts.Degeneracies[j]))):
            Overlaps[j][k[0], k[1]] = vectorutils.Overlap(Vecs1[np.int(np.sum(ViscosityOpts.Degeneracies[:j])) + k[0]], InitialStates[np.int(np.sum(ViscosityOpts.Degeneracies[:j])) + k[1]])
        Products[j] = Products[j] * Overlaps[j]
        BerryPhases[j] = np.angle(np.linalg.eig(Products[j])[0])/InvariantArea
        Viscosities[j] = (2.0 * BerryPhases[j] / np.float(NbrParticles)) + 0.5
        if MyRank == 0:
            if sys.version_info[0] == 3:
                f = open(LogFiles[i], "a", encoding='utf8', newline='') # python 3 version
            elif sys.version_info[0] == 2:
                f = open(LogFiles[i], "a")
            csv_writer = csv.writer(f)
            csv_writer.writerow([Step, str(Products[j]).replace('\n', ' '), str(Overlaps[j]).replace('\n', ' '), BerryPhases[j], Viscosities[j]])
            f.close()

        myoverlaps.append(Overlaps[j])
        myproducts.append(Products[j])
        PetscPrint (str(j) + ": Overlap: " + str(Overlaps[j]).replace('\n', ' '))
        PetscPrint (str(j) + ": Product: " + str(Products[j]).replace('\n', ' ') + " arg: " + str(np.angle(np.trace(Products[j]))) + " abs: " + str(np.abs(np.trace(Products[j]))))
        PetscPrint (str(j) + ": BerryPhase: " + str(BerryPhases[j]))
        PetscPrint (str(j) + ": Unitarity: " + str(np.linalg.norm(np.eye(ViscosityOpts.Degeneracies[j]) - Products[j]*Products[j].conjugate().transpose())))
        PetscPrint (str(j) + ": Viscosity: " + str(Viscosities[j]))

    if MyRank == 0 and ViscosityOpts.Debug:
        pickle.dump(myoverlaps, open('overlaps.pickle','w'))
        pickle.dump(myproducts, open('products.pickle','w'))


    if MyRank == 0:
        fname = "Results_interaction_" + (str(Opts.Interaction) if Opts.ID == '' else Opts.ID) + TorusDetailsString + "_cr_" + '{:.3f}'.format(Centre.real) + "_ci_" + '{:.3f}'.format(Centre.imag) + "_nbrstates_" + str(NbrStates) + ".csv"
        if sys.version_info[0] == 3:
            f = open(fname, "w", encoding='utf8', newline='') # python 3 version
        elif sys.version_info[0] == 2:
            f = open(fname, "wb")
        csv_writer = csv.writer(f)
        for i in range(NbrSpaces):
            for j in range(ViscosityOpts.Degeneracies[i]):
                csv_writer.writerow([Viscosities[i][j], BerryPhases[i][j], str(Products[i]).replace('\n', ' '), InvariantArea, Steps, Radius, Centre.real, Centre.imag, i, ViscosityOpts.Degeneracies[i]])
        f.close()

    return Viscosities


class ViscosityArgsParser:
    """
    Class to parse command line arguments specific to viscosity calculation utility.

    TODO: Change to use dict datatype intenally or even to be a subclass of dict to make more extensible.
    """


    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(description='Calculate the hall viscosity for the provided torus parameters.')
        self.Parser.add_argument('--steps', type=int, default=200, help='The number of steps to use when computing the viscosity.')
        self.Parser.add_argument('--radius', type=float, default=0.005, help='Radius of area to sample from (relative to area centre).')
        self.Parser.add_argument('--save-states', action='store_true', help='Save the states to disk. Default states are not saved.')
        self.Parser.add_argument('--cr', type=float, default=0.0, help='Real component of centre.')
        self.Parser.add_argument('--ci', type=float, default=1.0, help='Imaginary component of centre.')
        self.Parser.add_argument('--verbose', action='store_true', help='This flag indicates whether or not to be verbose or not.')
        self.Parser.add_argument('--resume-log', default='', help='Log file to resume from, in the case that the calculation was interupted. Sould be the filename up to the _state_*.csv part as the rest will be filled in automatically.')
        self.Parser.add_argument('--resume-vector', default='', help='The last state to use when resuming. Expected is the filename up to the _state_*.vec part as the rest will be filled in automatically.')
        self.Parser.add_argument('--product-momentums', type=int, default=None, nargs='*', help='If provided, gives the momentums to use for each factor state of the product. Can be a list of two or three numbers from 0 to (NbrFlux - 1).')
        self.Parser.add_argument('--product-momentums2', type=int, default=None, nargs='*', help='If provided, gives the momentums to use for each factor state of a second product state. Can be a list of two or three numbers from 0 to (NbrFlux - 1).')
        self.Parser.add_argument('--degeneracies', type=int, default=None, nargs='*', help='If provided, specifies the degeneracy to consider for each of the states we are considering. Should sum to the number of states being considered. By default, will be one for each state (or two in the case where two bosonic product momentums are specified).')
        self.Parser.add_argument('--debug', action='store_true', help='If specified, debugging information will be output.')
        self.Parser.add_argument('--adaptive-step', type=float, default=0.0, help='The option specifies that an adaptive step size should be used to ensure the overlap is unitary up to the specified threshold (0 indicates fixed step size).')
        self.Parser.add_argument('--maximum-steps', type=int, default=1000, help='The maximum number of steps to use for adaptive step mode (default=1000).')
        self.Parser.add_argument('--subspace-dimension', type=int, default=0, help='The dimension of the low energy subspace to use for calculation, 0 full basis used (default 0).')



    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.Steps = ArgVals.steps
        self.Radius = ArgVals.radius
        self.SaveStates = ArgVals.save_states
        self.Centre = np.complex(ArgVals.cr, ArgVals.ci)
        self.Verbose = ArgVals.verbose
        self.ResumeLog = ArgVals.resume_log
        self.ResumeVector = ArgVals.resume_vector
        self.ProductMomentums = ArgVals.product_momentums
        self.ProductMomentums2 = ArgVals.product_momentums2
        self.Degeneracies = ArgVals.degeneracies
        self.Debug = ArgVals.debug
        self.AdaptiveStep = ArgVals.adaptive_step
        self.MaximumSteps = ArgVals.maximum_steps
        self.SubSpaceDimension = ArgVals.subspace_dimension


def GetNextStates(Opts, ViscosityOpts, BasisObj=None):
    if ViscosityOpts.ProductMomentums is None:
        return TorusDiagonalise(Opts, BasisObj=BasisObj) # Do first diagonalisation.
    else:
        # Get factor states from diagonalisation and multiply together.
        ReturnData = dict()
        Vectors = list()
        States = list()
        Bases = list()
        BasisIndices = dict() # To save recalculating the same basis keep a list of indices with identifying label.
        StateIndices = dict() # Same for states.

        ProductMomentums = [ViscosityOpts.ProductMomentums, ViscosityOpts.ProductMomentums2]
        for ProductMomentum in [x for x in ProductMomentums if x is not None]:
            LastMomentum = 0
            LastParticles = 0
            for i in range(1, len(ProductMomentum)):
                if i == 1:
                    Momentum1 = ProductMomentum[0]
                    Particles1 = Opts.Particles
                else:
                    Momentum1 = LastMomentum
                    Particles1 = LastParticles

                Momentum2 = ProductMomentum[i]
                Particles2 = Opts.Particles

                # now get the two states and the three bases
                if (Particles1, Opts.NbrFlux, Momentum1) not in BasisIndices:
                    BasisIndices[(Particles1, Opts.NbrFlux, Momentum1)] = len(Bases)
                    Basis = FQHTorusBasisBosonic(Particles1, Opts.NbrFlux, Momentum1)
                    Basis.GenerateBasis()
                    Bases.append(Basis)

                if (Particles2, Opts.NbrFlux, Momentum2) not in BasisIndices:
                    BasisIndices[(Particles2, Opts.NbrFlux, Momentum2)] = len(Bases)
                    Basis = FQHTorusBasisBosonic(Particles2, Opts.NbrFlux, Momentum2)
                    Basis.GenerateBasis()
                    Bases.append(Basis)

                LastParticles = Particles1 + Particles2
                LastMomentum = (Momentum1 + Momentum2) % Opts.NbrFlux
                if (LastParticles, Opts.NbrFlux, LastMomentum) not in BasisIndices:
                    BasisIndices[(LastParticles, Opts.NbrFlux, LastMomentum)] = len(Bases)
                    Basis = FQHTorusBasisBosonic(LastParticles, Opts.NbrFlux, LastMomentum)
                    Basis.GenerateBasis()
                    Bases.append(Basis)


                MyBases = list()
                MyBases.append(Bases[BasisIndices[(Particles1, Opts.NbrFlux, Momentum1)]])
                MyBases.append(Bases[BasisIndices[(Particles2, Opts.NbrFlux, Momentum2)]])

                MyStates = list()

                if i == 1:
                    if (Particles1, Opts.NbrFlux, Momentum1) not in StateIndices:
                        StateIndices[(Particles1, Opts.NbrFlux, Momentum1)] = len(States)
                        Opts.Particles = Particles1
                        Opts.Momentum = Momentum1
                        Opts.InitialState = None
                        Data = TorusDiagonalise(Opts, BasisObj=Bases[BasisIndices[(Particles1, Opts.NbrFlux, Momentum1)]])
                        States.append(Data['EigenVectors'][0])
                    MyStates.append(States[StateIndices[(Particles1, Opts.NbrFlux, Momentum1)]])
                else:
                    MyStates.append(LastState)

                if (Particles2, Opts.NbrFlux, Momentum2) not in StateIndices:
                    StateIndices[(Particles2, Opts.NbrFlux, Momentum2)] = len(States)
                    Opts.Particles = Particles2
                    Opts.Momentum = Momentum2
                    Opts.InitialState = None
                    Data = TorusDiagonalise(Opts, BasisObj=Bases[BasisIndices[(Particles2, Opts.NbrFlux, Momentum2)]])
                    States.append(Data['EigenVectors'][0])

                MyStates.append(States[StateIndices[(Particles2, Opts.NbrFlux, Momentum2)]])

                LastState = BosonicProductWF(MyStates, MyBases, Bases[BasisIndices[(LastParticles, Opts.NbrFlux, LastMomentum)]])

            Vectors.append(LastState)

#        if ViscosityOpts.ProductMomentums2 is not None:
#            States = list()
#            Bases = list()
#            for i in ViscosityOpts.ProductMomentums2:
#                Opts.Momentum = i
#                Opts.InitialState = None
#                Data = Diagonalise(Opts, BasisObj=None)
#                States.append(Data['EigenVectors'][0])
#                Bases.append(FQHTorusBasisBosonic(Opts.Particles, Opts.NbrFlux, i))
#            Vectors.append(BosonicProductWF(States, Bases, BasisObj))
#
        print((list(StateIndices.keys())))
        print((list(BasisIndices.keys())))
        print([Bases[x].GetDimension() for x in list(BasisIndices.values())])

        ReturnData['EigenVectors'] = vectorutils.Orthogonalise(Vectors, True)

        return ReturnData


def DiagonaliseByOverlap(Vecs1, Vecs2, Verbose=False):
    """
    Given two lists of vectors Vecs1 and Vecs2, this method tries to return
    a list of vectors, each of which is a linear super-position of the vectors in Vecs2, in
    such a way that the overlap matrix between the sets of vectors is diagonalised.
    """
    PetscPrint = PETSc.Sys.Print
    Dim = Vecs1.__len__()
    NewVecs = list()
    if Dim > 1 :
        A = np.zeros([Dim, Dim],dtype=np.complex128)
        for i in range(Dim):
            for j in range(Dim):
                A[i,j] = vectorutils.Overlap(Vecs1[i], Vecs2[j])
                if Verbose:
                    PetscPrint(str(i) + ', ' + str(j) + ': ' +  str(A[i,j]))
        [w, v] = linalg.eig(A)
        # Populate the list of rotated vectors
        for i in range(Dim):
            if Verbose:
                PetscPrint("Eigenvalue " + str(i) + ": " + str(w[i]))
            NewVec = Vecs2[i].duplicate()
            NewVec.zeroEntries()
            for j in range(Dim):
                if Verbose:
                    PetscPrint(str(v[j,i]) + ',')
                NewVec.axpy(v[j,i], Vecs2[j])
            if Verbose:
                PetscPrint('\n')
            NewVecs.append(NewVec)

        for i in range(Dim):
            Vec = Vecs2.pop()
            Vec.destroy()

        for i in range(Dim):
            Vecs2.append(NewVecs[i])

        if Verbose:
            PetscPrint('Quick check to see if we are sane.\n')
            for i in range(Dim):
                for j in range(Dim):
                    PetscPrint(str(i) + ', ' + str(j) + ': ' +  str(vectorutils.Overlap(Vecs2[i], Vecs2[j])))

    return Vecs2


def GetSubSpace(Opts, SubSpaceDimension, BasisObj=None):
    """
    Get the required number of eigenvectors.
    """
    TmpNbrStates = Opts.NbrStates
    TmpNbrEigvals = Opts.NbrEigvals
    Opts.NbrStates = SubSpaceDimension
    Opts.NbrEigvals = SubSpaceDimension
    Data = TorusDiagonalise(Opts, BasisObj=BasisObj) # Do first diagonalisation.
    if len(Data['EigenVectors']) < SubSpaceDimension:
        print(("Only " + str(len(Data['EigenVectors'])) + " converged."))
    assert(len(Data['EigenVectors']) >= SubSpaceDimension)
    Vecs = list()
    for i in range(SubSpaceDimension):
        Vecs.append(Data['EigenVectors'][i])

    Opts.NbrStates = TmpNbrStates
    Opts.NbrEigvals = TmpNbrEigvals
    return Vecs


if __name__ == '__main__':
    __main__(sys.argv)
