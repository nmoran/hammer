#! /usr/bin/env python

""" TauValue.py: Script the return the tau value of a certain range. Convenient for use with scripts to scan tau space. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
import argparse
import sys


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Utility to help calculate ranges of parameters in tau plane.')
    parser.add_argument('--number','-n',type=int,default=11,help='Number of data points in total.')
    parser.add_argument('--minexp','-f',type=float,default=-1.0,help='Miniumum exponent of tau.')
    parser.add_argument('--maxexp','-t',type=float,default=1.0,help='Maximum exponent of tau.')
    parser.add_argument('--point','-p',type=int,default=1,help='Index of desired point (0 to n-1).')

    [ArgVals, Unknowns] = parser.parse_known_args(sys.argv)

    NumPoints = ArgVals.number
    MinExponent = ArgVals.minexp
    MaxExponent = ArgVals.maxexp
    PointIdx = ArgVals.point

    print((str(TauValue(MinExponent, MaxExponent, NumPoints, PointIdx))))

def TauValue(MinExponent, MaxExponent, NumPoints, PointIdx):
    """
    Function tau2 value withint range.

    Parameters
    ----------
    MinExponent: float
        Minimum value of tau2 exponent.
    MaxExponent: float
        Maximum value of tau2 exponent.
    NumPoints: int
        The number of equally spaced points between min and max exponent values.
    PointIdx: int
        The index of the poitn to print.
    """
    StepSize = (MaxExponent - MinExponent) / np.float(NumPoints-1)
    Exponent = MinExponent + np.float(PointIdx) * StepSize

    return 10.0**Exponent
