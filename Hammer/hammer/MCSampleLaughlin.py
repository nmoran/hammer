#! /usr/bin/env python

""" MCSampleLaughlin.py: Script that samples the laughlin satate using the metropolis hasings algorithm and saves to hdf5"""

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2017"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"

import sys
import argparse
import numpy as np
import h5py
import time
from hammer.FQHLaughlin import FQHTorusLaughlinWF

def __main__():

    """
    Main driver function, if script called from the command line it will use the command line arguments as the vector filenames.
    """
    full_start = time.time()
    Parser = argparse.ArgumentParser(
        description='Utility to sample the Laughlin wave function')
    Parser.add_argument('-wfn', type=str, help='output file for wfn samples psi(z|tau)*U(z\')^-1.',default="wfn_hammer.hdf5")
    Parser.add_argument('-wgt', type=str, help='output file for wfn weights from with the distributions is drawn.',default="wgt.hdf5")
    Parser.add_argument('-pos', type=str, help='input file for (x,y) coordinates, z=x+tau*y.',default="pos.hdf5")
    Parser.add_argument('--scale',type=str,choices=['lin','log'],default='log',help='Type of scale {lin,log} on the wfn data (default=log).')
    Parser.add_argument('--pos-style',type=str,choices=['reduced','lattice'],default='reduced',help='Type of positional input on the wfn data (default=reduced). If chose is lattice - the progam will assume that the positions stem from a predefined lattice z=L*(m+tau*n)/Ns and will attempt to round the lattice numbers to the nearest such number. (default=reduced) ')
    Parser.add_argument('--qp-pos-x',type=float, help='X-position(s) of quasiholes',nargs="+")
    Parser.add_argument('--qp-pos-y',type=float, help='Y-position(s) of quasiholes',nargs="+")
    Parser.add_argument('--Electrons',"-Ne", type=int, help='Number of electrons')
    Parser.add_argument('--Samples',"-N", type=int, help='Number of samples')
    Parser.add_argument('--Momentum',"-K1", type=int, help='Momentum sector')
    Parser.add_argument('--Sample-at-Ns', type=int, help='Generate coordinates the do not lie on the naive Ns-grid but on a different one')
    Parser.add_argument('--Denominator',"-q", type=int, help='Momentum sector',default=3)
    Parser.add_argument('--Reuse',"-r", action="store_true", help='Reuse the coordiantes in the egive file')
    Parser.add_argument('--lattice-sample', action="store_true", help='Pre-sample the lattice. Default not done')
    Parser.add_argument('--tau1',"-R", type=float, help='Real part of tau. (default=0.0)',default=0.0)
    Parser.add_argument('--tau2',"-I", type=float, help='Imaginary part of tau. (default=1.0)',default=1.0)
    Parser.add_argument('-dl',"--sampling-gap", type=int, help='Number of steps (times Ne) between sampling.',default=1)
    Parser.add_argument('-th',"--thermalization-steps", type=int, help='Number of steps (times Ne) to use in thermalization.',default=500)

    Parser.add_argument('--seed',type=int, help='Seed so that random process can be reproduced. Default=None (seed from /dev/urandom)')


    Args = Parser.parse_args()

    Wgt_out=Args.wgt
    Wf_out=Args.wfn
    tau=Args.tau1+1j*Args.tau2
    CoordFile=Args.pos

    denom=Args.Denominator
    NSamples=Args.Samples
    Ne=Args.Electrons
    K1=Args.Momentum
    Logform=(Args.scale=="log")

    if Args.qp_pos_x != None or Args.qp_pos_y != None:
        print "We have quasiparticles"
        if Args.qp_pos_x == None and Args.qp_pos_x == None:
            raise Exception, "Only one of x or y is set for q-positons"
        else:
            if len(Args.qp_pos_x) == len(Args.qp_pos_y): ###Same numbers of inpus as well
                print "Args.qp_pos_x:",Args.qp_pos_x
                print "Args.qp_pos_y:",Args.qp_pos_y
                QPPos=np.zeros((len(Args.qp_pos_x)),dtype=np.complex128)
                QPPos[:]=np.array(Args.qp_pos_x)+1j*np.array(Args.qp_pos_y)
            else:
                raise Exception,"Differnt lengths on qp positons vectors"
    else:
        QPPos=None

    
    if K1==None:###No momentum specified - revert to the default momentum
        K1=np.mod((Ne*(Ne-1))/2,Ne*denom)
    if np.mod((Ne*(Ne-1))/2-K1,Ne)!=0:
        raise Exception,"The desired momentum "+str(K1)+" does not match Ne="+str(Ne)

    
    WFN=FQHTorusLaughlinWF(Ne,K1,Tau=tau,QHPos=QPPos,LatticeSample=Args.lattice_sample,Denom=denom,EffectiveNs=Args.Sample_at_Ns)
    if Args.Reuse:####Open up the old CoordFileinates
        print(("Open file '"+CoordFile+"' for reading.."))
        with h5py.File(CoordFile, 'r') as coord:
            xcoord=np.array(coord.get('xvalues'))
            ycoord=np.array(coord.get('yvalues'))
        if Args.pos_style=="lattice":
            wfval=WFN.WfnValueSerialLattice(xcoord+1j*ycoord,logform=Logform)
        else:
            wfval=WFN.WfnValueSerial(xcoord+1j*ycoord,logform=Logform)
    else: ####Generate the Laughlin wave function using a random walk monte carlo process
        wfval,coords=WFN.WfnValueGenerateLattice(NSamples,logform=Logform,seed=Args.seed,sampling_gap=Args.sampling_gap*Ne,therm=Args.thermalization_steps*Ne)
        
    Nsamples=len(wfval)
    wfval_out=np.zeros((Nsamples,2),dtype=np.float64)
    wgt_out=np.zeros((Nsamples,2),dtype=np.float64)
    wfval_out[:,0]=np.real(wfval)
    wfval_out[:,1]=np.imag(wfval)
    wgt_out=np.array(wfval_out)
        
    ###Print to file
    print "Write the wave functions to file: ",Wf_out
    try:
        of = h5py.File(Wf_out, 'w')
    except IOError as ioerror:
        print(("ERROR: Problem writing to file " + Wf_out  + ': ' + str(ioerror)))
        sys.exit(-1)
    of.create_dataset("wf_values", data=wfval_out)
    of.close()

    
    if not Args.Reuse:####Only samve weight and coodinates and coordinates are not reused
        print "Write the weight to file: ",Wgt_out
        try:
            of = h5py.File(Wgt_out, 'w')
        except IOError as ioerror:
            print(("ERROR: Problem writing to file " + Wgt_out  + ': ' + str(ioerror)))
            sys.exit(-1)
        of.create_dataset("wf_values", data=wgt_out)
        of.close()

        print "Write the coordinates to file: ",CoordFile
        try:
            of = h5py.File(CoordFile, 'w')
        except IOError as ioerror:
            print(("ERROR: Problem writing to file " + CoordFile  + ': ' + str(ioerror)))
            sys.exit(-1)
        of.create_dataset("xvalues", data=coords.real)
        of.create_dataset("yvalues", data=coords.imag)
        of.close()

def ensure_flag_dep(F1,FName1,F2,FName2):
    if F1!=None:
        ##Flag 1 has a value
        if F2==None:
            print(("ERROR: Flag "+FName1+" depends on flag "+FName2+", which has not been set!"))
            sys.exit(-1)

if __name__ == '__main__' :
    __main__()
