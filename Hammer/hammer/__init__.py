#! /usr/bin/env python

""" __init__.py: Initialisation for hammer module. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

# import basis objects
from hammer.FQHBasisManager import FQHBasisManager
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion
from hammer.FQHTorusBasisBosonicCOMMomentum import FQHTorusBasisBosonicCOMMomentum
from hammer.FQHTorusBasisBosonicCOMMomentumInversion import FQHTorusBasisBosonicCOMMomentumInversion

# import coefficient objects
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients2Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients3Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients4Body
from hammer.FQHSphereCoefficients import FQHSphereInteractionCoefficients2Body

# import hamiltonian arguments
from hammer.FQHTorusHamiltonian import FQHTorusHamiltonian
from hammer.FQHSphereHamiltonian import FQHSphereHamiltonian

# import miscellaneous
from hammer.HammerArgs import HammerArgsParser
import hammer.vectorutils as vectorutils
import hammer.matrixutils as matrixutils
from hammer.TorusDiagonalise import TorusDiagonalise
from hammer.TorusDiagonalise import TorusLaughlinGroundState
from hammer.TorusDiagonalise import TorusPfaffianGroundState
from hammer.TorusDiagonalise import TorusCoulombGroundState
from hammer.TorusDiagonalise import TorusCoulombStates
from hammer.TorusDiagonalise import TorusCoulombHamiltonian
from hammer.SphereDiagonalise import SphereDiagonalise
from hammer.TauValue import TauValue
from hammer.FQHTorusWF import FQHTorusWF
from hammer.FQHSphereWF import FQHSphereWF

import sys

def exit(code):
    """
    Function to with given error code.
    """
    sys.exit(code)

