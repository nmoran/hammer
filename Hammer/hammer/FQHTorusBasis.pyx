#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusBasis.pyx: This is a cython implementation of code to work with the FQH basis on a Torus. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
cimport numpy as np
import sympy as syp
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
from petsc4py import PETSc
from slepc4py import SLEPc
from mpi4py import MPI
import scipy.linalg.lapack
import time
import sys
import hammer.utils_cython as utils_cython
from hammer.FQHTorusWF import FQHTorusWF


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)


#cdef extern from "complex.h":
    #double complex cexp(double complex x) nogil
    #double cabs(double complex x) nogil
    #double creal(double complex x) nogil
    #double complex conj(double complex x) nogil

    
# Defining lookup table of reversed bytes for fast inversion of Fock configurations.
cdef unsigned char* BitReverseTable256 = [
  0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,
  0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,
  0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,
  0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,
  0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,
  0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
  0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,
  0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
  0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
  0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,
  0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
  0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
  0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
  0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
  0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,
  0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
];


cdef class FQHTorusBasis:


    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.Particles = 0
        self.NbrFlux = 0
        self.Momentum = 0
        self.Dim = 0
        self.FluxOffset = 0
        self.ApproximateBasisFlag = 0


    def __init__(self, int Particles, int NbrFlux, int Momentum, int FluxOffset=0):
        """
        Set the variables

        Parameters
        -----------
        Particles: int
            Number of particles.
        NbrFlux: int
            Number of flux.
        Momentum: int
            Total momentum,
        FluxOffset: int
            The flux offset. Used for getting basis on subsystem and can also be used for sphere
            and cylinder bases (default=0).
        """
        self.Particles = Particles
        self.NbrFlux = NbrFlux
        self.Momentum = Momentum
        self.FluxOffset = FluxOffset
        self.Dim = 0
        self.Mask = (1l << self.NbrFlux) - 1l
        self.MonomialRep = <int *>PyMem_Malloc(self.NbrFlux * sizeof(int))

        # these are needed for inversion operations
        self.BytesToReverse = np.ceil(self.NbrFlux / 8.0)
        self.ShiftAfterReverse = 64 - self.NbrFlux

        self.Distance = 1


    def ReadBasis(self, filename):
        try:
            f = open(filename, "r")
            line = f.readline()
            while line != "":
                self.Dim += 1
                line = f.readline()

            self.BasisElements = <long *>PyMem_Malloc(self.Dim * sizeof(long))

            f.seek(0)
            self.Dim = 0
            line = f.readline()
            while line != "":
                self.BasisElements[self.Dim] = <long>int(line)
                self.Dim += 1
                line = f.readline()
            f.close()

        except IOError:
            self.Dim = 0
            return


    def WriteBasis(self, filename):
        """
        Function write the basis to a file on disk.
        TODO: Write version that writes in binary format instead, possibly hdf5
        """
        if PETSc.Comm.getRank(PETSc.COMM_WORLD) == 0: # only write from the process with rank 0
            f = open(filename, "w")
            i = 0
            while i < self.Dim:
                f.write(str(self.BasisElements[i]) + "\n")
                i += 1

            f.close()


    cpdef GenerateBasis(self, filename='', DimensionOnly=False):
        """
        Generate basis elements, first calculate the dimension, allocate space and then
        populate the basis array.

        Parameters
        -----------
        filename: str
            Filename to read basis from if it exists.
        DimensionOnly: bool
            Flag that indicates that the dimension should only be calculated and not the elements.
        """

        # Some safety checks
        if self.Particles < 0 or self.NbrFlux < 1 or self.NbrFlux > 64 :
            return

        if filename != '' :
            self.ReadBasis(filename)  # Try to read the basis, if no such file or basis is invalid then dimension is set to 0.
            if self.Dim <= 0:  # If basis could not be read for what ever reason we generate it and write it to disk
                self.Dim = self.FindBasisDimension(self.Momentum, self.NbrFlux, self.Particles)
                if not DimensionOnly:
                    self.BasisElements = <long *>PyMem_Malloc(self.Dim * sizeof(long))
                    self.FindBasis(self.Momentum,self.NbrFlux,self.Particles)
                    self.WriteBasis(filename)
        else:       # If no filename was specified we simply generate the basis.
            self.Dim = self.FindBasisDimension(self.Momentum, self.NbrFlux, self.Particles)
            if not DimensionOnly:
                self.BasisElements = <long *>PyMem_Malloc(self.Dim * sizeof(long))
                self.FindBasis(self.Momentum,self.NbrFlux,self.Particles)


    def FindBasisDimension(self, int J, int Nphi, int Ne):
        """
        Calculates the basis dimension for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasisDimension to calculate the dimension of the basis.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int dim = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        cdef int i = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 -i)
        i = 0
        while (J + i*(Nphi + self.FluxOffset)) <= MaxMomentum:
            dim += self.FindSubBasisDimension(J + i * (Nphi + self.FluxOffset), Nphi, Ne)
            i+=1
        return dim


    cdef int FindSubBasisDimension(self, int J, int Nphi, int Ne):
        """
        Recursive function to find the basis dimension for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int dim = 0

        if (Ne == 0):
            if (J == 0):
                return 1
            else:
                return 0
        elif (Ne > Nphi):
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                dim += self.FindSubBasisDimension(J-(Nphi-1), Nphi-1, Ne-1)

            dim += self.FindSubBasisDimension(J, Nphi-1, Ne)
        return dim


    def FindBasis(self, int J, int Nphi, int Ne):
        """
        Calculates the basis elements for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasis to calculate the basis elements and populate the BasisElements array.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 - i)

        cdef int MaxTotMomentum = (Nphi + self.FluxOffset) * (MaxMomentum / (Nphi + self.FluxOffset)) + J

        return  self.FindSubBasis(MaxTotMomentum, Nphi + self.FluxOffset, Nphi, Ne, idx, 0l)


    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis elements for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0):  # this is used instead of the standard modulus operator
                self.BasisElements[idx] = base
                return 1
            else:
                return 0
        elif (Ne > Nphi):
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasis(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))

            idx += offset
            offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne,idx, base)
        return offset


    def __dealloc__(self):
        """
        This is the destructor which dallocates any memory that was allocated.
        """
        if self.Dim > 0:
            PyMem_Free(self.BasisElements)

        PyMem_Free(self.MonomialRep)



#    cpdef double AADebug(self, int Orbital1, int Orbital2, int Idx):
#        """
#        Apply annihilation operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx and store the result in TmpState.
#        TODO: investigate popcnt implementation to make this more efficient.
#
#        """
#        cdef double phase = 1.0
#        cdef long Mask
#
#        PetscPrint = PETSc.Sys.Print
#
#        # Create mask with ones in positions of particles to be annihilated
#        Mask = 1l << Orbital1
#        PetscPrint("Added: " + str(1l << Orbital1))
#        Mask += 1l << Orbital2
#        PetscPrint("Added: " + str(1l << Orbital2))
#
#        PetscPrint("Mask: " + str(Mask))
#
#        cdef long State = self.BasisElements[Idx]
#
#
#        PetscPrint("Size of int: " + str(sizeof(int)))
#        PetscPrint("Size of long: " + str(sizeof(long)))
#        PetscPrint("State: " + str(State))
#
#        if (State & Mask) == Mask: # If there are particles at both positions
#            self.TmpState = State ^ Mask  # New state is gotten by removing these with exclusive or operation
#        else:
#            return 0.0 # no particles to be annihilated so state annihilated
#
#        # Calculate the phase we get when hopping the annihilation operators in. Assume that Orbital1 is always greater than Orbital2
#        cdef int i = Orbital2
#        while i < Orbital1:
#            if (State & (1l << i)) > 0:
#                phase *= -1.0
#            i+=1
#
#        return phase
#
#
#    cpdef double AdAdDebug(self, int Orbital1, int Orbital2):
#        """
#        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.
#        TODO: investigate popcnt implementation to make this more efficient.
#
#        """
#        cdef double phase = 1.0
#        cdef long Mask
#
#        PetscPrint = PETSc.Sys.Print
#
#        # Create mask with ones in positions of particles to be annihilated
#        Mask = 1l << Orbital1
#        Mask += 1l << Orbital2
#
#        if (self.TmpState & Mask) == 0: # If there are no particles at both positions
#            self.TmpState2 = self.TmpState | Mask  # New state is gotten by adding particles using or operation
#        else:
#            return 0.0 # particles cannot be created
#
#
#        PetscPrint("Old state: " + str(self.TmpState))
#        PetscPrint("Mask: " + str(Mask))
#        PetscPrint("New state: " + str(self.TmpState2))
#
#        # Calculate the phase we get when hopping the creation operators in. Assume that Orbital1 is always greater than Orbital2
#        cdef int i = Orbital2
#        while i < Orbital1:
#            if (self.TmpState & (1l << i)) > 0:
#                phase *= -1.0
#            i+=1
#
#        return phase
#

    cpdef double N(self, int Orbital, int Idx):
        """
        Apply density operator to the site specified. Return 1 if site is occupied and zero otherwise.
        """
        if (<long>self.BasisElements[Idx] & (1l << Orbital)) > 0l:
            return 1.0
        else:
            return 0.0


    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply density density operator on the given sites to the configuration indexed by Idx.
        """
        if ((<long>self.BasisElements[Idx] & (1l << Orbital1)) > 0l) and ((<long>self.BasisElements[Idx] & (1l << Orbital2)) > 0l):
            return 1.0
        else:
            return 0.0


    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply density density operator on the given sites to the configuration indexed by Idx.
        """
        if ((<long>self.BasisElements[Idx] & (1l << Orbital1)) > 0l) and ((<long>self.BasisElements[Idx] & (1l << Orbital2)) > 0l) and ((<long>self.BasisElements[Idx] & (1l << Orbital3)) > 0l):
            return 1.0
        else:
            return 0.0


    cpdef double A(self, int Orbital, int Idx):
        """
        Apply annihilation operator to the site specified. Return 1 if site is occupied and zero otherwise.
        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital

        cdef long State = <long>self.BasisElements[Idx]

        if (State & Mask) == Mask: # If there are particles at both positions
            self.TmpState = State ^ Mask  # New state is gotten by removing these with exclusive or operation
        else:
            return 0.0 # no particles to be annihilated so state annihilated

        # Calculate the phase we get when hopping the annihilation operators in. Assume that Orbital1 is always greater than Orbital2
        cdef int i = 0
        while i < Orbital:
            if (State & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double complex Ad(self, int Orbital):
        """
        Apply creation operator on site with momentum Orbital state TmpState and store result in TmpState2.
        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital

        if (self.TmpState & Mask) == 0: # If there are no particles at both positions
            self.TmpState2 = self.TmpState | Mask  # New state is gotten by adding particles using or operation
        else:
            return 0.0 # particles cannot be created

        # Calculate the phase we get when hopping the creation operators in. Assume that Orbital1 is always greater than Orbital2
        cdef int i = 0
        while i < Orbital:
            if (self.TmpState & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double AA(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx and store the result in TmpState.
        It is always assumed that Orbital1 is greater than Orbital2.
        TODO: investigate popcnt implementation to make this more efficient.

        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital1
        Mask += 1l << Orbital2


        cdef long State = <long>self.BasisElements[Idx]

        #if Idx == 0:
        #    print "Mask: " + str(Mask) + " and state: " + str(State) + " (size of long " + str(sizeof(long)) + ")"

        if (State & Mask) == Mask: # If there are particles at both positions
            self.TmpState = State ^ Mask  # New state is gotten by removing these with exclusive or operation
        else:
            return 0.0 # no particles to be annihilated so state annihilated

        # Calculate the phase we get when hopping the annihilation operators in. Assume that Orbital1 is always greater than Orbital2
        cdef int i = Orbital2
        while i < Orbital1:
            if (State & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double StateAA(self, int Orbital1, int Orbital2, long State):
        """
        Apply annihilation operators on sites with momentum Orbital1 and Orbital2 to basis elements provided and store the result in TmpState.
        It is always assumed that Orbital1 is greater than Orbital2.
        TODO: investigate popcnt implementation to make this more efficient.

        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital1
        Mask += 1l << Orbital2


        #if Idx == 0:
        #    print "Mask: " + str(Mask) + " and state: " + str(State) + " (size of long " + str(sizeof(long)) + ")"

        if (State & Mask) == Mask: # If there are particles at both positions
            self.TmpState = State ^ Mask  # New state is gotten by removing these with exclusive or operation
        else:
            return 0.0 # no particles to be annihilated so state annihilated

        # Calculate the phase we get when hopping the annihilation operators in. Assume that Orbital1 is always greater than Orbital2
        cdef int i = Orbital2
        while i < Orbital1:
            if (State & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double complex AdAd(self, int Orbital1, int Orbital2):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.
        It is always assumed that Orbital1 is greater than Orbital2.
        TODO: investigate popcnt implementation to make this more efficient.

        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital1
        Mask += 1l << Orbital2

        if (self.TmpState & Mask) == 0: # If there are no particles at both positions
            self.TmpState2 = self.TmpState | Mask  # New state is gotten by adding particles using or operation
        else:
            return 0.0 # particles cannot be created

        # Calculate the phase we get when hopping the creation operators in. Assume that Orbital1 is always greater than Orbital2
        cdef int i = Orbital2
        while i < Orbital1:
            if (self.TmpState & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1, Orbital2 and Orbital3 to basis elements stored at index Idx and store the result in TmpState.
        TODO: investigate popcnt implementation to make this more efficient.

        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital1
        Mask += 1l << Orbital2
        Mask += 1l << Orbital3

        cdef long State = self.BasisElements[Idx]

        if (State & Mask) == Mask: # If there are particles at both positions
            self.TmpState = State ^ Mask  # New state is gotten by removing these with exclusive or operation
        else:
            return 0.0 # no particles to be annihilated so state annihilated

        # Calculate the phase we get when hopping the annihilation operators in. Assume that Orbital3 is always greater than Orbital2, is greater than Orbital1
        cdef int i = 0
        while i < Orbital1:
            if (State & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        i = Orbital2
        while i < Orbital3:
            if (State & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.
        TODO: investigate popcnt implementation to make this more efficient.

        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital1
        Mask += 1l << Orbital2
        Mask += 1l << Orbital3

        if (self.TmpState & Mask) == 0: # If there are no particles at both positions
            self.TmpState2 = self.TmpState | Mask  # New state is gotten by adding particles using or operation
        else:
            return 0.0 # particles cannot be created

        # Calculate the phase we get when hopping the creation operators in. Assume that Orbital3 is always greater than Orbital2, is greater than Orbital1
        cdef int i = 0
        while i < Orbital1:
            if (self.TmpState & (1l << i)) > 0:
                phase *= -1.0
            i+=1


        i = Orbital2
        while i < Orbital3:
            if (self.TmpState & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1, Orbital2, Orbital3 and Orbital4 to basis elements stored at index Idx and store the result in TmpState.
        TODO: investigate popcnt implementation to make this more efficient.

        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital1
        Mask += 1l << Orbital2
        Mask += 1l << Orbital3
        Mask += 1l << Orbital4

        cdef long State = self.BasisElements[Idx]

        if (State & Mask) == Mask: # If there are particles at both positions
            self.TmpState = State ^ Mask  # New state is gotten by removing these with exclusive or operation
        else:
            return 0.0 # no particles to be annihilated so state annihilated

        # Calculate the phase we get when hopping the annihilation operators in. Assume that Orbital4 > Orbital3 > Orbital2 > Orbital1
        # Below shows which parts must be considered for the phase. These are parts which are passed an odd number of times.
        #   -----o-----o-----o-----o-----
        # 1./////o-----o-----o-----o-----
        # 2.-----o/////o-----o-----o-----
        # 3./////o-----o/////o-----o-----
        # 4.-----o/////o-----o/////o-----
        cdef int i = Orbital1
        while i < Orbital2:
            if (State & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        i = Orbital3
        while i < Orbital4:
            if (State & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4):
        """
        Apply creation operators on sites with momentum Orbital1 ,Orbital2, Orbital3 and Orbital4 to state TmpState and store result in TmpState2.
        TODO: investigate popcnt implementation to make this more efficient.

        """
        cdef double phase = 1.0
        cdef long Mask

        # Create mask with ones in positions of particles to be annihilated
        Mask = 1l << Orbital1
        Mask += 1l << Orbital2
        Mask += 1l << Orbital3
        Mask += 1l << Orbital4

        if (self.TmpState & Mask) == 0: # If there are no particles at both positions
            self.TmpState2 = self.TmpState | Mask  # New state is gotten by adding particles using or operation
        else:
            return 0.0 # particles cannot be created

        # Calculate the phase we get when hopping the annihilation operators in. Assume that Orbital4 > Orbital3 > Orbital2 > Orbital1
        # Below shows which parts must be considered for the phase. These are parts which are passed an odd number of times.
        #   -----o-----o-----o-----o-----
        # 1./////o-----o-----o-----o-----
        # 2.-----o/////o-----o-----o-----
        # 3./////o-----o/////o-----o-----
        # 4.-----o/////o-----o/////o-----
        cdef int i = Orbital1
        while i < Orbital2:
            if (self.TmpState & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        i = Orbital3
        while i < Orbital4:
            if (self.TmpState & (1l << i)) > 0:
                phase *= -1.0
            i+=1

        return phase


    cpdef int BinaryArraySearchDescending(self):
        """
        Function to perform binary search on array of BasisElements for TmpState2
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef long l = self.Dim

        cdef long start = 0
        cdef long end = l
        cdef long dist = end - start
        cdef long test_item
        cdef long item = self.TmpState2
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    cpdef int BinaryArraySearchDescendingState(self, long item):
        """
        Function to perform binary search on array of BasisElements for TmpState2
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef long l = self.Dim

        cdef long start = 0
        cdef long end = l
        cdef long dist = end - start
        cdef long test_item
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    def GetDimension(self):
        """
        Return the basis dimension
        """
        return self.Dim


    def GetElement(self, Idx):
        """
        Return the basis element with index Idx
        """
        return self.BasisElements[Idx]


    cdef long GetElementC(self, int Idx):
        """
        Return the basis element with index Idx (C version).

        Parameters
        -----------
        Idx: int
            Index of the element.

        Returns
        --------
        long
            The desired element.
        """
        return self.BasisElements[Idx]


    def PrintOccupationRep(self,Idx):
        """
        Print the basis element with index Idx in the occupation representation
        """
        print '<' + self.GetOccupationRep(Idx) + '>'


    def GetOccupationRep(self,Idx):
        """
        Return the basis element with index Idx in the occupation representation
        """
        if Idx < self.Dim:
            State = self.BasisElements[Idx]
            return self.GetOccupationRepOfElement(State)
        else:
            return ""


    def GetBasisDetails(self):
        """
        Returns a dictionary object with parameters of the basis.
        """
        Details = dict()
        Details['Particles'] = self.Particles
        Details['NbrFlux'] = self.NbrFlux
        Details['Momentum'] = self.Momentum
        Details['Bosonic'] = False
        return Details


        
    def GetElementIdx(self, Element):
        """
        Return the index of a supplied basis element, -1 if the basis element does not exist in the basis.
        Parameters
        -----------
        Element: long


        """
        self.TmpState2 = Element
        return self.BinaryArraySearchDescending()


    def GetElementFromOccupationRep(self,Configuration):
        """
        Return the encoded integer corresponding the suppilied occupation representation of the element.
        """
        Element = 0
        for i in np.arange(0,self.NbrFlux):
            if Configuration[i] == '1':
                Element += 1l << i

        return Element


    def GetElementFromOccupationVec(self,Configuration):
        """
        Return the encoded integer corresponding the suppilied vector occupation representation of the element.
        """
        Element = 0
        for i in np.arange(0,self.NbrFlux):
            if Configuration[i] == 1:
                Element += 1l << i
        return Element


    
    def GetOccupationRepOfElement(self,State):
        """
        Return the basis element with index Idx in the occupation representation.
        """
        rep = ""
        for i in np.arange(0,self.NbrFlux):
            if (1l << i & State) > 0:
                rep = rep + "1 "
            else:
                rep = rep + "0 "
        return rep


    def GetOccupationVectorState(self,Idx):
        """
        Return the vector encoded occupation representation of the basis state with index Idx.
        """
        State = self.BasisElements[Idx]
        rep=np.zeros(self.NbrFlux,dtype=int)
        for i in np.arange(0,self.NbrFlux):
            if (1l << i & State) > 0:
                rep[i] = 1
            else:
                rep[i] = 0
        return rep


    #cpdef np.ndarray[np.int32_t, ndim=1] GetMonomialOfElement(self, long Element):
    cpdef GetMonomialOfElement(self, long Element):
        """
        Return monomial representation of the provided basis element.
        """
        cdef np.ndarray[np.int32_t, ndim=1] Monomial = np.zeros(self.Particles, dtype=np.int32)
        cdef int i, j

        j = 0
        i = 0
        while i < self.NbrFlux:
            if ((1l << i) & Element) > 0:
                Monomial[j] = i
                j += 1
                if j >= self.Particles:
                    return Monomial
            i += 1
        return Monomial


    cpdef CalcMonomialOfElement(self, long Element):
        """
        Calculate monomial representation of the provided basis element and store internally.
        """
        cdef int i, j

        j = 0
        i = 0
        while i < self.NbrFlux and j < self.Particles:
            if ((1l << i) & Element) > 0:
                self.MonomialRep[j] = i
                j += 1
            else:
                self.MonomialRep[j] = 0
            i += 1


    cdef int * GetMonomialRepC(self):
        """
        Return the monomial representation buffer. Use CalcMonomialOfElement to modify this.
        """
        return self.MonomialRep


    def GetMonomialRep(self):
        """
        Return the monomial representation buffer. Use CalcMonomialOfElement to modify this.
        """
        rep = np.zeros(self.Particles)
        cdef int i = 0
        while i < self.Particles:
            rep[i] = self.MonomialRep[i]
            i += 1
        return rep


    def GetElementOfMonomial(self, Monomial):
        """
        Return the encoded integer representation of the supplied monomial.
        """
        Element = 0
        for i in np.arange(0,self.Particles):
            Element += 1l << Monomial[i]

        if self.GetElementIdx(Element) != -1:
            return Element
        else:
            return -1


    cpdef long Translate(self, long State, int t):
        """
        Function that translates the given configuration by t orbitals.
        """
        cdef int phase = 1
        cdef long Mask = (1l << self.NbrFlux) - 1l
        cdef long OverFlowMask = Mask - ((1l << (self.NbrFlux - t)) - 1l)

        # this is going to count the particles in the section that rolls over the edge and set the phase accordingly
        for i in range(t):
            if (State & (1l << (self.NbrFlux - t + i))) > 0:
                phase *= -1

        return phase * (((State << t) + ((State & OverFlowMask) >> self.NbrFlux - t)) & Mask)


    cpdef int RepresentativeConfiguration(self, Idx):
        """
        Certain configurations are the same as others up to a translation. A configuration is a representative configuration if it has the
        smallest numerical value of the configurations which can be related via translation.

        Returns the index of the representative configuration.
        """
        # Translation will have to result in configuration in the same momentum sector so translation distance t by number of particles N must be a multiple of the number of flux.
        n = 1
        while n < self.Particles:
            if ((n * self.NbrFlux) % self.Particles) == 0:
                break;
            else:
                n += 1

        t = 1
        if n < self.Particles and ((n * self.NbrFlux) % self.Particles) == 0 :
            t = (n * self.NbrFlux) / self.Particles
        else:
            return Idx

        cdef long State = <long>self.BasisElements[Idx]
        cdef long TmpState = 0
        cdef long MinState = State
        cdef long Mask = (1l << self.NbrFlux) - 1l
        cdef long OverFlowMask = Mask - ((1l << (self.NbrFlux - t)) - 1l)

        # Keep translating State by t places until we arrive back to the original state.
        while TmpState != State:
            if TmpState == 0:
                TmpState = State
            TmpState = ((TmpState << t) + ((TmpState & OverFlowMask) >> self.NbrFlux - t)) & Mask
            if TmpState < MinState:
                MinState = TmpState

        return self.BinaryArraySearchDescendingState(MinState)


    cpdef long GetTmpState(self):
        """
        Return the internally stored variable TmpState
        """
        return self.TmpState


    cpdef long GetTmpState2(self):
        """
        Return the internally stored variable TmpState
        """
        return self.TmpState2


    cpdef SetTmpState(self, long state):
        """
        Set the value of internal variable TmpState
        """
        self.TmpState = state


    cpdef SetTmpState2(self, long state):
        """
        Set the value of internal variable TmpState2
        """
        self.TmpState2 = state


    @cython.cdivision(False)
    cpdef OrbitalEntanglementMatrix(self, State, int AOrbitalStart, int AOrbitalEnd, int AMomentum, int AParticles):
        """
        This method, given a state in the current basis will construct and return the Schmidt matrix corresponding to bipartitioning the system into two parts, one containing
        the A orbitals specified and the specified total momentum. Only works in serial.
        """

        cdef int Momentum = self.Momentum
        cdef int Particles = self.Particles
        cdef int NbrFlux = self.NbrFlux
        cdef int Dim, ADim, BDim

        #if PETSc.COMM_WORLD.getSize() > 1:
        #    PETSc.Sys.Print("OrbitalEntanglementMatrix method only works in serial for now.")
        #    return

        # First must determine the basis over the A orbitals and the B orbitals.
        cdef int BMomentum = (Momentum - AMomentum + NbrFlux) % NbrFlux  # the momentum over B orbitals must be equal to the total momentum minus the momentum over A orbitals.

        # the number of flux (orbitals) in each partitioin.
        cdef int AFlux = AOrbitalEnd - AOrbitalStart
        cdef int BFlux = NbrFlux  - AFlux

        cdef int BParticles = Particles - AParticles

        cdef int AEffectiveMomentum = (AMomentum - (AOrbitalStart * AParticles)) % NbrFlux
        if AEffectiveMomentum < 0:
            AEffectiveMomentum = NbrFlux + AEffectiveMomentum
        cdef int BEffectiveMomentum = (BMomentum - (AOrbitalEnd * BParticles)) % NbrFlux
        if BEffectiveMomentum < 0:
            BEffectiveMomentum = NbrFlux + BEffectiveMomentum

        cdef int AFluxOffset = NbrFlux - AFlux
        cdef int BFluxOffset = NbrFlux - BFlux

        cdef long Element, AElement, BElement

        ABasis = FQHTorusBasis(AParticles, AFlux, AEffectiveMomentum, FluxOffset = AFluxOffset); ABasis.GenerateBasis()
        BBasis = FQHTorusBasis(BParticles, BFlux, BEffectiveMomentum, FluxOffset = BFluxOffset); BBasis.GenerateBasis()

        ADim = ABasis.GetDimension()
        BDim = BBasis.GetDimension()

        Dim = self.GetDimension()


        if ADim == 0 or BDim == 0:
            return None

        # create the matrix
        W = PETSc.Mat(); W.createDense([ADim, BDim])
        #W.setSizes([ADim, BDim])
        W.setFromOptions()
        W.setUp()
        W.zeroEntries()

        #W = np.zeros((ADim, BDim), dtype=np.complex128)

        cdef long TotalMask = (1l << NbrFlux) - 1l  # mask which should be all ones over all orbitals
        cdef long AMask = ((1l << AFlux) - 1l) << AOrbitalStart # mask which should be ones only over the a orbitals.
        cdef long BMask = (((1l << NbrFlux) - 1l) ^ AMask) & TotalMask # inverse of A mask so should be ones over all orbitals not in A

        cdef int i = 0
        while i < Dim:
            Element = self.GetElement(i)
            AElement = (Element & AMask) >> AOrbitalStart
            BElement = ((Element & BMask) >> AOrbitalEnd)  +  ((Element & BMask) << (NbrFlux - AOrbitalEnd)) & TotalMask

            Row = ABasis.BinaryArraySearchDescendingState(AElement)
            Col = BBasis.BinaryArraySearchDescendingState(BElement)

            if Row >= 0 and Col >= 0 and Row < ADim and Col < BDim:
                #print str(AElement) + ', ' + str(BElement) + ': ' + str((Row, Col)) + " = " + str(State.getValue(i))
                W.setValue(Row, Col, State.getValue(i), True)
                #W[Row, Col] += State.getValue(i)

            i += 1

        W.assemble()

        return W


    @cython.cdivision(False)
    cpdef OrbitalEntanglementMatrixFast(self, State, int AOrbitalStart, int AOrbitalEnd, int AMomentum, int AParticles):
        """
        This method, given a state in the current basis will construct and return the Schmidt matrix corresponding to bipartitioning the system into two parts, one containing
        the A orbitals specified and the specified total momentum. Only works in serial.

        Parameters
        ------------
        State: Vec
            State to calculate the entanglement for.
        AOrbitalStart: int
            The index of the orbital where the A partition starts, from 0 to N_phi - 1.
        AOrbitalEnd: int
            The index of the orbital where the B partition begins, from AOrbitalStart + 1 to N_phi - 1.
        AMomentum: int
            The total momentum in the A partition.
        AParticles: int
            The number of particles in the A parition (1 to overall number of particles).
        """

        cdef int Momentum = self.Momentum
        cdef int Particles = self.Particles
        cdef int NbrFlux = self.NbrFlux
        cdef int Dim, ADim, BDim
        cdef int rstart, rend

        rstart, rend = State.getOwnershipRange()
        #if PETSc.COMM_WORLD.getSize() > 1:
        #    PETSc.Sys.Print("OrbitalEntanglementMatrix method only works in serial for now.")
        #    return

        # First must determine the basis over the A orbitals and the B orbitals.
        cdef int BMomentum = (Momentum - AMomentum + NbrFlux) % NbrFlux  # the momentum over B orbitals must be equal to the total momentum minus the momentum over A orbitals.

        # the number of flux (orbitals) in each partitioin.
        cdef int AFlux = AOrbitalEnd - AOrbitalStart
        cdef int BFlux = NbrFlux  - AFlux

        cdef int BParticles = Particles - AParticles

        cdef int AEffectiveMomentum = (AMomentum - (AOrbitalStart * AParticles)) % NbrFlux
        if AEffectiveMomentum < 0:
            AEffectiveMomentum = NbrFlux + AEffectiveMomentum
        cdef int BEffectiveMomentum = (BMomentum - (AOrbitalEnd * BParticles)) % NbrFlux
        if BEffectiveMomentum < 0:
            BEffectiveMomentum = NbrFlux + BEffectiveMomentum

        cdef int AFluxOffset = NbrFlux - AFlux
        cdef int BFluxOffset = NbrFlux - BFlux

        cdef long Element, AElement, BElement

        ABasis = FQHTorusBasis(AParticles, AFlux, AEffectiveMomentum, FluxOffset = AFluxOffset); ABasis.GenerateBasis()
        BBasis = FQHTorusBasis(BParticles, BFlux, BEffectiveMomentum, FluxOffset = BFluxOffset); BBasis.GenerateBasis()

        ADim = ABasis.GetDimension()
        BDim = BBasis.GetDimension()

        Dim = self.GetDimension()


        if ADim == 0 or BDim == 0:
            return None

        # create the matrix
        W = PETSc.Mat(); W.createDense([ADim, BDim])
        #W.setSizes([ADim, BDim])
        W.setFromOptions()
        W.setUp()
        W.zeroEntries()

        #W = np.zeros((ADim, BDim), dtype=np.complex128)

        cdef long TotalMask = (1l << NbrFlux) - 1l  # mask which should be all ones over all orbitals
        cdef long AMask = ((1l << AFlux) - 1l) << AOrbitalStart # mask which should be ones only over the a orbitals.
        cdef long BMask = (((1l << NbrFlux) - 1l) ^ AMask) & TotalMask # inverse of A mask so should be ones over all orbitals not in A

        cdef int i = 0, j = 0
        while i < ADim:
            j = 0
            while j < BDim:
                AElement = ABasis.GetElement(i)
                BElement = BBasis.GetElement(j)

                Element = (AElement << AOrbitalStart) + ((BElement << AOrbitalEnd) & TotalMask) + (BElement >> (BFlux - AOrbitalStart))

                Idx = self.BinaryArraySearchDescendingState(Element)

                if Idx >= rstart and Idx < rend:
                    W.setValue(i, j, State.getValue(Idx), True)

                j += 1
            i += 1

        W.assemble()

        return W


    @staticmethod
    def OrbitalEntanglementMatrixSuperposition(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, AParticles):
        """
        This method, given a superposition of states in the current basis will construct and return the Schmidt matrix corresponding to a bipartition of the system.
        """
        Momentums = list()

        # Get particle number and flux from the first basis as should be the same for all.
        Basis = Bases[0]
        Details = Basis.GetBasisDetails()
        Momentums.append(Details['Momentum'])
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        for Basis in Bases[1:]:
            Details = Basis.GetBasisDetails()
            Momentums.append(Details['Momentum'])
            if Details['Particles'] != Particles or Details['NbrFlux'] != NbrFlux:
                PETSc.Sys.Print("Particle number and flux must be the same for all bases.")
                return

        if PETSc.COMM_WORLD.getSize() > 1:
            PETSc.Sys.Print("OrbitalEntanglementMatrix method only works in serial for now.")
            return

        if AParticles > (AOrbitalEnd - AOrbitalStart):
            return None

        # the number of flux (orbitals) in each partitioin.
        AFlux = AOrbitalEnd - AOrbitalStart
        BFlux = NbrFlux  - AFlux
        BParticles = Particles - AParticles
        AFluxOffset = NbrFlux - AFlux
        BFluxOffset = NbrFlux - BFlux


        AMomentums = range(NbrFlux)
        BMomentums = range(NbrFlux)
        #AEffectiveMomentums = map(lambda x : (x - (AOrbitalStart * AParticles)) % NbrFlux, AMomentums)
        #BEffectiveMomentums = map(lambda x : (x - (AOrbitalEnd * BParticles)) % NbrFlux, BMomentums)
        AEffectiveMomentums = AMomentums
        BEffectiveMomentums = BMomentums

        ABases = dict()
        ABasesOffsets = dict()
        AOffset = 0
        for [AEffectiveMomentum, AMomentum] in zip(AEffectiveMomentums, AMomentums):
            if not ABases.has_key(AEffectiveMomentum):
                ABasis = FQHTorusBasis(AParticles, AFlux, AEffectiveMomentum, FluxOffset = AFluxOffset); ABasis.GenerateBasis()
                if ABasis.GetDimension():
                    ABases[AEffectiveMomentum] = ABasis
                    ABasesOffsets[AEffectiveMomentum] = AOffset
                    AOffset += ABasis.GetDimension()

        BBases = dict()
        BBasesOffsets = dict()
        BOffset = 0
        for [BEffectiveMomentum, BMomentum] in zip(BEffectiveMomentums, BMomentums):
            if not BBases.has_key(BEffectiveMomentum):
                BBasis = FQHTorusBasis(BParticles, BFlux, BEffectiveMomentum, FluxOffset = BFluxOffset); BBasis.GenerateBasis()
                if BBasis.GetDimension():
                    BBases[BEffectiveMomentum] = BBasis
                    BBasesOffsets[BEffectiveMomentum] = BOffset
                    BOffset += BBasis.GetDimension()

    #    # Find the minimum and maximum momentum possible for A partition
    #    # Put A particles in first orbits of A partition.
    #    AMomentumMinimum = np.sum(range(AOrbitalStart, AOrbitalEnd)[:AParticles])
    #    AMomentumMaximum = np.sum(range(AOrbitalStart, AOrbitalEnd)[-AParticles:])
    #
    ##    if (AMomentumMaximum % NbrFlux) <= AMomentumMinimum : # if the momentum wraps around then set this to new minimum and go to the end
    ###        AMomentumMinimum = AMomentumMaximum % NbrFlux
    ##        AMomentumMinimum = 0
    ##        AMomentumMaximum = NbrFlux - 1
    ##    else:
    ##        AMomentumMaximum = AMomentumMaximum % NbrFlux
    #
    #    AMomentums = range(AMomentumMinimum, AMomentumMaximum + 1) # range of possible A momentums
    #    AEffectiveMomentums = map(lambda x : (x - (AOrbitalStart * AParticles)) % NbrFlux, AMomentums)
    #
    #    BMomentums = dict()
    #    BEffectiveMomentums = dict()
    #    for Momentum in Momentums:
    #        BMomentums[Momentum] = list()
    #        BEffectiveMomentums[Momentum] = list()
    #        for AMomentum in AMomentums:
    #            BMomentum = (Momentum - AMomentum) % NbrFlux
    #            BMomentums[Momentum].append(BMomentum)  # the momentum over B orbitals must be equal to the total momentum minus the momentum over A orbitals.
    #            BEffectiveMomentums[Momentum].append((BMomentum - (AOrbitalEnd * BParticles)) % NbrFlux)
    #
    ##    print "A momentum " + str(AEffectiveMomentum)
    ##    print "A flux " + str(AFlux)
    ##    print "A particles " + str(AParticles)
    #
    ##    print "B momentum " + str(BEffectiveMomentums)
    ##    print "B flux " + str(BFlux)
    ##    print "B particles " + str(BParticles)
    ##
    #
    #    ABases = dict()
    #    ABasesOffsets = dict()
    #    AOffset = 0
    #    for [AEffectiveMomentum, AMomentum] in zip(AEffectiveMomentums, AMomentums):
    #        if not ABases.has_key(AEffectiveMomentum):
    #            ABasis = FQHTorusBasis(AParticles, AFlux, AEffectiveMomentum, FluxOffset = AFluxOffset); ABasis.GenerateBasis()
    #            if ABasis.GetDimension():
    #                ABases[AEffectiveMomentum] = ABasis
    #                ABasesOffsets[AEffectiveMomentum] = AOffset
    #                AOffset += ABasis.GetDimension()
    #
    #    BBases = dict()
    #    BBasesOffsets = dict()
    #    BOffset = 0
    #    for Momentum in BEffectiveMomentums.keys():
    #        for [BEffectiveMomentum, BMomentum] in zip(BEffectiveMomentums[Momentum], BMomentums[Momentum]):
    #            if not BBases.has_key(BEffectiveMomentum):
    #                BBasis = FQHTorusBasis(BParticles, BFlux, BEffectiveMomentum, FluxOffset = BFluxOffset); BBasis.GenerateBasis()
    #                if BBasis.GetDimension() > 0:
    #                    BBases[BEffectiveMomentum] = BBasis
    #                    BBasesOffsets[BEffectiveMomentum] = BOffset
    #                    BOffset += BBasis.GetDimension()
    #
        ADim = np.sum(map(lambda x: x.GetDimension(), ABases.values()))
        BDim = np.sum(map(lambda x: x.GetDimension(), BBases.values()))
    #
    #    print "A dim: " + str(ADim)
    #    print "B dim: " + str(BDim)

        if ADim == 0 or BDim == 0:
    #        return [None, ABasis, BBases]
            return None

        # create the matrix
        W = PETSc.Mat(); W.create()
        W.setSizes([ADim, BDim])
        W.setFromOptions()
        W.setUp()

    #    W = np.zeros((ADim, BDim), dtype=np.complex128)

        TotalMask = (1l << NbrFlux) - 1l  # mask which should be all ones over all orbitals
        AMask = ((1l << AFlux) - 1l) << AOrbitalStart # mask which should be ones only over the a orbitals.
        BMask = (((1l << NbrFlux) - 1l) ^ AMask) & TotalMask # inverse of A mask so should be ones over all orbitals not in A

        for [Basis, State, Coefficient] in zip(Bases, States, Coefficients):
            i = 0
            while i < Basis.GetDimension():
                AElement = (Basis.GetElement(i) & AMask) >> AOrbitalStart
                BElement = ((Basis.GetElement(i) & BMask) >> AOrbitalEnd)  +  ((Basis.GetElement(i) & BMask) << (NbrFlux - AOrbitalEnd)) & TotalMask

                AEffectiveMomentum = 0
                j = 0
                while j < AFlux:
                    if ((1l << j) & AElement) > 0 :
                        AEffectiveMomentum += j
                    j += 1

                AEffectiveMomentum = AEffectiveMomentum % NbrFlux

                BEffectiveMomentum = 0
                j = 0
                while j < BFlux:
                    if ((1l << j) & BElement) > 0 :
                        BEffectiveMomentum += j
                    j += 1
                BEffectiveMomentum = BEffectiveMomentum % NbrFlux

                if ABases.has_key(AEffectiveMomentum):
                    Row = ABases[AEffectiveMomentum].BinaryArraySearchDescendingState(AElement)
                else:
                    Row = -1

                if BBases.has_key(BEffectiveMomentum):
                    Col = BBases[BEffectiveMomentum].BinaryArraySearchDescendingState(BElement)
                else:
                    Col = -1

                if Row >= 0 and Col >= 0 and Row < ABases[AEffectiveMomentum].GetDimension() and Col < BBases[BEffectiveMomentum].GetDimension():
    #                print str(AElement) + ', ' + str(BElement) + ': ' + str(State.getValue(i))
                    W.setValue(Row + ABasesOffsets[AEffectiveMomentum], Col + BBasesOffsets[BEffectiveMomentum], State.getValue(i) * Coefficient, True)
                    #W[Row + ABasesOffsets[AEffectiveMomentum], Col + BBasesOffsets[BEffectiveMomentum]] += State.getValue(i) * Coefficient

                i += 1

        W.assemble()

    #    return [W, ABases, BBases]
        return W


    @staticmethod
    def OrbitalEntanglementMatrixSuperpositionSymmetry(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, AParticles, AMomentums):
        """
        This method, given a superposition of states in the current basis will construct and return the Schmidt matrix corresponding to a bipartition of the system, restricting momentum on A partition to those specified.
        """
        cdef int i, j, Row, Col, AEffectiveMomentum, BEffectiveMomentum, NbrFlux, AFlux, BFlux, BParticles
        cdef long AElement, BElement, AMask, BMask, TotalMask

        Momentums = list()

        # Get particle number and flux from the first basis as should be the same for all.
        Basis = Bases[0]
        Details = Basis.GetBasisDetails()
        Momentums.append(Details['Momentum'])
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        for Basis in Bases[1:]:
            Details = Basis.GetBasisDetails()
            Momentums.append(Details['Momentum'])
            if Details['Particles'] != Particles or Details['NbrFlux'] != NbrFlux:
                PETSc.Sys.Print("Particle number and flux must be the same for all bases.")
                return

        if PETSc.COMM_WORLD.getSize() > 1:
            PETSc.Sys.Print("OrbitalEntanglementMatrix method only works in serial for now.")
            return

        if AParticles > (AOrbitalEnd - AOrbitalStart):
            return None

        # the number of flux (orbitals) in each partitioin.
        AFlux = AOrbitalEnd - AOrbitalStart
        BFlux = NbrFlux  - AFlux
        BParticles = Particles - AParticles
        AFluxOffset = NbrFlux - AFlux
        BFluxOffset = NbrFlux - BFlux

        BMomentums = list()
        for AMomentum in AMomentums:
            for Momentum in Momentums:
                BMomentums.append(Momentum - AMomentum)
        BMomentums = list(set(BMomentums))

        AEffectiveMomentums = map(lambda x : (x - (AOrbitalStart * AParticles)) % NbrFlux, AMomentums)
        BEffectiveMomentums = map(lambda x : (x - (AOrbitalEnd * BParticles)) % NbrFlux, BMomentums)

        ABases = dict()
        ABasesOffsets = dict()
        AOffset = 0
        for [AEffectiveMomentum, AMomentum] in zip(AEffectiveMomentums, AMomentums):
            if not ABases.has_key(AEffectiveMomentum):
                ABasis = FQHTorusBasis(AParticles, AFlux, AEffectiveMomentum, FluxOffset = AFluxOffset); ABasis.GenerateBasis()
                if ABasis.GetDimension():
                    ABases[AEffectiveMomentum] = ABasis
                    ABasesOffsets[AEffectiveMomentum] = AOffset
                    AOffset += ABasis.GetDimension()

        BBases = dict()
        BBasesOffsets = dict()
        BOffset = 0
        for [BEffectiveMomentum, BMomentum] in zip(BEffectiveMomentums, BMomentums):
            if not BBases.has_key(BEffectiveMomentum):
                BBasis = FQHTorusBasis(BParticles, BFlux, BEffectiveMomentum, FluxOffset = BFluxOffset); BBasis.GenerateBasis()
                if BBasis.GetDimension():
                    BBases[BEffectiveMomentum] = BBasis
                    BBasesOffsets[BEffectiveMomentum] = BOffset
                    BOffset += BBasis.GetDimension()


        ADim = np.sum(map(lambda x: x.GetDimension(), ABases.values()))
        BDim = np.sum(map(lambda x: x.GetDimension(), BBases.values()))
    #
    #    print "A dim: " + str(ADim)
    #    print "B dim: " + str(BDim)

        if ADim == 0 or BDim == 0:
    #        return [None, ABasis, BBases]
            return None

        # create the matrix
        W = PETSc.Mat(); W.create()
        W.setSizes([ADim, BDim])
        W.setFromOptions()
        W.setUp()

        #cdef np.ndarray[np.complex128_t, ndim=2] W = np.zeros((ADim, BDim), dtype=np.complex128)

        TotalMask = (1l << NbrFlux) - 1l  # mask which should be all ones over all orbitals
        AMask = ((1l << AFlux) - 1l) << AOrbitalStart # mask which should be ones only over the a orbitals.
        BMask = (((1l << NbrFlux) - 1l) ^ AMask) & TotalMask # inverse of A mask so should be ones over all orbitals not in A

        for [Basis, State, Coefficient] in zip(Bases, States, Coefficients):
            if np.abs(Coefficient) > 0.0: #save some time
                i = 0
                while i < Basis.GetDimension():
                    AElement = (Basis.GetElement(i) & AMask) >> AOrbitalStart
                    BElement = ((Basis.GetElement(i) & BMask) >> AOrbitalEnd)  +  ((Basis.GetElement(i) & BMask) << (NbrFlux - AOrbitalEnd)) & TotalMask

                    AEffectiveMomentum = 0
                    j = 0
                    while j < AFlux:
                        if ((1l << j) & AElement) > 0 :
                            AEffectiveMomentum += j
                        j += 1

                    AEffectiveMomentum = AEffectiveMomentum % NbrFlux

                    BEffectiveMomentum = 0
                    j = 0
                    while j < BFlux:
                        if ((1l << j) & BElement) > 0 :
                            BEffectiveMomentum += j
                        j += 1
                    BEffectiveMomentum = BEffectiveMomentum % NbrFlux

                    if ABases.has_key(AEffectiveMomentum):
                        Row = ABases[AEffectiveMomentum].BinaryArraySearchDescendingState(AElement)
                    else:
                        Row = -1

                    if BBases.has_key(BEffectiveMomentum):
                        Col = BBases[BEffectiveMomentum].BinaryArraySearchDescendingState(BElement)
                    else:
                        Col = -1

                    if Row >= 0 and Col >= 0 and Row < ABases[AEffectiveMomentum].GetDimension() and Col < BBases[BEffectiveMomentum].GetDimension():
        #                print str(AElement) + ', ' + str(BElement) + ': ' + str(State.getValue(i))
                        W.setValue(Row+ ABasesOffsets[AEffectiveMomentum], Col + BBasesOffsets[BEffectiveMomentum], State.getValue(i) * Coefficient, True)
        #                W[Row + ABasesOffsets[AEffectiveMomentum], Col + BBasesOffsets[BEffectiveMomentum]] += State.getValue(i) * Coefficient

                    i += 1

        W.assemble()

    #    return [W, ABases, BBases]
        return W


    cpdef OrbitalEntanglementMatrices(self, State, AOrbitalStart, AOrbitalEnd, AParticles=-1, Fast = True):
        """
        Calculate the entanglement matrices due to a bipartite cut in orbital space. Return dictionary containing matrices where they keys are tuples consisting of the number of particles
        and momentum of the A partition.

        Parameters
        --------------
        State: Vec
            State to calculate the entanglement matrices for.
        AOrbitalStart: int
            The index of the first orbital in the A partition.
        AOrbitalEnd: int
            The index of the first orbital of the B partition.
        AParticles: int
            The number of particles in the A partition. If -1, then will consider all possible partitions of particles.
        Fast: bool
            Flag to indicate whether to use the faster variant (default is True).
        """
        Details = self.GetBasisDetails()
        Momentum = Details['Momentum']
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        Matrices = dict()

        # loop over sensible particle and momentum cuts
        for aParticles in ([AParticles] if AParticles != -1 else range(Particles+1)):
            for AMomentum in range(NbrFlux):
                if not Fast:
                    Matrices[(aParticles, AMomentum)] = self.OrbitalEntanglementMatrix(State, AOrbitalStart, AOrbitalEnd, AMomentum, aParticles)
                else:
                    Matrices[(aParticles, AMomentum)] = self.OrbitalEntanglementMatrixFast(State, AOrbitalStart, AOrbitalEnd, AMomentum, aParticles)

        return Matrices


    @staticmethod
    def OrbitalEntanglementMatricesSuperposition(Bases, States, Coefficients, AOrbitalStart=0, AOrbitalEnd=-1, AParticles=-1):
        """
        Calculate the entanglement matrices due to a bipartite cut in orbital space for the given
        superposition of states. Returns a dictionary containing matrices, with keys tuples consisting of
        the number of particles.

        Parameters
        -----------
        Bases: list
            List of basis objects.
        States: list
            List of state vectors.
        Coefficients: list
            List of coefficients to create superposition with.
        AOrbitalStart: int
            Starting orbital of cut (default=0).
        AOrbitalEnd: int
            Ending orbital of cut, if -1 then will be such that subsystem spanned half of the system (default=-1).
        AParticles: int
            The number of particles in the sub system. If -1 will calculate for all particle numbers (default=-1).

        Returns
        ---------
        dict of matrices
           A dictionary of petsc matcies where the key is the number of particle in subsystem A.
        """

        Details = Bases[0].GetBasisDetails()
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        Matrices = dict()

        if AOrbitalEnd == -1:
            AOrbitalEnd = (AOrbitalStart + (NbrFlux/2)) % NbrFlux;

        # loop over sensible particle and momentum cuts
        for aParticles in ([AParticles] if AParticles != -1 else range(Particles+1)):
    #        for AMomentum in range(NbrFlux):
                #Matrices[(AParticles, AMomentum)] = OrbitalEntanglementMatrix(Basis, State, AOrbitalStart, AOrbitalEnd, AMomentum, AParticles)
            Matrices[(aParticles)] = FQHTorusBasis.OrbitalEntanglementMatrixSuperposition(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, aParticles)

        return Matrices


    @staticmethod
    def OrbitalEntanglementEntropySuperposition(Bases, States, Coefficients, AOrbitalStart=0, AOrbitalEnd=-1, AParticles=-1):
        """
        Calculate the entanglement entropy due to a bipartite cut in orbital space for the given
        superposition of states.

        Parameters
        -----------
        Bases: list
            List of basis objects.
        States: list
            List of state vectors.
        Coefficients: list
            List of coefficients to create superposition with.
        AOrbitalStart: int
            Starting orbital of cut (default=0).
        AOrbitalEnd: int
            Ending orbital of cut, if -1 then will be such that subsystem spanned half of the system (default=-1).
        AParticles: int
            The number of particles in the sub system. If -1 will calculate for all particle numbers (default=-1).

        Returns
        ---------
        float
            Entanglement entropy.
        """

        Matrices = FQHTorusBasis.OrbitalEntanglementMatricesSuperposition(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, AParticles)
        SV = FQHTorusBasis.EntanglementSingularValues(Matrices)
        return FQHTorusBasis.EntanglementEntropy(SV)


    @staticmethod
    def OrbitalEntanglementMatricesSuperpositionSymmetry(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, AParticles=-1):
        """
        Calculate the entanglement matrices due to a bipartite cut in orbital space for the given
        superposition of states. Returns a dictionary containing matrices, with keys tuples consisting of
        the number of particles and momentum of parition A modulos the difference in momentum between states.
        This assumes that the momentum of states are spaced evenely and given in ascending order (as is the case with ground states on the torus).
        """

        Details = Bases[0].GetBasisDetails()
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        Dim = len(Bases)
        Momentums = list()
        for Basis in Bases:
            Details = Basis.GetBasisDetails()
            Momentums.append(Details['Momentum'])
        Momentums.sort()
        MomentumsArray = np.array(Momentums)
        MomentumDiffs = np.zeros(Dim, dtype=np.int)
        for i in range(Dim):
            MomentumDiffs[i] = (MomentumsArray[i] - MomentumsArray[(i-1) % Dim] + 2 * NbrFlux) %  NbrFlux

        # Make sure all differences are equal
        for i in range(1, Dim): assert(MomentumDiffs[i] == MomentumDiffs[i-1])

        MomentumSectors = MomentumDiffs[0]

        Matrices = dict()

        # loop over sensible particle and momentum cuts
        for aParticles in ([AParticles] if AParticles != -1 else range(Particles+1)):
            for AMomentumOffset in range(MomentumSectors):
                AMomentums = np.array(range(Dim))*MomentumSectors + AMomentumOffset
                #Matrices[(AParticles, AMomentum)] = OrbitalEntanglementMatrix(Basis, State, AOrbitalStart, AOrbitalEnd, AMomentum, AParticles)
                Matrices[(aParticles, AMomentumOffset)] = FQHTorusBasis.OrbitalEntanglementMatrixSuperpositionSymmetry(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, aParticles, AMomentums)

        return Matrices


    @staticmethod
    def EntanglementSingularValues(Matrices, MaxSingularValues=500):
        """
        Given a dictionary of entanglement matrices, this method will return arrays of singular values from which entropies and other values
        can be calculated.
        """
        cdef int i
        cdef int indx1,indx2

        SingularValues = dict()

        for key in Matrices.keys():
            if Matrices[key] is not None:
                size = Matrices[key].getSize()
                S = SLEPc.SVD(); S.create()
                S.setOperator(Matrices[key])
                S.setType(SLEPc.SVD.Type.LANCZOS)
                S.setDimensions((MaxSingularValues if min(size) > MaxSingularValues else min(size)))
                try: 
                    S.solve()
                    s = np.zeros(S.getConverged(), dtype=np.float64)
                    i = 0
                    while i < S.getConverged():
                        s[i] = S.getValue(i)
                        i += 1
                    #S.destroy()
                    s = s[s != 0.0]
                except Exception:
                    print "WARNING: Exception happened, obtaining svd using SLEPc for matrix of size", str(size)
                    print "WARNING: Falling back to obtaining svd using numpy instead"
                    ##### FIXME!!!
                    #### Replaced the SLEPc routines by numpy, since the SLEPc crashes for matrixes with many (all) zeros?!
                    ####Use Numpy instead of SLEPc since threre is a "bug" there.
                    ####Create a numpy array and fill it with the nubmers from the slepc array
                    NP_Matrix=np.zeros(size,dtype=np.complex128)
                    (nx,ny)=size
                    indx1=0
                    indx2=0
                    ###Populate the dense np matrix with elements from the PETc matrix
                    while indx1 < nx:
                        while indx2 < ny:
                            NP_Matrix[indx1,indx2] = Matrices[key][indx1,indx2]
                            indx2 += 1
                        indx1 += 1
                    #[v,s,u] = np.linalg.svd(Matrices[key])
                    U,s,V=np.linalg.svd(NP_Matrix)
                s = s[s != 0.0]
                SingularValues[key] = s
        return SingularValues
             
    
    @staticmethod
    def EntanglementEntropy(SingularValues):
        """
        Given a dictionary of entanglement matrices, this method will return the entanglement entropy, S = -tr(\rho_a log(\rho_a))
        """
        cdef double Entropy = 0.0

        for key in SingularValues.keys():
            s = SingularValues[key]
            Entropy += np.sum(-s**2 * np.log(s**2))

        return Entropy


    @staticmethod
    def EntanglementSpectrum(SingularValues):
        """
        Given a dictionary of entanglement matrices, this method will return the entanglement spectrum, as an array with corresponding particle and mometum counts.
        """
        ES = dict()

        for key in SingularValues.keys():
            s = SingularValues[key]
            ES[key] = - 2.0 * np.log(s[s != 0])

        return ES


    @staticmethod
    def EntanglementNorm(SingularValues):
        """
        Given a dictionary of entanglement matrices, this method will calculate the normal which corresponds to the trace of the density matrix.
        """
        cdef double norm = 0.0

        for key in SingularValues.keys():
            s = SingularValues[key]
            norm += np.sum(s**2)

        return norm


    def TranslateState(self, State, Amount, NewBasis=None):
        """
        Translate the given state by the amount of orbitals specified. Returns the new basis and new state.

        Parameters
        -----------
        State: petscvec
            Vector containing Fock coefficients of the state to translate.
        Amount: int
            The amount of orbitals by which to translate the state.
        NewBasis: basis object
            A precomputed basis object at the translated momentum. New basis object created if None (default=None).

        Returns
        --------
        BasisObject
            The basis object of the Hilbert space of the translated state.
        Vec
            The petsc vector object containing the Fock coefficients of the translated state.
        """
        cdef int Momentum = self.Momentum
        cdef int Particles = self.Particles
        cdef int NbrFlux = self.NbrFlux
        cdef int NewMomentum = (Momentum + (Particles * Amount)) % NbrFlux
        cdef double Phase = (-1)**(Particles-1) # the phsae that one picks up from hoping a particle around the edge

        # create and generate new basis object
        if NewBasis is None:
            NewBasis = FQHTorusBasis(Particles, NbrFlux, NewMomentum); NewBasis.GenerateBasis()

        cdef long OverflowMask = ((1l << Amount) - 1l) << (NbrFlux - Amount)
        cdef long Mask = (1l << NbrFlux) - 1l

        cdef int Dim = self.Dim #new basis will always have same dimension.
        cdef int i, j, NewIdx
        cdef long Overlap, Element, NewElement,
        cdef double ThisPhase

        rstart, rend = State.getOwnershipRange()
        cdef int LocalDim = rend - rstart
        cdef int BufferSize = LocalDim

        # allocate space to stored buffered indices and values.
        cdef np.ndarray[np.int32_t, ndim=1] Indices = np.zeros(BufferSize, dtype=np.int32)
        cdef np.ndarray[np.complex128_t, ndim=1] Values = np.zeros(BufferSize, dtype=np.complex128)
        cdef unsigned int Count = 0

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(Dim)
        NewState.setUp()
        NewState.setOption(PETSc.Vec.Option.IGNORE_NEGATIVE_INDICES, 1)

        i = rstart
        while i < rend:
            Element = self.GetElement(i)
            Overlap = (Element & OverflowMask) >> (NbrFlux - Amount)
            ThisPhase = 1.0
            j = 0
            while j < Amount:
                if ((1l << j) & Overlap) > 0:
                    ThisPhase *= Phase
                j += 1
            NewElement = ((Element << Amount) + Overlap) & Mask
            NewIdx = NewBasis.BinaryArraySearchDescendingState(NewElement)
            Indices[Count] = NewIdx
            Values[Count] = ThisPhase * State.getValue(i)
            Count += 1
            i += 1

        NewState.setValues(Indices, Values, False)
        NewState.assemble()

        return [NewBasis, NewState]


    cpdef TwoBodyOperator(self, int p, int q, int r, int s, Basis2):
        """
        This method implements a single two body intereaction, acting on the basis instance and connecting to basis2.
        O = a_p^+ a_q^+ a_r a_s
        """
        cdef int Dim1 = self.GetDimension()
        cdef int Dim2 = Basis2.GetDimension()
        cdef int rstart, rend

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([Dim1, Dim2])
        Op.setFromOptions()
        Op.setUp()
        rstart, rend = Op.getOwnershipRange()
        cdef LocalDim = rend - rstart
        Op.destroy()

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([Dim1, Dim2])
        Op.setFromOptions()
        #Op.setOption(PETSc.Vec.Option.IGNORE_NEGATIVE_INDICES, 1)
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)
        Op.setPreallocationNNZ((nnz, nnz)) # since only one term, only a single non zero element possible
        Op.setUp()

        cdef double complex Value

        cdef int Row = rstart
        cdef int Col
        cdef double Phase1
        cdef double complex Phase2
        #cdef np.ndarray[np.int32_t, ndim=1] Rows = np.zeros(LocalDim + 1, dtype=np.int32)
        #cdef np.ndarray[np.int32_t, ndim=1] Cols = np.zeros(LocalDim, dtype=np.int32)
        #cdef np.ndarray[np.complex128_t, ndim=1] Values = np.zeros(LocalDim, dtype=np.complex128)
        while Row < rend:
            Phase1 = self.AA(r, s, Row)
            if Phase1 != 0.0:
                Phase2 = self.AdAd(p, q)
                if Phase2 != 0.0:
                    #Rows[Row-rstart] = Row
                    #Cols[Row-rstart] = Basis2.BinaryArraySearchDescendingState(self.GetTmpState2())
                    #Values[Row-rstart] = Phase1 * Phase2
                    Col = Basis2.BinaryArraySearchDescendingState(self.GetTmpState2())
                    Value = Phase1 * Phase2
                    Op.setValue(Row, Col, Value, False)
            Row += 1
        #Rows[Row] = Row

        #Op.setValuesIJV(Rows, Cols, Values, False)
        Op.assemble()
        return Op


    cpdef ThreeBodyOperator(self, int o, int p, int q, int r, int s, int t, Basis2):
        """
        This method implements a single two body intereaction, acting on the basis instance and connecting to basis2.
        O = a_o^+ a_p^+ a_q^+ a_r a_s a_t
        """
        cdef int Dim1 = self.GetDimension()
        cdef int Dim2 = Basis2.GetDimension()
        cdef int rstart, rend

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([Dim1, Dim2])
        Op.setFromOptions()
        Op.setUp()
        rstart, rend = Op.getOwnershipRange()
        cdef LocalDim = rend - rstart
        Op.destroy()

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([Dim1, Dim2])
        Op.setFromOptions()
        #Op.setOption(PETSc.Vec.Option.IGNORE_NEGATIVE_INDICES, 1)
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)
        Op.setPreallocationNNZ((nnz, nnz)) # since only one term, only a single non zero element possible
        Op.setUp()

        cdef double complex Value

        cdef int Row = rstart
        cdef int Col
        cdef double Phase1
        cdef double complex Phase2
        #cdef np.ndarray[np.int32_t, ndim=1] Rows = np.zeros(LocalDim + 1, dtype=np.int32)
        #cdef np.ndarray[np.int32_t, ndim=1] Cols = np.zeros(LocalDim, dtype=np.int32)
        #cdef np.ndarray[np.complex128_t, ndim=1] Values = np.zeros(LocalDim, dtype=np.complex128)
        while Row < rend:
            Phase1 = self.AAA(r, s, t, Row)
            if Phase1 != 0.0:
                Phase2 = self.AdAdAd(o, p, q)
                if Phase2 != 0.0:
                    #Rows[Row-rstart] = Row
                    #Cols[Row-rstart] = Basis2.BinaryArraySearchDescendingState(self.GetTmpState2())
                    #Values[Row-rstart] = Phase1 * Phase2
                    Col = Basis2.BinaryArraySearchDescendingState(self.GetTmpState2())
                    Value = Phase1 * Phase2
                    Op.setValue(Row, Col, Value, False)
            Row += 1
        #Rows[Row] = Row

        #Op.setValuesIJV(Rows, Cols, Values, False)
        Op.assemble()
        return Op


    cpdef int GetStateTotalMomentum(self, long State):
        """
        Get the total momentum of the provided configuration. This is modulo the number of flux
        """
        cdef int i = 0, found = 0
        cdef int TotalMomentum = 0

        while found < self.Particles and i < self.NbrFlux:
            if State & (1l << i) > 0:
                TotalMomentum += i
                found += 1
            i += 1

        return TotalMomentum % self.NbrFlux


    cpdef int GetStateFullTotalMomentum(self, long State):
        """
        Get the total momentum of the provided configuration. No modulo operation is performed in this case
        """
        cdef int i = 0, found = 0
        cdef int TotalMomentum = 0

        while found < self.Particles and i < self.NbrFlux:
            if State & (1l << i) > 0:
                TotalMomentum += i
                found += 1
            i += 1

        return TotalMomentum


    cdef int GetStateCOM(self, long State):
        """
        Get the centre of mass orbital. Can be half integer.
        """
        cdef int i = 0, found = 0
        cdef int TotalDisplacement = 0
        cdef int Halfway = self.NbrFlux/2

        while found < self.Particles and i < self.NbrFlux:
            if State & (1l << i) > 0:
                if i > Halfway:
                    TotalDisplacement += (i - self.NbrFlux)
                else:
                    TotalDisplacement += i
                found += 1
            i += 1


        return (TotalDisplacement / np.float(self.Particles)) % self.NbrFlux


    cdef long SimpleTranslateArbitrary(self, long State, int Orbitals):
        """
        Function that translates the given configuration by the amount of orbitals specified.
        """
        Orbitals = Orbitals % self.NbrFlux

        cdef long OverFlowMask = ((1l << Orbitals) - 1l) << (self.NbrFlux - Orbitals)
        # translate everything to the left, then translate the overflow portion back to the right, and lastly take a mask of everthing.
        return  (((State << Orbitals) + ((State & OverFlowMask) >> (self.NbrFlux - Orbitals))) & self.Mask)


    cdef double TranslateArbitraryPhase(self, long State, int Orbitals):
        """
        Function that gives the phase due to parity of particles in the overflow section.
        """
        if (self.Particles % 2) == 1: # if the number of particles is odd then always return a phase of 1
            return 1.0

        Orbitals = Orbitals % self.NbrFlux

        cdef long OverFlowMask = ((1l << Orbitals) - 1l) << (self.NbrFlux - Orbitals)

        # this gets the partity of the section in question. From bithacks page https://graphics.stanford.edu/~seander/bithacks.html#ParityMultiply
        cdef unsigned long v = <unsigned long>(State & OverFlowMask)
        v ^= v >> 1;
        v ^= v >> 2;
        v = (v & 0x1111111111111111UL) * 0x1111111111111111UL;

        if ((v >> 60) & 1):
            return -1.0
        else:
            return 1.0


    cpdef np.ndarray[np.complex128_t, ndim=2] EvaluateWaveFunction(self, np.ndarray[np.complex128_t, ndim=2] Positions, States, WF, Verbose=False):
        """
        Function to calculate the value of the provided state at the given positions.

        Parameters
        ------------
        Positions: 2d complex array
            Array of positions at which to evaluate the wave-functions. Each row is a set of positions with each column the position of a different particle.
        States: vec or list of vecs
            PETSc vector object or list of PETSc vector objects containing the fock coefficients corresponding for each eigenstate.
        WF: wf object
            Wave-function object with WFValue method that will evaluate the an orbital at a given position.
        Verbose: bool
            Flag to indicate whether timings should be output on the console.

        Returns
        ----------
        2d complex array
            The wave-function values, rows corresponding to position sets and columns the different states.
        """
        PetscPrint = PETSc.Sys.Print

        # precompute the value of the single particle wave-functions at each position.
        cdef np.ndarray[np.complex128_t, ndim=2] WFValues = np.zeros((self.Particles, self.NbrFlux), dtype=np.complex128)
        cdef int i, j, k, l, m
        #cdef np.ndarray[np.complex128_t, ndim=2] Mat = np.zeros((self.Particles, self.Particles), dtype=np.complex128)
        cdef double complex Value = 0.0
        cdef double complex DetNorm, Det
        cdef int NbrStates
        cdef int rstart, rend # index for start and end of this processes ownership range.
        cdef int NbrPositions = Positions.shape[0]

        DetNorm = 1.0/np.sqrt(np.float(syp.factorial(self.Particles)))
        if isinstance(States, list):
            NbrStates = len(States)
        else:
            NbrStates = 1
            States = [States]

        rstart, rend = States[0].getOwnershipRange() # get ownership range, assume the same for all states.

        cdef np.ndarray[np.complex128_t, ndim=2] StateValues = np.zeros((rend - rstart, NbrStates), dtype=np.complex128)

        for i in range(NbrStates):
            StateValues[:, i] = States[i].array

        cdef np.ndarray[np.complex128_t, ndim=2]  Values = np.zeros((Positions.shape[0], NbrStates), dtype=np.complex128)

        MatDet = utils_cython.MatrixDeterminant(self.Particles)

        Timings = np.zeros(2)

        s = time.time()
        l = 0
        while l < NbrPositions:
            i = 0
            while i < self.Particles:
                j = 0
                while j < self.NbrFlux:
                    WFValues[i, j] = WF.WFValue(Positions[l, i], j)
                    j += 1
                i += 1

            # for each basis configuration calculate the Slater determinant
            i = 0
            while i < (rend - rstart):
                self.CalcMonomialOfElement(self.BasisElements[i + rstart])
                j = 0
                while j < self.Particles:
                    k = 0
                    while k < self.Particles:
                        MatDet.setitem(j, k, WFValues[j, self.MonomialRep[k]])
                        k += 1
                    j += 1

                #Det = np.linalg.det(Mat) * DetNorm
                Det = MatDet.det() * DetNorm

                m = 0
                while m < NbrStates:
                    Values[l, m] = Values[l, m] +  Det * StateValues[i, m]
                    m += 1
                i += 1
            l += 1
        e = time.time()
        Timings[0] = e-s
        if Verbose and PETSc.COMM_WORLD.tompi4py().Get_rank() == 0: PetscPrint('Evaluations took: ' + str(e-s) + ' seconds.')

        s = time.time()
        # must sum up the values from each process now to master process.
#        PETSc.COMM_WORLD.tompi4py().Allreduce(MPI.IN_PLACE, [Values, MPI.COMPLEX16], op=MPI.SUM)
        if PETSc.COMM_WORLD.tompi4py().Get_rank() == 0:
            PETSc.COMM_WORLD.tompi4py().Reduce(MPI.IN_PLACE, [Values, MPI.COMPLEX16], op=MPI.SUM, root=0)
        else:
            PETSc.COMM_WORLD.tompi4py().Reduce([Values, MPI.COMPLEX16], None, op=MPI.SUM, root=0)
        e = time.time()
        Timings[1] = e-s
        if Verbose and PETSc.COMM_WORLD.tompi4py().Get_rank() == 0: PetscPrint('Reduce operation took ' + str(e-s) + ' seconds.')

        return Values


    def AdA_Element(self, int ad_pos, int a_pos, unsigned long element):
        """
        Annihilate on one site and create at another on a particular element. This is a
        hopping operation. The associated phase is also calculated and returned.

        Parameters
        ------------
        ad_pos: int
            The position to annihilate at.
        a_pos: int
            The position to create at.
        element: unsigned long
            The occupation representation of the configuration (limited to 64 bits orbitals).

        Returns
        -------
        unsigned long
            The element after the operation has been performed.
        double
            The phase picked up during the operation.
        """

        if ((1ul << a_pos) & element) > 0:
            element -= (1ul << a_pos)
        else:
            print('Error: No particle on site ' + str(a_pos) + ' to annhilate.')

        if ((1ul << ad_pos) & element) == 0:
            element += (1ul << ad_pos)
        else:
            print('Error: Site ' + str(ad_pos) + ' already occupied or did not find orbital to create at.')

        cdef int mn = a_pos
        if ad_pos < a_pos:
            mn = ad_pos

        cdef int mx = ad_pos
        if a_pos > ad_pos:
            mx = a_pos

        cdef int pos = mn+1
        phase = 1.0
        while pos < mx:
            if ((1ul << pos) & element) > 0:
                phase *= -1.0
            pos+=1

        return element, phase


    def find_changed_orbitals(self, a_element, b_element, max_changes=-1):
        """
        Given the occupation representation of two elements find where the orbitals differ.

        Parameters
        -----------
        a_element: unsigned long
            The initial element.
        b_element: unsigned long
            The element we wish to transform to.
        max_changes: int
            The maximum number of changes between orbitals (nflux will be used if non provided).

        Returns
        -------
        matrix
            Matrix with details of changes, each row decribes a hopping operation.
        int
            The number of changes found.
        double
            The phase associated with the operation.
        """
        nflux = self.NbrFlux
        if max_changes == -1: max_changes=nflux
        diff = a_element ^ b_element
        annihilate = a_element & diff
        create = b_element & diff

        phase = 1.0
        # apply highest annihilation paired with highest orbital cration operator and iterate
        a_pos, b_pos = nflux-1, nflux-1

        change_idx = 0
        changes = np.zeros((max_changes, 3), np.int8)
    #     print(np.base_repr(annihilate, 2))

        while a_pos >= 0 and b_pos >= 0 and (max_changes - change_idx) > 0:
            # find next annihilation orbital
    #         print('With orbital ' + str(a_pos) + ': ' + str(np.base_repr((1 << a_pos) & annihilate, 2)))
            if ((1 << a_pos) & annihilate) > 0:
    #             print('found particle to annihilate.')
                # find next creation orbital
                while b_pos >= 0:
                    if ((1 << b_pos) & create) > 0:
                        break
                    b_pos -= 1
                a_element, phase_change = self.AdA_Element(b_pos, a_pos, a_element)
                phase *= phase_change
                changes[change_idx,:] = [0, a_pos, b_pos]
                change_idx += 1
                b_pos -= 1
            a_pos -= 1

        return changes, change_idx, phase


    def find_all_changing_orbitals(self, start=0, end=-1):
        """
        Method to find all the changes in a the given basis for certain subset of elements. Calls the
        find changed orbitals method for each pair of consecutive elements.

        Parameters
        ----------
        start: int
            The starting element (default=0).
        end: int
            The ending element, -1 for all (default=-1).

        Returns
        ---------
        tensor
            Array of matrices containing details of the hopping operations between elements.
        array
            The number of operations between each pair of consecutive elements.
        array
            The phase picked up for each transition.
        """
        Ne = self.Particles
        nflux = self.NbrFlux
        dim = self.GetDimension()
        if end == -1: end = dim
        dim = end - start

        orbitals_to_row_map = {}
        a_element = self.GetElement(start)

        a_orbitals = self.GetMonomialOfElement(a_element)   # we keep this sorted in ascending order
        for j in range(len(a_orbitals)): orbitals_to_row_map[a_orbitals[j]] = j

        change_counts = np.zeros(dim, dtype=np.int8)
        changes = np.zeros((dim, Ne, 3), dtype=np.int8)
        phases = np.ones(dim, dtype=np.float64)

        i = 1
        while i < dim:
            b_element = self.GetElement(start+i)
            changes[i,:], change_counts[i], phases[i] = self.find_changed_orbitals(a_element, b_element, Ne)
            # fill in the rows
    #         print (orbitals_to_row_map)
    #         print (changes[i,:])
            for change_idx in range(change_counts[i]):
                row = orbitals_to_row_map[changes[i,change_idx,1]]
                del orbitals_to_row_map[changes[i,change_idx,1]]
                orbitals_to_row_map[changes[i,change_idx,2]] = row
                changes[i,change_idx,0] = row
            a_element = b_element
            i += 1

        return changes, change_counts, phases


    def new_determinant(self, det_A, inv_A, u, v):
        """
        Method that calculates the new determinant and new inverse using the Sherman Morrison theorm
        when a matrix is changed by the addition of vectors u and v.

        Parameters
        -----------
        det_A: double complex
            The initial determinant of A.
        inv_A: matrix
            The initial inverse matrix for A.
        u: vector
            The u vector that updates A.
        v: vector
            The v vector that updates A.

        Returns
        ---------
        float
            New determinant
        matrix
            The new inverse matrix
        """
        vTAinv = np.dot(v.T, inv_A)
        factor = (1 + np.dot(vTAinv, u))
        det_new = factor*det_A
        inv_new = inv_A - np.dot(np.dot(inv_A, u), vTAinv)/factor
        return det_new[0][0], inv_new


    def new_determinant_fast(self, det_A, inv_A, u_nnz, v):
        """
        Method that calculates the new determinant and new inverse using the Sherman Morrison theorm
        when a matrix is changed by the addition of vectors u and v. In this particular version we know
        that u only contains a single non zero element and so can do this more efficiently if we only
        consider this.

        Parameters
        -----------
        det_A: double complex
            The initial determinant of A.
        inv_A: matrix
            The initial inverse matrix for A.
        u_nnz: int
            The index of the nonzero element of u.
        v: vector
            The v vector that updates A.

        Returns
        ---------
        float
            New determinant
        matrix
            The new inverse
        """
        Ainv_u = (inv_A[:,u_nnz]).reshape(v.shape) # just picks column of inv_A
    #     print Ainv_u.shape
        factor = (1 + np.dot(v.T, Ainv_u)[0][0])
        det_new = factor*det_A
        inv_new = inv_A - np.dot(Ainv_u, np.dot(v.T, inv_A))/factor
        return det_new, inv_new


    cpdef np.ndarray[np.complex128_t, ndim=2] EvaluateWaveFunctionShermanMorisson(self, np.ndarray[np.complex128_t, ndim=2] Positions, States, WF, Verbose=False):
        """
        Function to calculate the value of the provided state at the given positions making use of the Sherman Morisson theorem so that full determinant not
        calculated each time.
        FIXME: Implementation currently very inefficient due to calls to numpy for linear algebra operations.
        Can be addressed using methods discussed at http://stackoverflow.com/questions/16114100/calling-dot-products-and-linear-algebra-operations-in-cython
        and http://stackoverflow.com/questions/31485576/fast-basic-linear-algebra-in-cython-for-recurrent-calls

        Parameters
        ------------
        Positions: 2d complex array
            Array of positions at which to evaluate the wave-functions. Each row is a set of positions with each column the position of a different particle.
        States: vec or list of vecs
            PETSc vector object or list of PETSc vector objects containing the fock coefficients corresponding for each eigenstate.
        WF: wf object
            Wave-function object with WFValue method that will evaluate the an orbital at a given position.
        Verbose: bool
            Flag to indicate whether timings should be output on the console.

        Returns
        ----------
        2d complex array
            The wave-function values, rows corresponding to position sets and columns the different states.
        """
        PetscPrint = PETSc.Sys.Print

        # precompute the value of the single particle wave-functions at each position.
        cdef np.ndarray[np.complex128_t, ndim=2] WFValues = np.zeros((self.NbrFlux, self.Particles), dtype=np.complex128)
        cdef int i, j, k, l, m
        cdef double complex Value = 0.0
        cdef double complex DetNorm, Det
        cdef int NbrStates
        cdef int rstart, rend # index for start and end of this processes ownership range.
        cdef int NbrPositions = Positions.shape[0]
        cdef int Particles

        Particles = self.Particles
        DetNorm = 1.0/np.sqrt(np.float(syp.factorial(Particles)))
        if isinstance(States, list):
            NbrStates = len(States)
        else:
            NbrStates = 1
            States = [States]

        rstart, rend = States[0].getOwnershipRange() # get ownership range, assume the same for all states.

        cdef np.ndarray[np.complex128_t, ndim=2] StateValues = np.zeros((rend - rstart, NbrStates), dtype=np.complex128)

        for i in range(NbrStates):
            StateValues[:, i] = States[i].array

        # Create buffer to return the results in
        cdef np.ndarray[np.complex128_t, ndim=2]  Values = np.zeros((Positions.shape[0], NbrStates), dtype=np.complex128)

        cdef np.ndarray[np.int8_t, ndim=1] change_counts
        cdef np.ndarray[np.int8_t, ndim=3] changes
        cdef np.ndarray[np.float64_t, ndim=1] phases
        s=time.time()
        changes, change_counts, phases = self.find_all_changing_orbitals(rstart, rend)
        e=time.time()
        if Verbose and PETSc.COMM_WORLD.tompi4py().Get_rank() == 0: PetscPrint('Took ' + str(e-s) + ' to find changes between orbitals in advance of using Sherman-Morrison method.')
        #print(change_counts)
        #print(changes)
        #print('Phases: ' + str(phases))

        # now we find the determinant and inverse of the first matrix
        cdef np.ndarray[np.complex128_t, ndim=2] Mat = np.zeros((Particles, Particles), dtype=np.complex128)
        cdef np.ndarray[np.complex128_t, ndim=2] u = np.zeros((Particles, 1), dtype=np.complex128)
        cdef np.ndarray[np.complex128_t, ndim=2] v = np.zeros((Particles, 1), dtype=np.complex128)
        cdef double complex A_det, factor
        cdef np.ndarray[np.complex128_t, ndim=2] A_inv

        # create some pure c data structures for performing Sherman Morrison updates
        cdef double complex *A_inv_c # will store pointers to a matrix (column ordered) and vector
        cdef double complex *v_c
        cdef double complex *vT_A_inv_c
        cdef int u_col                 # index of the non zero u value
        cdef double complex *A_inv_u_c # will point to column of A_inv_c matrix

        A_inv_c = <double complex*>PyMem_Malloc(Particles * Particles * sizeof(double complex))
        v_c = <double complex*>PyMem_Malloc(Particles * sizeof(double complex))
        A_inv_u_c = <double complex*>PyMem_Malloc(Particles * sizeof(double complex))
        vT_A_inv_c = <double complex*>PyMem_Malloc(Particles * sizeof(double complex))

        Timings = np.zeros(2)

        s = time.time()
        l = 0
        while l < NbrPositions:
            i = 0
            while i < Particles:
                j = 0
                while j < self.NbrFlux:
                    WFValues[j, i] = WF.WFValue(Positions[l, i], j)
                    j += 1
                i += 1

            orbitals = self.GetMonomialOfElement(self.GetElement(rstart))
            for j in range(Particles):
                Mat[j,:] = WFValues[orbitals[j], :]

            A_det = np.linalg.det(Mat)
            A_inv = np.linalg.inv(Mat)

            i = 0
            while i < Particles:
                j = 0
                while j < Particles:
                    A_inv_c[i*Particles+j] = A_inv[j,i] # copy the matrix but put it in column ordered form
                    j += 1
                i += 1

            Det = DetNorm * A_det
#            print '0: ' + str(Det)
            m = 0
            while m < NbrStates:
                Values[l, m] = Det * StateValues[rstart, m]
                m += 1 # move to next state

            # for each basis configuration calculate the Slater determinant
            i = 1
            while i < (rend - rstart):
                j = 0
                while j < change_counts[i]:
#                    u[changes[i,j,0],0] = 1.0
#                    v[:,0] = WFValues[changes[i,j,2],:] - WFValues[changes[i,j,1],:]
#                    vTAinv = np.dot(v.T, A_inv)
                    #factor = (1 + np.dot(vTAinv, u)[0][0])
                    #A_det = (factor*A_det)
                    #A_inv = A_inv - np.dot(np.dot(A_inv, u), vTAinv)/factor
                    #u[changes[i,j,0],0] = 0.0 # reset row selection back to zero

                    # populate v with difference between rows
                    u_col = changes[i,j,0]
                    k = 0
                    while k < Particles:
                        v_c[k] = WFValues[changes[i,j,2],k] - WFValues[changes[i,j,1],k]
                        A_inv_u_c[k] = A_inv_c[u_col*Particles+k]
                        k+=1

                    #A_inv_u_c = &A_inv_c[changes[i,j,0]*Particles] # set to point to column which is picked out by non zero element of u

                    # calculate 1 + vT*A_inv*u
                    k = 0
                    factor = 1.0
                    while k < Particles:
                        factor += v_c[k]*A_inv_u_c[k]
                        k+=1
                    A_det = A_det * factor

                    # calculate vT*A_inv (vector x matrix)
                    k = 0
                    while k < Particles:
                        m = 0
                        vT_A_inv_c[k] = 0.0
                        while m < Particles:
                            vT_A_inv_c[k] += v_c[m] * A_inv_c[k*Particles+m]
                            m += 1
                        k += 1

#                    print('Before correct Ainv: ' + str(A_inv))
#
#                    output = 'Before attempt Ainv: '
#                    k=0
#                    while k < Particles:
#                        m = 0
#                        while m < Particles:
#                            output = output + str(A_inv_c[m*Particles+k]) + ' '
#                            m += 1
#                        output = output + '\n'
#                        k += 1
#                    print(output)

                    # calculate new A_inv = A_inv - A_inv * u * vT * A_inv/(1 + vT*A_inv*u)
                    k = 0
                    while k < Particles:
                        m = 0
                        while m < Particles:
                            A_inv_c[k*Particles+m] = A_inv_c[k*Particles+m] - A_inv_u_c[m] * vT_A_inv_c[k]/factor
                            m += 1
                        k += 1

#                    A_inv = A_inv - np.dot(np.dot(A_inv, u), vTAinv)/factor
#                    u[changes[i,j,0],0] = 0.0 # reset row selection back to zero

#                    print('Correct Ainv: ' + str(A_inv))
#                    output = 'Attempt Ainv: '
#                    k=0
#                    while k < Particles:
#                        m = 0
#                        while m < Particles:
#                            output = output + str(A_inv_c[m*Particles+k]) + ' '
#                            m += 1
#                        output = output + '\n'
#                        k += 1
#                    print(output)





                    j += 1

                A_det *= phases[i]
                Det = A_det * DetNorm
#                print str(i) + ': ' + str(Det)
                m = 0
                while m < NbrStates:
                    Values[l, m] = Values[l, m] +  Det * StateValues[i, m]
                    m += 1 # move to next state
                i += 1 # move to next fock configuration
            l += 1 # move to next position
        e = time.time()
        Timings[0] = e-s
        if Verbose and PETSc.COMM_WORLD.tompi4py().Get_rank() == 0: PetscPrint('Evaluations took: ' + str(e-s) + ' seconds.')

        s = time.time()
        # must sum up the values from each process now to master process.
#        PETSc.COMM_WORLD.tompi4py().Allreduce(MPI.IN_PLACE, [Values, MPI.COMPLEX16], op=MPI.SUM)
        if PETSc.COMM_WORLD.tompi4py().Get_rank() == 0:
            PETSc.COMM_WORLD.tompi4py().Reduce(MPI.IN_PLACE, [Values, MPI.COMPLEX16], op=MPI.SUM, root=0)
        else:
            PETSc.COMM_WORLD.tompi4py().Reduce([Values, MPI.COMPLEX16], None, op=MPI.SUM, root=0)
        e = time.time()
        Timings[1] = e-s
        if Verbose and PETSc.COMM_WORLD.tompi4py().Get_rank() == 0: PetscPrint('Reduce operation took ' + str(e-s) + ' seconds.')

        PyMem_Free(A_inv_c)
        PyMem_Free(A_inv_u_c)
        PyMem_Free(v_c)
        PyMem_Free(vT_A_inv_c)

        return Values


    cpdef np.ndarray[np.complex128_t, ndim=2] EvaluateSlaterDeterminants(self, np.ndarray[np.complex128_t, ndim=2] Positions, WF, Verbose=False):
        """
        Function to calculate the value of all slater determinants at the given positions.

        Parameters
        ------------
        Positions: 2d complex array
            Array of positions at which to evaluate the wave-functions. Each row is a set of positions with each column the position of a different particle.
        Verbose: bool
            Flag to indicate whether timings should be output on the console.

        Returns
        ----------
        2d complex array
            The wave-function values, rows corresponding to position sets and columns the different states.
        """
        PetscPrint = PETSc.Sys.Print

        # precompute the value of the single particle wave-functions at each position.
        cdef np.ndarray[np.complex128_t, ndim=2] WFValues = np.zeros((self.Particles, self.NbrFlux), dtype=np.complex128)
        cdef int i, j, k, l, m
        cdef double complex Value = 0.0
        cdef double complex DetNorm, Det
        cdef int NbrPositions = Positions.shape[0]

        DetNorm = 1.0/np.sqrt(np.float(syp.factorial(self.Particles)))
        Dim = self.GetDimension()

        Values = np.zeros((Positions.shape[0], Dim), dtype=np.complex128)

        MatDet = utils_cython.MatrixDeterminant(self.Particles)

        Timings = np.zeros(2)

        s = time.time()
        l = 0
        while l < NbrPositions:
            i = 0
            while i < self.Particles:
                j = 0
                while j < self.NbrFlux:
                    WFValues[i, j] = WF.WFValue(Positions[l, i], j)
                    j += 1
                i += 1

            # for each basis configuration calculate the Slater determinant
            i = 0
            while i < Dim:
                self.CalcMonomialOfElement(self.BasisElements[i])
                j = 0
                while j < self.Particles:
                    k = 0
                    while k < self.Particles:
                        MatDet.setitem(j, k, WFValues[j, self.MonomialRep[k]])
                        k += 1
                    j += 1

                Det = MatDet.det() * DetNorm
                Values[l, i] = Det
                i += 1
            l += 1
        e = time.time()
        Timings[0] = e-s
        if Verbose and PETSc.COMM_WORLD.tompi4py().Get_rank() == 0: PetscPrint('Evaluations took: ' + str(e-s) + ' seconds.')

        return Values


    cpdef int TooManyConsecutiveParticlesOrHoles(self, long Element, int ConsecutiveParticles, int ConsecutiveHoles):
        """
        Function that checks the whether the number of consecutive holes or particles exceeds the specified amount.

        Parameters
        -----------
        Element: long
            The element to check
        ConsecutiveParticles: int
            Number of consecutive particles to exclude a configuration. -1 to include all configurations.
        ConsecutiveHoles: int
            Number of consecutive holes to exclude a configuration. -1 to include all configurations.

        """
        cdef int i
        cdef int Ps = 0
        cdef int Hs = 0
        cdef int MaxPs = 0
        cdef int MaxHs = 0
        cdef int prev, curr
        cdef int Max


        #first find a site where there is a particle neighbouring a hole
        i = 1
        prev = ((Element & 1l) > 0l)
        while i < self.NbrFlux:
            curr = ((Element & (1l << i)) > 0l)
            if prev != curr:
                break;
            prev = curr
            i += 1

#        print i

        Max = self.NbrFlux + i
        while i < Max:
            if (Element & (1l << (i % self.NbrFlux))) > 0l:
                Ps += 1
                if Hs > MaxHs:
                    MaxHs = Hs
                Hs = 0
            else:
                Hs += 1
                if Ps > MaxPs:
                    MaxPs = Ps
                Ps = 0
            i += 1


        if Ps > MaxPs:
            MaxPs = Ps

        if Hs > MaxHs:
            MaxHs = Hs

        if MaxHs >= ConsecutiveHoles or MaxPs >= ConsecutiveParticles:
            return 1
        else:
            return 0


    def ApproximateBasisPy(self, ConsecutiveParticles, int ConsecutiveHoles):
        self.ApproximateBasis(ConsecutiveParticles, ConsecutiveHoles)

    cdef ApproximateBasis(self, int ConsecutiveParticles, int ConsecutiveHoles):
        """
        Function which creates a basis subset by excluding configurations with at least a given number of consecutive particles/holes.

        Parameters
        -----------
        ConsecutiveParticles: int
            Number of consecutive particles to exclude a configuration. -1 to include all configurations.
        ConsecutiveHoles: int
            Number of consecutive holes to exclude a configuration. -1 to include all configurations.
        """
        cdef int i

        if not self.ApproximateBasisFlag:
            self.FullDim = self.Dim
            self.Dim = 0
            i = 0
            while i < self.FullDim:
                if not self.TooManyConsecutiveParticlesOrHoles(self.BasisElements[i], ConsecutiveParticles, ConsecutiveHoles):
                    self.Dim += 1
                i += 1

            self.TmpElements = <long *>PyMem_Malloc(self.Dim * sizeof(long))
            self.ElementMap = <int *>PyMem_Malloc(self.Dim * sizeof(int))

            self.Dim = 0
            i = 0
            while i < self.FullDim:
                if not self.TooManyConsecutiveParticlesOrHoles(self.BasisElements[i], ConsecutiveParticles, ConsecutiveHoles):
                    self.TmpElements[self.Dim] = self.BasisElements[i]
                    self.ElementMap[self.Dim] = i
                    self.Dim += 1
                i += 1

            PyMem_Free(self.BasisElements)
            self.BasisElements = self.TmpElements

            self.ApproximateBasisFlag = 1


    cdef int FindPositionInSortedArray(self, int *Array, int N, int Element):
        """
        Find the position at which the given element appears in the array provided, or the
        position of the largest array element less than the provided element.

        Parameters
        ------------
        Array: int*
            Array of integers.
        N: int
            Size of the array.
        Element: int
            The element to search for.
        """
        cdef int start, end, dist
        cdef int TestElement

        start = 0
        end = N
        dist = end - start
        while dist > 1:
            TestElement = Array[start + dist/2]
            if TestElement == Element:
                return start + dist/2
            elif TestElement < Element:
                start = start + dist/2
            else:
                end = start + dist/2
            dist = end - start

        return start


    cdef int FindPositionInMapArray(self, int Element):
        """
        Find the position at which the given element in the map element array.

        Parameters
        ------------
        Element: int
            The element to search for.
        """
        return self.FindPositionInSortedArray(self.ElementMap, self.Dim, Element)


    cpdef ApproximateStateToFullState(self, State):
        """
        Given a state in the approximate basis, expand to the full basis.

        Parameters
        ----------
        State: Vec
            The approximate state to expand.
        """
        cdef int rstart, rend, LocalDim, StateDim, i, MapStart
        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(self.FullDim)
        NewState.setUp()
        rstart, rend = NewState.getOwnershipRange()
        LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)
        StateDim = State.getSize()

        #if state not approximated, then return none
        if not self.ApproximateBasisFlag or StateDim != self.Dim:
            return None
        else:
            Op = PETSc.Mat(); Op.create()
            Op.setSizes([self.FullDim, self.Dim])
            Op.setFromOptions()
            Op.setPreallocationNNZ((nnz, nnz)) # one term per sector
            Op.setUp()

            MapStart = self.FindPositionInSortedArray(self.ElementMap, self.Dim, rstart)
            i = MapStart
            while i < self.Dim and self.ElementMap[i] < rend:
                Op.setValue(self.ElementMap[i], i, 1.0)
                i += 1

            Op.assemble()

            Op.mult(State, NewState)

        return NewState


    def IsApproximateBasis(self):
        return self.ApproximateBasisFlag


    def GetPHSector(self):
        """
        Get the particle number and momentum sector in which to find the PH conjugated basis.

        Returns
        -------
        list
            List containing particle number and momentum at which to find PH conjugated basis.
        """
        # calculate momentum of PH conjugate by subtracting current momentum from fully filled momentum
        PHMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.Momentum) % self.NbrFlux
        PHParticles = self.NbrFlux - self.Particles
        return PHParticles, PHMomentum


    cpdef ConvertToPHConjugate(self, State, PHBasis=None, ReturnOp=False):
        """
        This method converts the state given into the particle hole conjugate state.

        Parameters
        ------------
        State: vec
            Vector object of state to convert.
        PHBasis: basis
            Basis object of PH conjugate or None if not given (default=None).
        ReturnOp: bool
            Flag to indicate whether to return the operator that transforms a state to
            its particle hole conjugate (default=False).

        Returns
        ----------
        basis
            Basis object of PH conjugate state.
        vec
            Vector containing coefficients of PH conjugate state.
        mat
            The operator that transforms the state, only returned in ReturnOp is True.
        """
        # calculate momentum of PH conjugate by subtracting current momentum from fully filled momentum
        PHMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.Momentum) % self.NbrFlux
        PHParticles = self.NbrFlux - self.Particles

        if PHBasis is None:
            PHBasis = FQHTorusBasis(PHParticles, self.NbrFlux, PHMomentum)
            PHBasis.GenerateBasis()

        cdef int BasisDim = self.Dim
        cdef int rstart, rend, Idx
        rstart, rend = State.getOwnershipRange()
        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([(LocalDim, BasisDim), BasisDim])
        Op.setFromOptions()
        Op.setPreallocationNNZ((nnz, nnz)) # one term per sector
        Op.setUp()

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(BasisDim)
        NewState.setUp()

        cdef double phase

        cdef int i = rstart
        while i < rend:
            Element = self.GetElement(i)
            if self.CalcParityOfElement(Element) == 1:
                phase = -1.0
            else:
                phase = 1.0
            # take inverse
            Element = (~Element) & self.Mask
            Idx = PHBasis.BinaryArraySearchDescendingState(Element)
            if Idx >= 0 and Idx < BasisDim:
                Op.setValue(i, Idx, phase, True)
            i += 1
        Op.assemble()

        Op.multTranspose(State, NewState)

        NewState.conjugate()

        if ReturnOp:
            return [PHBasis, NewState, Op]
        else:
            return [PHBasis, NewState]


    cpdef int CalcParityOfElement(self, long Element):
        """
        Return partiy of sum of non zero element positions.

        Parameters
        -----------
        Element: long
            Long number representing the element.

        Returns
        --------
        int
            Parity, either 0 or 1
        """
        cdef parity = 0
        cdef int i, j

        j = 0
        i = 0
        while i < self.NbrFlux:
            if ((1l << i) & Element) > 0:
                parity = (parity + i) % 2
                j += 1
                if j >= self.Particles:
                    return parity
            i += 1
        return parity


    def GetInvSector(self):
        """
        Get the momentum sector inverted basis.

        Returns
        -------
        list
            List containing momentum at which to find the inverted basis.
        """
        IMomentum = (2*self.NbrFlux - self.Momentum - self.Particles) % self.NbrFlux
        return IMomentum,


    cpdef ConvertToInvertedState(self, State, IBasis=None, ReturnOp=False):
        """
        This method inverts the given state by reflecting all orbitals around the central orbital.

        Parameters
        ------------
        State: vec
            Vector object of state to convert.
        IBasis: basis
            Basis object of PH conjugate or None if not given (default=None).
        ReturnOp: bool
            Flag to indicate whether to return the operator that transforms a state to
            its particle hole conjugate (default=False).

        Returns
        ----------
        basis
            Basis object of PH conjugate state.
        vec
            Vector containing coefficients of PH conjugate state.
        mat
            The operator that transforms the state, only returned in ReturnOp is True.
        """
        # calculate momentum of inverted state
        IMomentum = (2*self.NbrFlux - self.Momentum - self.Particles) % self.NbrFlux

        if IBasis is None:
            IBasis = FQHTorusBasis(self.Particles, self.NbrFlux, IMomentum)
            IBasis.GenerateBasis()

        cdef int BasisDim = self.Dim
        cdef int rstart, rend, Idx
        rstart, rend = State.getOwnershipRange()
        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([(LocalDim, BasisDim), BasisDim])
        Op.setFromOptions()
        Op.setPreallocationNNZ((nnz, nnz)) # one term per sector
        Op.setUp()

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(BasisDim)
        NewState.setUp()


        cdef int i = rstart
        while i < rend:
            Element = self.GetElement(i)
            # take inverse
            Element = self.InvertBits(Element)
            Idx = IBasis.BinaryArraySearchDescendingState(Element)
            if Idx >= 0 and Idx < BasisDim:
                Op.setValue(i, Idx, 1.0, True)
            i += 1
        Op.assemble()

        Op.multTranspose(State, NewState)


        if ReturnOp:
            return [IBasis, NewState, Op]
        else:
            return [IBasis, NewState]


    cpdef long InvertBits(self, long State):
        """
        Method to invert the orbitals. For a particle in orbital u, move it to orbital Nphi - u.

        Parameters
        -----------
        State: long
            The Fock configuration in binary form.

        Returns
        -------
        long
            The inverted binary string.
        """
        cdef unsigned long NewState = 0l
        cdef int i

        # Reflect around system COM
        i = 0
        while i < self.BytesToReverse:
            NewState |= (<long>BitReverseTable256[(State >> (i*8)) & 0xff]) << (64 - (i+1)*8)
            i += 1
        NewState = NewState >> self.ShiftAfterReverse
        return <long>NewState

    def OrbitalDensityState(self, state):
        """
        Return the probability of finding each orbital occupied
        
        Paramters
        ---------
        state: petsc vector or numpy array
        """
        if not isinstance(state, np.ndarray):
            state = state.array
        dim = self.GetDimension()

        ###Set up the occupation vector
        Occupation=np.zeros(self.NbrFlux,dtype=float)
        for i in range(dim):
            Occupation+=self.GetOccupationVectorState(i)*(np.abs(state[i]))**2
        return Occupation
            

        
    def PrettyPrintState(self, state, abs=False, sort=False, phase=False, rotate=False):
        """
        Print the given state with additional basis information.

        Parameters
        -----------
        state: petsc vector or numpy array
            State to print.
        abs: bool
            Flag to print absolute value.
        sort: bool
            Flag
        phase: bool
            Flag to indicate that phase printed too.
        rotate: bool
            Flag to indicate that coefficients should be rotated such that element with maximum magnitude is real.
        """

        if not isinstance(state, np.ndarray):
            state = state.array

        dim = self.GetDimension()

        if sort:
            idxs = np.argsort(np.abs(state))
        else:
            idxs = range(dim)

        if rotate:
            max_idx = np.argmax(np.abs(state))
            state = state*np.exp(-1j*np.angle(state[max_idx]))

        for i in idxs:
            if abs:
                print(self.GetOccupationRep(i) + ':' + str(np.abs(state[i]))
                   + ('' if not phase else ' (' + str(np.angle(state[i]))+ ')'))
            else:
                print(self.GetOccupationRep(i) + ':' + str(state[i])
                   + ('' if not phase else ' (' + str(np.angle(state[i]))+ ')'))


    def PrettyPrintStateDiff(self, state1, state2, sort=False, tol=1e-8, abs=False):
        """
        Print differing coefficients with additional basis information.

        Parameters
        -----------
        state1: petsc vector or numpy array
            State 1 to compare
        state2: petsc vector or numpy array
            State 2 to compare
        sort: bool
            Flag to indicate that sorting by first state
        """

        if not isinstance(state1, np.ndarray):
            state1 = state1.array

        if not isinstance(state2, np.ndarray):
            state2 = state2.array

        if abs:
            state1 = np.abs(state1)
            state2 = np.abs(state2)

        dim = self.GetDimension()

        if sort:
            idxs = np.argsort(np.abs(state1))
        else:
            idxs = range(dim)

        for i in idxs:
            if np.abs(state1[i] - state2[i]) > tol:
                print(self.GetOccupationRep(i) + ':' + str(state1[i]) + ', ' + str(state2[i]))


    def PrettyPrintBasis(self, Representative=False):
        PetscPrint = PETSc.Sys.Print
        if not Representative:
            for i in range(0,self.GetDimension()):
                PetscPrint(self.GetOccupationRep(i))
        else:
            count = 0
            for i in range(0,self.GetDimension()):
                if self.RepresentativeConfiguration(i) == i:
                    PetscPrint(self.GetOccupationRep(i))
                    count += 1
            PetscPrint("Number configurations: " + str(count))



    def GetSingleParticleWF(self,Tau=1j):
        """
        Return an object for calculating single particle wave-functions with current basis settings.

        Returns
        --------
        wf object
            A class instance which has a WFValue(z, orbital) function.
        """
        return FQHTorusWF(self.NbrFlux, Tau)


    def ApplyDensityOperator(self,State,int K1shift,int K2shift):
        """
        Applies the density operator
        rho(k1,k2) = \sum_n  exp(-i*2*pi*k2*n/Ns) a^dagger_(n+k1) a_n 
        to the input state.

        Parameters
        -----------
        State: petsc vector or numpy array
            State to act on
        k1: int
            Momentum index in K1 direction
        k2: int
            Momentum index in K2 direction
        """
        
        #print "The input vector:",State.getArray()
        cdef int i, pos, NewDimention, OldDimention
        cdef int NumElec, ElecPos, TargetPos, StartingPos
        cpdef long NewElement, BasisElement,NewVectorIndx
        cpdef double complex itwopi, StateWeight, Phase
        ##Memory wiew for electorn positions
        cdef long[:] ElecPositions
        cdef double complex[:] NewVector
        cpdef double twopi, Angle , OneOverNs

        OneOverNs=1.0/(1.0*self.NbrFlux)

        twopi=2*np.pi
        itwopi=1j*2*np.pi
        if MPI.COMM_WORLD.Get_size() > 1:
            print "WARNING: More than one (actually",MPI.COMM_WORLD.Get_size(),") processes running"
            print "WARNING: The function ApplyDensityOperator is not thread-safe"
            raise Exception, "The ApplyDensityOperator is not threadsafe"
            
        ## Generate a basis for the new state
        ShiftedBasis=FQHTorusBasis(self.Particles,self.NbrFlux,self.Momentum+K1shift)
        ShiftedBasis.GenerateBasis()

        OldDimention = self.GetDimension()
        NewDimention = ShiftedBasis.GetDimension()
        
        ###Create a new vector to transform to
        NewVector = np.zeros(NewDimention,dtype=np.complex)
        
        #print "Apply the density operator:"
        ## Loop over the states in the basis, and figure out how the transform acts
        for i in range(OldDimention):
            #print "Basis State no",i,"of",self.GetDimension()
            ###Loop over the electron positions
            ElecPositions=self.GetOccupationVectorState(i)
            BasisElement=self.GetElement(i)
            StateWeight=State.getValue(i)
            #print "State representation:"
            #self.PrintOccupationRep(i)
            #print "BasisElement",BasisElement
            #print "StateWeight",StateWeight
            for StartingPos in range(0,self.NbrFlux):
                #print "  -------"
                TargetPos=(StartingPos+K1shift) % self.NbrFlux
                if TargetPos < 0:
                    TargetPos = TargetPos + self.NbrFlux

                Angle=StartingPos*K2shift*twopi*OneOverNs
                #print "  K2shift:",K2shift
                #print "  StartingPos:",StartingPos
                #print "  TargetPos:",TargetPos
                #print "  twopi:",twopi
                #print "  OneOverNs:",OneOverNs
                #print "  Angle:",Angle
                Phase=cos(Angle)+1j*sin(Angle)
                #print "  Phase:",Phase
                #Phase=np.exp(1j*Angle)
                if TargetPos==StartingPos and ElecPositions[StartingPos]==1:
                    #print "  An electron is present at pos",TargetPos
                    NewVectorIndx=i
                    NewVector[NewVectorIndx]=NewVector[NewVectorIndx]+Phase*StateWeight
                elif ElecPositions[StartingPos]==1 and ElecPositions[TargetPos]==0:
                    #print " Move possible from pos",pos,"to pos",TargetPos
                    #print "  Count electrons between the two positions"
                    NumElec=0
                    for ElecPos in range(min(StartingPos,TargetPos)+1,max(StartingPos,TargetPos)):
                        NumElec+=ElecPositions[ElecPos]
                    #print "  Found",NumElec,"electrons"
                    #print "  Translating into ",(-1)**NumElec
                    NewElement=BasisElement-2**StartingPos+2**TargetPos
                    #print "NewElement:",NewElement
                    ###Now we find this elment in the listgs of the new basis
                    NewVectorIndx=ShiftedBasis.GetElementIdx(NewElement)
                    #print "NewVectorIndx:",NewVectorIndx
                    #NewElecPositions=ShiftedBasis.GetOccupationVectorState(NewVectorIndx)
                    #print "New State representation",NewElecPositions
                    NewVector[NewVectorIndx]=NewVector[NewVectorIndx]+Phase*(-1)**NumElec*StateWeight
        return ShiftedBasis, PETSc.Vec().createWithArray(NewVector)
