#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHSphereBasis.pyx: This is a cython implementation of code to work with the FQH basis on a Sphere. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
cimport numpy as np
from FQHSphereWF import FQHSphereWF
import time
import scipy.linalg.lapack
import sympy as syp
from petsc4py import PETSc
from slepc4py import SLEPc

cdef extern from "f2pyptr.h":
    void *f2py_pointer(object) except NULL

ctypedef int zgetrf_t(
    int *M, int *N,
    double complex *A,
    int *LDA,
    int *IPIV,
    int *INFO)


cdef class FQHSphereBasis(FQHTorusBasis):

    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.TwiceLz = 0
        self.TwiceMaxLz = 0
        self.EffectiveMomentum = 0


    def __init__(self, int Particles, int TwiceMaxLz, int TwiceLz):
        """
        Set the variables
        """
        self.Particles = Particles
        self.TwiceMaxLz = TwiceMaxLz
        self.TwiceLz = TwiceLz
        self.NbrFlux = TwiceMaxLz + 1
        self.Dim = 0
        self.EffectiveMomentum = (self.TwiceLz + self.Particles * self.TwiceMaxLz)/2
        super().__init__(Particles, self.NbrFlux, self.EffectiveMomentum, self.Particles * self.NbrFlux)


    def GetSingleParticleWF(self):
        """
        Return an object for calculating single particle wave-functions with current basis settings.

        Returns
        --------
        wf object
            A class instance which has a WFValue(z, orbital) function.
        """
        return FQHSphereWF(self.TwiceMaxLz)
