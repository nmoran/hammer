#! /usr/bin/env python


""" CalcPlotCoefficients.py: Utility to plot interaction coefficients. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


#from InteractionCoefficients import FQHTorusInteractionCoefficients
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients2Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients3Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients4Body
from hammer.FQHTorusRange import FQHTorusRange
from hammer.PlotCoefficients import PlotTorusRange
from hammer.HammerArgs import HammerArgsParser
import hammer.utils as hammerutils
import numpy as np
import tempfile
import time
import argparse
import sys
import seaborn

if __name__ == '__main__':

    seaborn.set_style('whitegrid')


    Parser = argparse.ArgumentParser(description='Calculate and plot coefficient values.')

    Parser.add_argument('--num-datapoints',type=int,default=30)
    Parser.add_argument('--threshold','-t',type=float,default=1e-50)
    Parser.add_argument('--anti-sym', action='store_true')
    Parser.add_argument('--min-exponent', type=float, default=-1.0)
    Parser.add_argument('--max-exponent', type=float, default=1.0)
    Parser.add_argument('--lengths', action='store_true')

    [ArgVals,Unknowns] = Parser.parse_known_args(sys.argv)

    Opts = HammerArgsParser()
    Opts.parseArgs(Unknowns)

    # We want to vary tau 2 from 10^-2 to 10^2 with 1 in the centre
    NumPoints = ArgVals.num_datapoints
    MinExponent = ArgVals.min_exponent
    MaxExponent = ArgVals.max_exponent
    Tau2Exponent = np.arange(MinExponent,MaxExponent+0.0001, (MaxExponent-MinExponent)/float(NumPoints))
    PlotWithLengths = ArgVals.lengths
    Tau2 = 10.0**Tau2Exponent
    Tau1 = np.zeros(Tau2.__len__())
    Threshold = ArgVals.threshold
    Ns = Opts.NbrFlux
    if Opts.ID != '':
        Id = Opts.ID
    else:
        Id = 'Quad'
    LL = Opts.LL
    AntiSym = ArgVals.anti_sym

    if Opts.ID != '':
        folder = tempfile.mkdtemp(prefix='Coefs_Ns_' + str(Ns) + '_LL_' + str(LL) + '_' + Opts.ID + '_' ,dir='.')
    else:
        folder = tempfile.mkdtemp(prefix='Coefs_Ns_' + str(Ns) + '_LL_' + str(LL) + '_',dir='.')

    Range = FQHTorusRange()
    for i in np.arange(0,Tau2.__len__()):
        Tau = np.complex(Tau1[i],Tau2[i])
        if Opts.Interaction == '3Body':
            Torus = FQHTorusInteractionCoefficients3Body(Tau, Ns, Interaction=Opts.Interaction, Threshold=Threshold, LL=LL, MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic);
        elif Opts.Interaction == '4Body':
            Torus = FQHTorusInteractionCoefficients4Body(Tau, Ns, Interaction=Opts.Interaction, Threshold=Threshold, LL=LL, MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic);
        else:
            if Opts.PseudoPotentials is not None:
                print(('Pseudopotentials: ' + str(Opts.PseudoPotentials)))
            Torus = FQHTorusInteractionCoefficients2Body( Tau, Ns, Interaction=Opts.Interaction, Threshold=Threshold, LL=LL, MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic,  PseudoPotentials=Opts.PseudoPotentials);
        start = time.time(); Torus.CalculateCoefficients();end = time.time()

        print(("Took " + str(end-start) + " seconds."))
        if AntiSym:
            Torus.WriteAntiSymCoefficients(folder + '/Coefs_tau1_' + '{:.3f}'.format(Tau1[i]) + '_tau2_' + '{:.3f}'.format(Tau2[i]) + '.csv')
        else:
            Torus.WriteCoefficients(folder + '/Coefs_tau1_' + '{:.3f}'.format(Tau1[i]) + '_tau2_' + '{:.3f}'.format(Tau2[i]) + '.csv')
        Range.AddTorus(Torus)

    if not PlotWithLengths:
        Range.AddXAxis(Tau2)
        Range.AddXAxisLabel(r'$\tau_2$')
        Range.AddPrefix(folder + '/Ns_' + str(Ns) + '_LL_' + str(LL) + '_' + Id)
    else:
        #Range.AddXAxis(map(lambda x:  (-hammerutils.GetLengths(np.complex(0.0,x), Ns)[1] if x < 1.0 else hammerutils.GetLengths(np.complex(0.0, x), Ns)[0]), Tau2), scale='linear')
        Range.AddXAxis([(hammerutils.GetLengths(np.complex(0.0,x), Ns)[1] - hammerutils.GetLengths(np.complex(0.0, x), Ns)[0]) for x in Tau2], scale='linear')
        Range.AddXAxisLabel(r'$L_s$')
        Range.AddPrefix(folder + '/Ns_' + str(Ns) + '_LL_' + str(LL) + '_' + Id)

    plots = PlotTorusRange(Range)
    plots.WritePlots(color=True,legend=True,antisym=AntiSym, style='-o')
