#!/usr/bin/python

#
# This class serves to hold a the default settings of the Hammer package Wherenver a default value is need this package should be called.
#


def eigvals(): return 5
def numstates(): return 5
def eps_tol(): return 1e-15
def spectral_shift(): return 100.0
def max_iterations(): return 1000
def ll(): return 0
def interaction(): return 'Coulomb'
def ThreeBodyStength(): return 0

def tau1(): return 0.0
def tau2(): return 1.0
def magnetic_length(): return 1.0

def Ne(): return 4
def Ns(): return 12
def K1(): return 0
def K2(): return -1
def Inversion(): return -1
