#! /usr/bin/env python

""" ModularTransformState.py: Script that applies a modular transformation (or serries thereof) to coordinates and also applies the corresponding gauge transformation to the wave function. """

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"

import sys
import argparse
import numpy as np
import h5py
import time

def __main__():

    """
    Main driver function, if script called from the command line it will use the command line arguments as the vector filenames.
    """
    full_start = time.time()
    Parser = argparse.ArgumentParser(
        description='Utility to perform modular transformations on coordinates and wave functions. '+
        'One can here take both an active and a passive view. '+
        'In general under a set of modular transformations z->z\' and tau->tau\'. '+
        'For a wave function psi_s(z|tau) this means that psi_s(z|tau)= U(z\')*sum_{s\'} M_{ss\'} psi_s\'(z\'|tau\'), where M is a unitary matrix and U(z\') is a gauge transformation. '+
        'This utility will compute z\' and U(z\') for a list of modular transformation steps. '+
        'The matrix M can be estimated by computing M_{ss\'} = \\int psi_s(z|tau)*U(z\')*psi_{s\'}(z\'|tau\')*dz\'')
    Parser.add_argument('--wfn-in', type=str, help='input file for wfn samples psi(z|tau).')
    Parser.add_argument('--wfn-out', type=str, help='output file for wfn samples psi(z|tau)*U(z\')^-1.')
    Parser.add_argument('--coord-in', type=str, help='input file for (x,y) coordinates, z=x+tau*y.')

    Parser.add_argument('--coord-out', type=str, help='output file for (x\',y\') coordinates, z\'=x\'+tau\'*y\'.')
    Parser.add_argument('--transformation','-T', type=str, default='S',
                        help="Type of transformation {S,T,R=T^-1} to use. Can be a string 'S..T' that is read from left to right."+
                        "Ex: Under 'T' then tau'=tau+1, x'=x-y, y'=y and U(z')=exp(i*pi*Ns*y'^2). "+
                        "Ex: Under 'S' then tau'=-/tau, x'=-y, y'=x and U(z')=exp(i*2pi*Ns*x'*y').")
    Parser.add_argument('--scale',type=str,default='log',help='Type of scale {lin,log} on the wfn data (default=log).')
    Parser.add_argument('--tau-in', type=str, help='input file for tau. Format is <tau1> <tau2>.')
    Parser.add_argument('--tau-out', type=str, help='output file for tau\'. Format is <tau1> <tau2>.')
    Parser.add_argument('--fluxes',type=int,help='Number of fluxes. Required if wfn values are given.')

    Args = Parser.parse_args()

    Wf_in=Args.wfn_in
    Wf_out=Args.wfn_out
    tau_in=Args.tau_in
    tau_out=Args.tau_out
    coord_in=Args.coord_in
    coord_out=Args.coord_out
    Trans=Args.transformation
    scale=Args.scale
    Nphi=Args.fluxes

    ### Load the Wave-functions and the coordinates
    ensure_flag_dep(Wf_out,'--wfn-out',Wf_in,'--wfn-in')
    ensure_flag_dep(Wf_out,'--wfn-out', coord_in,'--coord-in')
    ensure_flag_dep(Wf_out,'--wfn-out', Nphi,'--fluxes')
    ensure_flag_dep(coord_out,'--coord-out', coord_in,'--coord-in')
    ensure_flag_dep(tau_out,'--tau-out', tau_in,'--tau-in')

    Trasforms=list(Trans)

    if coord_in!=None:
        print(("Open file '"+coord_in+"' for reading.."))
        with h5py.File(coord_in, 'r') as coord:
            xcoord_in=np.array(coord.get('xvalues'))
            ycoord_in=np.array(coord.get('yvalues'))

        ###Compute the transforfmation one at a time
        xcoord_tmp=np.array(xcoord_in)
        ycoord_tmp=np.array(ycoord_in)
        CumPhase=0
        for Trans in Trasforms:
            print('Perform Transformation: '+str(Trans))
            if Trans=='S':
                xcoord_out=-np.array(ycoord_tmp)
                ycoord_out=np.array(xcoord_tmp)
                Phase=2*xcoord_tmp*ycoord_tmp
            elif Trans=='T':
                xcoord_out=xcoord_tmp-ycoord_tmp
                ycoord_out=np.array(ycoord_tmp)
                Phase=ycoord_tmp**2
            elif Trans=='R':
                xcoord_out=xcoord_tmp+ycoord_tmp
                ycoord_out=np.array(ycoord_tmp)
                Phase=-ycoord_tmp**2
            else:
                print("ERROR: Transformation "+str(Trans)+" is not {S,T,R}")
                sys.exit(-1)
            CumPhase=CumPhase+Phase
            xcoord_tmp=np.array(xcoord_out)
            ycoord_tmp=np.array(ycoord_out)

    if coord_out!=None:
        print("Write the transformed coordinates to file")
        try:
            of = h5py.File(coord_out, 'w')
        except IOError as ioerror:
            print(("ERROR: Problem writing to file " + coord_out  + ': ' + str(ioerror)))
            sys.exit(-1)
        of.create_dataset("xvalues", data=xcoord_out)
        of.create_dataset("yvalues", data=ycoord_out)
        of.close()

    if Wf_out!=None:
        print("Open file ",Wf_in," for read")
        with h5py.File(Wf_in, 'r') as wf:
            wfval_in=np.array(wf.get('wf_values'))
            ###Add the accumulated phase
        SumPhase=np.pi*Nphi*np.sum(CumPhase,1)
        if scale=='log':
            wfval_out=np.array(wfval_in) ###Creates new array!
            wfval_out[:,1]=wfval_out[:,1]+SumPhase
        elif scale=='lin':
            wfval_out=np.array(wfval_in) ###Creates new array! ###FIXME Only sets dimensions
            wfval_out[:,0]=np.cos(SumPhase)*wfval_in[:,0]-np.sin(SumPhase)*wfval_in[:,1]
            wfval_out[:,1]=np.sin(SumPhase)*wfval_in[:,0]+np.cos(SumPhase)*wfval_in[:,1]
        else:
            print("ERROR: Scale of wfn has to be 'log' or 'lin'. Not "+str(scale))
            sys.exit(-1)
        print("Write the transformed wfns to file")
        try:
            of = h5py.File(Wf_out, 'w')
        except IOError as ioerror:
            print(("ERROR: Problem writing to file " + Wf_out  + ': ' + str(ioerror)))
            sys.exit(-1)
        of.create_dataset("wf_values", data=wfval_out)
        of.close()

    if tau_out!=None:
        tauarry=np.loadtxt(tau_in,delimiter=" ")
        print "Tauarray:",tauarry
        tau_tmp=tauarry[0]+1j*tauarry[1]
        for Trans in Trasforms:
            print('Perform Transformation: '+str(Trans))
            print "Tau_tmp:",tau_tmp
            if Trans=='S':
                tau_prime=-1.0/tau_tmp
            elif Trans=='T':
                tau_prime=tau_tmp+1.0
            elif Trans=='R':
                tau_prime=tau_tmp-1.0
            else:
                print("ERROR: Transformation "+str(Trans)+" is not {S,T,R}")
                sys.exit(-1)
            print "Tau_prime:",tau_prime
            tau_tmp=tau_prime
        tau_value=(np.real(tau_prime),np.imag(tau_prime))
        np.savetxt(tau_out,tau_value,fmt="%1.3f",delimiter=" ")
        

def ensure_flag_dep(F1,FName1,F2,FName2):
    if F1!=None:
        ##Flag 1 has a value
        if F2==None:
            print(("ERROR: Flag "+FName1+" depends on flag "+FName2+", which has not been set!"))
            sys.exit(-1)

if __name__ == '__main__' :
    __main__()
