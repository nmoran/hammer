
cdef class FQHTorusBasisBosonic:

    # Member variables for storing parameters for the basis
    cdef int Dim, Momentum, Particles, NbrFlux, EffectiveFlux, EffectiveMomentum, FluxOffset
    # Pointer to array where basis elements will be stored
    cdef long  *BasisElements
    # A temporary state to store a state as we apply operations to it
    cdef long TmpState
    cdef long TmpState2
    cdef long *Monomial1
    cdef long *Monomial2


    cdef int FindBasisDimension(self)
    cdef int FindSubBasisDimension(self, int J, int Nphi, int Ne)
    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base)
    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx)
    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double AA(self, int Orbital1, int Orbital2, int Idx)
    cpdef double complex AdAd(self, int Orbital1, int Orbital2)
    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3)
    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx)
    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4)
    cpdef int BinaryArraySearchDescending(self)
    cpdef int BinaryArraySearchDescendingElement(self, long item)
    cpdef long GetElement(self, int Idx)
    cpdef long GetElementSum(self, long Element1, long Element2)
    cpdef double GetElementNorm(self, long Element)
    cpdef OrbitalEntanglementMatrix(self, State, AOrbitalStart, AOrbitalEnd, AMomentum, AParticles)
    cpdef OrbitalEntanglementMatrices(self, State, AOrbitalStart, AOrbitalEnd, AParticles=?)

