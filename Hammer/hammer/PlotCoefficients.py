#! /usr/bin/env python

""" PlotCoefficients.py: Script with utility functions to plot interaction coefficients. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import matplotlib
matplotlib.use('PDF')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
import colorsys
from collections import OrderedDict

import seaborn

seaborn.set_style('whitegrid')

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)


def get_N_HexCol(N=5):

    HLS_tuples = [(x*1.0/np.float(N), 0.7, 1.0) for x in range(N)]
    RGB_tuples = [colorsys.hls_to_rgb(*x) for x in HLS_tuples]
    RGB_tuples = [tuple([int(y * 255) for y in x]) for x in RGB_tuples]
    HEX_tuples = [tuple([chr(y).encode('hex') for y in x]) for x in RGB_tuples]
    HEX_tuples = ["".join(x) for x in HEX_tuples]

    return HEX_tuples


def updateLightness(ColorTuple, N, V):
    red = int(ColorTuple[0:2],16)/255.0
    green = int(ColorTuple[2:4],16)/255.0
    blue = int(ColorTuple[4:6],16)/255.0
    HLS_tuple = colorsys.rgb_to_hls(red,green,blue)
    RGB_tuple = colorsys.hls_to_rgb(HLS_tuple[0],0.2 + V*(0.95-0.2)/float(N), HLS_tuple[2])
    RGB_tuple = tuple([int(y * 255) for y in RGB_tuple])
    HEX_tuple = tuple([chr(y).encode('hex') for y in RGB_tuple])

    return "".join(HEX_tuple)


class PlotTorusRange:
    def __init__(self,Range):
        self.Range = Range
        self.lw = 0.8

    def ShowPlots(self):
        plt.clf()
        for i in np.arange(0,self.Range.NumberCoefficients):
            plt.plot(np.abs(self.Range.GetCoefficientsByIndex(i)))
        plt.show()

    def WritePlots(self, color=False, legend=False, style='',antisym=False, plotkzero=False):
        MyPrefix = self.Range.Prefix

        Ns = self.Range.TorusObjs[0].Ns
        colors = get_N_HexCol(Ns)
        colors2 = get_N_HexCol(Ns/2+1)

        plt.clf()
        if color == False:
            for i in np.arange(0,self.Range.NumberCoefficients):
                plt.plot(self.Range.XAxis, np.abs(self.Range.GetCoefficientsByIndex(i, antisym)),style,lw=self.lw)
        else:
            for i in np.arange(0,self.Range.NumberCoefficients):
                k = np.abs(self.Range.GetSeparationByIndex(i))
                m = int(np.abs(self.Range.GetHoppingByIndex(i)))
                if k > 0 or plotkzero:
                    plt.plot(self.Range.XAxis, np.abs(self.Range.GetCoefficientsByIndex(i, antisym)),style,color='#'+updateLightness(colors2[m],Ns/2,k),lw=self.lw)

            if legend == True:
                self.AddLegend(Ns, colors2, style)

        plt.xscale(self.Range.scale)
        plt.xlabel(self.Range.XLabel)
        plt.ylabel('abs(E)')
        plt.xlim(self.Range.XAxis[0] - 0.01, self.Range.XAxis[self.Range.XAxis.__len__()-1] + 0.01)
        plt.savefig(MyPrefix + '_Abs.pdf')

        plt.clf()
        if color == False:
            for i in np.arange(0,self.Range.NumberCoefficients):
                plt.plot(self.Range.XAxis, np.real(self.Range.GetCoefficientsByIndex(i, antisym)),style,lw=self.lw)
        else:
            for i in np.arange(0,self.Range.NumberCoefficients):
                k = np.abs(self.Range.GetSeparationByIndex(i))
                m = int(np.abs(self.Range.GetHoppingByIndex(i)))
                if k > 0 or plotkzero:
                    plt.plot(self.Range.XAxis, np.real(self.Range.GetCoefficientsByIndex(i, antisym)),style,color='#'+updateLightness(colors2[m],Ns/2,k),lw=self.lw)

            if legend == True:
                self.AddLegend(Ns, colors2, style)
        plt.xscale(self.Range.scale)
        plt.xlabel(self.Range.XLabel)
        plt.ylabel(r'$Re(V_k,m)$')
        plt.xlim(self.Range.XAxis[0] - 0.01, self.Range.XAxis[self.Range.XAxis.__len__()-1] + 0.01)
        plt.savefig(MyPrefix + '_Real.pdf')

        plt.clf()
        MaxVals = np.ones(self.Range.XAxis.__len__())*1e-15
        for i in np.arange(0,self.Range.NumberCoefficients):
            k = np.abs(self.Range.GetSeparationByIndex(i))
            if k > 0 or plotkzero:
                for j in np.arange(self.Range.XAxis.__len__()):
                    if np.abs(self.Range.GetCoefficientsByIndex(i, antisym)[j]) > MaxVals[j] :
                        MaxVals[j] = np.abs(self.Range.GetCoefficientsByIndex(i, antisym)[j])

        if color == False:
            for i in np.arange(0,self.Range.NumberCoefficients):
                plt.plot(self.Range.XAxis, np.abs(self.Range.GetCoefficientsByIndex(i, antisym))/MaxVals,style,lw=self.lw)
        else:
            for i in np.arange(0,self.Range.NumberCoefficients):
                k = np.abs(self.Range.GetSeparationByIndex(i))
                m = int(np.abs(self.Range.GetHoppingByIndex(i)))
                if k > 0 or plotkzero:
                    plt.plot(self.Range.XAxis, np.abs(self.Range.GetCoefficientsByIndex(i, antisym))/MaxVals,style,color='#'+updateLightness(colors2[m],Ns/2,k),lw=self.lw)
            if legend == True:
                self.AddLegend(Ns, colors2, style)

        plt.xscale(self.Range.scale)
        plt.xlabel(self.Range.XLabel)
        plt.ylabel('abs(E)/max(abs(E))')
        plt.xlim(self.Range.XAxis[0] - 0.01, self.Range.XAxis[self.Range.XAxis.__len__()-1] + 0.01)
        plt.savefig(MyPrefix + '_Relative.pdf')

        plt.clf()
        ScaleFactors = self.Range.ScaleFactors
        if color == False:
            for i in np.arange(0,self.Range.NumberCoefficients):
                plt.plot(self.Range.XAxis, np.real(self.Range.GetCoefficientsByIndex(i, antisym))/ScaleFactors,style,lw=self.lw)
        else:
            for i in np.arange(0,self.Range.NumberCoefficients):
                k = np.abs(self.Range.GetSeparationByIndex(i))
                m = int(np.abs(self.Range.GetHoppingByIndex(i)))
                if k > 0 or plotkzero:
                    plt.plot(self.Range.XAxis, np.real(self.Range.GetCoefficientsByIndex(i, antisym))/ScaleFactors,style,color='#'+updateLightness(colors2[m],Ns/2,k),lw=self.lw)
            if legend == True:
                self.AddLegend(Ns, colors2, style)
        plt.xscale(self.Range.scale)
        plt.xlabel(self.Range.XLabel)
        plt.ylabel(r'Re(E)/max($L_x,L_y$)')
        plt.xlim(self.Range.XAxis[0] - 0.01, self.Range.XAxis[self.Range.XAxis.__len__()-1] + 0.01)
        plt.savefig(MyPrefix + '_Scaled.pdf')

        plt.clf()
        if color == False:
            for i in np.arange(0,self.Range.NumberCoefficients):
                plt.plot(self.Range.XAxis, np.imag(self.Range.GetCoefficientsByIndex(i, antisym)),style,lw=self.lw)
        else:
            for i in np.arange(0,self.Range.NumberCoefficients):
                k = np.abs(self.Range.GetSeparationByIndex(i))
                m = int(np.abs(self.Range.GetHoppingByIndex(i)))
                if k > 0 or plotkzero:
                    plt.plot(self.Range.XAxis, np.imag(self.Range.GetCoefficientsByIndex(i, antisym)),style,color='#'+updateLightness(colors2[m],Ns/2,k),lw=self.lw)
            if legend == True:
                self.AddLegend(Ns, colors2, style)
        plt.xscale(self.Range.scale)
        plt.xlabel(self.Range.XLabel)
        plt.ylabel(r'Im(E)')
        plt.xlim(self.Range.XAxis[0] - 0.01, self.Range.XAxis[self.Range.XAxis.__len__()-1] + 0.01)
        plt.savefig(MyPrefix + '_Imag.pdf')

    def AddLegend(self, Ns, colors2, style):
        ax = plt.subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.9, box.height])
        for m in np.arange(0,Ns/2+1):
            plt.plot([self.Range.XAxis[0]-5.0],[0.0],style,color='#'+colors2[m],lw=2.0,label='m='+str(m))
            ax.legend(loc='center left', bbox_to_anchor=(1,0.5)) # upper centre placement
