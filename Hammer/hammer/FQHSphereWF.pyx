#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

"""FQHSphereWF.pyx: Cython code for calculating values of single particle spherical wave-functions.

__author__      = "Niall Moran,Mikael Fremling"
__copyright__   = "Copyright 2017"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com,micke.fremling@gmail.com"
"""

import petsc4py.PETSc as PETSc
import time
import hammer.utils_cython as utils_cython
import sympy as syp
import numpy as np
cimport numpy as np
import scipy.special as sps
import hammer.utils_timer as timer

cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)

cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double cimag(double complex x) nogil
    double complex conj(double complex x) nogil

cdef class FQHSphereWF:
    """
    This class represents a single particle wave-function for a sphere
    wave-function which are spherical harmonics.
    """

    cdef int TwiceMaxLz,ll,Nstates
    cdef double Q
    cdef double [:] Norms, LL0Norm
    cdef double [:,:] HigherLLNorms

    

    def __init__(self, int TwiceMaxLz, int LL=0):
        """
        Constructor to setup value necessary.

        Parameters
        ----------
        TwiceMaxLz: int
            Twice the maximum Lz allowable.
        LL: int
            Landau level of the wave function

        """
        cdef int i,s,Nstates,min_s,max_s
        cdef double IndepNorm,lleff
        
        self.TwiceMaxLz = TwiceMaxLz
        self.ll = LL
        self.Nstates=self.TwiceMaxLz+2*self.ll+1
        self.Q = TwiceMaxLz/2.0
        
        #print("Initiate memory views")
        self.Norms = np.zeros(self.Nstates)
        self.HigherLLNorms = np.zeros((self.Nstates,self.ll+1))
        self.LL0Norm = np.zeros(self.Nstates)


        #print("Setup the Normalizations")
        lleff=self.Q+self.ll
        IndepNorm=(((2*lleff + 1)/(4.0*np.pi))
                   *sps.binom(2*lleff, lleff - self.Q))**.5       
        # we precalculate the norms of each of the orbitals
        #print "Nstates:",self.Nstates
        for i in range(self.Nstates):
            m = -lleff + i
            #print "--"           
            #print "i,m:",i,m

            if self.ll==0:
                self.LL0Norm[i] = (((2*self.Q + 1)/(4.0*np.pi))
                                   *sps.binom(2*self.Q, 2*self.Q - i))**.5       
            
            self.Norms[i] = IndepNorm*(sps.binom(2*lleff,lleff-m)**(-0.5))
            max_s=np.min((self.Nstates -1 - i,self.ll))
            min_s=np.max((self.ll - i,0))
            #print "max s",max_s
            #print "min s",min_s
            for s in range(min_s,max_s+1):
                #print "l,m,s:",self.ll,m,s
                self.HigherLLNorms[i,s] = (
                    sps.binom(self.ll,s)
                    *sps.binom(self.ll+self.TwiceMaxLz,
                               self.Nstates-1-i-s)
                    *(-1)**s)

    def WFValue(self, AngleArray, OrbNo):
        """
        Calculate the value of the wave-function at angular positions phi+1j*theta
        
        Paramters:
        --------
        pos : (array_like) double complex
            The position where the real part is the azimuthal angle and imaginary part the polar angle.
        orbital: int
            The orbital index at which to calculate the wave-function value.

        Returns
        -------
        (array_like) double complex
            The wave-function value.

        """
        cdef double complex angleval
        cdef int orbital = <int>OrbNo
        cdef int n
        cdef double complex [:] wfArray
        cdef double complex [:] PosArray
        ##Check if input is a numpy array or a "scalar"
        if hasattr(AngleArray, "__len__"): ##Array
            wfArray=np.zeros(AngleArray.shape,dtype=np.complex128)
            Elems=len(AngleArray)
            PosArray=np.asarray(AngleArray,dtype=np.complex128)
            n = 0
            while n < Elems:
                angleval=PosArray[n]
                wfArray[n] = self.WFValueScalar(angleval, orbital)
                n+=1
            return wfArray
        else:
            angleval=AngleArray
            return self.WFValueScalar(angleval, orbital)


    cpdef double complex WFValueScalar(self, double complex pos, int orbital):
        """
        Function to calculate the wave-function value at the given position for the given orbital.

        Parameters
        ----------
        pos: double complex
            The position where the real part is the azimuthal angle and imaginary
            part the polar angle.
        orbital: int
            The orbital index at which to calculate the wave-function value.

        Returns
        -------
        double complex
            The wave-function value.
        """
        cdef int s,min_s,max_s
        cdef double theta = pos.real
        cdef double phi = pos.imag
        cdef double m = -self.Q-self.ll + orbital , PsinLL
        cdef double complex PsiHolomorph

        ###Orbitals are implemented  as  psi= z^(m+Q)/(1+z\bar z)^Q
        ### where z=tan(theta/2)*exp(i*phi)
        ##This puts the origin of the stereographically projected plane at theta=0 with a continuous coordinate patch in this region
        ### The dirac string thus comes in at theta=pi and there the transforamtions is that
        ### theta -> 2*pi - theta , phi -> phi + pi comes with a phase (-1)^(2*Q)
        ## but
        ### theta -> - theta , phi -> phi + pi commes with no extra phase...

        #### The form of the functions are inspired by Jains book eqg. 3.157,
        ### expect that the (-1)^(l-m) is not present, and the gauges is chosen such that the the Dirac string is exiting though both the south pole and the north pole. Also the sin and cos have traded places, making the orbital counting the reverese....
        if orbital<0 or orbital>self.Nstates:
            ERRORMESSAGE="The orbital number "+str(orbital)+" is not in the allowed range 0<= orbital<="+str(self.Nstates)+"!"
            raise ValueError,ERRORMESSAGE


        #### if ll=0 (which is most likely) we can simplify substantially
        if self.ll==0:
            PsinLL=(self.LL0Norm[orbital]
                    *pow(cos(theta/2.0),self.TwiceMaxLz-orbital)
                    *pow(sin(theta/2.0),orbital))
            PsiHolomorph=cexp(1j*phi*(self.Q-orbital))
        else:
            ###Note that self.Q-orbital=-m
            PsiHolomorph=(self.Norms[orbital]
                          *cexp(1j*phi*(self.Q-orbital)))
            PsinLL=0.0
            if self.Nstates -1 - orbital > self.ll:
                max_s = self.Nstates -1 - orbital
            else:
                max_s = self.ll
            if self.ll - orbital > 0:
                min_s=self.ll - orbital
            else:
                min_s = 0
            #print "max s",max_s
            #print "min s",min_s
            #print "m:",m
            for s in range(min_s,max_s+1):
                PsinLL+=(self.HigherLLNorms[orbital,s]
                         *pow(cos(theta/2.0),self.TwiceMaxLz+3*self.ll-orbital-2*s)
                         *pow(sin(theta/2.0),orbital-self.ll+2*s))
                #print "Norms", self.HigherLLNorms[orbital,s]
                #print "PsinLL", PsinLL
            #print "PsiHolomorph:", PsiHolomorph
            #print "product:", PsiHolomorph*PsinLL
        return PsiHolomorph*PsinLL
    

    def GetNumOrbitals(self):
        """
        Return the number of orbitals
        """
        return self.TwiceMaxLz+1

    
    def Rho(self, xy, Basis, State):
        import hammer.FQHCorrelations as FQHCorrelations
        return FQHCorrelations.Rho(self,xy,Basis,State)

    def TwoBody(self, z1, z2, Basis, State):
        import hammer.FQHCorrelations as FQHCorrelations
        return FQHCorrelations.TwoBody(self,z1,z2,Basis,State)


cdef class FQHSphereSlaterDeterminant:
    """
    This class represents a slater determinant of single particle wave-function on a sphere.
    """

    cdef int TwiceMaxLz,Particles,maxLL
    cdef int [:] LLindx, OrbNoIndx

    def __init__(self, int TwiceMaxLz, OrbNoIndx, LLindx):
        """
        Constructor to setup the slater determinant.
        TwiceMaxLz: int
            Twice the maximum Lz allowable.
        OrbNoIndx: [int]
            List of Orbital Numbers filled
        LLindx: [int]
            List of LL:s the orbitals bellong to
        """
        
        self.TwiceMaxLz=TwiceMaxLz
        NeOrb=len(OrbNoIndx)
        NeLL=len(LLindx)
        if NeOrb!=NeLL:
            ERRTXT="Error, the length or the orbital and LL lists are not the same "+str(NeOrb)+" != "+str(NeLL)
            raise ValueError, ERRTXT
        else:
            self.Particles=NeOrb

        ###Check which LL are highetst LL:s involved
        self.maxLL=np.max(LLindx)
        PetscPrint = PETSc.Sys.Print
        PetscPrint("OrbNoIndx:",OrbNoIndx)
        PetscPrint("LLindx:",LLindx)
        self.OrbNoIndx=np.array(OrbNoIndx,dtype=np.intc)
        self.LLindx=np.array(LLindx,dtype=np.intc)


    def EvaluateSlaterDeterminant(self, Positions, bint Verbose=False):
        ArrayDim=Positions.ndim
        if ArrayDim==1:
            PosIn=np.zeros((1,len(Positions)),dtype=np.complex128)
            PosIn[0,:]=Positions[:]
            return self.DoEvaluateSlaterDeterminant(PosIn,Verbose)
        elif ArrayDim==2:
            return self.DoEvaluateSlaterDeterminant(Positions,Verbose)
        else:
            raise Exception, "To many dimetions in array"
        
    cpdef np.ndarray[np.complex128_t] DoEvaluateSlaterDeterminant(self, np.ndarray[np.complex128_t,ndim=2] Positions,bint Verbose=False):
        """
        Function to calculate the value of the slater determinants at the given positions.

        Parameters
        ------------
        Positions: 2d complex array
            Array of positions at which to evaluate the wave-functions. Each row is a set of positions with each column the position of a different particle.
        Verbose: bool
            Flag to indicate whether timings should be output on the console.

        Returns
        ----------
        1d complex array
            The wave-function values,
        """
        cdef int l,j,k
        cdef int OrbNo,LLNo,ArrayDim,NbrPositions
        cdef double complex DetNorm, Det,WfWal,Pos
        cdef FQHSphereWF Wfn
        
        PetscPrint = PETSc.Sys.Print
        DetNorm = 1.0/np.sqrt(np.float(syp.factorial(self.Particles)))

        NbrPositions = Positions.shape[0]
        cdef np.ndarray[np.complex128_t] OutputValues = np.zeros(NbrPositions, dtype=np.complex128) ###If input is 2D then output is 1D

        ### Setup the amtrix used for the determinant
        MatDet = utils_cython.MatrixDeterminant(self.Particles)

        Timings = np.zeros(2)


        ###Create a python list with the orbitals involved
        LLOrbTypeList=[]
        for LL in range(self.maxLL+1):
            LLOrbTypeList.append(FQHSphereWF(self.TwiceMaxLz,LL))
        #PetscPrint("LLOrbTypeList:",LLOrbTypeList)
        s = time.time()
        mytimer=timer.timer()
        l = 0
        # for each set of configurations calculate the Slater determinant
        while l < NbrPositions:
            j  = 0 ### loop over the OrbNumbers
            while j < self.Particles:
                OrbNo=self.OrbNoIndx[j]
                LLNo=self.LLindx[j]
                Wfn=LLOrbTypeList[LLNo] ###Just lookup in the list
                k = 0 ### loop over the ElectronNumbers
                while k < self.Particles:
                    WfWal=Wfn.WFValueScalar(Positions[l,k],OrbNo)
                    #PetscPrint("j, k, WfWal:",j, k, WfWal)
                    MatDet.setitem(j, k, WfWal)
                    k += 1
                j += 1
            Det = MatDet.det() * DetNorm
            OutputValues[l]=Det ###If input is 2D then output is 1D
            if Verbose:
                mytimer.write_progress(l+1,NbrPositions)
            l+=1
        e = time.time()
        Timings[0] = e-s
        if Verbose and PETSc.COMM_WORLD.tompi4py().Get_rank() == 0: PetscPrint('Evaluations took: ' + str(e-s) + ' seconds.')

        return OutputValues
    
def MollweideConversion(xy,R=1.0):
    """
    Calculate the conversion from planar Mollewide coordinates to spherical coordinates. Note that xy satisfy  |x/sqrt(2)|^2 + |y*sqrt(2)|^2 < 4*R^2
    
    Parameters:
    ---------------------
    xy: (array_like) double complex
        The planar Mollweide coordinates -1 < x/(2*sqrt(2)*R) < 1 and  -1 < (y*sqrt(2))/(2*R) < 1
      
    R: scalar
        The radius of the sphere

    Return
    -------------
    AngleCoords: (array_like) double complex
        azimutal + 1j * polar
    """

    xval=np.real(xy)
    yval=np.imag(xy)

    theta=np.arcsin(yval/(R*np.sqrt(2)))

    azimutal= np.pi/2.0 - np.arcsin( (2*theta + np.sin(2*theta))/np.pi)
    polar = np.pi*xval/(2*R*np.sqrt(2)*np.cos(theta))

    return azimutal + 1j*polar

