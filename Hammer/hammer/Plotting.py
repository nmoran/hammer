#! /usr/bin/env python

""" Plotting.py:Some utility functions for creating plots from the outout data. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import colorsys
import numpy as np
import csv
import matplotlib.pyplot as plt
from matplotlib import rc, font_manager
from hammer.HammerArgs import HammerArgsParser
import hammer.vectorutils as vecutils
from hammer.FQHTorusWF import FQHTorusWF
from scipy.interpolate import griddata
import os

try:
    import brewer2mpl
    Havebrewer2mpl = True
except ImportError:
    Havebrewer2mpl = False

def getNColours(N=5, set='Set1'):
    """
    Function to get N distinct colours that span the Hue of the HSL colour space.
    Returned in hexadecimal format.
    """
    if Havebrewer2mpl:
        Hex_tuples = brewer2mpl.get_map(set, 'qualitative', N).hex_colors
        return Hex_tuples
    else:
        HLS_tuples = [(x*1.0/np.float(N), 0.7, 1.0) for x in range(N)]
        RGB_tuples = [colorsys.hls_to_rgb(*x) for x in HLS_tuples]
        RGB_tuples = [tuple([int(y * 255) for y in x]) for x in RGB_tuples]
        HEX_tuples = [tuple([chr(y).encode('hex') for y in x]) for x in RGB_tuples]
        HEX_tuples = ["".join(x) for x in HEX_tuples]
        HEX_tuples = ["#"+ x for x in HEX_tuples]
        return HEX_tuples
    return []


def GetEnergies(Args, path=''):
    """
    Read the energies for the run specified by Args at the given path.
    """
    if path == '':
        path = Args.Interaction
    Energies = list()
    f = open(path + '/' + Args.getEnergiesFilename())
    csv_reader = csv.reader(f)
    for row in csv_reader:
        Energies.append(float(row[0]))
    return Energies


class MultiPanelPlot():
    """
    Class to setup multipanel plotting with desired number of plots.
    Note that it only supports proper placement of legend with up to 2 rows and 2 columns currently.
    """

    def __init__(self, rows, cols, figw=0.8, figh=0.8):
        self.rows = rows
        self.cols = cols
        plt.clf()
        plt.figure(1)
        ax = plt.subplot(111)
        box = ax.get_position()
        colsf = np.float(cols)
        rowsf = np.float(rows)
        w = figw * ((box.width * 0.9)/colsf)
        h = figh * (box.height/rowsf)
        xpad = 0.05
        ypad = 0.05

        self.subplotpositions = list()
        self.subplots = list()
        plotnum = 1
        for row in np.arange(0,rows):
            for col in np.arange(0, cols):
                self.subplots.append([rows, cols, plotnum])
                plotnum += 1
                if col > 0:
                    self.subplotpositions.append([box.x0 + xpad + col*(box.width*0.9)/cols, box.y0 + ypad + (rows - row - 1)*box.height/rows, w, h])
                else:
                    self.subplotpositions.append([box.x0 + col*(box.width*0.9)/cols, box.y0 + ypad + (rows - row - 1)*box.height/rows, w, h])

    def SetupPlot(self, idx):
        p = self.subplots[idx]
        ax = plt.subplot(p[0], p[1], p[2])
        ax.set_position(self.subplotpositions[idx])
        return ax

    def LegendAxes(self):
        """
        This will return the axes for creating the legend on the centre right hand side.
        """
        idx = self.rows * self.cols - 1 # legednd will be relative to the last plot
        ax = self.SetupPlot(idx)
        if self.rows == 1:
            anchor = (1, 0.5)
        else:
            anchor = (1,1)

        return [ax,anchor]


def CreateEnergyPlots(ArgsList, Titles, Taus, Ks, xaxis, filename, scale='log', legend=True, figw=0.8, figh=0.8,N=2, prefix='.', xlabel=r'$\tau_2$', xlimits=None):
    plt.clf()
    plt.figure(1)
    ax = plt.subplot(111)
    box = ax.get_position()
    colours = getNColours(9)

    # set up font
    sizeOfFont = 6
    fontProperties = {'family':'sans-serif','sans-serif':['Helvetica'], 'weight' : 'normal', 'size' : sizeOfFont}
    ticks_font = font_manager.FontProperties(family='Helvetica', style='normal',
    size=sizeOfFont, weight='normal', stretch='normal')
    rc('text', usetex=True)
    rc('font',**fontProperties)

    w = figw * (box.width * 0.9)/2.0
    h = figh * box.height/2.0
    xpad = 0.05
    ypad = 0.05

    if len(ArgsList)  <= 2:
        if len(ArgsList) == 1:
            MultiPlot = MultiPanelPlot(1,1)
        else:
            MultiPlot = MultiPanelPlot(1,2)
    else:
        MultiPlot = MultiPanelPlot(2,2)

    for idx in np.arange(0, ArgsList.__len__()):
        MultiPlot.SetupPlot(idx)
        plt.title(Titles[idx])
        PlotEnergies(xaxis, Taus, ArgsList[idx], Ks, N, prefix=prefix)
        plt.xscale(scale)
        if not xlimits:
            xmin = xaxis[0]
            xmax = xaxis[-1]
        else:
            xmin = xlimits[0]
            xmax = xlimits[1]

        plt.xlim(xmin, xmax)
        # Sort out tick labelling
        NumberTicks = 8.0
        #find locations to place ticks
        if scale == 'log':
            step = (np.log(xmax) - np.log(xmin))/NumberTicks
            LogTickVals = np.arange(np.log(xmin) + step, np.log(xmax), step)
            TickVals = np.exp(LogTickVals)
        else:
            step = (xmax - xmin)/NumberTicks
            TickVals = np.arange(xmin + step, xmax, step)
        TickLabels = list()
        for Val in TickVals:
            TickLabels.append('{:0.3g}'.format(Val))

        a = plt.gca()
        a.set_xticklabels(a.get_xticks(), fontProperties)

        plt.xticks(TickVals, TickLabels)

        if idx == ArgsList.__len__() - 1:
            plt.xlabel(xlabel)
        plt.ylabel(r'$E_i - E_0$')

    if legend:
        [ax , anchor] = MultiPlot.LegendAxes()
        for K in Ks:
            plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[K],label='K='+str(K))
        ax.legend(loc='center left', bbox_to_anchor=anchor) # upper centre placement


    plt.savefig(filename)


def PlotEnergies(Tau2s, Taus, Args, Ks, N, Thres=1e-9, prefix='', PlotGaps=True):
    """
    Function to plot the energies or energy gaps for the specified parameters.
    Tau2s: Tau2 values, used as x-axis.
    Taus: Tau values to use.
    Args: Instance of HammerArgsParser class containging other parameter identifying data to plot.
    Ks: Array of the momentum sectors to consider.
    N: Number of energy bands in each sector to plot.
    Thres: The threshold to use when deciding whether energy levels are degenerate.
    prefix: Path prefix to location where data files can be found.
    PlotGaps: Flag to indicate whether we should plot gaps or the raw energy values.
    """
    Energies = dict()
    EnergiesK = dict()
    colours = getNColours(9)
    symbols = ['+','x','>','<']
    for K in Ks:
        Energies[K] = dict()
        for Tau in Taus:
            Args.Momentum = K
            Args.Tau = Tau
            Energies[K][Tau] = GetEnergies(Args, path=prefix)
        EnergiesK[K] = dict()
        for n in np.arange(0,N):
            EnergiesK[K][n] = list()

    for Tau in Taus:
        # Find ground state energy as the lowest energy among all momentum sectors.
        for Kidx in range(len(Ks)):
            if Kidx == 0:
                GS = Energies[Ks[Kidx]][Tau][0]
                GSKidx = Kidx
            else:
                if Energies[Ks[Kidx]][Tau][0] < GS:
                    GS = Energies[Ks[Kidx]][Tau][0]
                    GSKidx = Kidx

        # Now find the first N energies above this
        for Kidx in range(len(Ks)):
            K = Ks[Kidx]
            j = 0
            if PlotGaps:
                EP = GS # if we are plotting the gaps then will not plot the ground state so will start there.
            else:
                EP = GS - 0.1 # when not plotting the gaps, we want to start below the ground state so that this will also be considered.
            for n in np.arange(0,N):
                while (j < Energies[K][Tau].__len__()) and (np.abs(Energies[K][Tau][j] - EP) < Thres) : # Keep moving up the spectrum until we hit the next band
                    j += 1
                try:
                    if PlotGaps:
                        EnergiesK[K][n].append(Energies[K][Tau][j] - GS)
                    else:
                        EnergiesK[K][n].append(Energies[K][Tau][j])
                    EP = Energies[K][Tau][j]
                except IndexError:
                    if PlotGaps:
                        EnergiesK[K][n].append(Energies[K][Tau][j-1] - GS)
                    else:
                        EnergiesK[K][n].append(Energies[K][Tau][j-1])
                    EP = Energies[K][Tau][j-1]
                    pass

    for n in np.arange(0,N):
        for K in Ks:
            if n == 0:
                plt.plot(Tau2s, EnergiesK[K][n], '-+', color=colours[K], alpha=1.0)
            else:
                plt.plot(Tau2s, EnergiesK[K][n], '-+' ,color=colours[K], alpha=1.0)
    return EnergiesK


def PlotOverlaps(MyArgs, Taus, Tau2s, figw=0.8, figh=0.8, legend=True):
    plt.clf()
    plt.figure(1)
    ax = plt.subplot(111)
    box = ax.get_position()
    colours = getNColours(9)

    w = figw * (box.width * 0.9)/2.0
    h = figh * box.height/2.0
    xpad = 0.05
    ypad = 0.05

    MultiPlot = MultiPanelPlot(2,2)

    Args = HammerArgsParser()
    Args.parseArgs([])
    Args.Particles = MyArgs.Particles
    Args.NbrFlux = MyArgs.NbrFlux

    # The first plot is 1LL Coulomb vs:
    #    1. TT
    #    2. 0LL Sq. Coulomb
    #    3. 1LL Sq. Coulomb
    #    4. OLL Sq. Delta
    Args.Interaction = 'Coulomb'
    Args.LL = 0
    Args.Tau = Taus[0]
    Args.Momentum = 0
    Args.LL = 1
    ThinTorusLeftLL1 = Args.Interaction + '/' + Args.getVectorFilename(0)
    Args.Tau = Taus[-1]
    Args.LL = 1
    ThinTorusRightLL1 = Args.Interaction + '/' + Args.getVectorFilename(0)

    Args.LL = 0
    Args.Tau = np.complex(0.0,1.0)
    CoulombSquareLL0 = Args.Interaction + '/' + Args.getVectorFilename(0)

    Args.LL = 1
    Args.Tau = np.complex(0.0,1.0)
    CoulombSquareLL1 = Args.Interaction + '/' + Args.getVectorFilename(0)

    Args.LL = 0
    Args.Tau = np.complex(0.0,1.0)
    Args.Interaction = 'Delta'
    DeltaSquareLL0 = Args.Interaction + '/' + Args.getVectorFilename(0)


    Overlaps = dict()
    Overlaps['LTTLL1'] = list()
    Overlaps['RTTLL1'] = list()
    Overlaps['SqLL0'] = list()
    Overlaps['SqLL1'] = list()
    Overlaps['SqLL0Delta'] = list()

    Args.Interaction = 'Coulomb'
    Args.LL = 1
    for Tau in Taus:
        Args.Tau = Tau
        CoulombLL1 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Overlaps['LTTLL1'].append(vecutils.Overlap(CoulombLL1, ThinTorusLeftLL1)**2)
        Overlaps['RTTLL1'].append(vecutils.Overlap(CoulombLL1, ThinTorusRightLL1)**2)
        Overlaps['SqLL0'].append(vecutils.Overlap(CoulombSquareLL0, CoulombLL1)**2)
        Overlaps['SqLL1'].append(vecutils.Overlap(CoulombSquareLL1, CoulombLL1)**2)
        Overlaps['SqLL0Delta'].append(vecutils.Overlap(DeltaSquareLL0, CoulombLL1)**2)

    MultiPlot.SetupPlot(0)
    plt.plot(Tau2s, Overlaps['LTTLL1'], color=colours[0])
    plt.plot(Tau2s, Overlaps['RTTLL1'], color=colours[0])
    plt.plot(Tau2s, Overlaps['SqLL0'], color=colours[1])
    plt.plot(Tau2s, Overlaps['SqLL1'], color=colours[2])
    plt.plot(Tau2s, Overlaps['SqLL0Delta'], color=colours[3])
    plt.xlim(Tau2s[0], Tau2s[-1])
    plt.ylim(0.0,1.1)
    plt.xscale('log')
    plt.xlabel(r'$\tau_2$')
    plt.ylabel(r'$|\langle \psi_i | \psi_j \rangle|^2$')
    plt.title('1LL Coulomb')

    # The second plot is 0LL Coulomb vs:
    #    1. TT
    #    2. 0LL Sq. Coulomb
    #    3. 1LL Sq. Coulomb
    #    4. OLL Sq. Delta

    Overlaps = dict()
    Overlaps['LTTLL1'] = list()
    Overlaps['RTTLL1'] = list()
    Overlaps['SqLL0'] = list()
    Overlaps['SqLL1'] = list()
    Overlaps['SqLL0Delta'] = list()

    Args.Interaction = 'Coulomb'
    Args.LL = 0
    for Tau in Taus:
        Args.Tau = Tau
        CoulombLL0 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Overlaps['LTTLL1'].append(vecutils.Overlap(CoulombLL0, ThinTorusLeftLL1)**2)
        Overlaps['RTTLL1'].append(vecutils.Overlap(CoulombLL0, ThinTorusRightLL1)**2)
        Overlaps['SqLL0'].append(vecutils.Overlap(CoulombSquareLL0, CoulombLL0)**2)
        Overlaps['SqLL1'].append(vecutils.Overlap(CoulombSquareLL1, CoulombLL0)**2)
        Overlaps['SqLL0Delta'].append(vecutils.Overlap(DeltaSquareLL0, CoulombLL0)**2)

    MultiPlot.SetupPlot(1)
    plt.plot(Tau2s, Overlaps['LTTLL1'], color=colours[0])
    plt.plot(Tau2s, Overlaps['RTTLL1'], color=colours[0])
    plt.plot(Tau2s, Overlaps['SqLL0'], color=colours[1])
    plt.plot(Tau2s, Overlaps['SqLL1'], color=colours[2])
    plt.plot(Tau2s, Overlaps['SqLL0Delta'], color=colours[3])
    plt.xlim(Tau2s[0], Tau2s[-1])
    plt.ylim(0.0,1.1)
    plt.xscale('log')
    plt.xlabel(r'$\tau_2$')
    plt.ylabel(r'$|\langle \psi_i | \psi_j \rangle|^2$')
    plt.title('0LL Coulomb')

    # The third plot is 1LL Coulomb vs:
    #    1. 0LL Coulomb
    #    2. 0LL Delta
    Overlaps = dict()
    Overlaps['0LLCoulomb'] = list()
    Overlaps['0LLDelta'] = list()

    for Tau in Taus:
        Args.Interaction = 'Coulomb'
        Args.LL = 1
        Args.Tau = Tau
        CoulombLL1 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Args.LL = 0
        CoulombLL0 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Args.Interaction = 'Delta'
        DeltaLL0 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Overlaps['0LLCoulomb'].append(vecutils.Overlap(CoulombLL1, CoulombLL0)**2)
        #Overlaps['0LLDelta'].append(vecutils.Overlap(CoulombLL1, DeltaLL0 )**2)

    MultiPlot.SetupPlot(2)
    plt.plot(Tau2s, Overlaps['0LLCoulomb'], color=colours[4])
    #plt.plot(Tau2s, Overlaps['0LLDelta'], color=colours[6])
    plt.xlim(Tau2s[0], Tau2s[-1])
    plt.ylim(0.0,1.1)
    plt.xscale('log')
    plt.xlabel(r'$\tau_2$')
    plt.ylabel(r'$|\langle \psi_i | \psi_j \rangle|^2$')
    plt.title('1LL Coulomb')

    # The fourth plot is 0LL Coulomb vs:
    #    1. 1LL Coulomb
    #    2. 0LL Delta
    Overlaps = dict()
    Overlaps['1LLCoulomb'] = list()
    Overlaps['0LLDelta'] = list()

    for Tau in Taus:
        Args.Interaction = 'Coulomb'
        Args.LL = 1
        Args.Tau = Tau
        CoulombLL1 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Args.LL = 0
        CoulombLL0 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Args.Interaction = 'Delta'
        DeltaLL0 = Args.Interaction + '/' + Args.getVectorFilename(0)
        Overlaps['1LLCoulomb'].append(vecutils.Overlap(CoulombLL1, CoulombLL0)**2)
        #Overlaps['0LLDelta'].append(vecutils.Overlap(CoulombLL0, DeltaLL0 )**2)

    MultiPlot.SetupPlot(3)
    plt.plot(Tau2s, Overlaps['1LLCoulomb'], color=colours[7])
    #plt.plot(Tau2s, Overlaps['0LLDelta'], color=colours[6])
    plt.xlim(Tau2s[0], Tau2s[-1])
    plt.ylim(0.0,1.1)
    plt.xscale('log')
    plt.xlabel(r'$\tau_2$')
    plt.ylabel(r'$|\langle \psi_i | \psi_j \rangle|^2$')
    plt.title('0LL Coulomb')

    xaxis = Tau2s
    if legend:
        [ax , anchor] = MultiPlot.LegendAxes()
        plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[0],label='Thin Torus')
        plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[1],label='0LL Sq. Coulomb')
        plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[2],label='1LL Sq. Coulomb')
        plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[3],label='0LL Sq. Delta')
        plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[4],label='0LL Coulomb')
        plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[7],label='1LL Coulomb')
        plt.plot([xaxis[0]-5.0],[0.0],'+-',color=colours[6],label='0LL Delta')
        ax.legend(loc='center left', bbox_to_anchor=anchor,prop={'size':8}) # upper centre placement


    plt.savefig('N' + str(Args.Particles) + 'Overlaps.pdf')
    return


def PlotFundamentalDomain(Taus, Values, NbrFlux, WithL1=True, Pixels=(1000,1000),
                          InterpolationMethod='linear',Cmap=None, Limits=None, Colorbar=True):
    """
    Function to plot the provided values over the fundamental domain of the complex plane.

    Parameters
    ----------
    Taus : ndarray[dtype=np.complex128, ndim=1]
        Tau values for each point..
    Values :  ndarray[dtype=np.float64, ndim=1]
        The values to plot.
    NbrFlux : int
        The number of flux through the torus.
    WithL1 : bool
        Flag to indicate to plot vs L1 instead of imag(Tau) (default=True).
    Pixels: tuple
        Resolution to use in each direction (default =(1000,1000)).
    InterpolationMethod: string
        Interpolation method to use (default = 'linear').
    Cmap: colormap
        Colormap to use for image plot. None will use currently selected colormap (default = 'None').
    Limits: tuple
        Tuple of two numbers which are upper and lower limits to plot.
    Colorbar: bool
        Draw colorbar (default = True).
    """
    Points = np.zeros((len(Taus),2), dtype=np.float64)

    if WithL1:
        for idx in range(len(Taus)):
            WF = FQHTorusWF(NbrFlux, Taus[idx])
            Points[idx,0] = WF.L1
            Points[idx,1] = np.real(Taus[idx])
    else:
        Points[:,0] = np.imag(Taus)
        Points[:,1] = np.real(Taus)

    XMin = np.min(Points[:,0])
    #XMin = 0
    YMin = np.min(Points[:,1])
    YMin = 0
    XMax = np.max(Points[:,0])
    YMax = np.max(Points[:,1])

    grid_x, grid_y = np.mgrid[XMin:XMax:np.complex(0,Pixels[0]), YMin:YMax:np.complex(0,Pixels[1])]
    grid = griddata(Points, Values, (grid_x, grid_y), method=InterpolationMethod, rescale=True)

    if Limits is not None:
        VMin = max(Limits[0], np.min(Values))
        VMax = min(Limits[1], np.max(Values))
    else:
        VMin = None
        VMax = None

    if WithL1:
        boundary = np.sqrt(2.0*np.pi*NbrFlux/np.sin(np.arccos(grid_y)))
        grid[grid_x > boundary] = np.nan
        XLimits = (XMin, XMax)
        plt.xlim([XMin, XMax])
        Aspect = (XMax-XMin)/(YMax-YMin)
    else:
        boundary = np.sin(np.arccos(grid_y))
        grid[grid_x < boundary] = np.nan
        XLimits = (XMax, XMin)
        Aspect = (XMax-XMin)/(YMax-YMin)

    Extent = (XMin,XMax,YMin,YMax)
    plt.imshow(grid.T, extent=Extent, origin='lower',
           aspect=Aspect, vmin=VMin, vmax=VMax,
           cmap=Cmap)
    plt.plot(Points[:,0], Points[:,1], '+k', fillstyle='none', mew=1)
    plt.xlim(XLimits)
    plt.grid(None)
    plt.ylim([YMin, YMax])
    if Colorbar:
        plt.colorbar()


def PlotFundamentalDomain3D(Taus, Values, NbrFlux, WithL1=True, Pixels=(500,500),
                          InterpolationMethod='linear',Cmap=None, Limits=None, Colorbar=False):
    """
    Function to plot the provided values over the fundamental domain of the complex plane.
    Parameters
    ----------
    Taus : ndarray[dtype=np.complex128, ndim=1]
        Tau values for each point..
    Values :  ndarray[dtype=np.float64, ndim=1]
        The values to plot.
    NbrFlux : int
        The number of flux through the torus.
    WithL1 : bool
        Flag to indicate to plot vs L1 instead of imag(Tau) (default=True).
    Pixels: tuple
        Resolution to use in each direction (default =(1000,1000)).
    InterpolationMethod: string
        Interpolation method to use (default = 'linear').
    Cmap: colormap
        Colormap to use for image plot. None will use currently selected colormap (default = 'None').
    Limits: tuple
        Tuple of two numbers which are upper and lower limits to plot.
    Colorbar: bool
        Draw colorbar (default = True).
    """
    Points = np.zeros((len(Taus),2), dtype=np.float64)

    if WithL1:
        for idx in range(len(Taus)):
            WF = FQHTorusWF(NbrFlux, Taus[idx])
            Points[idx,0] = WF.L1
            Points[idx,1] = np.real(Taus[idx])
    else:
        Points[:,0] = np.imag(Taus)
        Points[:,1] = np.real(Taus)

    XMin = np.min(Points[:,0])
    YMin = np.min(Points[:,1])
    YMin = 0 # this ensurs only points with positive real part are displayed.
    XMax = np.max(Points[:,0])
    YMax = np.max(Points[:,1])

    grid_x, grid_y = np.mgrid[XMin:XMax:np.complex(0,Pixels[0]), YMin:YMax:np.complex(0,Pixels[1])]
    grid = griddata(Points, Values, (grid_x, grid_y), method=InterpolationMethod, rescale=True)

    if Limits is not None:
        VMin = max(Limits[0], np.min(Values))
        VMax = min(Limits[1], np.max(Values))
    else:
        VMin = None
        VMax = None

    if WithL1:
        boundary = np.sqrt(2.0*np.pi*NbrFlux/np.sin(np.arccos(grid_y)))
        grid[grid_x > boundary] = np.nan
        XLimits = (XMin, XMax)
        plt.xlim([XMin, XMax])
        Aspect = (XMax-XMin)/(YMax-YMin)
    else:
        boundary = np.sin(np.arccos(grid_y))
        grid[grid_x < boundary] = np.nan
        XLimits = (XMax, XMin)
        Aspect = (XMax-XMin)/(YMax-YMin)

    Extent = (XMin,XMax,YMin,YMax)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    grid[grid > VMax] = VMax
    grid[grid < VMin] = VMin


    ax.plot_surface(grid_x, grid_y, grid, vmin=VMin, vmax=VMax, cmap=Cmap, zorder=1, alpha=0.8)

    GoodPoints = (Values >= VMin)
    Points = Points[GoodPoints,:]
    Values = Values[GoodPoints]
    GoodPoints = (Values <= VMax)
    Points = Points[GoodPoints,:]
    Values = Values[GoodPoints]
    GoodPoints = (Points[:,1] >= 0.0)

    ax.scatter(Points[:,0][GoodPoints], Points[:,1][GoodPoints], Values[GoodPoints],c='k')
    ax.set_xlim((XMin, XMax))
    ax.set_ylim((YMin, YMax))

    if Colorbar:
        plt.colorbar()

    return ax


def ReadAllEnergiesFilesUnder(path):
    """
    Function to read all the output csv files containing energy values under the given path and return in
    a pandas dataframe.

    Parameters
    ----------
    path: str
        The path to recursively search under.
    """
    import fnmatch
    import pandas as pd

    dfrows = list()

    for root, dirnames, filenames in os.walk(path):
        for filename in fnmatch.filter(filenames, '*_Energies.csv'):
            FullFilename = os.path.join(root, filename)
            N = int(filename.split('_p_')[1].split('_')[0])
            Ns = int(filename.split('_Ns_')[1].split('_')[0])
            K = int(filename.split('_K_')[1].split('_')[0])
            P = (-1 if filename.find('_P_') < 0 else int(filename.split('_P_')[1].split('_')[0]))
            I = (-1 if filename.find('_I_') < 0 else int(filename.split('_I_')[1].split('_')[0]))
            tau1 = float(filename.split('tau1_')[1].split('_')[0])
            tau2 = float(filename.split('tau2_')[1].split('_')[0])
            Tau = np.complex(tau1, tau2)
            Theta = np.angle(Tau)
            SinTheta = np.sin(Theta)
            CosTheta = np.cos(Theta)
            Ratio = np.imag(Tau)/SinTheta
            MagneticLength = 1.0
            Scale = np.sqrt(2.0*np.pi*Ns*MagneticLength**2/(Ratio*SinTheta))
            L1 = Scale
            L2 = Ratio*Scale
            LL = (0 if filename.find('LL') < 0 else int(filename.split('LL')[1][0]))
            Interaction = 'Other'
            if filename.find('Coulomb') >= 0:
                Interaction = 'Coulomb'
            elif filename.find('Delta') >= 0:
                Interaction = 'Delta'
            elif filename.find('3Body') >= 0:
                Interaction = '3Body'
            elif filename.find('4Body') >= 0:
                Interaction = '4Body'
            elif filename.find('PseudoPotentials') >= 0:
                Interaction = 'PseudoPotentials'

            f = open(FullFilename,"r")
            csv_reader = csv.reader(f)
            energies = list()
            n = 0
            for row in csv_reader:
                energies.append(float(row[0]))
            f.close()
            #dfrows.append([N, Ns, K, P, I, Tau, Theta, L1, L2, LL, Interaction, pd.Series(np.array(energies)), filename, path + '/' + root])
            dfrows.append([N, Ns, K, P, I, Tau, Theta, L1, L2, LL, Interaction, np.array(energies), filename, path + '/' + root])
    df = pd.DataFrame(dfrows, columns=('Particles', 'NbrFlux', 'K1', 'K2', 'Inv', 'Tau', 'Theta', 'L1', 'L2', 'LandauLevel', 'Interaction', 'Energy', 'Filename', 'Root'))
    return df

