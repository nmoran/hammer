#! /usr/bin/env python

""" SqueezedWeight.py: Script the weight in configurations squeezed from supplied configuration(s) of a  binary PETSc. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


from petsc4py import PETSc
import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils
from hammer.FQHBasisManager import FQHBasisManager
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic


def ParseConfiguration(Configuration, ConfigurationFile, Basis, OccupationRep):
    """
    This function parses the supplied configuration (if given) and ensures it is a valid basis configuration.
    If a filename is given instead of a single configuration, it attempts to read configurations from the supplied file
    and checks each of these.
    Basis configurations are returned in the integer representation.
    """
    Configurations = list()
    BasisDetails = Basis.GetBasisDetails()
    NbrFlux = BasisDetails['NbrFlux']

    if Configuration != '':
        if not OccupationRep:
            # If it is not in the occupation representation we assume it is already in the encoded integer format so just convert to an
            # integer and check it is a valid basis element.
            Conf = np.int(Configuration)
        else:
            # Element is supplied in the occupation representation so we must convert to integer format
            # Fist we check the lenght
            Length = len(Configuration)
            if Length < NbrFlux: # if the pattern given is too short, repeat it until it fills out the space.
                for i in range(NbrFlux-Length):
                    Configuration += Configuration[i % Length]
            elif Length > NbrFlux:
                Configuration = Configuration[0:NbrFlux]

            Conf = Basis.GetElementFromOccupationRep(Configuration)

        if Basis.GetElementIdx(Conf) != -1:
            Configurations.append(Conf)


    elif ConfigurationFile != '':
        f = open(ConfigurationFile, 'r')
        Configuration = f.readline()
        while Configuration:
            Length = len(Configuration)
            if Length > 0:
                if not OccupationRep:
                    # If it is not in the occupation representation we assume it is already in the encoded integer format so just convert to an
                    # integer and check it is a valid basis element.
                    Conf = np.int(Configuration)
                else:
                    # Element is supplied in the occupation representation so we must convert to integer format
                    # Fist we check the lenght
                    if Length < NbrFlux: # if the pattern given is too short, repeat it until it fills out the space.
                        for i in range(NbrFlux-Length):
                            Configuration += Configuration[i % Length]
                    elif Length > NbrFlux:
                        Configuration = Configuration[0:NbrFlux]

                    Conf = Basis.GetElementFromOccupationRep(Configuration)

                if Basis.GetElementIdx(Conf) != -1:
                    Configurations.append(Conf)
            line = f.readline()

    return Configurations


def GetCOM(Configuration, Basis, From=0):
    BasisDetails = Basis.GetBasisDetails()
    Particles = BasisDetails['Particles']
    NbrFlux = BasisDetails['NbrFlux']
    Monomial = Basis.GetMonomialOfElement(Configuration)
    PetscPrint = PETSc.Sys.Print

    # Find the centre of mass so we know which way is the squeezing direction.
    # Start from first particle and sum the shortest distances to the other particles and then
    # divide by the number of particles.
    TotalDistance = 0
    for i in range(0, Particles):
        if i != From:
            if i > From:
                if (Monomial[i] - Monomial[From]) < (NbrFlux - Monomial[i] + Monomial[From]):
                    TotalDistance += Monomial[i] - Monomial[From]
                else:
                    TotalDistance -= NbrFlux - Monomial[i] + Monomial[From]
            else:
                if (Monomial[From] - Monomial[i]) < (NbrFlux - Monomial[From] + Monomial[i]):
                    TotalDistance -= Monomial[From] - Monomial[i]
                else:
                    TotalDistance += NbrFlux - Monomial[From] + Monomial[i]

    COM = (Monomial[From] + TotalDistance/np.float(Particles)) % NbrFlux

    PetscPrint("Total: " + str(TotalDistance) + ", Centre of mass is: " + str(COM))

    return COM


def SqueezeConfiguration(Configuration, Basis, COM, SqueezedConfigurations):
    SqueezedConfigurations = list()
    BasisDetails = Basis.GetBasisDetails()
    Particles = BasisDetails['Particles']
    NbrFlux = BasisDetails['NbrFlux']
    Monomial = Basis.GetMonomialOfElement(Configuration)

    PetscPrint = PETSc.Sys.Print

    import pdb
    NewConfigs = list()
    # Now know the sqeezing direction so iterate through pairs of particles and sqeeze towards COM.
    for i in range(0, Particles - 1):
        for j in range(i+1, Particles):
            NewMonomial = np.array(Monomial)
            BeforeDistance = min((Monomial[i] - COM) % NbrFlux, (COM - Monomial[i]) % NbrFlux)
            BeforeDistance += min((Monomial[j] - COM) % NbrFlux, (COM - Monomial[j]) % NbrFlux)
            pdb.set_trace()

            AfterDistanceInner = min((((Monomial[i] + 1) % NbrFlux) - COM) % NbrFlux, (COM - ((Monomial[i] + 1) % NbrFlux)) % NbrFlux)
            AfterDistanceInner += min((((Monomial[j] - 1) % NbrFlux) - COM) % NbrFlux, (COM - ((Monomial[j] - 1) % NbrFlux)) % NbrFlux)

            AfterDistanceOuter = min((((Monomial[i] - 1) % NbrFlux) - COM) % NbrFlux, (COM - ((Monomial[i] - 1) % NbrFlux)) % NbrFlux)
            AfterDistanceOuter += min((((Monomial[j] + 1) % NbrFlux) - COM) % NbrFlux, (COM - ((Monomial[j] + 1) % NbrFlux)) % NbrFlux)

            if AfterDistanceInner < BeforeDistance:
                NewMonomial[i] = (NewMonomial[i] + 1) % NbrFlux
                NewMonomial[j] = (NewMonomial[j] - 1) % NbrFlux
            elif AfterDistanceOuter < BeforeDistance:
                NewMonomial[i] = (NewMonomial[i] - 1) % NbrFlux
                NewMonomial[j] = (NewMonomial[j] + 1) % NbrFlux
            else:
                continue

            np.sort(NewMonomial)
            Element = Basis.GetElementOfMonomial(NewMonomial)
            if Element != -1:
                SqueezedConfigurations.append(Element)
                NewConfigs.append(Element)


    PetscPrint('Sqeezed from: ' + Basis.GetOccupationRepOfElement(Configuration) )
    for conf in NewConfigs:
        PetscPrint(Basis.GetOccupationRepOfElement(conf) )

    if len(NewConfigs) > 0:
        for conf in NewConfigs:
            SqueezeConfiguration(conf, Basis, COM, SqueezedConfigurations)


def SimpleSqueezeConfiguration(Configuration, Basis, SqueezedConfigurations):
    SqueezedConfigurations = list()
    BasisDetails = Basis.GetBasisDetails()
    Particles = BasisDetails['Particles']
    NbrFlux = BasisDetails['NbrFlux']
    Monomial = Basis.GetMonomialOfElement(Configuration)

    PetscPrint = PETSc.Sys.Print

    NewConfigs = list()
    # Now know the sqeezing direction so iterate through pairs of particles and sqeeze towards COM.
    for i in range(0, Particles - 1):
        for j in range(i+1, Particles):
            NewMonomial = np.array(Monomial)

            Pos1 = Monomial[i]
            Pos2 = Monomial[j]

            Separation = min(Pos2 - Pos1, NbrFlux - Pos2 + Pos1)
            if Separation <= 1:
                continue

            NewPos1 = (Monomial[i] + 1)  % NbrFlux
            NewPos2 = (Monomial[j] - 1)  % NbrFlux
            SeparationInner = min(NewPos2 - NewPos1, NbrFlux - NewPos2 + NewPos1)

            NewPos1 = (Monomial[i] - 1)  % NbrFlux
            NewPos2 = (Monomial[j] + 1)  % NbrFlux
            if NewPos1 > NewPos2:
                tmp = NewPos2
                NewPos2 = NewPos1
                NewPos1 = tmp

            SeparationOuter = min(NewPos2 - NewPos1, NbrFlux - NewPos2 + NewPos1)

            if SeparationOuter < Separation:
                NewMonomial[i] = (NewMonomial[i] - 1) % NbrFlux
                NewMonomial[j] = (NewMonomial[j] + 1) % NbrFlux
            elif SeparationInner < Separation :
                NewMonomial[i] = (NewMonomial[i] + 1) % NbrFlux
                NewMonomial[j] = (NewMonomial[j] - 1) % NbrFlux
            else:
                continue

            np.sort(NewMonomial)
            Element = Basis.GetElementOfMonomial(NewMonomial)
            if Element != -1:
                SqueezedConfigurations.append(Element)
                NewConfigs.append(Element)


    PetscPrint('Sqeezed from: ' + Basis.GetOccupationRepOfElement(Configuration) )
    for conf in NewConfigs:
        PetscPrint(Basis.GetOccupationRepOfElement(conf) )

    if len(NewConfigs) > 0:
        for conf in NewConfigs:
            SimpleSqueezeConfiguration(conf, Basis, SqueezedConfigurations)







def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    PetscPrint = PETSc.Sys.Print

    Parser = argparse.ArgumentParser(description='Utility to calculate the inner product of vectors.')
    Parser.add_argument('state', metavar='vec', type=str, nargs=1)
    Parser.add_argument('--verbose','-v', action='store_true')
    Parser.add_argument('--configuration', '-c', type=str, default='')
    Parser.add_argument('--conf-file', '-f', type=str, default='')
    Parser.add_argument('--occupation', '-o', action='store_true')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose

    # Read the vector, assumed it's in binary format
    A = vectorutils.ReadBinaryPETScVector(Args.state[0])

    BasisManager = FQHBasisManager()
    BasisManager.SetTorusDetailsFromFilename(Args.state[0])
    Basis = BasisManager.GetBasis(Verbose=Verbose)
    Configuration = Args.configuration
    ConfigurationFile = Args.conf_file
    OccupationRep = Args.occupation

    # Read the configurations into a list of valid basis elements.
    Configurations = ParseConfiguration(Configuration, ConfigurationFile, Basis, OccupationRep)

    PetscPrint("Configurations to find weight below")
    for conf in Configurations:
        PetscPrint(Basis.GetOccupationRepOfElement(conf) )

    for i in range(BasisManager.Particles):
        PetscPrint("\nSqeezed configs")
        for conf in Configurations:
            COM = GetCOM(conf, Basis, i)
            SqueezedConfigs = list()
            #SqueezeConfiguration(conf, Basis, COM, SqueezedConfigs)
            SimpleSqueezeConfiguration(conf, Basis, SqueezedConfigs)
            for new_config in SqueezedConfigs:
                PetscPrint(Basis.GetOccupationRepOfElement(new_config))


if __name__ == '__main__' :
    __main__()
