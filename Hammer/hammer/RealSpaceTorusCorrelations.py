#! /usr/bin/env python

""" RealSpaceTorusCorrelations.py: Script to calculate real space torus correlations from wave-function samples. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys
import string
import sys
import argparse
import numpy as np
import time
import h5py
from hammer.FQHTorusWF import FQHTorusWF


def __main__(argv):
    # Run the argument parser and process command line arguments.
    Opts = RealSpaceCorrelationsArgsParser()
    Opts.parseArgs(argv)
    TorusCorrelations(Opts)

def TorusCorrelations(Opts):

    f = h5py.File(Opts.PositionsFile, 'r')
    NbrSamples = f['xvalues'].shape[0]
    if Opts.NbrSamples > 0:
        NbrSamples = Opts.NbrSamples
    if Opts.Verbose:
        print(('Reading ' + str(NbrSamples) + ' positions from ' + Opts.PositionsFile + '.'))
    xpos = f['xvalues'][:NbrSamples,:] + 0.5
    ypos = f['yvalues'][:NbrSamples,:] + 0.5
    f.close()

    f = h5py.File(Opts.SamplesFile, 'r')
    if Opts.Verbose:
        print(('Reading ' + str(NbrSamples) + ' sample values from ' + Opts.SamplesFile + '.'))
    WF_Values = f['wf_values'][:NbrSamples,2] + np.complex(0.0, 1.0)*f['wf_values'][:NbrSamples,3]
    WF_Values = np.exp(WF_Values - WF_Values[np.argmax(np.abs(np.real(WF_Values)))])
    Probs = f['wf_values'][:NbrSamples,0] + np.complex(0.0, 1.0)*f['wf_values'][:NbrSamples,1]
    Probs = np.exp(Probs - Probs[np.argmax(np.abs(np.real(Probs)))])
    f.close()

    NbrParticles = xpos.shape[1]
    if Opts.Verbose:
        print(('Number of particles: ' + str(NbrParticles)))

    G2 = np.zeros((Opts.GridPoints, Opts.GridPoints), dtype=np.float64)
    Rho = np.zeros((Opts.GridPoints, Opts.GridPoints), dtype=np.float64)

    # these are bin centres so must offset to get bin ranges.
    bin_centres = np.arange(0.0, 1.0, 1.0/np.float(Opts.GridPoints)) 
    edges = np.zeros(len(bin_centres)+1, dtype=np.float64)
    edges[:-1] = bin_centres - 1.0/(2.0*np.float(Opts.GridPoints))
    edges[-1] = bin_centres[-1] + 1.0/(2.0*np.float(Opts.GridPoints))

    s = time.time()
    weights = np.real(WF_Values * np.conj(WF_Values)/ (Probs * np.conj(Probs)))
    for i in range(0, NbrParticles):
        #A = np.histogram2d(xpos[:,i], ypos[:,i], Opts.GridPoints, [[0.0,1.0],[0.0,1.0]], weights=weights, normed=True)
        A = np.histogram2d(xpos[:,i], ypos[:,i], bins=[edges, edges], weights=weights, normed=True)
        Rho += A[0]
        for j in range(0, NbrParticles):
            if i != j:
                #H = np.histogram2d((xpos[:,j] - xpos[:,i]) % 1.0, (ypos[:,j] - ypos[:,i])%1.0, Opts.GridPoints, [[0.0,1.0],[0.0,1.0]], weights=weights, normed=True)
                H = np.histogram2d((xpos[:,j] - xpos[:,i]) % 1.0, (ypos[:,j] - ypos[:,i])%1.0, bins=[edges, edges], weights=weights, normed=True)
                G2 += H[0]
    e = time.time()
    print(('Correlator calculated in ' + str(e-s) + ' seconds.'))

    # write to output files
    f = h5py.File(Opts.Output + '_TwoPoint.hdf5', 'w')
    dset = f.create_dataset('two_point', (Opts.GridPoints, Opts.GridPoints), dtype=np.float64)
    dset[:,:] = G2[:,:]
    dset.attrs['Particles'] = NbrParticles
    dset.attrs['NbrSamples'] = NbrSamples
    dset.attrs['TimeTaken'] = e - s
    f.close()


    # write to output files
    f = h5py.File(Opts.Output + '_Density.hdf5', 'w')
    dset = f.create_dataset('density', (Opts.GridPoints, Opts.GridPoints), dtype=np.float64)
    dset[:,:] = Rho[:,:]
    dset.attrs['Particles'] = NbrParticles
    dset.attrs['NbrSamples'] = NbrSamples
    dset.attrs['TimeTaken'] = e - s
    f.close()


class RealSpaceCorrelationsArgsParser:
    """
    Class to parse command line arguments specific to correlations calculation utility.

    """

    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(description='Calculate correlations of the given torus wave-functions.')
        self.Parser.add_argument('--samples', help='File containing the wave-function values and probabilities with which they were sampled.', required=True)
        self.Parser.add_argument('--nbr-samples', type=int, default=0, help='The number of samples to use out of those proviced (default is all samples are used).')
        self.Parser.add_argument('--positions', help='File containing positions of particles corresponding to the samples.', required=True)
        self.Parser.add_argument('--output', '-o', default='TorusRealSpace', help='Output filename prefix.')
        self.Parser.add_argument('--grid-points', '-g', type=int, default=50, help='The number of grid points in each direction to use (default is 50).')
        self.Parser.add_argument('--verbose', action='store_true', help='Flag to indicate verbose output.')


    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.SamplesFile = ArgVals.samples
        self.PositionsFile =  ArgVals.positions
        self.Output = ArgVals.output
        self.Verbose = ArgVals.verbose
        self.GridPoints = ArgVals.grid_points
        self.NbrSamples = ArgVals.nbr_samples


if __name__ == '__main__':
    __main__(sys.argv)
