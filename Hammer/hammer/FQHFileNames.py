#! /usr/bin/env python

""" TorusFileNames.py: Utility tat keeps track of the torus file name structure. """

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"

import argparse
from hammer.HammerArgs import HammerArgsParser
import hammer.FQHFileManager as FileName

def __main__():
    # Run the argument parser and process command line arguments.
    Parser=argparse.ArgumentParser(parents=[HammerArgsParser().BasisParser(),
                                            HammerArgsParser().GeometryParser(),
                                            HammerArgsParser().InteractionParser()],
        description='Show filename for given basis.')
    Parser.add_argument('--type',help=('What type of file extention is needed? '
                                       +'(0=basis, 1=geometry, 2=interaction)'),
                        type=int,default=0)
    Args = Parser.parse_args()
    #print "Args\n",Args
    if Args.type==0:
        print(FileName.TorusBasisShort(Args.bosonic,Args.particles,Args.nbrflux,Args.momentum,
                                       Args.commomentum,Args.inversion_sector))
    elif Args.type==1:
        print(FileName.TorusGeometryShort(Args.bosonic,Args.particles,Args.nbrflux,Args.momentum,
                        Args.commomentum,Args.inversion_sector,Args.tau1,Args.tau2,Args.magnetic_length))
    elif Args.type==2:
        print(FileName.TorusInteractionShort(Args.bosonic,Args.particles,Args.nbrflux,Args.momentum,
                                             Args.commomentum,Args.inversion_sector,Args.tau1,
                                             Args.tau2,Args.magnetic_length,Args.pseudopotentials,
                                             Args.landau_level,Args.interaction,Args.add_three_body))
    else:
        raise ValueError('Type argument is out of range')

if __name__ == '__main__':
    __main__()
