#! /usr/bin/env python

""" TorusEntanglement.py: Script the entanglement spectrum and entropy for orbital cut of torus wave-functons. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys, slepc4py
import string
slepc4py.init(sys.argv)

from petsc4py import PETSc
from slepc4py import SLEPc

# Custom modules
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients2Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients3Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients4Body
from hammer.FQHTorusHamiltonian import FQHTorusHamiltonian
from hammer.HammerArgs import HammerArgsParser
import hammer.vectorutils as vecutils
from hammer.FQHBasisManager import FQHBasisManager
import hammer.matrixutils as matutils
from fractions import gcd

# System modules
import sys
import argparse
import numpy as np
import time
import csv


def __main__(argv):
    # Run the argument parser and process command line arguments.
    Opts = EntanglementArgsParser()
    Opts.parseArgs(argv)
    TorusEntanglement(Opts)

def TorusEntanglement(Opts):
    PetscPrint = PETSc.Sys.Print
    # Read in state descriptions
    NumStates = 0
    if Opts.StateFile != '':  # if a state file was specified
        csv_reader = csv.reader(open(Opts.StateFile, "r"))
        Opts.States = list()
        Opts.Weights = list()
        Opts.Phases = list()
        for line in csv_reader:
            if len(line) < 3:
                PetscPrint("Expect three columns in state file.")
                return
            Opts.States.append(line[0])
            Opts.Weights.append(np.float(line[1]))
            Opts.Phases.append(np.float(line[2]))
        NumStates = len(Opts.States)
    elif Opts.States is not None:
        NumStates = len(Opts.States)
        if Opts.Weights is None:
            Weight = 1.0/np.sqrt(NumStates)
            Opts.Weights = np.ones(NumStates)*Weight
        if Opts.Phases is None:
            Opts.Phases = np.zeros(NumStates)

    # read in states and construct bases
    States = list()
    Coefficients = list()
    BasisManagers = list()
    Details = list()
    Momentums = list()
    COMMomentums = list()

    for i in range(NumStates):
        States.append(vecutils.ReadBinaryPETScVector(Opts.States[i]))
        Coefficients.append(Opts.Weights[i]*np.exp(np.pi*Opts.Phases[i]))
        BasisManager = FQHBasisManager()
        BasisManager.SetTorusDetailsFromFilename(Opts.States[i])
        Details.append(BasisManager.GetDetailsDict())
        BasisManagers.append(BasisManager)

    Particles = Details[0]['Particles']
    NbrFlux = Details[0]['NbrFlux']
    Bosonic = Details[0]['Bosonic'] # bosonic not supported yet
    if Opts.EndingOrbital == -1:
        Opts.EndingOrbital = NbrFlux / 2

    Bases = list()
    for i in range(NumStates):
        Momentum = Details[i]['Momentum']
        COMMomentum = Details[i]['COMMomentum']
        Momentums.append(Momentum)
        COMMomentums.append(COMMomentum)
        InversionSector = Details[i]['InversionSector']
        Basis = BasisManagers[i].GetBasis(Verbose=Opts.Verbose)
        if COMMomentum >= 0:
            [FullBasis, NewState] = Basis.ConvertToFullBasis(States[i])
            Bases.append(FullBasis)
            States[i] = NewState
        else:
            Bases.append(Basis)

    if len(States) == 1:
        if Opts.Verbose:
            PetscPrint("Vector norm: " + str(States[0].dot(States[0])))

        if not Opts.AltCut:
            Matrices = Bases[0].OrbitalEntanglementMatrices(States[0], Opts.StartingOrbital,Opts.EndingOrbital, AParticles=Opts.AParticles)
        else:
            Cells = gcd(BasisManagers[0].Particles, BasisManagers[0].NbrFlux)
            Sectors = BasisManagers[0].NbrFlux/Cells
            if Opts.Verbose: PetscPrint('Considering other ' + str(Sectors - 1) + ' states. These are in momentum sectors ' + str(Cells) + '  apart.')
            BasisSet = list()
            StateSet = list()
            BasisSet.append(Bases[0])
            StateSet.append(States[0])
            for i in range(1, Sectors):
                [NewBasis, NewState] = Bases[0].TranslateState(States[0], i)
                if Opts.Verbose: PetscPrint('Translated state has details: ' + str(NewBasis.GetBasisDetails()))
                if np.abs(1.0 - NewState.norm()) > 1e-9:
                    PetscPrint('Translated state only has norm ' + str(NewState.norm()) + '.')
                    return
                BasisSet.append(NewBasis)
                StateSet.append(NewState)

            Coefficients = np.ones(Sectors)/np.sqrt(Sectors)
            Matrices = FQHTorusBasis.OrbitalEntanglementMatricesSuperpositionSymmetry(BasisSet, StateSet,
                                                                                      Coefficients,
                                                                                      Opts.StartingOrbital,
                                                                                      Opts.EndingOrbital,
                                                                                      AParticles=Opts.AParticles)

    else:
        if not Bosonic:
            Matrices = FQHTorusBasis.OrbitalEntanglementMatricesSuperpositionSymmetry(Bases, States,
                                                                                      Coefficients,
                                                                                      Opts.StartingOrbital,
                                                                                      Opts.EndingOrbital,
                                                                                      AParticles=Opts.AParticles)
        else:
            Matrices = FQHTorusBasisBosonic.OrbitalEntanglementMatricesSuperposition(Bases, States,
                                                                                     Coefficients,
                                                                                     Opts.StartingOrbital,
                                                                                     Opts.EndingOrbital,
                                                                                     AParticles=Opts.AParticles)


    SingularValues = FQHTorusBasis.EntanglementSingularValues(Matrices)
    EE = FQHTorusBasis.EntanglementEntropy(SingularValues)
    ES = FQHTorusBasis.EntanglementSpectrum(SingularValues)

    if Opts.Verbose:
        PetscPrint("A particles: " + str(Opts.AParticles))
        PetscPrint("Trace of reduced density matrix: " + str(FQHTorusBasis.EntanglementNorm(SingularValues)))
        PetscPrint("For partition A from " + str(Opts.StartingOrbital) + " to " + str(Opts.EndingOrbital)
                   + " EE is " + str(EE))
        #print("For partition A from " + str(Opts.StartingOrbital) + " to " + str(Opts.EndingOrbital) + " ES is: ")
        #for AParticles in list(ES.keys()):
        #    print("Na = " + str(AParticles))
        #    print(str(ES[AParticles]))


    if Opts.Name == '':
        FileNamePrefix = ("TorusEntanglement_p_" + str(Particles) + "_l_" + str(NbrFlux) + "_" +
                         str(Opts.StartingOrbital) + "_to_" + str(Opts.EndingOrbital))
    else:
        FileNamePrefix = (Opts.Name + "_p_" + str(Particles) + "_l_" + str(NbrFlux) + "_"
                          + str(Opts.StartingOrbital) + "_to_" + str(Opts.EndingOrbital))

    if PETSc.COMM_WORLD.getRank() == 0:
        if sys.version_info[0] == 3:
            f = open(FileNamePrefix + ".ent", "w", encoding='utf8', newline='') # python 3 version
        elif sys.version_info[0] == 2:
            f = open(FileNamePrefix + ".ent", "wb")
        f.write(str(EE))
        f.close()

        if sys.version_info[0] == 3:
            f = open(FileNamePrefix + ".entspec", "w", encoding='utf8', newline='') # python 3 version
        elif sys.version_info[0] == 2:
            f = open(FileNamePrefix + ".entspec", "wb")
        csv_writer = csv.writer(f)
        csv_writer.writerow(["AParticles", "AMomentum", "Value"])
        ParticleKeys = list()
        MomentumKeys=  list()

        for key in list(ES.keys()):
            if np.isscalar(key):
                ParticleKeys.append(key)
            else:
                ParticleKeys.append(key[0])
                MomentumKeys.append(key[1])

        ParticleKeysSet = set(ParticleKeys)
        ParticleKeys = list(ParticleKeysSet)
        ParticleKeys.sort()
        MomentumKeysSet = set(MomentumKeys)
        if MomentumKeys is not None:
            MomentumKeys = list(MomentumKeysSet)
            MomentumKeys.sort()

        if MomentumKeys is None or len(MomentumKeys) == 0:
            for ParticleKey in ParticleKeys:
                values = ES[ParticleKey]
                for val in values:
                    csv_writer.writerow([ParticleKey, -1, val])
        else:
            for ParticleKey in ParticleKeys:
                for MomentumKey in MomentumKeys:
                    if (ParticleKey, MomentumKey) in ES:
                        values = ES[(ParticleKey, MomentumKey)]
                        for val in values:
                            csv_writer.writerow([ParticleKey, MomentumKey, val])

        f.close()

class EntanglementArgsParser:
    """
    Class to parse command line arguments specific to entanglement calculation utility.

    """

    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(description='Calculate entanglement properties of the specified torus wave-functions.')
        self.Parser.add_argument('--states', nargs='+', help='The state(s) to use to calculate entanglement.')
        self.Parser.add_argument('--state-file', default='', help='CSV file containing list of states to use, as well as weights and phases. Format is: state filename in first column, weight in second and phase (in multiples of pi) in the third.')
        self.Parser.add_argument('--weights', type=float, nargs='+', help='If more than one state specfied, this is the weights to use for each. Default is equal weight.')
        self.Parser.add_argument('--phases', type=float, nargs='+', help='The phase for each state, given in multiples of pi (default is 0).')
        self.Parser.add_argument('--starting_orbital', '-f', default=0, type=int, help='Orbital to start the A partition from labelled 0 to Nphi - 1 (default 0).')
        self.Parser.add_argument('--ending_orbital', '-t', default=-1, type=int, help='Orbital to end the A partition by, this orbital will be the first of the B partition (default is Nphi/2).')
        self.Parser.add_argument('--name', default='', help='Indentifier to use for output filenames.')
        self.Parser.add_argument('--aparticles', default=-1, type=int, help='Number of particles in partition A.')
        self.Parser.add_argument('--alternative-cut', action='store_true', help='Find entanglement for cut other direction.')
        self.Parser.add_argument('--verbose', action='store_true', help='Flag to indicate verbose output.')


    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.States = ArgVals.states
        self.Weights = ArgVals.weights
        self.Phases = ArgVals.phases
        self.StateFile = ArgVals.state_file
        self.StartingOrbital = ArgVals.starting_orbital
        self.EndingOrbital = ArgVals.ending_orbital
        self.Name = ArgVals.name
        self.AParticles = ArgVals.aparticles
        self.AltCut = ArgVals.alternative_cut
        self.Verbose = ArgVals.verbose


if __name__ == '__main__':
    __main__(sys.argv)
