#! /usr/bin/env python

""" ConvertBinToHDF.py: Script to convert a binary PETSc vector to a HDF5 version. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

from petsc4py import PETSc
import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils
from hammer.FQHBasisManager import FQHBasisManager
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to calculate the inner product of vectors.')
    Parser.add_argument('state', metavar='vec', type=str, nargs=1)
    Parser.add_argument('--output', '-o', type=str, default='')
    Parser.add_argument('--real','-r', action='store_true', help='Try to make WF real by applying global phase change.')
    Parser.add_argument('--verbose','-v', action='store_true')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    OutputFilename = Args.output
    State = Args.state[0]
    Real = Args.real
    if OutputFilename == '':
        OutputFilename = State.replace('.vec', '.hdf5')

    # Read the vector, assumed it's in binary format
    A = vectorutils.ReadBinaryPETScVector(State)

    if Real:
        vectorutils.RotateToMakeReal(A)

    # Write file in HDF5 format
    vectorutils.writeHDF5File(A, OutputFilename)


if __name__ == '__main__' :
    __main__()
