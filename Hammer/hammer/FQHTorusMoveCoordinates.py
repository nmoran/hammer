#!/usr/bin/python
from __future__ import print_function

""" FQHTorusMoveCoordinates.py: Core that moves the torus corinates a sertain distance. """

__author__      = "Mikael Freming"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"



import numpy as np
import argparse
import h5py
import sys

def __main__():

    parser = argparse.ArgumentParser(description="Move the Coordinates a certain distance in x,y direction.")
    parser.add_argument("--coord-in", type=str, help="Input file for x,y coordinates",required=True)
    parser.add_argument("--coord-out", type=str, help="Output file for x,y coordinates",required=True)
    parser.add_argument("-Ns", type=int, help="Number of fluxes in system",required=True)
    groupx=parser.add_mutually_exclusive_group()
    groupx.add_argument("-x", type=float, help="Number of translation steps in the x-direction")
    groupx.add_argument("-x-list", type=str, help="Number of translation steps in the x-direction for each particle.",nargs='+')
    groupy=parser.add_mutually_exclusive_group()
    groupy.add_argument("-y", type=float, help="Number of translation steps in the y-direction")
    groupy.add_argument("-y-list", type=str, help="Number of translation steps in the y-direction for each particle.",nargs='+')
   
    Args=parser.parse_args()

    CoordFile_in=Args.coord_in
    CoordFile_out=Args.coord_out

    print('Read coordinates from file:',CoordFile_in)
    with h5py.File(CoordFile_in, 'r') as coord:
        xcoord=np.array(coord.get('xvalues'))
        ycoord=np.array(coord.get('yvalues'))

    (MC,Ne)=xcoord.shape
    xshift=OptToSHift(Args.x,Args.x_list,Ne,Args.Ns,'x')
    yshift=OptToSHift(Args.y,Args.y_list,Ne,Args.Ns,'y')
        
    xcoord+=xshift
    ycoord+=yshift

    print('Saving new coordinates to file:',CoordFile_out)
    try:
        of = h5py.File(CoordFile_out, 'w')
    except IOError as ioerror:
        print(("ERROR: Problem writing to file " + CoordFile_out  + ': ' + str(ioerror)))
        sys.exit(-1)
    of.create_dataset("xvalues", data=xcoord)
    of.create_dataset("yvalues", data=ycoord)
    of.close()

def OptToSHift(x,x_list,Ne,Ns,Type):
    if x==None and x_list==None:
        xshift=0.0
    elif x!=None:
        xshift=x/float(Ns)
    elif x_list!=None:
        print("ERROR: "+Type+"_list='"+x_list+"' not zero not implemented yet")
        sys.exit(-1)
    else:
        print("ERROR: This option should not happen")
        sys.exit(-1)
    return xshift

    
if __name__ == '__main__' :
    __main__()
