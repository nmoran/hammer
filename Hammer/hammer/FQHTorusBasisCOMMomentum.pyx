#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusBasisCOMMomentum.pyx: This is a cython implementation of code to work with the FQH basis on a Torus with centre of mass momentum symmetry. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
cimport numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
from petsc4py import PETSc
from hammer.FQHTorusBasis cimport FQHTorusBasis


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)

cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double complex conj(double complex x) nogil


cdef class FQHTorusBasisCOMMomentum(FQHTorusBasis):
#    cdef int COMMomentum
#    cdef int Distance # amount by which to translate each time.
#    cdef int Sectors # number of translations to get all the way around.
#    cdef long Mask # mask which covers all the flux in the system.
#    cdef long *OverFlowMasks  # array of masks, which cover the overflow section for each translation.
#    cdef double complex *Phases # array to hold pre computed phases to save time.
#    cdef long TmpRepresentative
#
    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.Distance = 0
        self.COMMomentum = -1
        self.Sectors = 0


    def __init__(self,int Particles, int NbrFlux, int Momentum, int COMMomentum):
        """
        Set the variables. Inputs are:
        Particles: Number of particles in the basis.
        NbrFlux: Number of flux quanta to consider.
        Momentum: The total momentum, sum of the orbital angular momentum of each particle.
        COMMomentum: The center of mass momentum. If -1 then do not use this symmetry.
        """
        super().__init__(Particles, NbrFlux, Momentum)
        self.COMMomentum = COMMomentum

        # work out the translation distance underwhich the Hamiltonian is invariant and the number of sectors.
        # If the number of particles does not divide into the number of flux, then keep checking larger and larger multiples of the nubmer of flux.
        n = 1
        while n < self.Particles:
            if ((n * self.NbrFlux) % self.Particles) == 0:
                break;
            else:
                n += 1

        # now set the translation distance that takes one back to the same momemntum sector.
        self.Distance = 1
        if n < self.Particles and ((n * self.NbrFlux) % self.Particles) == 0 :
            self.Distance = (n * self.NbrFlux) / self.Particles
        else:
            self.Distance = self.NbrFlux # there is only going to be a single sector, must translate all the way around

        # number of sectors is then how many of these translations fit around the torus.
        self.Sectors = self.NbrFlux / self.Distance

        # now pre-assign masks to make the translations faster.
        self.OverFlowMasks = <long *>PyMem_Malloc(self.Sectors * sizeof(long))
        for i in range(self.Sectors):
            self.OverFlowMasks[i] = self.Mask - ((1l << (self.NbrFlux - self.Distance * i)) - 1l)

        # precompute phases to be used.
        self.Phases = <double complex*>PyMem_Malloc(self.Sectors * sizeof(double complex))
        for i in range(self.Sectors):
            self.Phases[i] = cexp(1j * 2.0 * np.pi * np.float(i) * np.float(self.COMMomentum) / np.float(self.Sectors))


    def FindBasisDimension(self, int J, int Nphi, int Ne):
        """
        Calculates the basis dimension for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasisDimension to calculate the dimension of the basis.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 - i)

        cdef int MaxTotMomentum = Nphi * (MaxMomentum / Nphi) + J

        return  self.FindSubBasisDimensionCOMMomentum(MaxTotMomentum,Nphi,Nphi,Ne,idx,0l)


    cdef int FindSubBasisDimensionCOMMomentum(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis dimension for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0) and self.IsRepresentativeConfiguration(base):  # this is used instead of the standard modulus operator
                return 1
            else:
                return 0
        elif Ne > Nphi:
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasisDimensionCOMMomentum(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))

            idx += offset
            offset += self.FindSubBasisDimensionCOMMomentum(J, NphiTot, Nphi-1, Ne, idx, base)
        return offset


    def FindBasis(self, int J, int Nphi, int Ne):
        """
        Calculates the basis elements for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasis to calculate the basis elements and populate the BasisElements array.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 - i)

        cdef int MaxTotMomentum = Nphi * (MaxMomentum / Nphi) + J

        return  self.FindSubBasis(MaxTotMomentum,Nphi,Nphi,Ne,idx,0l)


    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis elements for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0) and self.IsRepresentativeConfiguration(base):  # this is used instead of the standard modulus operator
                self.BasisElements[idx] = base
                return 1
            else:
                return 0
        elif Ne > Nphi:
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasis(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))

            idx += offset
            offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne,idx, base)
        return offset


    cdef int IsRepresentativeConfiguration(self, long State):
        """
        Method to check if the configuration passed is a representative configuration.
        """
        cdef long TranslatedState
        cdef double complex Normal = 0.0

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(State, i)
            if TranslatedState < State:
                return 0
            if TranslatedState == State:
                Normal += self.Phases[i] * self.TranslatePhase(State, i)
            i += 1

        if cabs(Normal) > 1e-10:
            return 1
        else:
            return 0


    cdef  double GetNormalIdx(self, Idx):
        """
        Given the index of a basis element, calculate the normal
        """
        cdef double complex Normal = 0.0
        cdef long TranslatedState

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.BasisElements[Idx], i)
            if TranslatedState == self.BasisElements[Idx]:
                Normal += self.Phases[i] * self.TranslatePhase(self.BasisElements[Idx], i)
            i += 1

        return sqrt(creal(Normal))


    cdef double GetNormal(self):
        """
        Get the normal of the configuration stored in self.TmpState2
        """
        cdef double complex Normal = 0.0
        cdef long TranslatedState

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.TmpState2, i)
            if TranslatedState == self.TmpState2:
                Normal += self.Phases[i] * self.TranslatePhase(self.TmpState2, i)
            i += 1

        return sqrt(creal(Normal))


    cpdef long GetRepresentative(self):
        """
        Try to find the index of the representative of element TmpState2
        """
        cdef long RepState = self.TmpState2
        cdef long TranslatedState

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.TmpState2, i)
            if TranslatedState < RepState:
                RepState = TranslatedState
            i+=1

        self.TmpRepresentative = RepState

        return RepState


    cpdef double complex GetRepresentativePhase(self):
        """
        Try to find phase relating the configuration in tmpstate2 and its representative configuration.
        """
        cdef double complex Phase = 0.0
        cdef long TranslatedState

        cdef int i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.TmpState2, i)
            if TranslatedState == self.TmpRepresentative:
                Phase += conj(self.Phases[i] * self.TranslatePhase(self.TmpState2, i))
            i+=1

        return Phase


    cpdef int BinaryArraySearchDescending(self):
        """
        Function to perform binary search on array of BasisElements for TmpState2
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef long l = self.Dim

        cdef long start = 0
        cdef long end = l
        cdef long dist = end - start
        cdef long test_item
        cdef long item = self.TmpRepresentative
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply density operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx.
        """
        self.TmpRepresentative = self.BasisElements[Idx]
        self.TmpState2 = self.BasisElements[Idx]
        return  self.GetRepresentativePhase() * FQHTorusBasis.NN(self,Orbital1, Orbital2, Idx) / (self.GetNormalIdx(Idx) * np.conj(self.GetNormalIdx(Idx)))


    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply density operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx.
        """
        self.TmpRepresentative = self.BasisElements[Idx]
        self.TmpState2 = self.BasisElements[Idx]
        return  self.GetRepresentativePhase() * FQHTorusBasis.NNN(self,Orbital1, Orbital2, Orbital3, Idx) / (self.GetNormalIdx(Idx) * np.conj(self.GetNormalIdx(Idx)))


    cpdef double AA(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx and store the result in TmpState.
        TODO: investigate popcnt implementation to make this more efficient.
        """
        return  FQHTorusBasis.AA(self,Orbital1, Orbital2, Idx) / self.GetNormalIdx(Idx)


    cpdef double complex AdAd(self, int Orbital1, int Orbital2):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.
        """
        cdef double complex Phase, Normal
        Phase = FQHTorusBasis.AdAd(self,Orbital1, Orbital2)
        #print "AdAd phase: " + str(Phase)
        self.GetRepresentative()
        Phase *= self.GetRepresentativePhase()
        #print "Rep phase: " + str(self.GetRepresentativePhase())
        Normal = self.GetNormal()
        #print "Normal: " + str(Normal)
        if Normal == 0:
            return 0.0
        else:
            return  Phase / Normal


    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1, Orbital2 and Orbital3 to basis elements stored at index Idx and store the result in TmpState.
        """
        return  FQHTorusBasis.AAA(self,Orbital1, Orbital2, Orbital3, Idx) / self.GetNormalIdx(Idx)


    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.
        """
        cdef double complex Phase, Normal
        Phase = FQHTorusBasis.AdAdAd(self,Orbital1, Orbital2, Orbital3)
        self.GetRepresentative()
        Phase *= self.GetRepresentativePhase()
        Normal = self.GetNormal()
        if Normal == 0:
            return 0.0
        else:
            return  Phase / Normal


    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1, Orbital2, Orbital3 and Orbital4 to basis elements stored at index Idx and store the result in TmpState.

        """
        return  FQHTorusBasis.AAAA(self,Orbital1, Orbital2, Orbital3, Orbital4, Idx) / self.GetNormalIdx(Idx)


    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4):
        """
        Apply creation operators on sites with momentum Orbital1 ,Orbital2, Orbital3 and Orbital4 to state TmpState and store result in TmpState2.
        """
        cdef double complex Phase, Normal
        Phase = FQHTorusBasis.AdAdAdAd(self,Orbital1, Orbital2, Orbital3, Orbital4)
        self.GetRepresentative()
        Phase *= self.GetRepresentativePhase()
        Normal = self.GetNormal()
        if Normal == 0:
            return 0.0
        else:
            return  Phase / Normal


    cpdef CreateConversionToFullOp(self, FullBasis):
        """
        This method creates the operator that will convert a state in this basis to the full basis given.

        Parameters
        -----------
        FullBasis: basis object
            Basis object of full basis.

        Returns
        -------
        matrix
            If ReturnOp is true the projection operator is returned.
        """
        cdef int BasisDim = self.Dim
        cdef int FullBasisDim = FullBasis.GetDimension()
        cdef int rstart, rend, Idx

        # create state to get typical ownership range.
        State = PETSc.Vec()
        State.create()
        State.setSizes(BasisDim)
        State.setUp()
        rstart, rend = State.getOwnershipRange()
        State.destroy()

        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)

        if (self.Sectors) > (BasisDim-1):
            Op = PETSc.Mat(); Op.createDense([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            Op.setUp()
        else:
            Op = PETSc.Mat(); Op.create()
            Op.setSizes([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            #Op.setOption(PETSc.Vec.Option.IGNORE_NEGATIVE_INDICES, 1)
            nnz = nnz * self.Sectors
            Op.setPreallocationNNZ((nnz, nnz)) # one term per sector
            Op.setUp()

        cdef int j, i = 0
        cdef double complex Phase
        cdef double Normal
        cdef long TranslatedState
        cdef double OverallNorm = np.sqrt(self.Sectors)

        i = rstart
        while i < rend:
            j = 0
            while j < self.Sectors:
                TranslatedState = self.SimpleTranslate(self.GetElement(i), j)
                Phase = self.TranslatePhase(self.GetElement(i), j) * self.Phases[j]
                Normal = self.GetNormalIdx(i)
                Idx = FullBasis.BinaryArraySearchDescendingState(TranslatedState)
                if Idx >= 0 and Idx < FullBasisDim:
                    Op.setValue(i, Idx, Phase/(Normal * OverallNorm), True)
                j += 1
            i += 1
        Op.assemble()

        return Op


    cpdef ConvertToFullBasis(self, State, FullBasis=None, ReturnOp=False):
        """
        This method expands the state in the reduced centre of mass momentum basis to the full basis.

        Parameters
        -----------
        State: vector
            Vector of Fock coefficients of state to convert.
        FullBasis: basis object
            Basis object of full basis. If None basis is constructed.
        ReturnOp: bool
            Flag to indicate that the projection operator should be returned.

        Returns
        -------
        basis object
            Full basis.
        vector
            Full state vector.
        matrix
            If ReturnOp is true the projection operator is returned.
        """
        if FullBasis is None:
            FullBasis = FQHTorusBasis(self.Particles, self.NbrFlux, self.Momentum)
            FullBasis.GenerateBasis()

        Op = self.CreateConversionToFullOp(FullBasis)

        cdef int FullBasisDim = FullBasis.GetDimension()

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(FullBasisDim)
        NewState.setUp()

        Op.multTranspose(State, NewState)

        if ReturnOp:
            return [FullBasis, NewState, Op]
        else:
            Op.destroy()
            return [FullBasis, NewState]


    cpdef ConvertFromFullBasis(self, FullState, FullBasis=None, ReturnOp=False):
        """
        This method projects the given state in the full basis onto the subspace spanned by this basis.

        Parameters
        -----------
        FullState: vector
            Vector of Fock coefficients of state to convert.
        FullBasis: basis object
            Basis object of full basis. If None basis is constructed.
        ReturnOp: bool
            Flag to indicate that the projection operator should be returned.

        Returns
        -------
        basis object
            Full basis.
        vector
            Full state vector.
        matrix
            If ReturnOp is true the projection operator is returned.
        """
        if FullBasis is None:
            FullBasis = FQHTorusBasis(self.Particles, self.NbrFlux, self.Momentum)
            FullBasis.GenerateBasis()

        Op = self.CreateConversionToFullOp(FullBasis)
        OpC = Op.conjugate()

        cdef int BasisDim = self.Dim

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(BasisDim)
        NewState.setUp()

        OpC.mult(FullState, NewState)

        if ReturnOp:
            return [FullBasis, NewState, Op]
        else:
            Op.destroy()
            return [FullBasis, NewState]


    def GetBasisDetails(self):
        """
        Returns a dictionary object with parameters of the basis.
        """
        Details = super().GetBasisDetails()
        Details['COMMomentum'] = self.COMMomentum
        return Details


    cpdef double complex GetPhase(self, int Idx):
        """
        Returns the phase corresponding to the provided index, or 0 if out of range
        """
        if Idx < 0 or Idx >= self.Sectors:
            return 0.0
        else:
            return self.Phases[Idx]


    def GetPHSector(self):
        """
        Get the particle number and momentum sector in which to find the PH conjugated basis.

        Returns
        -------
        list
            List containing particle number, momentum and COM momentum at which to find PH conjugated basis.
        """
        # calculate momentum of PH conjugate by subtracting current momentum from fully filled momentum
        PHMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.Momentum) % self.NbrFlux
        PHCOMMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.COMMomentum) % self.Sectors
        PHParticles = self.NbrFlux - self.Particles
        return PHParticles, PHMomentum, PHCOMMomentum


    cpdef ConvertToPHConjugate(self, State, PHBasis=None, ReturnOp=False):
        """
        This method converts the state given into the particle hole conjugate state.

        Parameters
        ------------
        State: vec
            Vector object of state to convert.
        PHBasis: basis
            Basis object of PH conjugate or None if not given (default=None).
        ReturnOp: bool
            Flag to indicate whether to return the operator that transforms a state to
            its particle hole conjugate (default=False).

        Returns
        ----------
        basis
            Basis object of PH conjugate state.
        vec
            Vector containing coefficients of PH conjugate state.
        mat
            The operator that transforms the state, only returned in ReturnOp is True.
        """
        # calculate momentum of PH conjugate by subtracting current momentum from fully filled momentum
        PHMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.Momentum) % self.NbrFlux
        PHCOMMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.COMMomentum) % self.Sectors
        PHParticles = self.NbrFlux - self.Particles

        if PHBasis is None:
            PHBasis = FQHTorusBasisCOMMomentum(PHParticles, self.NbrFlux, PHMomentum, PHCOMMomentum)
            PHBasis.GenerateBasis()

        cdef int BasisDim = self.Dim
        cdef int rstart, rend, Idx
        rstart, rend = State.getOwnershipRange()
        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([(LocalDim, BasisDim), BasisDim])
        Op.setFromOptions()
        Op.setPreallocationNNZ((nnz, nnz)) # one term per sector
        Op.setUp()

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(BasisDim)
        NewState.setUp()

        cdef long Element, Representative
        cdef double complex phase
        cdef double Normal

        cdef int i = rstart
        while i < rend:
            Element = self.GetElement(i)
            if self.CalcParityOfElement(Element) == 1:
                phase = -1.0
            else:
                phase = 1.0
            self.SetTmpState2(Element)
            Normal = self.GetNormal()
            # take inverse
            Element = (~Element) & self.Mask
            PHBasis.SetTmpState2(Element)
            Representative = PHBasis.GetRepresentative()
            phase *= PHBasis.GetRepresentativePhase()
            Idx = PHBasis.BinaryArraySearchDescendingState(Representative)
            if Idx >= 0 and Idx < BasisDim:
                Op.setValue(i, Idx, phase/(Normal*Normal), False)
            i += 1
        Op.assemble()

        Op.multTranspose(State, NewState)

        NewState.conjugate()

        if ReturnOp:
            return [PHBasis, NewState, Op]
        else:
            return [PHBasis, NewState]


    def GetInvSector(self):
        """
        Get the momentum sector inverted basis.

        Returns
        -------
        list
            List containing momentum and COM momentum at which to find the inverted basis.
        """
        IMomentum = (2*self.NbrFlux - self.Momentum - self.Particles) % self.NbrFlux
        ICOMMomentum = (self.Sectors - self.COMMomentum) % self.Sectors
        return IMomentum, ICOMMomentum


    cpdef ConvertToInvertedState(self, State, IBasis=None, ReturnOp=False):
        """
        This method inverts the given state by reflecting all orbitals around the central orbital. Currently works
        by expanding to full basis, inverting and then converting back.

        Parameters
        ------------
        State: vec
            Vector object of state to convert.
        IBasis: basis
            Basis object of PH conjugate or None if not given (default=None).
        ReturnOp: bool
            Flag to indicate whether to return the operator that transforms a state to
            its particle hole conjugate. Not implemented yet, returns None (default=False).

        Returns
        ----------
        basis
            Basis object of PH conjugate state.
        vec
            Vector containing coefficients of PH conjugate state.
        mat
            The operator that transforms the state, only returned in ReturnOp is True (not implemented returns None).
        """
        # calculate momentum of inverted state
        IMomentum = (2*self.NbrFlux - self.Momentum - self.Particles) % self.NbrFlux
        ICOMMomentum = (self.Sectors - self.Particles) % self.Sectors

        FullBasis, FullState = self.ConvertToFullBasis(State)
        FullIBasis, FullIState = self.ConvertToInvertedState(FullState)

        if IBasis is None:
            IBasis = FQHTorusBasisCOMMomentum(self.Particles, self.NbrFlux, IMomentum, ICOMMomentum)
            IBasis.GenerateBasis()

        FullIBasis, IState = IBasis.ConvertFromFullBasis(FullIState, FullIBasis)

        if ReturnOp:
            return [IBasis, IState, None]
        else:
            return [IBasis, IState]


    cpdef long SimpleTranslate(self, long State, int Steps):
        """
        Function that translates the given configuration by t orbitals.
        """
        # translate everything to the left, then translate the overflow portion back to the right, and lastly take a mask of everthing.
        return  (((State << (Steps * self.Distance)) + ((State & self.OverFlowMasks[Steps]) >> (self.NbrFlux - (Steps * self.Distance)))) & self.Mask)


    cpdef double TranslatePhase(self, long State, int Steps):
        """
        Function that gives the phase due to parity of particles in the overflow section.
        """
        if (self.Particles % 2) == 1: # if the number of particles is odd then always return a phase of 1
            return 1.0

        # this gets the partity of the section in question. From bithacks page https://graphics.stanford.edu/~seander/bithacks.html#ParityMultiply
        cdef unsigned long v = <unsigned long>(State & self.OverFlowMasks[Steps])
        v ^= v >> 1;
        v ^= v >> 2;
        v = (v & 0x1111111111111111UL) * 0x1111111111111111UL;

        if ((v >> 60) & 1):
            return -1.0
        else:
            return 1.0

