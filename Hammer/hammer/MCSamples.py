#! /usr/bin/env python

"""
MCSamples.py: Some utilities and data structures for dealing with sets of samples.

"""

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils
import h5py
import petsc4py.PETSc as PETSc
import csv
import time
import itertools as iter
import hammer


class Samples(object):
    """
    Class that represents sets of samples to be used in Monte Carlo calculations.
    """
    def __init__(self, N, M, log_scale=False, real=False):
        """
        Constructor for Samples class

        Parameters
        ----------
        N: int
            The number of samples.
        M: int
            The number of different sample sets.
        log_scale: bool
            Flag to indicate log scale used (default=False).
        """
        self.N = N
        self.M = M
        self.log_scale = log_scale
        self.real = real


    def get_samples_array(self, number=-1, offset=0, dataset=-1, log_scale=False, norm=False):
        """
        Function to retrieve samples in requested range.

        Parameters
        ------------
        number: int
            Number of samples to retrieve
        offset: int
            Offset position to start from.
        dataset: int
            Dataset to return or -1 to return all (default=-1).
        log_scale: bool
            Flag to indicate samples should be in log scale (default=False).
        norm: bool
            Flag to indicate that samples should be normalised by dividing by largest sample (default=False).

        Returns
        ----------
        matrix
            Array of samples from requested range of datasets.
        scalars
            If norm is true then returns the log of the value used to normalise samples. If norm is false then only returns the array.
        """
        if number == -1: number = self.N
        samples = self.get_samples(number, offset, dataset)
        return samples.get_samples_array(number, offset=0, dataset=-1, log_scale=log_scale, norm=norm)


class SamplesArray(Samples):
    """
    Class that represents a set of samples as a numpy array in memory.
    """

    def __init__(self, data, log_scale=False, real=False):
        """
        Constructor for SamplesArray class

        Parameters
        ----------
        data: np.ndarray
            Array storing samples, one set per column (unless real and imaginary part stored separately).
        log_scale: bool
            Flag to indicate log scale used (default=False).
        real: bool
            Flag to indicate that samples are real (default=False).
        """
        N = data.shape[0]
        self.split = False # flag to indicate dataset split over different columns
        if real:
            M = data.shape[1]
        else:
            if np.issubdtype(np.complex, data.dtype):
                M = data.shape[1]
            else:
                if (data.shape[1] % 2) == 1:
                    print('Number of columns expected to be even!, shape is ' + str(data.shape) + ' and data type is ' + str(data.dtype))
                    raise ValueError
                    hammer.exit(-1)
                M = data.shape[1]/2
                self.split = True
        super(self.__class__, self).__init__(N, M, log_scale, real)
        self.data = data


    def get_samples(self, number=-1, offset=0, dataset=-1):
        """
        Function to retrieve samples in requested range.

        Parameters
        ------------
        number: int
            Number of samples to retrieve
        offset: int
            Offset position to start from.
        dataset: int
            Dataset to return or -1 to return all (default=-1).


        Returns
        ----------
        SamplesArray
            A samples array object with the requested samples.
        """
        if number == -1: number = self.N
        if dataset == -1:
            if self.split:
                samples = (self.data[offset:(offset+number), ::2]
                           +self.data[offset:(offset+number), 1::2]*1j)
            else:
                samples = self.data[offset:(offset+number), :]
        else:
            if self.split:
                samples = (self.data[offset:(offset+number), dataset*2]
                           +self.data[offset:(offset+number), dataset*2+1]*1j)
            else:
                samples = self.data[offset:(offset+number), dataset]

        return SamplesArray(samples, self.log_scale, self.real)


    def get_samples_array(self, number=-1, offset=0, dataset=-1, log_scale=False, norm=False):
        """
        Function to retrieve samples in requested range.

        Parameters
        ------------
        number: int
            Number of samples to retrieve.
        offset: int
            Offset position to start from.
        dataset: int
            Datset to retrieve, if -1 get them all (default=-1).
        log_scale: bool
            Flag to indicate samples should be in log scale (default=False).
        norm: bool
            Flag to indicate that samples should be normalised by dividing by largest sample (default=False).

        Returns
        ----------
        matrix
            Array of samples from requested range.
        array
            If norm is true then returns the log of the value used to normalise samples. If norm is false then only returns the array.
        """
        if number == -1: number = self.N
        sample_obj = self.get_samples(number, offset, dataset)
        samples = sample_obj.data

        max_samples_log = np.zeros(self.M)
        if norm:
            if self.log_scale:
                max_samples_log = np.reshape(np.max(np.real(samples), axis=0), (1, self.M))
                samples = samples - max_samples_log
            else:
                max_samples = np.reshape(np.max(np.abs(samples), axis=0), (1, self.M))
                samples = samples / max_samples
                max_samples_log = np.log(max_samples)

        if log_scale == self.log_scale:
            return (samples if not norm else [samples, max_samples_log])
        elif self.log_scale:
            return (np.exp(samples) if not norm else [np.exp(samples), max_samples_log])
        else: return (np.log(samples) if not norm else [np.log(samples), max_samples_log])


class SamplesHDF5(Samples):
    """
    Class that represents a set of samples stored on disk in a HDF5 file.
    """

    def __init__(self, filename, datasets, log_scale=False, real=False):
        """
        Constructor for SamplesHDF5 class

        Parameters
        ----------
        filename: str
            Filename of HDF5 file where samples are stored.
        dataset: list
            Datasets inside HDF5 file where samples are found. If two given
            it is assumed that the first are the real parts and the second the
            complex parts.
        log_scale: bool
            Flag to indicate log scale used (default=False).
        real: bool
            Flag to indicate that samples are real (default=False).
        """
        self.filename = filename
        self.datasets = datasets

        # setting the split flag which indicates that samples are split across
        # datasets.
        if len(datasets) == 2:
            self.split = True
        elif len(datasets) == 1:
            self.split = False
        else:
            print('Datasets must contain either 1 or 2 strings.')
            hammer.exit(-1)

        try:
            f = h5py.File(filename, 'r')
            N = f[datasets[0]].shape[0]
            if real or self.split:
                M = f[datasets[0]].shape[1]
            else:
                if (f[datasets[0]].shape[1] % 2) == 1:
                    print('Expected even number of columns in dataset.')
                    hammer.exit(-1)
                M = f[datasets[0]].shape[1]/2
            f.close()
        except IOError as ioerror:
            print('Error reading ' + filename + '.')
            hammer.exit(-1)

        self.super(N, M, log_scale, real)


    def get_samples(self, number=-1, offset=0, dataset=-1):
        """
        Function to retrieve samples in requested range.

        Parameters
        ------------
        number: int
            Number of samples to retrieve
        offset: int
            Offset position to start from.
        dataset: int
            Datset to retrieve. If -1 will retrieve all datasets (default=-1).

        Returns
        ----------
        SamplesArray
            A samples array object with the requested samples.
        """
        if number == -1: number = self.N
        with h5py.File(self.filename) as f:
            if dataset == -1:
                dset1 = f[self.datasets[0]]
                if self.split:
                    dset2 = f[self.datasets[1]]
                if self.real:
                    data = dset1[offset:(offset+number), :]
                else:
                    if self.split:
                        data = (dset1[offset:(offset+number), :] +
                                dset2[offset:(offset+number), :]*1j)
                    else:
                        data = (dset1[offset:(offset+number), ::2] +
                                dset2[offset:(offset+number), 1::2]*1j)
                samples = SamplesArray(data, self.log_scale, self.real)
            else:
                dset1 = f[self.datasets[0]]
                if self.split:
                    dset2 = f[self.datasets[1]]
                if self.real:
                    data = dset1[offset:(offset+number), dataset]
                else:
                    if self.split:
                        data = (dset1[offset:(offset+number), dataset] +
                                dset2[offset:(offset+number), dataset]*1j)
                    else:
                        data = (dset1[offset:(offset+number), 2*dataset] +
                                dset2[offset:(offset+number), 2*dataset+1]*1j)
                samples = SamplesArray(data, self.log_scale, self.real)
            return samples

        return None


class SamplesCombined(Samples):
    """
    Class that represents a collection of other samples objects. This is
    useful when a set of samples spans multiple files. It is expected that
    all the samples objects have the same number of datasets and the same scale
    and type.
    """

    def __init__(self, sample_objs):
        """
        Constructor for SamplesCombined class

        Parameters
        ----------
        sample_objs: list
            List of samples objects to combine.
        """
        self.sample_objs = sample_objs
        self.num_objs = len(sample_objs)
        self.edges = np.zeros(self.num_objs + 1, np.int64)
        N = 0
        M = sample_objs[0].M
        for i in range(self.num_objs):
            N += sample_objs[i].N
            self.edges[i+1] = N
        super.__init__(N, M, sample_objs[0].log_scale, sample_objs[0].real)


    def get_samples(self, number=-1, offset=0, dataset=-1):
        """
        Function to retrieve samples in requested range.

        Parameters
        ------------
        number: int
            Number of samples to retrieve
        offset: int
            Offset position to start from.
        dataset: int
            Dataset to return, if -1 return all datasets (default=-1).

        Returns
        ----------
        SamplesArray
            A samples array object with the requested samples.
        """
        if number == -1: number = self.N
        data = np.zeros((number, (self.M if dataset == -1 else 1)), dtype=(np.float64 if self.real else np.complex128))
        offset_local = 0
        i = 0
        while i < self.num_objs and number > 0:
            if offset >= self.edges[i] and offset < self.edges[i+1]:
                start = offset - self.edges[i]
                if (offset + number) >= self.edges[i+1]:
                    number_to_read = self.edges[i+1] - self.edges[i]
                else:
                    number_to_read = self.edges[i+1] - offset
                data[offset_local:(offset_local+number_to_read),:] = self.sample_objs[i].get_samples_array(number_to_read, start, dataset)
                offset_local += number_to_read
                offset += number_to_read
                number -= number_to_read
            i += 1

        return SamplesArray(data, self.log_scale, self.real)


class MonteCarloOverlapRawSamples(object):
    """
    Class to calculate MonteCarlo overlaps on groups of raw samples.
    """

    def __init__(self, samples1, samples2, weights, bins=1):
        """
        Constructor to setup the overlap calculation.

        Parameters
        ----------
        samples1: Samples object
            An object representing a number of samples.
        samples2: Samples object
            An object representing a number of samples.
        weights: samples object
            Weights with which samples were generated.
        bins: int
            The number of bins to use (default=1).
        """
        if not (samples1.N == samples2.N == weights.N):
            print('For overlap, there must be the same number of samples in each set.')
            hammer.exit(-1)
        self.N = samples1.N
        self.M1 = samples1.M
        self.M2 = samples2.M
        self.samples1 = samples1
        self.samples2 = samples2
        self.weights = weights
        self.bins = bins
        self.samples_per_bin = int(self.N/bins)
        self.intermediate_data = False


    def calculate_intermediate_data(self):
        """
        Calculate the intermediate overlap data for the provided samples.
        """
        mus = np.zeros((self.bins, self.M1), np.float64)
        nus = np.zeros((self.bins, self.M2), np.float64)
        deltas = np.zeros((self.bins, self.M1, self.M2), np.complex128)
        samples1_max_log = np.zeros((self.bins, self.M1), np.float64)
        samples2_max_log = np.zeros((self.bins, self.M2), np.float64)
        weights_max_log = np.zeros((self.bins,1), np.float64)
        numbers = np.ones((self.bins,1), np.int)*self.samples_per_bin

        for bin in range(self.bins):
            bin_start = bin*self.samples_per_bin
            samples1_bin, samples1_max_log[bin,:] = self.samples1.get_samples_array(self.samples_per_bin, bin_start, log_scale=False, norm=True)
            samples2_bin, samples2_max_log[bin,:] = self.samples2.get_samples_array(self.samples_per_bin, bin_start, log_scale=False, norm=True)
            weights_bin, weights_max_log[bin,:] = self.weights.get_samples_array(self.samples_per_bin, bin_start, log_scale=False, norm=True)
            mus[bin,:] = np.real(np.sum(samples1_bin.conj() * samples1_bin/weights_bin, axis=0))
            nus[bin,:] = np.real(np.sum(samples2_bin.conj() * samples2_bin/weights_bin, axis=0))
            deltas[bin,:,:] = np.tensordot(samples1_bin.conj(), samples2_bin/weights_bin, axes=[[0], [0]])

        self.CompressedSamples = MonteCarloOverlapCompressedSamples(mus, nus, deltas, samples1_max_log, samples2_max_log, weights_max_log, numbers)
        self.CompressedSamples.normalise()
        self.intermediate_data = True


    def calculate_overlaps(self):
        """
        Calculate the overlaps and error estimates.
        """
        if not self.intermediate_data:
            self.calculate_intermediate_data()

        return self.CompressedSamples.calculate_overlaps()


    def get_intermediate_data(self):
        """
        Return a dictionary with the intermediate data from the overlap calcualtion.
        """
        if not self.intermediate_data:
            self.calculate_intermediate_data()
        return self.CompressedSamples.get_intermediate_data()


class MonteCarloOverlapCompressedSamples(object):
    """
    Class to calculate MonteCarlo overlaps on groups of compressed samples.
    """

    def __init__(self, mus, nus, deltas, samples1_max_log, samples2_max_log, weights_max_log, numbers):
        """
        Constructor to setup the overlap calculation.

        Parameters
        ----------
        mus: matrix
            Matrix representing normalised overlap of first wave-function with itself for each bin.
        nus: matrix
            Matrix representing normalised overlap of second wave-function with itself for each bin.
        deltas: tensor
            Tensor representing overlap between wave-functions for each bin.
        samples1_max_log: matrix
            The log of the norms used for first wave-function.
        samples2_max_log: matrix
            The log of the norms used for second wave-function.
        weights_max_log: vector
            The log of the norm used to normalise the weights.
        numbers: array
            The nubmer of samples in each bin.
        """
        if not (nus.shape[0] == mus.shape[0] == deltas.shape[0]):
            print('Different numbers of bins used for each.')
            hammer.exit(-1)
        self.mus = mus
        self.nus = nus
        self.deltas = deltas
        self.samples1_max_log = samples1_max_log
        self.samples2_max_log = samples2_max_log
        self.weights_max_log = weights_max_log
        self.bins = nus.shape[0]
        self.M1 = mus.shape[1]
        self.M2 = nus.shape[1]
        self.numbers = numbers


    def normalise(self):
        """
        Rescale each of the intermediate quantites so that they are all scaled
        by the global maximums.
        """
        # find the maximum value out of all the bins
        global_samples1_max_log = np.reshape(np.max(self.samples1_max_log, axis=0), (1, self.M1))
        global_samples2_max_log = np.reshape(np.max(self.samples2_max_log, axis=0), (1, self.M2))
        global_weights_max_log = np.max(self.weights_max_log, axis=0)

        self.mus *= np.exp(2*(self.samples1_max_log - global_samples1_max_log))/np.exp(self.weights_max_log - global_weights_max_log)
        self.nus *= np.exp(2*(self.samples2_max_log - global_samples2_max_log))/np.exp(self.weights_max_log - global_weights_max_log)
        for m1 in range(self.M1):
            for m2 in range(self.M2):
                self.deltas[:, m1, m2] *= (np.exp(self.samples1_max_log[:,m1] - global_samples1_max_log[0,m1] +
                                                  self.samples2_max_log[:,m2] - global_samples2_max_log[0,m2])
                                           /np.exp(self.weights_max_log[:,0] - global_weights_max_log))

        # update the max values to reflect the new normalisation
        for m1 in range(self.M1): self.samples1_max_log[:, m1] = global_samples1_max_log[0, m1]
        for m2 in range(self.M2): self.samples2_max_log[:, m2] = global_samples2_max_log[0, m2]
        self.weights_max_log[:] = global_weights_max_log


    def calculate_overlaps(self):
        """
        Calculate the overlaps and error estimates.
        """
        # assume all the bins have the same number of samples, check this
        if np.sum(np.abs(self.numbers - self.numbers[0])) > 0:
            print('Error: bins contain different numbers of samples.')
            hammer.exit(-1)
        overlaps = np.zeros((self.M1, self.M2), np.complex128) # will contain full overlap of each state with each other state
        bin_overlaps = np.zeros((self.bins, self.M1, self.M2), np.complex128) # will contain full overlap of each state with each other state
        overlap_errors = np.zeros((self.M1, self.M2))

        # find the overall overlap for each pair of states
        for m1 in range(self.M1):
            for m2 in range(self.M2):
                overlaps[m1,m2] = np.sum(self.deltas[:,m1,m2])/np.sqrt(np.sum(self.mus[:,m1])*np.sum(self.nus[:,m2]))
                bin_overlaps[:,m1,m2] = self.deltas[:,m1,m2]/np.sqrt(self.mus[:,m1]*self.nus[:,m2])
        overlap_errors = np.std(bin_overlaps, axis=0, ddof=0)/np.sqrt(self.bins)

        return overlaps, overlap_errors, bin_overlaps


    def get_intermediate_data(self):
        """
        Return a dictionary with the intermediate data from the overlap calcualtion.
        """
        Data = {}
        Data['mus'] = self.mus
        Data['nus'] = self.nus
        Data['deltas'] = self.deltas
        Data['samples1_max_log'] = self.samples1_max_log
        Data['samples2_max_log'] = self.samples2_max_log
        Data['weights_max_log'] = self.weights_max_log
        Data['numbers'] = self.numbers

        return Data


class MonteCarloOverlapCompressedSamplesFromFile(MonteCarloOverlapCompressedSamples):
    """
    Class to calculate MonteCarlo overlaps on groups of compressed samples, read from (a) file(s).
    """

    def __init__(self, filenames):
        """
        Constructor to setup the overlap calculation from file(s) containing compressed samples.

        Parameters
        ----------
        filenames: list
            List of filenames to read compressed samples from.
        """
        if len(filenames) == 0:
            print('Must provide at least one file.')
            hammer.exit(-1)

        f = h5py.File(filenames[0], 'r')
        mus = f['mus'][:]
        nus = f['nus'][:]
        deltas = f['deltas'][:]
        samples1_max_log = f['samples1_max_log'][:]
        samples2_max_log = f['samples2_max_log'][:]
        weights_max_log = f['weights_max_log'][:]
        numbers = f['numbers'][:]
        f.close()

        for idx in range(1, len(filenames)):
            f = h5py.File(filenames[idx], 'r')
            mus = np.vstack([mus, f['mus']])
            nus = np.vstack([nus, f['nus']])
            deltas = np.vstack([deltas, f['deltas']])
            samples1_max_log = np.vstack([samples1_max_log, f['samples1_max_log']])
            samples2_max_log = np.vstack([samples2_max_log, f['samples2_max_log']])
            weights_max_log = np.vstack([weights_max_log, f['weights_max_log']])
            numbers = np.vstack([numbers, f['numbers']])


        super(self.__class__, self).__init__(mus, nus, deltas, samples1_max_log, samples2_max_log, weights_max_log, numbers)
