#! /usr/bin/env python

""" FQHTorusJKWF.py: Script to calculate the Jain-Kamilla projected wave function. """

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"

import sys
import argparse
import numpy as np
import h5py
import petsc4py.PETSc as PETSc
import csv
import time
import itertools as iter
from hammer.FQHTorusJKWF import FQHTorusJKWF
import hammer.FQHTorusJKWF

try:
    HaveJacobiThetaModule = True
    from jacobitheta import *
except:
    HaveJacobiThetaModule = False


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    full_start = time.time()
    parser = argparse.ArgumentParser(description='Utility to calculate Jain Kamilla Overlap from.')

    parser.add_argument('-Ne', type=int, help='Total numer of fermions.')
    parser.add_argument("-FLL","--filled-landau-levels", type=int, help="Number of filled LL:s")
    parser.add_argument("-FP","--flux-pairs", type=int, help="Number of captured flux pairs",default=1)
    parser.add_argument("-Ns","--total-fluxes", type=int, help="Number of total fluxes in system")
    parser.add_argument("-Nr","--reduced-fluxes", type=int, help="Number of reduced fluxes after flux capturing")
    parser.add_argument("-D","--steps", type=int, help="Number of steps to take in x-direction",default=1)    
    parser.add_argument("--coord", type=str, help="Input file for x,y coordinates")
    parser.add_argument("--wfn", type=str, help="Output file for the JK-prjected wfn",default="JKWfn.hdf5")
    parser.add_argument("-K1", type=int, help="Project onto momentum sector K1")
    parser.add_argument("--tau1", type=float, help="Real part of tau",default=0.0)
    parser.add_argument("--tau2", type=float, help="Imaginary part of tau",default=1.0)
    parser.add_argument("--dry-run", action="store_true", help="Do not generate wave-functions, only perform the sanity checks on the input")
    parser.add_argument("--LL-force", action="store_true", help="Force the higher Lambda level wave functions to be in the lowest LL. Should give zero under antisymetriaztion.")
    parser.add_argument("--No-determinant", action="store_true", help="Only construct a product of CF wave functions and not a determiant. Mostly usefull for testing purposes.")
    
    Args = parser.parse_args(sys.argv[1:])

    ###Set tau
    tau = Args.tau1 + 1j*Args.tau2 
    
    print ("Args:",Args)
    ###Setting variables
    Ne = Args.Ne
    Ns = Args.total_fluxes
    Nr = Args.reduced_fluxes
    FLL = Args.filled_landau_levels
    FP = Args.flux_pairs

    DoDet=(not Args.No_determinant)
    ####We begin with a few sanity checks
    #### At least two variables need to be specifies
    VariablesSet=np.sum((Ne!=None,Ns!=None,Nr!=None,FLL!=None,FP!=None))
    if VariablesSet <= 2:
        print "ERROR: To few variables specified to tell how the JK-projection was done."
        print "       Need at least three out our the flags -Ne,-Ns,-Nr,-Fll,-FP!"
        raise Exception

    ##Assume enough parameters are set
    ##There are several solutions her to play with:
    ###  Ne,Ns
    if Nr !=None and FLL!=None :
        NeEff = Nr * FLL
        NsEff = Nr + 2*FP*NeEff
        Ne=test_set_same(NeEff,Ne,"Ne")
        Ns=test_set_same(NsEff,Ns,"Ns")
    elif Nr!=None and Ne !=None:
        NsEff = Nr + 2*FP*Ne
        Ns=test_set_same(NsEff,Ns,"Ns")
        FLLEff=test_divisible(Ne,Nr,"Ne","Nr")
        FLL=test_set_same(FLLEff,FLL,"FLL")
    elif Ne!=None and FLL!=None:
        NrEff=test_divisible(Ne,FLL,"Ne","FLL")
        Nr=test_set_same(NrEff,Nr,"Nr")
        NsEff = Nr + 2*FP*Ne
        Ns=test_set_same(NsEff,Ns,"Ns")
    elif Ne!=None and Ns!=None:
        NrEff = Ns - 2*FP*Ne
        Nr=test_set_same(NrEff,Nr,"Nr")
        FLLEff=test_divisible(Ne,Nr,"Ne","Nr")
        FLL=test_set_same(FLLEff,FLL,"FLL")
    else: ####Could not find a fit!
        print("INTERNAL ERROR: Could not reconstruct the JK-structure")
        raise Exception
    ###Check that no variables are set unknown
    VariablesSet=np.sum((Ne!=None,Ns!=None,Nr!=None,FLL!=None,FP!=None))
    if VariablesSet != 5:
        print "INTERNAL ERROR: All variables have not been set yet!"
        raise Exception


    ##Compute the fillnig fraction
    ##We knwo that nu = FLL / (2*FP*FLL +1)
    ### Which means that
    nominator = FLL
    denomin = 2*FP*FLL+1
    print("The filling fraction is nu=",nominator,"/",denomin)
    
    
    if not Args.dry_run: ###Now we compute the wave function
        ### CHaeck that input coordiantes are set
        if Args.coord == None:
            print("ERROR: No input coordinates where given")
            raise Exception

        ### Load the coordinate date
        print('Read coordinates from file:',Args.coord)
        with h5py.File(Args.coord, 'r') as coord:
            xcoord=np.array(coord.get('xvalues'))
            ycoord=np.array(coord.get('yvalues'))
        ###These coordiantes are all as matrices, so XY-list so be as well
        XYList=xcoord+1j*ycoord
        (PosConfigs,NeCord)=XYList.shape

        ###Initalize the wave function
        [Momenta,LLList]=hammer.FQHTorusJKWF.get_filled_LL_occupation(Nr,FLL-1)
        WFN=FQHTorusJKWF(Nr,Momenta,LLList,tau,
                         fluxpairs=FP,LL_force=Args.LL_force,Steps=Args.steps)

        t0=time.clock()
        if Args.K1==None: ###Construct wfn as is
            JKWFN=np.array(WFN.WFValue(XYList,det=DoDet))
        elif Args.K1==0: ###Project on to K1=0 sector
            ###This is only guaranteed to work if FLL is even, since then we can be sure a K0 sector is present.
            ### WE project on the K1=0 sector by adding poers of T1 acting on psi_JK with equal weight
            #### FIXME probably write a test for this somehow,
            JKWFN=np.zeros((PosConfigs),dtype=np.complex128)
            for T1indx in range(denomin):
                print("Adding phase factor:",T1indx," of ",denomin)
                shift=(T1indx*1.0)/Ns
                JKWFN+=np.array(WFN.WFValue(XYList+shift,det=DoDet))
        else:
            print("ERROR: Projection onto other than K=0 not implemented")
        print('Took:',time.clock()-t0,' second of CPU time')
            
        ###Save the coordinates to file
        try:
            of = h5py.File(Args.wfn, 'w')
        except IOError as ioerror:
            print(("ERROR: Problem writing to file " + Args.wfn  + ': ' + str(ioerror)))
            sys.exit(-1)
        #Separate The real an imaginary parts of the file
        JKSave=np.zeros((PosConfigs,2),dtype=np.float64)
        JKSave[:,0]=np.real(JKWFN)
        JKSave[:,1]=np.imag(JKWFN)
        of.create_dataset("wf_values", data=JKSave)
        of.close()

        
def test_divisible(A,B,NameA,NameB):
    if A % B != 0:
        print "ERROR: The value of",NameB,"=",B," does not divide the value of",NameA,"=",A,"As is should!"
        raise Exception
    return A/B
    
        
    
def test_set_same(ValEff,Val,Name):
    if Val==None: ##Set Val to ValEff
        Val = ValEff
    elif Val!=ValEff:
        print(("ERROR: The computed value of",Name,"=",ValEff," is note the same as the input value =",Val))
        raise Exception
    return Val
        
if __name__ == '__main__' :
    __main__()
