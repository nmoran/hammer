#! /usr/bin/env python

""" ShowBasis.py: Utility to access the basis building functionality of Hammer. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys, slepc4py
import string
slepc4py.init(sys.argv)

from petsc4py import PETSc
from slepc4py import SLEPc

# Custom modules
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion
from hammer.FQHTorusBasisBosonicCOMMomentum import FQHTorusBasisBosonicCOMMomentum
from hammer.FQHTorusBasisBosonicCOMMomentumInversion import FQHTorusBasisBosonicCOMMomentumInversion
from hammer.FQHSphereBasis import FQHSphereBasis
from hammer.HammerArgs import HammerArgsParser
import hammer.vectorutils as vecutils
import hammer.matrixutils as matutils

# System modules
import sys
import argparse
import numpy as np
import time
import csv


def __main__(argv):
    # Run the argument parser and process command line arguments.
    ShowBasisOpts = ShowBasisArgsParser()
    ShowBasisOpts.parseArgs(argv)
    ShowBasis(ShowBasisOpts)


def ShowBasis(Opts):
    PetscPrint = PETSc.Sys.Print
    # Construction of basis.
    if not Opts.Bosonic:
        start = time.time()
        if Opts.COMMomentum == -1:
            MyBasis = FQHTorusBasis(Opts.Particles, Opts.NbrFlux, Opts.Momentum)
        else:
            if Opts.InversionSector == -1:
                MyBasis = FQHTorusBasisCOMMomentum(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum)
            else:
                MyBasis = FQHTorusBasisCOMMomentumInversion(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum, Opts.InversionSector)
        MyBasis.GenerateBasis(DimensionOnly=Opts.DimensionOnly)
        end = time.time()
    else:
        start = time.time();
        if Opts.COMMomentum == -1:
            MyBasis = FQHTorusBasisBosonic(Opts.Particles, Opts.NbrFlux, Opts.Momentum)
        else:
            if Opts.InversionSector == -1:
                MyBasis = FQHTorusBasisBosonicCOMMomentum(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum)
            else:
                MyBasis = FQHTorusBasisBosonicCOMMomentumInversion(Opts.Particles, Opts.NbrFlux, Opts.Momentum, Opts.COMMomentum, Opts.InversionSector)
        MyBasis.GenerateBasis(DimensionOnly=Opts.DimensionOnly)
        end = time.time()
    if Opts.DimensionOnly:
        PetscPrint(str(MyBasis.GetDimension()))
    else:
        PetscPrint('Basis dimension: ' + str(MyBasis.GetDimension()) + ', built in ' + str(end-start) + ' seconds.')

    dim = MyBasis.GetDimension()
    if not Opts.DimensionOnly and Opts.Show:
        if not Opts.Representative:
            for i in range(0,dim):
                PetscPrint(MyBasis.GetOccupationRep(i))
        else:
            count = 0
            for i in range(0,dim):
                if MyBasis.RepresentativeConfiguration(i) == i:
                    PetscPrint(MyBasis.GetOccupationRep(i))
                    count += 1
            PetscPrint("Number configurations: " + str(count))


class ShowBasisArgsParser:
    """
    Class to parse command line arguments specific to show basis utility.

    """

    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(
            parents=[HammerArgsParser().BasisParser()],
            description='Calculate and show information about basis.')
        self.Parser.add_argument('-d','--dimension-only', action='store_true', help='Only show the dimension and exit.')
        self.Parser.add_argument('-r','--representative', action='store_true', help='Don\'t count representatives.')
        self.Parser.add_argument('-s','--show', action='store_true', help='Show configurations.')



    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.Particles = ArgVals.particles
        self.NbrFlux = ArgVals.nbrflux
        self.Momentum = ArgVals.momentum
        self.COMMomentum = ArgVals.commomentum
        self.Bosonic = ArgVals.bosonic
        self.InversionSector = ArgVals.inversion_sector
        self.ExemptContOrbitals = ArgVals.exempt_cont_orbitals
        self.DimensionOnly = ArgVals.dimension_only
        self.Representative = ArgVals.representative
        self.Show = ArgVals.show

        ###Test that there are no unknown options
        if(len(self.Unknown)>1): ###First unknown unknown option is the filename
            print(("There are unknown options (the first is always present)"))
            print((self.Unknown))
            print(("Please try again. See the help for a list of valid options"))
            sys.exit(1)


if __name__ == '__main__':
    __main__(sys.argv)
