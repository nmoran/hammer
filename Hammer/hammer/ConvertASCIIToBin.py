#! /usr/bin/env python

""" ConvertASCIIToBin.py: Utility to convert state files in ASCII format to binary format. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

from petsc4py import PETSc
import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils

def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to calculate the inner product of vectors.')
    Parser.add_argument('state', metavar='vec', type=str, nargs=1)
    Parser.add_argument('--output', '-o', type=str, default='')
    Parser.add_argument('--verbose','-v', action='store_true')
    Parser.add_argument('--plain','-p', action='store_true')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose

    if Args.output == '' :
        OutputFilename = Args.state[0] + '.vec'
    else:
        OutputFilename = Args.output

    # Read the vector, assumed it's in PETSc ASCII format unless plain is specified
    if Args.plain == True:
        A = vectorutils.ReadPlainASCIIVector(Args.state[0])
    else:
        A = vectorutils.ReadASCIIPETScVector(Args.state[0])

    if Verbose:
        print(("Read vector from " + str(Args.state[0]) + " with " + str(A.getSize()) + " elements."))

    vectorutils.writeBinaryPETScFile(A, OutputFilename)


if __name__ == '__main__' :
    __main__()
