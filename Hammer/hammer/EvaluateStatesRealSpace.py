#! /usr/bin/env python

""" EvaluateStatesRealSpace.py: Script to calculate the wave-function values of given state(s) at the provided positions. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import sys
import argparse
import hammer.vectorutils as vecutils
from hammer.FQHBasisManager import FQHBasisManager
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion
from hammer.FQHTorusWF import *
import time
import h5py


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    PetscPrint = PETSc.Sys.Print
    Parser = argparse.ArgumentParser(description='Utility to calculate wave-function values at the given points.')
    Parser.add_argument('states', metavar='vec', type=str, nargs='*')
    Parser.add_argument('--states-file', '-sf', type=str, default='', help='File containing list of states one per line.')
    Parser.add_argument('--verbose','-v', action='store_true', help='Flag to indicate that script should run in verbose mode.')
    Parser.add_argument('--x-positions', '-x', default='square_xx.rnd', help='Path to file containing x coordinates to use to evaluate the wave-function(s) are stored.')
    Parser.add_argument('--y-positions', '-y', default='square_yy.rnd', help='Path to file containing y coordinates to use to evaluate the wave-function(s) are stored.')
    Parser.add_argument('--output', '-o', default='hammer_', help=('Path and prefix for output files that will contain the evaluted wave-function(s) values. For plain text output, two files are created for each ' +
            'state, with the state index appended along with "re.rnd" for the real parts and "im.rnd" for the imaginary parts. If hdf5-output is enabled the .hdf5 suffix is added and an hdf5 file with a "samples" ' +
            'dataset is created. This dataset has a row for each position set and two columns for each state, corresponding to the real and imaginary parts.'))
    Parser.add_argument('--batch-size', '-b', type=int, default=1000, help='The number of positions to group into each batch.')
    Parser.add_argument('--hdf5-output', action='store_true', help='Flag to inidicate that the HDF5 file format should be used for output.')
    Parser.add_argument('--hdf5-nometa', action='store_true', help='Flag to inidicate that meta data should not be included in output files.')
    Parser.add_argument('--hdf5-positions', default='', help='Filename of positions file in HDF format. By default this is empty and text input is read from specified files. If this argument is used, an hdf5 format file ' +
            'is expected which contains a dataset named "positions" containing a row for each positions set and two columns for each particle, corresponding to the position of the particle on the square unit cell.')
    Parser.add_argument('--rotate-states', action='store_true', help='Flag to indicate that the states should be rotated such that the largest coefficient is real. Should make results more reproducable, since there'
                        + ' is a guage freedom on states.')
    Parser.add_argument('--slaters', action='store_true', help='Flag to indicate that all slater determinants should be evaluated and fock coefficients ignored. At least one state must still be given so '
                        + 'that parameters can be deduced from the filename.')
    Parser.add_argument('--landau-level', '-n', type=int, default=0, help='The Landau level index to be used for wave-functions (default=0).')
    Parser.add_argument('--sherman-morrison', '-sm', action='store_true', help='Flag to indicate that the Sherman Morrison algorithm should be used to optimise evaluate of Slater determinants.')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    if Args.states_file != '':
        StateNames = []
        try:
            with open(Args.states_file, 'r') as f:
                line = f.readline()
                while line:
                    StateNames.append(line.strip())
                    line = f.readline()
        except IOError as ioerror:
            PetscPrint('Problem reading ' + Args.states_file + ', ' + str(ioerror))
    else:
        StateNames = Args.states
        if len(StateNames) == 0:
            PetscPrint('At least one state or states file must be specified.')
            sys.exit(1)
    NbrStates = len(StateNames)
    XPositions = Args.x_positions
    YPositions = Args.y_positions
    OutputPrefix = Args.output
    BatchSize = Args.batch_size
    HDF5Output = Args.hdf5_output
    HDF5NoMeta = Args.hdf5_nometa
    HDFPositions = Args.hdf5_positions
    RotateStates = Args.rotate_states
    Slaters = Args.slaters
    ShermanMorrison = Args.sherman_morrison
    if ShermanMorrison:
        PetscPrint ("Using Sherman Morrison method for calculating determinants (Note this is slower as currently is unoptimised).")
    LL = Args.landau_level
    if NbrStates <= 0:
        PetscPrint ("Must enter at least one state.")
        return

    NbrProcesses = PETSc.Comm.Get_size(PETSc.COMM_WORLD)
    ProcessRank = PETSc.Comm.Get_rank(PETSc.COMM_WORLD)

    PetscPrint ("Calculating wave-function values.")
    PetscPrint ("Processes: " + str(NbrProcesses))
    PetscPrint ("Building basis.")

    BasisManager = FQHBasisManager()
    BasisManager.SetTorusDetailsFromFilename(StateNames[0])
    Basis = BasisManager.GetBasis(Verbose=Verbose)

    if NbrProcesses > 1 and Basis.GetDimension() < (NbrProcesses * 10):
        PetscPrint('Warning not very efficient to use ' + str(NbrProcesses) + ' processes for this basis size, consider using less processes.')

    Particles = BasisManager.Particles

    FullBasis = None
    # Read in state(s) and expand if necessary
    States = list()
    for i in range(NbrStates):
        if Verbose: PetscPrint('Reading state <' + StateNames[i] + '>.')
        State = vecutils.ReadBinaryPETScVector(StateNames[i])
        if Verbose: PetscPrint('Read state ' + StateNames[i] + ' with dimension ' + str(State.getSize()) + '.')
        if BasisManager.COMMomentum >= 0 or BasisManager.InversionSector >= 0:
            [FullBasis, FullState] = Basis.ConvertToFullBasis(State, FullBasis)
        else:
            FullBasis = Basis
            FullState = State

        States.append(FullState)
    Basis = FullBasis
    PetscPrint("Full basis dimension: " + str(Basis.GetDimension()))

    Positions = None
    if HDFPositions != '' :
        try:
            f = h5py.File(HDFPositions, 'r')
            Positions = dict()
            Positions['xvalues'] = f['xvalues']
            Positions['yvalues'] = f['yvalues']
            if Positions['xvalues'].shape[1] != Particles or Positions['yvalues'].shape[1] != Particles:
                PetscPrint("Positions dataset must have " + str(2*Particles) + " columns.")
                return
            NbrPositions = Positions['xvalues'].shape[0]
            if Positions['xvalues'].shape[0] != Positions['yvalues'].shape[0]:
                PetscPrint("Positions dataset must have the same number of x and y positions.")
                return
        except IOError as ioerror:
            PetscPrint("Error opening hdf file " + HDFPositions + ".")
            PetscPrint("Error: " + str(ioerror) )
            sys.exit(1)
            return
        except KeyError:
            PetscPrint("Error openning 'positions' dataset in " + HDFPositions + ". Make sure this file has " +
                    "such a dataset.")
            sys.exit(1)
            return
    else:
        Positions = ReadCoordinatesColumnFormat(XPositions , YPositions)
        NbrPositions = len(Positions)
    if Positions is None or NbrPositions == 0:
        PetscPrint('No positions read. Exiting!')
        return
    PetscPrint ('Read in ' + str(NbrPositions) + ' sets of coordinates.')

    if Slaters:
        NbrStates = Basis.GetDimension()

    # In the case where the batch size is larger than the number of positions, reset the batch size.
    if NbrPositions < BatchSize:
        BatchSize = NbrPositions

    if ProcessRank == 0 and HDF5Output:
        f = h5py.File(OutputPrefix + '.hdf5', 'w')
        # create datasets
        dset = f.create_dataset('wf_values', (NbrPositions, 2*NbrStates), dtype=np.float64,
                                chunks=((2000000 if NbrPositions >= 2000000 else NbrPositions), 1))
        if not Slaters and not HDF5NoMeta:
            filename_dset = f.create_dataset('vector_filenames', (1, NbrStates), dtype="S1000") # create data set of 200 character strings to store the vector filenames
            for i in range(NbrStates):
                filename_dset[0,i] = np.string_(StateNames[i])
        dset.attrs['NbrStates'] = NbrStates
        dset.attrs['NbrSamples'] = NbrPositions
        f.close()

    WF = BasisManager.GetSingleParticleWF()
    PetscPrint('Evaluating ' + str(NbrStates) + ' wave-functions.' )
    start = time.time()
    for BatchIdx in range(int(NbrPositions/BatchSize) + 1):
        BatchStart = BatchIdx * BatchSize
        BatchEnd = ((BatchIdx + 1) * BatchSize if ((BatchIdx + 1) * BatchSize) < NbrPositions else NbrPositions)
        if (BatchEnd - BatchStart) > 0:
            if HDFPositions != '' :
                BatchPositions = Positions['xvalues'][BatchStart:BatchEnd, :] + Positions['yvalues'][BatchStart:BatchEnd, :]*np.complex(0.0,1.0)
            else:
                BatchPositions = np.vstack(Positions[BatchStart:BatchEnd])
            if not Slaters:
                if ShermanMorrison:
                    Values = Basis.EvaluateWaveFunctionShermanMorisson(BatchPositions, States, WF, Verbose=Verbose)
                else:
                    Values = Basis.EvaluateWaveFunction(BatchPositions, States, WF, Verbose=Verbose)
            else:
                Values = Basis.EvaluateSlaterDeterminants(BatchPositions, WF, Verbose=Verbose)

            split = time.time()
            TimePerPositionSet = BatchEnd / (split-start)
            RemainingTime = (NbrPositions - BatchEnd) / TimePerPositionSet
            StartWrite = time.time()
            if ProcessRank == 0:
                if HDF5Output:
                    f = h5py.File(OutputPrefix + '.hdf5', 'r+')
                    dset = f['wf_values']
                    for StateIdx in range(NbrStates):
                        dset[BatchStart:BatchEnd, StateIdx*2] = np.real(Values[:, StateIdx]);
                        dset[BatchStart:BatchEnd, StateIdx*2+1] = np.imag(Values[:, StateIdx]);
                    f.close()
                else:
                    for StateIdx in range(NbrStates):
                        WriteWaveFunctionValuesColumnFormat(Values[:, StateIdx], OutputPrefix + '_' + str(StateIdx), Append=(True if BatchIdx > 0 else False))
            EndWrite = time.time()
            PetscPrint(str(BatchEnd) + '/' + str(NbrPositions) + ' in ' + '{:.2f}'.format(split-start) + ' s ' + '(writing  took ' + str(EndWrite - StartWrite) + 's), estimated time remaining: ' + '{:.2f}'.format(RemainingTime) + 's.')

    end = time.time()
    PetscPrint('Evaluated wave-functions in ' + '{:.2f}'.format(end-start) + ' seconds.')


if __name__ == '__main__' :
    __main__()
