#! /usr/bin/env python

""" ConvertBinToASCII.py: Utility to convert state files in binary format to ASCII format. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

from petsc4py import PETSc
import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils
from hammer.FQHBasisManager import FQHBasisManager
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to calculate the inner product of vectors.')
    Parser.add_argument('state', metavar='vec', type=str, nargs=1)
    Parser.add_argument('--output', '-o', type=str, default='')
    Parser.add_argument('--verbose','-v', action='store_true')
    Parser.add_argument('--monomial', '-m', action='store_true')
    Parser.add_argument('--significant-digits', type=int, default=12)
    Parser.add_argument('--sort', '-s', action='store_true')
    Parser.add_argument('--rotate', '-r', action='store_true', help='Rotate such that largest magnitude element is real.')
    Parser.add_argument('--output-fock-coefficients', action='store_true')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    Precision = Args.significant_digits
    fstr = '{:.' + str(Precision) + 'f}'
    Monomial = Args.monomial
    Sort = Args.sort
    OutputFockCoefficients = Args.output_fock_coefficients
    Rotate = Args.rotate

    if Args.output == '' :
        if OutputFockCoefficients:
            OutputFilename = Args.state[0] + '_fock_coeffs.dat'
        else:
            OutputFilename = Args.state[0] + '.csv'
    else:
        OutputFilename = Args.output

    # Read the vector, assumed it's in binary format
    A = vectorutils.ReadBinaryPETScVector(Args.state[0])
    if Rotate: vectorutils.RotateVector(A)

    # if we wish to provice basis monomials aswell, we work out basis details from the filename and generate it.
    if Monomial:
        BasisManager = FQHBasisManager()
        BasisManager.SetTorusDetailsFromFilename(Args.state[0])
        Basis = BasisManager.GetBasis(Verbose=Verbose)

    if OutputFockCoefficients:
        vectorutils.WriteFockCoefficients(A, OutputFilename)

    # Write to file
    f = open(OutputFilename, "w")
    Elements = A.getArray()

    if Sort:
        Aabs = A.copy()
        Aabs.abs()
        AbsElements = Aabs.getArray()
        SortIndices = np.argsort(AbsElements)


    if not Monomial:
        if not Sort:
            for Element in Elements:
                f.write(fstr.format(Element.real) + ',' + fstr.format(Element.imag) + '\n')
        else:
            for idx in SortIndices[::-1]:
    	        f.write(fstr.format(Elements[idx].real) + ',' + fstr.format(Elements[idx].imag) + '\n')
    else:
        if not Sort:
            for Idx in range(0, Elements.__len__()):
                OccupationRep = Basis.GetOccupationRep(Idx)
                f.write(fstr.format(Elements[Idx].real) + ',' + fstr.format(Elements[Idx].imag) + ',' + OccupationRep + '\n')
        else:
            for idx in SortIndices[::-1]:
                OccupationRep = Basis.GetOccupationRep(idx)
                f.write(fstr.format(Elements[idx].real) + ',' + fstr.format(Elements[idx].imag) + ',' + OccupationRep + '\n')
    f.close()


if __name__ == '__main__' :
    __main__()
