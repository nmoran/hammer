#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=True
# cython: cdivision=True
"""
This is a cython implementation of code to calculate product wave-functions. 

"""
import numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
from petsc4py import PETSc

cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)


def OccupationNorm(Occupation):
    """
    Function to calculate the normal of a given configuration. Given by the product of square roots of the factorials of the occupation counts.
    """
    cdef double Norm = 1.0
    for count in Occupation:
        j = 1
        while j <= count:
            Norm *= j
            j += 1
    return sqrt(Norm)


def BosonicProductWF(States, Bases, FinalBasis, Normalise=True):
    """
    Function to calculate the wave-function products for bosonic wave-functions.
    """

    # Create an empty output vector
    D = PETSc.Vec()
    D.create()
    D.setSizes(FinalBasis.GetDimension())
    D.setUp()
    D.zeroEntries()

    cdef int i, j, k
    cdef int dim1, dim2, dim3, idx
    cdef double complex Coeff1, Coeff2, Coeff3, Coeff 
    cdef double Norm, Norm1, Norm2
    cdef long SummedElement

    dim1 = Bases[0].GetDimension()
    dim2 = Bases[1].GetDimension()

    if len(States) == 3:
        # Now populate the entries
        dim3 = Bases[2].GetDimension()
        i = 0
        while i < dim1:
            Norm1 = Bases[0].GetElementNorm(Bases[0].GetElement(i))
            Coeff1 = States[0].getValue(i)
            j = 0
            while j < dim2:
                Norm2 = Bases[1].GetElementNorm(Bases[1].GetElement(j))
                Coeff2 = States[1].getValue(j) * Coeff1
                k = 0
                while k < dim3:
                    SummedElement = Bases[0].GetElementSum(Bases[0].GetElementSum(Bases[0].GetElement(i), Bases[1].GetElement(j)), Bases[2].GetElement(k))
                    idx = FinalBasis.BinaryArraySearchDescendingElement(SummedElement)
                    if idx >= 0:
                        Norm3 = Bases[2].GetElementNorm(Bases[2].GetElement(k))
                        Coeff3 = States[2].getValue(k) * Coeff2
                        Norm = Bases[2].GetElementNorm(SummedElement)
                        Coeff = Coeff3 * Norm / (Norm1 * Norm2 * Norm3)
                        D.setValue(idx, Coeff, True)
                    k += 1
                j += 1
            i += 1
    elif len(States) == 2:
        # Now populate the entries
        i = 0
        while i < dim1:
            Norm1 = Bases[0].GetElementNorm(Bases[0].GetElement(i))
            Coeff1 = States[0].getValue(i)
            j = 0
            while j < dim2:
                SummedElement = Bases[0].GetElementSum(Bases[0].GetElement(i), Bases[1].GetElement(j))
                idx = FinalBasis.BinaryArraySearchDescendingElement(SummedElement)
                if idx >= 0:
                    Norm = FinalBasis.GetElementNorm(SummedElement)
                    Norm2 = Bases[1].GetElementNorm(Bases[1].GetElement(j))
                    Coeff2 = States[1].getValue(j)
                    Coeff = Coeff1 * Coeff2 * Norm / (Norm1 * Norm2)
                    D.setValue(idx, Coeff, True)
                j += 1
            i += 1

    norm = D.norm()
    if norm > 0.0 and Normalise:
        D.scale(1.0/norm)

    return D

