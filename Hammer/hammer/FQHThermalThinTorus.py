#! /usr/bin/env python

""" FQHThermalThinTorus.py: Script to evaluate the thin torus limit states at finite temperature. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import petsc4py.PETSc as PETSc
import numpy as np
import csv
import sys
import argparse
import time

# Custom modules
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients2Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients3Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients4Body
from hammer.FQHTorusHamiltonian import FQHTorusHamiltonian
from hammer.HammerArgs import HammerArgsParser
from hammer.Diagonalise import BuildBasis
import hammer.vectorutils as vecutils
import hammer.matrixutils as matutils
import hammer.FQHFileManager as FileName


def __main__(argv):
    # Run the argument parser and process command line arguments.
    TTTOpts = TTTArgsParser()
    TTTOpts.parseArgs(argv)
    Opts = HammerArgsParser()
    Opts.parseArgs(TTTOpts.Unknown)
    ThermalThinTorus(TTTOpts, Opts)

def ThermalThinTorus(TTTOpts, Opts):
    """
    Function to calculate values at finite temperature for the thin torus regime.
    """
    # This print is to be used when working with many processes as will ensure that statement is only printed once.
    PetscPrint = PETSc.Sys.Print
    MyRank = PETSc.COMM_WORLD.getRank()

    # Prefix to use for data files.
    if Opts.Prefix == '':
        Prefix = FileName.TorusStatePrefix(Opts.Bosonic,Opts.Particles,Opts.NbrFlux,
                                           Opts.Momentum,-1,-1,Opts.Tau.real,Opts.Tau.imag,
                                           Opts.MagneticLength,Opts.ID)
    else:
        Prefix = Opts.Prefix


    MyBasis = BuildBasis(Opts)
    PetscPrint('Basis dimension: ' + str(MyBasis.GetDimension()) + ', using precomputed basis.')

    # Calculation of interaction coefficients.
    PetscPrint('Calculating coefficients for ' + str(Opts.ID) + ' interaction.')
    if Opts.Interaction == '3Body':
        InteractionCoefficients = FQHTorusInteractionCoefficients3Body(Opts.Tau, Opts.NbrFlux, Interaction=Opts.Interaction, LL=Opts.LL,MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic);
    elif Opts.Interaction == '4Body':
        InteractionCoefficients = FQHTorusInteractionCoefficients4Body(Opts.Tau, Opts.NbrFlux, Interaction=Opts.Interaction, LL=Opts.LL,MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic);
    else:
        if Opts.PseudoPotentials is not None:
            PetscPrint('Pseudopotentials: ' + str(Opts.PseudoPotentials))
        InteractionCoefficients = FQHTorusInteractionCoefficients2Body(Opts.Tau, Opts.NbrFlux, Interaction=Opts.Interaction, LL=Opts.LL,MagneticLength=1.0, MPMath=Opts.MPMath, Bosonic=Opts.Bosonic, PseudoPotentials=Opts.PseudoPotentials);
    start = time.time(); InteractionCoefficients.CalculateCoefficients();end = time.time()
    PetscPrint('L1: ' + str(InteractionCoefficients.L1) + ', L2: ' + str(InteractionCoefficients.L2) + '.')
    PetscPrint('Coefficients calculated in ' + str(end-start) + ' seconds.')

    if Opts.WriteCoefficients and MyRank == 0: # Only do this if the fist process.
        InteractionCoefficients.WriteCoefficients('Coeff.csv')
        InteractionCoefficients.WriteAntiSymCoefficients('CoeffSym.csv')

    # Construct the Hamiltonian matrix
    TorusHamiltonian = FQHTorusHamiltonian(MyBasis, InteractionCoefficients, Opts.KeepReal)
#    start = time.time()
#    if Opts.Interaction == '4Body':
#        HamiltonianMatrix = TorusHamiltonian.CreateSlepc4BodyHamiltonian(Opts.PreallocatedMatrix)
#    elif Opts.Interaction == '3Body':
#        HamiltonianMatrix = TorusHamiltonian.CreateSlepc3BodyHamiltonian(Opts.PreallocatedMatrix)
#    else:
#        HamiltonianMatrix = TorusHamiltonian.CreateSlepcHamiltonian(Opts.PreallocatedMatrix)
#    end = time.time()
#    PetscPrint( 'Hamiltonian constructed in ' + str(end-start) + ' seconds.')

    EVals = TorusHamiltonian.CalculateDiagonal()

    # Now calculate the energy expectation value at each temperature step.
    Ts = np.arange(TTTOpts.TempMin, TTTOpts.TempMax, TTTOpts.TempStep)
    Es = np.zeros(len(Ts))
    ESqrds = np.zeros(len(Ts))
    k = TTTOpts.K
#    Diag = PETSc.Vec()
#    HamiltonianMatrix.getDiagonal(Diag)
#    EVals = Diag.getArray()
    MinE = EVals.min()
    EVals = EVals - MinE # subtracting minimum energy to make all energies positive.

    for idx in range(len(Ts)):
        T = Ts[idx]
        if T == 0:
            T = 1e-50
        Beta = 1.0/(k * T)
        Z = 0.0
        E = 0.0
        ESqrd = 0.0

        for Val in EVals:
            tmp = np.exp(-Beta * Val)
            Z += tmp
            E += Val * tmp
            ESqrd += Val * Val * tmp

        Es[idx] = E/Z + MinE # adding back in the offset to make positive
        ESqrds[idx] = ESqrd/Z + MinE**2 # adding back in the offset to make positive

    Filename = Prefix + '_Thermal.csv'
    f = open(Filename, 'w')
    csv_writer = csv.writer(f)
    for idx in range(len(Ts)):
        csv_writer.writerow([Ts[idx], Es[idx], ESqrds[idx]])
    f.close()




class TTTArgsParser:
    """
    Class to parse command line arguments specific to viscosity calculation utility.

    TODO: Change to use dict datatype intenally or even to be a subclass of dict to make more extensible.
    """


    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(description='Calculate quantities for the thermal thin torus .')
        self.Parser.add_argument('--temp-min', type=float, default=0.0, help='The minimum temperature to consider (default = 0).')
        self.Parser.add_argument('--temp-max', type=float, default=20.0, help='The minimum temperature to consider (default = 20).')
        self.Parser.add_argument('--temp-step', type=float, default=0.5, help='The steps in temperature to take (default = 0.5).')
        self.Parser.add_argument('-k', type=float, default=1.0, help='Boltzman constant (default = 0.5).')


    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.TempStep = ArgVals.temp_step
        self.TempMin = ArgVals.temp_min
        self.TempMax = ArgVals.temp_max
        self.K = ArgVals.k;


if __name__ == '__main__':
    __main__(sys.argv)
