#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True
# cython: profile=False
# cython: linetrace=False


""" FQHLaughlin.pyx: Cython code for calculating values of the laughlin state on varipus geometries

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2017"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"
"""

import time
from hammer.utils_timer import timer

import numpy as np
cimport numpy as np

cimport cython

import sys
try:
    HaveJacobiThetaModule = True
    from jacobitheta import *
except:
    HaveJacobiThetaModule = False
    raise Exception,"No Jacobi Theta Module installed. This module will be functional then"


cdef extern from "math.h":
    double exp(double x)
    double pow(double x, double y)
    double atan(double x)
    double fmod(double x,double y)


cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double cimag(double complex x) nogil
    double complex conj(double complex x) nogil
    double complex clog(double complex x) nogil




cdef class FQHTorusLaughlinWF:
    """
    This class represents Laughlin wave function for a torus wave-function which an eigenstate of the translation operator in the x direction (if no quasi-particles are present) and with a number of quasi-holes attached to it.
    Here we assume that the tau-gauge is used which in reduced coordinates z=L(x+tau*y) reads A = B(-y,0)
    """

    cdef bint LatticeSampled
    cdef int Ne,Ns,Denom,Nqh,EffNs
    cdef double complex Tau,IPiTauNs,QhSum,NormFactor,IPiTauNsbyQ
    cdef double pi,TwoPi,tfactor
    cdef double complex [:,:] JastrowTable ###
    cdef double complex [:,:] JastrowQhTable ###
    cdef double complex [:,:] CoMTable ###
    cdef double complex [:] GaussianTable ###
    cdef double complex [:] QhPos ###
    cdef long [:] xint,yint,oldxint,oldyint

    def __init__(self, int Ne, int Mom, QHPos=None, double complex Tau=1j,Denom=3,LatticeSample=False,EffectiveNs=None):
        """
        Constructor. Sets the number of flux, torus parameter tau, momentum  and positon of quasiholes.
        """
        ###Take down input
        self.Denom=Denom
        self.Ne=Ne
        self.Tau = Tau
        ###Identify quasiholes
        if type(QHPos)==type(None):
            self.Nqh=0
        else:###Assume it is a list of some sort
            self.Nqh = len(QHPos)
            self.QhPos=np.array(QHPos,dtype=np.complex128)###Make into array if a list
        if self.Nqh>0:
            self.QhSum=np.sum(QHPos.real)+self.Tau*np.sum(QHPos.imag)
        else:
            self.QhSum=0.0
        
        
        self.Ns = self.Ne*self.Denom+self.Nqh

        # calculate derived values
        self.TwoPi = 6.28318530717958647692528676656
        self.pi = np.pi
        self.IPiTauNs =1j*self.pi*self.Tau*self.Ns
        self.IPiTauNsbyQ =1j*self.pi*self.Tau*self.Ns/(self.Denom*1.0)
        ###Valid momenta are
        #Ne(Ne-1)/2 mod Ne#
        RawMom=np.mod((self.Ne*(self.Ne-1))/2,self.Ne)
        if np.mod(Mom-RawMom,self.Ne)!=0:
            ERRTXT="Invalid momentum: "+str(Mom)+" compared with reduced momentum="+str(RawMom)+" and Ne="+str(self.Ne)
            raise Exception, ERRTXT
        else:
            if self.Nqh==0:
                self.tfactor=(1.0*Mom)/self.Ns
            else:
                ###Fixme, this does not utilize the freedom of the momenta
                self.tfactor=np.mod((1.0*self.Ns-self.Denom)/2+(Mom-RawMom)/(1.0*self.Ne*self.Denom),1.0)
        print "t-factor:",self.tfactor

        ###Initiate memory to keep the reduced lattice coordinates
        self.xint=np.zeros((self.Ne),dtype=np.int)
        self.yint=np.zeros((self.Ne),dtype=np.int)
        self.oldxint=np.zeros((self.Ne),dtype=np.int)
        self.oldyint=np.zeros((self.Ne),dtype=np.int)

        #####Set the effective Ns on wich the lattice coordinates will be sampled
        if EffectiveNs==None:
            self.EffNs=self.Ns
        else:
            self.EffNs=EffectiveNs
        
        if LatticeSample:
            self.SampleLattice()

        self.NormFactor=self.compute_NormFactor()


       
    cdef double complex compute_NormFactor(self):
        cdef double complex logfactor,zdiff
        cdef int n1,n2
        if self.Nqh >= 1:
            #### Add the gaussian pieces
            logfactor=self.IPiTauNsbyQ*np.sum(np.imag(self.QhPos)**2)
            #### Add the quasiholes part
            n1=0
            while n1 < (self.Nqh -1):
                n2=n1+1
                while n2 < self.Nqh:
                    #print "n1,n2:",n1,n2
                    zdiff=self.QhPos[n1]-self.QhPos[n2]
                    zdiff=zdiff.real + self.Tau*zdiff.imag
                    #print "zdiff:",zdiff
                    logfactor+=jacobitheta.logtheta1(zdiff, self.Tau)*(1.0/self.Denom)
                    #print "logwfn2:",logwfn
                    n2+=1
                n1+=1
        else:
            logfactor=0j
        return logfactor
        
        
    def WfnValue(self, xyval,logform=False):
        cdef int n1,n2
        cdef double complex logwfn,zdiff,zsum,psi_com
        #print xyval
        logwfn=self.NormFactor
        #### Gaussian piece
        logwfn+=self.IPiTauNs*np.sum(xyval.imag**2)
        #print "logwfn1:",logwfn
        #### Jastrow factor
        n1=0
        while n1 < (self.Ne -1):
            n2=n1+1
            while n2 < self.Ne:
                #print "n1,n2:",n1,n2
                zdiff=xyval[n1]-xyval[n2]
                zdiff=zdiff.real + self.Tau*zdiff.imag
                #print "zdiff:",zdiff
                logwfn+=jacobitheta.logtheta1(zdiff, self.Tau)*self.Denom
                #print "logwfn2:",logwfn
                n2+=1
            n1+=1
        ### The quasi-holes
        n1=0
        while n1 < (self.Ne):
            n2=0
            while n2 < self.Nqh:
                #print "ne,nqh:",n1,n2
                #print "xyval,QhPos:",xyval[n1],self.QhPos[n2]
                zdiff=xyval[n1]-self.QhPos[n2]
                zdiff=zdiff.real + self.Tau*zdiff.imag
                #print "zdiff:",zdiff
                logwfn+=jacobitheta.logtheta1(zdiff, self.Tau)
                #print "logwfn2:",logwfn
                n2+=1
            n1+=1
            
        #### CoM function
        zsum=np.sum(xyval)
        zsum=zsum.real + self.Tau*zsum.imag
        #print "zsum:",zsum
        psi_com=jacobitheta.logthetagen(self.tfactor,self.tfactor*self.Denom,
                                        self.Denom*zsum+self.QhSum,self.Denom*self.Tau)
        logwfn+=psi_com
        #print "Com-value:",psi_com
        #print "logwfn3:",logwfn
        if not logform:
            return cexp(logwfn)
        else:
            return self.reduce_mod2pi(logwfn)

    def UpdateWfnValue(self, newxyval,oldxyval,NeMoved,oldPsi,logform=False):
        """ Updates the Laughlin ave function based on the knowlege that only one particles has moved."""
        cdef int n1,n2
        cdef double complex logwfn,newzdiff,oldzdiff,newzsum,oldzsum,oldlog
        #print oldxyval


        if not logform: ###Make old wave-function log scale if nececarry
            oldlog=clog(oldPsi)
        else:
            oldlog=oldPsi

        #### Gaussian piece
        logwfn=oldlog+self.IPiTauNs*(newxyval[NeMoved].imag**2-oldxyval[NeMoved].imag**2)

        #### Jastrow factor
        n1=0
        while n1 < self.Ne:
            if n1 != NeMoved:
                oldzdiff=oldxyval[n1]-oldxyval[NeMoved]
                oldzdiff=oldzdiff.real + self.Tau*oldzdiff.imag

                newzdiff=newxyval[n1]-newxyval[NeMoved]
                newzdiff=newzdiff.real + self.Tau*newzdiff.imag

                
                logwfn+=jacobitheta.logtheta1(newzdiff, self.Tau)*self.Denom
                logwfn-=jacobitheta.logtheta1(oldzdiff, self.Tau)*self.Denom
            n1+=1
        ### The quasi-holes
        n2=0
        while n2 < self.Nqh:
            #print "ne,nqh:",n1,n2
            oldzdiff=oldxyval[NeMoved]-self.QhPos[n2]
            oldzdiff=oldzdiff.real + self.Tau*oldzdiff.imag

            newzdiff=newxyval[NeMoved]-self.QhPos[n2]
            newzdiff=newzdiff.real + self.Tau*newzdiff.imag
            
            logwfn+=jacobitheta.logtheta1(newzdiff, self.Tau)
            logwfn-=jacobitheta.logtheta1(oldzdiff, self.Tau)
            n2+=1
            
        #### CoM function
        newzsum=np.sum(newxyval)
        newzsum=newzsum.real + self.Tau*newzsum.imag

        oldzsum=np.sum(oldxyval)
        oldzsum=oldzsum.real + self.Tau*oldzsum.imag
        
        #print "zsum:",zsum
        logwfn+=jacobitheta.logthetagen(self.tfactor,self.tfactor*self.Denom,
                                        self.Denom*newzsum+self.QhSum,self.Denom*self.Tau)
        logwfn-=jacobitheta.logthetagen(self.tfactor,self.tfactor*self.Denom,
                                        self.Denom*oldzsum+self.QhSum,self.Denom*self.Tau)
        #print "logwfn3:",logwfn
        if not logform:
            return cexp(logwfn)
        else:
            return logwfn

    @cython.profile(False)        
    cpdef double complex WfnValueLattice(self,long [:] xint_in,long [:] yint_in,logform=False):
        """
        Compute Lauglin function by using pre-sampled values
        """

        cdef int n1,n2, xdiff,ydiff,xsum,ysum
        cdef double complex logwfn,b,bsum
        cdef double phase
        if not self.LatticeSampled:
            print "Pre-sample the lattice."
            self.SampleLattice()

        ####Reduce coodinates to funcamental domain
        ###The periodic boundary conditions are:
        ### psi(x+Neff)=psi(x)
        ### psi(y+Neff)=psi(y)*exp(i*2*pi*x*Ns/Neff)= [if Ns == Neff] = psi(y)
        ### The wave-functions are actually periodic for the integer lattice  but not for a  generalized lattice ###
        logwfn=self.NormFactor
        n1 = 0
        while n1 < self.Ne:
            self.xint[n1]=mod(xint_in[n1],self.EffNs)
            self.yint[n1]=mod(yint_in[n1],self.EffNs)
            if self.EffNs != self.Ns:
                logwfn+=1j*self.TwoPi*self.xint[n1]*(self.yint[n1]-yint_in[n1])*self.Ns/(1.0*self.EffNs*self.EffNs)
            n1+=1
            
        
        #### Gaussian piece
        n1=0
        while n1 < self.Ne:
            b=self.GaussianTable[self.yint[n1]]
            logwfn+=b
            n1+=1
        #### Jastrow factor
        n1=0
        while n1 < (self.Ne -1):
            n2=n1+1
            while n2 < self.Ne:
                #print "n1,n2:",n1,n2
                xdiff=self.xint[n1]-self.xint[n2]
                ydiff=self.yint[n1]-self.yint[n2]
                b=self.JastrowTable[xdiff+self.EffNs,ydiff+self.EffNs]*self.Denom
                logwfn+=b
                n2+=1
            n1+=1
        ### The quasi-holes
        n1=0
        while n1 < (self.Ne):
            bsum=self.JastrowQhTable[self.xint[n1],self.yint[n1]]
            logwfn+=bsum
            n1+=1

        logwfn+=self.GetCoMValue()
        #print "Com-value:",self.GetCoMValue()

        if not logform:
            return cexp(logwfn)
        else:
            return self.reduce_mod2pi(logwfn)

    cdef double complex GetCoMValue(self,old=False):
        cdef int xsum,ysum,n1,Xrem,Yrem,Xshift,Yshift
        cdef double complex Zrem
        #### CoM function
        xsum=0
        ysum=0
        n1=0
        while n1 < self.Ne:
            if old:
                xsum+=self.oldxint[n1]
                ysum+=self.oldyint[n1]
            else:
                xsum+=self.xint[n1]
                ysum+=self.yint[n1]
            n1+=1
        #### If 0 > xsum,ysum > Ns (which is likely) we need to shift it back again.
        Xshift=xsum/self.EffNs
        Yshift=ysum/self.EffNs
        Xrem=xsum%self.EffNs
        Yrem=ysum%self.EffNs
        if Xrem<0:
            Xrem+=self.EffNs
            Xshift-=1
        if Yrem<0:
            Yrem+=self.EffNs
            Yshift-=1
        ### This implements the x-shift and y-shift
        Zrem=(Xrem+self.Tau*Yrem)/(1.0*self.EffNs)
        return (self.CoMTable[Xrem,Yrem]
                +1j*2*self.pi*self.tfactor*self.Denom*Xshift
                -1j*2*self.pi*Yshift*(self.tfactor*self.Denom+self.Denom*Zrem+self.QhSum)
                -1j*self.pi*self.Denom*self.Tau*Yshift*Yshift)
        
    @cython.profile(False)        
    cpdef double complex UpdateWfnValueLattice(self,long [:] newxint_in,long [:] newyint_in,long [:] oldxint_in,long [:] oldyint_in,int MovedNe, double complex oldPsi,logform=False):
        cdef int n1,n2, newxdiff,newydiff,newxsum,newysum
        cdef int oldxdiff,oldydiff,oldxsum,oldysum
        cdef double complex logwfn,oldlog
        if not self.LatticeSampled:
            self.SampleLattice()

        if not logform: ###Make old wave-function log scale if nececarry
            oldlog=clog(oldPsi)
        else:
            oldlog=oldPsi




                    ####Reduce coodinates to funcamental domain
        ###The periodic boundary conditions are:
        ### psi(x+Neff)=psi(x)
        ### psi(y+Neff)=psi(y)*exp(i*2*pi*x*Ns/Neff)= [if Ns == Neff] = psi(y)
        ### The wave-functions are actually periodic for the integer lattice  but not for a  generalized lattice ###

        logwfn=oldlog
        n1 = 0
        while n1 < self.Ne:
            self.xint[n1]=mod(newxint_in[n1],self.EffNs)
            self.yint[n1]=mod(newyint_in[n1],self.EffNs)
            self.oldxint[n1]=mod(oldxint_in[n1],self.EffNs)
            self.oldyint[n1]=mod(oldyint_in[n1],self.EffNs)
            if self.EffNs != self.Ns:
                logwfn+=1j*self.TwoPi*self.xint[n1]*(self.yint[n1]-newyint_in[n1])*self.Ns/(1.0*self.EffNs*self.EffNs)
                logwfn-=1j*self.TwoPi*self.oldxint[n1]*(self.oldyint[n1]-oldyint_in[n1])*self.Ns/(1.0*self.EffNs*self.EffNs)
            n1+=1
            
        #### Gaussian piece
        logwfn+=self.GaussianTable[self.yint[MovedNe]]
        logwfn-=self.GaussianTable[self.oldyint[MovedNe]]

        #### Jastrow factor
        n1=0
        while n1 < self.Ne:
            if n1 != MovedNe:
                newxdiff=self.xint[n1]-self.xint[MovedNe]
                newydiff=self.yint[n1]-self.yint[MovedNe]
                logwfn+=self.JastrowTable[newxdiff+self.EffNs,newydiff+self.EffNs]*self.Denom
                
                oldxdiff=self.oldxint[n1]-self.oldxint[MovedNe]
                oldydiff=self.oldyint[n1]-self.oldyint[MovedNe]
                logwfn-=self.JastrowTable[oldxdiff+self.EffNs,oldydiff+self.EffNs]*self.Denom
            n1+=1
        ### The quasi-holes
        logwfn+=self.JastrowQhTable[self.xint[MovedNe],self.yint[MovedNe]]
        logwfn-=self.JastrowQhTable[self.oldxint[MovedNe],self.oldyint[MovedNe]]

        #### CoM function
        logwfn+=self.GetCoMValue()
        logwfn-=self.GetCoMValue(old=True)
        
        if not logform:
            return cexp(logwfn)
        else:
            return self.reduce_mod2pi(logwfn)

        
    def SampleLattice(self):
        """
        Precalulate the theta factors going into the center of masses, and jastrow factors and gaussians
        """
        cdef int ypos,xpos,n2
        cdef double complex z,z2
        if self.EffNs!=self.Ns:
            print "Sample the lattice with effective size",self.EffNs,"x",self.EffNs
            print "Actual size is ",self.Ns,"x",self.Ns
        else:
            print "Sample the lattice size",self.EffNs,"x",self.EffNs
        StartTime=time.time()
        #### Initaite the diffierent tables
        self.GaussianTable=np.zeros((self.EffNs),dtype=np.complex128)
        self.JastrowQhTable=np.zeros((self.EffNs,self.EffNs),dtype=np.complex128)
        self.JastrowTable=np.zeros((2*self.EffNs,2*self.EffNs),dtype=np.complex128)
        self.CoMTable=np.zeros((self.EffNs,self.EffNs),dtype=np.complex128)
        ####Sample gaussian
        #print "Gaussian"
        ypos=0
        while ypos < self.EffNs:
            yvalue=(1.0*ypos)/self.EffNs
            self.GaussianTable[ypos]=self.IPiTauNs*yvalue**2
            ypos+=1
        ####Sample jatrow table between -EffNs and (EffNs-1)
        #print "Jastrow Table"
        ypos=-self.EffNs
        while ypos < self.EffNs:
            xpos=-self.EffNs
            while xpos < self.EffNs:
                z=(xpos+self.Tau*ypos)/self.EffNs
                self.JastrowTable[xpos+self.EffNs,ypos+self.EffNs]=jacobitheta.logtheta1(z,self.Tau)
                xpos+=1
            ypos+=1

        ####Sample jatrow table between 0 and (EffNs-1) for quasiholes
        #print "Jastrow Quasi-holes"
        ypos=0
        while ypos < self.EffNs:
            xpos=0
            while xpos < self.EffNs:
                self.JastrowQhTable[xpos,ypos]=0.0
                z=(xpos+self.Tau*ypos)/self.EffNs
                n2=0
                while n2 < self.Nqh:
                    z2=z-self.QhPos[n2].real-self.QhPos[n2].imag*self.Tau
                    self.JastrowQhTable[xpos,ypos]+=jacobitheta.logtheta1(z2,self.Tau)
                    n2+=1
                xpos+=1
            ypos+=1

        ####Sample comtable between (0x0) and (Ne*Ns x Ne*Ns)
        #print "Center of mass"
        ypos=0
        while ypos < self.EffNs:
            xpos=0
            while xpos < self.EffNs:
                z=(xpos+self.Tau*ypos)/self.EffNs
                self.CoMTable[xpos,ypos]=jacobitheta.logthetagen(self.tfactor,
                                                                 self.tfactor*self.Denom,
                                                                 self.Denom*z+self.QhSum,
                                                                 self.Denom*self.Tau)
                #print "CoMTable,x,y,",xpos,ypos,self.CoMTable[xpos,ypos]
                xpos+=1
            ypos+=1

        self.LatticeSampled=True
        print "Lattice sampled"
        EndTime=time.time()
        print "Sampling time: "+str(EndTime-StartTime)+"s = "+str((EndTime-StartTime)/60.0)+"min"

                    
    def WfnValueSerial(self, xyArray,axis=0,logform=False):
        """
        Calculate the value of the wave-function at xy in in slices along axis=axis.
        """
        cdef int n
        #print "Shape of xyArray:",xyArray.shape
        
        if axis==0:
            (Nsamples,Ne)=xyArray.shape
        elif axis==1:
            (Ne,Nsamples)=xyArray.shape

        mytimer=timer()
        wfArray=np.zeros((Nsamples),dtype=np.complex128)
        xyval=np.zeros((Ne),dtype=np.complex128)
        n = 0
        while n < Nsamples:
            if axis==0:
                xyval=xyArray[n,:]
            elif axis==1:
                xyval=xyArray[:,n]
            #print "xyval:",xyval
            #for indx in range(Ne):
            #    print xyval[indx]
            wfArray[n] = self.WfnValue(xyval,logform=logform)
            n+=1
            mytimer.write_progress(n,Nsamples)
        return wfArray

    def WfnValueSerialLattice(self, xyArray,axis=0,logform=False):
        """
        Calculate the value of the wave-function at xy in in slices along axis=axis using the precomputed lattice
        """
        cdef int n,m
        cdef long [:] xint,yint
        #print "Shape of xyArray:",xyArray.shape
        
        if axis==0:
            (Nsamples,Ne)=xyArray.shape
        elif axis==1:
            (Ne,Nsamples)=xyArray.shape
        ####Sanity check that xy-array fits on the target lattice
        print "Test that values are on lattice"
        DiffX=np.real(xyArray)*self.EffNs
        DiffY=np.imag(xyArray)*self.EffNs
        DiffX=np.abs(DiffX-np.round(DiffX))
        DiffY=np.abs(DiffY-np.round(DiffY))
        if np.any(DiffX>1.0e-10) or np.any(DiffY>1.0e-10):
            #print "DiffX:",DiffX
            #print "DiffY:",DiffY
            ERRTXT="At least one latticepoint does not lie on the "+str(self.EffNs)+"x"+str(self.EffNs)+" lattice!"
            raise ValueError,  ERRTXT
        print "Test completed - and passed"

        mytimer=timer()
        wfArray=np.zeros((Nsamples),dtype=np.complex128)
        xyval=np.zeros((Ne),dtype=np.complex128)
        xint=np.zeros((Ne),dtype=np.int)
        yint=np.zeros((Ne),dtype=np.int)
        print "Compute wave-function values"
        n = 0
        while n < Nsamples:
            if axis==0:
                xyval=xyArray[n,:]*self.EffNs
            elif axis==1:
                xyval=xyArray[:,n]*self.EffNs
            #print "xyval:",xyval
            m = 0
            while m < Ne:
                xint[m]=np.long(np.round(np.real(xyval[m])))
                yint[m]=np.long(np.round(np.imag(xyval[m])))
                m+=1
            #print "xint:",xint
            #print "yint:",yint
            wfArray[n] = self.WfnValueLattice(xint,yint,logform=logform)
            n+=1
            mytimer.write_progress(n,Nsamples)
        return wfArray

    def WfnValueGenerateLattice(self,long NSamples,logform=False,seed=None,long sampling_gap=-1,long therm=0):
        """
        Generate the Laughlin wave function through a random walk using Metroplois-Hastings algortithm
        """
        cdef long randstepx,randstepy,moveX,moveY,coordno
        cdef long [:] xcoord,ycoord,newXcoord,newYcoord
        cdef long Loops,FullLoopNo,RecalibrateNo,CalibrateAfter
        cdef double LogQuotient
        cdef double complex PsiOld, NewPsi
        cdef double complex [:] WfnList

        cdef int n,loopno,acceptcounter,stepcounter,sample_gap,thermalcounter
        ####Fixme, make these steps roughly one magnetic length or so
        ###but make sure that are at least 1~2 index steps
        (randstepx,randstepy)=self.get_orbitals_from_magnetic_length(3.0)
        print "randstepx: ",randstepx
        print "randstepy: ",randstepy

        
        if sampling_gap==-1: ###Set the sapling pag to the desired integer
            sample_gap=1 #### Pick up every sampled step (no omissions)
        else:
            sample_gap=sampling_gap
            
        print "Setup the memory view to store coords and wave-function"
        ###Generate the inital configuration
        np.random.seed(seed)
        WfnList=np.zeros((NSamples),dtype=np.complex128)
        CoordList=np.zeros((NSamples,self.Ne),dtype=np.complex128)
        xcoord=np.zeros((self.Ne),dtype=np.integer)
        ycoord=np.zeros((self.Ne),dtype=np.integer)
        newXcoord=np.zeros((self.Ne),dtype=np.integer)
        newYcoord=np.zeros((self.Ne),dtype=np.integer)
        xycoord=np.zeros((self.Ne),dtype=np.complex128)
        #### Set the inital configuration
        [xcoord,ycoord]=self.get_starting_configuration()
        ###Compute the inital laughlin wave functoin value
        PsiOld=self.WfnValueLattice(xcoord, ycoord,logform=True)
        #print "PsiOld:",PsiOld

        ####NOTE: 18/9-2017 For 40 particles (4'000'000 steps)  CA=0 => 27s, CA=1 => 20s,
        #####  CA=Ne => 12s, CA=Ns=>11s, CA=Ne*Ne=>11s (so there is a difference between updates and not)
        CalibrateAfter=self.Ns
        Loops=1000
        FullLoopNo=Loops-1
        print "Thermalize the configurations through "+str(therm)+" steps..."
        mytimer=timer()
        n=0
        Thermalize=True
        thermalcounter=0
        loopno=0
        acceptcounter=0
        stepcounter=0
        StartTime=time.time()
        RecalibrateNo=0
        while n< NSamples:
            FullLoopNo+=1
            if FullLoopNo==Loops:
                ##Reset the predrawn list of randomnumbers
                #print "Reset the loops."
                coordnoList=np.random.randint(self.Ne,size=(Loops))
                moveXList=np.random.random_integers(-randstepx,randstepx,size=(Loops))
                moveYList=np.random.random_integers(-randstepy,randstepy,size=(Loops))
                RandomList=np.random.random(size=(Loops))
                FullLoopNo=0
                #print "Loopno:",FullLoopNo
            ###Move the coordiantes
            ###Choose wich coodrinate to move
            coordno=coordnoList[FullLoopNo]
            moveX=moveXList[FullLoopNo]
            moveY=moveYList[FullLoopNo]

            #print "coordno,movex,movey:",coordno,moveX,moveY
            newXcoord[:]=xcoord
            newYcoord[:]=ycoord
            newXcoord[coordno]=mod(newXcoord[coordno]+moveX,self.EffNs)
            newYcoord[coordno]=mod(newYcoord[coordno]+moveY,self.EffNs)

            if RecalibrateNo == CalibrateAfter:
                NewPsi=self.WfnValueLattice(newXcoord, newYcoord,logform=True)
                RecalibrateNo=0
            else:
                NewPsi=self.UpdateWfnValueLattice(newXcoord, newYcoord,xcoord,ycoord,coordno,PsiOld,logform=True)
                RecalibrateNo+=1

            #for n2 in range(self.Ne):
            #    xycoord[n2]=(newXcoord[n2]+1j*newYcoord[n2])/(1.0*self.EffNs)
            #NewPsiAbs=self.WfnValue(xycoord,logform=True)

            """
            if (np.abs(NewPsi-NewPsiAbsLatt) > 10e-10):# or (np.abs(NewPsi-NewPsiAbs) > 10e-10) :
                print "xcoord:   "
                print_array(xcoord,self.Ne)
                print "newXcoord:"
                print_array(newXcoord,self.Ne)
                print "ycoord:   "
                print_array(ycoord,self.Ne)                        
                print "newYcoord:"
                print_array(newYcoord,self.Ne)
                ERRTXT="ERROR: Discrepancy!"
                ERRTXT+="\nUpdate no: "+str(n)
                ERRTXT+="\nUpdate electron: "+str(coordno)
                ERRTXT+="\nPsiOld: "+str(PsiOld)
                ERRTXT+="\nNewPsi: "+str(NewPsi)
                ERRTXT+="\nNewPsiAbsLatt: "+str(NewPsiAbsLatt)
                ERRTXT+="\nNewPsiAbs: "+str(NewPsiAbs)
                ERRTXT+="\nDiff AbsLatt: "+str(NewPsi-NewPsiAbsLatt)
                ERRTXT+="\nDiff Abs: "+str(NewPsi-NewPsiAbs)
                ERRTXT+="\nNe,Ns,EffNs: "+str(self.Ne)+", "+str(self.Ns)+", "+str(self.EffNs)
                ERRTXT+="\nDenom,NQh: "+str(self.Denom)+", "+str(self.Nqh)
                print ERRTXT
                raise Exception,ERRTXT
            """
            
            LogQuotient=creal(NewPsi-PsiOld)*2
            #print "LogQuotient:",LogQuotient
            ###Here is the metropolis step
            if exp(LogQuotient) > RandomList[FullLoopNo]:###Test to accept the step
                #print("Accepted")
                ### Update the coodinates and results
                xcoord[:]=newXcoord
                ycoord[:]=newYcoord
                PsiOld=NewPsi
                acceptcounter+=1
            #else:#print("Rejected")#Try again
            if Thermalize:
                thermalcounter+=1
                mytimer.write_progress(thermalcounter,therm)
                if thermalcounter >= therm:
                    Thermalize=False
                    print "Thermalized ...."
                    print "acceptcounter:",acceptcounter
                    print "stepcounter:",therm
                    print "Acceptance rate:",(acceptcounter*100.0)/therm,"%"
                    EndTime=time.time()
                    print "Thermalization time: "+str(EndTime-StartTime)+"s = "+str((EndTime-StartTime)/60.0)+"min"
                    print 
                    acceptcounter=0
                    print "Generare configurations and sampling.."
                    print "Keep every "+str(sample_gap)+":th step"
                    StartTime=time.time()

                    mytimer=timer()
            else:
                if loopno==sample_gap-1:
                    ###Record the data if loopno+1=sample_gap
                    if logform:
                        WfnList[n]=self.reduce_mod2pi(PsiOld)
                    else:
                        WfnList[n]=cexp(PsiOld)
                    ###rescale the lattice coordinates to redduced coordinates for easier later comparison
                    for indx in range(self.Ne):
                        CoordList[n,indx]=(xcoord[indx]+1j*ycoord[indx])/(1.0*self.EffNs)
                    n+=1
                    loopno=0
                    #print "sample"
                    mytimer.write_progress(n,NSamples)
                else: ### make another loop
                    loopno+=1
                    #print "loop"
                stepcounter+=1

        print "acceptcounter:",acceptcounter
        print "stepcounter:",stepcounter
        print "Acceptance rate:",(acceptcounter*100.0)/stepcounter,"%"
        EndTime=time.time()
        print "Sampling time: "+str(EndTime-StartTime)+"s = "+str((EndTime-StartTime)/60.0)+"min"
        ####
        return WfnList,CoordList


    def get_orbitals_from_magnetic_length(self,float steplength):
        """
        Return the number of orbitals to hop, based on the desired magnetic length of the hopping
        """
        ##Computing the hopping length
        ##Tries to hopp ~ magnetic length
        ## But if ~l_b < orbital_spacing this is set to the step length
        fluxfact=0.5/np.sqrt(self.Ns);
        print("Naive number of magnetic lengths to step:",steplength)
        l_x=max(1.0/self.Ns,fluxfact*np.sqrt(np.imag(self.Tau)))*steplength/2.0
        l_y=max(1.0/self.Ns,fluxfact/np.sqrt(np.imag(self.Tau)))*steplength/2.0
        print "Step-length on reduced lattice x-direction:",l_x
        print "Step-length on reduced lattice y-direction: ",l_y
        print "Corresponding to number of orbitals in x: ",l_x*self.Ns
        print "Corresponding to number of orbitals in y: ",l_y*self.Ns
        int_x=int(np.round(l_y*self.Ns))
        int_y=int(np.round(l_y*self.Ns))
        print "Rounded to orbitals in x: ",int_x
        print "Rounded to orbitals in y: ",int_y
        return (int_x,int_y)
 
    def get_starting_configuration(self):
        ###Generate a set of positions that are guaranteed to to be non-overlapping.... (If EffNs**2 >= Ne)
        if self.EffNs**2 < self.Ne:
            raise Exception,"There is not enough flux positions to place electrons"
        ###Define the target coordinates
        xcoord=np.zeros((self.Ne),dtype=np.integer)
        ycoord=np.zeros((self.Ne),dtype=np.integer)
        print "Generate starting position for",self.Ne,"electrons and",self.EffNs,"effective flux." 
        for n in range(self.Ne):
            while True:
                ###Choose a new datapoint
                NewX=np.random.randint(self.EffNs)
                NewY=np.random.randint(self.EffNs)
                ###Check that this datapoint is not occupied
                NewPoint=not np.sum(np.logical_and(
                    np.mod(NewX-xcoord[:n],self.EffNs)==0,
                    np.mod(NewY-ycoord[:n],self.EffNs)==0))
                #print("NewPoint:",NewPoint)
                if NewPoint:
                    ###ser the new point
                    xcoord[n]=NewX
                    ycoord[n]=NewY
                    break
                else:
                    print "Position (",NewX,",",NewY,") already occupied"
                ### else try again...
            n+=1
        #print "xcoord:",xcoord
        #print "ycoord:",ycoord
        return (xcoord,ycoord)

    @cython.profile(False)
    cdef inline double complex reduce_mod2pi(self, double complex x):
        cdef double ytmp=fmod(x.imag, self.TwoPi)
        if ytmp<0.0:
            ytmp+=self.TwoPi
        return x.real + 1j*ytmp


def print_array(array,length):
    sys.stdout.write('[ ')
    for n in range(length-1):
        sys.stdout.write(str(array[n])+" , ")
    sys.stdout.write(str(array[length-1])+" ")
    sys.stdout.write(']')
    print 

@cython.profile(False)
cdef inline long mod(long x,long y):
    cdef long r
    r=x%y
    if r< 0:
        r+=y
    return r
    

