#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" utils_cython.pyx: Number of useful utility functions as a cython extension.

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"
"""

import numpy as np
cimport numpy as np
import sys
import sympy as syp
import scipy as scipy


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)
    double round(double x)


cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double complex conj(double complex x) nogil


cdef extern from "f2pyptr.h":
    void *f2py_pointer(object) except NULL

# Prototype for lapack LU factorisation function for complex numbers.
ctypedef int zgetrf_t(
    int *M, int *N,
    double complex *A,
    int *LDA,
    int *IPIV,
    int *INFO)

cdef zgetrf_t *zgetrf = <zgetrf_t*>f2py_pointer(scipy.linalg.lapack.zgetrf._cpointer)


cdef class MatrixDeterminant:
    """
    Class for performing fast determinant calculations by calling LAPACK routine directly.
    """

    cdef double complex [::1,:] A # define of A matrix to be column ordered.
    cdef int N, M, LDA, INFO
    cdef int [::1] IPIV

    def __init__(self, N):
        """
        Constructor that sets up storage and variables that will be needed.

        Parameters
        -----------
        N: int
            Dimension of matrices to get determinants of.
        """
        self.A = np.zeros((N, N), dtype=np.complex128, order='F')
        self.IPIV = np.zeros(N, dtype=np.int32, order='F')
        self.N = N
        self.M = N
        self.LDA = N


    def __str__(self):
        """
        Return string to print.
        """
        desc = 'Dimension: ' + str(self.N) + '\n'
        for i in range(self.N):
            for j in range(self.N):
                desc = desc + '{:.4e}  '.format(self.A[i,j])
            desc = desc + '\n'
        return desc


    cpdef setitem(self, int i, int j, double complex x):
        self.A[i,j] = x


    cpdef double complex getitem(self, int i, int j):
        return self.A[i,j]


    cpdef double complex det(self):
        """
        Calculate determinant.

        Returns
        -------
        double complex
            The determinant.
        """
        cdef double complex Det
        cdef int m, n
        zgetrf(&self.M, &self.N, &self.A[0,0], &self.LDA, &self.IPIV[0], &self.INFO)
        if self.INFO != 0:
            Det = 0.0
        else:
            Det = 1.0
            m = 0
            while m < self.N:
                if self.IPIV[m] != (m + 1):
                    Det = -Det * self.A[m,m]
                else:
                    Det = Det * self.A[m,m]
                m += 1
        return Det


def PlaneWaveDeterminant(np.ndarray[np.float64_t, ndim=2] xcoord, np.ndarray[np.float64_t, ndim=2] ycoord,
                         np.ndarray[np.int8_t, ndim=2] k1_set, np.ndarray[np.int8_t, ndim=2] k2_set):
    """
    Function to calculate determinants of matrices of plane waves with given momentums
    at the given positions.

    Parameters
    -----------
    xcoord: matrix
        Matrix of x coordinates.
    ycoord: matrix
        Matrix of y coordinates.
    k1_set: matrix
        Rows store k1 momentums for each configuration.
    k2_set: matrix
        Rows store k2 momentums for each configuration.

    Return
    -------
    matrix
        Matrix with determinant values. Rows correspoond to samples and columns
        difference configurations.
    """
    cdef int NSamples, NConfigs, MC_No, FS_No, i, j, Particles
    Particles = xcoord.shape[1]
    NSamples = xcoord.shape[0]
    NConfigs = k1_set.shape[0]
    Particles = xcoord.shape[1]
    cdef np.ndarray[np.complex128_t, ndim=2] Dets = np.zeros((NSamples,NConfigs), dtype=np.complex128)

    MatDet = MatrixDeterminant(Particles)

    MC_No = 0
    while MC_No < NSamples:
        FS_No = 0
        while FS_No < NConfigs:
            i = 0
            while i < Particles:
                j = 0
                while j < Particles:
                    MatDet.setitem(i,j,cexp((xcoord[MC_No, i]*k1_set[FS_No, j] - ycoord[MC_No, i]*k2_set[FS_No, j])*2*np.pi*1j))
                    j+=1
                i+=1
            Dets[MC_No, FS_No] = MatDet.det()
            FS_No += 1
        MC_No += 1

    return Dets
