from hammer.FQHTorusBasis cimport FQHTorusBasis

cdef class FQHTorusBasisCOMMomentum(FQHTorusBasis):
    cdef int COMMomentum
    cdef int Sectors # number of translations to get all the way around.
    cdef long *OverFlowMasks  # array of masks, which cover the overflow section for each translation.
    cdef double complex *Phases # array to hold pre computed phases to save time.
    cdef long TmpRepresentative

#    cdef int FindBasisDimension(self, int J, int Nphi, int Ne)
    cdef int FindSubBasisDimensionCOMMomentum(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base)
    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base)
    cdef int IsRepresentativeConfiguration(self, long State)
    cdef double GetNormalIdx(self, Idx)
    cdef double GetNormal(self)
    cpdef long GetRepresentative(self)
    cpdef double complex GetRepresentativePhase(self)
    cpdef int BinaryArraySearchDescending(self)
    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx)
    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double AA(self, int Orbital1, int Orbital2, int Idx)
    cpdef double complex AdAd(self, int Orbital1, int Orbital2)
    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3)
    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx)
    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4)
    cpdef CreateConversionToFullOp(self, FullBasis)
    cpdef ConvertToFullBasis(self, State, FullBasis=*, ReturnOp=*)
    cpdef ConvertFromFullBasis(self, State, FullBasis=*, ReturnOp=*)
    cpdef double complex GetPhase(self, int Idx)
    cpdef long SimpleTranslate(self, long State, int Steps)
    cpdef double TranslatePhase(self, long State, int Steps)

