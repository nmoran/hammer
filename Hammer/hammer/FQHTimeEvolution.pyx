#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTimeEvolution.pyx: Cython code for calculating time evolution.

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"
"""

import petsc4py.PETSc as PETSc

import numpy as np
cimport numpy as np
import scipy.special as sps
import sys
from hammer.utils_timer import timer
import sympy as syp


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)
    double round(double x)


cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double complex conj(double complex x) nogil


def StepsTimeEvolveNP(Hamiltonian,State,DeltaTime,Steps):
    """ Evolve a given state some steps of length deltattime by repeatedly applying the tamiltonian to it"""
    Dim=len(State)
    TimeEvolvedMatrix=np.zeros((Dim,Steps),dtype=np.complex128)
    for step in range(Steps):
        if step==0:
            TimeEvolvedMatrix[:,step]=TimeEvolveNP(Hamiltonian,State,DeltaTime)
        else:
            TimeEvolvedMatrix[:,step]=TimeEvolveNP(Hamiltonian,TimeEvolvedMatrix[:,step-1],DeltaTime)
    return TimeEvolvedMatrix

def StepsTimeEvolvePETSc(Hamiltonian,State,DeltaTime,Steps,saved=1,printed=1,RotatingFrame=False):
    """ Evolve a given state some steps of length delta-time by repeatedly applying the Hamiltonian to it"""
    TimeEvolvedMatrix=[State]
    TimeList=[0.0]
    if RotatingFrame==True:
        ####Compute the energy expectation value of the stating state an remove it from the Hamiltonian#
        Energy=0
        MultipliedState=State.copy()
        ConjugatedState=State.copy()
        ConjugatedState.conjugate()
        Hamiltonian.mult(State,MultipliedState)
        Energy=MultipliedState.dot(ConjugatedState)
        print "The energy before shift:",Energy
        Innerproduct=State.norm()###Computes the two norm
        print "Inner proocuds:",Innerproduct
        ###Shift the Hamiltonian by the energy...
        Hamiltonian.shift(-Energy/np.sqrt(Innerproduct))
        Hamiltonian.mult(State,MultipliedState)
        Energy=MultipliedState.dot(ConjugatedState)
        print "The energy after shift :",Energy
        #sys.exit()
    mytimer=timer()
    for step in range(1,Steps+1):
        Abstime=step*DeltaTime
        if step==1:
            Outputvector,ConvOrder=TimeEvolvePETSc(Hamiltonian,State,DeltaTime)
        else:
            Outputvector,ConvOrder=TimeEvolvePETSc(Hamiltonian,Outputvector,DeltaTime)
        if step%saved==0:
            TimeEvolvedMatrix.append(Outputvector)
            TimeList.append(Abstime)
        mytimer.write_progress(step,Steps)
        if step%printed==0:
            print "step",step,"of",Steps
            print "at time",Abstime
            print "converged at order",ConvOrder

    return TimeEvolvedMatrix,TimeList

def TimeEvolveNPExact(Energies,Eivenvectors,State,Time):
    """ Evolve a given state some time t by expanding it in its eigenvectors and evolving their phases"""
    ###Assert that Eivenvectors deimetions match the Energies
    #print "Entering the exact eigenvector expander"
    (D1,D2)=Eivenvectors.shape
    N=len(Energies)
    assert(D1==D2) ###Asser square matrix
    assert(D1==N) ### Assert energies match eigenvctors
    ### The Eivenvectors are assumes to be ordered by collumn
    ### i.e. V_i = Eivenvectors[:,i]
    ### Compute scalar product <V_i|S> to get the expansion of S in terms of V_i.
    Coeffs=np.conj(np.dot(np.conj(State),Eivenvectors))
    #### Time Evolve the coefficents
    Coeffs=Coeffs*np.exp(1j*Energies*Time)
    NewVector=np.dot(Eivenvectors,Coeffs)
    return NewVector
    



def TimeEvolveNPExactSteps(Energies,Eivenvectors,State,DeltaTime,Steps,saved=1,printed=1):
    """ Evolve a given state some steps of length deltattime by repeatedly applying the tamiltonian to it"""
    Dim=len(State)
    TimeEvolvedMatrix=np.zeros((Dim,Steps),dtype=np.complex128)
    TimeList=DeltaTime*np.arange(Steps)
    for step in range(Steps):
        TimeEvolvedMatrix[:,step]=TimeEvolveNPExact(Energies,Eivenvectors,State,TimeList[step])
        print "step",step,"of",Steps
    return TimeEvolvedMatrix,TimeList


def TimeEvolveNP(Hamiltonian,State,Time):
    """ Evolve a given state some time t by repeadtely applying the tamiltonian to it"""
    
    #### Time evolution is obtained as
    ##|t> = exp(itH)=sum_{n=0}(itH)^n/(n!)
    ### WE expand this sum term by term and terminate when a given precision has been obtained

    #### Set vector dimentions
    NewState=np.array(State)
    MulipliedState=np.array(State)
    OldState=np.array(State)
    Diff=np.array(State)
    n=1
    while True:
        scalar=(1j*Time/n)
        ###Apply the hamiltonian to the state
        ###Rescale the Multiplicaiton vector
        MulipliedState=scalar*Hamiltonian.dot(MulipliedState)
        ###Copy the old state to the new state
        OldState=np.array(NewState)
        NewState+=MulipliedState ###Update the New state
        Diff=NewState-OldState
        print "n=",n
        #print "MulipliedState:",MulipliedState
        #print "OldState:",OldState
        #print "NewState:",NewState
        #print "Diff:",Diff
        print "sum(Diff):",np.sum(np.abs(Diff))
        n+=1
        if np.sum(np.abs(Diff)) == 0.0:
            break
    return  NewState


def TimeEvolvePETSc(Hamiltonian,State,Time,Verbose=False):
    """ Evolve a given state some time t by repeadtely applying the tamiltonian to it"""
    
    ###Assume Time evoltuion is a Petsc matrix and the input state is a Petc vector
    
    
    #### Time evolution is obtained as
    ##|t> = exp(itH)=sum_{n=0}(itH)^n/(n!)
    ### WE expand this sum term by term and terminate when a given precision has been obtained

    #### Create copy the old state to the new
    NewState=State.copy()
    MultipliedStateNew=State.copy()
    MultipliedState=State.copy()
    OldState=State.copy()
    Diff=State.copy()
    n=1
    while True:
        ###Copy the multiplication from the hold on
        MultipliedStateNew.copy(MultipliedState)
        scalar=1j*Time/n
        ###Rescale the Multiplicaiton vector
        MultipliedState.scale(scalar)
        ###Copy the old state to the new state
        NewState.copy(OldState)
        ###Apply the hamiltonian to the old MultipliedState
        Hamiltonian.mult(MultipliedState,MultipliedStateNew)
        ###.... and add into the new state
        NewState.__iadd__(MultipliedStateNew)
        ###The old state is now the differnce
        OldState.__isub__(NewState)
        #print "n=",n
        OldState.abs()
        TotDiff=np.real(OldState.sum())
        #print "TotDiff:  ",TotDiff
        if TotDiff == 0.0:
            if Verbose: print "Neded",n,"steps to converge"
            break
        n+=1
    return  NewState,n
