#! /usr/bin/env python

""" SphereDiagonalise.py: Utility perform exact diagonalisation on spherical FQH systems. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys, slepc4py
import string
slepc4py.init(sys.argv)

from petsc4py import PETSc
from slepc4py import SLEPc

# Custom modules
import hammer.FQHSphereCoefficients as FQHSphereCoefficients
from hammer.FQHSphereBasis import FQHSphereBasis
from hammer.FQHSphereCoefficients import FQHSphereInteractionCoefficients2Body
from hammer.FQHSphereHamiltonian import FQHSphereHamiltonian
import hammer.vectorutils as vecutils
import hammer.matrixutils as matutils

# System modules
import sys
import argparse
import numpy as np
import time
import csv

import petsc4py
PETSc_Major_Version = np.int(petsc4py.__version__.split('.')[0])
PETSc_Minor_Version = np.int(petsc4py.__version__.split('.')[1])

def __main__(argv):
    # Run the argument parser and process command line arguments.
    Opts = SphereArgsParser()
    Opts.parseArgs(argv)
    Opts.WriteEnergies = True
    SphereDiagonalise(Opts)


def SphereDiagonalise(Opts):
    # This print is to be used when working with many processes as will ensure that statement is only printed once.
    PetscPrint = PETSc.Sys.Print

    # Find the number of processes.
    NumProcesses = PETSc.Comm.getSize(PETSc.COMM_WORLD)
    MyRank = PETSc.Comm.getRank(PETSc.COMM_WORLD)

    PetscPrint("Using %d processes." % NumProcesses)

    # Prefix to use for data files.
    if Opts.Prefix == '':
        Prefix = 'Sphere_'  'p_' + str(Opts.Particles) + '_2s_' + str(Opts.TwiceLzMax) + '_lz_' + str(Opts.TwiceLz) + "_" + Opts.ID
    else:
        Prefix = Opts.Prefix

    PetscPrint('Diagonalising Sphere wih %d particles, twice max Lz %d and twice Lz sector %d' % (Opts.Particles, Opts.TwiceLzMax, Opts.TwiceLz))


    ###------------------------------------------
    ###           Compute interaction coefficients
    ###------------------------------------------

    if Opts.PseudoPotentials is None and Opts.InteractionFile != '':
        PetscPrint('Reading pseudopotentials from %s.' % (Opts.InteractionFile))
        PseudoPotentials = ReadPseudoPotentialsFile(Opts.InteractionFile)
        if PseudoPotentials is None:
            PetscPrint('Problem reading pseudopotentials.')
            sys.exit(1)
        PetscPrint('%d pseudopotenials values read.' % (len(PseudoPotentials)))
    elif Opts.Interaction == "Coulomb":
        PseudoPotentials = FQHSphereCoefficients.VkCoulomb(Opts.TwiceLzMax)
    elif Opts.Interaction == "Delta":
        PseudoPotentials = FQHSphereCoefficients.VkDelta(Opts.TwiceLzMax)
    elif not (Opts.PseudoPotentials is None):
        PseudoPotentials = Opts.PseudoPotentials
    else:
        raise Exception,"No pseudotentials supplied"
    

    if len(PseudoPotentials) > (Opts.TwiceLzMax + 1) and not Opts.Force:
        PetscPrint('Warning: more pseudopotential values ('+str(len(PseudoPotentials))+') than orbitals ('+str(Opts.TwiceLzMax + 1)+') have been provided. Use force (-f) option if you want to go ahead with computation.')
        sys.exit(-1)
    elif len(PseudoPotentials) < (Opts.TwiceLzMax + 1) and not Opts.Force:
        PetscPrint('Warning: less pseudopotential values ('+str(len(PseudoPotentials))+') than orbitals ('+str(Opts.TwiceLzMax + 1)+') have been provided. Use force (-f) option if you want to go ahead with computation.')
        sys.exit(-1)



    start = time.time()
    PetscPrint('Compute interaction coefficients')
    InteractionCoefficients = FQHSphereInteractionCoefficients2Body(Opts.TwiceLzMax, PseudoPotentials=PseudoPotentials, L2Factor=Opts.L2Factor, Lz=Opts.TwiceLz/2.0)
    InteractionCoefficients.CalculateCoefficients()
    end = time.time()
    PetscPrint('Coefficients calculated in ' + str(end-start) + ' seconds.')

    if Opts.WriteCoefficients and MyRank == 0: # Only do this if the fist process.
        InteractionCoefficients.WriteAntiSymCoefficients(Prefix+'_CoeffSym.csv')

    if Opts.CoefficientsOnly:
        return

    
    ###------------------------------------------
    ###           Construction of basis.
    ###------------------------------------------
    MyBasis = FQHSphereBasis(Opts.Particles, Opts.TwiceLzMax, Opts.TwiceLz)
    MyBasis.GenerateBasis()
    BasisDimension = MyBasis.GetDimension()
    PetscPrint('Hilbert space dimension: %d' % (BasisDimension))

    
    
    if MyBasis.GetDimension() <= 0:
        PetscPrint('Basis dimension ' + str(MyBasis.GetDimension()) + ', exiting.')
        return

    # Construct the Hamiltonian matrix
    SphereHamiltonian = FQHSphereHamiltonian(MyBasis, InteractionCoefficients)
    start = time.time()
    HamiltonianMatrix = SphereHamiltonian.CreateSlepcHamiltonian(None)
    end = time.time()
    PetscPrint( 'Hamiltonian constructed in ' + str(end-start) + ' seconds.')
    MyMatrix = HamiltonianMatrix

    
    if Opts.L2Value:
        start = time.time()
        L2InteractionCoefficients = FQHSphereInteractionCoefficients2Body(Opts.TwiceLzMax, L2Factor=1.0, Lz=Opts.TwiceLz/2.0)
        L2InteractionCoefficients.CalculateCoefficients()
        end = time.time()
        PetscPrint('L2 Coefficients calculated in ' + str(end-start) + ' seconds.')

        # Construct the Hamiltonian matrix
        L2SphereOperator = FQHSphereHamiltonian(MyBasis, L2InteractionCoefficients)
        start = time.time()
        L2Matrix = L2SphereOperator.CreateSlepcHamiltonian(None)
        end = time.time()
        PetscPrint( 'L2 operator constructed in ' + str(end-start) + ' seconds.')

    #Break and lonly return the hamiltonian
    if Opts.ReturnHamtiltonian and Opts.ReturnBasis:
        if Opts.L2Value:
            return MyMatrix,MyBasis,L2Matrix
        else:
            return MyMatrix,MyBasis
    elif Opts.ReturnHamtiltonian:
        if Opts.L2Value:
            return MyMatrix,L2Matrix
        else:
            return MyMatrix
    elif Opts.ReturnBasis:
        if Opts.L2Value:
            return MyBasis, L2Matrix
        else:
            return MyBasis
        
    # Remove basis object to save some memory
    del MyBasis

    # read in initial states if any given
    InitialStates = []
    for Filename in Opts.InitialStateNames:
        State = vecutils.ReadBinaryPETScVector(Filename)
        InitialStates.append(State)

    # read in deflation states if any given
    DeflationStates = []
    for Filename in Opts.DeflationStateNames:
        State = vecutils.ReadBinaryPETScVector(Filename)
        DeflationStates.append(State)


    if len(Opts.StateNames) == 0:
        E = SLEPc.EPS()
        E.create()
        #E.setType(E.Type.LANCZOS)
        E.setOperators(MyMatrix)
        E.setProblemType(SLEPc.EPS.ProblemType.HEP)
        E.setWhichEigenpairs(SLEPc.EPS.Which.SMALLEST_REAL)
        E.setDimensions(nev=Opts.NbrEigvals)
        #E.setTolerances(tol=Opts.EPSTol, max_it=Opts.MaxIterations)
        #S = E.getST()
        #S.setType(S.Type.SHIFT)
        #S.setShift(Opts.Shift)
        #S.setFromOptions()
        E.setFromOptions()

        # set the initial space for solver to start at
        if len(InitialStates) > 0:
            PetscPrint( 'Setting initial space for the solver with ' + str(len(InitialStates)) + ' states.')
            E.setInitialSpace(InitialStates)

        # set the deflation space for solver to start at
        if len(DeflationStates) > 0:
            PetscPrint( 'Setting deflation space for the solver with ' + str(len(DeflationStates)) + ' states.')
            E.setDeflationSpace(DeflationStates)

        start = time.time();E.solve(); end = time.time()
        PetscPrint( 'Hamiltonian diagonalised in ' + str(end-start) + ' seconds.')

        its = E.getIterationNumber()
        PetscPrint( "Number of iterations of the method: %d" % its )

        eps_type = E.getType()
        PetscPrint( "Solution method: %s" % eps_type )

        nev, ncv, mpd = E.getDimensions()
        PetscPrint( "Number of requested eigenvalues: %d" % nev )

        tol, maxit = E.getTolerances()
        PetscPrint( "Stopping condition: tol=%.4g, maxit=%d" % (tol, maxit) )

        nconv = E.getConverged()
        PetscPrint( "Number of converged eigenpairs %d" % nconv )

        if MyRank == 0 and Opts.WriteEnergies:
            if sys.version_info[0] == 3:
                f = open(Prefix + "_Energies.csv","w", encoding='utf8', newline='')
            elif sys.version_info[0] == 2:
                f = open(Prefix + "_Energies.csv","wb")
            csv_writer = csv.writer(f)

        EigenValues = list()
        EigenVectors = list()

        SLEPcVersion = SLEPc.Sys.getVersion()

        if nconv > 0:
            # Create the results vectors
            vr, wr = MyMatrix.getVecs()
            vi, wi = MyMatrix.getVecs()
            #
            PetscPrint()
            if Opts.L2Value:
                PetscPrint("        k          ||Ax-kx||/||kx||   Estimate       ||Ax-kBx||_2     L2")
                PetscPrint("----------------- ------------------  -------------  -------------   -----")
            else:
                PetscPrint("        k          ||Ax-kx||/||kx||   Estimate       ||Ax-kBx||_2 ")
                PetscPrint("----------------- ------------------  -------------  -------------")
            for i in range(nconv):
                if i < Opts.NbrStates:
                    k = E.getEigenpair(i, vr, vi)
                    if SLEPcVersion[1] >= 6:
                        relativeError = E.computeError(i, SLEPc.EPS.ErrorType.RELATIVE)
                        residualNorm = E.computeError(i, SLEPc.EPS.ErrorType.BACKWARD)
                        errorEstimate = E.computeError(i, SLEPc.EPS.ErrorType.ABSOLUTE)
                    else:
                        relativeError = E.computeRelativeError(i)
                        residualNorm = E.computeResidualNorm(i)
                        errorEstimate = E.getErrorEstimate(i)

                    if Opts.ReturnEigenValues:
                        EigenValues.append([k.real, k.imag, relativeError, errorEstimate, residualNorm])

                    EigenState = vr.copy()
                    if Opts.ReturnEigenVectors:
                        EigenVectors.append(EigenState)

                    if Opts.L2Value:
                        L2Value = matutils.ExpectationValue(L2Matrix, EigenState)

                    if Opts.SaveStates:
                        Viewer = PETSc.Viewer()
                        Viewer.createBinary(name=Prefix + "_State_" + str(i) + ".vec", mode=PETSc.Viewer.Mode.W)
                        Viewer.view(EigenState)
                        (Viewer.clear() if PETSc_Minor_Version <= 4 else Viewer.clearDraw())
                        Viewer.destroy()

                    if Opts.L2Value:
                        PetscPrint( "%.12g, %.12g, %.12g, %.12g, %.12g, %.12g" % (k.real, k.imag, relativeError, errorEstimate, residualNorm, np.real(L2Value)) )
                        if MyRank == 0:
                            csv_writer.writerow([k.real, k.imag, relativeError, errorEstimate, residualNorm, np.real(L2Value)])
                    else:
                        PetscPrint( "%.12g, %.12g, %.12g, %.12g, %.12g" % (k.real, k.imag, relativeError, errorEstimate, residualNorm) )
                        if MyRank == 0 and Opts.WriteEnergies:
                            csv_writer.writerow([k.real, k.imag, relativeError, errorEstimate, residualNorm])

            PetscPrint()

        if MyRank == 0 and Opts.WriteEnergies:
            f.close()

        ReturnSet = dict()
        ReturnSet['EigenValues'] = EigenValues
        ReturnSet['EigenVectors'] = EigenVectors
        return ReturnSet


            
    else:
        ExpectationValues = []
        for StateName in Opts.StateNames:
            State = vecutils.ReadBinaryPETScVector(StateName)
            StateDimension = State.getSize()
            if StateDimension != BasisDimension:
                PetscPrint('Basis dimension ' + str(BasisDimension) + ' differs from the dimesion of the sepcified state (' + str(StateDimension) +').')
                sys.exit(1)

            ExpectationValues.append(matutils.ExpectationValue(MyMatrix, State))

        if MyRank == 0:
            if sys.version_info[0] == 3:
                f = open(Prefix + "_ExpectationValue.csv","w", encoding='utf8', newline='')
            elif sys.version_info[0] == 2:
                f = open(Prefix + "_ExpectationValue.csv","wb")
            csv_writer = csv.writer(f)
            for ExpectationValue in ExpectationValues:
                csv_writer.writerow([np.real(ExpectationValue), np.imag(ExpectationValue)])
            f.close()

        PetscPrint("Expectation value:")
        for ExpectationValue in ExpectationValues:
            PetscPrint( str(np.real(ExpectationValue)) + ',' + str(np.imag(ExpectationValue)))




def ReadPseudoPotentialsFile(filename):
    try:
        f = open(filename,'r')
        found = False
        line = 'none'
        while not found and line != '':
            line = f.readline()
            if line.find('Pseudopotentials') == 0:
                found = True

        if found:
            parts = line.split('=')
            pseudopotentials = [np.float(x.strip()) for x in parts[1].split()]
            return pseudopotentials
        else:
            print(('Could not find "Pseudopotentials" keyword in file ' + filename))
            return None

    except IOError as ioeror:
        print(('Error openning file ' + filename))
        return None


class SphereArgsParser:
    """
    Class to parse command line arguments specific to sphere diagonalisation.
    """

    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(description='Diagonalise operators for FQH systems on the sphere.')
        self.Parser.add_argument('--particles', '-p', type=int, default=4, help='Number of particles to consider (default=4).')
        self.Parser.add_argument('--twice-max-lz', '-l', type=int, default=9, help='Twice the maximum Lz number and one less than the total number of flux (default=9).')
        self.Parser.add_argument('--twice-lz', '-z', type=int, default=0, help='Twice the value of Lz.')
        self.Parser.add_argument('--eigenstate',action='store_true', help='Flag to indicate eigenstates should be written to disk.')
        self.Parser.add_argument('--nbr-eigvals', '-n', type=int,default=5, help='Minimum number of eigenvalues to attempt to calculate (default=5).')
        self.Parser.add_argument('--l2-factor', type=float, default=0.0, help='The factor with which to add the L2 operator (default=0.0).')
        group = self.Parser.add_mutually_exclusive_group()
        group.add_argument('--interaction', type=str, default='', help='Name a predefined interaction, loading the appropriate pseudopotentials Vk',choices=['Delta','Coulomb'])

        group.add_argument('--interaction-file', type=str, default='', help='Diagham style file containing pseudopotential values. File should '
                                 + 'contain "Pseudopotentials = " followed by space separated list of pseudopotential values.' )
        group.add_argument('--pseudopotentials', type=float, nargs='+', help='List of pseudopotentials starting from V_0. Any not specified are automatically set to zero.')
        self.Parser.add_argument('--id', default='', help='ID to use to identify output files.')
        self.Parser.add_argument('--prefix', default='', help='Prefix to use for output files.')
        self.Parser.add_argument('--states', type=str, nargs='*', help='State(s) file to calculate expectation value for.')
        self.Parser.add_argument('--states-file', type=str, default='', help='Name of file containing the names of states calculate expectation values for.')
        self.Parser.add_argument('--get-l2value', action='store_true', help='This flag indicates that we should calculate the expectation value of the L2 operator for each eigenstate.')
        self.Parser.add_argument('--force', '-f', action='store_true', help='This flag indicates that one should continue with the computation despite mismatch in number of pseudopotentials (default is False).')
        self.Parser.add_argument('--coefficients-only',action='store_true', help='Flag to indicate that execution should stop after coefficients are calculated.')
        self.Parser.add_argument('--write-coefficients',action='store_true', help='Flag to indicate that coefficient values shoudl be written to disk.')

        self.Parser.add_argument('--initial-states', type=str, nargs='*', help='State(s) to use as initial states for the solver.')
        self.Parser.add_argument('--initial-states-file', type=str, default='', help='Name of file containing the names of the states to use as inital states to the solver.')
        self.Parser.add_argument('--deflation-states', type=str, nargs='*', help='State(s) to use as deflation space.')
        self.Parser.add_argument('--deflation-states-file', type=str, default='', help='Name of file containing the names of the states to use for deflation space.')
        self.Parser.add_argument('--verbose', action='store_true', help='This flag indicates whether or not to be verbose or not.')


    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        PetscPrint = PETSc.Sys.Print
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.Particles = ArgVals.particles
        self.TwiceLzMax = ArgVals.twice_max_lz
        self.TwiceLz = ArgVals.twice_lz
        self.SaveStates = ArgVals.eigenstate
        self.NbrEigvals = ArgVals.nbr_eigvals
        self.NbrStates = self.NbrEigvals
        self.L2Factor = ArgVals.l2_factor
        self.Interaction = ArgVals.interaction
        self.InteractionFile = ArgVals.interaction_file
        self.PseudoPotentials = ArgVals.pseudopotentials
        self.ID = (ArgVals.id if ArgVals.id != '' else 'default')
        self.Prefix = ArgVals.prefix
        self.L2Value = ArgVals.get_l2value
        self.Force = ArgVals.force
        self.Verbose = ArgVals.verbose
        self.WriteCoefficients = ArgVals.write_coefficients
        self.CoefficientsOnly = ArgVals.coefficients_only
        #### Handbreake to stop when halitonian is obtained
        self.ReturnHamtiltonian=False
        self.ReturnBasis=False
        self.ReturnEigenValues=True
        self.ReturnEigenVectors=False
        
        # check if any states given
        self.StateNames = get_state_list(ArgVals.states, ArgVals.states_file, '')
        self.InitialStateNames = get_state_list(ArgVals.initial_states, ArgVals.initial_states_file, 'initial')
        self.DeflationStateNames = get_state_list(ArgVals.deflation_states, ArgVals.deflation_states_file, 'deflation')

        ###Test that there are no unknown options
        if(len(self.Unknown)>1): ###First unknown unknown option is the filename
            print(("There are unknown options (the first is always present)"))
            print((self.Unknown))
            print(("Please try again. See the help for a list of valid options"))
            sys.exit(1)

        

def get_state_list(states, states_file, key):
    """
    Function to extract list of states from command line list arguement and filename arguement. If both are
    given an error is given. If list is valid, then this is returned, otherwise the file is loaded into a list
    and this is returned.

    Parameters
    ----------
    states: list
        List of arguements or None.
    states_fiel: str
        Name of file containing staets.
    key: str
        Identifying string for states.

    Returns
    --------
    list
        List of the filenames of the states.
    """
    PetscPrint = PETSc.Sys.Print
    if states is not None and states_file != '':
        PetscPrint('Must only specify ' + key + ' states or ' + key + ' states file, but not both.')
        sys.exit(1)
    if states is not None:
        return states
    elif states_file != '':
        states = []
        try:
            f = open(states_file, 'r')
            line = f.readline()
            while line:
                states.append(line.rstrip())
                line = f.readline()
            f.close()
        except IOError:
            PetscPrint('Error openning file: ' + states_file + '.')
            sys.exit(1)
        return states
    else:
        return []


if __name__ == '__main__':
    __main__(sys.argv)
