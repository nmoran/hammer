#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

"""FQHSphereWF.pyx: Cython code for calculating correlations functions on sphere and torus.

__author__      = "Niall Moran,Mikael Fremling"
__copyright__   = "Copyright 2017"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com,micke.fremling@gmail.com"
"""

import petsc4py.PETSc as PETSc

import numpy as np
cimport numpy as np
import scipy.special as sps

cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)

cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double cimag(double complex x) nogil
    double complex conj(double complex x) nogil

def Rho(WFN, xy, Basis, State):
    """
    Method to calculate the density of the provided state, basis , WFn and (geometry specific) cooridnates
    """
    ###cdef double val = 0
    cdef int p = 0 ###Enumerate orbitals
    cdef int i
    cdef int dim = Basis.GetDimension()
    cdef rstart, rend
    cdef int NbrFlux = WFN.GetNumOrbitals()
    rstart, rend = State.getOwnershipRange()
    if hasattr(xy, "__len__"): ##Array
        Elems=len(xy)
    else:
        Elems=1
    val = np.zeros(Elems, dtype=np.float64)
    ValueBuffer = np.zeros(Elems, dtype=PETSc.RealType)
    ValueBufferRecv = np.zeros(Elems, dtype=PETSc.RealType)
    
    while p < NbrFlux:
        factor1 = WFN.WFValue(xy, p)
        factor1 = factor1 * np.conj(factor1)
        i = rstart
        while i < rend:
            if Basis.N(p,i) == 1.0:
                val += np.real(factor1 * (State.getValue(i) * conj(State.getValue(i))))
            i += 1
        p += 1

    ValueBuffer = val
    PETSc.COMM_WORLD.tompi4py().Allreduce(ValueBuffer, ValueBufferRecv)

    if Elems==1:
        return ValueBufferRecv[0]
    else:
        return ValueBufferRecv


def TwoBody(WFN, z1,z2, Basis, State):
    ###Here we have four differnt behaviours depending on if we have scalar/vector - vectors/scalar
    ArrayZ1=hasattr(z1, "__len__")
    ArrayZ2=hasattr(z2, "__len__")
    if ArrayZ1:
        Elems=len(z1)
        TwoPOut=np.zeros(Elems,dtype=np.complex128)
        if ArrayZ2:
            ###If both are arrays make sure they have same length then evaluate for pairs of elements 
            assert(len(z1)==len(z2))
            for indx in range(Elems):
                TwoPOut[indx]=DoTwoBody(WFN, z1[indx],z2[indx], Basis, State)
        else:
            for indx in range(Elems):
                TwoPOut[indx]=DoTwoBody(WFN, z1[indx],z2, Basis, State)
    else:
        if ArrayZ2:
            Elems=len(z2)
            TwoPOut=np.zeros(Elems,dtype=np.complex128)
            for indx in range(Elems):
                TwoPOut[indx]=DoTwoBody(WFN, z1,z2[indx], Basis, State)
        else:
            TwoPOut=DoTwoBody(WFN, z1,z2, Basis, State)
    return TwoPOut

def DoTwoBody(WFN, z1, z2, Basis, State):
    """
    Calculate the two body correlator between points z1 and z2.
    """

    cdef double complex val = 0.0
    # precompute values needed.
    cdef int NbrFlux = WFN.GetNumOrbitals()

    WFsZ1s = np.zeros(NbrFlux, dtype=np.complex128)
    WFsZ2s = np.zeros(NbrFlux, dtype=np.complex128)
    for p in range(NbrFlux):
        WFsZ1s[p] = WFN.WFValue(z1, p)
        WFsZ2s[p] = WFN.WFValue(z2, p)

    rhoz = np.real(WFN.Rho(z1, Basis, State))

    Particles = Basis.GetBasisDetails()['Particles']

    TmpVec = State.duplicate()

    for q in range(NbrFlux-1):
        for p in range(q+1, NbrFlux):
            factor1 = conj(WFsZ1s[p] * WFsZ2s[q])
            factor2 = conj(WFsZ2s[p] * WFsZ1s[q])
            for s in range(NbrFlux-1):
                r = (p + q - s + 2 * NbrFlux) % NbrFlux
                if r > s:
                    factor3 = WFsZ1s[r] * WFsZ2s[s]
                    factor4 = WFsZ2s[r] * WFsZ1s[s]
                    Op = Basis.TwoBodyOperator(p, q, r, s, Basis)
                    Op.mult(State, TmpVec)
                    val += (factor1 - factor2)*(factor3 - factor4) * State.dot(TmpVec)

    return -val / rhoz
