#! /usr/bin/env python

""" ProductWF.py: Script to take the product of provided vectors. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

from petsc4py import PETSc
import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils
from hammer.FQHBasisManager import FQHBasisManager
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
import hammer.FQHFileManager as FileName

def OccupationNorm(Occupation):
    """
    Function to calculate the normal of a given configuration. Given by the product of square roots of the factorials of the occupation counts.
    """
    Norm = 1.0
    for count in Occupation:
        for j in range(1,count+1):
            Norm *= np.sqrt(j)
    return Norm


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to calculate the inner product of vectors.')
    Parser.add_argument('--states', type=str, nargs='*')
    Parser.add_argument('--output', '-o', type=str, default='')
    Parser.add_argument('--verbose','-v', action='store_true')
    Parser.add_argument('--ascii', action='store_true')
    Parser.add_argument('--id', type=str, default='Product')

    [Args, Unknown] = Parser.parse_known_args(sys.argv)
    Verbose = Args.verbose
    WriteASCII = Args.ascii
    Id = Args.id

    if Args.states is None or len(Args.states) < 2:
        print("Two states must be specified.")
        return

    Bases = list()
    TotalParticles = 0
    TotalFlux = 0
    TotalMomentum = 0
    AllBosonic = True
    States = list()
    for State in Args.states:
        BasisManager = FQHBasisManager()
        BasisManager.SetTorusDetailsFromFilename(State)
        Basis = BasisManager.GetBasis()
        TotalParticles += BasisManager.Particles
        TotalMomentum += BasisManager.Momentum
        AllBosonic = AllBosonic and BasisManager.Bosonic
        if TotalFlux == 0: TotalFlux = BasisManager.NbrFlux
        if TotalFlux != BasisManager.NbrFlux: print("Flux of all states must be equal.")
        States.append(vectorutils.ReadBinaryPETScVector(State))
        Bases.append(Basis)

    TotalMomentum = TotalMomentum % TotalFlux
    FinalBasis = FQHTorusBasisBosonic(TotalParticles, TotalFlux, TotalMomentum)
    FinalBasis.GenerateBasis()
    if not AllBosonic:
        print("Can only handle Bosonic states (for now).")

    D = BosonicProductWF(States, Bases, FinalBasis)

    if Args.output == '' :
        OutputFilename = FileName.TorusBasisData(True,TotalParticles,TotalFlux,TotalMomentum,-1,-1,Id)
        if WriteASCII:
            OutputFilename += ".csv"
        else:
            OutputFilename += ".vec"
    else:
        OutputFilename = Args.output

    if WriteASCII:
        vectorutils.writeASCIIPETScFile(D, OutputFilename)
    else:
        vectorutils.writeBinaryPETScFile(D, OutputFilename)



def BosonicProductWF(States, Bases, FinalBasis, Normalise=True):
    """
    Function to calculate the wave-function products for bosonic wave-functions.
    """

    # Create an empty output vector
    D = PETSc.Vec()
    D.create()
    D.setSizes(FinalBasis.GetDimension())
    D.setUp()
    D.zeroEntries()

    if len(States) == 3:
        # Now populate the entries
        for i in range(Bases[0].GetDimension()):
            Occupation1 = Bases[0].GetOccupationRepArray(i)
            Norm1 = OccupationNorm(Occupation1)
            Coeff1 = States[0].getValue(i)
            for j in range(Bases[1].GetDimension()):
                Occupation2 = Bases[1].GetOccupationRepArray(j)
                Norm2 = OccupationNorm(Occupation2)
                Coeff2 = States[1].getValue(j)
                for k in range(Bases[2].GetDimension()):
                    Occupation3 = Bases[2].GetOccupationRepArray(k)
                    Norm3 = OccupationNorm(Occupation3)
                    Coeff3 = States[2].getValue(k)
                    Occupation = Occupation1 + Occupation2 + Occupation3
                    Norm = OccupationNorm(Occupation)
                    Coeff = Coeff1 * Coeff2 * Coeff3 * Norm / (Norm1 * Norm2 * Norm3)
                    FinalBasis.SetMonomialRep2(Occupation)
                    idx = FinalBasis.BinaryArraySearchDescending()

                    if idx >= 0:
                        D.setValue(idx, Coeff, True)
    elif len(States) == 2:
        # Now populate the entries
        for i in range(Bases[0].GetDimension()):
            Occupation1 = Bases[0].GetOccupationRepArray(i)
            Norm1 = OccupationNorm(Occupation1)
            Coeff1 = States[0].getValue(i)
            for j in range(Bases[1].GetDimension()):
                Occupation2 = Bases[1].GetOccupationRepArray(j)
                Norm2 = OccupationNorm(Occupation2)
                Coeff2 = States[1].getValue(j)
                Occupation = Occupation1 + Occupation2
                Norm = OccupationNorm(Occupation)
                Coeff = Coeff1 * Coeff2 * Norm / (Norm1 * Norm2)
                FinalBasis.SetMonomialRep2(Occupation)
                idx = FinalBasis.BinaryArraySearchDescending()
                if idx >= 0:
                    D.setValue(idx, Coeff, True)

    norm = D.norm()
    if norm > 0.0 and Normalise:
        D.scale(1.0/norm)

    return D


if __name__ == '__main__' :
    __main__()
