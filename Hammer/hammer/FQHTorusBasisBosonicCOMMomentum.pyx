#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusBasisBosonicCOMMomentum.pyx: This is a cython implementation of code to work with the FQH bosonic basis on a Torus taking into account centre of mass symmetry. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
cimport numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
from petsc4py import PETSc
from hammer.FQHTorusBasisBosonic cimport FQHTorusBasisBosonic


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)

cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil

cdef class FQHTorusBasisBosonicCOMMomentum(FQHTorusBasisBosonic):
#    cdef int COMMomentum
#    cdef int Distance # amount by which to translate each time.
#    cdef int Sectors # number of translations to get all the way around.
#    cdef long Mask # mask which covers all the flux in the system.
#    cdef double complex *Phases # array to hold pre computed phases to save time.
#    cdef long TmpRepresentative

    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.Distance = 0
        self.COMMomentum = -1
        self.Sectors = 0


    def __init__(self,int Particles, int NbrFlux, int Momentum, int COMMomentum):
        """
        Set the variables. Inputs are:
        Particles: Number of particles in the basis.
        NbrFlux: Number of flux quanta to consider.
        Momentum: The total momentum, sum of the orbital angular momentum of each particle.
        COMMomentum: The center of mass momentum. If -1 then do not use this symmetry.
        """
        super().__init__(Particles, NbrFlux, Momentum)
        self.COMMomentum = COMMomentum

        # work out the translation distance under which the Hamiltonian is invariant and the number of sectors.
        # If the number of particles does not divide into the number of flux, then keep checking larger and larger multiples of the nubmer of flux.
        n = 1
        while n < self.Particles:
            if ((n * self.NbrFlux) % self.Particles) == 0:
                break;
            else:
                n += 1

        # now set the translation distance.
        self.Distance = 1
        if n < self.Particles and ((n * self.NbrFlux) % self.Particles) == 0 :
            self.Distance = (n * self.NbrFlux) / self.Particles
        else:
            self.Distance = self.NbrFlux

        # number of sectors is how many of these translations fit around the torus.
        self.Sectors = self.NbrFlux / self.Distance

        # now pre-assign masks to make the translations faster.
        self.Mask = (1l << self.EffectiveFlux) - 1l

        # precompute phases to be used.
        self.Phases = <double complex*>PyMem_Malloc(self.Sectors * sizeof(double complex))
        for i in range(self.Sectors):
            self.Phases[i] = cexp(1j * 2.0 * np.pi * np.float(i) * np.float(self.COMMomentum) / np.float(self.Sectors))


    cdef int FindBasisDimension(self):
        """
        Calculates the basis dimension for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasisDimension to calculate the dimension of the basis.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int dim = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        cdef int i = 0
        for i in np.arange(0, self.Particles):
            MaxMomentum += (self.EffectiveFlux - 1 -i)
        i = 0
        while (self.EffectiveMomentum + i*(self.NbrFlux + self.FluxOffset)) <= MaxMomentum:
            dim += self.FindSubBasisDimensionCOMMomentum(self.EffectiveMomentum + i*(self.NbrFlux + self.FluxOffset), self.EffectiveFlux, self.Particles, 0l)
            i+=1
        return dim


    cdef int FindSubBasisDimensionCOMMomentum(self, int J, int Nphi, int Ne, long base):
        """
        Recursive function to find the basis dimension for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int dim = 0

        if (Ne == 0):
            if (J == 0) and self.IsRepresentativeConfiguration(base):
                return 1
            else:
                return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                dim += self.FindSubBasisDimensionCOMMomentum(J-(Nphi-1), Nphi-1, Ne-1, base + (1l << (Nphi-1)))
            #elif J >= 0:
            #    dim += self.FindSubBasisDimension(J, Nphi-1, Ne)
            dim += self.FindSubBasisDimensionCOMMomentum(J, Nphi-1, Ne, base)
        return dim


    def FindBasis(self):
        """
        Calculates the basis elements for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasis to calculate the basis elements and populate the BasisElements array.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, self.Particles):
            MaxMomentum += (self.EffectiveFlux - 1 - i)

        cdef int MaxTotMomentum = (self.NbrFlux + self.FluxOffset) * (MaxMomentum / (self.NbrFlux + self.FluxOffset)) + self.EffectiveMomentum

        return self.FindSubBasis(MaxTotMomentum, self.NbrFlux + self.FluxOffset, self.EffectiveFlux, self.Particles, idx, 0l)


    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis elements for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0) and self.IsRepresentativeConfiguration(base):  # this is used instead of the standard modulus operator
                self.BasisElements[idx] = base
                return 1
            else:
                return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasis(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))
            #elif J >= 0:
            #    idx += offset
            #    offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne, idx, base)
            idx += offset
            offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne, idx, base)
        return offset


    cpdef int IsRepresentativeConfiguration(self, long State):
        """
        Method to check if the configuration passed is a representative configuration for case with even numbers of particles.
        """
        cdef long TranslatedState
        cdef double complex Normal = 0.0

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(State, i)
            if TranslatedState < State:
                return 0
            if TranslatedState == State:
                Normal += self.Phases[i]
            i += 1

        if cabs(Normal) > 1e-10:
            return 1
        else:
            return 0


    cpdef long SimpleTranslate(self, long State, int Steps):
        """
        Function that translates the given configuration by t orbitals.

        For bosons the algoritm is as follows
        1. Iterate starting at lowest bit of fermionic representation until "(Steps * Distance)" orbitals have been counted (each zero corresponds to orbital).
        2. Translate the state by the amount of bits that have been iterated over.
        """
        cdef int pos = 0
        cdef int orbitals = 0
        cdef int orbitals_target = Steps * self.Distance

        while pos < self.EffectiveFlux and orbitals < orbitals_target:
            if (State & (1l << pos)) == 0l:
                orbitals += 1
            pos += 1

        # now translate by pos orbitals
        return ((State >> pos) + (State << (self.EffectiveFlux - pos + 1))) & self.Mask


    cdef double GetNormalIdx(self, Idx):
        """
        Given the index of a basis element, calculate the normal
        """
        cdef double complex Normal = 0.0
        cdef long TranslatedState

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.BasisElements[Idx], i)
            if TranslatedState == self.BasisElements[Idx]:
                Normal += self.Phases[i]
            i += 1

        return sqrt(creal(Normal))


    cdef double GetNormal(self):
        """
        Get the normal of the configuration stored in self.TmpState2
        """
        cdef double complex Normal = 0.0
        cdef long TranslatedState

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.TmpState2, i)
            if TranslatedState == self.TmpState2:
                Normal += self.Phases[i]
            i += 1

        return sqrt(creal(Normal))



    cdef long GetRepresentative(self):
        """
        Try to find the index of the representative of element TmpState2
        """
        cdef long RepState = self.TmpState2
        cdef long TranslatedState

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.TmpState2, i)
            if TranslatedState < RepState:
                RepState = TranslatedState
            i+=1

        self.TmpRepresentative = RepState

        return RepState


    cdef double complex GetRepresentativePhase(self):
        """
        Try to find phase relating the configuration in tmpstate2 and its representative configuration.
        """
        cdef double complex Phase = 0.0
        cdef long TranslatedState

        i = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(self.TmpState2, i)
            if TranslatedState == self.TmpRepresentative:
                Phase += self.Phases[i]
            i+=1

        return Phase


    cpdef int BinaryArraySearchDescending(self):
        """
        Function to perform binary search on array of BasisElements for TmpState2
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef long l = self.Dim

        cdef long start = 0
        cdef long end = l
        cdef long dist = end - start
        cdef long test_item
        cdef long item = self.TmpRepresentative
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply density operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx.
        """
        self.TmpRepresentative = self.BasisElements[Idx]
        self.TmpState2 = self.BasisElements[Idx]
        return  self.GetRepresentativePhase() * FQHTorusBasisBosonic.NN(self,Orbital1, Orbital2, Idx) / (self.GetNormalIdx(Idx) * np.conj(self.GetNormalIdx(Idx)))


    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply density operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx.
        """
        self.TmpRepresentative = self.BasisElements[Idx]
        self.TmpState2 = self.BasisElements[Idx]
        return  self.GetRepresentativePhase() * FQHTorusBasisBosonic.NNN(self,Orbital1, Orbital2, Orbital3, Idx) / (self.GetNormalIdx(Idx) * np.conj(self.GetNormalIdx(Idx)))


    cpdef double AA(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx and store the result in TmpState.
        TODO: investigate popcnt implementation to make this more efficient.
        """
        return  FQHTorusBasisBosonic.AA(self,Orbital1, Orbital2, Idx) / self.GetNormalIdx(Idx)


    cpdef double complex AdAd(self, int Orbital1, int Orbital2):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.
        """
        cdef double complex Phase, Normal
        Phase = FQHTorusBasisBosonic.AdAd(self,Orbital1, Orbital2)
        self.TmpState2 = self.GetFermionicRep2()
        self.GetRepresentative()
        Phase *= self.GetRepresentativePhase()
        Normal = self.GetNormal()
        if Normal == 0:
            return 0.0
        else:
            return  Phase / Normal


    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1, Orbital2 and Orbital3 to basis elements stored at index Idx and store the result in TmpState.
        """
        return  FQHTorusBasisBosonic.AAA(self,Orbital1, Orbital2, Orbital3, Idx) / self.GetNormalIdx(Idx)


    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.
        """
        cdef double complex Phase, Normal
        Phase = FQHTorusBasisBosonic.AdAdAd(self,Orbital1, Orbital2, Orbital3)
        self.TmpState2 = self.GetFermionicRep2()
        self.GetRepresentative()
        Phase *= self.GetRepresentativePhase()
        Normal = self.GetNormal()
        if Normal == 0:
            return 0.0
        else:
            return  Phase / Normal


    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1, Orbital2, Orbital3 and Orbital4 to basis elements stored at index Idx and store the result in TmpState.

        """
        return  FQHTorusBasisBosonic.AAAA(self,Orbital1, Orbital2, Orbital3, Orbital4, Idx) / self.GetNormalIdx(Idx)


    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4):
        """
        Apply creation operators on sites with momentum Orbital1 ,Orbital2, Orbital3 and Orbital4 to state TmpState and store result in TmpState2.
        """
        cdef double complex Phase, Normal
        Phase = FQHTorusBasisBosonic.AdAdAdAd(self,Orbital1, Orbital2, Orbital3, Orbital4)
        self.TmpState2 = self.GetFermionicRep2()
        self.GetRepresentative()
        Phase *= self.GetRepresentativePhase()
        Normal = self.GetNormal()
        if Normal == 0:
            return 0.0
        else:
            return  Phase / Normal


    cpdef GetPhases(self):
        """
        Simple accessor method to return the array of phases.
        """
        phases = np.zeros(self.Sectors, dtype=np.complex128)
        for i in range(self.Sectors):
            phases[i] = self.Phases[i]

        return phases


    cpdef GetNumberSectors(self):
        """
        Simple accessor method to return the number of momentum sectors.
        """
        return self.Sectors


    def GetBasisDetails(self):
        """
        Returns a dictionary object with parameters of the basis.
        """
        Details = dict()
        Details['Particles'] = self.Particles
        Details['NbrFlux'] = self.NbrFlux
        Details['Momentum'] = self.Momentum
        Details['COMMomentum'] = self.COMMomentum
        Details['Bosonic'] = True
        Details['Sectors'] = self.Sectors
        Details['Distance'] = self.Distance
        Details['EffectiveFlux'] = self.EffectiveFlux
        return Details


    cpdef ConvertToFullBasis(self, State, FullBasis=None):
        """
        This method expands the state in the reduced centre of mass momentum basis to the full basis.

        Parameters
        -----------
        State: vector
            Vector of Fock coefficients.
        FullBasis: basis object
            Basis object of full basis, if None will be computed (default=None).

        Returns
        ---------
        basis object
            The basis for the full system.
        vector
            Vector of Fock coefficients for the full state.
        """
        if FullBasis is None:
            FullBasis = FQHTorusBasisBosonic(self.Particles, self.NbrFlux, self.Momentum)
            FullBasis.GenerateBasis()

        cdef int BasisDim = self.Dim
        cdef int FullBasisDim = FullBasis.GetDimension()
        cdef int rstart, rend, Idx
        rstart, rend = State.getOwnershipRange()
        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)

        if (self.Sectors) > (BasisDim-1):
            Op = PETSc.Mat(); Op.createDense([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            Op.setUp()
        else:
            Op = PETSc.Mat(); Op.create()
            Op.setSizes([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            #Op.setOption(PETSc.Vec.Option.IGNORE_NEGATIVE_INDICES, 1)
            nnz = nnz * self.Sectors
            Op.setPreallocationNNZ((nnz, nnz)) # one term per sector
            Op.setUp()

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(FullBasisDim)
        NewState.setUp()

        cdef int j, i = 0
        cdef double complex Phase
        cdef double Normal
        cdef long TranslatedState
        cdef double OverallNorm = np.sqrt(self.Sectors)

        i = rstart
        while i < rend:
            j = 0
            while j < self.Sectors:
                TranslatedState = self.SimpleTranslate(self.GetElement(i), j)
                Phase = self.Phases[j]
                Normal = self.GetNormalIdx(i)
                Idx = FullBasis.BinaryArraySearchDescendingElement(TranslatedState)
                if Idx >= 0 and Idx < FullBasisDim:
                    Op.setValue(i, Idx, Phase/(Normal * OverallNorm), True)
                j += 1
            i += 1
        Op.assemble()

        Op.multTranspose(State, NewState)

        return [FullBasis, NewState]

