#!/usr/bin/python

""" FQHTorusRange.py: Some code to manage torus object (not maintained). """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"



class FQHTorusRange:
    def __init__(self):
        self.NumTori = 0
        self.TorusObjs = list()
        self.NumberCoefficients = 0
        self.ScaleFactors = list()
        self.Prefix = 'foo'
        self.XLabel = ''


    def AddTorus(self,TorusObj):
        self.TorusObjs.append(TorusObj)
        self.NumTori += 1
        self.NumberCoefficients = TorusObj.CoefficientCount()
        self.ScaleFactors.append(max(TorusObj.L1, TorusObj.L2))


    def AddXAxis(self,XAxis, scale = 'log'):
        self.XAxis =  XAxis
        self.scale = scale


    def AddXAxisLabel(self,XLabel):
        self.XLabel = XLabel


    def GetCoefficientsByIndex(self, Idx, antisym=False):
        vals = list()
        for Torus in self.TorusObjs:
            vals.append(Torus.GetCoefficientByIndex(Idx,antisym))
        return vals


    def GetSeparationByIndex(self, Idx):
        Torus = self.TorusObjs[0]
        return Torus.GetSeparationByIndex(Idx)


    def GetHoppingByIndex(self, Idx):
        Torus = self.TorusObjs[0]
        return Torus.GetHoppingByIndex(Idx)


    def AddPrefix(self, Prefix):
        self.Prefix = Prefix
