#! /usr/bin/env python

""" TorusCorrelations.py: Script the calculate one and two point correlators for torus wave-funtions. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys, slepc4py
import string
slepc4py.init(sys.argv)

from petsc4py import PETSc
from slepc4py import SLEPc

import sys
import argparse
import numpy as np
import time
import csv
from fractions import gcd
import h5py


# Custom modules
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisBosonic import FQHTorusBasisBosonic
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusHamiltonian import FQHTorusHamiltonian
from hammer.HammerArgs import HammerArgsParser
from hammer.FQHTorusWF import FQHTorusWF
import hammer.vectorutils as vecutils
from hammer.FQHBasisManager import FQHBasisManager
import hammer.matrixutils as matutils


def __main__(argv):
    # Run the argument parser and process command line arguments.
    Opts = CorrelationsArgsParser()
    Opts.parseArgs(argv)
    TorusCorrelations(Opts)

def TorusCorrelations(Opts):
    PetscPrint = PETSc.Sys.Print

    # read in states
    State = vecutils.ReadBinaryPETScVector(Opts.State)
    BasisDimension = State.getSize()
    BasisManager = FQHBasisManager()
    BasisManager.SetTorusDetailsFromFilename(Opts.State)
    Basis = BasisManager.GetBasis(Verbose=Opts.Verbose)

    if BasisManager.COMMomentum >= 0:
        [FullBasis, FullState] = Basis.ConvertToFullBasis(State)
        State = FullState
        Basis = FullBasis

    # Create the WF object
    WF = FQHTorusWF(BasisManager.NbrFlux, BasisManager.Tau, Opts.LL)

    # compute the correlations
    [G, Integral] = WF.TwoBodyArrayUsingOperators(Opts.z1, Basis, State, Opts.GridPoints)
    [Rho, RhoIntegral] = WF.RhoArray(Basis, State, Opts.GridPoints)
    if Opts.Verbose: PetscPrint("Two point sum: " + str(Integral) + ', density sum: ' + str(RhoIntegral))

    if Opts.MixTranslations:
        Cells = gcd(BasisManager.Particles, BasisManager.NbrFlux)
        Sectors = BasisManager.NbrFlux/Cells
        if Opts.Verbose: PetscPrint('Considering other ' + str(Sectors - 1) + ' states. These are in momentum sectors ' + str(Cells) + '  apart.')
        for i in range(1, Sectors):
            [NewBasis, NewState] = Basis.TranslateState(State, i)
            if Opts.Verbose: PetscPrint('Translated state has details: ' + str(NewBasis.GetBasisDetails()))
            if np.abs(1.0 - NewState.norm()) > 1e-9:
                PetscPrint('Translated state only has norm ' + str(NewState.norm()) + '.')
                return
            [G2, Integral2] = WF.TwoBodyArrayUsingOperators(Opts.z1, NewBasis, NewState, Opts.GridPoints)
            G += G2; Integral += Integral2
            [Rho2, RhoIntegral2] = WF.RhoArray(NewBasis, NewState, Opts.GridPoints)
            if Opts.Verbose: PetscPrint("Two point sum: " + str(Integral2) + ', density sum: ' + str(RhoIntegral2))
            Rho += Rho2; RhoIntegral += RhoIntegral2
        G /= np.float(Sectors); Integral /= np.float(Sectors)
        Rho /= np.float(Sectors); Integral2 /= np.float(Sectors)

    if Opts.Verbose: PetscPrint("Totals: Two point sum: " + str(Integral) + ', density sum: ' + str(RhoIntegral))

    MyRank = PETSc.Comm.getRank(PETSc.COMM_WORLD)
    if MyRank == 0:
        f = h5py.File(Opts.Output + '_TwoPoint.hdf5', 'w')
        dset = f.create_dataset('two_point', (Opts.GridPoints, Opts.GridPoints), dtype=np.complex128)
        dset[:,:] = G[:,:]
        dset.attrs['norm'] = Integral
        dset.attrs['fixed_position'] = Opts.z1
        dset.attrs['Tau'] = BasisManager.Tau
        dset.attrs['L1'], dset.attrs['L2'] = WF.GetTorusSides()
        dset.attrs['Particles'] = BasisManager.Particles
        dset.attrs['NbrFlux'] = BasisManager.NbrFlux
        f.close()

        f = h5py.File(Opts.Output + '_Density.hdf5', 'w')
        dset = f.create_dataset('density', (Opts.GridPoints, Opts.GridPoints), dtype=np.float64)
        dset[:,:] = Rho[:,:]
        dset.attrs['norm'] = RhoIntegral
        dset.attrs['Tau'] = BasisManager.Tau
        dset.attrs['L1'], dset.attrs['L2'] = WF.GetTorusSides()
        dset.attrs['Particles'] = BasisManager.Particles
        dset.attrs['NbrFlux'] = BasisManager.NbrFlux
        f.close()

    if Opts.Plot:
        PlotCorrelations(G, WF, Opts.Output + '_TwoPoint', '$g_2$')
        PlotCorrelations(Rho, WF, Opts.Output + '_Density', '$\rho$')


def PlotCorrelations(G, WF, Prefix, ylabel):
    """
    Given a mesh of correlator values, a WF object and a filename prefix, this function creates plots of the given values.
    """
    MyRank = PETSc.Comm.getRank(PETSc.COMM_WORLD)
    if MyRank == 0:
        # import and set some plotting parameters.
        import matplotlib
        from matplotlib import rc
        matplotlib.use('PDF')
        import matplotlib.pyplot as plt
        #import seaborn
        #seaborn.set_style('whitegrid')

        rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
        ## for Palatino and other serif fonts use:
        #rc('font',**{'family':'serif','serif':['Palatino']})
        rc('text', usetex=True)

        GridPoints = G.shape[0]
        L1, L2 = WF.GetTorusSides()

        # create rectangular plot
        plt.figure()
        im = plt.imshow(np.real(G) * 2.0 * np.pi, origin='lower', extent=(0.0, L1, 0.0, L2), aspect='auto')
        cbar = plt.colorbar()
        cbar.solids.set_rasterized(True)
        cbar.solids.set_edgecolor("face")
        plt.savefig(Prefix + ".pdf")

        # create torus shapped plot
        plt.figure()
        [GData, Extent] = WF.TorusPlotData(np.real(G) * 2.0 * np.pi)
        im = plt.imshow(GData, origin='lower', extent=Extent, aspect='equal')
        cbar = plt.colorbar()
        cbar.solids.set_rasterized(True)
        cbar.solids.set_edgecolor("face")
        plt.savefig(Prefix + "_Aspect.pdf")

        plt.figure()
        XAxis = np.linspace(0.0, L1, GridPoints)
        plt.plot(XAxis, np.real(G[0,:]) * 2.0 * np.pi)
        plt.xlabel('$l_B$')
        #plt.ylabel('$g_2$')
        plt.ylabel(ylabel)
        plt.savefig(Prefix + "_X.pdf")

        plt.figure()
        YAxis = np.linspace(0.0, L2, GridPoints)
        plt.plot(YAxis, np.real(G[:,0]) * 2.0 * np.pi)
        plt.xlabel('$l_B$')
        plt.ylabel(ylabel)
        plt.savefig(Prefix + "_Y.pdf")

        plt.figure()
        XAxis = np.linspace(0.0, np.sqrt(Extent[1]**2 + Extent[3]**2), GridPoints)
        YValues = np.real(np.diag(G)) * 2.0 * np.pi
        plt.plot(XAxis, YValues)
        plt.xlabel('$l_B$')
        plt.ylabel(ylabel)
        plt.savefig(Prefix + "_diag.pdf")


class CorrelationsArgsParser:
    """
    Class to parse command line arguments specific to correlations calculation utility.

    """

    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(description='Calculate correlations of the given torus wave-functions.')
        self.Parser.add_argument('--state', help='The state to use to calculate correlations.', required=True)
        self.Parser.add_argument('--output', '-o', default='', help='Indentifier to use for output filenames. (Default is '
                                 + 'to use the state name with the suffix removed).')
        self.Parser.add_argument('--landau-level', '-n', type=int, default=0, help='The Landau level index to use when calculating correlations (default is LLL (n=0)).')
        self.Parser.add_argument('--mix-translations', '-m', action='store_true', help='Flag to indicate that states the correlations for states in degenerate sectors should also be calcualted and summed.')
        self.Parser.add_argument('--grid-points', '-g', type=int, default=100, help='The number of grid points in each direction to use (default is 100).')
        self.Parser.add_argument('--x1', type=float, default=0.0, help='x location of first particle.')
        self.Parser.add_argument('--y1', type=float, default=0.0, help='y location of first particle.')
        self.Parser.add_argument('--plot', action='store_true', help='Flag to indicate that plots should be created.')
        self.Parser.add_argument('--verbose', action='store_true', help='Flag to indicate verbose output.')


    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.State = ArgVals.state
        self.Output = (ArgVals.output if ArgVals.output != '' else self.State.strip('.vec'))
        self.LL = ArgVals.landau_level
        self.MixTranslations = ArgVals.mix_translations
        self.Verbose = ArgVals.verbose
        self.GridPoints = ArgVals.grid_points
        self.z1 = np.complex(ArgVals.x1, ArgVals.y1)
        self.Plot = ArgVals.plot


if __name__ == '__main__':
    __main__(sys.argv)
