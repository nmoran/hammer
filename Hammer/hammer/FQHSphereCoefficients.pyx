#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHSphereCoefficients.pyx: This is a cython implementation calculate FQH interaction coefficents for the sphere. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys
import numpy as np
cimport numpy as np
import csv
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import scipy.special as special
import cython
from sympy.physics.quantum.cg import CG
import petsc4py.PETSc as PETSc
try:
    import pygsl.testing.sf as pygsl_sf
    HavePYGSL = True
except ImportError:
    HavePYGSL = False


def clebsch_gordan(j1, j2, j3, m1, m2, m3):
    if HavePYGSL:
        return (-1)**(m1+m2)*np.sqrt(2*j3+1.0)*pygsl_sf.coupling_3j_e(int(2*j1), int(2*j2), int(2*j3), int(2*m1), int(2*m2), -int(2*m3))[0]
    else:
        # revert to using sympy which can be slow for large systems
        cg = CG(j1,m1,j2,m2,j3,m3)
        return cg.doit().evalf()


class FQHSphereInteractionCoefficients2Body:
    """
    Class to group methods and data structures for calculating interaction coefficients for FQH systems on the sphere.

    """

    def __init__(self, TwiceLzMax, Bosonic=False, PseudoPotentials=None, double L2Factor=0.0, Lz=None):
        """
        Constructor which sets up the class.

        Parameters
        --------------
        TwiceLzMax: int
            Twice the maximum Lz value.
        Bosonic: flag
            Flag to indicate bosonic case, not yet implemented (default = False).
        PseudoPotentials: list
            List of pseudopotential values. If None given all pseudopotentials are set to 0.
        L2Factor: double
            Factor to use to add L2 operator with.
        Lz: double
            The Lz sector we are in so that we can just add diagonal terms for Lz^2 part of L^2 operator.
        """

        PetscPrint = PETSc.Sys.Print
        self.TwiceLzMax = TwiceLzMax
        self.Ns = self.TwiceLzMax + 1
        self.PseudoPotentials = PseudoPotentials
        if self.PseudoPotentials is None:
            self.PseudoPotentials = [0.0] * self.Ns
            #self.PseudoPotentials[1] = 1.0
        else:
            if len(PseudoPotentials) < self.Ns:
                PetscPrint('Should be ' + str(self.Ns) + ' pseudopotential values, padding with zeros.')
                self.PseudoPotentials = PseudoPotentials + ([0.0] * (self.Ns - len(PseudoPotentials)))
        self.L2Factor = L2Factor
        self.Bosonic = Bosonic
        self.Lz = Lz


    def CalculateCoefficients(self):
        """
        High level function to calculate interaction coefficients and store for later use.
        """

        ExchangePhase = -1.0
        if self.Bosonic == True:
            ExchangePhase = 1.0

        # Now setup the AntiSymmetric coefficients.
        # Do first fully in python to get non zero coefficients and indexing.
        # list for annihilation index operators
        AAOps = list() # list of j3,j4 pairs
        AdAdOps = list() # list of lists of j1,j2 pairs and coefficient values
        NumNonZeros = list() # list of non zeros in at each j3,j4 pair
        self.NumPairs = 0
        self.NumDensity = 0 # the number of density density interactions
        self.Diag = 0.0 # constant to place on diagonal
        NNOps = dict()

        #create table of all Clebsch Gordon coefficients we will need and precalculate
        # here element m1, m1, L will correspond to CG coefficient <Q m1, Q m2 | L m1+m2>
        print "Precalulate Clebsch Gordon coefficients"
        CGTable = np.zeros((self.Ns, self.Ns, self.Ns))
        Q = self.TwiceLzMax/2.0
        for m1_idx in xrange(self.Ns):
            m1 = -Q + m1_idx
            for m2_idx in xrange(self.Ns):
                m2 = -Q + m2_idx
                for L in xrange(self.Ns):
                    CGTable[m1_idx,m2_idx,self.Ns-L-1] = clebsch_gordan(Q,Q,L,m1,m2,m1+m2)
        print "Precalulation complete"
        for j4 in np.arange(0, self.Ns):
            m4 = -Q + j4
            for j3 in np.arange(j4+1, self.Ns):
                m3 = -Q + j3
                AAOps.append([j3,j4])
                count = 0
                Tmp = dict()
                for j2 in np.arange(0, self.Ns):
                    j1 = j3 + j4 - j2
                    if j1 >= 0 and j1 < self.Ns and j1 > j2:
                        m2 = -Q + j2
                        m1 = -Q + j1
                        Coef = 0.0
                        for L in np.arange(self.Ns)[1::2]:
                            Coef +=  -4.0 * CGTable[j1,j2,L] * CGTable[j3,j4,L] * self.PseudoPotentials[L]

                        if np.abs(Coef) > 0.0:
                            if j3 == j1 and j4 == j2: # if it is a density density term, treat it differenctly
                                NNOps[(j1, j2)] = -Coef # negative sign here due to re-ordering of operators.
                                self.NumDensity += 1
                            else:
                                Tmp[(j1,j2)] = Coef
                                count += 1
                AdAdOps.append(Tmp)
                NumNonZeros.append(count)
                if count > 0:
                    self.NumPairs += 1

        # first the L+ L- + L- L+ terms
        if np.abs(self.L2Factor) > 0.0:
            RF = np.zeros(self.Ns) # raising factors
            LF = np.zeros(self.Ns) # lowering factors
            for j in range(self.Ns):
                m = j - Q
                RF[j] = np.sqrt(Q*(Q+1)-m*(m+1))
                LF[j] = np.sqrt(Q*(Q+1)-m*(m-1))

            for r in range(self.Ns-1):
                for l in range(1,self.Ns):
                    if r != l: # as m's will anihilate
                        coef = RF[r] * LF[l]
                        if (r+1) == l:
                            if NNOps.has_key((l,l)):
                                NNOps[(l,l)] += 0.5 * coef * self.L2Factor
                            else:
                                NNOps[(l,l)] = 0.5 * coef * self.L2Factor
                                self.NumDensity +=1

                            if NNOps.has_key((r,r)):
                                NNOps[(r,r)] += 0.5 * coef * self.L2Factor
                            else:
                                NNOps[(r,r)] = 0.5 * coef * self.L2Factor
                                self.NumDensity +=1

                            if NNOps.has_key((l,r)):
                                NNOps[(l,r)] += -coef * self.L2Factor
                            else:
                                NNOps[(l,r)] =  -coef * self.L2Factor
                                self.NumDensity +=1
                        else:
                            try:
                                if l > r:
                                    j1 = l
                                    j2 = r
                                    j3 = l - 1
                                    j4 = r + 1
                                else:
                                    j1 = r
                                    j2 = l
                                    j3 = r + 1
                                    j4 = l - 1
                                idx = AAOps.index([j3,j4])
                                if AdAdOps[idx].has_key((j1,j2)):
                                    AdAdOps[idx][(j1,j2)] += - coef * self.L2Factor
                                else:
                                    AdAdOps[idx][(j1,j2)] = - coef * self.L2Factor
                                    if NumNonZeros[idx] == 0:
                                        self.NumPairs += 1
                                    NumNonZeros[idx] += 1
                            except ValueError:
                                AAOps.append([j3,j4])
                                Tmp = dict()
                                Tmp[(j1,j2)] = - coef * self.L2Factor
                                AdAdOps.append(Tmp)
                                NumNonZeros.append(1)
                                self.NumPairs += 1


            if self.Lz is None:
                for r in range(self.Ns):
                    mr = r - Q
                    for l in range(self.Ns):
                        ml = l - Q
                        if NNOps.has_key((l,r)):
                            NNOps[(l,r)] += mr * ml * self.L2Factor
                        else:
                            NNOps[(l,r)] = mr * ml * self.L2Factor
                            self.NumDensity +=1
            else:
                self.Diag += self.Lz**2 * self.L2Factor

        #Now we create the final arrays
        self.J3J4Vals = np.zeros([self.NumPairs,2],dtype=np.int64)
        self.J1J2Vals = list()
        self.AntiSymCoefs = list()
        self.NonZeros = np.zeros(self.NumPairs,dtype=np.int64)

        Idx = 0
        for i in np.arange(0, AAOps.__len__()):
            if NumNonZeros[i] > 0:
                self.J3J4Vals[Idx,:] = AAOps[i]
                self.J1J2Vals.append(np.zeros([NumNonZeros[i],2],dtype=np.int64))
                self.AntiSymCoefs.append(np.zeros(NumNonZeros[i],dtype=np.complex128))
                self.NonZeros[Idx] = NumNonZeros[i]
                j = 0
                for key in AdAdOps[i].keys():
                    self.J1J2Vals[Idx][j,:] = key[:]
                    self.AntiSymCoefs[Idx][j] = AdAdOps[i][key]
                    j += 1
                Idx += 1

        #Now create final arrays for density density interactions
        self.N1N2Vals = np.zeros([self.NumDensity,2], dtype=np.int64)
        self.DensityCoefs = np.zeros(self.NumDensity, dtype=np.complex128)
        i =0
        for key in NNOps.keys():
            self.N1N2Vals[i,:] = key[:]
            self.DensityCoefs[i] = NNOps[key]
            i += 1



    def WriteAntiSymCoefficients(self,filename):
        """
        Write anti-symmeterised cofficients to a csv file.
        """
        if sys.version_info[0] == 3:
            f = open(filename, "w", encoding='utf8', newline='') # python 3 version
        elif sys.version_info[0] == 2:
            f = open(filename, "wb")
        csv_writer = csv.writer(f)
        for i in np.arange(0,self.NumPairs):
            for j in np.arange(0,self.NonZeros[i]):
                Cols = [self.J1J2Vals[i][j,0], self.J1J2Vals[i][j,1], self.J3J4Vals[i,0], self.J3J4Vals[i,1], self.AntiSymCoefs[i][j].real, self.AntiSymCoefs[i][j].imag]
                csv_writer.writerow(Cols)

        for i in np.arange(0, self.NumDensity):
            Cols = [self.N1N2Vals[i][0], self.N1N2Vals[i][1], self.N1N2Vals[i][0], self.N1N2Vals[i][1], self.DensityCoefs[i].real, self.DensityCoefs[i].imag]
            csv_writer.writerow(Cols)
        f.close()


def VkCoulomb(TwoS):
    from  scipy.special import binom as bn
    #### Implements the coulomb coefficents a la
    ### G. Fano (1986) in Phys.Rev. B,34,2670
    V1=np.zeros((TwoS+1),dtype=float)
    V2=np.zeros((TwoS+1),dtype=float)
    V3=np.zeros((TwoS+1),dtype=float)

    for J in range(TwoS+1):
        V1[TwoS-J]=bn(2*TwoS - 2*J,TwoS-J)
        V2[TwoS-J]=bn(2*TwoS + 2*J+2,TwoS+J+1)
        V3[TwoS-J]=bn(2*TwoS+2,TwoS+1)

    Vj=2*V1*V2/(np.sqrt(TwoS/2.0)*V3**2)
    return Vj
    

def VkDelta(TwoS):
    #### Implements the coulomb coefficents a la
    ### V= 0,1,0,0,0,0,.....
    Vk=np.zeros((TwoS+1),dtype=float)
    Vk[1]=1.0
    return Vk
