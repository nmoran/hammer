#! /usr/bin/env python

""" vectorutils.py: Utility functions for interfacing with vectors returned by Hammer. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


from petsc4py import PETSc
import numpy as np
import numpy.linalg as linalg
import sys

import petsc4py
PETSc_Major_Version = np.int(petsc4py.__version__.split('.')[0])
PETSc_Minor_Version = np.int(petsc4py.__version__.split('.')[1])


def ReadBinaryPETScVector(filename):
    """
    Given the filename of a binary PETSc vector this will read it and return
    a vector object.
    """
    Viewer = PETSc.Viewer()
    Viewer.createBinary(filename,"r")
    Vec = PETSc.Vec().load(Viewer)
    Viewer.destroy()
    return Vec


def ReadASCIIPETScVector(filename):
    """
    Given the filename of an ASCII PETSc vector this will read it and return
    a vector object.
    """
    Viewer = PETSc.Viewer()
    Viewer.createASCII(filename,"r")
    Vec = PETSc.Vec().load(Viewer)
    Viewer.destroy()
    return Vec


def writeBinaryPETScFile(A, filename):
    """
    Write the vector A to 'filename' in binary format.
    """
    Viewer = PETSc.Viewer()
    Viewer.createBinary(name=filename, mode=PETSc.Viewer.Mode.W)
    Viewer.view(A)
    (Viewer.clear() if PETSc_Minor_Version <= 4 else Viewer.clearDraw())
    Viewer.destroy()


def writeHDF5File(A, filename, dataset='fock_coefs'):
    """
    Write the vector A to 'filename' in HDF5 format.

    Parameters
    -----------
    A: PETSc vector
        Vector to write.
    filename: str
        Filename to write to.
    dataset: str
        Name of dataset to use in hdf5 file (default=fock_coefs).
    """
    import h5py

    f = h5py.File(filename, 'w')
    dim = A.getSize()
    dset = f.create_dataset(dataset, (dim,2), dtype=np.float64)
    dset[:,0] = np.real(A.array)
    dset[:,1] = np.imag(A.array)
    f.close()


def writeASCIIPETScFile(A, filename):
    """
    Write the vector A to 'filename' in ascii format.
    """
    Viewer = PETSc.Viewer()
    Viewer.createASCII(name=filename, mode=PETSc.Viewer.Mode.W)
    Viewer.view(A)
    (Viewer.clear() if PETSc_Minor_Version <= 4 else Viewer.clearDraw())
    Viewer.destroy()


def ReadPlainASCIIVector(filename):
    """
    Open vector file that is a list in plain text and create petsc vector object for it.
    """
    f = open(filename, "r")
    line = f.readline()
    Complex = False
    if line.find(',') >= 0:
        Complex = True
    lines = 0
    while line != "":
        lines += 1
        line = f.readline()
    f.close()
    Vec = PETSc.Vec()
    Vec.create()
    Vec.setSizes(lines)
    Vec.setUp()

    f = open(filename, "r")
    line = f.readline()
    lines = 0
    if not Complex:
        while line != "":
            Vec.setValue(lines, np.float(line))
            line = f.readline()
            lines += 1
    else:
        while line != "":
            vals = [np.float(x) for x in line.split(',')]
            val = np.complex(vals[0], vals[1])
            #if np.abs(val) > 1e-8: print val
            Vec.setValue(lines, val)
            line = f.readline()
            lines += 1

    f.close()

    Vec.assemble()

    return Vec


def Overlap(State1, State2, Verbose=False, Absolute=False, conjugate=False):
    """
    Given the filenames or two PETSc vectors, this method will read the vectors and return their overlap,
    which is the absolute value of their inner product. Note that it is the first vector that is conjugated.

    Parameters
    ------------
    State1: str or vector
        Filename or vector object of first state.
    State2: str or vector
        Filename or vector object of second state.
    Absolute: bool
        Flag to indicate that absolute value should be returned.
    Conjugate: bool
        Flag to indicate that the conjugate of the second vector should be taken before calculating the overlap.

    Returns
    -------
    scalar
        Overlap (inner-product) between vectors.
    """
    if isinstance(State1, PETSc.Vec):
        A = State1
    else:
        A = ReadBinaryPETScVector(State1)

    if isinstance(State2, PETSc.Vec):
        B = State2
    else:
        B = ReadBinaryPETScVector(State2)

    if conjugate:
        B.conjugate()

    if Verbose:
        if isinstance(State1, PETSc.Vec) or isinstance(State1, PETSc.Vec):
            print(("State 1 dimension: " + str(A.getSize())))
            print(("State 2 dimension: " + str(B.getSize())))
        else:
            print(("State 1: " + State1 + ", Dimension: " + str(A.getSize())))
            print(("State 2: " + State2 + ", Dimension: " + str(B.getSize())))
        print(("Inner product: " + str(B.dot(A))))

    if Absolute:
        return np.abs(B.dot(A))
    else:
        return B.dot(A)


def SpaceOverlap(States1, States2, Verbose=False, Absolute=False):
    """
    Given two lists of filenames or vector objects, this method will find the overlap between the spaces
    spanned by each list. It is assumed that vectors in each list are orthogonal.

    Parameters
    ------------
    States1: list of strs or vectors
        List of filenames or vector objects spanning first space.
    States2: list strs or vectors
        List of filenames or vector objects spanning second space.
    Absolute: bool
        Flag to indicate that absolute value should be returned.

    Returns
    -------
    matrix
        The overlap matrix between lists of states.
    scalar
        Overlap (inner-product) between vector spaces.
    """
    PetscPrint = PETSc.Sys.Print
    dim1 = len(States1)
    dim2 = len(States2)
    overlaps = np.zeros((dim1, dim2), dtype=np.complex128)
    for i in range(dim1):
        if isinstance(States1[i], PETSc.Vec):
            A = States1[i]
        else:
            A = ReadBinaryPETScVector(States1[i])
        for j in range(dim2):
            if isinstance(States2[j], PETSc.Vec):
                B = States2[j]
            else:
                B = ReadBinaryPETScVector(States2[j])

            if A.getSize() != B.getSize():
                PetscPrint("Vector dimensions don't match.")
                sys.exit(-1)
            overlaps[i,j] = B.dot(A)

    if Absolute:
        return np.abs(overlaps), np.abs(np.trace(np.dot(overlaps.T.conj(), overlaps)))
    else:
        return overlaps, np.trace(np.dot(overlaps.T.conj(), overlaps))


def GetGlobalValue(A, Index):
    """
    Since the petsc api does not have the ability to get values from non local portions, we implement ourselves using mpi.
    """
    # find the root rank
    size = PETSc.COMM_WORLD.getSize()
    rank = PETSc.COMM_WORLD.getRank()

    root = 0
    while (Index >= A.owner_ranges[root+1]) and (root < size):
        root += 1

    Val = np.zeros(1, PETSc.ScalarType)
    if root == rank:
        Val[0] = A.getValue(Index)
    else:
        Val[0] = 0

    PETSc.COMM_WORLD.tompi4py().Bcast(Val,root)

    return Val


def RotateToMakeReal(A):
    """
    Apply a global rotation to make the vector real. Not possible in all cases. Uses the phase on the
    vector coefficient with the maximum absolute value.

    Parameters
    -----------
    A: petsc vec
        Vector object to rotate.
    """
    AAbs = A.copy()
    AAbs.abs()
    [MaxIndex,MaxValAbs] = AAbs.max()
    MaxVal = GetGlobalValue(A, MaxIndex)
    ScaleVal = np.exp(np.complex(0.0, -np.angle(MaxVal)))
    A.scale(ScaleVal)
    AAbs.destroy()


def Orthogonalise(Vecs, Verbose=False):
    """
    Given a list of vectors Vecs, this method returns a list of superpositions of these which are
    mutually orthogonal.
    """
    PetscPrint = PETSc.Sys.Print
    Dim = len(Vecs)
    NewVecs = list()
    if Dim > 1 :
        A = np.zeros([Dim, Dim],dtype=np.complex128)
        for i in range(Dim):
            for j in range(Dim):
                A[i,j] = Overlap(Vecs[i], Vecs[j])
                if Verbose:
                    PetscPrint(str(i) + ', ' + str(j) + ': ' +  str(A[i,j]))
        [w, v] = linalg.eig(A)
        # Populate the list of rotated vectors
        for i in range(Dim):
            if Verbose:
                PetscPrint("Eigenvalue " + str(i) + ": " + str(w[i]))
            NewVec = Vecs[i].duplicate()
            NewVec.zeroEntries()
            for j in range(Dim):
                if Verbose:
                    PetscPrint(str(v[j,i].conjugate()) + ',')
                NewVec.axpy(v[j,i].conjugate(), Vecs[j])
            if Verbose:
                PetscPrint('\n')
            NewVec.normalize()
            NewVecs.append(NewVec)

        if Verbose:
            PetscPrint('Quick check to see if we are sane.\n')
            for i in range(Dim):
                for j in range(Dim):
                    PetscPrint(str(i) + ', ' + str(j) + ': ' +  str(Overlap(NewVecs[i], NewVecs[j])))
    elif Dim == 1:
        NewVecs.append(Vecs[0])

    return NewVecs


def OrthogonaliseGramSchmidt(Vecs):
    """
    Given a list of non-orthogonal vectors, this method returns a list of orthogonal vectors obtained using
    the Gram Schmidt process.

    Parameters
    ----------
    Vecs: list of PETSc Vectors
        Vectors to orthogonalise.
    """
    NewVecs = list()
    for j in range(len(Vecs)-1, -1, -1):
        A = Vecs[j].copy()
        A.normalize()
        for i in range(j):
            c = Overlap(A, Vecs[i])
            A.axpy(-c, Vecs[i])
        A.normalize()
        NewVecs.insert(0,A)

    return NewVecs


def WriteFockCoefficients(State, Filename):
    """
    Method to write the specified state to the provided filename.
    Format is two space separated columns with real part in the first and imaginary in the second.
    """

    # we start off writing on the process with rank zero, use barrier to synchronise processes.
    for i in range(PETSc.COMM_WORLD.Get_size()):
        if PETSc.COMM_WORLD.Get_rank() == i:
            if PETSc.COMM_WORLD.Get_rank() == 0:
                f = open(Filename, "w")
            else:
                f = open(Filename, "a")

            values = State.array
            for j in range(len(values)):
                f.write(str(np.real(values[j])) + "   " + str(np.imag(values[j])) + "\n")
            f.close()
        PETSc.COMM_WORLD.barrier()


def Superposition(States, Coefficients):
    """
    Calculate the superposition of the provided states using the supplied coefficients.
    """
    NbrStates = len(States)
    Superposition = None

    if isinstance(States[0], PETSc.Vec):
        Superposition = States[0].copy()
        Superposition.scale(Coefficients[0])
    else:
        Superposition = ReadBinaryPETScVector(States[0])
        Superposition.scale(Coefficients[0])

    for StateIdx in range(1, NbrStates):
        if isinstance(States[StateIdx], PETSc.Vec):
            Superposition.axpy(Coefficients[StateIdx], States[StateIdx])
        else:
            State = ReadBinaryPETScVector(States[StateIdx])
            Superposition.axpy(Coefficients[StateIdx], State)

    return Superposition


def RotateVector(State):
    """
    Rotate the vector such that the coefficient with maximum value is real.

    Parameters
    State: Petsc vector
        Vector to rotate.
    """
    StateAbs = State.copy()
    StateAbs.abs()
    [MaxIndex,MaxValAbs] = StateAbs.max() 
    MaxVal = GetGlobalValue(State, MaxIndex)  
    ScaleVal = np.exp(np.complex(0.0, -np.angle(MaxVal)))
    State.scale(ScaleVal)
    StateAbs.destroy()

