#! /usr/bin/env python

""" ShowBasisSphere.py: Utility to access the basis building functionality of Hammer for the sphere. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys, slepc4py
import string
slepc4py.init(sys.argv)

from petsc4py import PETSc
from slepc4py import SLEPc

# Custom modules
from hammer.FQHSphereBasis import FQHSphereBasis
import hammer.vectorutils as vecutils
import hammer.matrixutils as matutils

# System modules
import sys
import argparse
import numpy as np
import time
import csv


def __main__(argv):
    # Run the argument parser and process command line arguments.
    ShowBasisOpts = ShowBasisArgsParser()
    ShowBasisOpts.parseArgs(argv)
    ShowBasis(ShowBasisOpts)


def ShowBasis(Opts):
    PetscPrint = PETSc.Sys.Print
    # Construction of basis.
    start = time.time()
    MyBasis = FQHSphereBasis(Opts.Particles, Opts.TwiceLzMax, Opts.TwiceLz)
    MyBasis.GenerateBasis(DimensionOnly=Opts.DimensionOnly)
    end = time.time()

    if Opts.DimensionOnly:
        PetscPrint(str(MyBasis.GetDimension()))
    else:
        PetscPrint('Basis dimension: ' + str(MyBasis.GetDimension()) + ', built in ' + str(end-start) + ' seconds.')

    dim = MyBasis.GetDimension()
    if not Opts.DimensionOnly and Opts.Verbose:
        for i in range(0,dim):
            PetscPrint(MyBasis.GetOccupationRep(i))


class ShowBasisArgsParser:
    """
    Class to parse command line arguments specific to show basis utility.

   """

    def __init__(self):
        """
        Constructor method which creates parser object and sets the command line arguments that are available as well as their defaults.
        """
        self.Parser = argparse.ArgumentParser(description='Show basis for FQH systems on the sphere.')
        self.Parser.add_argument('--particles', '-p', type=int, default=4, help='Number of particles to consider (default=4).')
        self.Parser.add_argument('--twice-max-lz', '-l', type=int, default=9, help='Twice the maximum Lz number and one less than the total number of flux (default=9).')
        self.Parser.add_argument('--twice-lz', '-z', type=int, default=0, help='Twice the value of Lz.')
        self.Parser.add_argument('-d','--dimension-only', action='store_true', help='Only show the dimension and exit.')
        self.Parser.add_argument('-r','--representative', action='store_true', help='Don\'t count representatives.')
        self.Parser.add_argument('-v','--verbose', action='store_true', help='Show configurations.')


    def parseArgs(self,argv):
        """
        Method to parse the arguments in the array argv and store them in internal class members.
        """
        [ArgVals, self.Unknown] = self.Parser.parse_known_args(argv)
        self.Particles = ArgVals.particles
        self.TwiceLzMax = ArgVals.twice_max_lz
        self.TwiceLz = ArgVals.twice_lz
        self.DimensionOnly = ArgVals.dimension_only
        self.Representative = ArgVals.representative
        self.Verbose = ArgVals.verbose

        ###Test that there are no unknown options
        if(len(self.Unknown)>1): ###First unknown unknown option is the filename
            print(("There are unknown options (the first is always present)"))
            print((self.Unknown))
            print(("Please try again. See the help for a list of valid options"))
            sys.exit(1)


if __name__ == '__main__':
    __main__(sys.argv)
