cimport numpy as np


cdef class FQHTorusBasis:

    # Member variables for storing parameters for the basis
    cdef int Dim, Momentum, Particles, NbrFlux, FluxOffset, FullDim
    # Pointer to array where basis elements will be stored
    cdef long *BasisElements
    cdef long *TmpElements
    # A temporary state to store a state as we apply operations to it
    cdef long TmpState
    cdef long TmpState2
    cdef long Mask # mask which covers all the flux in the system.

    # When using an approximate basis, we keep track of each lements index in the full
    # basis in this array.
    cdef int *ElementMap
    cdef int ApproximateBasisFlag # flag to indicate if approximate basis is being used.
    cdef int *MonomialRep # keep a variable for monomial rep

    # data members needed for inversion operations
    cdef int BytesToReverse
    cdef int ShiftAfterReverse

    cdef int Distance # amount by which to translate each time.

    #cdef int FindBasisDimension(self, int J, int Nphi, int Ne)
    cdef int FindSubBasisDimension(self, int J, int Nphi, int Ne)
    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base)
    cpdef GenerateBasis(self, filename=*, DimensionOnly=*)
    cpdef double N(self, int Orbital, int Idx)
    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx)
    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double A(self, int Orbital, int Idx)
    cpdef double complex Ad(self, int Orbital)
    #cpdef double AADebug(self, int Orbital1, int Orbital2, int Idx)
    #cpdef double AdAdDebug(self, int Orbital1, int Orbital2)
    cpdef double AA(self, int Orbital1, int Orbital2, int Idx)
    cpdef double StateAA(self, int Orbital1, int Orbital2, long State)
    cpdef double complex AdAd(self, int Orbital1, int Orbital2)
    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx)
    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3)
    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx)
    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4)
    cpdef int BinaryArraySearchDescending(self)
    cpdef int BinaryArraySearchDescendingState(self, long item)
    #cpdef np.ndarray[np.int32_t, ndim=1] GetMonomialOfElement(self, long Element)
    cpdef GetMonomialOfElement(self, long Element)
    cpdef CalcMonomialOfElement(self, long Element)
    cpdef long Translate(self, long State, int t)
    cpdef int RepresentativeConfiguration(self, Idx)
    cpdef long GetTmpState(self)
    cpdef long GetTmpState2(self)
    cpdef SetTmpState(self, long state)
    cpdef SetTmpState2(self, long state)
    cpdef OrbitalEntanglementMatrix(self, State, int AOrbitalStart, int AOrbitalEnd, int AMomentum, int AParticles)
    cpdef OrbitalEntanglementMatrixFast(self, State, int AOrbitalStart, int AOrbitalEnd, int AMomentum, int AParticles)
    cpdef OrbitalEntanglementMatrices(self, State, AOrbitalStart, AOrbitalEnd, AParticles=?, Fast=?)
    cpdef TwoBodyOperator(self, int p, int q, int r, int s, Basis2)
    cpdef ThreeBodyOperator(self, int o, int p, int q, int r, int s, int t, Basis2)
    cpdef int GetStateTotalMomentum(self, long State)
    cpdef int GetStateFullTotalMomentum(self, long State)
    cdef int GetStateCOM(self, long State)
    cdef long SimpleTranslateArbitrary(self, long State, int Orbitals)
    cdef double TranslateArbitraryPhase(self, long State, int Orbitals)
    cpdef np.ndarray[np.complex128_t, ndim=2] EvaluateWaveFunction(self, np.ndarray[np.complex128_t, ndim=2] Positions, States, WF, Verbose=*)
    cpdef np.ndarray[np.complex128_t, ndim=2] EvaluateWaveFunctionShermanMorisson(self, np.ndarray[np.complex128_t, ndim=2] Positions, States, WF, Verbose=*)
    cpdef np.ndarray[np.complex128_t, ndim=2] EvaluateSlaterDeterminants(self, np.ndarray[np.complex128_t, ndim=2] Positions, WF, Verbose=*)
    cdef ApproximateBasis(self, int ConsecutiveParticles, int ConsecutiveHoles)
    cpdef int TooManyConsecutiveParticlesOrHoles(self, long Element, int ConsecutiveParticles, int ConsecutiveHoles)
    cdef int FindPositionInSortedArray(self, int *Array, int N, int Element)
    cdef int FindPositionInMapArray(self, int Element)
    cpdef ApproximateStateToFullState(self, State)
   # cpdef FullStateToApproximateState(self, State)
    cdef int * GetMonomialRepC(self)
    cdef long GetElementC(self, int Idx)
    cpdef ConvertToPHConjugate(self, State, PHBasis=*, ReturnOp=*)
    cpdef int CalcParityOfElement(self, long Element)
    cpdef ConvertToInvertedState(self, State, IBasis=*, ReturnOp=*)
    cpdef long InvertBits(self, long State)
