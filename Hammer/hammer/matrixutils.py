#!/usr/bin/python

""" matrixutils.py: Utility functions for interfacing with matrices via Hammer. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

from petsc4py import PETSc
import numpy as np


def ExpectationValue(Operator, Vector):
    """
    Calculate the expectation value of the provided operator in the given state.
    """
    Vec2 = Vector.duplicate()
    Operator.mult(Vector, Vec2)
    return Vector.dot(Vec2)


def ProjectMatrix(Matrix, Vectors):
    """
    Project the given matrix onto the provided supspace and return the projected matrix.
    """
    Dim = len(Vectors) # the dimension of the matrix is the number of subspace vectors.

    A = PETSc.Mat(); A.create()
    A.setSizes([Dim, Dim])
    A.setFromOptions()
    A.setType('dense')
    A.setUp()
    rstart, rend = A.getOwnershipRange()
#    A.destroy()
#
#    # Create final matrix this time
#    A = PETSc.Mat(); A.create()
#    A.setSizes([Dim, Dim])
#    A.setFromOptions()

    LocalDim = rend - rstart # dimension of local portion of matrix, by row

    TmpVector = Vectors[0].duplicate()

    for j in range(Dim):
        Matrix.mult(Vectors[j], TmpVector)
        for i in range(Dim):
            Value = Vectors[i].dot(TmpVector)
            if j >= rstart and j < rend:
                A.setValue(j, i, Value, False)
    A.assemble()

    return A


def PETScMatToNumpyArray(Matrix):
    """
    Given a PETSc matrix data struction, this function will return a numpy ndarray version of this matrix.
    Does not work in parallel.

    Parameters
    --------------
    Matrix: Mat
        The PETSc matrix that we want to convert.
    """
    size = Matrix.getSize() # get the dimension of the matrix

    return Matrix.getValues(np.arange(size[0], dtype=np.int32), np.arange(size[1], dtype=np.int32))
