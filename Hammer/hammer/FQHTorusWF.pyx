#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusWF.pyx: Cython code for calculating values of single particle torus wave-functions.

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"
"""

import petsc4py.PETSc as PETSc

import numpy as np
cimport numpy as np
import scipy.special as sps
import sys
import sympy as syp
try:
    HaveJacobiThetaModule = True
    from jacobitheta import *
except:
    HaveJacobiThetaModule = False


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)
    double round(double x)


cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double complex conj(double complex x) nogil




cdef class FQHTorusWF:
    """
    This class represents a single particle wave-function for a torus wave-function which is an eigenstate of the translation operator in the x direction.
    Here we assume that the tau-gauge is used which in reduced coordinates z=L(x+tau*y) reads A = B(-y,0)
    """

    cdef int MaxK,NbrFlux,LL
    cdef double complex Tau,TwoPiINs,IPiTauNs
    cdef double Precision,Theta,CosTheta,SinTheta,Ratio,Scale,L1,L2,LLLNorm,nLLNorm,pi,Sqrt2PiTau2Ns
    ##cdef np.ndarray[np.float64_t,ndim=1] HpolTable
    cdef double [:] HpolTable

    def __init__(self, int NbrFlux, double complex Tau=1j, int LL=0, double Precision=1e-50):
        """
        Constructor. Sets the number of flux, torus parameter tau and Landau level.
        """
        self.NbrFlux = NbrFlux
        ###FIXME::
        ###Function uses -\bar{tau} instead of tau. That logic is here.
        ###
        self.Tau = -np.real(Tau) + 1j*np.imag(Tau)
        self.Precision = Precision
        if LL<0:
            raise ValueError("ERROR: Negative LL:s not valid. Use LL>=0!")
        else:
            self.LL = LL

        # calculate derived values
        self.Theta = np.angle(self.Tau)
        self.CosTheta = np.cos(self.Theta)
        self.SinTheta = np.sin(self.Theta)
        self.Ratio = np.imag(self.Tau)/self.SinTheta
        self.Scale = np.sqrt(2.0*np.pi*self.NbrFlux/(self.Ratio*self.SinTheta))
        self.L1 = self.Scale
        self.L2 = self.Ratio*self.Scale
        self.pi = np.pi
        self.TwoPiINs = 1j*2*self.pi*self.NbrFlux
        self.IPiTauNs =1j*self.pi*self.Tau*self.NbrFlux
        self.Sqrt2PiTau2Ns = np.sqrt(2*self.pi*self.NbrFlux*np.imag(self.Tau))

        # calculate the maximum k to sum to to reach the desired precision.
        ### The width of the wave-function in y space is
        SigmaWidth=np.sqrt((2*self.LL+1)/(4*self.pi*self.NbrFlux*np.imag(self.Tau)))
        ###We we want at least 10 sigma summed in both directions. This gives us
        self.MaxK = np.ceil(SigmaWidth*10)
        ###Normalization factor for LLL and higher LL corrections
        self.LLLNorm = 1/np.sqrt(self.L1* np.sqrt(self.pi))
        self.nLLNorm = 1/np.sqrt(self.L1* np.sqrt(self.pi)*sps.factorial(self.LL)* 2.0**(self.LL))


        ###INtialize the Hermite polynomial table
        self.HpolTable = HermitePolynomialVector(self.LL)


    def WFValue(self, xyArray, double momentum):
        """
        Calculate the value of the wave-function at xy
        """
        cdef double complex xyval
        cdef double mom = <double>momentum
        cdef int n
        cdef double complex [:] wfArray
        cdef double complex [:] PosArray
        ##Check if input is a numpy array or a "scalar"
        if hasattr(xyArray, "__len__"): ##Array
            wfArray=np.zeros(xyArray.shape,dtype=np.complex128)
            Elems=len(xyArray)
            PosArray=np.asarray(xyArray,dtype=np.complex128)
            n = 0
            if self.LL == 0:
                ###FIXME, MICKE (august 2016): Maybe change so that jacobi theta is only called if tau_2 < 1. If  tau_2 > 1 the native sum should be fast enough.....
                if HaveJacobiThetaModule:
                    while n < Elems:
                        xyval=PosArray[n]
                        wfArray[n] = self.WFValueJT(xyval, momentum)
                        n+=1
                else:
                    while n < Elems:
                        xyval=PosArray[n]
                        wfArray[n]=self.WFValueLLL(xyval, momentum)
                        n+=1
            elif self.LL == 1:
                while n < Elems:
                    xyval=PosArray[n]
                    wfArray[n] = self.WFValueSLL(xyval, momentum)
                    n+=1
            else:
                while n < Elems:
                    xyval=PosArray[n]
                    wfArray[n]=self.WFValueGeneral(xyval, momentum)
                    n+=1
            return wfArray
        else:
            ###Set xyvalue
            xyval=xyArray
            if self.LL == 0:
                if HaveJacobiThetaModule:
                    return self.WFValueJT(xyval, momentum)
                else:
                    return self.WFValueLLL(xyval, momentum)
            elif self.LL == 1:
                return self.WFValueSLL(xyval, momentum)
            else:
                return self.WFValueGeneral(xyval, momentum)


    cpdef double complex WFValueGeneral(self, double complex xy, double momentum):
        """
        Calculate the value of the wave-function at z
        """
        cdef int k
        cdef double l
        cdef double complex val = 0.0
        cdef double x = xy.real
        cdef double y = xy.imag
        cdef double Momentum = <double>momentum
        cdef double NbrFlux = self.NbrFlux
        cdef double complex tau = self.Tau
        cdef double complex tbt=conj(self.Tau)-self.Tau
        cdef double basis_norm = self.LLLNorm
        cdef double MomentumOverNs = Momentum/NbrFlux
        cdef double complex xtilde,ytilde,expxy,HermitePol
        cdef double yHermite
        cdef double yshift

        yshift=round(MomentumOverNs+y)
        l = -self.MaxK-yshift
        while l <= self.MaxK-yshift:
            xtilde=self.TwoPiINs*(l+MomentumOverNs)*x
            ytilde=self.IPiTauNs*(l+MomentumOverNs+y)**2
            yHermite=self.Sqrt2PiTau2Ns*(l+MomentumOverNs+y)
            expxy=cexp(xtilde+ytilde)
            k=0
            HermitePol=0.0
            while 2*k <= self.LL:
                ###The hermite polynomial ####
                HermitePol+=self.HpolTable[self.LL-2*k]*(yHermite)**(self.LL-2*k)
                k+=1
            val+=HermitePol*expxy
            l+=1.0

        if self.LL==0:
            return self.LLLNorm * val
        else:
            return self.nLLNorm * val


    cpdef double complex WFValueLLL(self, double complex xy, double momentum):
        """
        Calculate the value of the wave-function at z
        """
        cdef double k
        cdef double complex val = 0.0
        cdef double x = xy.real
        cdef double y = xy.imag
        cdef double Momentum = <double>momentum
        cdef double NbrFlux = self.NbrFlux
        cdef double MomOverNs = Momentum / NbrFlux
        cdef double complex tau = self.Tau
        cdef double complex TwoPiIXNs = 1j*2*self.pi*x*NbrFlux
        cdef double complex PiITauNs = 1j*self.pi*tau*NbrFlux
        cdef double complex expxy
        cdef double basis_norm = self.LLLNorm

        yshift=round(MomOverNs+y)
        k = -self.MaxK-yshift
        while k <= self.MaxK-yshift:
            expxy = cexp(TwoPiIXNs*(k+MomOverNs)) *cexp(PiITauNs*(y+k+MomOverNs)**2)
            val +=  expxy
            k+=1.0
        return basis_norm * val


    cpdef double complex WFValueSLL(self, double complex xy, double momentum):
        """
        Calculate the value of the wave-function at xy
        """

        cdef double k
        cdef double complex val = 0.0
        cdef double complex tau = self.Tau
        cdef double NbrFlux = self.NbrFlux
        cdef double Momentum = <double>momentum
        cdef double basis_norm = self.nLLNorm
        cdef double x = xy.real
        cdef double y = xy.imag


        ###Dependents
        cdef double MomOverNs = Momentum / NbrFlux
        cdef double complex TwoPiIXNs = 1j*2*self.pi*x*NbrFlux
        cdef double complex PiITauNs = 1j*self.pi*tau*NbrFlux
        cdef double complex prefactor = 2.0*self.Sqrt2PiTau2Ns
        cdef double yshift=round(MomOverNs+y)

        #print "yshift,MaxK:",yshift,self.MaxK
        ###Shift the sum over K so that the maximum is in the middle of the sum
        k = -self.MaxK-yshift
        while k <= self.MaxK-yshift:
            val += cexp(TwoPiIXNs*(k+MomOverNs)) *cexp(PiITauNs*(y+MomOverNs+k)**2)*(MomOverNs+y+k)
            k+=1.0
        val = basis_norm * val *prefactor
        #print('val',val)
        return val


    cpdef double complex WFValueJT(self, double complex xy, double momentum):
        """
        Evaulate the wave-function using the external JacobiTheta module one value at a time
        """
        cdef double complex Value = 0.0
        cdef double Ns = <double>self.NbrFlux
        cdef double n = momentum
        cdef double complex tau = self.Tau
        cdef double complex tau_gauge = 1j * self.pi * Ns * tau * (xy.imag**2)
        cdef double complex theta
        cdef double basis_norm = self.LLLNorm

        if HaveJacobiThetaModule:
            theta = jacobitheta.logthetagen(1.0*n/Ns, 0.0, Ns*(xy.real+tau*xy.imag), Ns*tau)
            return cexp(tau_gauge + theta) * basis_norm
        else:
            raise RuntimeError("ERROR: JacobiThetaModule not installed!\nWFValueJT can not be used in this case. Call WFValueLLL instead")

    def GetNumOrbitals(self):
        """
        Return the number of orbitals
        """
        return self.NbrFlux

        
    def Rho(self, xy, Basis, State):
        import hammer.FQHCorrelations as FQHCorrelations
        return FQHCorrelations.Rho(self,xy,Basis,State)

    
    def TwoBody(self, z1, z2, Basis, State):
        import hammer.FQHCorrelations as FQHCorrelations
        return FQHCorrelations.TwoBody(self,z1,z2,Basis,State)

        
    def RhoSuperposition(self, z, Bases, States, Coefficients):
        """
        Method to calculate the density of the provided superposition of states at z
        """
        cdef double complex val = 0
        cdef int N = len(Bases) # number of states in the superposition.

        # all terms including cross terms
        # < psi_i | a_p^{+}a_q | psi_j>
        for i in range(N):
            for j in range(i, N):
                Basis1 = Bases[i]
                Basis2 = Bases[j]
                Dim1 = Basis1.GetDimension()
                Dim2 = Basis2.GetDimension()
                Details1 = Basis1.GetBasisDetails()
                Details2 = Basis2.GetBasisDetails()
                State1 = States[i]
                State2 = States[j]
                Momentum1 = Details1['Momentum']
                Momentum2 = Details2['Momentum']
                MomentumDiff = (Momentum1 - Momentum2) % self.NbrFlux
                for q in range(self.NbrFlux):
                    p = (Momentum1 - Momentum2 + q) % self.NbrFlux
                    factor1 = conj(self.WFValue(xy,p))
                    factor2 = self.WFValue(xy,q)
                    for k in range(Dim2):
                        phase1 = Basis2.A(q, k)
                        if np.abs(phase1) > 0.0:
                            phase2 = Basis2.Ad(p)
                            Idx = Basis1.BinaryArraySearchDescendingState(Basis2.GetTmpState2())
                            if np.abs(phase2) > 0 and Idx >=0 and Idx < Dim1:
                                val += phase1 * phase2 * factor1 * factor2 * conj(State1.getValue(Idx)) * State2.getValue(k) * Coefficients[j] * conj(Coefficients[i])
                                if i != j: # if not equal should also consider reversed case.
                                    val += conj(phase1 * phase2 * factor1 * factor2 * conj(State1.getValue(Idx)) * State2.getValue(k) * Coefficients[j] * conj(Coefficients[i]))
        return val


    def PreComputeWFs(self, GridDims):
        """
        Precompute the values of the wave-function for each momentum at points on a grid of dimension GridDims spanning the unit cell.
        """
        cdef int p, i, j

        cdef np.ndarray[np.complex128_t, ndim=3] WF = np.zeros((self.NbrFlux, GridDims[0], GridDims[1]), dtype=np.complex128)

        xrange = np.linspace(0.0, 1.0, GridDims[1])
        yrange = np.linspace(0.0, 1.0, GridDims[0])
        for [j,y] in zip(range(GridDims[0]), yrange):
            for [i,x] in zip(range(GridDims[1]), xrange):
                p = 0
                while p < self.NbrFlux:
                    WF[p, j, i] = self.WFValue(np.complex(x,y), p)
                    p += 1

        return WF


    def RhoArray(self, Basis, State, n):
        """
        Method to calculate the density for an array of an nxn array of points covering the torus.
        """
        cdef int dim = Basis.GetDimension()

        WFs = self.PreComputeWFs((n, n))

        ValuesBuffer = np.zeros((n, n), dtype=PETSc.RealType)
        ValuesBufferRecv = np.zeros((n, n), dtype=PETSc.RealType)

        cdef rstart, rend, i
        rstart, rend = State.getOwnershipRange()

        p = 0
        while p < self.NbrFlux:
            factor1 = np.real(WFs[p] * np.conj(WFs[p])) # element wise multiplication
            i = rstart
            while i < rend:
                if Basis.N(p,i) == 1.0:
                    ValuesBuffer += np.real(factor1 * (State.getValue(i) * conj(State.getValue(i))))
                i += 1
            p += 1

        PETSc.COMM_WORLD.tompi4py().Allreduce(ValuesBuffer, ValuesBufferRecv)
        Integral = np.sum(ValuesBufferRecv) * (self.L1/n) * (self.L2*self.SinTheta/n)

        return [ValuesBufferRecv, Integral]


    def RhoSuperpositionArray(self, Bases, States, Coefficients, n):
        """
        Method to calculate the density of the provided superposition of states at points on an evenly space nxn grid over
        the torus unit cell.
        """
        cdef int N = len(Bases) # number of states in the superposition.
        cdef int i, j, k, q, Idx
        values = np.zeros((n, n), dtype=np.complex128)

        # precallculate wf values across the grid
        WFs = self.PreComputeWFs((n, n))

        # all terms including cross terms
        # < psi_i | a_p^{+}a_q | psi_j>
        i = 0
        while i < N:
            j = i
            while j < N:
                Basis1 = Bases[i]
                Basis2 = Bases[j]
                Dim1 = Basis1.GetDimension()
                Dim2 = Basis2.GetDimension()
                Details1 = Basis1.GetBasisDetails()
                Details2 = Basis2.GetBasisDetails()
                State1 = States[i]
                State2 = States[j]
                Momentum1 = Details1['Momentum']
                Momentum2 = Details2['Momentum']
                MomentumDiff = (Momentum1 - Momentum2) % self.NbrFlux
                q = 0
                while q < self.NbrFlux:
                    p = (Momentum1 - Momentum2 + q) % self.NbrFlux
                    factor1 = np.conj(WFs[p])
                    factor2 = WFs[q]
                    k = 0
                    while k < Dim2:
                        phase1 = Basis2.A(q, k)
                        if np.abs(phase1) > 0.0:
                            phase2 = Basis2.Ad(p)
                            Idx = Basis1.BinaryArraySearchDescendingState(Basis2.GetTmpState2())
                            if np.abs(phase2) > 0 and Idx >=0 and Idx < Dim1:
                                values += phase1 * phase2 * factor1 * factor2 * conj(State1.getValue(Idx)) * State2.getValue(k) * Coefficients[j] * conj(Coefficients[i])
                                if i != j: # if not equal should also consider reversed case.
                                    values += np.conj(phase1 * phase2 * factor1 * factor2 * conj(State1.getValue(Idx)) * State2.getValue(k) * Coefficients[j] * conj(Coefficients[i]))
                        k += 1
                    q += 1
                j += 1
            i += 1

        integral = np.sum(values) * (self.L1/n) * (self.L2*self.SinTheta/n)

        return [values, integral]

    def TwoBodyArray(self, z, Basis, State, n, OffDiagonal=True):
        """
        Calculate the two body correlator between points z and points evenly spaced out over the torus unit cell.
        """
        values = np.zeros((n, n), dtype=np.complex128)

        # precompute values needed.
        wfzs = np.zeros(self.NbrFlux, dtype=np.complex128)
        for p in range(self.NbrFlux):
            wfzs[p] = self.WFValue(z, p)

        rhoz = np.real(self.Rho(z, Basis, State))

        xstep, ystep, WFs = self.PreComputeWFs((n, n))

        Particles = Basis.GetBasisDetails()['Particles']

        if OffDiagonal:
            for q in range(self.NbrFlux-1):
                for p in range(q+1, self.NbrFlux):
                    factor1 = np.conj(wfzs[p] * WFs[q])
                    factor2 = np.conj(wfzs[q] * WFs[p])
                    for s in range(self.NbrFlux-1):
                        r = (p + q - s + 2 * self.NbrFlux) % self.NbrFlux
                        if r > s:
                            factor3 = wfzs[r] * WFs[s]
                            factor4 = wfzs[s] * WFs[r]
                            for j in range(Basis.GetDimension()):
                                Phase1 = Basis.AA(r,s,j)
                                if fabs(Phase1) > 0:
                                    Phase2 = Basis.AdAd(p,q)
                                    if cabs(Phase2) > 0:
                                        Idx = Basis.BinaryArraySearchDescending()
                                        if  Idx >= 0 and Idx < Basis.GetDimension():
                                            values += Phase1 * Phase2 * (factor1 - factor2)*(factor3 - factor4) * State.getValue(j) * conj(State.getValue(Idx))
        else:
            for q in range(self.NbrFlux-1):
                for p in range(q+1, self.NbrFlux):
                    factor1 = np.conj(wfzs[p] * WFs[q])
                    factor2 = np.conj(wfzs[q] * WFs[p])
                    r = p
                    s = q
                    factor3 = wfzs[r] * WFs[s]
                    factor4 = wfzs[s] * WFs[r]
                    for j in range(Basis.GetDimension()):
                        Phase1 = Basis.AA(r,s,j)
                        if fabs(Phase1) > 0:
                            Phase2 = Basis.AdAd(p,q)
                            if cabs(Phase2) > 0:
                                Idx = Basis.BinaryArraySearchDescending()
                                if  Idx >= 0 and Idx < Basis.GetDimension():
                                    values += Phase1 * Phase2 * (factor1 - factor2)*(factor3 - factor4) * State.getValue(j) * conj(State.getValue(Idx))


        values =  -values / rhoz
        integral = np.sum(values) * self.xstep * self.ystep

        return [values, integral]


    def TwoBodyArrayUsingOperators(self, z, Basis, State, n):
        """
        Calculate the two body correlator between points z and points evenly spaced out over the torus unit cell.

        Parameters
        -----------
        z: complex
            Fixed position to calculate two point correlator from.
        Basis: Basis instance
            Object containing fock configurations and methods for acting on them.
        State: Vector object
            Vector object containing fock coefficients.
        n: int
            The number of grid points in each direction to use.
        """
        MyRank = PETSc.Comm.getRank(PETSc.COMM_WORLD)
        cdef np.ndarray[np.complex128_t, ndim=2] values = np.zeros((n, n), dtype=np.complex128)
        cdef int r, s, p, q

        # precompute values needed.
        cdef np.ndarray[np.complex128_t, ndim=1] wfzs = np.zeros(self.NbrFlux, dtype=np.complex128)
        for p in range(self.NbrFlux):
            wfzs[p] = self.WFValue(z, p)

        cdef double rhoz = np.real(self.Rho(z, Basis, State))
        cdef np.ndarray[np.float64_t, ndim=2] rhozs
        cdef np.ndarray[np.complex128_t, ndim=2] factor1, factor2, factor3, factor4
        cdef double RhoInt
        [rhozs, RhoInt] = self.RhoArray(Basis, State, n)
        #print "Density: " + str(rhoz)

        WFs = self.PreComputeWFs((n, n))

        Particles = Basis.GetBasisDetails()['Particles']

        TwoBodyFactors = dict()
        TmpVec = State.duplicate()

        if Particles < self.NbrFlux:
            for q in range(self.NbrFlux-1):
                for p in range(q+1, self.NbrFlux):
                    for s in range(self.NbrFlux-1):
                        r = (p + q - s + 2 * self.NbrFlux) % self.NbrFlux
                        if r > s:
                            Op = Basis.TwoBodyOperator(p, q, r, s, Basis)
                            Op.mult(State, TmpVec)
                            TwoBodyFactors[(p,q,r,s)] = State.dot(TmpVec)


            for q in range(self.NbrFlux-1):
                for p in range(q+1, self.NbrFlux):
                    factor1 = np.conj(wfzs[p] * WFs[q])
                    factor2 = np.conj(wfzs[q] * WFs[p])
                    for s in range(self.NbrFlux-1):
                        r = (p + q - s + 2 * self.NbrFlux) % self.NbrFlux
                        if r > s:
                            factor3 = wfzs[r] * WFs[s]
                            factor4 = wfzs[s] * WFs[r]
                            values += (factor1 - factor2)*(factor3 - factor4) * TwoBodyFactors[(p,q,r,s)]

        else:
            for q in range(self.NbrFlux-1):
                for p in range(q+1, self.NbrFlux):
                    factor1 = np.conj(wfzs[p] * WFs[q])
                    factor2 = np.conj(wfzs[q] * WFs[p])
                    r = p
                    s = q
                    factor3 = wfzs[r] * WFs[s]
                    factor4 = wfzs[s] * WFs[r]
                    values += (factor1 - factor2)*(factor3 - factor4)*(-1.0)

        values =  -values / rhoz
        integral = np.sum(values) * (self.L1/n) * (self.L2*self.SinTheta/n)

        return [values, integral]


    def TwoBodySuperposition(self, z1, z2, Bases, States, Coefficients):
        """
        Calculate the two body correlator between points z1 and z2 of the specified superposition of states.
        """
        cdef double complex val = 0.0
        cdef double complex rhoz = np.real(self.RhoSuperposition(z1, Bases, States, Coefficients))
        cdef int N = len(Bases) # number of states in the superposition.
        cdef int i, j, q, p, s, r, Dim1, Dim2, Momentum1, Momentum2, MomentumDiff
        cdef double complex factor1, factor2, factor3, factor4, phase2, valtmp
        cdef double phase1
        cdef int NbrFlux = self.NbrFlux

        # precompute values needed.
        WFsZ1s = np.zeros(NbrFlux, dtype=np.complex128)
        WFsZ2s = np.zeros(NbrFlux, dtype=np.complex128)
        for p in range(NbrFlux):
            WFsZ1s[p] = self.WFValue(z1, p)
            WFsZ2s[p] = self.WFValue(z2, p)

        Particles = Bases[0].GetBasisDetails()['Particles']


        # all terms including cross terms
        # < psi_i | a_p^{+}a_q^{+} a_r a_s | psi_j>
        i = 0
        while i < N:
            j = i
            while j < N:
                Basis1 = Bases[i]
                Basis2 = Bases[j]
                Dim1 = Basis1.GetDimension()
                Dim2 = Basis2.GetDimension()
                Details1 = Basis1.GetBasisDetails()
                Details2 = Basis2.GetBasisDetails()
                State1 = States[i]
                State2 = States[j]
                Momentum1 = Details1['Momentum']
                Momentum2 = Details2['Momentum']
                MomentumDiff = (Momentum1 - Momentum2) % NbrFlux
                q = 0
                while q < (NbrFlux - 1):
                    p = q + 1
                    while p < NbrFlux:
                        factor1 = conj(WFsZ1s[p] * WFsZ2s[q])
                        factor2 = conj(WFsZ2s[p] * WFsZ1s[q])
                        s = 0
                        while s < (NbrFlux-1):
                            r = (p + q + MomentumDiff - s + 2 * NbrFlux) % NbrFlux
                            if r > s:
                                factor3 = WFsZ1s[r] * WFsZ2s[s]
                                factor4 = WFsZ2s[r] * WFsZ1s[s]
                                k = 0
                                while k < Dim2:
                                    phase1 = Basis2.AA(p, q, k)
                                    if fabs(phase1) > 0.0:
                                        phase2 = Basis2.AdAd(r, s)
                                        Idx = Basis1.BinaryArraySearchDescendingState(Basis2.GetTmpState2())
                                        if cabs(phase2) > 0 and Idx >=0 and Idx < Dim1:
                                            valtmp = phase1 * phase2 * (factor1 - factor2) * (factor3 - factor4) * conj(State1.getValue(Idx)) * State2.getValue(k) * Coefficients[j] * conj(Coefficients[i])
                                            if i != j: # if not equal should also consider reversed case.
                                                valtmp += conj(valtmp)
                                            val += valtmp
                                    k += 1
                            s += 1
                        p += 1
                    q += 1
                j += 1
            i += 1

        return - val / rhoz


    def TwoBodySuperpositionArray(self, z, Bases, States, Coefficients, n):
        """
        Calculate the two body correlator between points z1 and z2 of the
        specified superposition of states.
        """
        cdef int N = len(Bases) # number of states in the superposition.
        cdef int k, p, q, r, s
        values = np.zeros((n, n), dtype=np.complex128)

        # precompute values needed.
        wfzs = np.zeros(self.NbrFlux, dtype=np.complex128)
        xstep, ystep, WFs = self.PreComputeWFs((n, n))
        for p in range(self.NbrFlux):
            wfzs[p] = self.WFValue(z, p)

        Particles = Bases[0].GetBasisDetails()['Particles']

        rhoz = np.real(self.RhoSuperposition(z, Bases, States, Coefficients))

        # all terms including cross terms
        # < psi_i | a_p^{+}a_q^{+} a_r a_s | psi_j>
        for i in range(N):
            for j in range(i, N):
                Basis1 = Bases[i]
                Basis2 = Bases[j]
                Dim1 = Basis1.GetDimension()
                Dim2 = Basis2.GetDimension()
                Details1 = Basis1.GetBasisDetails()
                Details2 = Basis2.GetBasisDetails()
                State1 = States[i]
                State2 = States[j]
                Momentum1 = Details1['Momentum']
                Momentum2 = Details2['Momentum']
                MomentumDiff = (Momentum1 - Momentum2) % self.NbrFlux
                q = 0
                while q < self.NbrFlux-1:
                    p = q + 1
                    while p < self.NbrFlux:
                        factor1 = np.conj(wfzs[p] * WFs[q])
                        factor2 = np.conj(wfzs[q] * WFs[p])
                        s = 0
                        while s < self.NbrFlux-1:
                            r = (p + q + MomentumDiff - s + 2 * self.NbrFlux) % self.NbrFlux
                            if r > s:
                                factor3 = wfzs[r] * WFs[s]
                                factor4 = wfzs[s] * WFs[r]
                                k = 0
                                while k < Dim2:
                                    phase1 = Basis2.AA(p, q, k)
                                    if fabs(phase1) > 0.0:
                                        phase2 = Basis2.AdAd(r, s)
                                        Idx = Basis1.BinaryArraySearchDescendingState(Basis2.GetTmpState2())
                                        if cabs(phase2) > 0 and Idx >=0 and Idx < Dim1:
                                            val = phase1 * phase2 * (factor1 - factor2) * (factor3 - factor4) * conj(State1.getValue(Idx)) * State2.getValue(k) * Coefficients[j] * conj(Coefficients[i])
                                            if i != j: # if not equal should also consider reversed case.
                                                val += np.conj(val)
                                            values += val
                                    k += 1
                            s += 1
                        p += 1
                    q += 1

        values = - values / rhoz
        integral = np.sum(values) * xstep * ystep

        return [values, integral]


    def TorusPlotData(self, Data):
        """
        Pad the provided data array so that when drawn, the shape of the torus is correct.
        """
        L1 = self.L1
        L2 = self.L2
        Theta = self.Theta
        SinTheta = self.SinTheta
        CosTheta = self.CosTheta
        (ynum, xnum) = Data.shape

        xlength = L1 + L2 * CosTheta
        xstep = L1/np.float(xnum)
        ylength = L2
        ystep = ylength/np.float(ynum)
        xnumnew = xnum * xlength/L1 + 1
        #print "X num: " + str(xnum)
        PlotData = np.ones((ynum, xnumnew)) * np.NAN

        for j in range(ynum):
            Xoffset = j * ystep * CosTheta
            Xstart = np.floor(xnumnew * Xoffset / xlength)
            #print Xstart
            PlotData[j, Xstart:(Xstart + xnum)] = Data[j,:]

        Extent = (0, xlength, 0, ylength * SinTheta)
        return [PlotData, Extent]


    def ThreeBodyArrayUsingOperators(self, z1, z2, Basis, State, n):
        """
        Calculate the three body correlator between points z_1, z_2 and a third
        point evenly spaced out over the torus unit cell. Use matrix operators
        to make to optimise.
        """
        MyRank = PETSc.Comm.getRank(PETSc.COMM_WORLD)
        values = np.zeros((n, n), dtype=np.complex128)

        # precompute values needed.
        wfzs1 = np.zeros(self.NbrFlux, dtype=np.complex128)
        wfzs2 = np.zeros(self.NbrFlux, dtype=np.complex128)
        for p in range(self.NbrFlux):
            wfzs1[p] = self.WFValue(z1, p)
            wfzs2[p] = self.WFValue(z2, p)

        rhoz1 = np.real(self.Rho(z1, Basis, State))
        print "Dividing by density: " + str(rhoz1)
        rhoz2 = np.real(self.Rho(z2, Basis, State))
        [rhozs, RhoInt] = self.RhoArray(Basis, State, n)

        G2 = np.real(self.TwoBody(z1, z2, Basis, State))
        print "Dividing by two body: " + str(G2)

        xstep, ystep, WFs = self.PreComputeWFs((n, n))

        Particles = Basis.GetBasisDetails()['Particles']

        ThreeBodyFactors = dict()
        TmpVec = State.duplicate()

        if Particles < self.NbrFlux:
            for q in range(self.NbrFlux-2):
                for p in range(q+1, self.NbrFlux-1):
                    for o in range(p+1, self.NbrFlux):
                        for t in range(self.NbrFlux-2):
                            for s in range(t+1, self.NbrFlux-1):
                                r = (p + q + o - s - t + 3 * self.NbrFlux) % self.NbrFlux
                                if r > s:
                                    Op = Basis.ThreeBodyOperator(o, p, q, r, s, t, Basis)
                                    Op.mult(State, TmpVec)
                                    ThreeBodyFactors[(o, p, q, r, s, t)] = State.dot(TmpVec)


            for q in range(self.NbrFlux-2):
                for p in range(q+1, self.NbrFlux-1):
                    for o in range(p+1, self.NbrFlux):
                        factor1 = np.conj(wfzs1[o] * (wfzs2[p] * WFs[q] - wfzs2[q] * WFs[p]) - wfzs1[p] * (wfzs2[o] * WFs[q] - wfzs2[q] * WFs[o]) + wfzs1[q] * (wfzs2[o] * WFs[p] - wfzs2[p] * WFs[o]))
                        for t in range(self.NbrFlux-2):
                            for s in range(t+1, self.NbrFlux-1):
                                r = (p + q + o - s - t + 3 * self.NbrFlux) % self.NbrFlux
                                if r > s:
                                    factor2 = wfzs1[r] * (wfzs2[s] * WFs[t] - wfzs2[t] * WFs[s]) - wfzs1[s] * (wfzs2[r] * WFs[t] - wfzs2[t] * WFs[r]) + wfzs1[t] * (wfzs2[r] * WFs[s] - wfzs2[s] * WFs[r])
                                    values += (factor1)*(factor2) * ThreeBodyFactors[(o, p, q, r, s, t)]

            #print ThreeBodyFactors

        else:
            for q in range(self.NbrFlux-2):
                for p in range(q+1, self.NbrFlux-1):
                    for o in range(p+1, self.NbrFlux):
                        factor1 = np.conj(wfzs1[o] * (wfzs2[p] * WFs[q] - wfzs2[q] * WFs[p]) - wfzs1[p] * (wfzs2[o] * WFs[q] - wfzs2[q] * WFs[o]) + wfzs1[q] * (wfzs2[o] * WFs[p] - wfzs2[p] * WFs[o]))
                        r = o
                        s = p
                        t = q
                        factor2 = wfzs1[r] * (wfzs2[s] * WFs[t] - wfzs2[t] * WFs[s]) - wfzs1[s] * (wfzs2[r] * WFs[t] - wfzs2[t] * WFs[r]) + wfzs1[t] * (wfzs2[r] * WFs[s] - wfzs2[s] * WFs[r])
                        values += (factor1)*(factor2)*(-1.0)**(o+p+q+r+s+t)


        values =  values / (rhoz2 * G2)
        integral = np.sum(values) * xstep * ystep

        return [values, integral]


    def SingleParticleEnergy(self):
        """
        Function to calculate the single particle contribution to the Coulomb
        energy for the specified torus. Uses formalism described in Yoshioka
        and Halperin (1983) and the expression for the constant that is needed
        is in Bonsall and Maradudin (1977). Only implemented for rectangular
        tori for the moment. Uses implementation of incomplete
        gamma function from scipy.special module. The constant we return here
        is half that given the Bonsall paper, which makes the results match
        exactly those in Yoshioka et. al.
        """

        cdef double c = 0.0, z, old_c = -1.0, r = self.Ratio, tol = 1e-14
        cdef int l = 1, l1, l2, lmax = 100

        while abs(c-old_c) > tol and l < lmax:
            old_c = c
            l1 = l; l2 = -l
            while l2 <= l:
                z = self.pi* (r*l1**2 + l2**2/r)
                c += sps.gamma(0.5) * z**(-0.5)*sps.gammaincc(0.5, z)
                l2+=1

            l1 = -l; l2 = -l
            while l2 <= l:
                z = self.pi* (r*l1**2 + l2**2/r)
                c += sps.gamma(0.5) * z**(-0.5)*sps.gammaincc(0.5, z)
                l2+=1

            l2 = l; l1 = -l+1
            while l1 < l:
                z = self.pi* (r*l1**2 + l2**2/r)
                c += sps.gamma(0.5) * z**(-0.5)*sps.gammaincc(0.5, z)
                l1+= 1

            l2 = -l; l1 = -l+1
            while l1 < l:
                z = self.pi* (r*l1**2 + l2**2/r)
                c += sps.gamma(0.5) * z**(-0.5)*sps.gammaincc(0.5, z)
                l1+= 1

            l += 1

        area = self.L1 * self.L2

        return -(2.0 - c) / sqrt(area)


    def GetTorusSides(self):
        """
        Return tuple with side lengths L1 and L2.
        """
        return (self.L1, self.L2)


def HermitePolynomialVector(int ll):
    """
    Gives the Hermite monomial coefficents for the polynomial of degree LL
    """

    if ll<0: ### Value should be smaller than
        raise ValueError("Negative degree of polynomials not accepted.")
    HVec=np.zeros((ll+1),dtype='float64')
    if ll==0:
        HVec[0]=1.0
    elif ll==1:
        HVec[1]=2.0
    elif ll==2:
        HVec[2]=4.0
        HVec[0]=-2.0
    elif ll==3:
        HVec[3]=8.0
        HVec[1]=-12.0
    elif ll==4:
        HVec[4]=16.0
        HVec[2]=-48.0
        HVec[0]=12.0
    elif ll==5:
        HVec[5]=32.0
        HVec[3]=-160.0
        HVec[1]=120.0
    elif ll==6:
        HVec[6]=64.0
        HVec[4]=-480.0
        HVec[2]=720.0
        HVec[0]=-120.0
    elif ll==7:
        HVec[7]=128.0
        HVec[5]=-1344.0
        HVec[3]=3360.0
        HVec[1]=-1680.0
    elif ll==8:
        HVec[8]=256.0
        HVec[6]=-3584.0
        HVec[4]=13440.0
        HVec[2]=-13440.0
        HVec[0]=1680.0
    elif ll==9:
        HVec[9]=512.0
        HVec[7]=-9216.0
        HVec[5]=48384.0
        HVec[3]=-80640.0
        HVec[1]=30240.0
    elif ll==10:
        HVec[10]=1024.0
        HVec[8]=-23040.0
        HVec[6]=161280.0
        HVec[4]=-403200.0
        HVec[2]=302400.0
        HVec[0]=-30240.0
    elif ll==11:
        HVec[11]=2048.0
        HVec[9]=-56320.0
        HVec[7]=506880.0
        HVec[5]=-1774080.0
        HVec[3]=2217600.0
        HVec[1]=-665280.0
    else:
        raise ValueError("No hermite polynomial of degree "+str(ll)+" has been implement. If you believe it should be implemented, bug micke.fremling@gmail.com to add it.")
    return HVec

def ConvertFromSquareCoordinates(Coordinates, Tau, Ns):
    """
    Given coordinates defined on square, convert these to coordinates defined
    on torus unit cell. Coordinated: This is a list of arrays of complex
    numbers where the real and imaginary parts are between -0.5 and 0.5.
    """
    WF = FQHTorusWF(Ns, Tau)
    L1 = WF.L1
    NewCoordinates = list()

    for CoordinateSet in Coordinates:
        NewCoordinateSet = np.real(CoordinateSet) * WF.L1 + np.imag(CoordinateSet) * WF.L1 * Tau
        NewCoordinates.append(NewCoordinateSet)

    return NewCoordinates


def ReadCoordinatesColumnFormat(FilenameReal, FilenameImag):
    """
    This function reads coordinates (2D) from the provided files which are in column format and
    returns a two dimensional array of complex numbers, with rows cooresponding to different
    coordinate sets and columns cooresponding to different particles.
    """
    Coordinates = list()
    try:
        RealFile = open(FilenameReal); ImagFile = open(FilenameImag)
        RealLine = RealFile.readline(); ImagLine = ImagFile.readline()
        while RealLine != '' and ImagLine != '':
            Reals = map(lambda x: np.float(x), RealLine.split())
            Imags = map(lambda x: np.float(x), ImagLine.split())
            Coordinates.append(np.asarray(Reals) + np.complex(0.0, 1.0) * np.asarray(Imags))
            RealLine = RealFile.readline(); ImagLine = ImagFile.readline()

        Coordinates = np.vstack(Coordinates)
        return Coordinates

    except IOError:
        print "Problems opening " + FilenameReal + " or " + FilenameImag
        return None


def WriteWaveFunctionValuesColumnFormat(Values, Prefix, Append=False):
    """
    This function writes the provided values to files in column format. The
    real parts will be written to a filenamed 'Prefix'_re.rnd and the imaginary
    parts to a file names 'Prefix'_im.rnd.
    """

    try:
        if Append:
            RealFile = open(Prefix + '_re.rnd', 'a'); ImagFile = open(Prefix + '_im.rnd', 'a');
        else:
            RealFile = open(Prefix + '_re.rnd', 'w'); ImagFile = open(Prefix + '_im.rnd', 'w');

        for Value in Values:
            RealFile.write('%.16E' % np.real(Value) + '\n')
            ImagFile.write('%.16E' % np.imag(Value) + '\n')
        RealFile.close()
        ImagFile.close()
        return True

    except IOError:
        print "Problems opening " + Prefix + "_re.rnd or " + Prefix + "_im.rnd for writing."
        return False


def ReadWaveFunctionValuesColumnFormat(Prefix, RealFilename=None, ImagFilename=None):
    """
    This function reads wave-function values from the files indicated by the
    given prefix. Attempts to read the real parts from '<Prefix>_re.rnd' and
    the imaginary parts from '<Prefix>_im.rnd'. A list of complex values is
    returned.

    Parameters
    -----------
    Prefix: string
        Prefix of files to read.
    """
    if RealFilename is None and ImagFilename is None:
        RealFilename = Prefix + '_re.rnd'
        ImagFilename = Prefix + '_im.rnd'

    try:
        RealFile = open(RealFilename, 'r'); ImagFile = open(ImagFilename, 'r');
        Values = list()
        RealLine = RealFile.readline(); ImagLine = ImagFile.readline()
        while RealLine != '' and ImagLine != '':
            Real = np.float(RealLine)
            Imag = np.float(ImagLine)
            Values.append(np.complex(Real, Imag))
            RealLine = RealFile.readline(); ImagLine = ImagFile.readline()
        return Values
    except IOError:
        print "Problems opening " + Prefix + "_re.rnd or " + Prefix + "_im.rnd for writing."
        return False


def ReadWaveFunctionValuesSpaceSeparatedFile(Filename):
    """
    This function reads wave-function values from a single file which is specified with real and imaginary parts
    alternating in space separated columns.

    Parameters
    -----------
    Filename: str
        Filename to read from.
    """
    try:
        f = open(Filename, 'r')
        Values = list()
        Line = f.readline()
        while Line != '':
            fields = Line.split()
            Values.append(np.asarray(map(lambda x: np.float(x), fields[::2])) +
                          np.asarray(map(lambda x: np.float(x), fields[1::2]))*np.complex(0.0, 1.0))
            Line = f.readline()
        return np.vstack(Values)
    except IOError:
        print "Problem opening " + Filename + '.'
        return False
