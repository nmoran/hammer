#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True
"""
 This is a cython implementation construct Hamiltonian matrices for FQH systems on tori.
"""
import numpy as np
cimport numpy as np
import time
from mpi4py import MPI
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free

try:
    HaveSciPy = True
    import scipy as sp
    import scipy.sparse as sparse
except:
    HaveSciPy = False

try:
    from petsc4py import PETSc
    from slepc4py import SLEPc
    HaveSlepcLibs = True
except ImportError:
    HaveSlepcLibs = False

class FQHTorusHamiltonian:
    """
    Class to group methods and data structures for dealing constructing Hamiltonians for FQH systems on Tori
    """


    def __init__(self, MyBasis, Coefficients, KeepReal=False):
        """
        Constructor to setup class
        """
        self.MyBasis = MyBasis
        self.Coefficients = Coefficients
        self.Dim = MyBasis.GetDimension()
        self.KeepReal = KeepReal


    def CreateSlepcHamiltonian(self, Ham, Assemblies=10):
        """
        Method to create and populate the PETSc matrix data structure.

        TODO: Further optimise to make 10 particles tractable.
        Can try:
        - Using setValues to set the values from arrays instead of individually.
        - Remove duplicates when setting the memory preallocation.
        - Further type and cythonise the code.
        - Profile in more detail to find bottle necks.
        - Matrix is Hermitian so only need to set upper or lower half.

        Performance tracking log
        10/03/2014: Ne=8, Ns=20: 1.22s on single core. Change was to change coefficient data structures to c datatypes.
        01/03/2014: Ne=8, Ns=20: 7.4s on single core. Change was to change dnnz and odnnz to c arrays for the preallocation loop.
        28/02/2014: Ne=8, Ns=20: 14s on single core. Change was to redefine AA and Add with cpdef and specify types.
        28/02/2014: Ne=8, Ns=20: 31s on single core.
        """
        PetscPrint = PETSc.Sys.Print
        Coefficients = self.Coefficients

        # Define c datatypes for coefficients to optimise loops
        cdef int rstart, rend, LocalDim, i, j, k, Col, Row, Dim
        cdef double complex Phase1, Phase2
        cdef int NumPairs, NumDensity
        cdef int *J3Vals
        cdef int *J4Vals
        cdef int **J1Vals
        cdef int **J2Vals
        cdef int *NonZeros
        cdef double complex **SymCoefs
        cdef double nearly_zero = np.finfo(PETSc.ScalarType).eps**2
        cdef int assembly_interval
        cdef int assembly_count
        cdef int *nnz_buffer
        cdef np.ndarray[np.int32_t, ndim=1] Py_dnnz, Py_odnnz
        cdef int *N1Vals
        cdef int *N2Vals
        cdef double complex *DensityCoefs

        # Allocate memory for local copies of interaction coefficients
        Dim = self.Dim
        NumPairs = Coefficients.NumPairs
        NumDensity = Coefficients.NumDensity
        J3Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J4Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J1Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        J2Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        NonZeros = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        SymCoefs = <double complex**>PyMem_Malloc(NumPairs * sizeof(double complex*))
        N1Vals = <int *>PyMem_Malloc(NumDensity * sizeof(int))
        N2Vals = <int *>PyMem_Malloc(NumDensity * sizeof(int))
        DensityCoefs = <double complex *>PyMem_Malloc(NumDensity * sizeof(double complex))
        j = 0
        while j < NumPairs:
            NonZeros[j] = Coefficients.NonZeros[j]
            J3Vals[j] = Coefficients.J3J4Vals[j,0]
            J4Vals[j] = Coefficients.J3J4Vals[j,1]
            J1Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            J2Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            SymCoefs[j] = <double complex*>PyMem_Malloc(NonZeros[j] * sizeof(double complex))
            k = 0
            while k < NonZeros[j]:
                J1Vals[j][k] = Coefficients.J1J2Vals[j][k,0]
                J2Vals[j][k] = Coefficients.J1J2Vals[j][k,1]
                SymCoefs[j][k] = Coefficients.AntiSymCoefs[j][k]
                k += 1
            j += 1

        j = 0
        while j < NumDensity:
            N1Vals[j] = Coefficients.N1N2Vals[j][0]
            N2Vals[j] = Coefficients.N1N2Vals[j][1]
            DensityCoefs[j] = Coefficients.DensityCoefs[j]
            j += 1
        # end of assigning c datatypes

        if Ham is None:
            # Creating temporary matrix to get the local dimension as cannot preallocate after doing setUp and cannot
            # get the local dimension without calling setUp.
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([Dim, Dim])
            Ham.setFromOptions()
            Ham.setUp()
            rstart, rend = Ham.getOwnershipRange()
            Ham.destroy()

            # Create final matrix this time
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([Dim, Dim])
            Ham.setFromOptions()

            LocalDim = rend - rstart # dimension of local portion of matrix, by row

            # Create python objects for these to pass to Preallocation function
            Py_dnnz = np.ones(LocalDim, dtype=np.int32)  # Diagonal non zeros
            Py_odnnz = np.zeros(LocalDim, dtype=np.int32) # off diagonal non zeros
            j = 0
            local_elements = np.array(0, dtype=np.int64)
            global_elements = np.array(0, dtype=np.int64)

            # Create C arrays to count the number of non zero entries for each row, distinguish by those that are in the diagonal block and those that are not.
            nnz_buffer = <int *>PyMem_Malloc(Dim * sizeof(int)) # Allocate space for a buffer to count non zero column entries
            j = 0
            while j < Dim:
                nnz_buffer[j] = -1
                j += 1

            start = time.time()
            Row = rstart
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j][k], J2Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0 and Col < Dim:
                                    if nnz_buffer[Col] < Row: # otherwise, this column has already been counted.
                                        if Col >= rstart and Col < rend:
                                            #if Col != Row : dnnz[Row-rstart] += 1
                                            if Col != Row : Py_dnnz[Row-rstart] += 1
                                        else:
                                            Py_odnnz[Row-rstart] += 1
                                        nnz_buffer[Col] = Row # gives last row where this column was occupied
                            k += 1
                    j += 1

                local_elements += <long>(Py_dnnz[Row-rstart] + Py_odnnz[Row-rstart])
                Row += 1

            PETSc.COMM_WORLD.tompi4py().Allreduce([local_elements, MPI.LONG], [global_elements, MPI.LONG], op=MPI.SUM)

            Ham.setPreallocationNNZ((Py_dnnz,Py_odnnz))
            Ham.setUp()
            PyMem_Free(nnz_buffer)
            end = time.time()
            PetscPrint("Preallocation: %.3gs (for %ld non zero values with local count %ld)" % (end-start, global_elements, local_elements))
        else:
            start = time.time()
            rstart, rend = Ham.getOwnershipRange()
            Ham.zeroEntries()
            end = time.time()
            PetscPrint("Preallocation (only clearing): %.3gs" % (end-start))


        assembly_intervals = int((rend - rstart) / Assemblies)
        assembly_count = 0
        start = time.time()
        Row = rstart
        if PETSc.ScalarType == np.complex128:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j][k], J2Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1 * Phase2 * SymCoefs[j][k], True)
                            k += 1
                    j += 1

                # now look at density density terms
                j = 0
                while j < NumDensity:
                    Phase1 = self.MyBasis.NN( N1Vals[j], N2Vals[j], Row)
                    if Phase1 != 0.0:
                        Ham.setValue(Row, Row, Phase1 * DensityCoefs[j], True)
                    j += 1

                if (Row - rstart) > Row and (Row - rstart) % assembly_intervals == 0 and assembly_count < (Assemblies - 1):
                    Ham.assemble(assembly=PETSc.Mat.AssemblyType.FLUSH)
                    assembly_count += 1
                Row += 1
        else:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j][k], J2Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1.real * Phase2.real * SymCoefs[j][k].real, True)
                            k += 1
                    j += 1

                # now look at density density terms
                j = 0
                while j < NumDensity:
                    Phase1 = self.MyBasis.NN( N1Vals[j], N2Vals[j], Row)
                    if Phase1 != 0.0:
                        Ham.setValue(Row, Row, Phase1.real * DensityCoefs[j].real, True)
                    j += 1

                if (Row - rstart) > Row and (Row - rstart) % assembly_intervals == 0 and assembly_count < (Assemblies - 1):
                    Ham.assemble(assembly=PETSc.Mat.AssemblyType.FLUSH)
                    assembly_count += 1
                Row += 1

        # keep aseembling until everyone is finished.
        while assembly_count < (Assemblies -1):
            Ham.assemble(assembly=PETSc.Mat.AssemblyType.FLUSH)
            assembly_count += 1
        Ham.assemble(assembly=PETSc.Mat.AssemblyType.FINAL)

        # Deallocate space used for c datatypes during preallocation.
        PyMem_Free(J3Vals)
        PyMem_Free(J4Vals)
        j = 0
        while j < NumPairs:
            PyMem_Free(J1Vals[j])
            PyMem_Free(J2Vals[j])
            PyMem_Free(SymCoefs[j])
            j += 1
        PyMem_Free(NonZeros)
        PyMem_Free(J1Vals)
        PyMem_Free(J2Vals)
        PyMem_Free(SymCoefs)
        # Finished deallocating space

        end = time.time()
        PetscPrint("Construction: %.3gs." % (end-start))

        return Ham


    def CalculateDiagonal(self):
        """
        Method to calculate the values on the diagonal only.
        """
        cdef int rstart, rend, LocalDim, i, j, k, Col, Row
        cdef double Phase1, Phase2

        PetscPrint = PETSc.Sys.Print


        # Define c datatypes for coefficients to optimise loops
        Coefficients = self.Coefficients
        cdef int NumPairs
        cdef int *J3Vals
        cdef int *J4Vals
        cdef int *J1Vals
        cdef int *J2Vals
        cdef int *NonZeros
        cdef double complex *AntiSymCoefs

        NumPairs = Coefficients.NumPairs
        J3Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J4Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J1Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J2Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        NonZeros = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        AntiSymCoefs = <double complex *>PyMem_Malloc(NumPairs * sizeof(double complex))
        j = 0
        while j < NumPairs:
            NonZeros[j] = Coefficients.NonZeros[j]
            J3Vals[j] = Coefficients.J3J4Vals[j,0]
            J4Vals[j] = Coefficients.J3J4Vals[j,1]
            k = 0
            while k < NonZeros[j]:
                if Coefficients.J1J2Vals[j][k,0] == J3Vals[j] and Coefficients.J1J2Vals[j][k,1] == J4Vals[j]:
                    J1Vals[j] = Coefficients.J1J2Vals[j][k,0]
                    J2Vals[j] = Coefficients.J1J2Vals[j][k,1]
                    AntiSymCoefs[j] = Coefficients.AntiSymCoefs[j][k]
                k += 1
            j += 1
        # end of assigning c datatypes

        Diagonal = np.zeros(self.Dim)

        start = time.time()
        Row = 0
        rend = self.Dim
        if PETSc.ScalarType == np.complex128:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
    #                    print "Non zero"
                        if J1Vals[j] == J3Vals[j] and J2Vals[j] == J4Vals[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j], J2Vals[j])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Diagonal[Row] += Phase1 * Phase2 * AntiSymCoefs[j]
                    j += 1
                Row += 1
        else:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AA(J3Vals[j], J4Vals[j], Row)
                    if Phase1 != 0.0:
    #                    print "Non zero"
                        if J1Vals[j] == J3Vals[j] and J2Vals[j] == J4Vals[j]:
                            Phase2 = self.MyBasis.AdAd(J1Vals[j], J2Vals[j])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Diagonal[Row] += Phase1 * Phase2 * AntiSymCoefs[j].real
                    j += 1
                Row += 1


        # Deallocate space used for c datatypes during preallocation.
        PyMem_Free(J3Vals)
        PyMem_Free(J4Vals)
        PyMem_Free(J1Vals)
        PyMem_Free(J2Vals)
        PyMem_Free(AntiSymCoefs)

        end = time.time()
        PetscPrint("Construction: %.3gs." % (end-start))

        return Diagonal


    def CreateSlepc3BodyHamiltonian(self, Ham):
        """
        Method to create and populate the PETSc matrix data structure.

        Performance tracking log
        """
        cdef int rstart, rend, LocalDim, i, j, k, Col, Row, Dim
        cdef double complex Phase1, Phase2
        cdef int NumPairs, NumDensity
        # Define c datatypes for coefficients to optimise loops
        cdef int *J4Vals
        cdef int *J5Vals
        cdef int *J6Vals
        cdef int **J1Vals
        cdef int **J2Vals
        cdef int **J3Vals
        cdef int *NonZeros
        cdef double complex **SymCoefs
        cdef int *nnz_buffer
        cdef np.ndarray[np.int32_t, ndim=1] Py_dnnz, Py_odnnz
        cdef int *N1Vals
        cdef int *N2Vals
        cdef int *N3Vals
        cdef double complex *DensityCoefs

        Dim = self.Dim
        Coefficients = self.Coefficients
        NumPairs = Coefficients.NumPairs
        NumDensity = Coefficients.NumDensity
        J4Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J5Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J6Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J1Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        J2Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        J3Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        NonZeros = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        SymCoefs = <double complex**>PyMem_Malloc(NumPairs * sizeof(double complex*))
        N1Vals = <int *>PyMem_Malloc(NumDensity * sizeof(int))
        N2Vals = <int *>PyMem_Malloc(NumDensity * sizeof(int))
        N3Vals = <int *>PyMem_Malloc(NumDensity * sizeof(int))
        DensityCoefs = <double complex *>PyMem_Malloc(NumDensity * sizeof(double complex))
        j = 0
        while j < NumPairs:
            NonZeros[j] = Coefficients.NonZeros[j]
            J4Vals[j] = Coefficients.J4J5J6Vals[j,0]
            J5Vals[j] = Coefficients.J4J5J6Vals[j,1]
            J6Vals[j] = Coefficients.J4J5J6Vals[j,2]
            J1Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            J2Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            J3Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            SymCoefs[j] = <double complex*>PyMem_Malloc(NonZeros[j] * sizeof(double complex))
            k = 0
            while k < NonZeros[j]:
                J1Vals[j][k] = Coefficients.J1J2J3Vals[j][k,0]
                J2Vals[j][k] = Coefficients.J1J2J3Vals[j][k,1]
                J3Vals[j][k] = Coefficients.J1J2J3Vals[j][k,2]
                SymCoefs[j][k] = Coefficients.SymCoefs[j][k]
                k += 1
            j += 1
        j = 0
        while j < NumDensity:
            N1Vals[j] = Coefficients.N1N2N3Vals[j][0]
            N2Vals[j] = Coefficients.N1N2N3Vals[j][1]
            N3Vals[j] = Coefficients.N1N2N3Vals[j][2]
            DensityCoefs[j] = Coefficients.DensityCoefs[j]
            j += 1
        # end of assigning c datatypes

        PetscPrint = PETSc.Sys.Print

        if Ham is None:
            # Creating temporary matrix to get the local dimension as cannot preallocate after doing setUp and cannot
            # get the local dimension without calling setUp.
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([Dim, Dim])
            Ham.setFromOptions()
            Ham.setUp()
            rstart, rend = Ham.getOwnershipRange()
            Ham.destroy()

            # Create final matrix this time
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([Dim, Dim])
            Ham.setFromOptions()

            LocalDim = rend - rstart # dimension of local portion of matrix, by row

             # Create python objects for these to pass to Preallocation function
            Py_dnnz = np.ones(LocalDim, dtype=np.int32)  # Diagonal non zeros
            Py_odnnz = np.zeros(LocalDim, dtype=np.int32) # off diagonal non zeros
            j = 0
            local_elements = np.array(0, dtype=np.int64)
            global_elements = np.array(0, dtype=np.int64)

            # Create C arrays to count the number of non zero entries for each row, distinguish by those that are in the diagonal block and those that are not.
            nnz_buffer = <int *>PyMem_Malloc(Dim * sizeof(int)) # Allocate space for a buffer to count non zero column entries
            j = 0
            while j < Dim:
                nnz_buffer[j] = -1
                j += 1

            start = time.time()
            Row = rstart
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AAA(J4Vals[j], J5Vals[j], J6Vals[j], Row)
                    #print "Applied AAA with " + str([J4Vals[j], J5Vals[j], J6Vals[j]]) + " to " + self.MyBasis.GetOccupationRep(Row) + " and got phase " + str(Phase1)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAdAd(J1Vals[j][k], J2Vals[j][k], J3Vals[j][k])
                            #print "Applied AdAdAd with " + str([J1Vals[j][k], J2Vals[j][k], J3Vals[j][k]]) + " to " + self.MyBasis.GetOccupationRep(Row) + " and got phase " + str(Phase2)
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                #print "Got col index " + str(Col)
                                if Col >= 0 and Col < Dim:
                                    if nnz_buffer[Col] < Row: # otherwise, this column has already been counted.
                                        if Col >= rstart and Col < rend:
                                            if Col != Row : Py_dnnz[Row-rstart] += 1
                                        else:
                                            Py_odnnz[Row-rstart] += 1
                                        nnz_buffer[Col] = Row # gives last row where this column was occupied
                            k += 1
                    j += 1

                local_elements += <long>(Py_dnnz[Row-rstart] + Py_odnnz[Row-rstart])
                Row += 1

            PETSc.COMM_WORLD.tompi4py().Allreduce([local_elements, MPI.LONG], [global_elements, MPI.LONG], op=MPI.SUM)

            Ham.setPreallocationNNZ((Py_dnnz,Py_odnnz))
            Ham.setUp()
            PyMem_Free(nnz_buffer)
            end = time.time()
            PetscPrint("Preallocation: %.3gs (for %ld non zero values with local count %ld)" % (end-start, global_elements, local_elements))
        else:
            start = time.time()
            rstart, rend = Ham.getOwnershipRange()
            Ham.zeroEntries()
            end = time.time()
            PetscPrint("Preallocation (only clearing): %.3gs" % (end-start))


        start = time.time()
        Row = rstart
        if PETSc.ScalarType == np.complex128:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AAA(J4Vals[j], J5Vals[j], J6Vals[j], Row)
                    if Phase1 != 0.0:
    #                    print "Non zero"
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAdAd(J1Vals[j][k], J2Vals[j][k], J3Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1 * Phase2 * SymCoefs[j][k], True)
    #                                print "Setting " + str(Row) + ", " + str(Col) + " to " + str(Phase1 * Phase2 * Coefficients.AntiSymCoefs[j][k])
                            k += 1
                    j += 1

                # now look at density density terms
                j = 0
                while j < NumDensity:
                    Phase1 = self.MyBasis.NNN(N1Vals[j], N2Vals[j], N3Vals[j], Row)
                    if Phase1 != 0.0:
                        Ham.setValue(Row, Row, Phase1 * DensityCoefs[j], True)
                    j += 1

                Row += 1
        else:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AAA(J4Vals[j], J5Vals[j], J6Vals[j], Row)
                    if Phase1 != 0.0:
    #                    print "Non zero"
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAdAd(J1Vals[j][k], J2Vals[j][k], J3Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1 * Phase2 * SymCoefs[j][k].real, True)
    #                                print "Setting " + str(Row) + ", " + str(Col) + " to " + str(Phase1 * Phase2 * Coefficients.AntiSymCoefs[j][k])
                            k += 1
                    j += 1

                # now look at density density terms
                j = 0
                while j < NumDensity:
                    Phase1 = self.MyBasis.NNN(N1Vals[j], N2Vals[j], N3Vals[j], Row)
                    if Phase1 != 0.0:
                        Ham.setValue(Row, Row, Phase1 * DensityCoefs[j].real, True)
                    j += 1
                Row += 1

        Ham.assemble()

        # Deallocate space used for c datatypes during preallocation.
        PyMem_Free(J4Vals)
        PyMem_Free(J5Vals)
        PyMem_Free(J6Vals)
        j = 0
        while j < NumPairs:
            PyMem_Free(J1Vals[j])
            PyMem_Free(J2Vals[j])
            PyMem_Free(J3Vals[j])
            PyMem_Free(SymCoefs[j])
            j += 1
        PyMem_Free(NonZeros)
        PyMem_Free(J1Vals)
        PyMem_Free(J2Vals)
        PyMem_Free(J3Vals)
        PyMem_Free(SymCoefs)
        # Finished deallocating space

        end = time.time()
        PetscPrint("Construction: %.3gs" % (end-start))

        return Ham


    def CreateSlepc4BodyHamiltonian(self, Ham):
        """
        Method to create and populate the PETSc matrix data structure.

        Can try:

        Performance tracking log
        """
        cdef int rstart, rend, LocalDim, i, j, k, Col, Row
        cdef double Phase1
        cdef double complex Phase2
        cdef int* dnnz
        cdef int* odnnz

        # Define c datatypes for coefficients to optimise loops
        cdef int NumPairs
        cdef int *J5Vals
        cdef int *J6Vals
        cdef int *J7Vals
        cdef int *J8Vals
        cdef int **J1Vals
        cdef int **J2Vals
        cdef int **J3Vals
        cdef int **J4Vals
        cdef int *NonZeros
        cdef double complex **SymCoefs

        Coefficients = self.Coefficients
        NumPairs = Coefficients.NumPairs
        J5Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J6Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J7Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J8Vals = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        J1Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        J2Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        J3Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        J4Vals = <int **>PyMem_Malloc(NumPairs * sizeof(int*))
        NonZeros = <int *>PyMem_Malloc(NumPairs * sizeof(int))
        SymCoefs = <double complex**>PyMem_Malloc(NumPairs * sizeof(double complex*))
        j = 0
        while j < NumPairs:
            NonZeros[j] = Coefficients.NonZeros[j]
            J5Vals[j] = Coefficients.J5J6J7J8Vals[j,0]
            J6Vals[j] = Coefficients.J5J6J7J8Vals[j,1]
            J7Vals[j] = Coefficients.J5J6J7J8Vals[j,2]
            J8Vals[j] = Coefficients.J5J6J7J8Vals[j,3]
            J1Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            J2Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            J3Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            J4Vals[j] = <int *>PyMem_Malloc(NonZeros[j] * sizeof(int))
            SymCoefs[j] = <double complex*>PyMem_Malloc(NonZeros[j] * sizeof(double complex))
            k = 0
            while k < NonZeros[j]:
                J1Vals[j][k] = Coefficients.J1J2J3J4Vals[j][k,0]
                J2Vals[j][k] = Coefficients.J1J2J3J4Vals[j][k,1]
                J3Vals[j][k] = Coefficients.J1J2J3J4Vals[j][k,2]
                J4Vals[j][k] = Coefficients.J1J2J3J4Vals[j][k,3]
                SymCoefs[j][k] = Coefficients.SymCoefs[j][k]
                k += 1
            j += 1
        # end of assigning c datatypes

        PetscPrint = PETSc.Sys.Print

        if Ham is None:
            # Creating temporary matrix to get the local dimension as cannot preallocate after doing setUp and cannot
            # get the local dimension without calling setUp.
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([self.Dim, self.Dim])
            Ham.setFromOptions()
            Ham.setUp()
            rstart, rend = Ham.getOwnershipRange()
            Ham.destroy()

            # Create final matrix this time
            Ham = PETSc.Mat(); Ham.create()
            Ham.setSizes([self.Dim, self.Dim])
            Ham.setFromOptions()

            LocalDim = rend - rstart # dimension of local portion of matrix, by row

            # Create C arrays to count the number of non zero entries for each row, distinguish by those that are in the diagonal block and those that are not.
            dnnz = <int *>PyMem_Malloc(LocalDim * sizeof(int))  # Diagonal non zeros
            odnnz = <int *>PyMem_Malloc(LocalDim * sizeof(int))  # off diagonal non zeros
            j = 0
            while j < LocalDim:
                dnnz[j] = 0
                odnnz[j] = 0
                j += 1


            start = time.time()


            Row = rstart
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AAAA(J5Vals[j], J6Vals[j], J7Vals[j], J8Vals[j], Row)
                    #print "Applied AAA with " + str([J4Vals[j], J5Vals[j], J6Vals[j]]) + " to " + self.MyBasis.GetOccupationRep(Row) + " and got phase " + str(Phase1)
                    if Phase1 != 0.0:
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAdAdAd(J1Vals[j][k], J2Vals[j][k], J3Vals[j][k], J4Vals[j][k])
                            #print "Applied AdAdAd with " + str([J1Vals[j][k], J2Vals[j][k], J3Vals[j][k]]) + " to " + self.MyBasis.GetOccupationRep(Row) + " and got phase " + str(Phase2)
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                #print "Got col index " + str(Col)
                                if Col >= 0:
                                    if Col >= rstart and Col < rend:
                                        dnnz[Row-rstart] += 1
                                    else:
                                        odnnz[Row-rstart] += 1
                            k += 1
                    j += 1
                if dnnz[Row-rstart] > LocalDim:
                    dnnz[Row-rstart] = LocalDim
                #print "D Non zeros for row " + str(Row) + ": " + str(dnnz[Row-rstart])
                if odnnz[Row-rstart] > (self.Dim - LocalDim):
                    odnnz[Row-rstart] = self.Dim - LocalDim
                #print "OD Non zeros for row " + str(Row) + ": " + str(odnnz[Row-rstart])
                Row += 1


            # Create python objects for these to pass to Preallocation function
            Py_dnnz = np.zeros(LocalDim,np.int32)  # Diagonal non zeros
            Py_odnnz = np.zeros(LocalDim,np.int32) # off diagonal non zeros

            j = 0
            while j < LocalDim:
                Py_dnnz[j] = dnnz[j]
                Py_odnnz[j] = odnnz[j]
                j += 1

            Ham.setPreallocationNNZ((Py_dnnz,Py_odnnz))
            Ham.setUp()
            PyMem_Free(dnnz)
            PyMem_Free(odnnz)
            end = time.time()
            PetscPrint("Preallocation: %.3gs" % (end-start))
        else:
            start = time.time()
            rstart, rend = Ham.getOwnershipRange()
            Ham.zeroEntries()
            end = time.time()
            PetscPrint("Preallocation (only clearing): %.3gs" % (end-start))


        start = time.time()
        Row = rstart
        if PETSc.ScalarType == np.complex128:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AAAA(J5Vals[j], J6Vals[j], J7Vals[j], J8Vals[j], Row)
                    if Phase1 != 0.0:
    #                    print "Non zero"
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAdAdAd(J1Vals[j][k], J2Vals[j][k], J3Vals[j][k], J4Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1 * Phase2 * SymCoefs[j][k], True)
    #                                print "Setting " + str(Row) + ", " + str(Col) + " to " + str(Phase1 * Phase2 * Coefficients.AntiSymCoefs[j][k])
                            k += 1
                    j += 1
                Row += 1
        else:
            while Row < rend:
                j = 0
                while j < NumPairs: # loop over pairs of j3, j4
                    Phase1 = self.MyBasis.AAAA(J5Vals[j], J6Vals[j], J7Vals[j], J8Vals[j], Row)
                    if Phase1 != 0.0:
    #                    print "Non zero"
                        k = 0
                        while k < NonZeros[j]:
                            Phase2 = self.MyBasis.AdAdAdAd(J1Vals[j][k], J2Vals[j][k], J3Vals[j][k], J4Vals[j][k])
                            if Phase2 != 0.0:
                                Col = self.MyBasis.BinaryArraySearchDescending()
                                if Col >= 0:
                                    Ham.setValue(Row, Col, Phase1 * Phase2 * SymCoefs[j][k].real, True)
    #                                print "Setting " + str(Row) + ", " + str(Col) + " to " + str(Phase1 * Phase2 * Coefficients.AntiSymCoefs[j][k])
                            k += 1
                    j += 1
                Row += 1

        Ham.assemble()

        # Deallocate space used for c datatypes during preallocation.
        PyMem_Free(J5Vals)
        PyMem_Free(J6Vals)
        PyMem_Free(J7Vals)
        PyMem_Free(J8Vals)
        j = 0
        while j < NumPairs:
            PyMem_Free(J1Vals[j])
            PyMem_Free(J2Vals[j])
            PyMem_Free(J3Vals[j])
            PyMem_Free(J4Vals[j])
            PyMem_Free(SymCoefs[j])
            j += 1
        PyMem_Free(NonZeros)
        PyMem_Free(J1Vals)
        PyMem_Free(J2Vals)
        PyMem_Free(J3Vals)
        PyMem_Free(J4Vals)
        PyMem_Free(SymCoefs)
        # Finished deallocating space

        end = time.time()
        PetscPrint("Construction: %.3gs" % (end-start))

        return Ham
