#! /usr/bin/env python

""" utils.py: General utility functions for Hammer. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import csv
import numpy as np


def ReadCSVFile(filename):
    """
    Function to read a CSV file and return a list of the rows.
    """
    f = open(filename, 'r')
    csv_reader = csv.reader(f)
    Rows = list()
    for row in csv_reader:
        Rows.append(row)

    f.close()
    return Rows


def GetLengths(Tau, Ns, MagLength=1.0):
    """
    Given the modular parameter tau and the number of flux return [L1, L2].
    """
    Theta = np.angle(Tau)
    SinTheta = np.sin(Theta)
    CosTheta = np.cos(Theta)
    Ratio = np.imag(Tau)/SinTheta
    L1 = np.sqrt(2.0*np.pi*np.float(Ns)*MagLength**2/(Ratio*SinTheta))
    L2 = Ratio * L1

    return [L1, L2]


def FundamentalDomainCoveringTaus(MaxRatio, Xdim, Ydim):
    """
    Generate a list of taus that will cover a fundamental domain up to the specified maximum
    ratio with the specified number of points in each direction.

    Parameters
    ----------
    MaxRatio: float
        The maximum ratio between the torus sides.
    Xdim: int
        Dimension in x direction.
    Ydim: int
        Dimension in y direction.
    """

    Dims = [Xdim, Ydim] # grid dimensions

    xPositions = np.linspace(0.0,0.5,Dims[0])
    yMin = np.sqrt(1.0 - xPositions**2)
    yMax = np.sqrt(MaxRatio**2 - xPositions**2)
    print(yMax)
    yMinLog = np.log(yMin)
    yMaxLog = np.log(yMax)
    print(yMinLog)
    print(yMaxLog)

    Xs = list()
    Ys = list()

    for j in range(Dims[1]):
        for i in range(Dims[0]):
            Xs.append(xPositions[i])
            Ys.append(np.exp(np.linspace(yMinLog[i], yMaxLog[i], Dims[1])[j]))

    Taus = [np.complex(x[0], x[1]) for x in zip(Xs, Ys)]

    return Taus


def FundamentalDomainTausAroundL1(L1, Width, Xdim, Ydim, NbrFlux):
    """
    Generate a list of taus that cross the fundamental domain with specified width around the specified L1 point.

    Parameters
    ----------
    L1: float
        The value of L1 to generate points around.
    Width: float
        The space around L1 to span, on L1 scale.
    Xdim: int
        Dimension in x direction.
    Ydim: int
        Dimension in y direction.
    NbrFlux: int
        The number of flux on the torus.
    """

    Dims = [Xdim, Ydim] # grid dimensions
    L1s = np.linspace(L1-Width/2.0, L1+Width/2.0, Dims[1])
    xPositions = np.linspace(0.0,0.5,Dims[0])
    Xs = list()
    Ys = list()

    for j in range(Dims[1]):
        for i in range(Dims[0]):
            Xs.append(xPositions[i])
            if xPositions[i] == 0:
                theta = np.pi/2
            else:
                theta = np.arctan(2 * np.pi * NbrFlux/(xPositions[i]*L1s[j]**2)) # get theta from real part of tau and L1
            L2 = 2*np.pi*NbrFlux/(np.sin(theta)*L1s[j])
            Ys.append((L2/L1s[j])*np.sin(theta))

    Taus = [np.complex(x[0], x[1]) for x in zip(Xs, Ys)]

    return Taus
