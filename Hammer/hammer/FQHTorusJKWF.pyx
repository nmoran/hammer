#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusJKWF.pyx: Cython code for calculating values of single particle torus wave-functions.

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2017"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"
"""

from hammer.FQHTorusWF import FQHTorusWF

import numpy as np
cimport numpy as np
import sys
try:
    HaveJacobiThetaModule = True
    from jacobitheta import *
except:
    HaveJacobiThetaModule = False


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)
    double round(double x)


cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double complex conj(double complex x) nogil




cdef class FQHTorusJKWF:
    """
    This class represents composite fermion wave function for a given occupation of a Lambda-level structure.
    Here we assume that the tau-gauge is used which in reduced coordinates z=L(x+tau*y) reads A = B(-y,0)
    """

    cdef int NbrFlux,Ne,FP,Steps
    cdef double complex Tau,TauB
    cdef double pi,delta,gamma,L1,LLLNorm
    ### Define a niew into an array
    cdef int [:] LLconf, MomConf

    def __init__(self, int NbrFlux, Momenta, LLs, double complex Tau=1j, int fluxpairs=1, LL_force=False,Steps=1):
        """
        Constructor. Sets the number of lambda-level fluxes, the LL/Momenta occupation data and the torus parameter tau.
        """
        self.NbrFlux = NbrFlux
        self.FP = fluxpairs
        self.Tau = Tau
        ### The TorusWF module uses -bar(tau) instead of tau as input!
        self.TauB = -np.conj(Tau)
        self.Steps = Steps        

        #### LLs should be a vector/list containting all the LL:occupations
        #### Momenta should be a vbectors list containing the corresponding Momenta
        ERRTEXT="The LL configutation does not have the same length as the Momentum configurations"
        assert(len(LLs)==len(Momenta),ERRTEXT)
        
        ###FIXME: check all LL:s are positive

        # calculate derived values
        self.pi = np.pi
        self.Ne = len(LLs)

        ###Intialize the LL and mom-config
        self.LLconf = np.array(LLs,dtype=np.int32)
        self.MomConf = np.array(Momenta,dtype=np.int32)
        
        SumLLs=np.sum(LLs)
        if SumLLs==0 or LL_force:
            self.delta=0.0
            self.gamma=0.0
        else:
            self.delta = 1.0/(self.FP*SumLLs)
            self.gamma = (self.Ne*1.0) / SumLLs

        ####Set the Lambda-level-norm
        self.L1 = np.sqrt(2.0*self.pi*self.NbrFlux/(np.imag(self.Tau)))
        #print("L1:",self.L1)
        self.LLLNorm = 1.0/np.sqrt(self.L1* np.sqrt(self.pi))
        #print("LLLNorm:",self.LLLNorm)

    cpdef JK_SP_Unproj(self,XYList,int Neindx,int LLindx):
        cdef double complex tau = self.Tau
        cdef double complex tauB = self.TauB
        cdef double complex xy
        xy = XYList[Neindx]
        ###Compute the oribital part###
        WFN=FQHTorusWF(self.NbrFlux,Tau=tauB,LL=self.LLconf[LLindx])
        wfn1=WFN.WFValue(xy,self.MomConf[LLindx])
        XYOthers=np.concatenate((XYList[0:Neindx],XYList[(Neindx+1):self.Ne]))
        xyDiff=xy-XYOthers
        ###Add the jastrow factor
        if HaveJacobiThetaModule:
            LogJastrow=0.0
            for indx in range(self.Ne-1):
                LogJastrow+=(
                    ##Gaussian z_i
                    1j*self.pi*tau*(np.imag(xy))**2
                    ###Gaussian z_j
                    +1j*self.pi*tau*(np.imag(XYOthers[indx]))**2
                    ###Jastrow factor component
                    +jacobitheta.logtheta1(np.real(xyDiff[indx])
                                       +tau*np.imag(xyDiff[indx]),tau))
        else:
            raise RuntimeError("ERROR: JacobiThetaModule not installed!")
        return wfn1*np.exp(LogJastrow*self.FP)


    cpdef double complex JK_SP_Proj(self,double complex [:] XYInput,int Neindx,int LLindx,bint log=False):
        if not HaveJacobiThetaModule:
            raise RuntimeError("ERROR: JacobiThetaModule not installed!")

        ###The JK projection is the to mangetically translate the CF-orbital in the LLL by LL*eps,
        ###the JK wnf is   psi_(M(z+\gamma|M\tau)*\prod(z_ij+\delta)
        ### Where \delta = 1/(fluxpairs*\sum(ll:s))
        ###       \gamma = \delta*fluxpairs*Ne/M = Ne / ( M * sum(ll:s))
        cdef double complex tau = self.Tau
        cdef double complex LogJastrow,ipitau,xyother,xydiff,LLLPsi,wfnLambda
        cdef int LL = self.LLconf[LLindx]
        cdef int Mom = self.MomConf[LLindx]
        cdef double complex xy = XYInput[Neindx]
        cdef int indx

        ###Compute the oribital part###
        wfnLambda = self.LL0WFn(xy+(LL*self.Steps*self.gamma)/self.NbrFlux,Mom)
        if log: #Compute in log scale
            wfnLambda = np.log(wfnLambda)

        indx = 0 
        LogJastrow=0.0
        ipitau=1j*self.pi*tau
        while indx < self.Ne:
            if indx != Neindx: ###Do not include the self factor
                xyother=XYInput[indx]
                xydiff=xy-xyother
                
                ##Gaussian z_i
                LogJastrow+=ipitau*(xy.imag**2)
                ###Gaussian z_j
                LogJastrow+=ipitau*(xyother.imag**2)
                ###Jastrow factor component
                LogJastrow+=jacobitheta.logtheta1(xydiff.real+tau*xydiff.imag-LL*self.Steps*self.delta,tau)
                #print "n,LogJastrow:",indx,LogJastrow
            indx+=1
        if log: #Compute in log scale
            LLLPsi=wfnLambda+LogJastrow*self.FP
        else:
            LLLPsi=wfnLambda*cexp(LogJastrow*self.FP)
        return LLLPsi

    cpdef double complex CoMfunction(self,double complex [:] XYList,bint log=False):
        cdef double complex tau = self.Tau
        cdef double complex pi = self.pi
        cdef double complex Gaussian,ipitau2fp,Ztot,Theta,Res
        cdef int FP = self.FP
        cdef int n
        ###Compute Gaussian and Ztot
        ipitau2fp = 1j*pi*tau*2*FP
        Gaussian=0.0
        Ztot=0.0
        n=0
        while n < self.Ne:
            Gaussian+=(XYList[n].imag)**2
            Ztot+=XYList[n].real+tau*XYList[n].imag
            n+=1
        Gaussian*=ipitau2fp
        Theta=jacobitheta.logtheta3(2*FP*Ztot,2*FP*tau)
        Res=Theta+Gaussian
        if log: return Res
        else: return cexp(Res)


    cpdef double complex LL0WFn(self, double complex xy, double momentum):
        """
        Evaulate the LL0 wave-function using the external JacobiTheta module one value at a time
        """
        cdef double Ns = <double>self.NbrFlux
        cdef double n = momentum
        cdef double complex tau = self.Tau
        cdef double complex tau_gauge = 1j * self.pi * Ns * tau * (xy.imag**2)
        cdef double complex theta
        cdef double basis_norm = self.LLLNorm

        if HaveJacobiThetaModule:
            theta = jacobitheta.logthetagen(1.0*n/Ns, 0.0, Ns*(xy.real+tau*xy.imag), Ns*tau)
            return cexp(tau_gauge + theta) * basis_norm
        else:
            raise RuntimeError("ERROR: JacobiThetaModule not installed!\nWFValueJT can not be used in this case.")
        

    def WFValue(self, xyArray,bint det=True):
        """
        Calculate the value of the wave-function at xy. Handles vectorwized input of xy
        """
        cdef int n,MC,Ne
        cdef double complex [:] xyval
        cdef double complex [:] wfArray
        
        Dim=xyArray.ndim
        if Dim == 1: ### Just one configuration
            return self.WFValueSerial(xyArray,det=det)
        else: ### More than one configuration
            (MC,Ne)=xyArray.shape
            ERRTEXT="The number of electrons in the "+str(MC)+" coordinate configuratins (Ne="+str(Ne)+") are not he same as the precomputed number of electrons (Ne="+str(self.Ne)+")"
            if Ne!=self.Ne:
                raise Exception, ERRTEXT
            wfArray=np.zeros(MC,dtype=np.complex128)
            n = 0
            while n < MC:
                #print(n+1,"of",MC)
                xyval=xyArray[n,:]
                wfArray[n] = self.WFValueSerial(xyval,det=det)
                n+=1
            return wfArray

    cdef double complex WFValueSerial(self,double complex [:] XYwiev,bint det=True):
        """
        Calculate the value of the wave-function at xy (assumes xy is just one position)
        """
        cdef double complex ProdWfn,ProdFactor,Value,TotFunction,ExpTotFunction,CoMFunction
        cdef double complex [:,:] LogDetMatrix
        cdef double complex [:,:] DetMatrix

        cdef int Ne = self.Ne
                
        if not det: ###Only construct a product (choose as the diagonal product through the determinant matrix)
            ProdWfn=0.0
            #print "Compute product"
            #print "XYwiev:",XYwiev
            for NeIndx in range(Ne):
                #print("Values:",self.JK_SP_Proj(XYwiev,NeIndx,NeIndx,log=True))
                ProdWfn+=self.JK_SP_Proj(XYwiev,NeIndx,NeIndx,log=True)
                #print("ProdWfn:",ProdWfn)
        else: ### Here also compute the determinant
            LogDetMatrix=np.zeros((Ne,Ne),dtype=np.complex128)
            DetMatrix=np.zeros((Ne,Ne),dtype=np.complex128)
            for StateIndx in range(Ne):
                for ElIndx in range(Ne):
                    LogDetMatrix[ElIndx,StateIndx]=self.JK_SP_Proj(XYwiev,ElIndx,StateIndx,log=True)
            #print "LogDetMatrix:\n",LogDetMatrix
            ###Remove a scale factor from each row, to make the determinant close to one
            ###The factor is the multiplied back in
            ProdFactor=0.0
            for StateIndx in range(Ne):
                Value=LogDetMatrix[0,StateIndx]
                ProdFactor+=Value
                for ElIndx in range(Ne):
                    LogDetMatrix[ElIndx,StateIndx]-=Value
            #print "LogDetMatrix-Reduced:\n",LogDetMatrix
            #print "ProdFactor:",ProdFactor
            ###Take the determinant
            for StateIndx in range(Ne):
                for ElIndx in range(Ne):
                    DetMatrix[ElIndx,StateIndx]=cexp(LogDetMatrix[ElIndx,StateIndx])
            #print "DetMatrix:\n",DetMatrix
            Det=np.linalg.det(DetMatrix)
            #print "Det:",Det
            #print "log(Det):",np.log(Det)
            ProdWfn=np.log(Det)+ProdFactor
            #print("ProdWfn:",ProdWfn)

        ###Add the Com wave function
        if self.FP > 0:
            CoMFunction=self.CoMfunction(XYwiev,log=True)
        else:
            #print("No CoM function to add")
            CoMFunction=0.0
        #print("CoMFunction:",CoMFunction)
        TotFunction=ProdWfn+CoMFunction
        #print("TotFunction:",TotFunction)
        ExpTotFunction=cexp(TotFunction)
        #print("ExpTotFunction:",ExpTotFunction)
        return ExpTotFunction

        
    def gammadelta(self):
        return [self.gamma,self.delta]

def get_filled_LL_occupation(NbrFluxLL,LLs):
    Ne=NbrFluxLL*(LLs+1)
    ListLL=np.zeros((Ne),dtype=np.int32)
    ListMom=np.zeros((Ne),dtype=np.int32)
    for ll in range(LLs+1):
        ListLL[NbrFluxLL*ll:NbrFluxLL*(ll+1)]=ll
        for mom in range(NbrFluxLL):
            ListMom[mom+NbrFluxLL*ll]=mom
    return ListMom,ListLL
