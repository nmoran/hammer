#! /usr/bin/env python

""" Overlap.py: Script to get the overlap of two PETSc vectors that are stored in binary formats. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import sys
import argparse
import hammer.vectorutils as vectorutils
from numpy import real,imag

def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to calculate the inner product of vectors. Note that it is the FIST vector that is conjugated.')
    Parser.add_argument('state1', metavar='vec1', type=str)
    Parser.add_argument('state2', metavar='vec2', type=str)
    Parser.add_argument('--verbose' ,'-v', action='store_true')
    Parser.add_argument('--absolute' ,'-a', action='store_true')
    Parser.add_argument('--squared', '-s', action='store_true')
    Parser.add_argument('--ascii-friendly', '-f', action='store_true',help="Print the output in as '<real> <imag>' instead of the native python format '<real>+<imag>j'.")
    Parser.add_argument('--conjugate', '-c', action='store_true', help='Conjugate also the second state before taking overlap.')
    Parser.add_argument('--p1', action='store_true', help='Flag to indicate that the first vector specified is in plain text format (can read diagham ascii vector files).')
    Parser.add_argument('--p2', action='store_true', help='Same as p1 but for the second vector file.')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    Squared = Args.squared
    Absolute = Args.absolute
    Conjugate = Args.conjugate
    Friendly = Args.ascii_friendly

    vec1 = Args.state1
    vec2 = Args.state2

    if Args.p1:
        vec1 = vectorutils.ReadPlainASCIIVector(vec1)

    if Args.p2:
        vec2 = vectorutils.ReadPlainASCIIVector(vec2)

    if Squared:
        Overlap = vectorutils.Overlap(vec1, vec2, Verbose, True, conjugate=Conjugate)
        Overlap = Overlap**2
    else:
        Overlap = vectorutils.Overlap(vec1, vec2, Verbose, Absolute, conjugate=Conjugate)

    if Friendly:
        print(real(Overlap),imag(Overlap))
    else:
        print(Overlap)


if __name__ == '__main__' :
    __main__()
