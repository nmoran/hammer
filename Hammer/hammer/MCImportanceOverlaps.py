#! /usr/bin/env python

""" MCImportanceOverlaps.py: Script to calculate the overlap from samples using importance sampling. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils
import h5py
import petsc4py.PETSc as PETSc
import csv
import time
import itertools as iter


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    full_start = time.time()
    Parser = argparse.ArgumentParser(description='Utility to calculate overlaps from samples.')
    Parser.add_argument('--wfn-list', type=str, default='', nargs='+', help='List of filenames containing CFT wfn samples.')
    Parser.add_argument('--wgt-list', type=str, default='', nargs='+', help='List of filenames containing CFT MC probabilities.')
    Parser.add_argument('--ed-list', type=str, default='', nargs='+',help='List of file names containing ED wfn samples.')
    Parser.add_argument('--Column_CFT', type=int, help='Only use column no <Column_CDF> from the CFT file when computing the overlap.')
    Parser.add_argument('--Column_ED', type=int, help='Only use column no <Column_ED> from the ED file when computing the overlap.')
    Parser.add_argument('--bin-data-input', type=str, nargs='+',default='', help='Filename(s) containing compressed samples of data.')
    Parser.add_argument('--bins', type=int, default=500, help='Number of bins to use (default=500).')
    Parser.add_argument('--bin-data', action='store_true', help='Output raw bin data.')
    Parser.add_argument('--bin-data-csv', action='store_true', help='Output raw bin data in csv format. Default is hdf5')
    Parser.add_argument('--bin-groups', type=int, default=1, help='Number groups to use. Groups are used when computing the estimate of overlap squared. Important if overlaps are close of zero valued (default=<bins>.')
    Parser.add_argument('--bin-level', type=int, default=1, help='Coarse grainning of bins by this number to check error stabilitiy (default=1).')
    Parser.add_argument('--verbose','-v', action='store_true')
    Parser.add_argument('--output-prefix', default='Overlaps', help='Output prefix to use (default=Overlaps).')
    Parser.add_argument('--output-dir',help='Output directory to use for created Superpositions (default="same directory as the ED files").')
    Parser.add_argument('--create-superposition', action='store_true', help='Flag to idicate whether states should be created as superposition using overlaps as coefficients. Vector names are read from hdf5 file.')
    Parser.add_argument('--energies', type=str, default='', help='Filename where the energy values of the eigenstates are (values used are first column of csv file).')
    Parser.add_argument('--samples', type=int, default=0, help='Specify the amount of samples to use, if 0 all samples in provided files are used (default=0).')
    Parser.add_argument('--slaters', action='store_true', help='Flag to indicate that overlap is given for each of the slater determinants. Only relevant for creating superposition.')
    Parser.add_argument('--offset', type=int, default=0, help='The offset from where we should start reading samples (default=0).')
    Parser.add_argument('--hdf5-driver', type=str, default='stdio', help='The driver to use when reading hdf5 files (default=stdio, other option is sec2).')
    Parser.add_argument('--cft-scale', type=str, default='log', help='The scale of the CFT input (default=log).',choices={"lin","log"})
    Parser.add_argument('--prob-scale', type=str, default='log', help='The scale of the Probability distribution input (default=log).',choices={"lin","log"})
    Parser.add_argument('--ed-scale', type=str, default='lin', help='The scale of the ED input (default lin).',choices={"lin","log"})


    # Parse and read command line options.
    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    BinDataInputFiles = Args.bin_data_input
    Bins = Args.bins
    OutputPrefix = Args.output_prefix
    NbrSamplesToUse = Args.samples
    Slaters = Args.slaters
    OutputBinData = Args.bin_data
    SampleOffset = Args.offset
    NbrBinGroups = Args.bin_groups
    BinLevel = Args.bin_level
    BinDataCSV = Args.bin_data_csv
    ScCFT=set_loglin_to_bool(Args.cft_scale)
    ScProb=set_loglin_to_bool(Args.prob_scale)
    ScED=set_loglin_to_bool(Args.ed_scale)

    # Read in samples or previously compressed sample data.
    if BinDataInputFiles != '': #If compresses sample data is present it has precedence..
        print("Using Binned Data Files")
        if Args.wfn_list != '': #But non of the other flags are allowed to be set.
            ErrorFlagClash("--bin-data-input","--wfn_list",Parser)
        if Args.wgt_list != '':
            ErrorFlagClash("--bin-data-input","--wgt_list",Parser)
        if Args.ed_list != '':
            ErrorFlagClash("--bin-data-input","--ed_list",Parser)

        DataList=[]
        for BinDataInputFile in BinDataInputFiles:
            s = time.time()
            if Verbose: print(('Reading bin data from ' + BinDataInputFile))
            LocalData=ReadCompressedSamplesData(BinDataInputFile)
            e = time.time()
            if Verbose: print((str(LocalData['NbrSamples']) + ' read in ' + str(e-s) + ' seconds.'))
            #print "LocalData:",LocalData
            DataList.append(LocalData)
        Data=MergeCompressedData(DataList)
        #print "Data:",Data
    elif (Args.wfn_list != '' or Args.wgt_list != '' or Args.ed_list != ''):
        print("Using Raw Data Files")
        if Args.wfn_list == '':  ##Check that all three flags are set
            ErrorFlagMissing("--wfn_list",Parser)
        if Args.wgt_list == '':  ##Check that all three flags are set
            ErrorFlagMissing("--wgt_list",Parser)
        if Args.ed_list == '':  ##Check that all three flags are set
            ErrorFlagMissing("--ed_list",Parser)

        CFTWfnFiles = Args.wfn_list
        ProbFiles = Args.wgt_list
        EDFiles = Args.ed_list
        if Verbose: print(('Found ' + str(len(CFTWfnFiles)) + ' CFT sample files and ' + str(len(ProbFiles)) + ' MC probability files and ' + str(len(EDFiles)) + ' ED sample files. Reading...'))
        s = time.time()
        Data = ReadAndCompressSamples(CFTWfnFiles, ProbFiles,EDFiles, NbrSamplesToUse,
                                      SampleOffset, Bins, DsetName="wf_values", Verbose=Verbose,
                                      logscale=[ScCFT,ScProb,ScED],
                                      ColumnCFT=Args.Column_CFT,ColumnED=Args.Column_ED)
        e = time.time()
        if Verbose: print((str(Data['NbrSamples']) + ' read in ' + str(e-s) + ' seconds.'))
    else:
        print('No config file or bins files provided.')
        sys.exit(-1)

    if OutputBinData:
        if Verbose: print(('Writing bin data from in ' + ('HDF5' if not BinDataCSV else 'csv') + ' format.'))
        WriteCompressedSamplesData(Data, OutputPrefix, csv_format=BinDataCSV)

    if Verbose: print('Calculating overlaps.')
    Overlaps = CalculateOverlaps(Data, NbrBinGroups, BinLevel)
    WriteOverlaps(Overlaps, OutputPrefix)

    CreateSuperpositionFlag = Args.create_superposition
    EnergiesFile = Args.energies
    NbrStates = len(Overlaps['Overlaps'])

    if EnergiesFile != '':
        Energies = np.zeros(NbrStates, dtype=np.float64)
        # read in energies
        f = csv.reader(open(EnergiesFile, "r"))
        idx = 0
        for row in f:
            if idx >= NbrStates: break
            Energies[idx] = np.float(row[0])
            idx += 1
        if idx < NbrStates:
            print(('Not enough energy values given, only ' + str(idx) + ' given for ' + str(NbrStates) + ' states.'))
            sys.exit(-1)

        EnergyData = EstimateEnergy(Data, Overlaps, Energies, NbrBinGroups, BinLevel)
        if Verbose:
            print(('Energy expectation value: ' + str(EnergyData['EnergyEstimate']) + ', error estimate: ' + str(EnergyData['EnergyError']) + ', and from binning: ' + str(EnergyData['BinEnergyError'])))
            print(('Normed: Energy expectation value: ' + str(EnergyData['NormEnergyEstimate']) + ', error estimate: ' + str(EnergyData['NormEnergyError']) + ', and from binning: ' + str(EnergyData['NormBinEnergyError'])))
        WriteEnergyData(EnergyData, OutputPrefix)

    if CreateSuperpositionFlag:
        CreateSuperposition(Overlaps, Data['EDFilenames'], OutputPrefix, Slaters,Args.output_dir)

    WriteMetaData(Data['NbrSamples'],Bins, NbrBinGroups, BinLevel, OutputPrefix)

    full_end = time.time()
    if Verbose: print(('Took ' + str(full_end-full_start) + ' seconds.'))


def set_loglin_to_bool(String):
    if String == 'log':
        return True
    elif String =='lin':
        return False
    else:
        print("ERROR: Unknown String '"+str(String)+"' for log/lin scale")
        sys.exit(-1)


def ErrorFlagClash(Flag1,Flag2,parser):
    parser.print_help()
    print("ERROR: Conflicting flags: %s and %s can NOT be specified at the same time." % (Flag1,Flag2))
    print("       Please remove one of the two flags!")
    sys.exit(-1)


def ErrorFlagMissing(Flag1,parser):
    parser.print_help()
    print("ERROR: Flags missing: The three flags --wfn_list, --wgt_list, --ed_list need all to be specified.")
    print("       You forgot to add %s. Please add it!" % (Flag1))
    sys.exit(-1)


def ReadFileNamesFromList(InputList):
    """
    Read the names of the input files from input as comma separated list.

    Parameters
    -----------
    InputList: str
        comma separated list.

    Returns
    FileList:
        List of files.
    """
    FileList = list()
    if InputList != '':
        for part in InputList.split(','):
            FileList.append(part.rstrip())
    return FileList


def FindDatasetDimensions(Filenames, DatasetName):
    """
    Function to find the dimensions of each of the datasets in the given files.

    Parameters
    Filenames: list
        List of filenames to read dataset dimensions from.
    DatasetName: str
        The dataset name to look at in each.

    Returns
    --------
    list
         List containing shape information for the dataset in each file.
    """
    dims = list()
    for Filename in Filenames:
        try:
            f = h5py.File(Filename, 'r')
            dims.append(f[DatasetName].shape)
            f.close()
        except IOError as ioError:
            print(("Error reading filename " + Filename + "."))
            sys.exit(-1)
    return dims


def GetSampleCount(Dims):
    SampleCounts = np.cumsum(list([x[0] for x in Dims]))
    NbrSamples = np.sum(list([x[0] for x in Dims])) # sum rows in each
    return [SampleCounts, NbrSamples]


def ReadAndCompressSamples(CFTWfnFiles, ProbFiles, EDFiles, NbrSamplesToUse, Offset, Bins, DsetName="wf_values", Verbose=False, logscale=(True,True,False),ColumnCFT=None,ColumnED=None):
    """
    Read the requested number of samples from the files given in the lists. Compress these
    into arrays of mus, nus and deltas for each bin. Also record the maximum in each bin
    that is used to normalise the bin values.

    CFTWfnFiles: list
        List of filenames where wave-function values are stored.
    ProbFiles: list
        List of filenames where MC probability values are stored.
    EDFiles: list
        List of filenames where ED wave-function values are stored.
    NbrSamplesToUse: int
        The number of samples to read.
    Offset: int
        The offset before one starts to read samples.
    Bins: int
        The number of bins to divide the samples into.
    linlog: (Bool,Bool,Bool)
        Tuple of bools specifying if the data is in log scale. Each element corresponds to CFT, probabilities and ED (default=(True,True, False)).
    """
    CFTDims = FindDatasetDimensions(CFTWfnFiles, DsetName)
    ProbDims = FindDatasetDimensions(ProbFiles, DsetName)
    EDDims = FindDatasetDimensions(EDFiles, DsetName)
    [CFTSampleCounts,NbrCFTSamples]=GetSampleCount(CFTDims)
    [ProbSampleCounts,NbrProbSamples]=GetSampleCount(ProbDims)
    [EDSampleCounts,NbrEDSamples]=GetSampleCount(EDDims)
    if ColumnED==None:
        NbrStates = int(EDDims[0][1]/2)
    else:
        NbrStates = 1

    if NbrEDSamples != NbrCFTSamples:
        print('Error: number of ED samples does not match the number of CFT samples.')
        sys.exit(-1)
    if NbrEDSamples != NbrProbSamples:
        print('Error: number of ED samples does not match the number of MC Probability values.')
        sys.exit(-1)

    if NbrSamplesToUse == 0:
        NbrSamplesToUse = NbrCFTSamples - Offset

    if Offset + NbrSamplesToUse > NbrCFTSamples:
        print(('Not enough samples. Asked to use ' + str(NbrSamplesToUse) + ' from sample ' + str(SampleOffset) + ', but only ' + str(NbrCFTSamples) + ' present.'))
        sys.exit(-1)

    if NbrSamplesToUse<Bins:
        print("ERROR: More bins="+str(Bins)+" than number of samples to use="+str(NbrSamplesToUse))
        sys.exit(-1)
    else:
        # we ignore any remainder samples
        SamplesPerBin = int(NbrSamplesToUse/Bins)

    if Verbose:
        print((str(NbrCFTSamples) + ' samples found. Using ' + str(Bins*SamplesPerBin) + ' in ' + str(Bins) + ' bins.'))
        sys.stdout.flush()

    # we create some data-structures to store partial overlaps
    ProbLogMaximums = np.zeros(Bins, dtype=np.float64)
    CFTLogMaximums = np.zeros(Bins, dtype=np.float64)
    EDLogMaximums = np.zeros((NbrStates, Bins),dtype=np.float64)
    mus = np.zeros(Bins, dtype=np.float64)
    nus = np.zeros((NbrStates, Bins), dtype=np.float64)
    deltas = np.zeros((NbrStates, Bins), dtype=np.complex128)
    BinOverlaps = np.zeros((NbrStates, Bins), dtype=np.float128) # Using absolute values this will be real
    BinNormalisedOverlaps = np.zeros((NbrStates, Bins), dtype=np.float128) # Using absolute values this will be real

    BinStarts = np.arange(Bins)*SamplesPerBin + Offset
    BinEnds = np.arange(1, Bins+1)*SamplesPerBin + Offset

    ProbSamples = np.zeros(SamplesPerBin, dtype=np.complex128)
    CFTSamples = np.zeros(SamplesPerBin, dtype=np.complex128)
    EDSamples = np.zeros((SamplesPerBin,NbrStates), dtype=np.complex128)

    CFTDsets = list()
    ProbDsets = list()
    EDDsets = list()
    HDF5Files = list()

    EDFilenames = list()
    EDFilenamesEmpty = True

    for CFTFile in CFTWfnFiles:
        f = h5py.File(CFTFile, "r", driver="stdio")
        CFTDsets.append(f[DsetName])
        HDF5Files.append(f)

    for ProbFile in ProbFiles:
        f = h5py.File(ProbFile, "r", driver="stdio")
        ProbDsets.append(f[DsetName])
        HDF5Files.append(f)

    for EDFile in EDFiles:
        f = h5py.File(EDFile, "r", driver="stdio")
        EDDsets.append(f[DsetName])
        HDF5Files.append(f)
        # assume all ED filenames have the same path to exact files.
        if 'vector_filenames' in list(f.keys()) and EDFilenamesEmpty:
            EDFilenames.extend(list(map(lambda x: x.decode("utf-8"), f['vector_filenames'][0,:])))
            EDFilenamesEmpty = False

    if ColumnCFT==None:
        ColumnCFT=0
    for StateIdx in range(NbrStates):
        if Verbose:
            print(('State ' + str(StateIdx) + '/' + str(NbrStates)))
            sys.stdout.flush()
        for bin in range(Bins):
            ###Load the CFT wfn samples
            LoadDataToBin(CFTWfnFiles,CFTSampleCounts,BinStarts[bin],BinEnds[bin],CFTDsets,CFTSamples,SamplesPerBin,Verbose,Pair=ColumnCFT)
            ###Load the Prob wfn samples
            LoadDataToBin(ProbFiles,ProbSampleCounts,BinStarts[bin],BinEnds[bin],ProbDsets,ProbSamples,SamplesPerBin,Verbose)
            if ColumnED==None:
                EDColumn=StateIdx
            else:
                EDColumn=ColumnED
            ###Load the ED wfn samples
            LoadDataToBin(EDFiles,EDSampleCounts,BinStarts[bin],BinEnds[bin],EDDsets,EDSamples[:,StateIdx],SamplesPerBin,Verbose,Pair=EDColumn)

            # now all the samples for this bin have been loaded in, we start to process these.

            # we normalize and save the amount we use for the normalization so that we can combine later.
            CFTSamples,CFTLogMaximums[bin] = scale_and_linearize(CFTSamples,logscale[0])
            ProbSamples,ProbLogMaximums[bin] = scale_and_linearize(ProbSamples,logscale[1])
            EDSamples[:,StateIdx],EDLogMaximums[StateIdx,bin] = scale_and_linearize(EDSamples[:,StateIdx],logscale[2])
            ###Remember that probabilities are naturally squared
            ProbSamples=np.conj(ProbSamples) * ProbSamples
            ProbLogMaximums[bin] = 2*ProbLogMaximums[bin]

            mus[bin] = np.real(np.inner(np.conj(CFTSamples), CFTSamples/ProbSamples))
            nus[StateIdx, bin] = np.real(np.inner(np.conj(EDSamples[:,StateIdx]), EDSamples[:,StateIdx]/ProbSamples))
            deltas[StateIdx,bin] = np.inner(np.conj(EDSamples[:,StateIdx]), CFTSamples/ProbSamples)

    for f in HDF5Files:
        f.close()

    # Find maximums of values used to normalise sample sets
    Data = CompressDataLists(ProbLogMaximums,CFTLogMaximums,EDLogMaximums,NbrStates,mus,nus,deltas)
    Data['EDFilenames'] = EDFilenames
    Data['NbrSamples'] = SamplesPerBin * Bins

    return Data


def CompressDataLists(ProbLogMaximums,CFTLogMaximums,EDLogMaximums,NbrStates,mus,nus,deltas):
    """
    Takes the list of mu,nu,deltas and the maxmimums of the cft,ed and
    probabilities and outputs a dictinoary with the data compresed.

    ProbLogMaximums: numpy.array
        List of maximum of underlying probability for each bin
    CFTLogMaximums: numpy.array
        List of maximum of underlying CFT for each bin
    EDLogMaximums: numpy.array
        List of maximum of underlying ED wave functoin for each bin
    NbrStates: int
        The number of ED-states
    mus: numpy.array
        Array with mu-values
    nus: numpy.array
        Array with nu-values
    deltas: numpy.array
        Array with delta-values
    """

    # Find maximums of values used to normalise sample sets
    ProbLogMaximum = np.max(ProbLogMaximums)
    CFTLogMaximum = np.max(CFTLogMaximums)
    EDLogMaximum = np.zeros(NbrStates)
    for j in range(NbrStates):
        EDLogMaximum[j] = np.max(EDLogMaximums[j,:])

    # using this we renormalise the nus, mus and deltas so the normalisation matches.
    mus = mus * np.exp(2*(CFTLogMaximums-CFTLogMaximum)) / np.exp(ProbLogMaximums-ProbLogMaximum)
    for j in range(NbrStates):
        nus[j,:] = nus[j,:] * np.exp(2*(EDLogMaximums[j,:]-EDLogMaximum[j]) - (ProbLogMaximums-ProbLogMaximum))
        deltas[j,:] = deltas[j,:] * np.exp((CFTLogMaximums-CFTLogMaximum) +(EDLogMaximums[j,:]-EDLogMaximum[j]) -(ProbLogMaximums-ProbLogMaximum))
    Data = dict()
    Data['mus'] = mus; Data['nus'] = nus; Data['deltas'] = deltas
    Data['NbrStates'] = NbrStates
    Data['ProbLogMaximum'] = ProbLogMaximum
    Data['CFTLogMaximum'] = CFTLogMaximum
    Data['EDLogMaximum'] = EDLogMaximum
    return Data


def scale_and_linearize(Samples,logscale):
    if logscale:
        LogMaxSamples = np.max(np.real(Samples))
        Samples = np.exp(Samples - LogMaxSamples)
    else:
        MaxSamples = np.max(np.abs(Samples))
        Samples = Samples / MaxSamples
        LogMaxSamples = np.log(MaxSamples)
    return Samples,LogMaxSamples


def LoadDataToBin(Files,FileCounts,BinStarts,BinEnds,Dsets,Samples,SamplesPerBin,Verbose,Pair=0):
    """

    """
    Loaded = 0 # keep track of how many samples we have collected so far.
    for i in range(len(Files)):
        if BinStarts < FileCounts[i]:
            if BinEnds < FileCounts[i]: # load full amount
                ToLoad = SamplesPerBin - Loaded
            else: # load as many as we can
                ToLoad = FileCounts[i] - BinStarts - Loaded
            try:
                dset = Dsets[i]
                DataPointer=BinStarts + Loaded - (0 if i < 1 else FileCounts[i-1])
                sl = slice(DataPointer,DataPointer + ToLoad)
                s = time.time()
                Samples[Loaded:(Loaded + ToLoad)] = dset[sl,Pair*2] + np.complex(0.0,1.0)*dset[sl,Pair*2+1];
                Loaded += ToLoad
                e = time.time()
                if Verbose: print("Read " + str(ToLoad) + " values from " + Files[i] + " in " + str(e-s) + " seconds.")
            except IOError as ioerror:
                print(("Error reading hdf5 file " + Files[i] + ', ' + str(ioerror)))
                sys.exit(-1)
    ###return(Samples) ##NB: Not needed as BinsSamples are modified in place


def MergeCompressedData(DataList):
    Elems=len(DataList)
    ##print("Elements:",Elems)
    ###FIXME Assumes bins are equal size as well as number of states
    assert same_shapes(DataList,'mus'),'Lists Do not have same shape'
    assert same_shapes(DataList,'nus'),'Lists Do not have same shape'
    assert same_shapes(DataList,'deltas'),'Lists Do not have same shape'
    assert same_values(DataList,'NbrSamples'),'Lists Do not have same value'
    assert same_values(DataList,'NbrStates'),'Lists Do not have same value'
    NbrStates=DataList[0]['NbrStates']
    SamplesPerElem=DataList[0]['NbrSamples']
    #print("NbrStates:",NbrStates)
    BinsPerData=DataList[0]['mus'].shape[0]
    #print("BinsPerData:",BinsPerData)
    Bins=BinsPerData*Elems
    musList = np.zeros(Bins, dtype=np.float64)
    nusList = np.zeros((NbrStates, Bins), dtype=np.float64)
    deltasList = np.zeros((NbrStates, Bins), dtype=np.complex128)
    CFTLogMaximums = np.zeros(Bins, dtype=np.float64)
    ProbLogMaximums = np.zeros(Bins, dtype=np.float64)
    EDLogMaximums = np.zeros((NbrStates, Bins), dtype=np.float64)

    n=0
    for Data in DataList:
        #print("n:",n)
        CFTMax=Data['CFTLogMaximum']
        ProbMax=Data['ProbLogMaximum']
        EdMax=Data['EDLogMaximum']
        mus=Data['mus']
        nus=Data['nus']
        deltas=Data['deltas']
        #print("CFTMax:",CFTMax)
        #print("ProbMax:",ProbMax)
        #print("EdMax:",EdMax)
        #print("deltas:",deltas)
        #print("nus:",nus)
        #print("mus:",mus)
        binindx=n+np.array(range(BinsPerData))
        ###Save The Maimums to the BINLIST
        ###Samve the mu,nu,delavalues fo the binlists
        CFTLogMaximums[binindx]=CFTMax
        ProbLogMaximums[binindx]=ProbMax
        musList[binindx]=mus
        for indx in range(BinsPerData):
            #print("indx:",indx)
            EDLogMaximums[:,n+indx]=EdMax
            nusList[:,n+indx]=nus[:,indx]
            deltasList[:,n+indx]=deltas[:,indx]
        n=n+BinsPerData
        #print("musList:n",musList)
        #print("CFTLogMaximums:n",CFTLogMaximums)
        #print("ProbLogMaximums:n",ProbLogMaximums)
    NewData = CompressDataLists(
        ProbLogMaximums,CFTLogMaximums,EDLogMaximums,NbrStates,
        musList,nusList,deltasList)
    NewData['NbrSamples'] = Elems*SamplesPerElem
    return NewData


def same_shapes(DataList,Key):
    ShapeList=[ Data[Key].shape for Data in DataList]
    return all(x==ShapeList[0] for x in ShapeList)


def same_values(DataList,Key):
    ValueList=[ Data[Key] for Data in DataList]
    return all(x==ValueList[0] for x in ValueList)


def WriteCompressedSamplesData(Data, OutputPrefix, csv_format=False):
    """
    Write the compressed samples data to disk.

    Parameters
    ----------
    Data: dict
        Dictionary containing data.
    OutputPrefix: str
        Prefix to use for output files.
    csv_format: bool
        Flag to indicate whether csv format should be used (default=False).
    """
    if not csv_format:
        f = h5py.File(OutputPrefix + '_bins.hdf5', 'w')
        for key in list(Data.keys()):
            if key != 'EDFilenames':
                if isinstance(Data[key], np.ndarray):
                    dset = f.create_dataset(key, shape=Data[key].shape, dtype=Data[key].dtype, data=Data[key])
                elif isinstance(Data[key], int):
                    dset = f.create_dataset(key, shape=[1], dtype=np.int64, data=[Data[key]])
                elif isinstance(Data[key], np.float):
                    dset = f.create_dataset(key, shape=[1], dtype=np.float64, data=[Data[key]])
                else:
                    dset = f.create_dataset(key, shape=[1], dtype=Data[key].__class__, data=[Data[key]])
        f.flush()
        f.close()
    else:
        #writing arrays to csv files. Writing real and imaginary parts
        np.savetxt(OutputPrefix + '_mus_re.csv', np.real(Data['mus']), delimiter=',')
        np.savetxt(OutputPrefix + '_mus_im.csv', np.imag(Data['mus']), delimiter=',')
        np.savetxt(OutputPrefix + '_nus_re.csv', np.real(Data['mus']), delimiter=',')
        np.savetxt(OutputPrefix + '_nus_im.csv', np.imag(Data['nus']), delimiter=',')
        np.savetxt(OutputPrefix + '_deltas_re.csv', np.real(Data['deltas']), delimiter=',')
        np.savetxt(OutputPrefix + '_deltas_im.csv', np.imag(Data['deltas']), delimiter=',')


def ReadCompressedSamplesData(Filename):
    """
    Read the compressed samples data from the given filename.

    Parameters
    -----------
    Filename: str
        Name of file containing data.

    Returns
    --------
    dict
        Dictionary containing data.
    """
    f = h5py.File(Filename, 'r')
    Data = dict()
    for key in list(f.keys()):
        if np.prod(f[key].shape) == 1:
            Data[key] = f[key][0]
        else:
            Data[key] = np.zeros(f[key].shape, dtype=f[key].dtype)
            Data[key][:] = f[key][:]
    f.close()
    return Data


def CalculateOverlaps(Data, NbrGroups=-1, BinningLevel=1):
    """
    Calculate the overlap for each state.

    Parameters
    -----------
    Data: dict
        Dictionary containing mus, nus and deltas for samples.
    NbrGroups: int
        The number of groups to use when calculating norm for overlaps(default same number of groups as number of bins).
    BinningLevel: int
        Number of bins to use in a 'super-bin' when binning for error calculation.

    Returns
    ---------
    dict:
        Dictionary containing overlap, overlap error (both normalised and unnormalised)
        as well as the normalisation and normalisation error.
    """
    mus = Data['mus'];nus = Data['nus']; deltas = Data['deltas']
    NbrStates = nus.shape[0]
    Bins = mus.shape[0]
    ###Use same number of groups as bins
    if NbrGroups<=0:
        NbrGroups=Bins
    GrpOvps = np.zeros(NbrGroups, dtype=np.complex128)
    BinsPerGroup = int(Bins/NbrGroups)
    OvpsSqrd = np.zeros(NbrStates, dtype=np.float64)
    Ovps = np.zeros(NbrStates, dtype=np.complex128)
    for i in range(NbrStates):
        Ovps[i] = (np.sum(deltas[i,:]) /  np.sqrt(np.sum(mus) * np.sum(nus[i,:])))
        if NbrGroups > 1:
            for j in range(NbrGroups):
                for k in range(NbrGroups):
                    GrpOvps[k] = (np.sum(deltas[i,(j*BinsPerGroup):((j+1)*BinsPerGroup)])
                                  /  np.sqrt(np.sum(mus)
                                  * np.sum(nus[i,(j*BinsPerGroup):((j+1)*BinsPerGroup)])))
            # we estimate the squared overlap by summing all off-diagonals.
            OvpsSqrd[i] = np.abs(np.real((np.sum(np.outer(GrpOvps[:], np.conj(GrpOvps[:].T))
                        - np.sum(GrpOvps[:] * np.conj(GrpOvps[:]))))/np.float(NbrGroups*(NbrGroups-1))))
            ###NB: Do not do this, as Overlaps**2 could be a negative number!!!!
            ###Ovps[i] = np.sqrt(OvpsSqrd[i])*Ovps[i]/np.abs(Ovps[i]) # use the magnetude found by grouping the bins.
        else:
            OvpsSqrd[i] = np.real(Ovps[i] * np.conj(Ovps[i]))

    # Now we estimate errors using binning
    NbrEffBins = int(Bins/(BinningLevel))
    if NbrEffBins < 2:
        print('Number of effective bins is too small. Pick more bins or smaller numbers of groups.')
        sys.exit(-1)
    BinOvps = np.zeros((NbrStates,NbrEffBins), dtype=np.complex128)
    for bin in range(NbrEffBins):
        start_bin = bin*BinningLevel
        end_bin = (bin+1)*BinningLevel
        s = slice(start_bin, end_bin)
        BinOvps[:,bin] = (np.sum(deltas[:,s], axis=1)/np.sqrt(np.sum(mus[s]) * np.sum(nus[:,s], axis=1)))  # Saving Overlap of the bins
        
    # Prepare output dictionary
    Overlaps = dict()
    Overlaps['Overlaps'] = Ovps
    Overlaps['OverlapsSquared'] = OvpsSqrd
    Overlaps['OverlapBinErrorsReal'] = np.std(np.real(BinOvps), axis=1, ddof=0)/np.sqrt(NbrEffBins)
    Overlaps['OverlapBinErrorsImag'] = np.std(np.imag(BinOvps), axis=1, ddof=0)/np.sqrt(NbrEffBins)

    return Overlaps


def EstimateEnergy(Data, Overlaps, Energies, NbrGroups=1, BinningLevel=1):
    """
    Estimate the energy from energies of eigenstates.

    Parameters
    -----------
    Data: dict
        Dictionary containing mus, nus and deltas for samples.
    Overlaps: dict
        Dictionary of overlap data and errors.
    Energies: array
        1D array of energies for each of the eigenstates.
    NbrGroups: int
        The number of groups to use.
    BinningLevel: int
        Number of levels to use when binning for error calculation.

    Returns
    ---------
    dict:
        Dictionary containing the energy estimate, energy estimate from error propagation and
        energy error from binning.
    """

    
    STR="FIXME: Mikael Fremling 9/4-2018 -- This funtion is broken as Normalizations where removed from the function due to giving confusing output -- Maybe norms may be incorporated again by adding a flag that activates them."
    raise Exception, STR
    
    
    mus = Data['mus']; nus = Data['nus']; deltas = Data['deltas']
    Bins = mus.shape[0]
    NbrStates = nus.shape[0]

    EnergyEstimate = np.sum(Overlaps['OverlapsSquared'] * Energies)
    NormEnergyEstimate = np.sum(Overlaps['NormOverlapsSquared'] * Energies)

    # Now we estimate errors by binning
    NbrEffBins = int(Bins/(BinningLevel))

    if NbrEffBins < 2:
        print('Number of effective bins is too small. Pick more bins or smaller numbers of groups.')
        sys.exit(-1)

    BinOvps = np.zeros((NbrStates,NbrEffBins), dtype=np.float64)
    NormBinOvps = np.zeros((NbrStates,NbrEffBins), dtype=np.float64)
    BinEnergies = np.zeros(NbrEffBins, dtype=np.float64)
    NormBinEnergies = np.zeros(NbrEffBins, dtype=np.float64)
    for bin in range(NbrEffBins):
        start_bin = bin*BinningLevel
        end_bin = (bin+1)*BinningLevel
        s = slice(start_bin, end_bin)
        BinOvps[:,bin] = np.abs(np.sum(deltas[:,s], axis=1)/np.sqrt(np.sum(mus[s]) * np.sum(nus[:,s], axis=1)))  # Saving absolute values of the bins
        Norm = np.sum(BinOvps[:,bin]**2)
        NormBinOvps[:,bin] = BinOvps[:,bin]/np.sqrt(Norm)
        BinEnergies[bin] = np.sum(BinOvps[:,bin]**2*Energies)
        NormBinEnergies[bin] = np.sum(NormBinOvps[:,bin]**2*Energies)
    EnergyError = 2.0*np.sqrt(np.sum(Overlaps['OverlapBinErrors']**2 * Energies**2 * Overlaps['OverlapsSquared']))
    NormEnergyError = 2.0*np.sqrt(np.sum(Overlaps['NormOverlapBinErrors']**2 * Energies**2 * Overlaps['NormOverlapsSquared']))
    BinEnergyError = np.std(BinEnergies, ddof=0)/np.sqrt(NbrEffBins)
    NormBinEnergyError = np.std(NormBinEnergies, ddof=0)/np.sqrt(NbrEffBins)

    EnergyData = dict()
    EnergyData['EnergyEstimate'] = EnergyEstimate
    EnergyData['NormEnergyEstimate'] = NormEnergyEstimate
    EnergyData['EnergyError'] = EnergyError
    EnergyData['NormEnergyError'] = NormEnergyError
    EnergyData['BinEnergyError'] = BinEnergyError
    EnergyData['NormBinEnergyError'] = NormBinEnergyError

    return EnergyData


def WriteOverlaps(Overlaps, Prefix):
    """
    Function to write the calculated overlaps to a csv file on disk.

    Parameters
    -----------
    Overlaps: dict
        Dictionary of overlaps and errors.
    Prefix: str
        Prefix to use for output file.
    """
    try:
        if sys.version_info[0] == 3:
            OutputFile = open(Prefix + '.csv', 'w', encoding='utf8', newline='')
        elif sys.version_info[0] == 2:
            OutputFile = open(Prefix + '.csv', 'wb')
        OutputCSV = csv.writer(OutputFile)
        OutputCSV.writerow(('StateIdx', 'Re(Overlap)', 'Im(Overlap)', 'Re(ErrOverlap)', 'Im(ErrOverlap)', 'abs(Overlap)','OverlapSquared'))
        OutputFile.flush()
        for j in range(len(Overlaps['Overlaps'])):
            OutputCSV.writerow((j,
                                np.real(Overlaps['Overlaps'][j]),
                                np.imag(Overlaps['Overlaps'][j]),
                                Overlaps['OverlapBinErrorsReal'][j],
                                Overlaps['OverlapBinErrorsImag'][j],
                                np.abs(Overlaps['Overlaps'][j]),
                                np.real(Overlaps['OverlapsSquared'][j]),
                            ))
        OutputFile.close()
    except IOError as ioerror:
        print(('Problem openning ' + Prefix + '.csv'))
        sys.exit(-1)


def WriteEnergyData(EnergyData, Prefix):
    """
    Write the energy estimates and error estimates to a csv file.

    Parameters
    ------------
    EnergyData: dict
        Dictionary containing energy values and error estimates.
    Prefix: str
        Prefix to use for filename.
    """
    try:
        fout = open(Prefix + '_Energy.csv', 'w')
        f = csv.writer(fout)
        f.writerow(['EnergyEstimate', 'EnergyError', 'BinEnergyError', 'NormEnergyEstimate', 'NormEnergyError', 'NormBinEnergyError'])
        f.writerow([EnergyData['EnergyEstimate'],
                    EnergyData['EnergyError'],
                    EnergyData['BinEnergyError'],
                    EnergyData['NormEnergyEstimate'],
                    EnergyData['NormEnergyError'],
                    EnergyData['NormBinEnergyError']])
        fout.close()
    except IOError as ioerror:
        print(('Problem openning ' + Prefix + '_Energy.csv'))
        sys.exit(-1)


def CreateSuperposition(Overlaps, EDFilenames, Prefix, Slaters=False, BaseName=None):
    """
    Function to create a superposition of eigenstates.

    Parameters
    ------------
    Overlaps: dict
        Dictionary of overlap data.
    EDFilenames: list
        List of filenames of the eigenstates.
    Prefix: str
        Prefix for output vector.
    Slaters: bool
        Flag to indicate that overlap is with Slaters (default=False).
    """
    NbrStates = Overlaps['Overlaps'].shape[0]
    if not Slaters:
        if len(EDFilenames) < NbrStates:
            print(('For this utility to create the superpositions, the filenames of the files containing fock coefficients must be' + ' embedded in the samples files.'))
            sys.exit(-1)
        else:
            Superposition = vectorutils.Superposition(EDFilenames, Overlaps['NormOverlaps'])
            Superposition.normalize()
            print(("Generate Basename from ED Filenames:"))
            EDName = EDFilenames[0]
            if EDName.find('_State') >= 0:
                EDName = EDName.split('_State')[0]
            if BaseName!=None:
                EDName=EDName.split('/')[-1]
                FileName=BaseName+'/'+EDName + '_' + Prefix + '.vec'
            else:
                FileName=             EDName + '_' + Prefix + '.vec'
            print(("Write Superpositions to file: "+FileName))
            vectorutils.writeBinaryPETScFile(Superposition,FileName)
    else:
        Superposition = PETSc.Vec()
        Superposition.create()
        Superposition.setSizes(NbrStates)
        Superposition.setUp()
        Superposition.array[:] = Overlaps['NormOverlaps'][:]
        Superposition.normalize()
        vectorutils.writeBinaryPETScFile(Superposition, Prefix + '.vec')


def WriteMetaData(NbrSamples, Bins, NbrBinGroups, BinLevel, Prefix):
    """
    Write some meta data to a csv file.

    Parameters
    ------------
    NbrSamples: int
        Number of samples used.
    Bins: int
        Number of bins that were used.
    NbrBinGroups: int
        The number of bin groups used.
    BinLevel: int
        The binning level.
    Prefix: str
        The prefix to use for the filename.
    """
    try:
        if sys.version_info[0] == 3:
            f = open(Prefix + '_Meta.csv', 'w', encoding='utf8', newline='')
        elif sys.version_info[0] == 2:
            f = open(Prefix + '_Meta.csv', 'wb')
        csv_writer = csv.writer(f)
        csv_writer.writerow(['NbrSamples', 'Bins', 'NbrBinGroups', 'BinLevel'])
        csv_writer.writerow([NbrSamples, Bins, NbrBinGroups, BinLevel])
        f.close()
    except IOError as ioerror:
        print(('Error writing file ' + Prefix + '_Meta.csv.'))
        print(('Error: ' + str(ioerror)))
        sys.exit(-1)



if __name__ == '__main__' :
    __main__()
