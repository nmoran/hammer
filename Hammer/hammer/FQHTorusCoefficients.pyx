#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True
"""
 This is a cython implementation calculate FQH interaction coefficents for the torus

"""
cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)

import numpy as np
cimport numpy as np
import csv
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import scipy.special as special
import cython
import sys


try:
    import mpmath
    HaveMPMath = True
except ImportError:
    HaveMPMath = False
    pass


class FQHTorusInteractionCoefficients2Body:
    """
    Class to group methods and data structures for calculating interaction coefficients for FQH systems on tori.

    """


    def __init__(self, Tau=1j, Ns=10, Interaction='Coulomb',
                 Threshold=1e-50, LL=0, MagneticLength=1.0,
                 Scale=None,ApplyModularS=False, MPMath=False,
                 CoefThreshold=0.0, Bosonic=False, PseudoPotentials=None,
                 Thickness=0.0, AppendPseudoPotentials=False):
        """
        Constructor which sets up the class to calculate coefficients.

        Parameters
        ----------
        Tau: complex
            Complex number which controls geomtry of torus (default=1j).
        Ns: int
            The number of orbitals (default=10).
        Interaction: str
            The interaction to use (default=Coulomb).
        Threshold: float
            The threshold to stop summing terms at (default=1e-50).
        LL: int
            The Landau Level index (default=0).
        MagneticLength: float
            The magnetic length (default=1.0).
        Scale: float
            An alternative way of setting the geometry. This becomes side length and angle of tau is the angle (default=None).
        ApplyModularS: bool
            Apply S transform to torus (default=False).
        MPMath: bool
            Use multiprecision math for higher precision (default=False).
        CoefThreshold: float
            The threshold value to cutoff coefficients at (default=0.0).
        Bosonic: bool
            Use bosonic degrees of freedom (default=False).
        Pseudopotentials: array
            Pseudopotential values that characterise the interaction (default=None).
        Thickness: float
            Add correction to approximate finite well thickness (default=0.0).
        AppendPseudoPotentials: bool
            Flag to indicate that given pseudopotentials should be added to original interaction.
        """
        self.Tau = Tau
        if ApplyModularS == True:
            self.Tau = -1.0/Tau
        self.Ns = Ns
        self.MagneticLength = MagneticLength
        self.Theta = np.angle(self.Tau)
        self.SinTheta = np.sin(self.Theta)
        self.CosTheta = np.cos(self.Theta)
        self.Ratio = np.imag(Tau)/self.SinTheta
        self.Scale = np.sqrt(2.0*np.pi*self.Ns*self.MagneticLength**2/(self.Ratio*self.SinTheta))
        self.L1 = self.Scale
        self.L2 = self.Ratio*self.Scale
        if Scale is not None:
            self.Scale = Scale
            self.L1 = self.Scale
            self.L2 = self.Ratio*self.Scale
            self.MagneticLength = np.sqrt(self.L1*self.L2*self.SinTheta/(2.0*np.pi*float(self.Ns)))
        self.Threshold = Threshold
        self.CoefThreshold = CoefThreshold
        self.LL = LL
        self.RefOrbital = 0 # this is the reference orbital which is assigned to j4. To get all interaction coefficients can translate from this
        self.Interaction = Interaction
        self.MPMath = MPMath
        self.Bosonic = Bosonic
        self.PseudoPotentials = PseudoPotentials
        if self.PseudoPotentials is not None:
            self.Polynomial = 0.0
            for i in range(len(self.PseudoPotentials)):
                if np.abs(self.PseudoPotentials[i]) > 0.0:
                    self.Polynomial += self.PseudoPotentials[i] * special.laguerre(i)

        self.Thickness = Thickness
        self.AppendPseudoPotentials = AppendPseudoPotentials


    def CalcInteractionDetails(self,k, m):
        """
        Given the separation k, hopping m and using reference orbital self.RefOrbital, returns a list with 6 elements
        with j1,j2,j3,j4,separation,hopping, where ji are specific orbitals and separation and hopping are signed values.
        """
        j4 = self.RefOrbital  # place j4 on first orbital
        j3 = j4 + k
        Separation = k
        Phase = 1.0
        if k > (self.Ns/2):
            Separation = self.Ns - k
            Phase = -1.0
        Hopping = Phase * m
        if m > (self.Ns/2):
            Hopping = -(self.Ns - m) * Phase
        j1 = (j4 + self.Ns - m) % self.Ns
        j2 = (j3 + self.Ns + m) % self.Ns
        return  [j1,j2,j3,j4,Separation,Hopping]


    def CalculateCoefficients(self):
        """
        Higher level function to calculate interaction coefficients and store for later use.
        """
        # If want to use multiprecision then use a different function to calculate the coefficients.
        if self.MPMath == True:
            InteractionFunc = self.TwoBodyCoefficientMP
        else:
            InteractionFunc = self.TwoBodyCoefficient

        ExchangePhase = -1.0
        if self.Bosonic == True:
            ExchangePhase = 1.0

        # Create 2D array to store non anti-symmeterised coefficients
        self.InteractionCoefs = np.zeros([self.Ns,self.Ns],dtype=np.complex128)
        # Create a list which will contain more lists, containing interaction details corresponding to the InteractionCoefs.
        self.InteractionDetails = list()
        for k in np.arange(0,self.Ns):
            self.InteractionDetails.append(list())
            for m in np.arange(0,self.Ns):
                InteractionDetails =  self.CalcInteractionDetails(k, m)
                self.InteractionCoefs[k,m] = InteractionFunc(InteractionDetails)
                self.InteractionDetails[k].append(InteractionDetails)
        self.NumberCoefs = self.Ns**2

        # Now setup the AntiSymmetric coefficients.
        # Do first in fully in python to get non zero coefficients and indexing.
        # list for annihilation index operators
        AAOps = list() # list of j3,j4 pairs
        AdAdOps = list() # list of lists of j1,j2 pairs and coefficient values
        NumNonZeros = list() # list of non zeros in at each j3,j4 pair
        self.NumPairs = 0
        self.NumDensity = 0 # the number of density density interactions
        NNOps = list()

        if self.Bosonic:
            for j4 in np.arange(0, self.Ns):
                for j3 in np.arange(j4, self.Ns):
                    AAOps.append([j3,j4])
                    count = 0
                    Tmp = list()
                    k = j3 - j4 # separation index, j3 always greater than j4
                    for j2 in np.arange(0, self.Ns):
                        j1 = (j3 + j4 - j2) % self.Ns
                        if j1 >= j2:
                            m = (j2 - j3) % self.Ns # hopping index
                            AntiSymCoef = 2.0 * self.InteractionCoefs[k,m] + ExchangePhase * 2.0 * self.InteractionCoefs[k,(j1-j3) % self.Ns]
                            if j1 == j2:
                                AntiSymCoef /= 2.0
                            if j3 == j4:
                                AntiSymCoef /= 2.0
                            if np.abs(AntiSymCoef) > self.CoefThreshold:
                                if j3 == j1 and j4 == j2: # if it is a density density term, treat it differenctly
                                    NNOps.append([j1, j2, AntiSymCoef])
                                    self.NumDensity += 1
                                else:
                                    Tmp.append([j1,j2,AntiSymCoef])
                                    count += 1
                    AdAdOps.append(Tmp)
                    NumNonZeros.append(count)
                    if count > 0:
                        self.NumPairs += 1
        else:
            for j4 in np.arange(0, self.Ns - 1):
                for j3 in np.arange(j4 + 1, self.Ns):
                    AAOps.append([j3,j4])
                    count = 0
                    Tmp = list()
                    k = j3 - j4 # separation index, j3 always greater than j4
                    for j2 in np.arange(0, self.Ns - 1):
                        j1 = (j3 + j4 - j2) % self.Ns
                        if j1 > j2:
                            m = (j2 - j3) % self.Ns # hopping index
                            AntiSymCoef = 2.0 * self.InteractionCoefs[k,m] + ExchangePhase * 2.0 * self.InteractionCoefs[k,(j1-j3) % self.Ns]
                            if np.abs(AntiSymCoef) > self.CoefThreshold:
                                if j3 == j1 and j4 == j2: # if it is a density density term, treat it differenctly
                                    NNOps.append([j1, j2, -AntiSymCoef])  # negative sign here due to re-ordering of operators.
                                    self.NumDensity += 1
                                else:
                                    Tmp.append([j1,j2,AntiSymCoef])
                                    count += 1
                    AdAdOps.append(Tmp)
                    NumNonZeros.append(count)
                    if count > 0:
                        self.NumPairs += 1


        #Now we create the final arrays
        self.J3J4Vals = np.zeros([self.NumPairs,2],dtype=np.int64)
        self.J1J2Vals = list()
        self.AntiSymCoefs = list()
        self.NonZeros = np.zeros(self.NumPairs,dtype=np.int64)

        Idx = 0
        for i in np.arange(0, AAOps.__len__()):
            if NumNonZeros[i] > 0:
                self.J3J4Vals[Idx,:] = AAOps[i]
                self.J1J2Vals.append(np.zeros([NumNonZeros[i],2],dtype=np.int64))
                self.AntiSymCoefs.append(np.zeros(NumNonZeros[i],dtype=np.complex128))
                self.NonZeros[Idx] = NumNonZeros[i]
                for j in np.arange(0, NumNonZeros[i]):
                    self.J1J2Vals[Idx][j,:] = AdAdOps[i][j][0:2]
                    self.AntiSymCoefs[Idx][j] = AdAdOps[i][j][2]
                Idx += 1

        #Now create final arrays for density density interactions
        self.N1N2Vals = np.zeros([self.NumDensity,2], dtype=np.int64)
        self.DensityCoefs = np.zeros(self.NumDensity, dtype=np.complex128)
        for i in np.arange(0, self.NumDensity):
            self.N1N2Vals[i,:] = NNOps[i][0:2]
            self.DensityCoefs[i] = NNOps[i][2]


    # Coulomb interaction for a given s and t
    def VCoulomb(self, double qxSqrd, double qySqrd):
        """
        Calculates the Fourier series coefficients of the Coulomb interaction at recipricol lattice point qx, qy.
        TODO: Look at inlining for further speed gains.

        Currently the potential is defined as V=1/(2*k), which corresponds to 
        V = 1/(4*pi*r)
        As the physical potential is V = e^2/(4*pi*epsilon_0*r)
        There is an (understood) factor of e^2/(epsilon_0)=1 in the potential.
        """
        
        #return 1.0/sqrt(qxSqrd + qySqrd)
        cdef double k = sqrt(qxSqrd + qySqrd)
        return 2.0*np.pi*exp(-self.Thickness*k/2.0)/k


    # Delta interaction for a given s and t
    def VDelta(self, double qxSqrd, double qySqrd):
        """
        Calculates the Fourier series coefficients of the Delta interaction at recipricol lattice point qx, qy.

        TODO: Look at inlining for further speed gains.
        """
        return (1.0 - qxSqrd - qySqrd)


    # Corresponds to non zero pseudopotentials V_1 and V_3
    def V1V3(self, double qxSqrd, double qySqrd):
        """
        Laguerre polynomial 5 for args.

        """
        x = qxSqrd + qySqrd

        return (-pow(x, 3.0) + 9.0*pow(x, 2.0) - 24.0*x + 12.0)/6.0


    # Corresponds to non zero pseudopotential V_3
    def V3(self, double qxSqrd, double qySqrd):
        """
        Laguerre polynomial 5 for args.
        """
        x = qxSqrd + qySqrd

        return (-pow(x, 3.0) + 9.0*pow(x, 2.0) - 18.0*x + 6.0)/6.0


    def EvaluatePseudoPotentials(self, double qxSqrd, double qySqrd):
        """
        Evaluate V corresponding to the provided pseudopotentials
        """
        x = qxSqrd + qySqrd

        if np.isscalar(self.Polynomial):
            return self.Polynomial
        else:
            return self.Polynomial(x)


    def PseudoPotentialsAndCoulomb(self, double qxSqrd, double qySqrd):
        """
        Evaluate both Coulomb and pseudopotentials
        """
        return self.EvaluatePseudoPotentials(qxSqrd, qySqrd) + (0.0 if (qxSqrd == 0.0 and qySqrd == 0.0) else self.VCoulomb(qxSqrd, qySqrd) )



    # This function gets hopping energy from j1, j2 to j3, j4
    def TwoBodyCoefficient(self,InteractionDetails):
        """
        Calculates the two body Coulomb interaction coefficients A_j1j2j3j4 between particles with
        momentum j3, j4 anad paticles with momentum j1, j2.

        Consists of iterations in two directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.

        TODO: precalculate and type terms involving member variables to get further gains in efficiency.
        """
        cdef double j1 = InteractionDetails[0]
        cdef double j2 = InteractionDetails[1]
        cdef double j3 = InteractionDetails[2]
        cdef double j4 = InteractionDetails[3]
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = 1.0/(2.0*self.L1*self.L2*self.SinTheta)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = self.MagneticLength**2
        cdef double sidx = 0.0
        cdef double s, t, Coeff2, qxSqrd, qySqrd, Coeff3, LLFactor, PhaseFactor
        cdef double tstart = 0.0 # what should t start at if s is 0, 0 for all but Coulomb.
        cdef double V
        PhaseFactor = (j3-j1)*TwoPi/self.Ns

        if self.PseudoPotentials is not None:
            if self.AppendPseudoPotentials:
                VFunc = self.PseudoPotentialsAndCoulomb
            else:
                VFunc = self.EvaluatePseudoPotentials
        elif self.Interaction == 'Coulomb':
            VFunc = self.VCoulomb
            tstart = 1.0
        elif self.Interaction == 'Delta':
            VFunc = self.VDelta
        elif self.Interaction == 'V1V3':
            VFunc = self.V1V3
        elif self.Interaction == 'V3':
            VFunc = self.V3
#            Coeff *= (self.L1**2 + self.L2**2) # this is to counter the algebraic decay for the detla interaction

        # The first while loop is over positive s values, starting at (j1-j4) offset by zero.
        sidx = 0.0
        while 1:
            s = <double>((j1 - j4 + self.Ns) % self.Ns + sidx*self.Ns)
            qxSqrd = pow(TwoPi*s/self.L1, 2.0)
            Coeff2 = exp(-LBSqrd*qxSqrd/2.0) * Coeff
            if s == 0.0:
                t = tstart
            else:
                t = 0.0

            # Loop over positive t values, including 0 if s != 0
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta))
                             + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                V = VFunc(qxSqrd,qySqrd)
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t += 1.0

            t = -1.0

            # Loop over negative t values
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                V = VFunc(qxSqrd,qySqrd)
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t -= 1.0

            if Coeff2 == 0.0 or Coeff2 < self.Threshold:
                break
            sidx += 1.0

        # The second while loop is over negative s values, starting at (j1-j4) offset by -Ns
        sidx = -1.0
        while 1:
            s = <double>((j1 - j4 + self.Ns) % self.Ns + sidx*self.Ns)
            qxSqrd = pow(TwoPi*s/self.L1, 2.0)
            Coeff2 = exp(-LBSqrd*qxSqrd/2.0) * Coeff
            if s == 0.0:
                t = tstart
            else:
                t = 0.0

            # Loop over positive t values, including 0 if s != 0
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                V = VFunc(qxSqrd,qySqrd)
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t += 1.0

            # Loop over negative t values
            t = -1.0
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                V = VFunc(qxSqrd,qySqrd)
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t -= 1.0

            if Coeff2 == 0.0 or Coeff2 < self.Threshold:
                break
            sidx -= 1.0

        return np.complex(AReal, AImag)

    # Coulomb interaction for a given s and t
    def VCoulombMP(self, qxSqrd, qySqrd):
        """
        Calculates the Fourier series coefficients of the Coulomb interaction at recipricol lattice point qx, qy. Using multiprecision module.

        Currently the potential is defined as V=1/(2*k), which corresponds to 
        V = 1/(4*pi*r)
        As the physical potential is V = e^2/(4*pi*epsilon_0*r)
        There is an (understood) factor of e^2/(epsilon_0)=1 in the potential.
        """

        return 2.0*mpmath.pi/sqrt(qxSqrd + qySqrd)



    # This function gets hopping energy from j1, j2 to j3, j4
    def TwoBodyCoefficientMP(self,InteractionDetails):
        """
        Calculates the two body Coulomb interaction coefficients A_j1j2j3j4 between particles with
        momentum j3, j4 anad paticles with momentum j1, j2 using multiprecision library for greater accuracy.

        TODO: Make more use of cython to increase efficiency. This method is slow compared to regular method.
        """

        j1 = InteractionDetails[0]
        j2 = InteractionDetails[1]
        j3 = InteractionDetails[2]
        j4 = InteractionDetails[3]
        TwoPi = 2.0*mpmath.pi
        Coeff = 1.0/(2.0*self.L1*self.L2*self.SinTheta)
        A = 0.0
        LBSqrd = self.MagneticLength**2
        sidx = 0.0
        while 1:
            sp = float((j1 - j4 + self.Ns) % self.Ns + sidx*self.Ns)
            slist = [sp]
            if sidx != 0:
                sm = float((j1 - j4 + self.Ns) % self.Ns - sidx*self.Ns)
                slist.append(sm)
            Coeff2Max = 0.0
            NewVal1 = 0.0
            for s in slist:
                qx = TwoPi*s/self.L1
                Coeff2 = mpmath.exp(-LBSqrd*qx**2/2.0) * Coeff
                if Coeff2 > Coeff2Max:
                    Coeff2Max = Coeff2
                if np.float(s) == 0.0:
                    start = 1.0
                else:
                    start = 0.0
                tidx = start
                while 1:
                    tlist = [tidx]
                    if tidx != 0.0:
                        tlist.append(-tidx)
                    NewVal2 = 0.0
                    Coeff3Max = 0.0
                    for t in tlist:
                        qy = (-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta)
                        LLFactor = 1.0
                        if self.LL == 1:
                            LLFactor = (1.0 - LBSqrd**2*(qx**2+qy**2)/2.0)**2
                        Coeff3 = Coeff2 * self.VCoulombMP(qx**2,qy**2) * mpmath.exp(-(LBSqrd/2.0)*(qy**2)) * LLFactor
                        if Coeff3 > Coeff3Max:
                            Coeff3Max = Coeff3
                        NewVal2 += Coeff3 * mpmath.exp(np.complex(0,((TwoPi*t/self.Ns)*(j3-j1))))
                    NewVal1 += NewVal2
                    if Coeff3Max == 0.0 or Coeff3Max < self.Threshold:
                        break
                    tidx += 1.0
            A += NewVal1
            if Coeff2Max == 0.0 or Coeff2Max < self.Threshold:
                break
            sidx += 1.0
        return np.complex(A)


    def WriteCoefficients(self,filename):
        """
        Write cofficients to a csv file.
        """
        if sys.version_info[0] == 3:
            f = open(filename, "w", encoding='utf8', newline='') # python 3 version
        elif sys.version_info[0] == 2:
            f = open(filename,"wb")
        csv_writer = csv.writer(f)
        for k in np.arange(0,self.Ns):
            for m in np.arange(0,self.Ns):
                Cols = list()
                Cols.extend(self.InteractionDetails[k][m])
                Cols.append(self.InteractionCoefs[k,m].real)
                Cols.append(self.InteractionCoefs[k,m].imag)
                csv_writer.writerow(Cols)
        f.close()


    def WriteAntiSymCoefficients(self,filename):
        """
        Write anti-symmeterised cofficients to a csv file.
        """
        if sys.version_info[0] == 3:
            f = open(filename, "w", encoding='utf8', newline='') # python 3 version
        elif sys.version_info[0] == 2:
            f = open(filename,"wb")
        csv_writer = csv.writer(f)
        for i in np.arange(0,self.NumPairs):
            for j in np.arange(0,self.NonZeros[i]):
                Cols = [self.J1J2Vals[i][j,0], self.J1J2Vals[i][j,1], self.J3J4Vals[i,0], self.J3J4Vals[i,1], self.AntiSymCoefs[i][j].real, self.AntiSymCoefs[i][j].imag]
                csv_writer.writerow(Cols)
        f.close()



    def PrintDetails(self):
        """
        Print some details about the torus
        """
        print "--------------------"
        print "L1: " + str(self.L1)
        print "L2: " + str(self.L2)
        print "SinTheta: " + str(self.SinTheta)
        print "MagneticLength: " + str(self.MagneticLength)


    def GetCoefficientByIndex(self,Idx,AntiSym=False):
        """
        Return coefficient corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        if not AntiSym :
            return self.InteractionCoefs[k,m]
        else:
            if m < ((-k-m) % self.Ns):
                return 2.0 * self.InteractionCoefs[k,m] - 2.0 * self.InteractionCoefs[k, (-k-m) % self.Ns]
            else:
                return 0.0


    def GetSeparationByIndex(self,Idx):
        """
        Return separation distance corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        return self.InteractionDetails[k][m][4]


    def GetHoppingByIndex(self,Idx):
        """
        Return hopping distance corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        return self.InteractionDetails[k][m][5]


    def CoefficientCount(self):
        """
        Return the number of (non anti-symmeterised) coefficients
        """
        return self.NumberCoefs




class FQHTorusInteractionCoefficients3Body:
    """
    Class to group methods and data structures for calculating 3-body interaction coefficients for FQH systems on tori.

    """


    def __init__(self, Tau=np.complex(0,1.0), Ns=10, Interaction='Delta', Threshold=1e-15, LL=0, MagneticLength=1.0,Scale=None,ApplyModularS=False, MPMath=False, CoefThreshold=0.0, Bosonic=False):
        """
        Constructor which sets up the class.
        """
        self.Tau = Tau
        if ApplyModularS == True:
            self.Tau = -1.0/Tau
        self.Ns = Ns
        self.MagneticLength = MagneticLength
        self.Theta = np.angle(self.Tau)
        self.SinTheta = np.sin(self.Theta)
        self.CosTheta = np.cos(self.Theta)
        self.Ratio = np.imag(Tau)/self.SinTheta
        self.Scale = np.sqrt(2.0*np.pi*self.Ns*self.MagneticLength**2/(self.Ratio*self.SinTheta))
        self.L1 = self.Scale
        self.L2 = self.Ratio*self.Scale
        if Scale is not None:
            self.Scale = Scale
            self.L1 = self.Scale
            self.L2 = self.Ratio*self.Scale
            self.MagneticLength = np.sqrt(self.L1*self.L2*self.SinTheta/(2.0*np.pi*float(self.Ns)))
        self.Threshold = Threshold
        self.CoefThreshold = CoefThreshold
        self.LL = LL
        self.RefOrbital = 0 # this is the reference orbital which is assigned to j4. To get all interaction coefficients can translate from this
        self.Interaction = Interaction
        self.MPMath = MPMath
        self.Bosonic = Bosonic
        self.InteractionCoefsReduced = dict()


    def CalcInteractionDetails(self, k1, k2, m1, m2):
        """
        Given the separations k1 and k2, hopping distances m1 and m2 and using reference orbital self.RefOrbital, returns a list with 10 elements
        with j1,j2,j3,j4,j5,j6,separation1,separation2,hopping1,hopping2 where ji are specific orbitals and separation and hopping are signed values.
        """
        j6 = self.RefOrbital  # place j6 on first orbital
        j4 = j6 + k1
        j5 = j6 + k2
        Separation1 = k1
        Separation2 = k2
        Phase = 1.0
        if k1 > (self.Ns/2):
            Separation1 = self.Ns - k1
            Separation2 = self.Ns - k2
            Phase = -1.0

        Hopping1 = Phase * m1
        Hopping2 = Phase * m2
        if m1 > (self.Ns/2):
            Hopping1 = -(self.Ns - m1) * Phase
        if m2 > (self.Ns/2):
            Hopping2 = -(self.Ns - m2) * Phase

        j1 = (j6 + self.Ns - m1) % self.Ns
        j2 = (j5 + self.Ns - m2) % self.Ns
        j3 = (j4 + j5 + j6 - j1 - j2) % self.Ns
        return  [j1,j2,j3,j4,j5,j6,Separation1,Separation2,Hopping1,Hopping2]


    def CalculateCoefficients(self):
        """
        Higher level function to calculate interaction coefficients and store for later use.
        """
        # If want to use multiprecision then use a different function to calculate the coefficients.

        if self.Bosonic:
            SymmetriseFunc = Symmetrise
            InteractionFunc = self.ThreeBodyDeltaBosonic
        else:
            SymmetriseFunc = AntiSymmetrise
            InteractionFunc = self.ThreeBodyDeltaFermionicFull

        self.InteractionCoefs = dict()
        self.InteractionCoefsCounts = dict()
        AAAOps = list() # list of j3,j4 pi#airs
        AdAdAdOps = list() # list of lists of j1,j2 pairs and coefficient values
        NumNonZeros = list() # list of non zeros in at each j3,j4 pair
        self.NumPairs = 0
        self.NumDensity = 0 # the number of density density interactions
        NNNOps = list()
        cdef int j1, j2, j3, j4, j5, j6

        if self.Bosonic:
            for j4 in np.arange(0, self.Ns):
                for j5 in np.arange(j4, self.Ns):
                    for j6 in np.arange(j5, self.Ns):
                        AAAOps.append([j4,j5,j6])
                        count = 0
                        Tmp = list()
                        for j1 in np.arange(0, self.Ns):
                            for j2 in np.arange(j1, self.Ns):
                                j3 = (2 * self.Ns + j4 + j5 + j6 - j1 - j2) % self.Ns
                                if j3 >= j2:
                                    SymCoef = 0.0
                                    [combs1, phases1] = SymmetriseFunc(np.array([j4, j5, j6], dtype=np.int32))
                                    for i in np.arange(0, combs1.__len__()):
                                        [combs2, phases2] = SymmetriseFunc(np.array([j1, j2, j3], dtype=np.int32))
                                        for j in np.arange(0, combs2.__len__()):
                                            InteractionDetails = combs2[j]
                                            InteractionDetails.extend(combs1[i])
                                            key = tuple(InteractionDetails)
                                            if key in self.InteractionCoefs:
                                                val = self.InteractionCoefs[key]
                                                self.InteractionCoefsCounts[key] += 1
                                            else:
                                                val = InteractionFunc(key)
                                                self.InteractionCoefs[key] = val
                                                self.InteractionCoefsCounts[key] = 1
                                                SymCoef += val * phases1[i] * phases2[j]
                                    if np.abs(SymCoef) > self.CoefThreshold:
                                        if j4 == j1 and j5 == j2 and j6 == j3: # if it is a density density term, treat it differenctly
                                            NNNOps.append([j1, j2, j3, SymCoef])
                                            self.NumDensity += 1
                                        else:
                                            Tmp.append([j1,j2,j3,SymCoef])
                                            count += 1
                        AdAdAdOps.append(Tmp)
                        NumNonZeros.append(count)
                        self.NumPairs += 1
        else: # Fermionic case
            for j4 in np.arange(0, self.Ns - 2 ):
                for j5 in np.arange(j4 + 1, self.Ns - 1):
                    for j6 in np.arange(j5 + 1, self.Ns):
                        AAAOps.append([j4,j5,j6])
                        count = 0
                        Tmp = list()
                        for j1 in np.arange(0, self.Ns - 2):
                            for j2 in np.arange(j1 + 1, self.Ns - 1):
                                j3 = (2 * self.Ns + j4 + j5 + j6 - j1 - j2) % self.Ns
                                if j3 > j2:
                                    SymCoef = 0.0
                                    [combs1, phases1] = SymmetriseFunc([j4, j5, j6])
                                    for i in np.arange(0, combs1.__len__()):
                                        [combs2, phases2] = SymmetriseFunc([j1, j2, j3])
                                        for j in np.arange(0, combs2.__len__()):
                                            InteractionDetails = combs2[j]
                                            InteractionDetails.extend(combs1[i])
                                            key = tuple(InteractionDetails)
                                            val = InteractionFunc(key)
                                            self.InteractionCoefs[key] = val
                                            SymCoef += val * phases1[i] * phases2[j]
                                    if np.abs(SymCoef) > self.CoefThreshold:
                                        if j4 == j1 and j5 == j2 and j6 == j3: # if it is a density density term, treat it differenctly
                                            NNNOps.append([j1, j2, j3, -SymCoef]) # negative sign here due to re-ordering of operators.
                                            self.NumDensity += 1
                                        else:
                                            Tmp.append([j1,j2,j3, SymCoef])
                                            count += 1
                        AdAdAdOps.append(Tmp)
                        NumNonZeros.append(count)
                        self.NumPairs += 1


        #Now we create the final arrays
        self.J4J5J6Vals = np.zeros([self.NumPairs,3],dtype=np.int64)
        self.J1J2J3Vals = list()
        self.SymCoefs = list()
        self.NonZeros = np.zeros(self.NumPairs,dtype=np.int64)
        Idx = 0
        for i in np.arange(0, AAAOps.__len__()):
            if NumNonZeros[i] > 0:
                self.J4J5J6Vals[Idx,:] = AAAOps[i]
                self.J1J2J3Vals.append(np.zeros([NumNonZeros[i],3],dtype=np.int64))
                self.SymCoefs.append(np.zeros(NumNonZeros[i],dtype=np.complex128))
                self.NonZeros[Idx] = NumNonZeros[i]
                for j in np.arange(0, NumNonZeros[i]):
                    self.J1J2J3Vals[Idx][j,:] = AdAdAdOps[i][j][0:3]
                    self.SymCoefs[Idx][j] = AdAdAdOps[i][j][3]
                Idx += 1

        #Now create final arrays for density density interactions
        self.N1N2N3Vals = np.zeros([self.NumDensity,3], dtype=np.int64)
        self.DensityCoefs = np.zeros(self.NumDensity, dtype=np.complex128)
        for i in np.arange(0, self.NumDensity):
            self.N1N2N3Vals[i,:] = NNNOps[i][0:3]
            self.DensityCoefs[i] = NNNOps[i][3]


    # This function gets hopping energy from the interaction details.
    @cython.cdivision(True)
    def ThreeBodyDeltaBosonic(self,InteractionDetails):
        """
        Calculates the three body delta interaction coefficients A_j1j2j3j4j5j6 between particles with
        momentum j4, j5, j6 anad paticles with momentum j1, j2, j3.

        Consists of iterations in four directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.

        TODO: precalculate and type terms involving member variables to get further gains in efficiency.
        """
        cdef double j1 = InteractionDetails[0]
        cdef double j2 = InteractionDetails[1]
        cdef double j3 = InteractionDetails[2]
        cdef double j4 = InteractionDetails[3]
        cdef double j5 = InteractionDetails[4]
        cdef double j6 = InteractionDetails[5]
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = 1.0/(2.0 * (self.L1*self.L2*self.SinTheta)**2 * self.MagneticLength**3)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = self.MagneticLength**2
        cdef double saidx = 0.0
        cdef double sbidx = 0.0
        cdef double sa, ta, sb, tb, Coeff1, Coeff2, Coeff3, Coeff4, qxaSqrd, qxbSqrd, qyaSqrd, qybSqrd, LLFactor, PhaseFactor1, PhaseFactor2, PhaseFactor3, FullCoeff
        cdef double qxa, qxb, qya, qyb, Threshold, sastep, sbstep, qxfactor, qyfactor1, qyfactor2
        cdef int Ns = self.Ns
        cdef double Nsd = <double>self.Ns
        PhaseFactor1 = (j1-j5)*TwoPi/Nsd
        PhaseFactor2 = (j1-j4)*TwoPi/Nsd
        PhaseFactor3 = np.pi/Nsd
        Threshold = self.Threshold
        qxfactor = TwoPi/self.L1 # factor that multiply s's by to get qx
        qyfactor1 = TwoPi/(self.L2*self.SinTheta) # factor that multiply t's by to get qy
        qyfactor2 = TwoPi*self.CosTheta/(self.L1*self.SinTheta) # factor that multiply s's by to get qy

        # Interate over positive sa's until we converge.
        saStarts = [0.0, -1.0]
        saSteps = [1.0, -1.0]
        sbStarts = [0.0, -1.0]
        sbSteps = [1.0, -1.0]
        for i in [0,1]:
            saidx = saStarts[i]
            sastep = saSteps[i]
            while 1:
                sa = <double>((j2 - j5 + Ns) % Ns + saidx*Ns)
                qxa = qxfactor*sa
                qxaSqrd = pow(qxa, 2.0)
                Coeff1 = exp(-LBSqrd*qxaSqrd/2.0)

                # Interate over positive sb's until we converge.
                for j in [0,1]:
                    sbidx = sbStarts[j]
                    sbstep = sbSteps[j]
                    while 1:
                        sb = <double>((j3 - j4 + Ns) % Ns + sbidx*Ns)
                        qxb = qxfactor*sb
                        qxbSqrd = pow(qxb, 2.0)
                        Coeff2 = exp(-LBSqrd*qxbSqrd/2.0)
                        #if cython.cmod(sa + sb, Ns) == cython.cmod(j6 - j1, Ns):
                        # Now iterate over the ta's and tb's until convergence.
                        ta = 0.0
                        while 1:
                            qya = qyfactor1*ta - qyfactor2*sa
                            qyaSqrd = pow(qya, 2.0)
                            Coeff3 = exp(-LBSqrd*qyaSqrd/2.0)

                            tb = 0.0
                            while 1:
                                qyb = qyfactor1*tb - qyfactor2*sb
                                qybSqrd = pow(qyb, 2.0)
                                Coeff4 = exp(-LBSqrd*(qybSqrd)/2.0)
                                FullCoeff = Coeff * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qyaSqrd + qybSqrd + qxa*qxb + qya*qyb)/2.0)
                                AReal += FullCoeff * cos(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                AImag += FullCoeff * sin(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                    break
                                tb += 1.0

                            tb = -1.0
                            while 1:
                                qyb = qyfactor1*tb - qyfactor2*sb
                                qybSqrd = pow(qyb, 2.0)
                                Coeff4 = exp(-LBSqrd*(qybSqrd)/2.0)
                                FullCoeff = Coeff * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qyaSqrd + qybSqrd + qxa*qxb + qya*qyb)/2.0)
                                AReal += FullCoeff * cos(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                AImag += FullCoeff * sin(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                    break
                                tb -= 1.0

                            if Coeff3 == 0.0 or fabs(Coeff3) < Threshold:
                                break

                            ta += 1.0

                        # Now iterate over the ta's and tb's until convergence.
                        ta = -1.0
                        while 1:
                            qya = qyfactor1*ta - qyfactor2*sa
                            qyaSqrd = pow(qya, 2.0)
                            Coeff3 = exp(-LBSqrd*qyaSqrd/2.0)

                            tb = 0.0
                            while 1:
                                qyb = qyfactor1*tb - qyfactor2*sb
                                qybSqrd = pow(qyb, 2.0)
                                Coeff4 = exp(-LBSqrd*(qybSqrd)/2.0)
                                FullCoeff = Coeff * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qyaSqrd + qybSqrd + qxa*qxb + qya*qyb)/2.0)
                                AReal += FullCoeff * cos(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                AImag += FullCoeff * sin(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                    break
                                tb += 1.0

                            tb = -1.0
                            while 1:
                                qyb = qyfactor1*tb - qyfactor2*sb
                                qybSqrd = pow(qyb, 2.0)
                                Coeff4 = exp(-LBSqrd*(qybSqrd)/2.0)
                                FullCoeff = Coeff * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qyaSqrd + qybSqrd + qxa*qxb + qya*qyb)/2.0)
                                AReal += FullCoeff * cos(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                AImag += FullCoeff * sin(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(sa*tb + sb*ta))
                                if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                    break
                                tb -= 1.0
                            if Coeff3 == 0.0 or fabs(Coeff3) < Threshold:
                                break
                            ta -= 1.0
                    #else:
                    #        print "Exception! " + str((sa + sb) % Ns) + " != " + str((j6 - j1 + Ns) % Ns)
                        if Coeff2 == 0.0 or fabs(Coeff2) < Threshold:
                            break
                        sbidx += sbstep
                if Coeff1 == 0.0 or fabs(Coeff1) < Threshold:
                    break
                saidx += sastep

        return np.complex(AReal, AImag)


    # This function gets hopping energy from the interaction details.
    def ThreeBodyDeltaFermionicFull(self,InteractionDetails):
        """
        Calculates the three body delta interaction coefficients A_j1j2j3j4j5j6 between particles with
        momentum j4, j5, j6 anad paticles with momentum j1, j2, j3.
        """
        cdef double j1 = InteractionDetails[0]
        cdef double j2 = InteractionDetails[1]
        cdef double j3 = InteractionDetails[2]
        cdef double j4 = InteractionDetails[3]
        cdef double j5 = InteractionDetails[4]
        cdef double j6 = InteractionDetails[5]

        key = ((j2 - j1) % self.Ns, (j1 - j3) % self.Ns, (j5 - j2) % self.Ns, (j3 - j4) % self.Ns)
        if key in self.InteractionCoefsReduced:
            val = self.InteractionCoefsReduced[key]
        else:
            val = self.ThreeBodyDeltaFermionic(j2 - j1, j1 - j3, j5 - j2, j3 - j4)
            self.InteractionCoefsReduced[key] = val

        return val


    # This function gets hopping energy from the interaction details.
    def ThreeBodyDeltaFermionic(self, j2mj1, j1mj3, j5mj2, j3mj4):
        """
        Calculates the three body delta interaction coefficients A_j1j2j3j4j5j6 from differences j2 - j1, j1 - j3, j5 - j2 and j3 - j4

        Consists of iterations in four directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.
        """
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = 1.0/(2.0 * (self.L1*self.L2*self.SinTheta)**2 * self.MagneticLength**3)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = self.MagneticLength**2
        cdef double saidx = 0.0
        cdef double sbidx = 0.0
        cdef double sa, sastep, ta, tastep, sb, sbstep, tb, tbstep, Coeff1, Coeff2, Coeff3, Coeff4, qxaSqrd, qxbSqrd, qyaSqrd, qybSqrd, LLFactor, PhaseFactor1, PhaseFactor2, PhaseFactor3, FullCoeff
        cdef double qxa, qxb, qya, qyb, Threshold, qxfactor, qyfactor1, qyfactor2
        cdef int Ns = self.Ns
        cdef double Nsd = <double>self.Ns
        PhaseFactor1 = (j2mj1)*TwoPi/Nsd
        PhaseFactor2 = (j1mj3)*TwoPi/Nsd
        PhaseFactor3 = np.pi/Nsd
        cdef double sabase = (j5mj2 + Nsd) % Nsd
        cdef double sbbase = (j3mj4 + Nsd) % Nsd
        Threshold = self.Threshold
        qxfactor = TwoPi/self.L1 # factor that multiply s's by to get qx
        qyfactor1 = TwoPi/(self.L2*self.SinTheta) # factor that multiply t's by to get qy
        qyfactor2 = TwoPi*self.CosTheta/(self.L1*self.SinTheta) # factor that multiply s's by to get qy

        Starts = [0.0, -1.0]
        Steps = [1.0, -1.0]
        for i in [0,1]:
            saidx = Starts[i]
            sastep = Steps[i]
            while 1:
                sa = (sabase + saidx*Nsd)
                qxa = qxfactor*sa
                qxaSqrd = pow(qxa, 2.0)
                Coeff1 = exp(-LBSqrd*qxaSqrd/2.0)
                for j in [0,1]:
                    sbidx = Starts[j]
                    sbstep = Steps[j]
                    while 1:
                        sb = (sbbase + sbidx*Nsd)
                        qxb = qxfactor*sb
                        qxbSqrd = pow(qxb, 2.0)
                        Coeff2 = exp(-LBSqrd*qxbSqrd/2.0)*Coeff1
                        for k in [0,1]:
                            ta = Starts[k]
                            tastep = Steps[k]
                            while 1:
                                qya = qyfactor1*ta - qyfactor2*sa
                                qyaSqrd = pow(qya, 2.0)
                                Coeff3 = exp(-LBSqrd*qyaSqrd/2.0)*Coeff2
                                for l in [0,1]:
                                    tb = Starts[l]
                                    tbstep = Steps[l]
                                    while 1:
                                        qyb = qyfactor1*tb - qyfactor2*sb
                                        qybSqrd = pow(qyb, 2.0)
                                        Coeff4 = exp(-LBSqrd*(qybSqrd)/2.0)*Coeff3
                                        FullCoeff = -Coeff * (pow(qxa, 4.0) + pow(qya, 4.0))*(pow(qxb - qxa, 2.0) + pow(qyb - qya, 2.0)) * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qyaSqrd + qybSqrd - qxa*qxb - qya*qyb)/2.0)
                                        #FullCoeff = -Coeff * (pow(qxa, 4.0) + pow(qya, 4.0))*(pow(qxb, 2.0) + pow(qyb, 2.0)) * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qyaSqrd + qybSqrd - qxa*qxb - qya*qyb)/2.0)
                                        AReal += FullCoeff * cos(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(2.0*(sa*ta + sb*tb) - sa*tb - sb*ta))
                                        AImag += FullCoeff * sin(PhaseFactor1*ta + PhaseFactor2*tb + PhaseFactor3*(2.0*(sa*ta + sb*tb) - sa*tb - sb*ta))
                                        if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                            break
                                        tb = tb + tbstep
                                if Coeff3 == 0.0 or fabs(Coeff3) < Threshold:
                                    break
                                ta = ta + tastep
                        if Coeff2 == 0.0 or fabs(Coeff2) < Threshold:
                            break
                        sbidx = sbidx + sbstep
                if Coeff1 == 0.0 or fabs(Coeff1) < Threshold:
                    break
                saidx = saidx + sastep

        return np.complex(AReal, AImag)


    def WriteCoefficients(self,filename):
        """
        Write cofficients to a csv file.
        """
        f = open(filename,"w")
        csv_writer = csv.writer(f)
        for key in  self.InteractionCoefs.keys():
            Cols = list()
            Cols.append(key)
            Cols.append(self.InteractionCoefs[key].real)
            Cols.append(self.InteractionCoefs[key].imag)
            Cols.append([key[4]-key[1],key[2]-key[3],key[1]-key[0],key[0]-key[2]])
            Cols.append([(key[4]-key[1]) % self.Ns, (key[2]-key[3]) % self.Ns, (key[1]-key[0]) % self.Ns, (key[0]-key[2]) % self.Ns])
            csv_writer.writerow(Cols)
        f.close()


    def WriteAntiSymCoefficients(self,filename):
        """
        Write anti-symmeterised cofficients to a csv file.
        """
        f = open(filename,"w")
        csv_writer = csv.writer(f)
        for i in np.arange(0,self.NumPairs):
            for j in np.arange(0,self.NonZeros[i]):
                Cols = list()
                Cols.extend(self.J1J2J3Vals[i][j,:])
                Cols.extend(self.J4J5J6Vals[i,0:3])
                Cols.extend([self.SymCoefs[i][j].real, self.SymCoefs[i][j].imag])
                csv_writer.writerow(Cols)
        f.close()



    def PrintDetails(self):
        """
        Print some details about the torus
        """
        print "--------------------"
        print "L1: " + str(self.L1)
        print "L2: " + str(self.L2)
        print "SinTheta: " + str(self.SinTheta)
        print "MagneticLength: " + str(self.MagneticLength)


    def GetCoefficientByIndex(self,Idx,AntiSym=False):
        """
        Return coefficient corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        if not AntiSym :
            return self.InteractionCoefs[k,m]
        else:
            if m < ((-k-m) % self.Ns):
                return 2.0 * self.InteractionCoefs[k,m] - 2.0 * self.InteractionCoefs[k, (-k-m) % self.Ns]
            else:
                return 0.0


    def GetSeparationByIndex(self,Idx):
        """
        Return separation distance corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        return self.InteractionDetails[k][m][4]


    def GetHoppingByIndex(self,Idx):
        """
        Return hopping distance corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        return self.InteractionDetails[k][m][5]


#    def CoefficientCount(self):
#        """
#        Return the number of (non anti-symmeterised) coefficients
#        """
#        return self.NumberCoefs



class FQHTorusInteractionCoefficients4Body:
    """
    Class to group methods and data structures for calculating 4-body interaction coefficients for FQH systems on tori.

    """


    def __init__(self, Tau=np.complex(0,1.0), Ns=10, Interaction='Delta', Threshold=1e-15, LL=0, MagneticLength=1.0,Scale=None,ApplyModularS=False, MPMath=False, CoefThreshold=0.0, Bosonic=False):
        """
        Constructor which sets up the class.
        """
        self.Tau = Tau
        if ApplyModularS == True:
            self.Tau = -1.0/Tau
        self.Ns = Ns
        self.MagneticLength = MagneticLength
        self.Theta = np.angle(self.Tau)
        self.SinTheta = np.sin(self.Theta)
        self.CosTheta = np.cos(self.Theta)
        self.Ratio = np.imag(Tau)/self.SinTheta
        self.Scale = np.sqrt(2.0*np.pi*self.Ns*self.MagneticLength**2/(self.Ratio*self.SinTheta))
        self.L1 = self.Scale
        self.L2 = self.Ratio*self.Scale
        if Scale is not None:
            self.Scale = Scale
            self.L1 = self.Scale
            self.L2 = self.Ratio*self.Scale
            self.MagneticLength = np.sqrt(self.L1*self.L2*self.SinTheta/(2.0*np.pi*float(self.Ns)))
        self.Threshold = Threshold
        self.CoefThreshold = CoefThreshold
        self.LL = LL
        self.RefOrbital = 0 # this is the reference orbital which is assigned to j4. To get all interaction coefficients can translate from this
        self.Interaction = Interaction
        self.MPMath = MPMath
        self.Bosonic = Bosonic
        self.InteractionCoefsReduced = dict()


    def CalculateCoefficients(self):
        """
        Higher level function to calculate interaction coefficients and store for later use.
        """
        # If want to use multiprecision then use a different function to calculate the coefficients.

        if self.Bosonic:
            SymmetriseFunc = Symmetrise
            InteractionFunc = self.FourBodyDeltaBosonicFull
        else:
            SymmetriseFunc = AntiSymmetrise
            InteractionFunc = self.FourBodyDeltaFermionicFull

        self.InteractionCoefs = dict()
        self.InteractionCoefsCounts = dict()
        AAAAOps = list() # list of j3,j4 pi#airs
        AdAdAdAdOps = list() # list of lists of j1,j2 pairs and coefficient values
        NumNonZeros = list() # list of non zeros in at each j3,j4 pair
        self.NumPairs = 0
        self.NumDensity = 0 # the number of density density interactions
        NNNNOps = list()


        cdef np.ndarray[np.int32_t, ndim=1] J1 = np.zeros(4, dtype=np.int32)
        cdef np.ndarray[np.int32_t, ndim=1] J2 = np.zeros(4, dtype=np.int32)
        cdef np.ndarray[np.int32_t, ndim=1] J1C = np.zeros(4, dtype=np.int32)
        cdef np.ndarray[np.int32_t, ndim=1] J2C = np.zeros(4, dtype=np.int32)
        cdef int Ns = self.Ns
        cdef double Norm1, Norm2, NormTmp
        cdef int i = 0

        if self.Bosonic:
            while J2[0] < Ns:
                J2[1] = J2[0]
                while J2[1] < Ns:
                    J2[2] = J2[1]
                    while J2[2] < Ns:
                        J2[3] = J2[2]
                        while J2[3] < Ns:
                            Norm2 = CombinationNorm(J2, 4)
                            #AAAAOps.append([j5,j6,j7,j8])
                            AAAAOps.append([J2[0], J2[1], J2[2], J2[3]])
                            count = 0
                            Tmp = list()
                            J1[0] = 0
                            while J1[0] < Ns:
                                J1[1] = J1[0]
                                while J1[1] < Ns:
                                    J1[2] = J1[1]
                                    while J1[2] < Ns:
                                        J1[3] = 0
                                        J1[3] = cython.cmod(np.sum(J2) - np.sum(J1) + 4 * Ns, Ns)
                                        if J1[3] >= J1[2]:
                                            Norm1 = CombinationNorm(J1, 4)
#                                            print "J1: " + str(J1)
#                                            print "J2: " + str(J2)
#                            for j1 in np.arange(0, Ns):
#                                for j2 in np.arange(j1, Ns):
#                                    for j3 in np.arange(j2, Ns):
#                                        j4 = (4 * Ns + j5 + j6 + j7 + j8 - j1 - j2 - j3) % Ns
#                                        if j4 >= j3:
#                                            SymCoef = 0.0
#                                            [combs1, phases1] = SymmetriseFunc(J2)
##                                            [combs1, phases1] = SymmetriseFunc([j5, j6, j7, j8])
#                                            for i in np.arange(0, combs1.__len__()):
#                                                [combs2, phases2] = SymmetriseFunc(J1)
##                                                [combs2, phases2] = SymmetriseFunc([j1, j2, j3, j4])
#                                                for j in np.arange(0, combs2.__len__()):
#                                                    InteractionDetails = combs2[j]
#                                                    InteractionDetails.extend(combs1[i])
#                                                    key = tuple(InteractionDetails)
#                                                    if self.InteractionCoefs.has_key(key):
#                                                        val = self.InteractionCoefs[key]
#                                                    else:
#                                                        val = InteractionFunc(key)
#                                                        self.InteractionCoefs[key] = val
#                                                    SymCoef += val
#                                            if np.abs(SymCoef) > self.CoefThreshold:
#                                                Tmp.append([J1[0], J1[1], J1[2], J1[3], SymCoef])
#                                                count += 1
                                            SymCoef = 0.0
                                            np.copyto(J2C, J2)
                                            np.copyto(J1C, J1)
                                            while J2C[0] >= 0:
                                                while J1C[0] >= 0:
                                                    key = tuple(np.concatenate((J1C, J2C)))
                                                    #if self.InteractionCoefs.has_key(key):
                                                    #    val = self.InteractionCoefs[key]
                                                    #else:
                                                    #    val = InteractionFunc(key)
                                                    #    self.InteractionCoefs[key] = val
                                                    #SymCoef += val
                                                    SymCoef += InteractionFunc(key)
                                                    J1C = NextPermutation(J1C, 3)
                                                J2C = NextPermutation(J2C, 3)
                                            if np.abs(SymCoef) > self.CoefThreshold:
                                                if J1[0] == J2[0] and J1[1] == J2[1] and J1[2] == J2[2] and J1[3] == J2[3] : # if it is a density density term, treat it differenctly
                                                    NNNNOps.append([J1[0], J1[1], J1[2], J1[3], SymCoef * Norm1 * Norm2])
                                                    self.NumDensity += 1
                                                else:
                                                    #print "Nonzero coef: " + str(J1) + str(J2) + ": " + str(SymCoef)
                                                    Tmp.append([J1[0], J1[1], J1[2], J1[3], SymCoef * Norm1 * Norm2])
                                                    count += 1
                                        J1[2] += 1
                                    J1[1] += 1
                                J1[0] += 1
                            AdAdAdAdOps.append(Tmp)
                            NumNonZeros.append(count)
                            self.NumPairs += 1
                            J2[3] += 1
                        J2[2] += 1
                    J2[1] += 1
                J2[0] += 1

        else: # Fermionic case
            for j5 in np.arange(0, self.Ns - 3):
                for j6 in np.arange(j5 + 1, self.Ns - 2):
                    for j7 in np.arange(j6 + 1, self.Ns - 1):
                        for j8 in np.arange(j7 + 1, self.Ns):
                            AAAAOps.append([j5,j6,j7,j8])
                            count = 0
                            Tmp = list()
                            for j1 in np.arange(0, self.Ns - 3):
                                for j2 in np.arange(j1 + 1, self.Ns - 2):
                                    for j3 in np.arange(j2 + 1, self.Ns - 1):
                                        j4 = (4 * self.Ns + j5 + j6 + j7 + j8 - j1 - j2 - j3) % self.Ns
                                        if j4 > j3:
                                            SymCoef = 0.0
                                            [combs1, phases1] = SymmetriseFunc([j5, j6, j7, j8])
                                            for i in np.arange(0, combs1.__len__()):
                                                [combs2, phases2] = SymmetriseFunc([j1, j2, j3, j4])
                                                for j in np.arange(0, combs2.__len__()):
                                                    InteractionDetails = combs2[j]
                                                    InteractionDetails.extend(combs1[i])
                                                    key = tuple(InteractionDetails)
                                                    val = InteractionFunc(key)
                                                    self.InteractionCoefs[key] = val
                                                    SymCoef += val * phases1[i] * phases2[j]
                                            if np.abs(SymCoef) > self.CoefThreshold:
                                                Tmp.append([j1,j2,j3,j4,SymCoef])
                                                count += 1
                            AdAdAdAdOps.append(Tmp)
                            NumNonZeros.append(count)
                            self.NumPairs += 1


        #Now we create the final arrays
        self.J5J6J7J8Vals = np.zeros([self.NumPairs,4],dtype=np.int64)
        self.J1J2J3J4Vals = list()
        self.SymCoefs = list()
        self.NonZeros = np.zeros(self.NumPairs,dtype=np.int64)
        Idx = 0
        for i in np.arange(0, AAAAOps.__len__()):
            if NumNonZeros[i] > 0:
                self.J5J6J7J8Vals[Idx,:] = AAAAOps[i]
                self.J1J2J3J4Vals.append(np.zeros([NumNonZeros[i],4],dtype=np.int64))
                self.SymCoefs.append(np.zeros(NumNonZeros[i],dtype=np.complex128))
                self.NonZeros[Idx] = NumNonZeros[i]
                for j in np.arange(0, NumNonZeros[i]):
                    self.J1J2J3J4Vals[Idx][j,:] = AdAdAdAdOps[i][j][0:4]
                    self.SymCoefs[Idx][j] = AdAdAdAdOps[i][j][4]
                Idx += 1

        #Now create final arrays for density density interactions
        self.N1N2N3N4Vals = np.zeros([self.NumDensity,4], dtype=np.int64)
        self.DensityCoefs = np.zeros(self.NumDensity, dtype=np.complex128)
        for i in np.arange(0, self.NumDensity):
            self.N1N2N3N4Vals[i,:] = NNNNOps[i][0:4]
            self.DensityCoefs[i] = NNNNOps[i][4]


    # This function gets hopping energy from the interaction details.
    def FourBodyDeltaBosonicFull(self,InteractionDetails):
        """
        """
        cdef double j1 = InteractionDetails[0]
        cdef double j2 = InteractionDetails[1]
        cdef double j3 = InteractionDetails[2]
        cdef double j4 = InteractionDetails[3]
        cdef double j5 = InteractionDetails[4]
        cdef double j6 = InteractionDetails[5]
        cdef double j7 = InteractionDetails[6]
        cdef double j8 = InteractionDetails[7]

        key = ((j1 - j2), (j1 - j3), (j1 - j4), ((j2 - j7) + self.Ns) % self.Ns, ((j3 - j6) + self.Ns) % self.Ns, (j4 - j5) % self.Ns)
        #if self.InteractionCoefsReduced.has_key(key):
        #    val = self.InteractionCoefsReduced[key]
        #else:
        #    val = self.FourBodyDeltaFermionic((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j7) % self.Ns, (j3 - j6) % self.Ns, (j4 - j5) % self.Ns)
        #    self.InteractionCoefsReduced[key] = val

#        return val
        return self.FourBodyDeltaBosonicTesting((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j7) % self.Ns, (j3 - j6) % self.Ns, (j4 - j5) % self.Ns)


    # This function gets hopping energy from the interaction details.
    def FourBodyDeltaFermionicFull(self,InteractionDetails):
        """
        Calculates the three body delta interaction coefficients A_j1j2j3j4j5j6 between particles with
        momentum j4, j5, j6 anad paticles with momentum j1, j2, j3.
        """
        cdef double j1 = InteractionDetails[0]
        cdef double j2 = InteractionDetails[1]
        cdef double j3 = InteractionDetails[2]
        cdef double j4 = InteractionDetails[3]
        cdef double j5 = InteractionDetails[4]
        cdef double j6 = InteractionDetails[5]
        cdef double j7 = InteractionDetails[6]
        cdef double j8 = InteractionDetails[7]

        #key = ((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j6) % self.Ns, (j3 - j7) % self.Ns, (j4 - j8) % self.Ns)
        #key = ((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j7) % self.Ns, (j3 - j6) % self.Ns, (j4 - j5) % self.Ns)
        key = ((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j7) % self.Ns, (j3 - j6) % self.Ns, (j4 - j5) % self.Ns)
        #if self.InteractionCoefsReduced.has_key(key):
        #    val = self.InteractionCoefsReduced[key]
        #else:
        #    val = self.FourBodyDeltaFermionic((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j7) % self.Ns, (j3 - j6) % self.Ns, (j4 - j5) % self.Ns)
        #    self.InteractionCoefsReduced[key] = val

#        return val
        #return self.FourBodyDeltaFermionic((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j7) % self.Ns, (j3 - j6) % self.Ns, (j4 - j5) % self.Ns)
        return self.FourBodyDeltaFermionic((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j7), (j3 - j6), (j4 - j5))
        #return self.FourBodyDeltaFermionic((j1 - j2), (j1 - j3), (j1 - j4), (j2 - j6) % self.Ns, (j3 - j7) % self.Ns, (j4 - j8) % self.Ns)


    def FourBodyDeltaBosonic(self, j1mj2, j1mj3, j1mj4, j2mj7, j3mj6, j4mj5):
        """
        Calculates the four body delta interaction coefficients A_j1...j8 from differences provided.

        Consists of iterations in six directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.
        """
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = 1.0/(2.0 * (self.L1*self.L2*self.SinTheta)**3 * self.MagneticLength**4)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = self.MagneticLength**2
        cdef double saidx = 0.0
        cdef double sbidx = 0.0
        cdef double scidx = 0.0
        cdef double s, t, sa, sastep, ta, tastep, sb, sbstep, tb, tbstep, sc, scstep, tc, tcstep, qxaSqrd, qxbSqrd, qxcSqrd, qyaSqrd, qybSqrd, qycSqrd, qxSqrd, qySqrd
        cdef double Coeff1, Coeff2, Coeff3, Coeff4, Coeff5, Coeff6, LLFactor, FullCoeff
        cdef double qxa, qxb, qxc, qya, qyb, qyc, Threshold, qxfactor, qyfactor1, qyfactor2, Phase
        cdef int Ns = self.Ns
        cdef double Nsd = <double>self.Ns
        cdef double PhaseFactor3 = np.pi/Nsd
        cdef double sabase = (j2mj7 + Nsd) % Nsd
        cdef double sbbase = (j3mj6 + Nsd) % Nsd
        cdef double scbase = (j4mj5 + Nsd) % Nsd
        cdef double j1mj2d = <double>j1mj2, j1mj3d = <double>j1mj3, j1mj4d = <double>j1mj4
        Threshold = self.Threshold
        qxfactor = TwoPi/self.L1 # factor that multiply s's by to get qx
        qyfactor1 = TwoPi/(self.L2*self.SinTheta) # factor that multiply t's by to get qy
        qyfactor2 = TwoPi*self.CosTheta/(self.L1*self.SinTheta) # factor that multiply s's by to get qy

        Starts = [0.0, -1.0]
        Steps = [1.0, -1.0]
        for i in [0,1]:
            saidx = Starts[i]
            sastep = Steps[i]
            while 1:
                sa = (sabase + saidx*Nsd)
                qxa = qxfactor*sa
                qxaSqrd = pow(qxa, 2.0)
                #Coeff1 = exp(-LBSqrd*qxaSqrd/2.0)
                Coeff1 = exp(-LBSqrd*qxaSqrd/4.0)
                for j in [0,1]:
                    sbidx = Starts[j]
                    sbstep = Steps[j]
                    while 1:
                        sb = (sbbase + sbidx*Nsd)
                        qxb = qxfactor*sb
                        qxbSqrd = pow(qxb, 2.0)
                        #Coeff2 = exp(-LBSqrd*qxbSqrd/2.0)*Coeff1
                        Coeff2 = exp(-LBSqrd*qxbSqrd/4.0) * Coeff1
                        for k in [0,1]:
                            scidx = Starts[k]
                            scstep = Steps[k]
                            while 1:
                                sc = (scbase + scidx*Nsd)
                                s = sa + sb + sc
                                qxc = qxfactor*sc
                                qxcSqrd = pow(qxc, 2.0)
                                #Coeff3 = exp(-LBSqrd*qxcSqrd/2.0) * Coeff2
                                Coeff3 = exp(-LBSqrd*qxcSqrd/4.0) * Coeff2
                                qxSqrd = pow(qxa + qxb + qxc, 2.0)
                                for l in [0,1]:
                                    ta = Starts[l]
                                    tastep = Steps[l]
                                    while 1:
                                        qya = qyfactor1*ta - qyfactor2*sa
                                        qyaSqrd = pow(qya, 2.0)
                                        #Coeff4 = exp(-LBSqrd*qyaSqrd/2.0)
                                        Coeff4 = exp(-LBSqrd*qyaSqrd/4.0)
                                        for m in [0,1]:
                                            tb = Starts[m]
                                            tbstep = Steps[m]
                                            while 1:
                                                qyb = qyfactor1*tb - qyfactor2*sb
                                                qybSqrd = pow(qyb, 2.0)
                                                #Coeff5 = exp(-LBSqrd*(qybSqrd)/2.0)
                                                Coeff5 = exp(-LBSqrd*(qybSqrd)/4.0) * Coeff4
                                                for n in [0,1]:
                                                    tc = Starts[n]
                                                    tcstep = Steps[n]
                                                    t = ta + tb + tc
                                                    while 1:
                                                        qyc = qyfactor1*tc - qyfactor2*sc
                                                        qycSqrd = pow(qyc, 2.0)
                                                        qySqrd = pow(qya + qyb + qyc, 2.0)
                                                        #Coeff6 = exp(-LBSqrd*(qycSqrd)/2.0)
                                                        Coeff6 = exp(-LBSqrd*(qycSqrd)/4.0) * Coeff5
                                                        #FullCoeff = Coeff * Coeff3 * Coeff4 * Coeff5 * Coeff6 * exp(-LBSqrd*(qxa*qxb + qxa*qxc + qxb*qxc + qya*qyb + qya*qyc + qyb*qyc)/2.0)
                                                        FullCoeff = Coeff * Coeff3 * Coeff6 * exp(-LBSqrd*(qySqrd + qxSqrd)/4.0)
                                                        Phase = PhaseFactor3*(ta*(s + sa) + tb*(s + sb) + tc*(s + sc)) + (2.0*PhaseFactor3)*(ta*j1mj2d + tb*j1mj3d + tc*j1mj4d)
                                                        AReal += FullCoeff * cos(Phase)
                                                        AImag += FullCoeff * sin(Phase)
                                                        if Coeff6 == 0.0 or fabs(Coeff6) < Threshold:
                                                            break
                                                        tc = tc + tcstep
                                                if Coeff5 == 0.0 or fabs(Coeff5) < Threshold:
                                                    break
                                                tb = tb + tbstep
                                        if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                            break
                                        ta = ta + tastep
                                if Coeff3 == 0.0 or fabs(Coeff3) < Threshold:
                                    break
                                scidx = scidx + scstep
                        if Coeff2 == 0.0 or fabs(Coeff2) < Threshold:
                            break
                        sbidx = sbidx + sbstep
                if Coeff1 == 0.0 or fabs(Coeff1) < Threshold:
                    break
                saidx = saidx + sastep

        return np.complex(AReal, AImag)



    # This function gets hopping energy from the interaction details.
    def FourBodyDeltaFermionic(self, j1mj2, j1mj3, j1mj4, j2mj7, j3mj6, j4mj5):
        """
        Calculates the four body delta interaction coefficients A_j1...j8 from differences provided.

        Consists of iterations in six directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.
        """
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = 1.0/(2.0 * (self.L1*self.L2*self.SinTheta)**3 * self.MagneticLength**4)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = self.MagneticLength**2
        cdef double saidx = 0.0
        cdef double sbidx = 0.0
        cdef double scidx = 0.0
        cdef double s, t, sa, sastep, ta, tastep, sb, sbstep, tb, tbstep, sc, scstep, tc, tcstep, qxaSqrd, qxbSqrd, qxcSqrd, qyaSqrd, qybSqrd, qycSqrd, qxSqrd, qySqrd
        cdef double Coeff1, Coeff2, Coeff3, Coeff4, Coeff5, Coeff6, LLFactor, FullCoeff
        cdef double qxa, qxb, qxc, qya, qyb, qyc, Threshold, qxfactor, qyfactor1, qyfactor2, Phase, V
        cdef int Ns = self.Ns
        cdef double Nsd = <double>self.Ns
        cdef double PhaseFactor3 = np.pi/Nsd
        cdef double sabase = (j2mj7 + Nsd) % Nsd
        cdef double sbbase = (j3mj6 + Nsd) % Nsd
        cdef double scbase = (j4mj5 + Nsd) % Nsd
        cdef double j1mj2d = j1mj2, j1mj3d = j1mj3, j1mj4d = j1mj4
        Threshold = self.Threshold
        Threshold = 1e-10
        qxfactor = TwoPi/self.L1 # factor that multiply s's by to get qx
        qyfactor1 = TwoPi/(self.L2*self.SinTheta) # factor that multiply t's by to get qy
        qyfactor2 = TwoPi*self.CosTheta/(self.L1*self.SinTheta) # factor that multiply s's by to get qy

        Starts = [0.0, -1.0]
        Steps = [1.0, -1.0]
        for i in [0,1]:
            saidx = Starts[i]
            sastep = Steps[i]
            while 1:
                sa = (sabase + saidx*Nsd)
                qxa = qxfactor*sa
                qxaSqrd = pow(qxa, 2.0)
                Coeff1 = exp(-LBSqrd*qxaSqrd/2.0)
                for j in [0,1]:
                    sbidx = Starts[j]
                    sbstep = Steps[j]
                    while 1:
                        sb = (sbbase + sbidx*Nsd)
                        qxb = qxfactor*sb
                        qxbSqrd = pow(qxb, 2.0)
                        Coeff2 = exp(-LBSqrd*(qxbSqrd + qxa*qxb)/2.0)*Coeff1
                        for k in [0,1]:
                            scidx = Starts[k]
                            scstep = Steps[k]
                            while 1:
                                sc = (scbase + scidx*Nsd)
                                s = sa + sb + sc
                                qxc = qxfactor*sc
                                qxcSqrd = pow(qxc, 2.0)
                                Coeff3 = exp(-LBSqrd*(qxcSqrd + qxc*(qxa + qxb))/2.0)*Coeff2
                                qxSqrd = pow(qxa + qxb + qxc, 2.0)
                                for l in [0,1]:
                                    ta = Starts[l]
                                    tastep = Steps[l]
                                    while 1:
                                        qya = qyfactor1*ta - qyfactor2*sa
                                        V = (pow(qxa, 6.0) + pow(qya, 6.0))
                                        qyaSqrd = pow(qya, 2.0)
                                        Coeff4 = (qxaSqrd + qyaSqrd) * exp(-LBSqrd*qyaSqrd/2.0)*Coeff3
                                        for m in [0,1]:
                                            tb = Starts[m]
                                            tbstep = Steps[m]
                                            while 1:
                                                qyb = qyfactor1*tb - qyfactor2*sb
                                                V *= (pow(qxb, 4.0) + pow(qyb, 4.0))
                                                qybSqrd = pow(qyb, 2.0)
                                                Coeff5 = (pow(qxbSqrd, 2.0) + pow(qybSqrd, 2.0)) * exp(-LBSqrd*(qybSqrd + qya*qyb)/2.0)*Coeff4
                                                for n in [0,1]:
                                                    tc = Starts[n]
                                                    tcstep = Steps[n]
                                                    t = ta + tb + tc
                                                    while 1:
                                                        qyc = qyfactor1*tc - qyfactor2*sc
                                                        qycSqrd = pow(qyc, 2.0)
                                                        qySqrd = pow(qya + qyb + qyc, 2.0)
                                                        Coeff6 = (pow(qxcSqrd, 3.0) + pow(qycSqrd, 3.0)) * exp(-LBSqrd*(qycSqrd + qyc*(qya + qyb))/2.0)*Coeff5
                                                        #FullCoeff = Coeff * (pow(qxa, 2.0) + pow(qya, 2.0))*(pow(qxb, 4.0) + pow(qyb, 4.0))*(pow(qxc, 6.0) + pow(qyc, 6.0))* exp(-LBSqrd*(qxaSqrd + qxbSqrd + qxcSqrd + qyaSqrd + qybSqrd + qycSqrd + qxSqrd + qySqrd )/4.0)
                                                        #FullCoeff = Coeff * Coeff6 * exp(-LBSqrd*(qxa*qxb + qxa*qxc + qxb*qxc + qya*qyb + qya*qyc + qyb*qyc)/2.0)
                                                        #FullCoeff = Coeff * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qxcSqrd + qyaSqrd + qybSqrd + qycSqrd + qxSqrd + qySqrd )/4.0)
                                                        FullCoeff = Coeff * Coeff6
                                                        #FullCoeff = Coeff * Coeff1 * Coeff2 * Coeff3 * Coeff4 * Coeff5 * Coeff6 * exp(-LBSqrd*(qxa*qxb + qxa*qxc + qxb*qxc + qya*qyb + qya*qyc + qyb*qyc)/2.0)
                                                        #V *= (pow(qxc, 2.0) + pow(qyc, 2.0))
                                                        Phase = PhaseFactor3*(ta*(s + sa) + tb*(s + sb) + tc*(s + sc)) + (2.0*PhaseFactor3)*(ta*j1mj2d + tb*j1mj3d + tc*j1mj4d)
                                                        AReal += FullCoeff * cos(Phase)
                                                        AImag += FullCoeff * sin(Phase)
                                                        if Coeff6 == 0.0 or fabs(Coeff6) < Threshold:
                                                            break
                                                        tc = tc + tcstep
                                                if Coeff5 == 0.0 or fabs(Coeff5) < Threshold:
                                                    break
                                                tb = tb + tbstep
                                        if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                            break
                                        ta = ta + tastep
                                if Coeff3 == 0.0 or fabs(Coeff3) < Threshold:
                                    break
                                scidx = scidx + scstep
                        if Coeff2 == 0.0 or fabs(Coeff2) < Threshold:
                            break
                        sbidx = sbidx + sbstep
                if Coeff1 == 0.0 or fabs(Coeff1) < Threshold:
                    break
                saidx = saidx + sastep

        return np.complex(AReal, AImag)


    # This function gets hopping energy from the interaction details.
    def FourBodyDeltaFermionicTesting(self, j1mj2, j1mj3, j1mj4, j2mj7, j3mj6, j4mj5):
        """
        Calculates the four body delta interaction coefficients A_j1...j8 from differences provided.

        Consists of iterations in six directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.
        """
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = 1.0/(2.0 * (self.L1*self.L2*self.SinTheta)**3 * self.MagneticLength**4)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = self.MagneticLength**2
        cdef double saidx = 0.0
        cdef double sbidx = 0.0
        cdef double scidx = 0.0
        cdef double s, t, sa, sastep, ta, tastep, sb, sbstep, tb, tbstep, sc, scstep, tc, tcstep, qxaSqrd, qxbSqrd, qxcSqrd, qyaSqrd, qybSqrd, qycSqrd, qxSqrd, qySqrd
        cdef double Coeff1, Coeff2, Coeff3, Coeff4, Coeff5, Coeff6, LLFactor, FullCoeff
        cdef double qxa, qxb, qxc, qya, qyb, qyc, Threshold, qxfactor, qyfactor1, qyfactor2, Phase
        cdef int Ns = self.Ns
        cdef double Nsd = <double>self.Ns
        cdef double PhaseCoeff = np.pi/Nsd
        cdef double sabase = (j2mj7 + Nsd) % Nsd
        cdef double sbbase = (j3mj6 + Nsd) % Nsd
        cdef double scbase = (j4mj5 + Nsd) % Nsd
        cdef double PhaseFactor1 = 2.0 * sabase + sbbase + scbase
        cdef double PhaseFactor2 = sabase + 2.0 * sbbase + scbase
        cdef double PhaseFactor3 = sabase + sbbase + 2.0 * scbase
        cdef double j1mj2d = j1mj2, j1mj3d = j1mj3, j1mj4d = j1mj4
        Threshold = self.Threshold
        qxfactor = TwoPi/self.L1 # factor that multiply s's by to get qx
        qyfactor1 = TwoPi/(self.L2*self.SinTheta) # factor that multiply t's by to get qy
        qyfactor2 = TwoPi*self.CosTheta/(self.L1*self.SinTheta) # factor that multiply s's by to get qy
        tlimit = 4

        Starts = [0.0, -1.0]
        Steps = [1.0, -1.0]
        for i in [0,1]:
            saidx = Starts[i]
            sastep = Steps[i]
            while 1:
                sa = (sabase + saidx*Nsd)
                qxa = qxfactor*sa
                qxaSqrd = pow(qxa, 2.0)
                Coeff1 = exp(-LBSqrd*qxaSqrd/2.0)
                for j in [0,1]:
                    sbidx = Starts[j]
                    sbstep = Steps[j]
                    while 1:
                        sb = (sbbase + sbidx*Nsd)
                        qxb = qxfactor*sb
                        qxbSqrd = pow(qxb, 2.0)
                        Coeff2 = exp(-LBSqrd*qxbSqrd/2.0)
                        for k in [0,1]:
                            scidx = Starts[k]
                            scstep = Steps[k]
                            while 1:
                                sc = (scbase + scidx*Nsd)
                                #print "New sc value: " + str(sc)
                                s = sa + sb + sc
                                qxc = qxfactor*sc
                                qxcSqrd = pow(qxc, 2.0)
                                Coeff3 = exp(-LBSqrd*qxcSqrd/2.0)
                                qxSqrd = pow(qxa + qxb + qxc, 2.0)
                                BReal = 0.0
                                BImag = 0.0
                                for l in [0,1]:
                                    ta = Starts[l]
                                    tastep = Steps[l]
                                    while 1:
                                        qya = qyfactor1*ta - qyfactor2*sa
                                        qyaSqrd = pow(qya, 2.0)
                                        Coeff4 = (pow(qxaSqrd, 2.0) + pow(qyaSqrd, 2.0)) * exp(-LBSqrd*qyaSqrd/2.0)*Coeff3
                                        for m in [0,1]:
                                            tb = Starts[m]
                                            tbstep = Steps[m]
                                            while 1:
                                                qyb = qyfactor1*tb - qyfactor2*sb
                                                qybSqrd = pow(qyb, 2.0)
                                                Coeff5 = (pow(qxbSqrd, 2.0) + pow(qybSqrd, 2.0)) * exp(-LBSqrd*(qybSqrd)/2.0)*Coeff4
                                                for n in [0,1]:
                                                    tc = Starts[n]
                                                    tcstep = Steps[n]
                                                    t = ta + tb + tc
                                                    while 1:
                                                        qyc = qyfactor1*tc - qyfactor2*sc
                                                        qycSqrd = pow(qyc, 2.0)
                                                        qySqrd = pow(qya + qyb + qyc, 2.0)
                                                        Coeff6 = (pow(qxcSqrd, 2.0) + pow(qycSqrd, 2.0)) * exp(-LBSqrd*(qycSqrd)/2.0)*Coeff5
                                                        #FullCoeff = Coeff * (pow(qxa, 2.0) + pow(qya, 2.0))*(pow(qxb, 4.0) + pow(qyb, 4.0))*(pow(qxc, 6.0) + pow(qyc, 6.0))* exp(-LBSqrd*(qxaSqrd + qxbSqrd + qxcSqrd + qyaSqrd + qybSqrd + qycSqrd + qxSqrd + qySqrd )/4.0)
                                                        #FullCoeff = Coeff * Coeff6 * exp(-LBSqrd*(qxa*qxb + qxa*qxc + qxb*qxc + qya*qyb + qya*qyc + qyb*qyc)/2.0)
                                                        FullCoeff = Coeff * exp(-LBSqrd*(qxaSqrd + qxbSqrd + qxcSqrd + qyaSqrd + qybSqrd + qycSqrd + qxSqrd + qySqrd )/4.0)
                                                        #FullCoeff = Coeff * Coeff1 * Coeff2 * Coeff3 * Coeff4 * Coeff5 * Coeff6 * exp(-LBSqrd*(qxa*qxb + qxa*qxc + qxb*qxc + qya*qyb + qya*qyc + qyb*qyc)/2.0)
                                                        V = (pow(qxa, 6.0) + pow(qya, 6.0)) * (pow(qxb, 4.0) + pow(qyb, 4.0)) * (pow(qxc, 2.0) + pow(qyc, 2.0))
                                                        Phase = PhaseCoeff*(ta*PhaseFactor1 + tb*PhaseFactor2 + tc*PhaseFactor3) + (2.0*PhaseCoeff)*(ta*j1mj2d + tb*j1mj3d + tc*j1mj4d)
                                                        BReal += V * FullCoeff * cos(Phase)
                                                        BImag += V * FullCoeff * sin(Phase)
                                                        #print str(ta) + ', ' + str(tb) + ', ' + str(tc) + ': phase=' + str(Phase) + ', sin=' + str(sin(Phase)) + ', coeff=' + str(FullCoeff)
                                                        AReal += V * FullCoeff * cos(Phase)
                                                        AImag += V * FullCoeff * sin(Phase)
                                                        #if Coeff6 == 0.0 or fabs(Coeff6) < Threshold:
                                                        if tc > tlimit or tc < -tlimit:
                                                            #print str(ta) + ", " + str(tb) + ", " + str(tc)
                                                            break
                                                        tc = tc + tcstep
                                                #if Coeff5 == 0.0 or fabs(Coeff5) < Threshold:
                                                if tb > tlimit or tb < -tlimit:
                                                    break
                                                tb = tb + tbstep
                                        #if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                        if ta > tlimit or ta < -tlimit:
                                            break
                                        ta = ta + tastep
#                                print str(sa) + ', ' + str(sb) + ',' + str(sc) + ': ' + str(BReal) + ', ' + str(BImag)
#                                print
#                                print
#                                print
                                if Coeff3 == 0.0 or fabs(Coeff3) < Threshold:
                                    break
                                scidx = scidx + scstep
                        if Coeff2 == 0.0 or fabs(Coeff2) < Threshold:
                            break
                        sbidx = sbidx + sbstep
                if Coeff1 == 0.0 or fabs(Coeff1) < Threshold:
                    break
                saidx = saidx + sastep

        return np.complex(AReal, AImag)



    def WriteCoefficients(self,filename):
        """
        Write cofficients to a csv file.
        """
        f = open(filename,"w")
        csv_writer = csv.writer(f)
        for key in  self.InteractionCoefs.keys():
            Cols = list()
            Cols.append(key)
            Cols.append(self.InteractionCoefs[key].real)
            Cols.append(self.InteractionCoefs[key].imag)
            #Cols.append([key[4]-key[1],key[2]-key[3],key[1]-key[0],key[0]-key[2]])
            #Cols.append([(key[4]-key[1]) % self.Ns, (key[2]-key[3]) % self.Ns, (key[1]-key[0]) % self.Ns, (key[0]-key[2]) % self.Ns])
            csv_writer.writerow(Cols)
        f.close()


    def WriteAntiSymCoefficients(self,filename):
        """
        Write anti-symmeterised cofficients to a csv file.
        """
        f = open(filename,"w")
        csv_writer = csv.writer(f)
        for i in np.arange(0,self.NumPairs):
            for j in np.arange(0,self.NonZeros[i]):
                Cols = list()
                Cols.extend(self.J1J2J3J4Vals[i][j,:])
                Cols.extend(self.J5J6J7J8Vals[i,0:4])
                Cols.extend([self.SymCoefs[i][j].real, self.SymCoefs[i][j].imag])
                csv_writer.writerow(Cols)
        f.close()



    def PrintDetails(self):
        """
        Print some details about the torus
        """
        print "--------------------"
        print "L1: " + str(self.L1)
        print "L2: " + str(self.L2)
        print "SinTheta: " + str(self.SinTheta)
        print "MagneticLength: " + str(self.MagneticLength)


    def FourBodyDeltaBosonicTesting(self, j1mj2, j1mj3, j1mj4, j2mj7, j3mj6, j4mj5):
        """
        Calculates the four body delta interaction coefficients A_j1...j8 from differences provided.

        Consists of iterations in six directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.
        """
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = 1.0/(2.0 * (self.L1*self.L2*self.SinTheta)**2 * self.MagneticLength**4)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = self.MagneticLength**2
        cdef double saidx = 0.0
        cdef double sbidx = 0.0
        cdef double scidx = 0.0
        cdef double s, t, sa, sastep, ta, tastep, sb, sbstep, tb, tbstep, sc, scstep, tc, tcstep, qxaSqrd, qxbSqrd, qxcSqrd, qyaSqrd, qybSqrd, qycSqrd, qxSqrd, qySqrd
        cdef double Coeff1, Coeff2, Coeff3, Coeff4, Coeff5, Coeff6, LLFactor, FullCoeff
        cdef double qxa, qxb, qxc, qya, qyb, qyc, Threshold, qxfactor, qyfactor1, qyfactor2, Phase
        cdef int Ns = self.Ns
        cdef double Nsd = <double>self.Ns
        cdef double PhaseFactor3 = np.pi/Nsd
        cdef double sabase = (j2mj7 + Nsd) % Nsd
        cdef double sbbase = (j3mj6 + Nsd) % Nsd
        cdef double scbase = (j4mj5 + Nsd) % Nsd
        cdef double j1mj2d = j1mj2, j1mj3d = j1mj3, j1mj4d = j1mj4
        Threshold = self.Threshold
        Threshold = 1e-10
        qxfactor = TwoPi/self.L1 # factor that multiply s's by to get qx
        qyfactor1 = TwoPi/(self.L2*self.SinTheta) # factor that multiply t's by to get qy
        qyfactor2 = TwoPi*self.CosTheta/(self.L1*self.SinTheta) # factor that multiply s's by to get qy

        Starts = [0.0, -1.0]
        Steps = [1.0, -1.0]
        for i in [0,1]:
            saidx = Starts[i]
            sastep = Steps[i]
            while 1:
                sa = (sabase + saidx*Nsd)
                qxa = qxfactor*sa
                qxaSqrd = pow(qxa, 2.0)
                Coeff1 = exp(-LBSqrd*qxaSqrd/2.0)
                for j in [0,1]:
                    sbidx = Starts[j]
                    sbstep = Steps[j]
                    while 1:
                        sb = (sbbase + sbidx*Nsd)
                        qxb = qxfactor*sb
                        qxbSqrd = pow(qxb, 2.0)
                        Coeff2 = exp(-LBSqrd*qxbSqrd/2.0)
                        for k in [0,1]:
                            scidx = Starts[k]
                            scstep = Steps[k]
                            while 1:
                                sc = (scbase + scidx*Nsd)
                                s = sa + sb + sc
                                qxc = qxfactor*sc
                                qxcSqrd = pow(qxc, 2.0)
                                Coeff3 = exp(-LBSqrd*qxcSqrd/2.0)
                                qxSqrd = pow(qxa + qxb + qxc, 2.0)
                                for l in [0,1]:
                                    ta = Starts[l]
                                    tastep = Steps[l]
                                    while 1:
                                        qya = qyfactor1*ta - qyfactor2*sa
                                        qyaSqrd = pow(qya, 2.0)
                                        Coeff4 = exp(-LBSqrd*qyaSqrd/2.0)
                                        for m in [0,1]:
                                            tb = Starts[m]
                                            tbstep = Steps[m]
                                            while 1:
                                                qyb = qyfactor1*tb - qyfactor2*sb
                                                qybSqrd = pow(qyb, 2.0)
                                                Coeff5 = exp(-LBSqrd*(qybSqrd)/2.0)
                                                for n in [0,1]:
                                                    tc = Starts[n]
                                                    tcstep = Steps[n]
                                                    t = ta + tb + tc
                                                    while 1:
                                                        qyc = qyfactor1*tc - qyfactor2*sc
                                                        qycSqrd = pow(qyc, 2.0)
                                                        qySqrd = pow(qya + qyb + qyc, 2.0)
                                                        Coeff6 = exp(-LBSqrd*(qycSqrd)/2.0)
                                                        #FullCoeff = Coeff * (pow(qxa, 6.0) + pow(qya, 6.0))*(pow(qxb, 4.0) + pow(qyb, 4.0))*(pow(qxc, 2.0) + pow(qyc, 2.0))* exp(-LBSqrd*(qxaSqrd + qxbSqrd + qxcSqrd + qyaSqrd + qybSqrd + qycSqrd + qxSqrd + qySqrd )/4.0)
                                                        FullCoeff = Coeff * Coeff1 * Coeff2 * Coeff3 * Coeff4 * Coeff5 * Coeff6 * exp(-LBSqrd*(qxa*qxb + qxa*qxc + qxb*qxc + qya*qyb + qya*qyc + qyb*qyc)/2.0)
                                                        Phase = PhaseFactor3*(ta*(s + sa) + tb*(s + sb) + tc*(s + sc)) + (2.0*PhaseFactor3)*(ta*j1mj2d + tb*j1mj3d + tc*j1mj4d)
                                                        AReal += FullCoeff * cos(Phase)
                                                        AImag += FullCoeff * sin(Phase)
                                                        if Coeff6 == 0.0 or fabs(Coeff6) < Threshold:
                                                            break
                                                        tc = tc + tcstep
                                                if Coeff5 == 0.0 or fabs(Coeff5) < Threshold:
                                                    break
                                                tb = tb + tbstep
                                        if Coeff4 == 0.0 or fabs(Coeff4) < Threshold:
                                            break
                                        ta = ta + tastep
                                if Coeff3 == 0.0 or fabs(Coeff3) < Threshold:
                                    break
                                scidx = scidx + scstep
                        if Coeff2 == 0.0 or fabs(Coeff2) < Threshold:
                            break
                        sbidx = sbidx + sbstep
                if Coeff1 == 0.0 or fabs(Coeff1) < Threshold:
                    #print "SA: Last: " + str(saidx) + ", val: " + str(Coeff1)
                    break
                saidx = saidx + sastep

        return np.complex(AReal, AImag)




#    def CoefficientCount(self):
#        """
#        Return the number of (non anti-symmeterised) coefficients
#        """
#        return self.NumberCoefs
#


def AntiSymmetrise(a):
    """
    Given a list of values, return the list of all combinations along with antisymmetric tensor values.
    """
    combinations = list()
    phases = list()
    if a.__len__() == 1:
        combinations.append([a[0]])
        phases.append(1)
    else:
        for i in np.arange(0, a.__len__()):
            # permute first element with each of the others
            tmp = a[0]; a[0] = a[i]; a[i] = tmp
            [subcombinations, subphases] = AntiSymmetrise(a[1:]) # get all possible combinations with phases of remaining elements.
            for comb in subcombinations:
                comb.insert(0,a[0])
                combinations.append(comb)
            if i == 0:
                phases.extend(subphases)
            else:
                phases.extend(map(lambda x: x * -1, subphases))
            tmp = a[0]; a[0] = a[i]; a[i] = tmp

    return [combinations, phases]


#cpdef Symmetrise(np.ndarray[np.int32_t, ndim=1] a, int pos):
def Symmetrise(a):
    """
    Given a list of values, return the list of all combinations along with symmetric tensor values.
    """
    combinations = list()
    phases = list()
    if a.__len__() == 1:
        combinations.append([a[0]])
        phases.append(1)
    else:
        for i in np.arange(0, a.__len__()):
            # permute first element with each of the others
            tmp = a[0]; a[0] = a[i]; a[i] = tmp
            [subcombinations, subphases] = Symmetrise(a[1:]) # get all possible combinations with phases of remaining elements.
            for comb in subcombinations:
                comb.insert(0,a[0])
                combinations.append(comb)
            if i == 0:
                phases.extend(subphases)
            else:
                phases.extend(map(lambda x: x * 1, subphases))
            tmp = a[0]; a[0] = a[i]; a[i] = tmp

    return [combinations, phases]


cpdef np.ndarray[np.int32_t, ndim=1] NextPermutation(np.ndarray[np.int32_t, ndim=1] A, int n):
    """
    Given an array of values, return the next permutation up until the array is descending in value.
    Algorithm works as follows:
    1. Start from the last item n. If this is less than the previous item n-1, proceed to n-1 and repeat this step.
    2. If item n is greater than item n-1. Swap n and n-1 and return new array.
    """
    cdef int tmp, p, q

    p = n-1
    while p >= 0 and A[p+1] <= A[p]:
        p -= 1

    if p < 0:
        A[0] = -1
    else:
        q = n
        while q > p and A[q] <= A[p]:
            q -= 1

        tmp = A[q]
        A[q] = A[p]
        A[p] = tmp

        p = p+1
        while p < n:
            tmp = A[n]
            A[n] = A[p]
            A[p] = tmp
            p += 1
            n -= 1

    return A


cpdef double CombinationNorm(np.ndarray[np.int32_t, ndim=1]A , int n):
    cdef double Norm = 1.0, NormTmp = 1.0
    cdef int i = 1
    cdef double Inc = 1.0

    while i < n:
        if A[i] == A[i-1]:
            Inc += 1.0
            NormTmp *= Inc
        else:
            Norm *= NormTmp
            NormTmp = 1.0
            Inc = 1.0
        i += 1

    return Norm * NormTmp
