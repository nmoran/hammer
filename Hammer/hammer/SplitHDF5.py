#! /usr/bin/env python

""" SplitHDF5.py: Script to split hdf5 files. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import sys
import argparse
import h5py
import numpy as np


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to split hdf5 files into a number of smaller files.')
    Parser.add_argument('input_file', metavar='vec', type=str, nargs=1, help='The file to split.')
    Parser.add_argument('--verbose','-v', action='store_true', help='Flag to give verbose output, detailing the dimensions of dataset in each file.')
    Parser.add_argument('--output-prefix','-o', type=str, default='split', help='The prefix to use for output files.')
    Parser.add_argument('--axis', type=int, default=0, help='The axis on which to split datasets (default=0).')
    Parser.add_argument('--number', '-n', type=int, default=2, help='The number of files to split file into.')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    InputFile = Args.input_file
    OutputPrefix = Args.output_prefix
    Axis = Args.axis
    NumberFiles = Args.number

    if Verbose:
        print(("Splitting " + str(len(InputFile)) + ' on axis ' + str(Axis) + ' to files ' + str(OutputPrefix + '*') + '.'))

    if len(InputFile) == 0:
        print("Must enter name of file to split.")
        return

    # count datasets and dimensions
    TotDims = dict()
    DataTypes = dict()
    try:
        f = h5py.File(InputFile[0], 'r')
        for key in list(f.keys()):
            if key in TotDims:
                if len(TotDims[key]) != len(f[key].shape) or DataTypes[key] != f[key].dtype: # ensure datasets have the same dimension
                    print(('Dataset ' + key + ' must have the same nubmer of dimensions and datatype in each file.'))
                    return
                else:
                    NDims = len(TotDims[key])
                    OtherDims = list(range(NDims))
                    OtherDims.remove(Axis); OtherDims = tuple(OtherDims)
                    if not (TotDims[key][OtherDims] == np.asarray(f[key].shape)[OtherDims]).all():
                        print(('For dataset ' + key + ' other dimensions must be the same to combine.'))
                    else:
                        TotDims[key][Axis] += f[key].shape[Axis]
            else:
                TotDims[key] = np.asarray(f[key].shape)
                DataTypes[key] = f[key].dtype
        f.close()
    except IOError as ioerror:
        print(("Problems reading " + InputFile + ": " + str(ioerror)))
        return

    SplitDims = dict() # dictionary of lists containing the number of dimensions of dataset in each file.
    for key in list(TotDims.keys()):
        SplitDims[key] = TotDims[key][Axis] / NumberFiles * np.ones(NumberFiles, dtype=np.int64)
        SplitDims[key][-1] += TotDims[key][Axis] - (SplitDims[key][0] * NumberFiles) # add remainder to last file.

    Indices = dict()
    for FileIdx in range(NumberFiles):
        Filename = OutputPrefix + '_p_' + str(FileIdx) + '.hdf5'
        try:
            of = h5py.File(Filename, 'w')
            f = h5py.File(InputFile[0], 'r')
            for key in list(TotDims.keys()):
                tmpshape = TotDims[key].copy()
                tmpshape[Axis] = SplitDims[key][FileIdx]
                out_dset = of.create_dataset(key, tmpshape, dtype=DataTypes[key])
                in_dset = f[key]
                if key not in list(Indices.keys()):
                    Idx = [slice(None)] * len(TotDims[key])
                    Idx[Axis] = slice(0, SplitDims[key][FileIdx])
                    Indices[key] = Idx
                else:
                    Start = Indices[key][Axis].stop
                    Indices[key][Axis] = slice(Start, Start + SplitDims[key][FileIdx])
                out_dset[:] = in_dset[tuple(Indices[key])]
            of.close()
            f.close()
        except IOError as ioerror:
            print(("Problem writing to file " + OutputPrefix + '_p_' + str(FileIdx) + '.hdf5, error: ' + str(ioerror)))
            return



if __name__ == '__main__' :
    __main__()
