#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True
"""
 This is a cython implementation of code to work with the FQH basis on a Torus, implements COMMomentum and inversion symmetries

"""
import numpy as np
cimport numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
from petsc4py import PETSc
from hammer.FQHTorusBasisCOMMomentum cimport FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasis cimport FQHTorusBasis


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)
    double ceil(double x)
    double floor(double x)

cdef extern from "complex.h":
    double complex cexp(double complex x) nogil
    double cabs(double complex x) nogil
    double creal(double complex x) nogil
    double cimag(double complex x) nogil
    double complex conj(double complex x) nogil


cdef class FQHTorusBasisCOMMomentumInversion(FQHTorusBasisCOMMomentum):
    cdef int InversionSector
    cdef int InversionSectors
    cdef int NumTranslations
    cdef long OverflowMask1

    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.Distance = 0
        self.COMMomentum = -1
        self.Sectors = 0
        self.InversionSector = -1
        self.InversionSectors = 0
        self.NumTranslations = 0


    def __init__(self, int Particles, int NbrFlux, int Momentum, int COMMomentum, int InversionSector):
        """
        Set the variables. Inputs are:
        Particles: Number of particles in the basis.
        NbrFlux: Number of flux quanta to consider.
        Momentum: The total momentum, sum of the orbital angular momentum of each particle.
        COMMomentum: The center of mass momentum. If -1 then do not use this symmetry.
        InversionSector: The sector for the inversion, translate symmetry 0 to 1.
        """
        super().__init__(Particles, NbrFlux, Momentum, COMMomentum)
        self.InversionSector = InversionSector

        def FindSectorsAndTranslations(Particles, NbrFlux, Momentum):
            k = 0.0
            InversionSectors = 0
            NumTranslations = 0
            while k < 100.0:
                if ((2.0 * np.float(Momentum) + k*np.float(NbrFlux))/np.float(Particles)) % 1.0 == 0.0:
                    NumTranslations = ((2.0 * np.float(Momentum) + k*np.float(NbrFlux))/np.float(Particles)) + 1
                    InversionSectors = 2
                    break
                k += 1.0
            return [NumTranslations, InversionSectors]

        [NumTranslations1, InversionSectors1] = FindSectorsAndTranslations(Particles, NbrFlux, Momentum)
        [NumTranslations2, InversionSectors2] = FindSectorsAndTranslations(Particles, NbrFlux, COMMomentum)

        # if no multiples found then only a single sector and restrict to lowest one.
        if (InversionSectors1 == 0)  or (InversionSectors2 == 0):
            self.InversionSectors = 0
            self.InversionSector = 0
        else:
            self.InversionSectors = 2
            self.NumTranslations = NumTranslations1


        self.NumTranslations = self.NumTranslations % self.NbrFlux

        # set overflow mask to top bits that will be rotated over the boundary
        self.OverflowMask1 = ((1l << self.NumTranslations) - 1l) << (self.NbrFlux - self.NumTranslations)

        #print 'NumTranslations: ' + str(self.NumTranslations)
        #print 'Overflow mask 1: ' + bin(self.OverflowMask1)
        #print 'Sectors: ' + str(self.InversionSectors)
        #print 'Current sectors: ' + str(self.InversionSectors)


    def FindBasisDimension(self, int J, int Nphi, int Ne):
        """
        Calculates the basis dimension for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasisDimension to calculate the dimension of the basis.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 - i)

        cdef int MaxTotMomentum = Nphi * (MaxMomentum / Nphi) + J

        return self.FindSubBasisDimensionCOMMomentumInversion(MaxTotMomentum,Nphi,Nphi,Ne,idx,0l)


    def FindBasis(self, int J, int Nphi, int Ne):
        """
        Calculates the basis elements for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasis to calculate the basis elements and populate the BasisElements array.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 - i)

        cdef int MaxTotMomentum = Nphi * (MaxMomentum / Nphi) + J

        return  self.FindSubBasis(MaxTotMomentum,Nphi,Nphi,Ne,idx,0l)


    cdef int FindSubBasisDimensionCOMMomentumInversion(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis dimension for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0) and self.IsBothRepresentativeConfiguration(base):  # this is used instead of the standard modulus operator
                return 1
            else:
                return 0
        elif Ne > Nphi:
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasisDimensionCOMMomentumInversion(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))

            idx += offset
            offset += self.FindSubBasisDimensionCOMMomentumInversion(J, NphiTot, Nphi-1, Ne, idx, base)
        return offset


    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis elements for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0) and self.IsBothRepresentativeConfiguration(base):  # this is used instead of the standard modulus operator
                self.BasisElements[idx] = base
                return 1
            else:
                return 0
        elif Ne > Nphi:
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasis(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))

            idx += offset
            offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne,idx, base)
        return offset


    cdef int IsBothRepresentativeConfiguration(self, long State):
        """
        Method to check if the configuration passed is a representative configuration in both the COMMomentum sector and in the Inversion sector
        """
        cdef long TranslatedState
        cdef long InvertedState
        cdef double complex Normal = 0.0
        cdef double complex InvertNormal = 0.0
        cdef int i, j

        j = 0
        while j < self.InversionSectors:
            InvertedState = State
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                InvertedState = self.SimpleCOMInversion(State)
                InvertNormal *= self.COMInversionPhase(State)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)

                if TranslatedState < State:
                    return 0
                if TranslatedState == State:
                    Normal += self.Phases[i] * self.TranslatePhase(InvertedState, i) * InvertNormal
                i += 1
            j += 1

        if cabs(Normal) > 1e-10:
            return 1
        else:
            return 0


    cdef inline long SimpleTranslateAfterInvert(self, long State):
        """
        Function that translates the given configuration by t orbitals.
        """
        # translate everything to the left, then translate the overflow portion back to the right, and lastly take a mask of everthing.
        return  (((State << self.NumTranslations) + ((State & self.OverflowMask1) >> (self.NbrFlux - self.NumTranslations))) & self.Mask)


    cdef inline double TranslateAfterInvertPhase(self, long State):
        """
        Function that gives the phase due to parity of particles in the overflow section.
        """

        if (self.Particles % 2) == 1: # if the number of particles is odd then always return a phase of 1
            return 1.0

        # this gets the partity of the section in question. From bithacks page https://graphics.stanford.edu/~seander/bithacks.html#ParityMultiply
        cdef unsigned long v = <unsigned long>(State & self.OverflowMask1)
        v ^= v >> 1;
        v ^= v >> 2;
        v = (v & 0x1111111111111111UL) * 0x1111111111111111UL;

        if ((v >> 60) & 1):
            return -1.0
        else:
            return 1.0



    cpdef long SimpleCOMInversion(self, long State):
        """
        Function that reflects positions of particles in the provided state around the COM point and retruns the resulting state.
        """
        cdef int TranslationAmount # number of translations to make to representative configuration.
        cdef long TranslatedState, MinState, OriginalState
        cdef int i

        OriginalState = State

        # Translate until minimum is found
        i = 1
        MinState = State
        TranslationAmount = 0
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(State, i)
            if TranslatedState < MinState:
                MinState = TranslatedState
                TranslationAmount = i
            i += 1


        State = self.InvertBits(MinState)
        #State = self.SimpleTranslateArbitrary(State, self.NumTranslations)
        State = self.SimpleTranslateAfterInvert(State)

        # Translate until minimum is found
        MinState = State

        TranslatedState = self.SimpleTranslate(MinState, (self.Sectors - TranslationAmount) % self.Sectors)

        return TranslatedState


    cpdef double complex COMInversionPhase(self, long State):
        """
        Function that reflects positions of particles in the provided state around the COM point and retruns the resulting phase.
        """
        cdef int TranslationAmount
        cdef double complex Phase1 = 1.0, Phase2 = 1.0
        cdef long TranslatedState, MinState, OriginalState
        cdef int i

        OriginalState = State

        # Translate until minimum is found
        MinState = State
        TranslationAmount = 0
        i = 1
        while i < self.Sectors:
            TranslatedState = self.SimpleTranslate(State, i)
            if TranslatedState < MinState:
                MinState = TranslatedState
                Phase1 = self.Phases[i] * self.TranslatePhase(State, i)
                TranslationAmount = i
            i += 1


        State = self.InvertBits(MinState)
        #Phase1 *= (-1)**(self.Particles - 1)
        #Phase1 *= self.TranslateArbitraryPhase(State, self.NumTranslations)
        #State = self.SimpleTranslateArbitrary(State, self.NumTranslations)
        Phase1 *= self.TranslateAfterInvertPhase(State)
        State = self.SimpleTranslateAfterInvert(State)

        # Translate until minimum is found
        MinState = State

        #Translate back
        TranslatedState = self.SimpleTranslate(MinState, (self.Sectors - TranslationAmount) % self.Sectors)
        Phase2 *= self.Phases[(self.Sectors - TranslationAmount) % self.Sectors] * self.TranslatePhase(MinState, (self.Sectors - TranslationAmount) % self.Sectors)

        return Phase1 * Phase2


    cpdef double GetNormalIdx(self, Idx):
        """
        Given the index of a basis element, calculate the normal
        """
        cdef long TranslatedState
        cdef long InvertedState
        cdef long State
        cdef double complex Normal = 0.0
        cdef double complex InvertNormal = 1.0
        cdef int i, j

        State = self.BasisElements[Idx]

        j = 0
        while j < self.InversionSectors:
            InvertedState = State
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                #InvertedState = self.SimpleInvertAndTranslate(self.BasisElements[Idx])
                #InvertNormal *= self.InvertAndTranslatePhase(self.BasisElements[Idx])
                InvertedState = self.SimpleCOMInversion(State)
                InvertNormal *= self.COMInversionPhase(State)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState == self.BasisElements[Idx]:
                    Normal += self.Phases[i] * self.TranslatePhase(InvertedState, i) * InvertNormal
                i += 1
            j += 1

        return sqrt(creal(Normal))


    cdef double GetNormal(self):
        """
        Get the normal of the configuration stored in self.TmpRepresentative
        """
        cdef long TranslatedState
        cdef long InvertedState
        cdef double complex Normal = 0.0
        cdef double complex InvertNormal = 1.0
        cdef int i, j

        j = 0
        while j < self.InversionSectors:
            InvertedState = self.TmpRepresentative
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                InvertedState = self.SimpleCOMInversion(self.TmpRepresentative)
                InvertNormal *= self.COMInversionPhase(self.TmpRepresentative)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState == self.TmpRepresentative:
                    Normal += self.Phases[i] * self.TranslatePhase(InvertedState, i) * InvertNormal
                i += 1
            j += 1


        return sqrt(creal(Normal))


    cpdef long GetRepresentative(self):
        """
        Try to find the index of the representative of element TmpState2
        """
        cdef long RepState = self.TmpState2
        cdef long TranslatedState
        cdef long InvertedState
        cdef int i, j

        j = 0
        while j < self.InversionSectors:
            InvertedState = self.TmpState2
            if j > 0:
                InvertedState = self.SimpleCOMInversion(self.TmpState2)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState < RepState:
                    RepState = TranslatedState
                i += 1
            j += 1

        self.TmpRepresentative = RepState

        return RepState


    cpdef double complex GetRepresentativePhase(self):
        """
        Try to find phase relating the configuration in tmpstate2 and its representative configuration.
        """
        cdef double complex Phase = 0.0
        cdef double complex TranslatePhase = 1.0
        cdef double complex InvertNormal = 1.0
        cdef long TranslatedState
        cdef long InvertedState
        cdef long MinTranslatedState
        cdef int i, j

        # Start frum representative and get relative phase back to TmpState2
        State = self.TmpRepresentative

        j = 0
        while j < self.InversionSectors:
            InvertedState = State
            InvertNormal = (-1.0)**(self.InversionSector * j)
            if j > 0:
                InvertedState = self.SimpleCOMInversion(State)
                InvertNormal *= self.COMInversionPhase(State)
            i = 0
            while i < self.Sectors:
                TranslatedState = self.SimpleTranslate(InvertedState, i)
                if TranslatedState == self.TmpState2:
                    Phase += self.Phases[i] * self.TranslatePhase(InvertedState, i) * InvertNormal
                i += 1
            j += 1

        return conj(Phase)


    cpdef CreateConversionToFullOp(self, FullBasis):
        """
        This method creates the operator that will convert a state in this basis to the full basis given.

        Parameters
        -----------
        FullBasis: basis object
            Basis object of full basis.

        Returns
        -------
        matrix
            If ReturnOp is true the projection operator is returned.
        """
        cdef int BasisDim = self.Dim
        cdef int FullBasisDim = FullBasis.GetDimension()
        cdef int rstart, rend, Idx

        # create state to get typical ownership range.
        State = PETSc.Vec()
        State.create()
        State.setSizes(BasisDim)
        State.setUp()
        rstart, rend = State.getOwnershipRange()
        State.destroy()

        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] dnnz = np.ones(LocalDim, dtype=np.int32) * self.Sectors * self.InversionSectors
        cdef np.ndarray[np.int32_t, ndim=1] odnnz = np.ones(LocalDim, dtype=np.int32) * self.Sectors * self.InversionSectors

        if (self.Sectors * self.InversionSectors) > (BasisDim-1):
            Op = PETSc.Mat(); Op.createDense([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            Op.setUp()
        else:

            Op = PETSc.Mat(); Op.create()
            Op.setSizes([(LocalDim, BasisDim), FullBasisDim])
            Op.setFromOptions()
            Op.setPreallocationNNZ((dnnz, odnnz)) # one term per sector
            Op.setUp()


        cdef int j, k, i = 0
        cdef double complex Phase
        cdef double Normal
        cdef long TranslatedState, InverstedState, Element
        cdef double OverallNorm = np.sqrt(self.Sectors * self.InversionSectors)
        cdef double complex InvertNormal

        i = rstart
        while i < rend:
            Element = self.GetElement(i)
            k = 0
            while k < self.InversionSectors:
                InvertedState = Element
                InvertNormal = (-1.0)**(self.InversionSector * k)
                if k > 0:
                    InvertedState = self.SimpleCOMInversion(Element)
                    InvertNormal *= self.COMInversionPhase(Element)
                j = 0
                while j < self.Sectors:
                    TranslatedState = self.SimpleTranslate(InvertedState, j)
                    Phase = self.TranslatePhase(InvertedState, j) * self.Phases[j] * InvertNormal
                    Normal = self.GetNormalIdx(i)
                    Idx = FullBasis.BinaryArraySearchDescendingState(TranslatedState)
                    if Idx >= 0 and Idx < FullBasisDim:
                        Op.setValue(i, Idx, Phase/(Normal * OverallNorm), True)
                    j += 1
                k += 1
            i += 1
        Op.assemble()

        return Op


#    cpdef ConvertToFullBasis(self, State, FullBasis=None, ReturnOp=False):
#        """
#        This method expands the state in the reduced centre of mass momentum basis to the full basis.
#
#        Parameters
#        ----------
#        State: vector
#            The Fock coefficients for the state to convert.
#        FullBasis: basis object
#            A basis object for the full basis (if None one will be created).
#        ReturnOp: bool
#            Flag to indicate that the operator that performs the projection should be returned also.
#
#        Returns
#        --------
#        basis object
#            Basis object for full basis.
#        vector
#            Vector of Fock coefficients for full vector.
#        """
#        if FullBasis is None:
#            FullBasis = FQHTorusBasis(self.Particles, self.NbrFlux, self.Momentum)
#            FullBasis.GenerateBasis()
#
#        cdef int FullBasisDim = FullBasis.GetDimension()
#
#        NewState = PETSc.Vec()
#        NewState.create()
#        NewState.setSizes(FullBasisDim)
#        NewState.setUp()
#
#        Op.self.CreateConversionToFullOp(FullBasis)
#
#        Op.multTranspose(State, NewState)
#
#        if ReturnOp:
#            return [FullBasis, NewState, Op]
#        else:
#            Op.destroy()
#            return [FullBasis, NewState]


    def GetBasisDetails(self):
        """
        Returns a dictionary object with parameters of the basis.
        """
        Details = super().GetBasisDetails()
        Details['InversionSector'] = self.InversionSector
        return Details


    cpdef ConvertToPHConjugate(self, State, PHBasis=None, ReturnOp=False):
        """
        This method converts the state given into the particle hole conjugate state.

        Parameters
        ------------
        State: vec
            Vector object of state to convert.
        PHBasis: basis
            Basis object of PH conjugate or None if not given (default=None).
        ReturnOp: bool
            Flag to indicate whether to return the operator that transforms a state to
            its particle hole conjugate (default=False).

        Returns
        ----------
        basis
            Basis object of PH conjugate state.
        vec
            Vector containing coefficients of PH conjugate state.
        mat
            The operator that transforms the state, only returned in ReturnOp is True.
        """
        # calculate momentum of PH conjugate by subtracting current momentum from fully filled momentum
        PHMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.Momentum) % self.NbrFlux
        PHCOMMomentum = ((self.NbrFlux * (self.NbrFlux-1))/2 - self.COMMomentum) % self.Sectors
        PHParticles = self.NbrFlux - self.Particles

        if PHBasis is None:
            PHBasis = FQHTorusBasisCOMMomentumInversion(PHParticles, self.NbrFlux, PHMomentum, PHCOMMomentum, self.InversionSector)
            PHBasis.GenerateBasis()

        cdef int BasisDim = self.Dim
        cdef int rstart, rend, Idx
        rstart, rend = State.getOwnershipRange()
        cdef int LocalDim = rend - rstart
        cdef np.ndarray[np.int32_t, ndim=1] nnz = np.ones(LocalDim, dtype=np.int32)

        Op = PETSc.Mat(); Op.create()
        Op.setSizes([(LocalDim, BasisDim), BasisDim])
        Op.setFromOptions()
        Op.setPreallocationNNZ((nnz, nnz)) # one term per sector
        Op.setUp()

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(BasisDim)
        NewState.setUp()

        cdef long Element, Representative
        cdef double complex phase
        cdef double Normal
        cdef double OverallNorm = np.sqrt(self.Sectors * self.InversionSectors)

        cdef int i = rstart
        while i < rend:
            Element = self.GetElement(i)
            if self.CalcParityOfElement(Element) == 1:
                phase = -1.0
            else:
                phase = 1.0
            self.SetTmpState2(Element)
            self.GetRepresentative()
            Normal = self.GetNormal()
            # take inverse
            Element = (~Element) & self.Mask
            PHBasis.SetTmpState2(Element)
            Representative = PHBasis.GetRepresentative()
            phase *= PHBasis.GetRepresentativePhase()
            Idx = PHBasis.BinaryArraySearchDescendingState(Representative)
            if Idx >= 0 and Idx < BasisDim:
                Op.setValue(i, Idx, phase/(Normal*Normal), False)
            i += 1
        Op.assemble()

        Op.multTranspose(State, NewState)
        NewState.conjugate()

        if ReturnOp:
            return [PHBasis, NewState, Op]
        else:
            return [PHBasis, NewState]


