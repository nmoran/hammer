#! /usr/bin/env python

""" MC_combine_trans.py: Script to combine the samples which were generated with different translations. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import sys
import argparse
import numpy as np
import hammer.vectorutils as vectorutils
import h5py
import petsc4py.PETSc as PETSc
from hammer.FQHTorusWF import *
import csv
import sympy.mpmath as sm
import fractions

def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """

    Parser = argparse.ArgumentParser(description='Utility to combine MC weights with different translations into single WF, all in hdf5 format.')
    Parser.add_argument('--output','-o', type=str, default='wf_values.hdf5', help='The name of the output file.')
    Parser.add_argument('-Ne', type=int, default=8, help='Number of electrons')
    Parser.add_argument('-N', type=int, default=1000, help='Number of samples')
    Parser.add_argument('-R', type=float, default=0, help='Re(tau)')
    Parser.add_argument('-I', type=float, default=1, help='Im(tau)')
    Parser.add_argument('-Nphi', type=int, default=24, help='Number of flux quanta')
    Parser.add_argument('--symmetric','-s', type=bool, default=False, help='Assume WF symmetric in x,y translations.')
    Parser.add_argument('--config', type=str, default='config.dat', help='Name of the file containing filenames of files which should be combined and their translations; i.e. 1 0 file_10.hdf5\n -1 1 file_-11.hdf5\n etc')

    PetscPrint = PETSc.Sys.Print

    Args = Parser.parse_args(sys.argv[1:])
    Output = Args.output
    Sym = Args.symmetric
    ConfigFile = Args.config
    Ne = int(Args.Ne)
    N = int(Args.N)
    Nphi = int(Args.Nphi)
    tau=float(Args.R)+float(Args.I)*np.complex(0.0, 1.0)
    Lambda=get_Lambda(Ne, Nphi)

    print(("Combining WF values for Ne="+str(Ne)+" particles at flux Nphi="+str(Nphi)+" with sign parameter Lambda="+str(Lambda)+" at geometry tau="+str(tau)))

    #for m in range(-2,3):
    #    for n in range(-2,3):
    #        print np.power(-1, Lambda*Nphi*m*n)*np.complex128(np.power(lambda_coeff(Nphi, m, n, Lambda, tau), Ne/2))
    #    print "\n"
    #quit()

    wf_values=np.zeros(N, dtype=np.complex128)

    try: # open config file
        f = open(ConfigFile, 'r')
        l = 0 # number of files
        while True:
            line = f.readline() # read translation and WF file
            if (len(line)==0): # end of config file
                break
            line = line.rstrip() # remove eol
            print(("Line ", l+1, ": ", line)) # show what line contains
            gottrans=False # found the translations

            sp=line.split()
            if len(sp) != 4:
                print(("Error in configuration file"+ConfigFile+" in line "+str(l+1)+": "+line))
                return -1

            m=int(sp[0])
            n=int(sp[1])
            Na=int(sp[2])
            filename=sp[3]

            print("The input",sp)
            if (m==0):
                if (n==0):
                    print(("Error in configuration file:"))
                    print(("Translations are both = 0, so lambda is not well defined"))
                    return -1

            try:
                fhdf5 = h5py.File(filename, 'r')
            except IOError as ioerror:
                PetscPrint("Error reading hdf5 file " + filename + ', ' + str(ioerror))
                return -1

            dset = fhdf5['wf_values']
            no_values=len(dset[:,0])
            if(N != no_values):
                print(("Error: "+str(n)+" samples in file rather than "+str(N)))
                return -1

            if(l==0):
                prob=dset[:,0]+np.complex(0.0, 1.0)*dset[:,1]
            else:
                tmp=dset[:,0]+np.complex(0.0, 1.0)*dset[:,1]
                if(diff_complex_array(tmp, prob, 12)):
                    print("Error: probability values in hdf5 files not equal")
                    return -1

            wf=np.exp(dset[:,2]+np.complex(0.0, 1.0)*dset[:,3])
            coeff=lambda_coeff(Nphi, m, n, Lambda, tau)
            print(("coeff for m="+str(m)+", n="+str(n)+": "+str(coeff)))
            print(("wf_values shape: " + str(wf_values.shape) + ', wf values shape: ' + str(wf.shape)))
            wf_values=wf_values+np.power(-1, Lambda*Nphi*m*n)*np.complex128(np.power(coeff, Na))*wf

            fhdf5.close()
            l=l+1

        f.close()

    except IOError as ioError:
        print(("Problem opening " + ConfigFile + ', ' + str(ioError)))
        return -1

    try:
        of = h5py.File(Output, 'w')
    except IOError as ioerror:
        print(("Problem writing to file " + Output  + ': ' + str(ioerror)))
        return -1


    dset = of.create_dataset("wf_values", (N,4), dtype=float)

    dset[:,0] = np.real(prob[:])
    dset[:,1] = np.imag(prob[:])
    dset[:,2] = np.real(np.log(wf_values[:]))
    dset[:,3] = np.imag(np.log(wf_values[:]))

    of.close()

    return 0


def get_Lambda(Ne, Nphi):
    gcd=fractions.gcd(Ne, Nphi)
    p=Ne/gcd
    q=Nphi/gcd
    for i in range(1,1000):
        if((p*i)%q==1):
            p_inv=i
            break

    #print "p = "+str(p)+", q = "+str(q)+", p^-1 = "+str(p_inv)
    return (p*p_inv+1)%2


def diff_complex_array(array1, array2, prec): # check if two complex arrays are equal up to <prec> digits
    n1=len(array1)
    n2=len(array2)
    if(n1!=n2):
        return True
    for i in range(0,n1):
        if(diff_complex(array1[i], array2[i], prec)):
            return True
    return False

def diff_complex(float1, float2, prec): # check if two complex numbers are equal up to <prec> digits
    if np.real(float1-float2)/np.maximum(np.real(float1),np.real(float2))>np.power(10, -prec):
        return True
    if np.imag(float1-float2)/np.maximum(np.imag(float1),np.imag(float2))>np.power(10, -prec):
        return True
    return False

def lambda_coeff(Nphi, m, n, Lambda, tau): # Assuming (r,t) = (0,0) and l = 0
    #print "getting coeff with m="+str(m)+", n="+str(n)+", tau="+str(tau)+", Nphi="+str(Nphi)+", Lambda="+str(Lambda)
    eta=np.exp(np.complex(0.0, 1.0)*np.pi*tau/12.0)*sm.qp(np.exp(2.0*np.pi*np.complex(0.0, 1.0)*tau))
    z=np.pi*(m+n*tau)/Nphi
    theta=sm.jtheta(1, z, np.exp(np.complex(0.0, 1.0)*np.pi*tau))
        #print "1st factor: "+str(sign*np.sqrt(tau.imag)*eta*eta*eta)
    #print "arg of 1st exp: "+str(-np.complex(0.0, 1.0)*np.pi*tau*n*n/(Nphi*Nphi))
    #print "arg of 2nd exp: "+str(-np.complex(0.0, 1.0)*np.pi*n*m/(Nphi*Nphi))
    #print "theta: "+str(theta)
    return np.sqrt(tau.imag)*eta*eta*eta*np.exp(-np.complex(0.0, 1.0)*np.pi*tau*n*n/(Nphi*Nphi))*np.exp(-np.complex(0.0, 1.0)*np.pi*n*m/(Nphi*Nphi))/theta


if __name__ == '__main__' : # If run as executable
    __main__()
