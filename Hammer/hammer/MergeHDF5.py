#! /usr/bin/env python

""" MergeHDF5.py: Script to merge hdf5 files. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

import sys
import argparse
import h5py
import numpy as np


def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments 
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to merge a number of hdf5 files into a single file.')
    Parser.add_argument('files', metavar='vec', type=str, nargs='+', help='The list of files to merge.')
    Parser.add_argument('--verbose','-v', action='store_true', help='Flag to give verbose output, detailing the dimensions of dataset in each file.')
    Parser.add_argument('--output','-o', type=str, default='merged.hdf5', help='The name of the output file.')
    Parser.add_argument('--axis', type=int, default=0, help='The axis on which to merge datasets (default=0).')

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = Args.verbose
    Files = Args.files
    Output = Args.output
    Axis = Args.axis

    if Verbose:
        print(("Merging " + str(len(Files)) + ' files on axis ' + str(Axis) + ' to file ' + str(Output) + '.'))

    if len(Files) == 0:
        print("Must enter at least one file to merge.")
        return

    # count datasets and dimensions
    TotDims = dict()
    DataTypes = dict()
    for file in Files:
        try:
            f = h5py.File(file, 'r')
            for key in list(f.keys()):
                if key in TotDims:
                    if len(TotDims[key]) != len(f[key].shape) or DataTypes[key] != f[key].dtype: # ensure datasets have the same dimension
                        print(('Dataset ' + key + ' must have the same nubmer of dimensions and datatype in each file.'))
                        return
                    else:
                        NDims = len(TotDims[key])
                        OtherDims = list(range(NDims))
                        OtherDims.remove(Axis); OtherDims = tuple(OtherDims)
                        if not (TotDims[key][OtherDims] == np.asarray(f[key].shape)[OtherDims]).all():
                            print(('For dataset ' + key + ' other dimensions must be the same to combine.'))
                        else:
                            TotDims[key][Axis] += f[key].shape[Axis]
                else:
                    TotDims[key] = np.asarray(f[key].shape)
                    DataTypes[key] = f[key].dtype
            f.close()
        except IOError as ioerror:
            print(("Problems reading " + file + ": " + str(ioerror)))
            return

    if Verbose:
        print("Output file will contain:")
        for key in list(TotDims.keys()):
            print(("Dataset with name \"" + key + "\", datatype " + str(DataTypes[key]) + " and dimensions " + str(TotDims[key]) + "."))

    # create output file and populate
    try:
        of = h5py.File(Output, 'w')
        Indices = dict()
        for key in list(TotDims.keys()):
            # create dataset
            dset = of.create_dataset(key, TotDims[key], dtype=DataTypes[key])
            for file in Files:
                try:
                    f = h5py.File(file, 'r')
                    if key in list(f.keys()):
                        if key not in list(Indices.keys()):
                            Idx = [slice(None)] * len(TotDims[key])
                            Idx[Axis] = slice(0, f[key].shape[Axis])
                            Indices[key] = Idx
                        else:
                            Start = Indices[key][Axis].stop
                            Indices[key][Axis] = slice(Start, Start + f[key].shape[Axis])
                        dset[tuple(Indices[key])] = f[key][:]
                    f.close()
                except IOError as ioerror:
                    return
        of.close()
    except IOError as ioerror:
        print(("Problem writing to file " + file  + ': ' + str(ioerror)))
        return



if __name__ == '__main__' :
    __main__()



