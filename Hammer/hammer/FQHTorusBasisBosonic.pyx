#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusBasisBosonic.pyx: This is a cython implementation of code to work with the FQH basis on a Torus. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
from petsc4py import PETSc

cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)


cdef class FQHTorusBasisBosonic:
    """
    Class to represent basis for Bosons in FQH effect.
    TODO: Replace mention of monomials with occupation representation as there is confusion.
    """
    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.Particles = 0
        self.NbrFlux = 0
        self.Momentum = 0
        self.Dim = 0
        self.EffectiveFlux = 0
        self.EffectiveMomentum = 0
        self.FluxOffset = 0


    def __init__(self, int Particles, int NbrFlux, int Momentum, int FluxOffset=0):
        """
        Set the variables and generate the basis
        """
        self.Particles = Particles
        self.NbrFlux = NbrFlux
        self.Momentum = Momentum
        self.FluxOffset = FluxOffset
        self.EffectiveFlux = self.NbrFlux + self.Particles - 1
        self.EffectiveMomentum = (self.Momentum + (self.Particles*(self.Particles-1))/2) % (self.NbrFlux + self.FluxOffset)
        self.Monomial1 = <long*>PyMem_Malloc(self.NbrFlux * sizeof(long))
        self.Monomial2 = <long*>PyMem_Malloc(self.NbrFlux * sizeof(long))


    def GenerateBasis(self, filename='', DimensionOnly=False):
        """
        Generate basis elements, first calculate the dimension, allocate space and then
        populate the basis array.

        Parameters
        -----------
        filename: str
            Filename to read basis from if it exists. Not implmented yet for bosonic bases, but included
            for compatibity.
        DimensionOnly: bool
            Flag that indicates that the dimension should only be calculated and not the elements.

        """
        self.Dim = self.FindBasisDimension()
        if not DimensionOnly:
            self.BasisElements = <long *>PyMem_Malloc(self.Dim * sizeof(long))
            self.FindBasis()


    cdef int FindBasisDimension(self):
        """
        Calculates the basis dimension for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasisDimension to calculate the dimension of the basis.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int dim = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        cdef int i = 0
        for i in np.arange(0, self.Particles):
            MaxMomentum += (self.EffectiveFlux - 1 -i)
        i = 0
        while (self.EffectiveMomentum + i*(self.NbrFlux + self.FluxOffset)) <= MaxMomentum:
            dim += self.FindSubBasisDimension(self.EffectiveMomentum + i*(self.NbrFlux + self.FluxOffset), self.EffectiveFlux, self.Particles)
            i+=1
        return dim


    cdef int FindSubBasisDimension(self, int J, int Nphi, int Ne):
        """
        Recursive function to find the basis dimension for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int dim = 0

        if (Ne == 0):
            if (J == 0):
                return 1
            else:
                return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                dim += self.FindSubBasisDimension(J-(Nphi-1), Nphi-1, Ne-1)
            #elif J >= 0:
            #    dim += self.FindSubBasisDimension(J, Nphi-1, Ne)
            dim += self.FindSubBasisDimension(J, Nphi-1, Ne)
        return dim


    def FindBasis(self):
        """
        Calculates the basis elements for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasis to calculate the basis elements and populate the BasisElements array.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, self.Particles):
            MaxMomentum += (self.EffectiveFlux - 1 - i)

        cdef int MaxTotMomentum = (self.NbrFlux + self.FluxOffset) * (MaxMomentum / (self.NbrFlux + self.FluxOffset)) + self.EffectiveMomentum

        return self.FindSubBasis(MaxTotMomentum, self.NbrFlux + self.FluxOffset, self.EffectiveFlux, self.Particles, idx, 0l)


    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis elements for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0):  # this is used instead of the standard modulus operator
                self.BasisElements[idx] = base
                return 1
            else:
                return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasis(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))
            #elif J >= 0:
            #    idx += offset
            #    offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne, idx, base)
            idx += offset
            offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne, idx, base)
        return offset


    def __dealloc__(self):
        """
        This is the destructor which dallocates any memory that was allocated.
        """
        if self.Dim > 0:
            PyMem_Free(self.BasisElements)

        PyMem_Free(self.Monomial1)
        PyMem_Free(self.Monomial2)


    cpdef double complex NN(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply density density operator on the given sites to the configuration indexed by Idx.
        """
        self.GetMonomialRep(Idx)

        if Orbital1 == Orbital2:
            if self.Monomial1[Orbital1] > 1:
                return self.Monomial1[Orbital1] * (self.Monomial1[Orbital1] - 1)
        else:
            if self.Monomial1[Orbital1] > 0 and self.Monomial1[Orbital2] > 0:
                return self.Monomial1[Orbital1] * self.Monomial1[Orbital2]

        return 0.0


    cpdef double complex NNN(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply density density operator on the given sites to the configuration indexed by Idx.
        """
        self.GetMonomialRep(Idx)

        if Orbital1 == Orbital2 == Orbital3:
            if self.Monomial1[Orbital1] > 2:
                return self.Monomial1[Orbital1] * (self.Monomial1[Orbital2] - 1) * (self.Monomial1[Orbital3] - 2)
        elif Orbital1 == Orbital2:
            if self.Monomial1[Orbital1] > 1 and self.Monomial1[Orbital3] > 0:
                return self.Monomial1[Orbital1] * (self.Monomial1[Orbital1] - 1) * self.Monomial1[Orbital3]
        elif Orbital1 == Orbital3:
            if self.Monomial1[Orbital1] > 1 and self.Monomial1[Orbital2] > 0:
                return self.Monomial1[Orbital1] * (self.Monomial1[Orbital1] - 1) * self.Monomial1[Orbital2]
        elif Orbital2 == Orbital3:
            if self.Monomial1[Orbital2] > 1 and self.Monomial1[Orbital1] > 0:
                return self.Monomial1[Orbital2] * (self.Monomial1[Orbital2] - 1) * self.Monomial1[Orbital1]
        else:
            return self.Monomial1[Orbital1] * self.Monomial1[Orbital2] * self.Monomial1[Orbital3]

        return 0.0


    cpdef double AA(self, int Orbital1, int Orbital2, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx and store the result in TmpState.

        """
        cdef double phase = 1.0

        self.GetMonomialRep(Idx)

        if (self.Monomial1[Orbital1] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital1])
            self.Monomial1[Orbital1] -= 1
        else:
            return 0.0

        if(self.Monomial1[Orbital2] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital2])
            self.Monomial1[Orbital2] -= 1
        else:
            return 0.0

        return phase


    cpdef double complex AdAd(self, int Orbital1, int Orbital2):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state in Monomial1 and store the result in Monomail2.

        """
        cdef int i = 0
        cdef double complex phase = 0
        while i < self.NbrFlux:
            self.Monomial2[i] = self.Monomial1[i]
            i += 1
        self.Monomial2[Orbital1] += 1
        phase = sqrt(<double>self.Monomial2[Orbital1])
        self.Monomial2[Orbital2] += 1
        phase *= sqrt(<double>self.Monomial2[Orbital2])

        return phase


    cpdef double AAA(self, int Orbital1, int Orbital2, int Orbital3, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1 and Orbital2 to basis elements stored at index Idx and store the result in TmpState.

        """
        cdef double phase = 1.0

        self.GetMonomialRep(Idx)

        if (self.Monomial1[Orbital1] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital1])
            self.Monomial1[Orbital1] -= 1
        else:
            return 0.0
        if (self.Monomial1[Orbital2] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital2])
            self.Monomial1[Orbital2] -= 1
        else:
            return 0.0
        if (self.Monomial1[Orbital3] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital3])
            self.Monomial1[Orbital3] -= 1
        else:
            return 0.0

        return phase


    cpdef double complex AdAdAd(self, int Orbital1, int Orbital2, int Orbital3):
        """
        Apply creation operators on sites with momentum Orbital1 and Orbital2 to state TmpState and store result in TmpState2.

        """
        cdef int i = 0
        cdef double complex phase = 0
        while i < self.NbrFlux:
            self.Monomial2[i] = self.Monomial1[i]
            i += 1
        self.Monomial2[Orbital1] += 1
        phase = sqrt(<double>self.Monomial2[Orbital1])
        self.Monomial2[Orbital2] += 1
        phase *= sqrt(<double>self.Monomial2[Orbital2])
        self.Monomial2[Orbital3] += 1
        phase *= sqrt(<double>self.Monomial2[Orbital3])

        return phase


    cpdef double AAAA(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4, int Idx):
        """
        Apply annihilation operators on sites with momentum Orbital1, Orbital2, Orbital3 and Orbital4 to basis elements stored at index Idx and store the result in TmpState.

        """
        cdef double phase = 1.0

        self.GetMonomialRep(Idx)

        if (self.Monomial1[Orbital1] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital1])
            self.Monomial1[Orbital1] -= 1
        else:
            return 0.0
        if (self.Monomial1[Orbital2] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital2])
            self.Monomial1[Orbital2] -= 1
        else:
            return 0.0
        if (self.Monomial1[Orbital3] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital3])
            self.Monomial1[Orbital3] -= 1
        else:
            return 0.0
        if (self.Monomial1[Orbital4] > 0):
            phase *= sqrt(<double>self.Monomial1[Orbital4])
            self.Monomial1[Orbital4] -= 1
        else:
            return 0.0

        return phase


    cpdef double complex AdAdAdAd(self, int Orbital1, int Orbital2, int Orbital3, int Orbital4):
        """
        Apply creation operators on sites with momentum Orbital1, Orbital2, Orbital3 and Orbital4 to state TmpState and store result in TmpState2.

        """
        cdef int i = 0
        cdef double complex phase = 0
        while i < self.NbrFlux:
            self.Monomial2[i] = self.Monomial1[i]
            i += 1
        self.Monomial2[Orbital1] += 1
        phase = sqrt(<double>self.Monomial2[Orbital1])
        self.Monomial2[Orbital2] += 1
        phase *= sqrt(<double>self.Monomial2[Orbital2])
        self.Monomial2[Orbital3] += 1
        phase *= sqrt(<double>self.Monomial2[Orbital3])
        self.Monomial2[Orbital4] += 1
        phase *= sqrt(<double>self.Monomial2[Orbital4])

        return phase


    cpdef int BinaryArraySearchDescending(self):
        """
        Function to perform binary search on array of BasisElements for state in Monomial2
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef int l = self.Dim
        cdef long item = self.GetFermionicRep2()
        cdef int start = 0
        cdef int end = l
        cdef int dist = end - start
        cdef long test_item
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    cpdef int BinaryArraySearchDescendingElement(self, long item):
        """
        Function to perform binary search on array of BasisElements for the provided state
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef int l = self.Dim
        cdef int start = 0
        cdef int end = l
        cdef int dist = end - start
        cdef long test_item
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    def GetDimension(self):
        """
        Return the basis dimension
        """
        return self.Dim


    def GetEffectiveMomentum(self):
        """
        Return the effective momentum
        """
        return self.EffectiveMomentum


    cpdef long GetElement(self, int Idx):
        """
        Return the basis element with index Idx
        """
        return self.BasisElements[Idx]


    def PrintOccupationRep(self,Idx):
        """
        Print the basis element with index Idx in the occupation representation
        """
        print '<' + self.GetOccupationRep(Idx) + '>'


    def GetOccupationRep(self,Idx):
        """
        Return the basis element with index Idx in the occupation representation
        """
        if Idx < self.Dim:
            State = self.BasisElements[Idx]
            rep = ""
            i = 0
            count = 0
            while i < (self.EffectiveFlux + 1):
                if (1l << i & State) > 0:
                    count += 1
                else:
                    if count > 0:
                        rep += str(count) + " "
                        count = 0
                    else:
                        rep = rep + "0 "
                i += 1
            return rep
        else:
            return ""


    def GetOccupationRepOfElement(self,State):
        """
        Return the basis element with index Idx in the occupation representation
        """
        rep = ""
        i = 0
        count = 0
        while i < (self.EffectiveFlux + 1):
            if (1l << i & State) > 0:
                count += 1
            else:
                if count > 0:
                    rep += str(count) + " "
                    count = 0
                else:
                    rep = rep + "0 "
            i += 1
        return rep


    def GetOccupationRepArray(self,Idx):
        """
        Return the basis element with index Idx in the occupation representation
        """
        cdef int pos = 0
        cdef int i, count

        rep = np.zeros(self.NbrFlux, dtype=np.int)
        if Idx < self.Dim:
            State = self.BasisElements[Idx]
            i = 0
            count = 0
            while i < (self.EffectiveFlux + 1):
                if (1l << i & State) > 0:
                    count += 1
                else:
                    if count > 0:
                        rep[pos] = count
                        pos += 1
                        count = 0
                    else:
                        pos += 1
                i += 1
            return rep
        else:
            return ""


    def GetFermionicOccupationRep(self,Idx):
        """
        Return the basis element with index Idx in the occupation representation
        """
        if Idx < self.Dim:
            State = self.BasisElements[Idx]
            rep = ""
            for i in np.arange(0,self.EffectiveFlux + 1):
                if (1l << i & State) > 0:
                    rep = rep + "1 "
                else:
                    rep = rep + "0 "
            return rep
        else:
            return ""


    def GetFermionicOccupationRepOfElement(self, State):
        """
        Return the basis element with index Idx in the occupation representation
        """
        rep = ""
        for i in np.arange(0,self.EffectiveFlux + 1):
            if (1l << i & State) > 0:
                rep = rep + "1 "
            else:
                rep = rep + "0 "
        return rep


    def GetMonomialRep(self, Idx):
        """
        Convert basis element to monomial representation and store in element monomial1.
        """
        cdef long State
        cdef int count
        cdef int i, j
        if Idx < self.Dim:
            j = 0
            i = 0
            State = self.BasisElements[Idx]
            count = 0
            while i < self.EffectiveFlux + 1:
                if (1l << i & State) > 0:
                    count += 1
                else:
                    if count > 0:
                        self.Monomial1[j] = count
                        j += 1
                        count = 0
                    else:
                        self.Monomial1[j] = 0
                        j += 1
                i += 1


    def GetFermionicRep1(self):
        """
        Convert from monomial representation to fermionic basis element.
        """
        cdef long State = 0
        cdef int i = 0
        cdef int j = 0

        while i < self.NbrFlux:
            if self.Monomial1[i] > 0:
                State += ((1l << (self.Monomial1[i])) - 1l) << j
                j += self.Monomial1[i]
            j += 1
            i += 1

        return State


    def GetFermionicRep2(self):
        """
        Convert from monomial representation to fermionic basis element.
        """
        cdef long State = 0
        cdef int i = 0
        cdef int j = 0

        while i < self.NbrFlux:
            if self.Monomial2[i] > 0:
                State += ((1l << (self.Monomial2[i])) - 1l) << j
                j += self.Monomial2[i]
            j += 1
            i += 1

        return State


    def GetMonomial1(self):
        Monomial = list()
        i = 0
        while i < self.NbrFlux:
            Monomial.append(self.Monomial1[i])
            i += 1
        return Monomial


    def GetMonomial2(self):
        Monomial = list()
        i = 0
        while i < self.NbrFlux:
            Monomial.append(self.Monomial2[i])
            i += 1
        return Monomial


    def GetBasisDetails(self):
        """
        Returns a dictionary object with parameters of the basis.
        """
        Details = dict()
        Details['Particles'] = self.Particles
        Details['NbrFlux'] = self.NbrFlux
        Details['Momentum'] = self.Momentum
        Details['Bosonic'] = True
        return Details

    def SetMonomialRep2(self, OccupationRep):
        """
        Set the second stored monomial representation using the provided occupation representation.
        """
        for j in range(self.NbrFlux): # loop over each flux and add encountered particles to the monomial
            self.Monomial2[j] = OccupationRep[j]


    cpdef long GetElementSum(self, long Element1, long Element2):
        """
        Given two basis elements in binary representation, add the particles at each orbital together to get the composite elemnt.
        """
        cdef long Result = 0l # Set result to empty`
        cdef int count1, count2, pos = 0

        # Now go through both basis elements and add particles to the resulting element.
        while (Element2 > 0) or (Element1 > 0): # stop once all particles are exhausted.
            count1 = 0
            while (Element1 & (1l << count1)) > 0: count1 += 1
            count2 = 0
            while (Element2 & (1l << count2)) > 0: count2 += 1

            #print "Count1: " + str(count1)
            #print "Count2: " + str(count2)

            Result += ((1l << (count1 + count2))-1l) << pos # add particles from both elements to result at the proper flux.
            pos += count1 + count2 + 1

            # advance to next flux on each element.
            Element1 = Element1 >> (count1 + 1)
            Element2 = Element2 >> (count2 + 1)

        return Result


    cpdef double GetElementNorm(self, long Element):
        """
        Given a basis element, calculate the normal, defined as the sqrt(prod_i(factorial(m_i))), where m_i is the number of particles at orbital i.
        """
        cdef double Normal = 1.0
        cdef int count

        while Element > 0:
            count = 0
            while (Element & (1l << count)) > 0:
                count += 1
                Normal *= <double>count
            Element = Element >> (count + 1)

        return sqrt(Normal)


    cpdef OrbitalEntanglementMatrix(self, State, AOrbitalStart, AOrbitalEnd, AMomentum, AParticles):
        """
        This method, given a state in the current basis will construct and return the Schmidt matrix corresponding to bipartitioning the system into two parts, one containing
        the A orbitals specified and the specified total momentum. Only works in serial.
        """
        Details = self.GetBasisDetails()

        cdef int Momentum = Details['Momentum']
        cdef int Particles = Details['Particles']
        cdef int NbrFlux = Details['NbrFlux']
        cdef int NbrEffectiveFlux = NbrFlux + Particles # this is the effective flux in the fermionic representation of the bosonic basis configuration.

        if PETSc.COMM_WORLD.getSize() > 1:
            PETSc.Sys.Print("OrbitalEntanglementMatrix method only works in serial for now.")
            return

        # First must determine the basis over the A orbitals and the B orbitals.
        cdef int BMomentum = (Momentum - AMomentum) % NbrFlux  # the momentum over B orbitals must be equal to the total momentum minus the momentum over A orbitals.
        # the number of flux (orbitals) in each partitioin.
        cdef int AFlux = AOrbitalEnd - AOrbitalStart
        cdef int BFlux = NbrFlux  - AFlux

        cdef int BParticles = Particles - AParticles

        cdef int AEffectiveMomentum = (AMomentum - (AOrbitalStart * AParticles)) % NbrFlux
        cdef int BEffectiveMomentum = (BMomentum - (AOrbitalEnd * BParticles)) % NbrFlux

        cdef int AFluxOffset = NbrFlux - AFlux
        cdef int BFluxOffset = NbrFlux - BFlux

        ABasis = FQHTorusBasisBosonic(AParticles, AFlux, AEffectiveMomentum, FluxOffset = AFluxOffset)
        BBasis = FQHTorusBasisBosonic(BParticles, BFlux, BEffectiveMomentum, FluxOffset = BFluxOffset)
        ABasis.GenerateBasis()
        BBasis.GenerateBasis()

        ADim = ABasis.GetDimension()
        BDim = BBasis.GetDimension()

        if ADim == 0 or BDim == 0:
            return None

        #W = np.zeros((ADim, BDim), dtype=np.complex128)
        W = PETSc.Mat(); W.create()
        W.setSizes([ADim, BDim])
        W.setFromOptions()
        W.setUp()


        cdef int i = 0
        while i < self.GetDimension():
            self.GetMonomialRep(i)
            Monomial = self.GetMonomial1()
            APartition = Monomial[AOrbitalStart:AOrbitalEnd]
            BPartition = Monomial[0:AOrbitalStart]
            BPartition.extend(Monomial[AOrbitalEnd:NbrFlux])

            ABasis.SetMonomialRep2(APartition)
            BBasis.SetMonomialRep2(BPartition)

            Row = ABasis.BinaryArraySearchDescending()
            Col = BBasis.BinaryArraySearchDescending()

            if Row >= 0 and Col >= 0 and Row < ADim and Col < BDim:
                #W[Row, Col] += State.getValue(i)
                W.setValue(Row, Col, State.getValue(i), True)
            i += 1

        W.assemble()

        return W


    @staticmethod
    def OrbitalEntanglementMatrixSuperposition(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, AParticles):
        """
        This method, given a superposition of states in the current basis will construct and return the Schmidt matrix corresponding to a bipartition of the system.
        """
        Momentums = list()

        # Get particle number and flux from the first basis as should be the same for all.
        Basis = Bases[0]
        Details = Basis.GetBasisDetails()
        Momentums.append(Details['Momentum'])
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        for Basis in Bases[1:]:
            Details = Basis.GetBasisDetails()
            Momentums.append(Details['Momentum'])
            if Details['Particles'] != Particles or Details['NbrFlux'] != NbrFlux:
                PETSc.Sys.Print("Particle number and flux must be the same for all bases.")
                return

        if PETSc.COMM_WORLD.getSize() > 1:
            PETSc.Sys.Print("OrbitalEntanglementMatrix method only works in serial for now.")
            return

        # the number of flux (orbitals) in each partitioin.
        AFlux = AOrbitalEnd - AOrbitalStart
        BFlux = NbrFlux  - AFlux
        BParticles = Particles - AParticles
        AFluxOffset = NbrFlux - AFlux
        BFluxOffset = NbrFlux - BFlux


        AMomentums = range(NbrFlux)
        BMomentums = range(NbrFlux)
        #AEffectiveMomentums = map(lambda x : (x - (AOrbitalStart * AParticles)) % NbrFlux, AMomentums)
        #BEffectiveMomentums = map(lambda x : (x - (AOrbitalEnd * BParticles)) % NbrFlux, BMomentums)
        AEffectiveMomentums = AMomentums
        BEffectiveMomentums = BMomentums

        ABases = dict()
        ABasesOffsets = dict()
        AOffset = 0
        for [AEffectiveMomentum, AMomentum] in zip(AEffectiveMomentums, AMomentums):
            if not ABases.has_key(AEffectiveMomentum):
                ABasis = FQHTorusBasisBosonic(AParticles, AFlux, AEffectiveMomentum, FluxOffset = AFluxOffset)
                ABasis.GenerateBasis()
                if ABasis.GetDimension():
                    ABases[AEffectiveMomentum] = ABasis
                    ABasesOffsets[AEffectiveMomentum] = AOffset
                    AOffset += ABasis.GetDimension()

        BBases = dict()
        BBasesOffsets = dict()
        BOffset = 0
        for [BEffectiveMomentum, BMomentum] in zip(BEffectiveMomentums, BMomentums):
            if not BBases.has_key(BEffectiveMomentum):
                BBasis = FQHTorusBasisBosonic(BParticles, BFlux, BEffectiveMomentum, FluxOffset = BFluxOffset)
                BBasis.GenerateBasis()
                if BBasis.GetDimension():
                    BBases[BEffectiveMomentum] = BBasis
                    BBasesOffsets[BEffectiveMomentum] = BOffset
                    BOffset += BBasis.GetDimension()

        ADim = np.sum(map(lambda x: x.GetDimension(), ABases.values()))
        BDim = np.sum(map(lambda x: x.GetDimension(), BBases.values()))
    #
    #    print "A dim: " + str(ADim)
    #    print "B dim: " + str(BDim)

        if ADim == 0 or BDim == 0:
            return None

        # create the matrix
        #W = np.zeros((ADim, BDim), dtype=np.complex128)
        W = PETSc.Mat(); W.create()
        W.setSizes([ADim, BDim])
        W.setFromOptions()
        W.setUp()


        TotalMask = (1l << NbrFlux) - 1l  # mask which should be all ones over all orbitals
        AMask = ((1l << AFlux) - 1l) << AOrbitalStart # mask which should be ones only over the a orbitals.
        BMask = (((1l << NbrFlux) - 1l) ^ AMask) & TotalMask # inverse of A mask so should be ones over all orbitals not in A

        for [Basis, State, Coefficient, p] in zip(Bases, States, Coefficients, range(len(Bases))):
            i = 0
            while i < Basis.GetDimension():
                Basis.GetMonomialRep(i)
                Monomial = Basis.GetMonomial1()
                APartition = Monomial[AOrbitalStart:AOrbitalEnd]
                BPartition = Monomial[0:AOrbitalStart]
                BPartition.extend(Monomial[AOrbitalEnd:NbrFlux])

                AEffectiveMomentum = np.sum(map(lambda x: x*APartition[x], range(AFlux))) % NbrFlux
                BEffectiveMomentum = np.sum(map(lambda x: x*BPartition[x], range(BFlux))) % NbrFlux

                if ABases.has_key(AEffectiveMomentum):
                    ABases[AEffectiveMomentum].SetMonomialRep2(APartition)
                    Row = ABases[AEffectiveMomentum].BinaryArraySearchDescending()
                else:
                    Row = -1

                if BBases.has_key(BEffectiveMomentum):
                    BBases[BEffectiveMomentum].SetMonomialRep2(BPartition)
                    Col = BBases[BEffectiveMomentum].BinaryArraySearchDescending()
                else:
                    Col = -1

                if Row >= 0 and Col >= 0 and Row < ABases[AEffectiveMomentum].GetDimension() and Col < BBases[BEffectiveMomentum].GetDimension():
                    W.setValue(Row + ABasesOffsets[AEffectiveMomentum], Col + BBasesOffsets[BEffectiveMomentum], State.getValue(i) * Coefficient, True)
                    #W[Row + ABasesOffsets[AEffectiveMomentum], Col + BBasesOffsets[BEffectiveMomentum]] += State.getValue(i) * Coefficient

                i += 1

        W.assemble()

        return W


    cpdef OrbitalEntanglementMatrices(self, State, AOrbitalStart, AOrbitalEnd, AParticles=-1):
        """
        Calculate the entanglement matrices due to a bipartite cut in orbital space. Return dictionary containing matrices where they keys are tuples consisting of the number of particles
        and momentum of the A partition.
        """
        Details = self.GetBasisDetails()
        Momentum = Details['Momentum']
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        Matrices = dict()

        # loop over sensible particle and momentum cuts
        for aParticles in ([AParticles] if AParticles != -1 else range(Particles+1)):
            for AMomentum in range(NbrFlux):
                #Matrices[(AParticles, AMomentum)] = OrbitalEntanglementMatrix(Basis, State, AOrbitalStart, AOrbitalEnd, AMomentum, AParticles)
                Matrices[(aParticles, AMomentum)] = self.OrbitalEntanglementMatrix(State, AOrbitalStart, AOrbitalEnd, AMomentum, aParticles)

        return Matrices


    @staticmethod
    def OrbitalEntanglementMatricesSuperposition(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, AParticles=-1):
        """
        Calculate the entanglement matrices due to a bipartite cut in orbital space for the given
        superposition of states. Returns a dictionary containing matrices, with keys tuples consisting of
        the number of particles.
        """

        Details = Bases[0].GetBasisDetails()
        Particles = Details['Particles']
        NbrFlux = Details['NbrFlux']

        Matrices = dict()

        # loop over sensible particle and momentum cuts
        for aParticles in ([AParticles] if AParticles != -1 else range(Particles+1)):
    #        for AMomentum in range(NbrFlux):
                #Matrices[(AParticles, AMomentum)] = OrbitalEntanglementMatrix(Basis, State, AOrbitalStart, AOrbitalEnd, AMomentum, AParticles)
            Matrices[(aParticles)] = FQHTorusBasisBosonic.OrbitalEntanglementMatrixSuperposition(Bases, States, Coefficients, AOrbitalStart, AOrbitalEnd, aParticles)

        return Matrices


    def TranslateState(self, State, Amount):
        """
        Translate the given state by the amount of orbitals specified. Returns the new basis and new state.

        Parameters
        ----------
        State: vector
            Fock coefficients of state to translate.
        Amount: int
            The number of orbitals to translate by.

        """
        Momentum = self.Momentum
        Particles = self.Particles
        NbrFlux = self.NbrFlux

        NewMomentum = (Momentum + (Particles * Amount)) % NbrFlux

        NewBasis = FQHTorusBasisBosonic(Particles, NbrFlux, NewMomentum); NewBasis.GenerateBasis()

        cdef int Dim = self.Dim

        NewState = PETSc.Vec()
        NewState.create()
        NewState.setSizes(Dim)
        NewState.setUp()

        cdef int i = 0
        while i < Dim:
            Element = self.GetMonomialRep(i)
            Monomial = self.GetMonomial1()
            NewMonomial = Monomial[NbrFlux-Amount:]
            NewMonomial.extend(Monomial[:NbrFlux-Amount])
            NewBasis.SetMonomialRep2(NewMonomial)
            NewIdx = NewBasis.BinaryArraySearchDescending()
            if NewIdx >= 0 and NewIdx < Dim:
                NewState.setValue(NewIdx, State.getValue(i))
    #            print np.binary_repr(Element) + " -> " + np.binary_repr(NewElement) + " with phase " + str(ThisPhase)
            i += 1
        NewState.assemble()

        return [NewBasis, NewState]



def __main__():
    pass

if __name__ == '__main__':
    __main__()
