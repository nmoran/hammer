#! /usr/bin/env python

""" TorusFileManager.py: Utility tat keeps track of the torus file name structure. """

__author__      = "Mikael Fremling"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "micke.fremling@gmail.com"

#### ---------------------------------------------------------
#### Torus: The main formating scripts
#### ---------------------------------------------------------

import hammer.HammerDefaults as HD

def TorusBasisShort(Bosonic=False,Ne=HD.Ne(),Ns=HD.Ns(),
                          K1=HD.K1(),K2=HD.K2(),InversionSector=HD.Inversion()):
    if Ne==None:
        raise ValueError('Partcle number can not be None-type')
    if Ns==None:
        raise ValueError('Number of fluxes can not be None-type')
    if K1==None:
        raise ValueError('Momentum can not be None-type')
    if K2==None:
        raise ValueError('Co-momentum can not be None-type')
    if InversionSector==None:
        raise ValueError('InversionSector can not be None-type')    
    return ('Torus_' + ('Bosons_' if Bosonic else '')
            + 'p_'  + str(Ne)
            + "_Ns_" + str(Ns)
            + "_K_" + str(K1)
            + ('' if K2 == -1 else '_P_' + str(K2))
            + ('' if InversionSector == -1 else '_I_' + str(InversionSector)))

def TorusGeometryShort(Bosonic=False,Ne=HD.Ne(),Ns=HD.Ns(),
                          K1=HD.K1(),K2=HD.K2(),InversionSector=HD.Inversion(),
                          Tau1=HD.tau1(),Tau2=HD.tau2(),MagneticLength=HD.magnetic_length()):
    if Tau1==None:
        raise ValueError('Tau1 can not be None-type')
    if Tau2==None:
        raise ValueError('Tau2 can not be None-type')
    if MagneticLength==None:
        raise ValueError('MagneticLength can not be None-type')
    return (TorusBasisShort(Bosonic,Ne,Ns,K1,K2,InversionSector)
            +formatTau1(Tau1)
            +formatTau2(Tau2)
            +formatMaglen(MagneticLength))

def TorusInteractionShort(Bosonic=False,Ne=HD.Ne(),Ns=HD.Ns(),
                          K1=HD.K1(),K2=HD.K2(),InversionSector=HD.Inversion(),
                          Tau1=HD.tau1(),Tau2=HD.tau2(),MagneticLength=HD.magnetic_length(),
                          PseudoPotentials=None,LL=HD.ll(),
                          Interaction=HD.interaction(),AddThreeBody=HD.ThreeBodyStength()):
    return (TorusGeometryShort(Bosonic,Ne,Ns,K1,K2,InversionSector,
                               Tau1,Tau2,MagneticLength)
            +'_'
            +formatInteractionName(PseudoPotentials,LL,Interaction,AddThreeBody))

def TorusTauMaglen(Tau1=HD.tau1(),Tau2=HD.tau2(),MagneticLength=HD.magnetic_length()):
    return (formatTau1(Tau1) + formatTau2(Tau2) + formatMaglen(MagneticLength))

def formatTau1(Tau1):
    return '_tau1_' + '{:.3f}'.format(Tau1)

def formatTau2(Tau2):
    return '_tau2_' + '{:.3f}'.format(Tau2)

def formatMaglen(MagneticLength):
    return '_maglen_' + '{:.2f}'.format(MagneticLength)


def formatInteractionName(PseudoPotentials,LL,Interaction,AddThreeBody):
    import numpy as np
    if PseudoPotentials is not None:
        ID = 'PseudoPotential'
    else:
        ID = "LL" + str(LL) + Interaction
    if np.abs(AddThreeBody) > 0.0:
        ID = ID + '_3B_' + str(AddThreeBody)
    return ID


#### ---------------------------------------------------------
#### Torus: Ways of calling for filenames
#### ---------------------------------------------------------

def TorusBasisData(Bosonic,Ne,Ns,K1,K2,InversionSector,ID):
    return (TorusBasisShort(Bosonic,Ne,Ns,K1,
                            K2,InversionSector)
            + "_" + ID)

def TorusStatePrefix(Bosonic,Ne,Ns,K1,K2,InversionSector,
                     Tau1,Tau2,MagneticLength,ID):
    return (TorusGeometryShort(Bosonic,Ne,Ns,K1,K2,InversionSector,
                               Tau1,Tau2,MagneticLength)
            + "_" + ID )

def TorusFilenamePrefix(Bosonic,Ne,Ns,K1,K2,InversionSector,
                     Tau,ID):
    return (TorusBasisShort(Bosonic,Ne,Ns,K1,
                            K2,InversionSector)
            + ('' if Tau is None else formatTau1(Tau.real) + formatTau2(Tau.imag))
            + ID)

def TorusVectorFilename(Ne,Ns,K1,Tau1,Tau2,
                        MagneticLength,LL,Interaction,State):
    return (TorusGeometryShort(False,Ne,Ns,K1,-1,-1,
                               Tau1,Tau2,MagneticLength)
            + "_LL" + str(LL) + Interaction
            + '_State_' + str(State) + '.vec')

def TorusEnergiesFilename(Ne,Ns,K1,Tau1,Tau2,MagneticLength,LL,Interaction):
    return (TorusGeometryShort(False,Ne,Ns,K1,-1,-1,
                               Tau1,Tau2,MagneticLength)
            + "_LL" + str(LL) + Interaction
            + '_Energies.csv')

def TorusBasisFilename(Bosonic,Ne,Ns,K1,K2,InversionSector):
    return (TorusBasisShort(Bosonic,Ne,Ns,K1,K2,InversionSector)
            + '_Basis.basis')


#### ---------------------------------------------------------
#### Sphere: The main formating scripts
#### ---------------------------------------------------------


def SphereBasisShort(Bosonic,Ne,TwiceMaxLz,TwiceLz):
    return ('Sphere' + ('Bosons_' if Bosonic else '')
            + 'p_'  + str(Ne)
            + '_2s_' + str(TwiceMaxLz)
            + '_lz_' + str(TwiceLz))

#### ---------------------------------------------------------
#### Sphere: Ways of calling for filenames
#### ---------------------------------------------------------


def SpheresBasisFilename(Bosonic,Ne,TwiceMaxLz,TwiceLz):
    return (SphereBasisShort(Bosonic,Ne,TwiceMaxLz,TwiceLz)
            + '_Basis.basis')


