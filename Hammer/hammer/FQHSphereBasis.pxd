from FQHTorusBasis cimport FQHTorusBasis
cimport numpy as np


cdef class FQHSphereBasis(FQHTorusBasis):
    # Member variables for storing parameters for the basis
    cdef int TwiceLz, TwiceMaxLz, EffectiveMomentum
