#! /usr/bin/env python

""" TransformState.py: A utility to transform states between different formats and representations. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"

from petsc4py import PETSc
import sys
import os
import argparse
import numpy as np
import hammer

def __main__():
    """
    Main driver function, if script called from the command line it will use the command line arguments
    as the vector filenames.
    """
    Parser = argparse.ArgumentParser(description='Utility to perform certain types of transformations on the Hammer PETSc vectors.')
    Parser.add_argument('state', metavar='vec', type=str, nargs=1, help='Filename of state to transform.')
    Parser.add_argument('--output', '-o', type=str, default='', help='Output filename to use for state.')
    Parser.add_argument('--id', type=str, default='', help='Add an indentifying string to the output filename.')
    Parser.add_argument('--verbose','-v', action='store_true', help='Be verbose.')
    Parser.add_argument('--rotate', '-r', action='store_true', help='Rotate such that largest magnitude element is real.')
    Parser.add_argument('--full', '-f', action='store_true', help='Convert state to the full basis.')
    Parser.add_argument('--ph-conjugate','-c', action='store_true', help='Transform state into its particle hole conjugate.')
    Parser.add_argument('--translate','-t', type=int, default=0, help='The number of orbitals to translate the state by.')
    Parser.add_argument('--invert','-i', action='store_true', help='Flag to indicate that the state should be inverted (orbitals reflected about the centre).')
    Parser.add_argument('--File-name-only', action='store_true', help='Do not save any files. Only print to the terminal the output filename that will be used. (Useful if see .e.g how momentum sectors change under PH and inversion). Deactivates verbosenes.')
    

    Args = Parser.parse_args(sys.argv[1:])
    Verbose = (False if Args.File_name_only else Args.verbose)
    Rotate = Args.rotate
    PHConjugate = Args.ph_conjugate
    Full = Args.full
    OutputID = Args.id
    TranslationAmount = Args.translate
    Inversion = Args.invert

    # Read the vector, assuming it's in binary format
    StateFilename = Args.state[0]
    ext = os.path.splitext(StateFilename)[1]
    if ext != '.vec':
        print('Utility only works with vectors in binary petsc format.')
        sys.exit(1)
    A = hammer.vectorutils.ReadBinaryPETScVector(StateFilename)

    # IF rotation is enabled
    if Rotate: hammer.vectorutils.RotateVector(A)

    OrigBasisManager = hammer.FQHBasisManager()
    OrigBasisManager.SetTorusDetailsFromFilename(Args.state[0])
    Basis = OrigBasisManager.GetBasis(Verbose=Verbose)
    if A.getSize() != Basis.GetDimension():
        print('Mismatch between dimension of state (' + str(A.getSize()) + ') and dimension of basis (' + str(Basis.GetDimension()) + ').' )
        sys.exit(1)

    # Convert to full basis if option given
    if Full:
        if type(Basis) in [hammer.FQHTorusBasisCOMMomentum, hammer.FQHTorusBasisCOMMomentumInversion]:
            Basis, A = Basis.ConvertToFullBasis(A)
        else:
            print('The full basis option not supported for this type of state.')
            sys.exit(1)

    # Perfrom PH conjugation
    if PHConjugate:
        if type(Basis) not in [hammer.FQHTorusBasis, hammer.FQHTorusBasisCOMMomentum, hammer.FQHTorusBasisCOMMomentumInversion]:
            print('Particle hole conjugate is not supported for this basis type.')
            sys.exit(1)
        [Basis, A] = Basis.ConvertToPHConjugate(A)

    # Translate the state
    if np.abs(TranslationAmount) > 0:
        if type(Basis) not in [hammer.FQHTorusBasis]:
            print('Translation of states only supported for states of type FQHTorusBasis. Maybe try converting to this type first using full option.')
            sys.exit(1)
        [Basis, A] = Basis.TranslateState(A, TranslationAmount)

    # Perfrom inversion of state
    if Inversion:
        if type(Basis) not in [hammer.FQHTorusBasis]:
            print('Particle hole conjugate is not supported for this basis type. Try adding flag to convert to full.')
            sys.exit(1)
        [Basis, A] = Basis.ConvertToInvertedState(A)

    if Args.output == '':
        BasisManager = hammer.FQHBasisManager()
        BasisManager.SetTorusDetailsFromDict(Basis.GetBasisDetails())
        OutputPrefix = BasisManager.GetFilenamePrefix(tau=OrigBasisManager.Tau, ID=OrigBasisManager.ID + OutputID)
        OutputFilename = OutputPrefix + ext
    else:
        OutputFilename = Args.output

    if Args.File_name_only:
        print(str(OutputFilename))
    else:
        # write vector to disk
        if Verbose:
            print('Writing output vector to ' + str(OutputFilename) + '.')
        hammer.vectorutils.writeBinaryPETScFile(A, OutputFilename)

if __name__ == '__main__' :
    __main__()
