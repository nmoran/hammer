
# coding: utf-8

# Calcultions of the entanglement spectrum for Laughlin ground states. This code uses a new method which calculates entanglement matrices from superpositions of states and labels them by the momentum offset in the A partition. The resulting entanglement spectrum is compared to that of a single ground state and as shown is pretty different.

# In[29]:

from hammer.Diagonalise import LaughlinGroundState
from hammer.FQHTorusBasis import FQHTorusBasis
import numpy as np

Particles = 8
NbrFlux = 3 * Particles
[Basis1, State1] = LaughlinGroundState(Particles, False, np.complex(0.0,1.0))
[Basis2, State2] = Basis1.TranslateState(State1, 1)
[Basis3, State3] = Basis1.TranslateState(State1, 2)


# In[30]:

States = [State1, State2, State3]
Bases = [Basis1, Basis2, Basis3]
Coefficients = np.ones(3) / np.sqrt(3)
Matrices = FQHTorusBasis.OrbitalEntanglementMatricesSuperpositionSymmetry(Bases, States, Coefficients, 0, NbrFlux/2)
Norm = FQHTorusBasis.EntanglementNorm(Matrices)
print ("Norm: " + str(Norm))


# In[35]:

import matplotlib.pyplot as plt

get_ipython().magic(u'matplotlib inline')

ES = FQHTorusBasis.EntanglementSpectrum(Matrices)
#colors = ['b','r','g','k','m']
EE = FQHTorusBasis.EntanglementEntropy(Matrices)

print EE


for key in ES.keys():
    p = key[0]
    k = key[1]
    vals = ES[key]
    if p == (Particles/2):
        plt.plot(np.ones(len(vals))*key[1], vals, 'bo')

plt.xlim((0,Particles))
plt.ylim((0,10))

    
        
    


# In[32]:

Matrices2 = Basis1.OrbitalEntanglementMatrices(State1, 0, NbrFlux/2)


# In[36]:

import matplotlib.pyplot as plt

get_ipython().magic(u'matplotlib inline')

ES2 = FQHTorusBasis.EntanglementSpectrum(Matrices2)
EE2 = FQHTorusBasis.EntanglementEntropy(Matrices2)

print EE2
#colors = ['b','r','g','k','m']

for key in ES2.keys():
    p = key[0]
    k = key[1]
    vals = ES2[key]
    if p == (Particles/2):
        plt.plot(np.ones(len(vals))*key[1], vals, 'bo')

plt.xlim((0,NbrFlux))
plt.ylim((0,10))


