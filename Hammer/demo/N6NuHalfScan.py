#! /usr/bin/env python

from hammer.HammerArgs import HammerArgsParser
import hammer.Diagonalise as HammerDiag
import numpy as np

MinExponent = -1.0
MaxExponent = 1.0
NumberPoints = 30.0

Tau2Exponents = np.arange(MinExponent, MaxExponent + 0.00001, (MaxExponent - MinExponent)/NumberPoints)
Tau2s = 10**Tau2Exponents

Args = HammerArgsParser()
Args.parseArgs([])

Args.Particles = 6
Args.NbrFlux = 12
Args.LL = 0
Args.Interaction = 'Coulomb'
Args.SaveStates = True
Args.EPSTol = 1e-12
Args.NbrEigvals = 10
Args.NbrStates = 4

for Tau2 in Tau2s:
    Args.Tau = np.complex(0.0, Tau2)
    for K in np.arange(0, 6):
        Args.Momentum = K
        HammerDiag.Diagonalise(Args)

Args.LL = 1
Args.Interaction = 'Coulomb'

for Tau2 in Tau2s:
    Args.Tau = np.complex(0.0, Tau2)
    for K in np.arange(0, 6):
        Args.Momentum = K
        HammerDiag.Diagonalise(Args)



