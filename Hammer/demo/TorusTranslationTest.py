# -*- coding: utf-8 -*-
"""
Created on Wed Aug  6 13:53:54 2014

@author: nmoran
"""

from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients2Body
from hammer.FQHTorusCoefficients import FQHTorusInteractionCoefficients3Body
from hammer.FQHTorusBasis import FQHTorusBasis
import scipy.sparse as sparse
import scipy.sparse.linalg as splinalg
import scipy as sp
import numpy.linalg as linalg
import numpy as np
import time


def GetNormal(MyBasis, Idx, t, l, n, Even):
    nf = np.float(n)
    normal = 0.0
    S = MyBasis.GetElement(Idx)
    for k in np.arange(nf):
        TS = MyBasis.Translate(S, t*k)
        phase = np.float(np.sign(TS))
        TS = np.abs(TS)
        if TS == S:
            #normal += phase * np.exp(np.complex(0.0,2.0*np.pi*k*np.float(l)/np.float(n)))
            #if l == 0:
            #    normal += 2.0 * np.cos(2.0*np.pi*np.float(l)*np.float(k)/float(n))
            #else:
            #    normal += 2.0 * np.cos(2.0*np.pi*np.float(l)*np.float(k)/float(n))
            #print normal
            #normal += 2.0 * (nf - k) * np.cos(2.0*np.pi*np.float(k)*np.float(l)/nf)
            
            if Even:
#                normal += (nf - k) * np.exp(np.complex(0.0,2.0*np.pi*l*k /nf)) * phase
#                normal += k * np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf)) * phase
                normal += np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf)) * phase
            else:
#                normal += (nf - k) * np.exp(np.complex(0.0,2.0*np.pi*l*k /nf)) 
#                normal += k * np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf)) 
                normal += np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf)) 
            
#            normal += (nf - k) * np.exp(np.complex(0.0,2.0*np.pi*l*k /nf))  
#            normal += k * np.exp(np.complex(0.0, 2.0*np.pi*(k-nf)*l/nf))  
            
    return normal    


def GetRep(MyBasis, Idx, IdxRep, t, l, n, Even):
    nf = np.float(n)
    Phase = 0.0
    if Idx >= 0:
        State = MyBasis.GetElement(Idx) # get explicit state at Idx
        RepState = MyBasis.GetElement(IdxRep) 
        for k in np.arange(nf):
            TS = MyBasis.Translate(State, k*t)
            TPhase = np.float(np.sign(TS))
            TS = np.abs(TS)
            if TS == RepState:
                if Even:
#                    Phase += (nf - k) * np.exp(np.complex(0.0, 2.0*np.pi*l*k /nf)) * TPhase
#                    Phase += k * np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf)) * TPhase
                    Phase += np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf)) * TPhase
                else:
#                    Phase += (nf - k) * np.exp(np.complex(0.0, 2.0*np.pi*l*k /nf)) 
#                    Phase += k * np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf)) 
                    Phase += np.exp(np.complex(0.0, 2.0*np.pi*(k)*l/nf))
            
#                Phase += (nf - k) * np.exp(np.complex(0.0, 2.0*np.pi*l*k /nf)) 
#                Phase += k * np.exp(np.complex(0.0, 2.0*np.pi*(k-nf)*l/nf))  
                
    return Phase


Particles = 5
NbrFlux = 15
Momentum = 0
t = 3
n = NbrFlux / t
if (Particles % 2) == 0:
    Even = True
else:
    Even = False

MyEigVals = dict()
MyMatrices = dict()
DenseMatrices = dict()
Normals = dict()
Counts = np.zeros(n)
UseSparse =  True 
AllEigVals = np.array([])
CalcEigVals = 5
BatchSize = 1000

MyBasis = FQHTorusBasis(Particles, NbrFlux, Momentum);
MyBasis.GenerateBasis()
#MyCoefficients = FQHTorusInteractionCoefficients2Body(Tau=np.complex(0.0, 1.0), Ns=NbrFlux,PseudoPotentials=[0.0, 0.0, 0.0, 1.0]);
MyCoefficients = FQHTorusInteractionCoefficients2Body(Tau=np.complex(0.0, 1.0), Ns=NbrFlux,PseudoPotentials=[0.0, 1.0, 0.0, 0.0]);
#MyCoefficients = FQHTorusInteractionCoefficients2Body(Tau=np.complex(0.0, 1.0), Ns=NbrFlux,PseudoPotentials=[0.0, 1.0, 0.0, 0.0]);
#MyCoefficients = FQHTorusInteractionCoefficients2Body(Tau=np.complex(0.0, 1.0), Ns=NbrFlux,PseudoPotentials=None);
#MyCoefficients = FQHTorusInteractionCoefficients2Body(Tau=np.complex(0.0, 1.0), Ns=NbrFlux,PseudoPotentials=None, LL=1);
#MyCoefficients = FQHTorusInteractionCoefficients2Body(Ns=NbrFlux);
#MyCoefficients = FQHTorusInteractionCoefficients3Body(Ns=NbrFlux, Interaction='3Body');
MyCoefficients.CalculateCoefficients()
Dim = MyBasis.GetDimension()

for l in range(0,n):
    Normals[l] = list()
    l = np.float(l)
    
    MapBig2Small = dict() # stored mapping from the full basis to the reduced basis.
    MapSmall2Big = dict() # stored mapping from reduced basis to full basis
        
    count = 0
    for Row in range(Dim):
        Normal1 = GetNormal(MyBasis, Row, t, l, n, Even)
        if MyBasis.RepresentativeConfiguration(Row) == Row and np.abs(Normal1) > 1e-10: # check if the config is representative.
            MapBig2Small[Row] = count
            MapSmall2Big[count] = Row
            count += 1
            
    ReducedDim = count

    print "Reduced dimension: " + str(ReducedDim)    
    
    Matrix = sparse.lil_matrix((ReducedDim, ReducedDim),dtype=np.complex)

    count = 0
    start = time.time()
    for ReducedRow in range(ReducedDim):
        Row = MapSmall2Big[ReducedRow]
        Normal1 = GetNormal(MyBasis, Row, t, l, n, Even)
        Normals[l].append(Normal1)
        if (ReducedRow % BatchSize) == 0 and ReducedRow > 0: 
            end = time.time()
            print "At row: " + str(ReducedRow) + "(in time " + str(end-start) +")"
        #if MyBasis.RepresentativeConfiguration(Row) == Row:
        #    count += 1
        if MyBasis.RepresentativeConfiguration(Row) == Row and np.abs(Normal1) > 1e-10: # check if the config is representative.
            count += 1
            for j in range(MyCoefficients.NumPairs):
                Phase1 = MyBasis.AA(MyCoefficients.J3J4Vals[j,0], MyCoefficients.J3J4Vals[j,1], Row)
                for m in range(MyCoefficients.NonZeros[j]):
                    if Phase1 != 0.0:
                        Phase2 = MyBasis.AdAd(MyCoefficients.J1J2Vals[j][m,0], MyCoefficients.J1J2Vals[j][m,1])
                        if Phase2 != 0.0:
                            #now must find the representative configuration of MyBasis.TmpState2
                            Idx = MyBasis.BinaryArraySearchDescending() # index of element that matrix term connects to 
                            Col = MyBasis.RepresentativeConfiguration(Idx) # get the index of the representative
                            Normal2 = GetNormal(MyBasis, Col, t, l, n, Even) # normal of the column rep
                            if np.abs(Normal2) > 1e-10:
                                Phase = GetRep(MyBasis, Idx, Col, t, l, n, Even)
                                Norm = 1.0 / ( np.sqrt(Normal1) * np.sqrt(Normal2))
                                ReducedCol = MapBig2Small[Col]
                                Matrix[ReducedRow, ReducedCol] += Norm * Phase * Phase1 * Phase2 * MyCoefficients.AntiSymCoefs[j][m]
    
    CSV_Matrix = Matrix.tocsr()
    del Matrix
    
    MyMatrices[l] = CSV_Matrix.copy()
    Counts[l] = ReducedDim    
    
    print "Diagonalising" 
    
    if UseSparse:
        [w,v] = splinalg.eigs(CSV_Matrix,k=(ReducedDim-1 if (ReducedDim-1) < CalcEigVals else CalcEigVals), which='SR')
    else:
        DenseMatrices[l] = CSV_Matrix.todense()
        [w,v] = linalg.eig(DenseMatrices[l])

    w = np.real(w)
    w = filter(lambda x:x!= 0, w)
    w = np.sort(w)
    print "Eigenvalues for momentum sector " + str(l) + " dimension " + str(Counts[l])
    print w
    MyEigVals[l] = w
    AllEigVals = np.concatenate((AllEigVals, w))
    
    
print "Sum: " + str(np.sum(Counts))

AllEigVals = np.sort(AllEigVals)
print AllEigVals



