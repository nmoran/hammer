#! /usr/bin/env python

"""
 Unit tests to test that the basis objects are working as expected for small systems.

"""
import unittest
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum as FQHTorusCOMBasis
from hammer.vectorutils import *

from petsc4py import PETSc
from slepc4py import SLEPc

import numpy as np

class TestDensityOperator(unittest.TestCase):

    def setUp(self):
        ###Nothing to to here
        print "Setup"
        
    def test_k1_v1(self):
        Ne=3
        Ns=6
        K1=0
        deltak1=1
        deltak2=0
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis 0"
        for Elem in range(Dim0):
            Basis0.PrintOccupationRep(Elem)
        print "Elements in basis 1"
        for Elem in range(Dim1):
            Basis1.PrintOccupationRep(Elem)

        ###Check that moving the particles gives the correct resultant state
        #(0) 000111 -> (1) 100110
        State0=CreatePETScVector([1,0,0,0])
        Target0=CreatePETScVector([0,1,0])
        PrintPETScVector(State0)
        BP0,DP0=Basis0.ApplyDensityOperator(State0,deltak1,deltak2)
        PrintPETScVector(DP0)
        print("Test No 0")
        TestEqualVector(self,DP0,Target0)
        #(1) 110001 -> (0) 101001
        State1=CreatePETScVector([0,1,0,0])
        Target1=CreatePETScVector([1,0,0])
        BP1,DP1=Basis0.ApplyDensityOperator(State1,deltak1,deltak2)
        PrintPETScVector(DP1)
        print("Test No 1")
        TestEqualVector(self,DP1,Target1)
        #(2) 101010 -> (2) 011010 + (1) 100110 + (0) 101001
        State2=CreatePETScVector([0,0,1,0])
        Target2=CreatePETScVector([1,1,1])
        BP2,DP2=Basis0.ApplyDensityOperator(State2,deltak1,deltak2)
        PrintPETScVector(DP2)
        print("Test No 2")
        TestEqualVector(self,DP2,Target2)
        #(3) 011100 -> (2) 011010
        State3=CreatePETScVector([0,0,0,1])
        Target3=CreatePETScVector([0,0,1])
        BP3,DP3=Basis0.ApplyDensityOperator(State3,deltak1,deltak2)
        PrintPETScVector(DP3)
        print("Test No 3")
        TestEqualVector(self,DP3,Target3)
        self.assertTrue(True)


    def test_k1_v2(self):
        Ne=3
        Ns=6
        K1=1
        deltak1=1
        deltak2=0
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis 0"
        for Elem in range(Dim0):
            Basis0.PrintOccupationRep(Elem)
        print "Elements in basis 1"
        for Elem in range(Dim1):
            Basis1.PrintOccupationRep(Elem)

        ###Check that moving the particles gives the correct resultant state
        #(0) 101001 -> (0) 100101 + (1) 011001
        State0=CreatePETScVector([1,0,0])
        Target0=CreatePETScVector([1,1,0])
        PrintPETScVector(State0)
        BP0,DP0=Basis0.ApplyDensityOperator(State0,deltak1,deltak2)
        PrintPETScVector(DP0)
        print("Test No 0")
        TestEqualVector(self,DP0,Target0)
        #(1) 100110 -> (2) 010110 + (0) 100101
        State1=CreatePETScVector([0,1,0])
        Target1=CreatePETScVector([1,0,1])
        BP1,DP1=Basis0.ApplyDensityOperator(State1,deltak1,deltak2)
        PrintPETScVector(DP1)
        print("Test No 1")
        TestEqualVector(self,DP1,Target1)
        #(2) 011010  -> (1) 011001 + (2) 010110
        State2=CreatePETScVector([0,0,1])
        Target2=CreatePETScVector([0,1,1])
        BP2,DP2=Basis0.ApplyDensityOperator(State2,deltak1,deltak2)
        PrintPETScVector(DP2)
        print("Test No 2")
        TestEqualVector(self,DP2,Target2)
        self.assertTrue(True)

        

    def test_k1_v3(self):
        Ne=4
        Ns=6
        K1=0
        deltak1=3
        deltak2=0
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis 0"
        for Elem in range(Dim0):
            Basis0.PrintOccupationRep(Elem)
        print "Elements in basis 1"
        for Elem in range(Dim1):
            Basis1.PrintOccupationRep(Elem)

        ###Check that moving the particles gives the correct resultant state
        #(0) 100111 ->  - (0) 110101 + (1) 101110
        State0=CreatePETScVector([1,0,0])
        Target0=CreatePETScVector([-1,1])
        PrintPETScVector(State0)
        BP0,DP0=Basis0.ApplyDensityOperator(State0,deltak1,deltak2)
        PrintPETScVector(DP0)
        print("Test No 0")
        TestEqualVector(self,DP0,Target0)
        #(1) 011011 -> 0
        State1=CreatePETScVector([0,1,0])
        Target1=CreatePETScVector([0,0])
        BP1,DP1=Basis0.ApplyDensityOperator(State1,deltak1,deltak2)
        PrintPETScVector(DP1)
        print("Test No 1")
        TestEqualVector(self,DP1,Target1)
        #(2) 111100  -> (1) 101110 - (0) 110101
        State2=CreatePETScVector([0,0,1])
        Target2=CreatePETScVector([-1,1])
        BP2,DP2=Basis0.ApplyDensityOperator(State2,deltak1,deltak2)
        PrintPETScVector(DP2)
        print("Test No 2")
        TestEqualVector(self,DP2,Target2)
        self.assertTrue(True)


    def test_k1_v4(self):
        Ne=4
        Ns=7
        K1=0
        deltak1=2
        deltak2=0
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis 0"
        for Elem in range(Dim0):
            Basis0.PrintOccupationRep(Elem)
        print "Elements in basis 1"
        for Elem in range(Dim1):
            Basis1.PrintOccupationRep(Elem)
            
        ###Test for a random vector - that everything adds nicely
        StateRandom=CreatePETScVector(np.random.rand(Dim0)+1j*np.random.rand(Dim0))
        TargetRandom=np.zeros(Dim1,dtype=np.complex)
        for Elem in range(Dim0):
            State0=np.zeros(Dim0,dtype=np.complex)
            State0[Elem]=StateRandom[Elem]
            DP0=Basis0.ApplyDensityOperator(CreatePETScVector(State0),deltak1,deltak2)[1].getArray()
            TargetRandom+=DP0
        ReturnBasis,ReturnRandom=Basis0.ApplyDensityOperator(StateRandom,deltak1,deltak2)
        TestEqualVector(self,CreatePETScVector(TargetRandom),ReturnRandom)
        self.assertTrue(True)



    def test_k2_v1(self):
        Ne=3
        Ns=6
        K1=0
        deltak1=0
        deltak2=1
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis 0"
        for Elem in range(Dim0):
            Basis0.PrintOccupationRep(Elem)
        print "Elements in basis 1"
        for Elem in range(Dim1):
            Basis1.PrintOccupationRep(Elem)

        ###Check that moving the particles gives the correct resultant state
        #(0) 000111 -> exp(3/6 4/6 5/6)000111
        State0=CreatePETScVector([1,0,0,0])
        Momenta=np.array([3,4,5])
        Target0=CreatePETScVector([np.sum(np.exp(Momenta*1j*2*np.pi/Ns)),0,0,0])
        BP0,DP0=Basis0.ApplyDensityOperator(State0,deltak1,deltak2)
        PrintPETScVector(DP0)
        print("Test No 0")
        TestEqualVector(self,DP0,Target0)
        #(1) 110001 -> exp(0/6 1/6 5/6)110001
        State1=CreatePETScVector([0,1,0,0])
        Momenta=np.array([0,1,5])
        Target1=CreatePETScVector([0,np.sum(np.exp(Momenta*1j*2*np.pi/Ns)),0,0])
        BP1,DP1=Basis0.ApplyDensityOperator(State1,deltak1,deltak2)
        PrintPETScVector(DP1)
        print("Test No 1")
        TestEqualVector(self,DP1,Target1)
        #(2) 101010 -> exp(0/6 2/6 4/6)101010
        State2=CreatePETScVector([0,0,1,0])
        Momenta=np.array([0,2,4])
        Target2=CreatePETScVector([0,0,np.sum(np.exp(Momenta*1j*2*np.pi/Ns)),0])
        BP2,DP2=Basis0.ApplyDensityOperator(State2,deltak1,deltak2)
        PrintPETScVector(DP2)
        print("Test No 2")
        TestEqualVector(self,DP2,Target2)
        #(3) 011100 -> exp(1/6 2/6 3/6)011100
        State3=CreatePETScVector([0,0,0,1])
        Momenta=np.array([1,2,3])
        Target3=CreatePETScVector([0,0,0,np.sum(np.exp(Momenta*1j*2*np.pi/Ns))])
        BP3,DP3=Basis0.ApplyDensityOperator(State3,deltak1,deltak2)
        PrintPETScVector(DP3)
        print("Test No 3")
        TestEqualVector(self,DP3,Target3)
        self.assertTrue(True)


    def test_k1_k2_v1(self):
        print ".................."
        Ne=2
        Ns=5
        K1=2
        deltak1=2
        deltak2=3
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis 0"
        for Elem in range(Dim0):
            Basis0.PrintOccupationRep(Elem)
        print "Elements in basis 1"
        for Elem in range(Dim1):
            Basis1.PrintOccupationRep(Elem)

        ###Check that moving the particles gives the correct resultant state
        #(0) 00011 -> exp(3*3/5) 10001 (0)  - exp(3*4/5) 01010 (1)
        State0=CreatePETScVector([1,0])
        Target0=CreatePETScVector([np.exp(3*deltak2*1j*2*np.pi/Ns),
                                   -np.exp(4*deltak2*1j*2*np.pi/Ns)])
        BP0,DP0=Basis0.ApplyDensityOperator(State0,deltak1,deltak2)
        PrintPETScVector(DP0)
        print("Test No 0")
        TestEqualVector(self,DP0,Target0)
        #(1) 10100 ->  exp(3*2/5) 10001 (0)
        State1=CreatePETScVector([0,1])
        Target1=CreatePETScVector([np.exp(2*deltak2*1j*2*np.pi/Ns),0])
        BP1,DP1=Basis0.ApplyDensityOperator(State1,deltak1,deltak2)
        PrintPETScVector(DP1)
        print("Test No 1")
        TestEqualVector(self,DP1,Target1)
        self.assertTrue(True)



    def test_k1_k2_v2(self):
        Ne=4
        Ns=7
        K1=3
        deltak1=2
        deltak2=3
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis 0"
        for Elem in range(Dim0):
            Basis0.PrintOccupationRep(Elem)
        print "Elements in basis 1"
        for Elem in range(Dim1):
            Basis1.PrintOccupationRep(Elem)
            
        ###Test for a random vector - that everything adds nicely
        StateRandom=CreatePETScVector(np.random.rand(Dim0)+1j*np.random.rand(Dim0))
        TargetRandom=np.zeros(Dim1,dtype=np.complex)
        for Elem in range(Dim0):
            State0=np.zeros(Dim0,dtype=np.complex)
            State0[Elem]=StateRandom[Elem]
            DP0=Basis0.ApplyDensityOperator(CreatePETScVector(State0),deltak1,deltak2)[1].getArray()
            TargetRandom+=DP0
        ReturnBasis,ReturnRandom=Basis0.ApplyDensityOperator(StateRandom,deltak1,deltak2)
        TestEqualVector(self,CreatePETScVector(TargetRandom),ReturnRandom)
        self.assertTrue(True)

    def test_k1_k2_eigenvalue_shift(self):
        qvalue=3
        Ne=3
        Ns=Ne*qvalue
        K1=2
        deltak1=1
        deltak2=1
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        Basis0.GenerateBasis()
        Basis1.GenerateBasis()
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        print "Dim0:",Dim0
        print "Dim1:",Dim1

        print "Elements in basis0:"
        for Elem in range(Dim0):
            print "Elements no:",Elem
            Basis0.PrintOccupationRep(Elem)
            for K2value in range(Ne): ####For 1/q above the number of K2 values equals Ne
                print "................................."
                print "Testing for Kvalue2=",K2value
                InitVector0=np.zeros(Dim0);
                InitVector0[Elem]=1
                State0=CreatePETScVector(InitVector0)
                VectorTarget=np.zeros(Dim0,dtype=np.complex);
                for Translation in range(0,Ns,qvalue):
                    BasisTranslate,StateTranslate=Basis0.TranslateState(State0,Translation)
                    #print " After translated:",Translation
                    #print " Momentum:",BasisTranslate.GetBasisDetails()['Momentum']
                    self.assertTrue(BasisTranslate.GetBasisDetails()['Momentum']==K1)
                    #PrintPETScVector(StateTranslate)
                    VectorTarget+=np.exp(-1j*Translation*K2value*2*np.pi/Ns)*StateTranslate.getArray()
                print " VectorTarget:",VectorTarget
                StateTarget=CreatePETScVector(VectorTarget)
                #### Now we assert that the T2 eigevnalue is correct
                [TestBasis,TestTarget]=Basis0.TranslateState(StateTarget,qvalue)
                TestEqualVector(self,CreatePETScVector(np.exp(1j*K2value*2*np.pi/Ne)*StateTarget),TestTarget)

                ###Now we act with the density operator and transform the state
                ShifterBasis,ShiftedState=Basis0.ApplyDensityOperator(StateTarget,deltak1,deltak2)

                ###The we check that the K1-> K1 + deltak1
                ###And that K2 -> K2 + deltak2

                self.assertTrue(Basis1.GetBasisDetails()['Momentum']==(K1+deltak1))
                [TestShiftBasis,TestShiftTarget]=Basis1.TranslateState(ShiftedState,qvalue)
                TestEqualVector(self,CreatePETScVector(np.exp(1j*(K2value-deltak2)*2*np.pi/Ne)
                                                       *ShiftedState),TestShiftTarget)

                
        self.assertTrue(True)



        
    def test_k1_k2_large_size(self):
        from timeit import default_timer as timer
        Ne=12
        Ns=24
        K1=3
        deltak1=2
        deltak2=3
        Basis0=FQHTorusBasis(Ne,Ns,K1)
        Basis1=FQHTorusBasis(Ne,Ns,K1+deltak1)
        print "Generate Basis 0"
        start = timer()
        Basis0.GenerateBasis()
        end = timer()
        print "Basis 0 took ",end - start,"seconds"
        print "Generate Basis 1"
        start = timer()
        Basis1.GenerateBasis()
        end = timer()
        print "Basis 1 took ",end - start,"seconds"
        Dim0=Basis0.GetDimension()
        Dim1=Basis1.GetDimension()
        #print "Dim0:",Dim0
        #print "Dim1:",Dim1


        print "Create Vector"
        ###Test for a random vector - that everything adds nicely
        StateRandom=CreatePETScVector(np.random.rand(Dim0)+1j*np.random.rand(Dim0))
        print "Transform Vector"
        start = timer()
        ReturnBasis, ReturnRandom=Basis0.ApplyDensityOperator(StateRandom,deltak1,deltak2)
        end = timer()
        print "Density operator took",end - start,"seconds"
        self.assertTrue(True)



    def test_k1_k2_generic(self):
        Ne=5
        Ns=10
        for K1 in range(Ns):
            for K2 in range(Ne):
                ###Build this basis
                print "K1,K2:",K1,K2
                BasisFrom=FQHTorusCOMBasis(Ne,Ns,K1,K2)
                BasisFrom.GenerateBasis()

                deltaK1=np.mod((K1+K2)**2+Ns/2,Ns)-Ns/2
                deltaK2=np.mod((K1-2*K2)**2+Ne/2,Ne)-Ne/2

                K1To=np.mod(K1+deltaK1,Ns)
                K2To=np.mod(K2+deltaK2,Ne)
                
                BasisTo=FQHTorusCOMBasis(Ne,Ns,K1+deltaK1,K2+deltaK2)
                BasisTo.GenerateBasis()
                print "deltaK1,deltaK2:",deltaK1,deltaK2
                print "K1To,K2To:",K1To,K2To

                print "Dimention From:",BasisFrom.GetDimension()
                print "Dimention To  :",BasisTo.GetDimension()
                
                VectorFrom=CreateRandomPETScVector(BasisFrom.GetDimension())
                VectorTo=CreateRandomPETScVector(BasisTo.GetDimension())

                ###Expand to Full Basis
                ExpBasisFrom,ExpVectorFrom=BasisFrom.ConvertToFullBasis(VectorFrom)
                ExpBasisTo  ,ExpVectorTo  =BasisTo.ConvertToFullBasis(    VectorTo)
                ###Check K2 value
                CheckK2Value(ExpBasisFrom,ExpVectorFrom,Ne,K2)
                CheckK2Value(ExpBasisTo,ExpVectorTo,Ne,K2To)
                ###Transform the From Basis to the To Basis
                ProjBasis,ProjVector=ExpBasisFrom.ApplyDensityOperator(ExpVectorFrom,deltaK1,deltaK2)
                ###Check that the target bases and the proj basa agree in K1 value
                CheckK1Value(ProjBasis,ExpBasisTo)
                CheckK2Value(ProjBasis,ProjVector,Ne,K2To)

        self.assertTrue(True)
def CheckK1Value(Basis1,Basis2):
    print(Basis1.GetBasisDetails())
    print(Basis2.GetBasisDetails())
    if Basis1.GetBasisDetails()!=Basis2.GetBasisDetails():
        ERRMSG="The Two bases:\n"+str(Basis1)+"\nand\n"+str(Basis2)+"\n Do not agree!"
        raise Exception, ERRMSG
        
def CheckK2Value(Basis,Vector,Ne,K2):
    ExpandK2=GetK2Value(Basis,Vector,Ne)
    if ExpandK2 != K2:
        ERRMSG=("CoMomentum in Basis = "+str(ExpandK2)
                + " differ from target = "+str(K2))
        raise Exception,ERRMSG

def GetK2Value(Basis,Vector,Ne):
    NewBasis, NewVector=Basis.TranslateState(Vector,2)
    ###This should produce an eigenvalue
    Norm=Vector.dot(Vector)
    NewNorm=NewVector.dot(NewVector)
    Overlap=Vector.dot(NewVector)
    if np.abs(Overlap/np.sqrt(Norm*NewNorm)) < 0.999 :
        print("Norm:",Norm)
        print("NewNorm:",NewNorm)
        print("|Overlap|:",np.abs(Overlap))
        print("Overlap:",Overlap)
        PrintPETScVector(Vector)
        PrintPETScVector(NewVector)
        raise Exception,"Vector does not have a K2 eigenvalue"
    Angle=np.angle(Overlap)*Ne/(2*np.pi)
    return np.mod(np.round(Angle),Ne) 
    
        

def CreateRandomPETScVector(Dim):
    return CreatePETScVector(np.random.rand(Dim)+1j*np.random.rand(Dim))
        
def TestEqualVector(self,PVec1,PVec2):
    Dim1=PVec1.getSize()
    Dim2=PVec2.getSize()
    if Dim1!=Dim2:
        self.assertTrue(False)
    Vec1=PVec1.getArray()
    Vec2=PVec2.getArray()
    Diff=Vec2-Vec1
    print "Vec1:",Vec1
    print "Vec2:",Vec2
    print "Diff:",Diff
    if np.sum(np.abs(Diff))>10**(-10):
        self.assertTrue(False)

        
def CreatePETScVector(Vector):
    A=PETSc.Vec().createWithArray(Vector)
    #print "Created Vector:",A.getArray()
    return A


def PrintPETScVector(Vector):
    print "PETScVector:",Vector.getArray()

if __name__ == '__main__':
    unittest.main()
