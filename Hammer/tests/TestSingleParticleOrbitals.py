#! /usr/bin/env python
from __future__ import print_function

"""
 Unit tests to for reflection symmetry code. Tests that the eigenvalues retrieved are correct for a few small systems. 
"""
from hammer.FQHTorusWF import FQHTorusWF


import unittest
import random
import numpy as np
import petsc4py.PETSc as PETSc


class TestSinglePartileOrbitals(unittest.TestCase):
    """
    Test suite for single particle orbitals.
    """

    def setUp(self):
        self.Threshold = 1e-09

    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_pbc_LLL(self):
        """ Test that periodic boundary conditions works for single particles in LLL in tau gauge"""
        MaxFlux=10
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            Lx=np.sqrt(2*np.pi*NbrFlux/np.imag(tau))
            WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=0)
            for Momenta in range(NbrFlux):
                x = random.uniform(-.5,.5)
                y = random.uniform(-.5,.5)
                xy = x+1j*y
                ## x-pbc
                x1 = x + 1.0 
                y1 = y
                xy1= x1+1j*y1
                ## y-pbc
                x2 = x
                y2 = y + 1.0 
                xy2= x2+1j*y2
                ## Extra phase for y-pbc
                phase = 2*np.pi*x2*NbrFlux
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0

                wfval =WFn.WFValueJT(xy ,Momenta)
                wfval1=WFn.WFValueJT(xy1,Momenta)
                wfval2=WFn.WFValueJT(xy2,Momenta)*np.exp(1j*phase)


                if np.abs(wfval-wfval1) > self.Threshold or np.abs(wfval-wfval2) > self.Threshold:
                    print("\nERROR: wfvalJT does not have periodic boundary conditions")
                    print("NbrFlux:",NbrFlux)
                    print('tau,Lx:',tau,Lx)
                    print('k,x,y:',Momenta,x,y)
                    print("phase: ",phase)
                    print("wfval: ",wfval)
                    print("wfval1:",wfval1)
                    print("wfval2:",wfval2)

                    print("log(wfval1/wfval): ",np.log(wfval1/wfval))
                    print("log(wfval2/wfval): ",np.log(wfval2/wfval))
                    print("log(wfval2/wfval)/phase: ",np.log(wfval2/wfval)/phase)

                    self.assertTrue(False)

        self.assertTrue(True)

 
    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_JT_equiv_LLL(self):
        """ Test that FQHTorusWF.WFValueJT gives the same result as FQHTorusWF.WFValueLLL """
        MaxFlux=10
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            Lx=np.sqrt(2*np.pi*NbrFlux/np.imag(tau))
            WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=0)
            for Momenta in range(NbrFlux):
                x = random.uniform(-.5,.5)
                y = random.uniform(-.5,.5)
                xy = x+1j*y
                wfvalJT=WFn.WFValueJT(xy,Momenta)
                wfvalLLL=WFn.WFValueLLL(xy,Momenta)
                if np.abs(wfvalJT-wfvalLLL) > self.Threshold:
                    print("\nERROR: wfvalJT and wfvalLLL are note the same")
                    print("NbrFlux:",NbrFlux)
                    print('tau,Lx:',tau,Lx)
                    print('k,x,y,z:',Momenta,x,y)
                    print("wfvalJT: ",wfvalJT)
                    print("wfvalLLL:",wfvalLLL)
                    print("|wfvalJT|: ",np.abs(wfvalJT))
                    print("|wfvalLLL|:",np.abs(wfvalLLL))
                    print("wfvalJT/wfvalLLL:",wfvalJT/wfvalLLL)
                    self.assertTrue(False)


    def test_General_equiv_LLL(self):
        """ Test that FQHTorusWF.WFValueJT gives the same result as FQHTorusWF.WFValueLLL """
        MaxFlux=10
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            tau=1j+.5
            Lx=np.sqrt(2*np.pi*NbrFlux/np.imag(tau))
            WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=0)
            for Momenta in range(NbrFlux):
                x = random.uniform(-.5,.5)
                y = random.uniform(-.5,.5)
                xy = x+1j*y
                wfvalGen=WFn.WFValueGeneral(xy,Momenta)
                wfvalLLL=WFn.WFValueLLL(xy,Momenta)
                if np.abs(wfvalGen/wfvalLLL-1) > self.Threshold:
                    print("\nERROR: wfvalGen and wfvalLLL are note the same")
                    print("NbrFlux:",NbrFlux)
                    print('tau,Lx:',tau,Lx)
                    print('k,x,y,z:',Momenta,x,y)
                    print("wfvalGen: ",wfvalGen)
                    print("wfvalLLL:",wfvalLLL)
                    print("|wfvalGen|: ",np.abs(wfvalGen))
                    print("|wfvalLLL|:",np.abs(wfvalLLL))
                    print("wfvalGen/wfvalLLL-1:",wfvalGen/wfvalLLL)
                    self.assertTrue(False)



    def test_General_equiv_SLL(self):
        """ Test that FQHTorusWF.WFValueJT gives the same result as FQHTorusWF.WFValueLLL """
        MaxFlux=10
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            Lx=np.sqrt(2*np.pi*NbrFlux/np.imag(tau))
            WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=1)
            for Momenta in range(NbrFlux):
                x = random.uniform(-.5,.5)
                y = random.uniform(-.5,.5)
                xy = x+1j*y
                wfvalGen=WFn.WFValueGeneral(xy,Momenta)
                wfvalSLL=WFn.WFValueSLL(xy,Momenta)
                if np.abs(wfvalGen/wfvalSLL-1) > self.Threshold:
                    print("\nERROR: wfvalGen and wfvalSLL are note the same")
                    print("NbrFlux:",NbrFlux)
                    print('tau,Lx:',tau,Lx)
                    print('k,x,y,z:',Momenta,x,y)
                    print("wfvalGen: ",wfvalGen)
                    print("wfvalSLL:",wfvalSLL)
                    print("|wfvalGen|: ",np.abs(wfvalGen))
                    print("|wfvalSLL|:",np.abs(wfvalSLL))
                    print("wfvalGen/wfvalSLL-1:",wfvalGen/wfvalSLL-1)
                    self.assertTrue(False)


    def test_pbc_SLL(self):
        """ Test that periodic boundary conditions works for single particles in SLL in tau gauge"""
        MaxFlux=10
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            Lx=np.sqrt(2*np.pi*NbrFlux/np.imag(tau))
            WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=1)
            for Momenta in range(NbrFlux):
                x = random.uniform(-.5,.5)
                y = random.uniform(-.5,.5)
                xy = x+1j*y
                ## x-pbc
                x1 = x + 1.0 
                y1 = y
                xy1= x1+1j*y1
                ## y-pbc
                x2 = x
                y2 = y + 1.0 
                xy2= x2+1j*y2
                ## Extra phase for y-pbc
                phase = 2*np.pi*x2*NbrFlux
                
                ###initialize to make sure complex###
                wfval=wfval1=wfval2=1j*1.0

                #print('----------')                
                wfval =WFn.WFValueSLL(xy ,Momenta)
                #print('----------')
                wfval1=WFn.WFValueSLL(xy1,Momenta)
                #print('----------')
                wfval2=WFn.WFValueSLL(xy2,Momenta)*np.exp(1j*phase)
                #print('----------')


                if np.abs(wfval-wfval1) > self.Threshold or np.abs(wfval-wfval2) > self.Threshold:
                    print("\nERROR: wfvalSLL does not have periodic boundary conditions")
                    print("NbrFlux:",NbrFlux)
                    print('tau,Lx:',tau,Lx)
                    print('k,x,y:',Momenta,x,y)
                    print("phase: ",phase)
                    print("wfval: ",wfval)
                    print("wfval1:",wfval1)
                    print("wfval2:",wfval2)

                    print("log(wfval1/wfval): ",np.log(wfval1/wfval))
                    print("log(wfval2/wfval): ",np.log(wfval2/wfval))
                    print("log(wfval2/wfval)/phase: ",np.log(wfval2/wfval)/phase)

                    self.assertTrue(False)

        self.assertTrue(True)


    def test_pbc_High_LL(self):
        """ Test that periodic boundary conditions works for single particles in TLL in tau gauge"""
        MaxFlux=10
        self.Threshold = 1e-08
        MaxLL=10
        for LLs in range(1,MaxLL+1):
            for NbrFlux in range(1,MaxFlux+1):
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                Lx=np.sqrt(2*np.pi*NbrFlux/np.imag(tau))
                WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=LLs)
                for Momenta in range(NbrFlux):
                    x = random.uniform(-.5,.5)
                    y = random.uniform(-.5,.5)
                    xy = x+1j*y
                    ## x-pbc
                    x1 = x + 1.0 
                    y1 = y
                    xy1= x1+1j*y1
                    ## y-pbc
                    x2 = x
                    y2 = y + 1.0 
                    xy2= x2+1j*y2
                    ## Extra phase for y-pbc
                    phase = 2*np.pi*x2*NbrFlux
                    
                    ###initialize to make sure complex###
                    wfval=wfval1=wfval2=1j*1.0
                    
                    #print('----------')                
                    wfval =WFn.WFValueGeneral(xy ,Momenta)
                    #print('----------')
                    wfval1=WFn.WFValueGeneral(xy1,Momenta)
                    #print('----------')
                    wfval2=WFn.WFValueGeneral(xy2,Momenta)*np.exp(1j*phase)
                    #print('----------')
                    
                    
                    if np.abs(wfval/wfval1-1) > self.Threshold or np.abs(wfval/wfval2-1) > self.Threshold:
                        print("\nERROR: wfvalnLL does not have periodic boundary conditions")
                        print("NbrFlux,LL:",NbrFlux,LLs)
                        print('tau,Lx:',tau,Lx)
                        print('k,x,y:',Momenta,x,y)
                        print("phase: ",phase)
                        print("wfval: ",wfval)
                        print("wfval1:",wfval1)
                        print("wfval2:",wfval2)
                        print("|wfval-wfval1|:",np.abs(wfval-wfval1))
                        print("|wfval-wfval2|:",np.abs(wfval-wfval2))

                        print("log(wfval1/wfval): ",np.log(wfval1/wfval))
                        print("log(wfval2/wfval): ",np.log(wfval2/wfval))
                        print("log(wfval2/wfval)/phase: ",np.log(wfval2/wfval)/phase)

                        self.assertTrue(False)

        self.assertTrue(True)

    def test_numpy_array_High_LL(self):
        import time
        """ Test that numpy arrays can be used and that return is a numpy array. This is note reallt a test, but more benchmarking, to see how long time it takes to sample a certain number of states..."""
        NMC=100000
        self.Threshold = 1e-08
        MaxLL=4
        NbrFlux=10
        for LLs in range(0,MaxLL+1):
            print("LL:",LLs)
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=LLs)
            for Momenta in range(NbrFlux):
                t0=time.clock()                    
                print("Momenta:",Momenta)
                x = np.random.rand(NMC)
                y = np.random.rand(NMC)
                xy = x+1j*y
                #print('----------')                
                wfval =WFn.WFValue(xy ,Momenta)
                print('Took:',time.clock()-t0,' second of wall time')
        self.assertTrue(True)


    def test_array_same_as_scalar(self):
        """ Test a call using numpy arrays gives the same as a call using repeated scalar arguments"""
        import time
        NMC=100
        self.Threshold = 1e-08
        MaxLL=4
        NbrFlux=10
        for LLs in range(0,MaxLL+1):
            ##print("LL:",LLs)
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            WFn=FQHTorusWF(NbrFlux, Tau=tau, LL=LLs)
            for Momenta in range(NbrFlux):
                t0=time.clock()                    
                ##print("Momenta:",Momenta)
                x = np.random.rand(NMC)
                y = np.random.rand(NMC)
                xyList = x+1j*y
                #print('----------')                
                wfList1 =WFn.WFValue(xyList ,Momenta)
                ###Compare the values to that you get my callingwith on value at a time
                wfList2 = np.array([WFn.WFValue(xy ,Momenta) for xy in xyList],dtype=np.complex128)

                if np.sum(np.abs(wfList1 - wfList2)) > self.Threshold:
                    
#                    [ print(wfList1[n],wfList2[n],wfList1[n]-wfList2[n]) for n in range(NMC)]
                    print('Took:',time.clock()-t0,' second of wall time')
                    self.assertTrue(False)
        
if __name__ == '__main__':
    unittest.main()
