#! /usr/bin/env python
from __future__ import print_function

"""
 Unit tests to for reflection symmetry code. Tests that the eigenvalues retrieved are correct for a few small systems. 
"""
from hammer.FQHTorusJKWF import FQHTorusJKWF
import hammer.FQHTorusJKWF

import unittest
import random
import numpy as np
import petsc4py.PETSc as PETSc


class TestJKCF(unittest.TestCase):
    """
    Test suite for single particle orbitals.
    """

    def setUp(self):
        self.Threshold = 1e-09

    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_pbc_naive_CF(self):
        """ Test that periodic boundary conditions works for the primary particles in unprojected CF in tau gauge"""
        MaxFlux=5
        MaxNe=5
        MaxLL=2
    
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            for NbrNe in range(1,MaxNe+1):
                for LL in range(0,MaxLL+1):
                    tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                    for Momenta in range(NbrFlux):
                        ###Initialize the wave function (all momenta arr the same since we only test one of the factors anyway)
                        WFN=FQHTorusJKWF(NbrFlux,[Momenta]*NbrNe,[LL]*NbrNe,tau)
                        XYList,XYList1,XYList2=set_xylist(NbrNe)
                        XYList1[0]+=1.0
                        XYList2[0]+=1.0*1j

                        x=np.real(XYList[0])
                        y=np.imag(XYList[0])
                        ZList=np.real(XYList[1:])+tau*np.imag(XYList[1:])
                        #print("XYList:",XYList)
                        Gauge=np.exp(1j*2*np.pi*((NbrNe-1)+NbrFlux)*x)
                        #print("Gauge:",Gauge)
                        Zphase=np.exp(-1j*2*np.pi*np.sum(ZList))

                        #print("Z-pahse:",Zphase)
                        ## Extra phase for y-pbc
                        phase1 = (-1.0+0j)**(NbrNe-1)
                        phase2 = ((-1.0+0j)**(NbrNe-1)
                                  *Gauge*Zphase)
                        ##Initialize to make sure complex
                        wfval=wfval1=wfval2=1j*1.0
                        wfval =WFN.JK_SP_Unproj(XYList ,0,0)
                        wfval1=WFN.JK_SP_Unproj(XYList1,0,0)*phase1
                        wfval2=WFN.JK_SP_Unproj(XYList2,0,0)*phase2


                        if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                            print("\nERROR: Unprojected CF does not have correct boundary conditions")
                            print("NbrFlux,NbrNe:",NbrFlux,NbrNe)
                            print('tau:',tau)
                            print('k,x,y:',Momenta,x,y)
                            print('XYList: ',XYList)
                            print('XYList1:',XYList1)
                            print('XYList2:',XYList2)
                            print("phase1/(2*pi): ",np.log(phase1)/(1j*2*np.pi))
                            print("phase2/(2*pi): ",np.log(phase2)/(1j*2*np.pi))
                            print("wfval: ",wfval)
                            print("wfval1:",wfval1)
                            print("wfval2:",wfval2)
                            print("wfval1/wfval",wfval1/wfval)
                            print("wfval2/wfval:",wfval2/wfval)
                            print("log(wfval1/wfval)/(2*pi): ",np.log(wfval1/wfval)/(2*np.pi))
                            print("log(wfval2/wfval)/(2*pi): ",np.log(wfval2/wfval)/(2*np.pi))
                            self.assertTrue(False,"Wave functions are not the same")


    def test_pbc_rest_naive_CF(self):
        """ Test that periodic boundary conditions works for the auxiallry particles for unprojected CF in tau gauge"""
        MaxFlux=5
        MaxNe=5
        MaxLL=2
    
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            for NbrNe in range(1,MaxNe+1):
                for LL in range(0,MaxLL+1):
                    tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                    for Momenta in range(NbrFlux):
                        WFN=FQHTorusJKWF(NbrFlux,[Momenta]*NbrNe,[LL]*NbrNe,tau)
                        XYList,XYList1,XYList2=set_xylist(NbrNe)

                        ###Set frst coordinate
                        x = np.real(XYList[0])
                        y = np.imag(XYList[0])
                        z = x + tau*y
                        ###Initalize the other coordinates as well
                        ###Initalize the other coordinates as well
                        ##print("XYList:",XYList)
                        for NeNum in range(1,NbrNe):
                            XYList1=np.array(XYList)
                            XYList2=np.array(XYList)
                            ## x-pbc
                            XYList1[NeNum]=XYList1[NeNum]+1.0
                            ## y-pbc
                            XYList2[NeNum]=XYList2[NeNum]+1j
                            #print("XYList:",XYList)
                            #print("XYList1:",XYList1)
                            #print("XYLis2:",XYList2)
                            Gauge=np.exp(1j*2*np.pi*np.real(XYList[NeNum]))
                            #print("Gauge:",Gauge)
                            #print("LogGauge:",np.log(Gauge))

                            Zphase=np.exp(-1j*2*np.pi*z)
                            #print("Z-pahse:",Zphase)
                            #print("log(Z-pahse):",np.log(Zphase))
                            ## Extra phase for y-pbc
                            phase1 = (-1.0+0j)
                            phase2 = ((-1.0+0j)*Gauge*Zphase)
                            ##Initialize to make sure complex
                            wfval=wfval1=wfval2=1j*1.0
                            wfval =WFN.JK_SP_Unproj(XYList ,0,0)
                            wfval1=WFN.JK_SP_Unproj(XYList1,0,0)*phase1
                            wfval2=WFN.JK_SP_Unproj(XYList2,0,0)*phase2
                            if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                                print("\nERROR: Unprojected CF does not have correct auxillary boundary conditions")
                                print("NbrFlux,NbrNe:",NbrFlux,NbrNe)
                                print('tau:',tau)
                                print('k,x,y:',Momenta,x,y)
                                print('XYList: ',XYList)
                                print('XYList1:',XYList1)
                                print('XYList2:',XYList2)
                                print("phase1/(2*pi): ",np.log(phase1)/(1j*2*np.pi))
                                print("phase2/(2*pi): ",np.log(phase2)/(1j*2*np.pi))
                                print("wfval: ",wfval)
                                print("wfval1:",wfval1)
                                print("wfval2:",wfval2)
                                print("wfval1/wfval",wfval1/wfval)
                                print("wfval2/wfval:",wfval2/wfval)
                                print("log(wfval1/wfval)/(2*pi): ",np.log(wfval1/wfval)/(2*np.pi))
                                print("log(wfval2/wfval)/(2*pi): ",np.log(wfval2/wfval)/(2*np.pi))
                                self.assertTrue(False,"Wave functions are not the same")                            

    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_pbc_JK_CF(self):
        """ Test that periodic boundary conditions works for the primary particles in projected CF in tau gauge"""
        MaxFlux=5
        MaxNe=5
        MaxLL=2
    
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            for NbrNe in range(1,MaxNe+1):
                for LL in range(0,MaxLL+1):
                    tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                    for Momenta in range(NbrFlux):
                        LLlist=[LL]+range(NbrNe-1)
                        WFN=FQHTorusJKWF(NbrFlux,[Momenta]*NbrNe,LLlist,tau)
                        XYList,XYList1,XYList2=set_xylist(NbrNe)
                        XYList1[0]+=1.0
                        XYList2[0]+=1.0*1j
                        x=np.real(XYList[0])
                        y=np.imag(XYList[0])
                        ZList=np.real(XYList[1:])+tau*np.imag(XYList[1:])
                        Gauge=np.exp(1j*2*np.pi*((NbrNe-1)+NbrFlux)*x)
                        #print("Gauge:",Gauge)
                        Zphase=np.exp(-1j*2*np.pi*np.sum(ZList))

                        [gamma,delta]=WFN.gammadelta()

                        gammaphase=np.exp(1j*2*np.pi*LL*gamma)
                        deltaphase=np.exp(-1j*2*np.pi*delta*(NbrNe-1)*LL)

                        #print("Z-pahse:",Zphase)
                        ## Extra phase for y-pbc
                        phase1 = (-1.0+0j)**(NbrNe-1)
                        phase2 = ((-1.0+0j)**(NbrNe-1)
                                  *Gauge*Zphase*gammaphase*deltaphase)
                        ##Initialize to make sure complex
                        wfval=wfval1=wfval2=1j*1.0
                        wfval =WFN.JK_SP_Proj(XYList ,0,0)
                        wfval1=WFN.JK_SP_Proj(XYList1,0,0)*phase1
                        wfval2=WFN.JK_SP_Proj(XYList2,0,0)*phase2

                        if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                            print("\nERROR: Unprojected CF does not have correct boundary conditions")
                            print("NbrFlux,NbrNe:",NbrFlux,NbrNe)
                            print('tau,ll:',tau,LL)
                            print('LLlist:',LLlist)
                            print('k,x,y:',Momenta,x,y)
                            print("gamma,delta:",gamma,delta)
                            print('XYList: ',XYList)
                            print('XYList1:',XYList1)
                            print('XYList2:',XYList2)
                            print("phase1/(2*pi): ",np.log(phase1)/(1j*2*np.pi))
                            print("phase2/(2*pi): ",np.log(phase2)/(1j*2*np.pi))
                            print("wfval: ",wfval)
                            print("wfval1:",wfval1)
                            print("wfval2:",wfval2)
                            print("wfval1/wfval",wfval1/wfval)
                            print("wfval2/wfval:",wfval2/wfval)
                            print("log(wfval1/wfval)/(2*pi): ",np.log(wfval1/wfval)/(2*np.pi))
                            print("log(wfval2/wfval)/(2*pi): ",np.log(wfval2/wfval)/(2*np.pi))
                            self.assertTrue(False,"Wave functions are not the same")


    def test_pbc_rest_JK_CF(self):
        """ Test that periodic boundary conditions works for the auxiallry particles for Projected CF in tau gauge"""
        MaxFlux=5
        MaxNe=5
        MaxLL=2
    
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            for NbrNe in range(1,MaxNe+1):
                for LL in range(0,MaxLL+1):
                    tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                    for Momenta in range(NbrFlux):
                        LLlist=[LL]+range(NbrNe-1)
                        WFN=FQHTorusJKWF(NbrFlux,[Momenta]*NbrNe,LLlist,tau)
                        XYList,XYList1,XYList2=set_xylist(NbrNe)
                        XYList1[0]+=1.0
                        XYList2[0]+=1.0*1j
                        x=np.real(XYList[0])
                        y=np.imag(XYList[0])
                        z=x+tau*y
                        ##print("XYList:",XYList)
                        for NeNum in range(1,NbrNe):
                            XYList1=np.array(XYList)
                            XYList2=np.array(XYList)
                            ## x-pbc
                            XYList1[NeNum]=XYList1[NeNum]+1.0
                            ## y-pbc
                            XYList2[NeNum]=XYList2[NeNum]+1j
                            i2pi=1j*2*np.pi
                            Gauge=np.exp(i2pi*np.real(XYList[NeNum]))
                            Zphase=np.exp(-i2pi*z)

                            [gamma,delta]=WFN.gammadelta()
                            deltaphase=np.exp(i2pi*LL*delta)

                            #print("Z-pahse:",Zphase)
                            #print("log(Z-pahse):",np.log(Zphase))
                            ## Extra phase for y-pbc
                            phase1 = (-1.0+0j)
                            phase2 = ((-1.0+0j)*Gauge*Zphase*deltaphase)
                            ##Initialize to make sure complex
                            wfval=wfval1=wfval2=1j*1.0
                            wfval =WFN.JK_SP_Proj(XYList ,0,0)
                            wfval1=WFN.JK_SP_Proj(XYList1,0,0)*phase1
                            wfval2=WFN.JK_SP_Proj(XYList2,0,0)*phase2

                            if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                                print("\nERROR: JKProjected CF does not have correct auxillary boundary conditions")
                                print("NbrFlux,NbrNe:",NbrFlux,NbrNe)
                                print('tau:',tau)
                                print('k,x,y:',Momenta,x,y)
                                print('XYList: ',XYList)
                                print('XYList1:',XYList1)
                                print('XYList2:',XYList2)
                                print("log(phase1)/(2*pi): ",np.log(phase1)/(i2pi))
                                print("log(phase2)/(2*pi): ",np.log(phase2)/(i2pi))
                                print("wfval: ",wfval)
                                print("wfval1:",wfval1)
                                print("wfval2:",wfval2)
                                print("wfval1/wfval",wfval1/wfval)
                                print("wfval2/wfval:",wfval2/wfval)
                                print("log(wfval1/wfval)/(2*pi): ",np.log(wfval1/wfval)/(2*np.pi))
                                print("log(wfval2/wfval)/(2*pi): ",np.log(wfval2/wfval)/(2*np.pi))
                                self.assertTrue(False,"Wave functions are not the same")            

    def test_T1_T2_JK_CF_wfn(self):
        """ Test that periodic boundary T1 T2 behaviour works for the projected  CF:s in tau gauge"""
        MaxFlux=5
        MaxLL=2
        i2pi=1j*2*np.pi
        
        ###We here assume a fully filled landau level such that
        #Ne = (LL+1)*NbrFlux
        #Ns = NbrFlux + 2*Ne = NbrFlux*(1+ 2*(LL+1))
        self.Threshold = 1e-10
        for LL in range(0,MaxLL+1):
            for NbrFlux in range(1,MaxFlux+1):
                #print("FLux,LL:",NbrFlux,LL)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                NbrNe = (LL+1)*NbrFlux
                NbrNs = NbrFlux + 2*NbrNe
                nu = NbrNe/(1.0*NbrNs)
                denom=(1+2*(LL+1))
                numerator=(LL+1)
                mom_shift=denom-2*numerator

                for Momenta in range(NbrFlux):
                    LLlist=[LL]+range(NbrNe-1)
                    WFN=FQHTorusJKWF(NbrFlux,[Momenta]*NbrNe,LLlist,tau)
                    ###Ther is a shift in momentum for T2
                    WFN2=FQHTorusJKWF(NbrFlux,[Momenta-mom_shift]*NbrNe,LLlist,tau)
                    move=denom/(1.0*NbrNs)
                    XYList,XYList1,XYList2=set_xylist(NbrNe)
                    x=np.real(XYList[0])
                    y=np.imag(XYList[0])
                    ###Move all the coorinates
                    XYList1+=move
                    XYList2+=1j*move

                    Ysum=np.sum(np.imag(XYList))

                    ZList=np.real(XYList[1:])+tau*np.imag(XYList[1:])
                    #print("XYList:",XYList)
                    #print("ZList:",ZList)
                    #print("LLlist:",LLlist)
                    Gauge=np.exp(i2pi*((NbrNe-1)+NbrFlux)*x)
                    #print("Gauge:",Gauge)
                    Zphase=np.exp(-i2pi*np.sum(ZList))
                    
                    [gamma,delta]=WFN.gammadelta()                    

                    gammaphase=np.exp(i2pi*LL*gamma)
                    deltaphase=np.exp(-i2pi*delta*(NbrNe-1)*LL)
                    
                    #print("Z-pahse:",Zphase)
                    ## Extra phase for y-pbc
                    phase1 = np.exp(-i2pi*Momenta*move)
                    phase2 = (np.exp(i2pi*NbrFlux*move*x)
                              *np.exp(i2pi*move*LL*gamma)
                              *np.exp(-i2pi*tau*(NbrNe-1)*move**2)
                              *np.exp(-i2pi*tau*(NbrNe-2)*move*y)
                              *np.exp(-i2pi*tau*move*Ysum))
                    ##Initialize to make sure complex
                    wfval=wfval1=wfval2=1j*1.0
                    wfval =WFN.JK_SP_Proj(XYList ,0,0)
                    wfval1=WFN.JK_SP_Proj(XYList1,0,0)*phase1
                    wfval2=WFN2.JK_SP_Proj(XYList2,0,0)*phase2
                    
                    
                    if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                        Text="ERROR: Unprojected CF does not have correct boundary conditions"
                        Text+="\nNbrFlux,Ns,NbrNe:"+str((NbrFlux,NbrNs,NbrNe))
                        Text+="\nnu:"+str(nu)
                        Text+="\ntau,ll:"+str((tau,LL))
                        Text+="\nk,x,y:"+str((Momenta,x,y))
                        Text+="\ngamma,delta:"+str((gamma,delta))
                        Text+="\nlog(phase1)/(2*pi): "+str(np.log(phase1)/(i2pi))
                        Text+="\nlog(phase2)/(2*pi): "+str(np.log(phase2)/(i2pi))
                        Text+="\nwfval: "+str(wfval)
                        Text+="\nwfval1:"+str(wfval1)
                        Text+="\nwfval2:"+str(wfval2)
                        Text+="\nwfval1/wfval"+str(wfval1/wfval)
                        Text+="\nwfval2/wfval:"+str(wfval2/wfval)
                        Text+="\nlog(wfval1/wfval)/(2*pi): "+str(np.log(wfval1/wfval)/(2*np.pi))
                        Text+="\nlog(wfval2/wfval)/(2*pi): "+str(np.log(wfval2/wfval)/(2*np.pi))
                        print(Text)
                        self.assertTrue(False,Text+"\nWave functions are not the same")


    def test_T1_T2_CoM(self):
        """ Test that periodic boundary T1 T2 behaviour works for the center of mass piece"""
        MaxFlux=5
        MaxLL=2
        i2pi=1j*2*np.pi

        ###We here assume a fully filled landau level such that
        #Ne = (LL+1)*NbrFlux
        #Ns = NbrFlux + 2*Ne = NbrFlux*(1+ 2*(LL+1))
        self.Threshold = 1e-10
        for LL in range(0,MaxLL+1):
            for NbrFlux in range(1,MaxFlux+1):
                #print("FLux,LL:",NbrFlux,LL)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                NbrNe = (LL+1)*NbrFlux
                NbrNs = NbrFlux + 2*NbrNe
                nu = NbrNe/(1.0*NbrNs)
                denom=(1+2*(LL+1))
                numerator=(LL+1)

                move=denom/(1.0*NbrNs)
                LLlist=[LL]+range(NbrNe-1)
                WFN=FQHTorusJKWF(NbrFlux,[0]*NbrNe,[0]*NbrNe,tau)
                XYList,XYList1,XYList2=set_xylist(NbrNe)
                ###Move all the coorinates
                XYList1+=move
                XYList2+=1j*move
                XSum=np.sum(np.real(XYList))
                ZSum=np.sum(np.real(XYList))+tau*np.sum(np.imag(XYList))

                ## Extra phase for y-pbc
                phase1 = 1.0#2*np.exp(-i2pi*move)
                phase2a = np.exp(i2pi*2*move*XSum)
                phase2b = np.exp(i2pi*tau*(numerator**2-NbrNe*move**2))
                phase2c = np.exp(i2pi*2*(numerator-move)*ZSum)

                phase2 = phase2a*phase2b*phase2c

                
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0
                wfval =WFN.CoMfunction(XYList)
                wfval1=WFN.CoMfunction(XYList1)*phase1
                wfval2=WFN.CoMfunction(XYList2)*phase2
                    
                    
                if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                    Text="ERROR: Unprojected CF does not have correct boundary conditions"
                    Text+="\nNbrFlux,Ns,NbrNe:"+str((NbrFlux,NbrNs,NbrNe))
                    Text+="\nnu,move:"+str((nu,move))
                    Text+="\ntau,ll:"+str((tau,LL))
                    Text+="\nXYList: "+str(XYList)
                    Text+="\nXYList1:"+str(XYList1)
                    Text+="\nXYList2:"+str(XYList2)
                    Text+="\nlog(phase1)/(2*pi): "+str(np.log(phase1)/(i2pi))
                    Text+="\nlog(phase2)/(2*pi): "+str(np.log(phase2)/(i2pi))
                    Text+="\nlog(phase2a)/(2*pi): "+str(np.log(phase2a)/(i2pi))
                    Text+="\nlog(phase2b)/(2*pi): "+str(np.log(phase2b)/(i2pi))
                    Text+="\nlog(phase2c)/(2*pi): "+str(np.log(phase2c)/(i2pi))
                    Text+="\nwfval: "+str(wfval)
                    Text+="\nwfval1:"+str(wfval1)
                    Text+="\nwfval2:"+str(wfval2)
                    Text+="\nwfval1/wfval"+str(wfval1/wfval)
                    Text+="\nwfval2/wfval:"+str(wfval2/wfval)
                    Text+="\nlog(wfval1/wfval)/(2*pi): "+str(np.log(wfval1/wfval)/(2*np.pi))
                    Text+="\nlog(wfval2/wfval)/(2*pi): "+str(np.log(wfval2/wfval)/(2*np.pi))
                    print(Text)
                    self.assertTrue(False,Text+"\nWave functions are not the same")

                            
                                
    def test_n0_JK_CF_same_as_naive_CF(self):
        """ Test that n0 Jain Kamilla is the same as the JKProjected version, since the PLLL is an identity operation"""
        MaxFlux=5
        MaxNe=5
        Values=10
        self.Threshold = 1e-10
        for NbrFlux in range(1,MaxFlux+1):
            for NbrNe in range(1,MaxNe+1):
                for Momenta in range(NbrFlux):
                    for indx in range(Values): ##Number of values to try   
                        tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                        WFN=FQHTorusJKWF(NbrFlux,[Momenta]*NbrNe,range(NbrNe),tau)
                        XYList,XYList1,XYList2=set_xylist(NbrNe)
                        x=np.real(XYList[0])
                        y=np.imag(XYList[0])
                        wfvalJK=wfvalUP=1j*1.0
                        wfvalJK =WFN.JK_SP_Proj(XYList,0,0)
                        wfvalUP =WFN.JK_SP_Unproj(XYList,0,0)

                        if np.abs(wfvalJK/wfvalUP-1) > self.Threshold:
                            print("\nERROR: N=0 JK Projected wfn is not the same as Unprojected CF")
                            print("NbrFlux,NbrNe:",NbrFlux,NbrNe)
                            print('tau:',tau)
                            print('k,x,y:',Momenta,x,y)
                            print("wfvalJP:",wfvalJK)
                            print("wfvalUP:",wfvalUP)
                            print("wfvalJK/wfvalUP",wfvalJK/wfvalUP)
                            print("log(wfvalJK/wfvalUP)/(2*pi): ",np.log(wfvalJK/wfvalUP)/(2*np.pi))
                            self.assertTrue(False,"Wave functions are not the same")

    #@unittest.skip("skipping for now to get to subsquent tests faster")
    
    def test_pbc_product_JK_CF(self):
        """ Test that periodic boundary conditions works for product of all the CF-wave functions """
        MaxFluxLL=5
        MaxLL=2
        
        self.Threshold = 1e-10
        self.WfnThreshold = 1e-30
        for NbrFluxLL in range(1,MaxFluxLL+1):
            for LLs in range(0,MaxLL+1):
                print("FLL,LLs:",NbrFluxLL,LLs)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                TotNe=NbrFluxLL*(LLs+1)
                XYList,XYList1,XYList2=set_xylist(TotNe)
                [Momenta,LLs]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
                #print("Momenta:",Momenta)
                #print("LLs:",LLs)
                move=1.0
                WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLs,tau)
                TotFlux=NbrFluxLL+2*TotNe
                for NeNum in range(TotNe):
                    XYList1=np.array(XYList)
                    XYList2=np.array(XYList)
                    XYList1[NeNum]+=move
                    XYList2[NeNum]+=1j*move
                    i2pi=1j*2*np.pi
                    #print("XYList:",XYList)
                    Gauge=np.exp(i2pi*TotFlux*np.real(XYList[NeNum]))
                    #Gauge=1.0
                    #print("Gauge:",Gauge)
                    #print("Log Gauge:",np.log(Gauge))
                    ## Extra phase for y-pbc
                    phase1 = 1.0
                    phase2 = Gauge
                    ##Initialize to make sure complex
                    wfval=wfval1=wfval2=1j*1.0
                    wfval =WFN.WFValue(XYList,det=False)
                    wfval1=WFN.WFValue(XYList1,det=False)*phase1
                    wfval2=WFN.WFValue(XYList2,det=False)*phase2

                    ##Are the wave fuctions different?
                    if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                        ###IF the wave functions are differnt also check if they are non-zero
                        if TestWfnZero(wfval,wfval1,wfval2,self.WfnThreshold):
                            print_error(NbrFluxLL,LLs,TotFlux,
                                        TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                                "Projected product CF is zero",self)
                        else:
                            print_error(NbrFluxLL,LLs,TotFlux,
                                        TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                                "Projected product CF does not have correct boundary conditions",self)

    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_pbc_det_JK_CF(self):
        """ Test that periodic boundary conditions works for determinant of all the CF-wave functions, and that the determinant mostly is not zero"""
        MaxFluxLL=5
        MaxLL=2
        
        self.Threshold = 1e-10
        self.WfnThreshold = 1e-30
        for NbrFluxLL in range(2,MaxFluxLL+1):
            for LLs in range(0,MaxLL+1):
                print("FLL,LLs:",NbrFluxLL,LLs)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                TotNe=NbrFluxLL*(LLs+1)

                XYList=get_max_separated(TotNe)
                XYList1=np.array(XYList)
                XYList2=np.array(XYList)
                [Momenta,LLs]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
                #print("XYList:",XYList)
                #print("Momenta:",Momenta)
                #print("LLs:",LLs)
                move=1.0
                WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLs,tau)
                TotFlux=NbrFluxLL+2*TotNe
                for NeNum in range(TotNe):
                    XYList1=np.array(XYList)
                    XYList2=np.array(XYList)
                    XYList1[NeNum]+=move
                    XYList2[NeNum]+=1j*move
                    i2pi=1j*2*np.pi
                    #print("XYList:",XYList)
                    Gauge=np.exp(i2pi*TotFlux*np.real(XYList[NeNum]))
                    #Gauge=1.0
                    #print("Gauge:",Gauge)
                    #print("Log Gauge:",np.log(Gauge))
                    ## Extra phase for y-pbc
                    phase1 = 1.0
                    phase2 = Gauge
                    ##Initialize to make sure complex
                    wfval=wfval1=wfval2=1j*1.0
                    wfval =WFN.WFValue(XYList,det=True)
                    wfval1=WFN.WFValue(XYList1,det=True)*phase1
                    wfval2=WFN.WFValue(XYList2,det=True)*phase2

                    ##Are the wave fuctions different?
                    if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                        ###IF the wave functions are differnt also check if they are non-zero
                        if TestWfnZero(wfval,wfval1,wfval2,self.WfnThreshold):
                            print_error(NbrFluxLL,LLs,TotFlux,
                                        TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                                "Projected determinant CF is zero",self)
                        else:
                            print_error(NbrFluxLL,LLs,TotFlux,
                                        TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                                "Projected determiant CF does not have correct boundary conditions",self)

                            
    def test_antisym_det_JK_CF(self):
        """ Test that the determiant is antisymmetric for determinant of all the CF-wave functions"""
        MaxFluxLL=4
        MaxLL=2
        
        self.Threshold = 1e-10
        self.WfnThreshold = 1e-30
        for NbrFluxLL in range(2,MaxFluxLL+1):
            for LLs in range(0,MaxLL+1):
                print("FLL,LLs:",NbrFluxLL,LLs)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                TotNe=NbrFluxLL*(LLs+1)

                XYList=get_max_separated(TotNe)
                XYList1=np.array(XYList)
                XYList2=np.array(XYList)
                [Momenta,LLs]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
                #print("XYList:",XYList)
                #print("Momenta:",Momenta)
                #print("LLs:",LLs)
                move=1.0
                WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLs,tau)
                TotFlux=NbrFluxLL+2*TotNe
                for Ne1 in range(TotNe-1):
                    for Ne2 in range(Ne1+1,TotNe):
                        print("Ne1,Ne2",Ne1+1,Ne2+1,"of",TotNe)
                        XYList1=np.array(XYList)
                        XYList1[Ne1]=XYList[Ne2]
                        XYList1[Ne2]=XYList[Ne1]
                        phase1 = -1.0
                        phase2 = 1.0
                        ##Initialize to make sure complex
                        wfval=wfval1=wfval2=1j*1.0
                        wfval =WFN.WFValue(XYList,det=True)
                        wfval1=WFN.WFValue(XYList1,det=True)*phase1
                        wfval2=wfval

                        ##Are the wave fuctions different?
                        if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                            ###IF the wave functions are differnt also check if they are non-zero
                            if TestWfnZero(wfval,wfval1,wfval2,self.WfnThreshold):
                                print_error(NbrFluxLL,LLs,TotFlux,
                                            TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                                            "Projected determinant CF is zero",self)
                            else:
                                print_error(NbrFluxLL,LLs,TotFlux,
                                            TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                                            "Projected determiant CF is not antrisymmetric between electrons"+str(Ne1)+"and"+str(Ne2)+"!",self)

                            

    def test_T1_T2_pbc_for_1_CF(self):
        """ Test that the right T1^q and T2^q sectors are populated for product of all the CF-wave functions """
        MaxFluxLL=5
        MaxLL=3
        i2pi=1j*2*np.pi    
        self.Threshold = 1e-10
        self.WfnThreshold = 1e-30
        for NbrFluxLL in range(1,MaxFluxLL+1):
            LLs=0
            print("FLL:",NbrFluxLL)
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            TotNe=NbrFluxLL
            TotFlux=NbrFluxLL

            XYList=get_max_separated(TotNe)
            XYList1=np.array(XYList)
            XYList2=np.array(XYList)

            [Momenta,LLList]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
            #print("Momenta:",Momenta)
            #print("LLList:",LLList)
            WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLList,tau,fluxpairs=0)
  
            T1QPhase=(-1.0+1j*0)**(NbrFluxLL-1)

            ####Denominator
            denom=1
            nomin=1
            move=1/(1.0*TotFlux)
            XYList1+=move
            XYList2+=move*1j

            #print("denom:",denom)
            #print("i2pi:",i2pi)
            #print("TotFlux:",TotFlux)
            #print("move:",move)

            Gauge=np.exp(i2pi*np.sum(np.real(XYList)))
            #print("Gauge:",Gauge)

            ## Extra phase for y-pbc
            phase1 = 1.0*T1QPhase
            phase2 = Gauge
            ##Initialize to make sure complex
            wfval=wfval1=wfval2=1j*1.0
            wfval =WFN.WFValue(XYList,det=False)
            wfval1=WFN.WFValue(XYList1,det=False)*phase1
            XYList2Rotate=np.concatenate((XYList2[1:],[XYList2[0]]))
            wfval2=WFN.WFValue(XYList2Rotate,det=False)*phase2
            
            #print("wfval: ",wfval)
            #print("wfval1:",wfval1)
            #print("wfval2:",wfval2)

            ##Are the wave fuctions different?
            if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                ###IF the wave functions are differnt also check if they are non-zero
                if TestWfnZero(wfval,wfval1,wfval2,self.WfnThreshold):
                    print_error(NbrFluxLL,LLs,TotFlux,
                                TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                        "Projected product CF is zero",self)
                else:
                    print_error(NbrFluxLL,LLs,TotFlux,
                                TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                        "Projected product CF does not have correct boundary conditions",self)

                            

    def test_T1_T2_pbc_product_JK_CF(self):
        """ Test that the right T1^q and T2^q sectors are populated for product of all the CF-wave functions """
        MaxFluxLL=5
        MaxLL=3
        i2pi=1j*2*np.pi    
        self.Threshold = 1e-10
        self.WfnThreshold = 1e-30
        for NbrFluxLL in range(1,MaxFluxLL+1):
            for LLs in range(0,MaxLL+1):
                print("FLL,LLs:",NbrFluxLL,LLs)
                ####Denominator
                denom=1+2*(LLs+1)
                numerator=LLs+1

                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                TotNe=NbrFluxLL*(LLs+1)
                mom_shift=denom-2*numerator
                XYList,XYList1,XYList2=set_xylist(TotNe)
                [Momenta,LLList]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
                #LLList=LLList*0
                #print("Momenta:",Momenta)
                #print("LLList:",LLList)
                WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLList,tau)
                WFN_shift=FQHTorusJKWF(NbrFluxLL,Momenta-mom_shift,LLList,tau)
                
                TotFlux=NbrFluxLL+2*TotNe
                T1QPhase=(-1.0+1j*0)**((LLs+1)*(NbrFluxLL-1))

                
                move=denom/(1.0*TotFlux)
                
                XYList1+=move
                XYList2+=move*1j

                #print("denom:",denom)
                #print("i2pi:",i2pi)
                #print("TotFlux:",TotFlux)
                #print("move:",move)
                
                Gauge=np.exp(i2pi*denom*np.sum(np.real(XYList)))
                #print("Gauge:",Gauge)

                ## Extra phase for y-pbc
                phase1 = 1.0*T1QPhase
                phase2 = Gauge
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0
                wfval =WFN.WFValue(XYList,det=False)
                wfval1=WFN.WFValue(XYList1,det=False)*phase1
                wfval2=WFN_shift.WFValue(XYList2,det=False)*phase2
                #print("wfval: ",wfval)
                #print("wfval1:",wfval1)
                #print("wfval2:",wfval2)
                
                ##Are the wave fuctions different?
                if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                    ###IF the wave functions are differnt also check if they are non-zero
                    if TestWfnZero(wfval,wfval1,wfval2,self.WfnThreshold):
                        print_error(NbrFluxLL,LLs,TotFlux,
                                    TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                            "Projected product CF is zero",self)
                    else:
                        print_error(NbrFluxLL,LLs,TotFlux,
                                    TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                            "Projected product CF does not have correct boundary conditions",self)


                        
    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_T1_T2_pbc_det_JK_CF(self):
        """ Test that the right T1^q and T2^q sectors are populated for determiant of all the CF-wave functions """
        MaxFluxLL=5
        MaxLL=2
    
        MaxFluxLL=5
        MaxLL=3
        i2pi=1j*2*np.pi    
        self.Threshold = 1e-10
        self.WfnThreshold = 1e-30
        for NbrFluxLL in range(2,MaxFluxLL+1):
            for LLs in range(0,MaxLL+1):
                print("FLL,LLs:",NbrFluxLL,LLs)
                tau=1.0j#random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                TotNe=NbrFluxLL*(LLs+1)

                XYList=get_max_separated(TotNe)
                XYList1=np.array(XYList)
                XYList2=np.array(XYList)
                [Momenta,LLList]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
                #print("Momenta:",Momenta)
                #print("LLList:",LLList)
                WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLList,tau)
                
                TotFlux=NbrFluxLL+2*TotNe

                T1QPhase=(-1.0+1j*0)**((LLs+1)*(NbrFluxLL-1))

                ####Denominator
                denom=1+2*(LLs+1)
                nomin=LLs+1
                
                move=denom/(1.0*TotFlux)
                
                XYList1+=move
                XYList2+=move*1j

                #print("denom:",denom)
                #print("i2pi:",i2pi)
                #print("TotFlux:",TotFlux)
                #print("move:",move)
                
                Gauge=np.exp(i2pi*denom*np.sum(np.real(XYList)))
                #print("Gauge:",Gauge)

                ## Extra phase for y-pbc
                phase1 = 1.0*T1QPhase
                phase2 = Gauge*T1QPhase
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0
                wfval =WFN.WFValue(XYList,det=True)
                wfval1=WFN.WFValue(XYList1,det=True)*phase1
                wfval2=WFN.WFValue(XYList2,det=True)*phase2
                #print("wfval: ",wfval)
                #print("wfval1:",wfval1)
                #print("wfval2:",wfval2)
                
                ##Are the wave fuctions different?
                if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                    ###IF the wave functions are differnt also check if they are non-zero
                    if TestWfnZero(wfval,wfval1,wfval2,self.WfnThreshold):
                        print_error(NbrFluxLL,LLs,TotFlux,
                                    TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                            "Projected det CF is zero",self)
                    else:
                        print_error(NbrFluxLL,LLs,TotFlux,
                                    TotNe,tau,XYList,wfval,wfval1,wfval2,phase1,phase2,move,
                            "Projected det CF does not have correct boundary conditions",self)



    def test_High_Throughput(self):
        """ Test that many configurations can be put through the wfn generator.his is not really a test, but more benchmarking, to see how long time it takes to sample a certain number of states """
        import time
        MaxFluxLL=5
        MaxLL=3
        MCPoints=100
        i2pi=1j*2*np.pi    
        self.Threshold = 1e-10
        self.WfnThreshold = 1e-30
        for NbrFluxLL in range(1,MaxFluxLL+1):
            for LLs in range(0,MaxLL+1):
                t0=time.clock()
                print("")
                print("FLL,LLs:",NbrFluxLL,LLs)
                ####Denominator
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                Ne=NbrFluxLL*(LLs+1)
                [Momenta,LLList]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
                WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLList,tau)

                ### Generate XY-LIST WITH MANY indexes
                XYList=np.random.rand(MCPoints,Ne)+1j*np.random.rand(MCPoints,Ne)
                #print("XYList:\n",XYList)
                print("shape(XYList):\n",XYList.shape)
                wfval =WFN.WFValue(XYList)
                print('Took:',time.clock()-t0,' second of CPU time')


    def test_LLL_product_JK_CF(self):
        """ Test that the product of all the CF-wave functions is in Lowest landau level"""
        self.do_test_LLL_product_JK_CF(False)

    def test_LLL_det_JK_CF(self):
        """ Test that the determinant of all the CF-wave functions is in Lowest landau level"""
        self.do_test_LLL_product_JK_CF(True)
        
                
    def do_test_LLL_product_JK_CF(self,DoDet):
        """ Test that the product of all the CF-wave functions is in Lowest landau level"""
        MaxFluxLL=4
        MaxLL=2
        epsilon=1e-8
        MC=5
        ###How close is the effective number of fluxes to the real thing"
        self.Threshold = 1e-1
        for NbrFluxLL in range(2,MaxFluxLL+1):
            for LLs in range(0,MaxLL+1):
                print("FLL,LLs:",NbrFluxLL,LLs)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                TotNe=NbrFluxLL*(LLs+1)
                XYList=np.random.rand(MC,TotNe)+1j*np.random.rand(MC,TotNe)
                [Momenta,LLs]=hammer.FQHTorusJKWF.get_filled_LL_occupation(NbrFluxLL,LLs)
                #print("Momenta:",Momenta)
                #print("LLs:",LLs)
                WFN=FQHTorusJKWF(NbrFluxLL,Momenta,LLs,tau)
                TotFlux=NbrFluxLL+2*TotNe
                from fractions import Fraction
                nu=Fraction(TotNe,TotFlux)
                for NeNum in range(TotNe):
                    XYList1=np.array(XYList)
                    XYList2=np.array(XYList)
                    XYList1[:,NeNum]+=epsilon
                    XYList2[:,NeNum]+=1j*epsilon
                    y=np.imag(XYList[:,NeNum])
                    i2pi=1j*2*np.pi
                    ##Initialize to make sure complex
                    wfval=wfval1=wfval2=1j*1.0
                    wfval =np.array(WFN.WFValue(XYList,det=DoDet))
                    wfval1=np.array(WFN.WFValue(XYList1,det=DoDet))
                    wfval2=np.array(WFN.WFValue(XYList2,det=DoDet))
                    DX=tau*(wfval1-wfval)/epsilon
                    DY=(wfval2-wfval)/epsilon
                    ScalarPart=1j*tau*2*np.pi*TotFlux*y*wfval
                    FullDerivative=DX-DY+ScalarPart

                    ###We assume that DX-DY+ScalarPart and solve for Ns"                    
                    EffectiveNs=(DY-DX)/(1j*tau*2*np.pi*y*wfval)
                    
                    RelDX=np.abs(FullDerivative)/np.abs(DX)
                    RelDY=np.abs(FullDerivative)/np.abs(DY)
                    RelScal=np.abs(FullDerivative)/np.abs(ScalarPart)
                    RelWfn=np.abs(FullDerivative)/np.abs(wfval)

                    ####If the relative derivatives are to large, then the wave funtions has no vanished
                    if np.sum(np.abs(EffectiveNs-TotFlux))>self.Threshold:
                        Text="nu = "+str(nu.numerator)+"/"+str(nu.denominator)
                        Text+="\n NbrFluxLL,LLs: "+str(NbrFluxLL)+","+str(LLs)
                        Text+="\n TotFlux,TotNe: "+str(TotFlux)+","+str(TotNe)
                        Text+="\n tau: "+str(tau)
                        Text+="\n epsilon: "+str(epsilon)
                        Text+="\n Threshold: "+str(self.Threshold)
                        Text+="\n xList,yList: "+str(XYList)
                        Text+="\n wfval: "+str(wfval)
                        Text+="\n wfval1: "+str(wfval1)
                        Text+="\n wfval2: "+str(wfval2)
                        Text+="\n delx: "+str(DX)
                        Text+="\n dely:"+str(DY)
                        Text+="\n ScalarPart: "+str(ScalarPart)
                        Text+="\n FullDerivative: "+str(FullDerivative)
                        Text+="\n EffectiveNs: "+str(EffectiveNs)
                        Text+="\n Diffs: "+str(EffectiveNs-TotFlux)
                        Text+="\n |Diffs|: "+str(np.abs(EffectiveNs-TotFlux))
                        Text+="\n |delx|: "+str(np.abs(DX))
                        Text+="\n |dely|:"+str(np.abs(DY))
                        Text+="\n |ScalarPart|: "+str(np.abs(ScalarPart))
                        Text+="\n |FullDerivative|: "+str(np.abs(FullDerivative))
                        Text+="\n Relative DX:: "+str(RelDX)
                        Text+="\n Relative DY:: "+str(RelDY)
                        Text+="\n Relative ScalarPart:: "+str(RelScal)
                        Text+="\n Relative Wavefunctoin:: "+str(RelWfn)
                        Text+="\n ERROR: The Wave function is not in the LLL"
                        print(Text)
                        self.assertTrue(False,Text)



def get_max_separated(Ne):
    import scipy.optimize as spo
    XYList=np.random.rand(2*Ne) ###Ral list whre fist hald if the X-position and second half is the Y-position
    ###Find configuration of electrons that minimize the couloumb repulsion energy
    OptimizeResult = spo.minimize(PeriodicColoumbEnergy,
                                  XYList,
                                  options={"disp":True,
                                           "maxiter":Ne*10,
                                           "maxfev":Ne*10},
                                  method='Nelder-Mead',
                                  args=())
    BestGuess=OptimizeResult.get('x')
    NewXY=BestGuess[:Ne]+1j*BestGuess[Ne:]
    return NewXY



def PeriodicColoumbEnergy(XYList):
    Ne=len(XYList)/2
    #print("XYList\n",XYList)
    V=0.0
    for indx1 in range(Ne-1):
        for indx2 in range(indx1+1,Ne):
            #print("i1,i2",indx1,indx2)
            XDiff=np.mod(XYList[indx1]-XYList[indx2]+0.5,1.0)-.5
            YDiff=np.mod(XYList[indx1+Ne]-XYList[indx2+Ne]+.5,1.0)-.5
            #print("XDiff,YDiff",XDiff,YDiff)
            Vloc=1.0/(XDiff**2 + YDiff**2)
            #print("Vloc",Vloc)
            V+=Vloc
    #print("V:",V)
    return V
    

def TestWfnZero(wfval,wfval1,wfval2,WfnThreshold):
    return ((np.abs(wfval) < WfnThreshold)
        or (np.abs(wfval1) < WfnThreshold)
        or (np.abs(wfval2) < WfnThreshold))


def TestWfnDiffernt(wfval,wfval1,wfval2,Threshold):
    IsZero = (np.abs(wfval) == 0.0 or
              np.abs(wfval1) == 0.0 or
              np.abs(wfval2) == 0.0)
    return ((np.abs(wfval/wfval1-1) > Threshold)
        or  (np.abs(wfval/wfval2-1) > Threshold )
        or  IsZero)


def print_error(NbrFluxLL,LLs,TotFlux,TotNe,tau,XYList,
                wfval,wfval1,wfval2,phase1,phase2,move,MESSAGE,self):
    from fractions import Fraction
    nu=Fraction(TotNe,TotFlux)
    Text="nu = "+str(nu.numerator)+"/"+str(nu.denominator)
    Text+="\n NbrFluxLL,LLs: "+str(NbrFluxLL)+","+str(LLs)
    Text+="\n TotFlux,TotNe: "+str(TotFlux)+","+str(TotNe)
    Text+="\n tau: "+str(tau)
    Text+="\n move: "+str(move)
    Text+="\n xList,yList: "+str(XYList)
    Text+="\n log(phase1)/(2*pi*i): "+str(np.log(phase1)/(1j*2*np.pi))
    Text+="\n log(phase2)/(2*pi*i): "+str(np.log(phase2)/(1j*2*np.pi))
    Text+="\n wfval: "+str(wfval)
    Text+="\n wfval1: "+str(wfval1)
    Text+="\n wfval2: "+str(wfval2)
    Text+="\n wfval1/wfval: "+str(wfval1/wfval)
    Text+="\n wfval2/wfval: "+str(wfval2/wfval)
    Text+="\n log(wfval1/wfval)/(2*pi*i): "+str(np.log(wfval1/wfval)/(1j*2*np.pi))
    Text+="\n log(wfval2/wfval)/(2*pi*i): "+str(np.log(wfval2/wfval)/(1j*2*np.pi))
    Text+="\n ERROR: "+MESSAGE
    print(Text)
    self.assertTrue(False,Text)


def set_xylist(NbrNe):
    XYList=np.random.rand(NbrNe)+1j*np.random.rand(NbrNe)
    XYList1=np.array(XYList)
    XYList2=np.array(XYList)
    return XYList,XYList1,XYList2
    
if __name__ == '__main__':
    unittest.main()
