#! /usr/bin/env python

"""
 Unit tests to test basic particle hole conjugation functionality.

"""
import unittest
from hammer.TorusDiagonalise import TorusDiagonalise
from hammer.HammerArgs import HammerArgsParser
from hammer.vectorutils import *

import numpy as np

class TestPHConjugation(unittest.TestCase):

    def setUp(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.ReturnEigenVectors = True
        self.Opts.NbrStates=1
        self.Opts.WriteBasisFlag = False
        self.Opts.ReturnBasis = True


    def test_three_particle_ph_conjugate(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.Interaction = 'Coulomb'
        self.Opts.COMMomentum = -1
        self.Opts.InversionSector = -1
        self.Opts.Silent = True
        
        Data = TorusDiagonalise(self.Opts);
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']
        _, PHGS = Basis.ConvertToPHConjugate(GS)

        self.Opts.Particles = 6
        Data2 = TorusDiagonalise(self.Opts);
        GS2 = Data2['EigenVectors'][0]

        print('Overlap of state and PH conjugate: ' + str(np.abs(Overlap(PHGS, GS2, Absolute=True))))

        self.assertTrue(np.abs(1.0 - Overlap(PHGS, GS2, Absolute=True)) < 1e-10)


    def test_four_particle_ph_conjugate(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 2
        self.Opts.COMMomentum = -1
        self.Opts.InversionSector = -1
        self.Opts.Interaction = 'Coulomb'
        self.Opts.Silent = True

        Data = TorusDiagonalise(self.Opts);
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']
        _, PHGS = Basis.ConvertToPHConjugate(GS)

        self.Opts.Particles = 8
        self.Opts.Momentum = 4
        Data2 = TorusDiagonalise(self.Opts);
        GS2 = Data2['EigenVectors'][0]

        print('Overlap of state and PH conjugate: ' + str(np.abs(Overlap(PHGS, GS2, Absolute=True))))

        self.assertTrue(np.abs(1.0 - Overlap(PHGS, GS2, Absolute=True)) < 1e-10)


    def test_four_particle_ph_conjugate_com(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 2
        self.Opts.InversionSector = -1
        self.Opts.Interaction = 'Coulomb'
        self.Opts.Silent = True

        Data = TorusDiagonalise(self.Opts);
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']
        _, PHGS = Basis.ConvertToPHConjugate(GS)

        self.Opts.Particles = 8
        self.Opts.Momentum = 4
        self.Opts.COMMomentum = 4
        Data2 = TorusDiagonalise(self.Opts);
        GS2 = Data2['EigenVectors'][0]

        print('Overlap of state and PH conjugate: ' + str(np.abs(Overlap(PHGS, GS2, Absolute=True))))

        self.assertTrue(np.abs(1.0 - Overlap(PHGS, GS2, Absolute=True)) < 1e-10)

    #@unittest.skip("Skip for now.")
    def test_four_particle_ph_conjugate_com_inv(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 2
        self.Opts.InversionSector = 0
        self.Opts.Interaction = 'Coulomb'
        self.Opts.Silent = True

        Data = TorusDiagonalise(self.Opts);
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']
        PHBasis, PHGS = Basis.ConvertToPHConjugate(GS)

        Details = PHBasis.GetBasisDetails()
        self.Opts.Particles = Details['Particles']
        self.Opts.Momentum = Details['Momentum']
        self.Opts.COMMomentum = Details['COMMomentum']
        self.Opts.InversionSector = Details['InversionSector']
        Data2 = TorusDiagonalise(self.Opts);
        GS2 = Data2['EigenVectors'][0]

        print('Overlap of state and PH conjugate: ' + str(np.abs(Overlap(PHGS, GS2, Absolute=True))))

        self.assertTrue(np.abs(1.0 - Overlap(PHGS, GS2, Absolute=True)) < 1e-10)


    def test_eight_particle_ph_conjugate_com_inv(self):
        self.Opts.Particles = 8
        self.Opts.NbrFlux = 20
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.InversionSector = 0
        self.Opts.Interaction = 'Coulomb'
        self.Opts.Silent = True

        Data = TorusDiagonalise(self.Opts);
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']
        PHBasis, PHGS = Basis.ConvertToPHConjugate(GS)

        Details = PHBasis.GetBasisDetails()
        self.Opts.Particles = Details['Particles']
        self.Opts.Momentum = Details['Momentum']
        self.Opts.COMMomentum = Details['COMMomentum']
        self.Opts.InversionSector = Details['InversionSector']
        Data2 = TorusDiagonalise(self.Opts);
        GS2 = Data2['EigenVectors'][0]

        print('Overlap of state and PH conjugate: ' + str(np.abs(Overlap(PHGS, GS2, Absolute=True))))

        self.assertTrue(np.abs(1.0 - Overlap(PHGS, GS2, Absolute=True)) < 1e-10)


    def test_five_particle_ph_conjugate(self):
        self.Opts.Particles = 5
        self.Opts.NbrFlux = 10
        self.Opts.InversionSector = -1
        self.Opts.Interaction = 'Coulomb'
        self.Opts.COMMomentum = -1
        self.Opts.Silent = True
        self.Opts.NbrStates=5
        self.Opts.NbrEgivals=5
        tol = 1e-10

        for Momentum in range(self.Opts.NbrFlux):
            self.Opts.Momentum = Momentum
            Data = TorusDiagonalise(self.Opts);
            GS_Space = []
            idx = 1
            last_e = Data['EigenValues'][0][0]
            GS_Space.append(Data['EigenVectors'][0])
            while idx < len(Data['EigenValues']):
                if np.abs(Data['EigenValues'][idx][0] - last_e) < tol:
                    GS_Space.append(Data['EigenVectors'][idx])
                else:
                    break
                idx+=1
            dim = len(GS_Space)
            Basis = Data['Basis']
            PH_Space = []
            for idx in range(dim):
                PHBasis, PHGS = Basis.ConvertToPHConjugate(GS_Space[idx])
                PH_Space.append(PHGS)

            Details = PHBasis.GetBasisDetails()
            self.Opts.Particles = Details['Particles']
            self.Opts.Momentum = Details['Momentum']
            Data2 = TorusDiagonalise(self.Opts)
            GS2_Space = list(map(lambda x: Data2['EigenVectors'][x], range(dim)))

            Overlaps = np.zeros((dim, dim), np.complex128)
            for i in range(dim):
                for j in range(dim):
                    Overlaps[i,j] = Overlap(PH_Space[i], GS2_Space[j])

            diag_overlaps,_ = np.linalg.eig(Overlaps)

            print('Overlap of 5 particle coulomb gs and PH conjugate at momentum ' + str(self.Opts.Momentum) + ' with dim ' + str(dim) + ' : ' + str(np.abs(diag_overlaps)))

            print('Overlap: ' + str(np.abs(float(dim) - np.sum(np.abs(diag_overlaps)))))
            self.assertTrue(np.abs(float(dim) - np.sum(np.abs(diag_overlaps))) < 1e-10)


    @unittest.skip("Skip for now.")
    def test_five_particle_ph_conjugate_squared(self):
        self.Opts.Particles = 5
        self.Opts.NbrFlux = 10
        self.Opts.InversionSector = -1
        self.Opts.Interaction = 'Coulomb'
        self.Opts.COMMomentum = -1
        self.Opts.Silent = True
        tol = 1e-10

        for Momentum in range(self.Opts.NbrFlux):
            self.Opts.Momentum = Momentum
            Data = TorusDiagonalise(self.Opts);
            GS_Space = []
            idx = 1
            last_e = Data['EigenValues'][0][0]
            GS_Space.append(Data['EigenVectors'][0])
            while idx < len(Data['EigenValues']):
                if np.abs(Data['EigenValues'][idx][0] - last_e) < tol:
                    GS_Space.append(Data['EigenVectors'][idx])
                else:
                    break
                idx+=1
            dim = len(GS_Space)
            Basis = Data['Basis']
            PH_Space = []
            for idx in range(dim):
                PHBasis, PHGS = Basis.ConvertToPHConjugate(GS_Space[idx])
                PH_Space.append(PHGS)

            Details = PHBasis.GetBasisDetails()
            self.Opts.Particles = Details['Particles']
            self.Opts.Momentum = Details['Momentum']
            Data2 = TorusDiagonalise(self.Opts)
            GS2_Space = list(map(lambda x: Data2['EigenVectors'][x], range(dim)))

            Overlaps = np.zeros((dim, dim), np.complex128)
            for i in range(dim):
                for j in range(dim):
                    Overlaps[i,j] = Overlap(PH_Space[i], GS2_Space[j])

            diag_overlaps,_ = np.linalg.eig(Overlaps)

            print('Overlap of 5 particle coulomb gs and PH conjugate at momentum ' + str(self.Opts.Momentum) + ' with dim ' + str(dim) + ' : ' + str(np.abs(diag_overlaps)))

            print('Overlap: ' + str(np.abs(float(dim) - np.sum(np.abs(diag_overlaps)))))
            self.assertTrue(np.abs(float(dim) - np.sum(np.abs(diag_overlaps))) < 1e-10)


if __name__ == '__main__':
    unittest.main()
