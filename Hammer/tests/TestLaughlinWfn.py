#! /usr/bin/env python
from __future__ import print_function

"""
 Unit tests to for reflection symmetry code. Tests that the eigenvalues retrieved are correct for a few small systems. 
"""
from hammer.FQHLaughlin import FQHTorusLaughlinWF

import unittest
import random
import numpy as np


class TestLaughlin(unittest.TestCase):
    """
    Test suite for single particle orbitals.
    """

    def setUp(self):
        self.Threshold = 1e-09

    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_pbc_Laughlin(self):
        """ Test that periodic boundary conditions works for Laughlin state"""
        MaxNe=10
    
        self.Threshold = 1e-10
        Denom=3
        for NbrNe in range(1,MaxNe+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            for Momenta in (NbrNe*np.arange(Denom)+(NbrNe*(NbrNe-1))/2):
                NbrFlux=NbrNe*Denom
                ###Initialize the wave function
                WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau)

                XYList,XYList1,XYList2=set_xylist(NbrNe)
                XYList1[0]+=1.0
                XYList2[0]+=1.0*1j

                x=np.real(XYList[0])
                y=np.imag(XYList[0])
                Gauge=np.exp(1j*2*np.pi*NbrFlux*x)

                #print("Z-pahse:",Zphase)
                ## Extra phase for y-pbc
                phase1 = 1.0
                phase2 = Gauge
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0
                wfval =WFN.WfnValue(XYList)
                wfval1=WFN.WfnValue(XYList1)*phase1
                wfval2=WFN.WfnValue(XYList2)*phase2


                if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                    print("\nERROR: Laughlin wfn does not have correct boundary conditions")
                    print("NbrFlux,NbrNe:",NbrFlux,NbrNe)
                    print('tau:',tau)
                    print('K1,x,y:',Momenta,x,y)
                    print('XYList: ',XYList)
                    print('XYList1:',XYList1)
                    print('XYList2:',XYList2)
                    print("phase1/(2*pi): ",np.log(phase1)/(1j*2*np.pi))
                    print("phase2/(2*pi): ",np.log(phase2)/(1j*2*np.pi))
                    print("wfval: ",wfval)
                    print("wfval1:",wfval1)
                    print("wfval2:",wfval2)
                    print("wfval1/wfval",wfval1/wfval)
                    print("wfval2/wfval:",wfval2/wfval)
                    print("log(wfval1/wfval)/(2*pi): ",np.log(wfval1/wfval)/(2*np.pi))
                    print("log(wfval2/wfval)/(2*pi): ",np.log(wfval2/wfval)/(2*np.pi))
                    self.assertTrue(False,"Wave functions are not the same")


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_sampled_Laughlin(self):
        """ Test that pre-sampled laughlin is same as direct computed Laughlin"""
        self.do_test_sampled_Laughlin(False)
    def test_sampled_Laughlin_with_effective_Ns(self):
        """ Test that pre-sampled laughlin is same as direct computed Laughlin for a lattice size that is not the canonical one"""
        self.do_test_sampled_Laughlin(True)
    def do_test_sampled_Laughlin(self,UseEffNs):
        MaxNe=5
        self.Threshold = 1e-10
        for Denom in np.arange(3)*2+1:###denom = 1,3,5
            for NbrNe in range(1,MaxNe+1):
                print("Denom,Ne:",Denom,NbrNe)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                for Momenta in (NbrNe*np.arange(Denom)+(NbrNe*(NbrNe-1))/2):
                    NbrFlux=NbrNe*Denom
                    if UseEffNs:
                        EffFlux=NbrFlux+random.randint(1,10)
                    else:
                        EffFlux=NbrFlux
                    ###Initialize the wave function
                    WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,Denom=Denom,EffectiveNs=EffFlux)
                    WFNLS=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,LatticeSample=True,Denom=Denom,EffectiveNs=EffFlux)

                    XList,YList,XYList1=set_unique_xyint(NbrNe,EffFlux)

                    wfval=wfval1=wfval2=1j*1.0
                    wfval =WFN.WfnValue(XYList1,logform=True)
                    wfval1=WFN.WfnValueLattice(XList,YList,logform=True)
                    wfval2=WFNLS.WfnValueLattice(XList,YList,logform=True)


                    if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold,log=True):
                        ERRTXT="ERROR: Sampled Laughlin wfn does agree with directly computed Laughlin"
                        ERRTXT+="\nNbrFlux,NbrNe,EffFLux: "+str(NbrFlux)+", "+str(NbrNe)+", "+str(EffFlux)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        ERRTXT+="\nXList: "+str(XList)
                        ERRTXT+="\nYList: "+str(YList)
                        ERRTXT+="\nXYList1: "+str(XYList1)
                        ERRTXT+="\nwfval: "+str(wfval)
                        ERRTXT+="\nwfval1: "+str(wfval1)
                        ERRTXT+="\nwfval2: "+str(wfval2)
                        ERRTXT+="\nwfval1/wfval: "+str(wfval1/wfval)
                        ERRTXT+="\nwfval2/wfval: "+str(wfval2/wfval)
                        ERRTXT+="\nlog(wfval1/wfval)/(2*pi): "+str(np.log(wfval1/wfval)/(2*np.pi))
                        ERRTXT+="\nlog(wfval2/wfval)/(2*pi): "+str(np.log(wfval2/wfval)/(2*np.pi))
                        self.assertTrue(False,ERRTXT)


                        
    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_updated_Laughlin(self):
        """ Test that pre-sampled laughlin is same as cirect computed Laughlin"""
        self.do_test_updated_Laughlin(False)
    def test_updated_Laughlin_with_effective_Ns(self):
        """ Test that pre-sampled laughlin is same as cirect computed Laughlin when useing effective Ns"""
        self.do_test_updated_Laughlin(True)

    def do_test_updated_Laughlin(self,UseEffNs):
        """ Test that pre-sampled laughlin is same as cirect computed Laughlin with or without effective lattice"""
        MaxNe=5
        self.Threshold = 1e-10
        for Denom in np.arange(3)*2+1:###denom = 1,3,5
            for NbrNe in range(2,MaxNe+1):
                print("Denom,Ne:",Denom,NbrNe)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                Momenta=(NbrNe*(NbrNe-1))/2
                NbrFlux=NbrNe*Denom
                if UseEffNs:
                    EffFlux=NbrFlux+random.randint(1,10)
                else:
                    EffFlux=NbrFlux
                ###Initialize the wave function
                WFN1=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,Denom=Denom,EffectiveNs=EffFlux)
                WFN2=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,Denom=Denom,EffectiveNs=EffFlux)
                WFNLS3=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,LatticeSample=True,Denom=Denom,EffectiveNs=EffFlux)
                WFNLS4=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,LatticeSample=True,Denom=Denom,EffectiveNs=EffFlux)

                XList,YList,XYList=set_unique_xyint(NbrNe,EffFlux)

                wfval1=wfval2=wfval3=wfval4=1j*1.0
                wfval1=WFN1.WfnValue(XYList,logform=True)
                wfval2=WFN2.WfnValue(XYList,logform=True)
                wfval3=WFNLS3.WfnValueLattice(XList,YList,logform=True)
                wfval4=WFNLS4.WfnValueLattice(XList,YList,logform=True)


                if TestWfnDiffernt(wfval1,wfval2,wfval3,self.Threshold,log=True) or TestWfnDiffernt(wfval2,wfval3,wfval4,self.Threshold,log=True) :
                    ERRTXT="ERROR: Sampled Laughlin wfn does agree with directly computed Laughlin"
                    ERRTXT+="\nNbrFlux,NbrNe,EffFLux: "+str(NbrFlux)+", "+str(NbrNe)+", "+str(EffFlux)
                    ERRTXT+="\ntau: "+str(tau)
                    ERRTXT+="\nK1: "+str(Momenta)
                    ERRTXT+="\nXList: "+str(XList)
                    ERRTXT+="\nYList: "+str(YList)
                    ERRTXT+="\nXYList: "+str(XYList)
                    ERRTXT+="\nwfval1: "+str(wfval1)
                    ERRTXT+="\nwfval2: "+str(wfval2)
                    ERRTXT+="\nwfval3: "+str(wfval3)
                    ERRTXT+="\nwfval4: "+str(wfval4)
                    ERRTXT+="\nwfval2/wfval1: "+str(wfval2/wfval1)
                    ERRTXT+="\nwfval3/wfval1: "+str(wfval3/wfval1)
                    ERRTXT+="\nwfval4/wfval1: "+str(wfval4/wfval1)
                    ERRTXT+="\nlog(wfval2/wfval1)/(2*pi): "+str(np.log(wfval2/wfval1)/(2*np.pi))
                    ERRTXT+="\nlog(wfval3/wfval1)/(2*pi): "+str(np.log(wfval3/wfval1)/(2*np.pi))
                    ERRTXT+="\nlog(wfval4/wfval1)/(2*pi): "+str(np.log(wfval4/wfval1)/(2*np.pi))
                    self.assertTrue(False,ERRTXT)

                #### Update the steps:
                MoveNe=random.randrange(NbrNe)
                #print("MoveNe:",MoveNe)
                while True:
                    ###Choose a new datapoint
                    NewX=random.randrange(EffFlux)
                    NewY=random.randrange(EffFlux)
                    ###Chack that this datapoint is not occupied
                    #print("X:",NewX==XList)
                    #print("Y:",NewY==YList)
                    #print("XY:",np.logical_and(NewX==XList,NewY==YList))
                    NewPoint=not np.sum(np.logical_and(
                        np.mod(NewX-XList,EffFlux)==0,
                        np.mod(NewY-YList,EffFlux)==0))
                    #print("NewPoint:",NewPoint)
                    if NewPoint:
                        break
                NewXList=XList+0
                NewYList=YList+0
                NewXList[MoveNe]=NewX
                NewYList[MoveNe]=NewY
                NewXYList=(NewXList+1j*NewYList)/EffFlux
                newwfval1=newwfval2=newwfval3=newwfval4=1j*1.0
                newwfval1=WFN1.WfnValue(NewXYList,logform=True)
                newwfval2=WFN2.UpdateWfnValue(NewXYList,XYList,MoveNe,wfval2,logform=True)
                newwfval3=WFNLS3.WfnValueLattice(NewXList,NewYList,logform=True)
                newwfval4=WFNLS4.UpdateWfnValueLattice(NewXList,NewYList,XList,YList,MoveNe,wfval4,logform=True)
                if TestWfnSame(wfval1,newwfval1) or TestWfnSame(wfval2,newwfval2) or TestWfnSame(wfval3,newwfval3) or TestWfnSame(wfval4,newwfval4):
                    ERRTXT="ERROR: Updated Laughlin wfn is not different from the original. This should not happen"
                    ERRTXT+="\nNbrFlux,NbrNe,EffFLux: "+str(NbrFlux)+", "+str(NbrNe)+", "+str(EffFlux)
                    ERRTXT+="\ntau: "+str(tau)
                    ERRTXT+="\nK1: "+str(Momenta)
                    ERRTXT+="\nXList: "+str(XList)
                    ERRTXT+="\nYList: "+str(YList)
                    ERRTXT+="\nXYList: "+str(XYList)
                    ERRTXT+="\nNewXList: "+str(NewXList)
                    ERRTXT+="\nNewYList: "+str(NewYList)
                    ERRTXT+="\nNewXYList: "+str(NewXYList)
                    ERRTXT+="\nwfval1: "+str(wfval1)
                    ERRTXT+="\nwfval2: "+str(wfval2)
                    ERRTXT+="\nwfval3: "+str(wfval3)
                    ERRTXT+="\nwfval4: "+str(wfval4)
                    ERRTXT+="\nnewwfval1: "+str(newwfval1)
                    ERRTXT+="\nnewwfval2: "+str(newwfval2)
                    ERRTXT+="\nnewwfval3: "+str(newwfval3)
                    ERRTXT+="\nnewwfval4: "+str(newwfval4)
                    self.assertTrue(False,ERRTXT)

                
                
                if TestWfnDiffernt(newwfval1,newwfval2,newwfval3,self.Threshold,log=True) or TestWfnDiffernt(newwfval2,newwfval3,newwfval4,self.Threshold,log=True) :
                    ERRTXT="ERROR: Updated Laughlin wfn does agree with directly computed Laughlin"
                    ERRTXT+="\nNbrFlux,NbrNe,EffFLux: "+str(NbrFlux)+", "+str(NbrNe)+", "+str(EffFlux)
                    ERRTXT+="\ntau: "+str(tau)
                    ERRTXT+="\nK1: "+str(Momenta)
                    ERRTXT+="\nXList: "+str(XList)
                    ERRTXT+="\nYList: "+str(YList)
                    ERRTXT+="\nXYList: "+str(XYList)
                    ERRTXT+="\nNewXList: "+str(NewXList)
                    ERRTXT+="\nNewYList: "+str(NewYList)
                    ERRTXT+="\nNewXYList: "+str(NewXYList)
                    ERRTXT+="\old wfval1: "+str(wfval1)
                    ERRTXT+="\nnewwfval1: "+str(newwfval1)
                    ERRTXT+="\nnewwfval2: "+str(newwfval2)
                    ERRTXT+="\nnewwfval3: "+str(newwfval3)
                    ERRTXT+="\nnewwfval4: "+str(newwfval4)
                    ERRTXT+="\nnewwfval2/newwfval1: "+str(newwfval2/newwfval1)
                    ERRTXT+="\nnewwfval3/newwfval1: "+str(newwfval3/newwfval1)
                    ERRTXT+="\nnewwfval4/newwfval1: "+str(newwfval4/newwfval1)
                    ERRTXT+="\nlog(newwfval2/newwfval1)/(2*pi): "+str(np.log(newwfval2/newwfval1)/(2*np.pi))
                    ERRTXT+="\nlog(newwfval3/newwfval1)/(2*pi): "+str(np.log(newwfval3/newwfval1)/(2*np.pi))
                    ERRTXT+="\nlog(newwfval4/newwfval1)/(2*pi): "+str(np.log(newwfval4/newwfval1)/(2*np.pi))
                    self.assertTrue(False,ERRTXT)
                        



    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_updated_Laughlin_quasiholes(self):
        """ Test that pre-sampled laughlin is same as cirect computed Laughlin"""
        MaxNe=5
        MaxQh=3
        self.Threshold = 1e-10
        for Denom in np.arange(3)*2+1:###denom = 1,3,5
            for NbrNe in range(2,MaxNe+1):
                print("Denom,Ne:",Denom,NbrNe)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                Momenta=(NbrNe*(NbrNe-1))/2

                for NbrQh in range(1,MaxQh+1):
                    NbrFlux=NbrNe*Denom+NbrQh
                    QHPos=set_qh_pos(NbrQh)
                    ###Initialize the wave function
                    WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,Denom=Denom,QHPos=QHPos)
                    WFNLS=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,QHPos=QHPos,LatticeSample=True,Denom=Denom)
                    XList,YList,XYList=set_unique_xyint(NbrNe,NbrFlux)

                    wfval1=wfval3=1j*1.0
                    wfval1=WFN.WfnValue(XYList,logform=True)
                    wfval3=WFNLS.WfnValueLattice(XList,YList,logform=True)


                    if TestWfnDiff(wfval1,wfval3,self.Threshold,log=True):
                        ERRTXT="ERROR: Sampled Laughlin wfn does agree with directly computed Laughlin with qhs present"
                        ERRTXT+="\nNbrFlux,NbrNe: "+str(NbrFlux)+" "+str(NbrNe)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        ERRTXT+="\nXList: "+str(XList)
                        ERRTXT+="\nYList: "+str(YList)
                        ERRTXT+="\nXYList: "+str(XYList)
                        ERRTXT+="\nwfval1: "+str(wfval1)
                        ERRTXT+="\nwfval3: "+str(wfval3)
                        ERRTXT+="\nwfval3/wfval1: "+str(wfval3/wfval1)
                        ERRTXT+="\nlog(wfval3/wfval1)/(2*pi): "+str(np.log(wfval3/wfval1)/(2*np.pi))
                        self.assertTrue(False,ERRTXT)

                    #### Update the steps:
                    MoveNe=random.randrange(NbrNe)
                    #print("MoveNe:",MoveNe)
                    while True:
                        ###Choose a new datapoint
                        NewX=random.randrange(NbrFlux)
                        NewY=random.randrange(NbrFlux)
                        ###Chack that this datapoint is not occupied
                        #print("X:",NewX==XList)
                        #print("Y:",NewY==YList)
                        #print("XY:",np.logical_and(NewX==XList,NewY==YList))
                        NewPoint=not np.sum(np.logical_and(
                            np.mod(NewX-XList,NbrFlux)==0,
                            np.mod(NewY-YList,NbrFlux)==0))
                        #print("NewPoint:",NewPoint)
                        if NewPoint:
                            break
                    NewXList=XList+0
                    NewYList=YList+0
                    NewXList[MoveNe]=NewX
                    NewYList[MoveNe]=NewY
                    NewXYList=(NewXList+1j*NewYList)/NbrFlux
                    newwfval1=newwfval2=newwfval3=newwfval4=1j*1.0
                    newwfval1=WFN.WfnValue(NewXYList,logform=True)
                    newwfval2=WFN.UpdateWfnValue(NewXYList,XYList,MoveNe,wfval1,logform=True)
                    newwfval3=WFNLS.WfnValueLattice(NewXList,NewYList,logform=True)
                    newwfval4=WFNLS.UpdateWfnValueLattice(NewXList,NewYList,XList,YList,MoveNe,wfval3,logform=True)
                    if TestWfnSame(wfval1,newwfval1) or TestWfnSame(wfval1,newwfval2) or TestWfnSame(wfval3,newwfval3) or TestWfnSame(wfval3,newwfval4):
                        ERRTXT="ERROR: Updated Laughlin wfn is not different from the original with qhs present. This should not happen"
                        ERRTXT+="\nNbrFlux,NbrNe: "+str(NbrFlux)+" "+str(NbrNe)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        ERRTXT+="\nXList: "+str(XList)
                        ERRTXT+="\nYList: "+str(YList)
                        ERRTXT+="\nXYList: "+str(XYList)
                        ERRTXT+="\nNewXList: "+str(NewXList)
                        ERRTXT+="\nNewYList: "+str(NewYList)
                        ERRTXT+="\nNewXYList: "+str(NewXYList)
                        ERRTXT+="\nwfval1: "+str(wfval1)
                        ERRTXT+="\nwfval3: "+str(wfval3)
                        ERRTXT+="\nnewwfval1: "+str(newwfval1)
                        ERRTXT+="\nnewwfval2: "+str(newwfval2)
                        ERRTXT+="\nnewwfval3: "+str(newwfval3)
                        ERRTXT+="\nnewwfval4: "+str(newwfval4)
                        self.assertTrue(False,ERRTXT)



                    if TestWfnDiffernt(newwfval1,newwfval2,newwfval3,self.Threshold,log=True) or TestWfnDiffernt(newwfval2,newwfval3,newwfval4,self.Threshold,log=True) :
                        ERRTXT="ERROR: Updated Laughlin wfn does not agree with directly computed Laughlin with qhs present"
                        ERRTXT+="\nNbrFlux,NbrNe: "+str(NbrFlux)+" "+str(NbrNe)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        ERRTXT+="\nXList: "+str(XList)
                        ERRTXT+="\nYList: "+str(YList)
                        ERRTXT+="\nXYList: "+str(XYList)
                        ERRTXT+="\nnewwfval1: "+str(newwfval1)
                        ERRTXT+="\nnewwfval2: "+str(newwfval2)
                        ERRTXT+="\nnewwfval3: "+str(newwfval3)
                        ERRTXT+="\nnewwfval4: "+str(newwfval4)
                        ERRTXT+="\nnewwfval2/newwfval1: "+str(newwfval2/newwfval1)
                        ERRTXT+="\nnewwfval3/newwfval1: "+str(newwfval3/newwfval1)
                        ERRTXT+="\nnewwfval4/newwfval1: "+str(newwfval4/newwfval1)
                        ERRTXT+="\nlog(newwfval2/newwfval1)/(2*pi): "+str(np.log(newwfval2/newwfval1)/(2*np.pi))
                        ERRTXT+="\nlog(newwfval3/newwfval1)/(2*pi): "+str(np.log(newwfval3/newwfval1)/(2*np.pi))
                        ERRTXT+="\nlog(newwfval4/newwfval1)/(2*pi): "+str(np.log(newwfval4/newwfval1)/(2*np.pi))
                        self.assertTrue(False,ERRTXT)
                        

                    
                        
    def test_T1_T2_Laughlin(self):
        """ Test that com monetum has correct pahse for laughglin state"""
        MaxNe=10
        self.Threshold = 1e-10
        Denom=3
        for NbrNe in range(1,MaxNe+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            for Momenta in (NbrNe*np.arange(Denom)+(NbrNe*(NbrNe-1))/2):
                NbrFlux=NbrNe*Denom
                ###Initialize the wave function
                WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau)

                XYList,XYList1,XYList2=set_xylist(NbrNe)
                XYList1[0]+=1.0
                XYList2[0]+=1.0*1j

                x=np.real(XYList[0])
                y=np.imag(XYList[0])
                Gauge=np.exp(1j*2*np.pi*NbrFlux*x)

                #print("Z-pahse:",Zphase)
                ## Extra phase for y-pbc
                phase1 = 1.0
                phase2 = Gauge
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0
                wfval =WFN.WfnValue(XYList)
                wfval1=WFN.WfnValue(XYList1)*phase1
                wfval2=WFN.WfnValue(XYList2)*phase2


                if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                    print("\nERROR: Laughlin wfn does not have correct boundary conditions")
                    print("NbrFlux,NbrNe:",NbrFlux,NbrNe)
                    print('tau:',tau)
                    print('K1,x,y:',Momenta,x,y)
                    print('XYList: ',XYList)
                    print('XYList1:',XYList1)
                    print('XYList2:',XYList2)
                    print("phase1/(2*pi): ",np.log(phase1)/(1j*2*np.pi))
                    print("phase2/(2*pi): ",np.log(phase2)/(1j*2*np.pi))
                    print("wfval: ",wfval)
                    print("wfval1:",wfval1)
                    print("wfval2:",wfval2)
                    print("wfval1/wfval",wfval1/wfval)
                    print("wfval2/wfval:",wfval2/wfval)
                    print("log(wfval1/wfval)/(2*pi): ",np.log(wfval1/wfval)/(2*np.pi))
                    print("log(wfval2/wfval)/(2*pi): ",np.log(wfval2/wfval)/(2*np.pi))
                    self.assertTrue(False,"Wave functions are not the same")

    def test_pbc_Laughlin_quasiholes(self):
        """ Test that periodic boundary conditions works for Laughlin state with quasiholes"""
        MaxNe=6
        MaxQh=4
    
        self.Threshold = 1e-10
        Denom=3
        for NbrNe in range(1,MaxNe+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            Momenta=(NbrNe*(NbrNe-1))/2
            for NbrQh in range(1,MaxQh+1):
                NbrFlux=NbrNe*Denom+NbrQh
                QHPos=set_qh_pos(NbrQh)
                ###Initialize the wave function
                WFN=FQHTorusLaughlinWF(NbrNe,Momenta,QHPos=QHPos,Tau=tau,Denom=Denom)
                XYList,XYList1,XYList2=set_xylist(NbrNe)
                XYList1[0]+=1.0
                XYList2[0]+=1.0*1j

                x=np.real(XYList[0])
                y=np.imag(XYList[0])
                Gauge=np.exp(1j*2*np.pi*NbrFlux*x)

                #print("Z-pahse:",Zphase)
                ## Extra phase for y-pbc
                phase1 = 1.0
                phase2 = Gauge
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0
                wfval =WFN.WfnValue(XYList)
                wfval1=WFN.WfnValue(XYList1)*phase1
                wfval2=WFN.WfnValue(XYList2)*phase2


                if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                    ERRTXT="ERROR: Laughlin wfn with QH:s does not have correct boundary conditions"
                    ERRTXT+="\nNbrFlux,NbrNe: "+str(NbrFlux)+" "+str(NbrNe)
                    ERRTXT+="\nNbrQh: "+str(NbrQh)
                    ERRTXT+="\nQHPos: "+str(QHPos)
                    ERRTXT+='\ntau: '+str(tau)
                    ERRTXT+='\nK1,x,y: '+str(Momenta)+" "+str(x)+" "+str(y)
                    ERRTXT+='\nXYList: '+str(XYList)
                    ERRTXT+='\nXYList1: '+str(XYList1)
                    ERRTXT+='\nXYList2: '+str(XYList2)
                    ERRTXT+="\nphase1/(2*pi): "+str(np.log(phase1)/(1j*2*np.pi))
                    ERRTXT+="\nphase2/(2*pi): "+str(np.log(phase2)/(1j*2*np.pi))
                    ERRTXT+="\nwfval:  "+str(wfval)
                    ERRTXT+="\nwfval1: "+str(wfval1)
                    ERRTXT+="\nwfval2: "+str(wfval2)
                    ERRTXT+="\nwfval1/wfval: "+str(wfval1/wfval)
                    ERRTXT+="\nwfval2/wfval: "+str(wfval2/wfval)
                    ERRTXT+="\nlog(wfval1/wfval)/(2*pi): "+str(np.log(wfval1/wfval)/(2*np.pi))
                    ERRTXT+="\nlog(wfval2/wfval)/(2*pi): "+str(np.log(wfval2/wfval)/(2*np.pi))
                    self.assertTrue(False,ERRTXT)



    def test_pbc_Laughlin_quasiholes_precomputed(self):
        """ Test that periodic boundary conditions works for precomputed Laughlin state with quasiholes. THis is more  test that the precomputed state handles boundary conditions in the correct way, since we do not wish to force the arument to lay within the fundamental domain."""
        MaxNe=6
        MaxQh=4
    
        self.Threshold = 1e-10
        Denom=3
        for NbrNe in range(1,MaxNe+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            Momenta=(NbrNe*(NbrNe-1))/2
            for NbrQh in range(1,MaxQh+1):
                NbrFlux=NbrNe*Denom+NbrQh
                QHPos=set_qh_pos(NbrQh)
                ###Initialize the wave function
                WFN=FQHTorusLaughlinWF(NbrNe,Momenta,QHPos=QHPos,Tau=tau,Denom=Denom)
                XList,YList,XYList=set_unique_xyint(NbrNe,NbrFlux)
                ###Initate the lists
                XList1=np.array(XList)
                YList2=np.array(YList)
                
                XList1[0]+=NbrFlux
                YList2[0]+=NbrFlux

                x=XList[0]/(1.0*NbrFlux)
                y=YList[0]/(1.0*NbrFlux)
                Gauge=np.exp(1j*2*np.pi*NbrFlux*x)

                #print("Z-pahse:",Zphase)
                ## Extra phase for y-pbc
                phase1 = 1.0
                phase2 = Gauge
                ##Initialize to make sure complex
                wfval=wfval1=wfval2=1j*1.0
                wfval =WFN.WfnValueLattice(XList,YList)
                wfval1=WFN.WfnValueLattice(XList1,YList)*phase1
                wfval2=WFN.WfnValueLattice(XList,YList2)*phase2


                if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold):
                    ERRTXT="ERROR: Laughlin wfn with QH:s does not have correct boundary conditions"
                    ERRTXT+="\nNbrFlux,NbrNe: "+str(NbrFlux)+" "+str(NbrNe)
                    ERRTXT+="\nNbrQh: "+str(NbrQh)
                    ERRTXT+="\nQHPos: "+str(QHPos)
                    ERRTXT+='\ntau: '+str(tau)
                    ERRTXT+='\nK1,x,y: '+str(Momenta)+" "+str(x)+" "+str(y)
                    ERRTXT+='\nXList:  '+str(XList)
                    ERRTXT+='\nXList1: '+str(XList1)
                    ERRTXT+='\nYList:  '+str(YList)
                    ERRTXT+='\nYList2: '+str(YList2)
                    ERRTXT+="\nx,y: "+str(x)+", "+str(y)
                    ERRTXT+="\nphase1/(2*pi): "+str(np.log(phase1)/(1j*2*np.pi))
                    ERRTXT+="\nphase2/(2*pi): "+str(np.log(phase2)/(1j*2*np.pi))
                    ERRTXT+="\nwfval:  "+str(wfval)
                    ERRTXT+="\nwfval1: "+str(wfval1)
                    ERRTXT+="\nwfval2: "+str(wfval2)
                    ERRTXT+="\nwfval1/wfval: "+str(wfval1/wfval)
                    ERRTXT+="\nwfval2/wfval: "+str(wfval2/wfval)
                    ERRTXT+="\nlog(wfval1/wfval)/(2*pi): "+str(np.log(wfval1/wfval)/(2*np.pi))
                    ERRTXT+="\nlog(wfval2/wfval)/(2*pi): "+str(np.log(wfval2/wfval)/(2*np.pi))
                    self.assertTrue(False,ERRTXT)

                    


    def test_T1_label_Laughlin_quasiholes(self):
        """ Test that setting_different_momenta gives different wave functions"""
        MaxNe=6
        MaxQh=4
        
        ##Initialize to make sure complex
        wfval0=wfval1=1j*1.0
    
        self.Threshold = 1e-6
        Denom=3
        for NbrNe in range(1,MaxNe+1):
            tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
            for NbrQh in range(1,MaxQh+1):
                NbrFlux=NbrNe*Denom+NbrQh
                QHPos=set_qh_pos(NbrQh)
                Momenta=(NbrNe*(NbrNe-1))/2
                ###Set the inital positons
                XYList,XYList1,XYList2=set_xylist(NbrNe)
                ###Initialize the wave function
                WFN0=FQHTorusLaughlinWF(NbrNe,Momenta,QHPos=QHPos,Tau=tau,Denom=Denom)
                wfval0 =WFN0.WfnValue(XYList)

        

                for indx in range(1,Denom):
                    Momenta2=Momenta+NbrNe*indx
                    ###Initialize the wave function
                    WFN1=FQHTorusLaughlinWF(NbrNe,Momenta2,QHPos=QHPos,Tau=tau,Denom=Denom)
                    wfval1=WFN1.WfnValue(XYList)

                    if TestWfnSame(wfval0,wfval1,self.Threshold):
                        ERRTXT="ERROR: Laughlin wfn with QH:s does note depend on the choice of K1"
                        ERRTXT+="\nNbrFlux,NbrNe: "+str(NbrFlux)+" "+str(NbrNe)
                        ERRTXT+="\nNbrQh: "+str(NbrQh)
                        ERRTXT+="\nQHPos: "+str(QHPos)
                        ERRTXT+='\ntau: '+str(tau)
                        ERRTXT+='\nK1: '+str(Momenta)+" and "+str(Momenta2)
                        ERRTXT+='\nXYList: '+str(XYList)
                        ERRTXT+="\nwfval0: "+str(wfval0)
                        ERRTXT+="\nwfval1: "+str(wfval1)
                        ERRTXT+="\nwfval1/wfval0: "+str(wfval1/wfval0)
                        ERRTXT+="\nlog(wfval1/wfval0)/(2*pi): "+str(np.log(wfval1/wfval0)/(2*np.pi))
                        self.assertTrue(False,ERRTXT)

                    
    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_sampled_Laughlin_quasiholes(self):
        """ Test that pre-sampled laughlin qith QH is same as direct computed Laughlin with QH"""
        MaxNe=5
        MaxQh=4
        self.Threshold = 1e-10
        for Denom in np.arange(3)*2+1:###denom = 1,3,5
            for NbrNe in range(1,MaxNe+1):
                print("Denom,Ne:",Denom,NbrNe)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                Momenta=(NbrNe*(NbrNe-1))/2
                for NbrQh in range(1,MaxQh+1):
                    NbrFlux=NbrNe*Denom+NbrQh
                    QHPos=set_qh_pos(NbrQh)
                    ###Initialize the wave function
                    WFN=FQHTorusLaughlinWF(NbrNe,Momenta,QHPos=QHPos,Tau=tau,Denom=Denom)
                    WFNLS=FQHTorusLaughlinWF(NbrNe,Momenta,QHPos=QHPos,Tau=tau,LatticeSample=True,Denom=Denom)

                    XList,YList,XYList1=set_unique_xyint(NbrNe,NbrFlux)

                    wfval=wfval1=wfval2=1j*1.0
                    wfval =WFN.WfnValue(XYList1,logform=True)
                    wfval1=WFN.WfnValueLattice(XList,YList,logform=True)
                    wfval2=WFNLS.WfnValueLattice(XList,YList,logform=True)


                    if TestWfnDiffernt(wfval,wfval1,wfval2,self.Threshold,log=True):
                        ERRTXT="ERROR: Sampled Laughlin wfn qith quasiholes does NOT agree with directly computed Laughlin"
                        ERRTXT+="\nDenom,NbrQh: "+str(Denom)+" "+str(NbrQh)
                        ERRTXT+="\nQHPos: "+str(QHPos)
                        ERRTXT+="\nNbrFlux,NbrNe: "+str(NbrFlux)+" "+str(NbrNe)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        ERRTXT+="\nXList: "+str(XList)
                        ERRTXT+="\nYList: "+str(YList)
                        ERRTXT+="\nXYList1: "+str(XYList1)
                        ERRTXT+="\nwfval: "+str(wfval)
                        ERRTXT+="\nwfval1: "+str(wfval1)
                        ERRTXT+="\nwfval2: "+str(wfval2)
                        ERRTXT+="\nwfval1/wfval: "+str(wfval1/wfval)
                        ERRTXT+="\nwfval2/wfval: "+str(wfval2/wfval)
                        ERRTXT+="\nlog(wfval1/wfval)/(2*pi): "+str(np.log(wfval1/wfval)/(2*np.pi))
                        ERRTXT+="\nlog(wfval2/wfval)/(2*pi): "+str(np.log(wfval2/wfval)/(2*np.pi))
                        self.assertTrue(False,ERRTXT)

    def test_generated_laughlin_is_consistent_with_coordinates_used(self):
        """ Test that generated laughlin is consitent witht the ccoriantes tht are gerated at the same time"""
        self.do_test_generated_laughlin_is_consistent_with_coordinates_used(False)
    def test_generated_laughlin_is_consistent_with_coordinates_used_eff_Ns(self):
        """ Test that generated laughlin is consitent witht the ccoriantes tht are gerated at the same time even with effective Ns"""
        self.do_test_generated_laughlin_is_consistent_with_coordinates_used(True)
        
    def do_test_generated_laughlin_is_consistent_with_coordinates_used(self,UseEffNs):
        """ Test that generated laughlin is consitent witht the ccoriantes tht are gerated at the same time"""
        MaxNe=5
        MaxQh=4
        NSamples=20
        for Denom in np.arange(3)*2+1:###denom = 1,3,5
            for NbrNe in range(2,MaxNe+1):
                print("Denom,Ne:",Denom,NbrNe)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                Momenta=(NbrNe*(NbrNe-1))/2
                for NbrQh in range(0,MaxQh+1):
                    print
                    print("**************************************")
                    print("**************************************")
                    print
                    NbrFlux=NbrNe*Denom+NbrQh
                    QHPos=set_qh_pos(NbrQh)
                    if UseEffNs:
                        EffFlux=NbrFlux+random.randint(1,10)
                    else:
                        EffFlux=NbrFlux
                    WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,QHPos=QHPos,Denom=Denom,EffectiveNs=EffFlux)
                    #print("TEST: Generate the wfn and coordiantes")
                    wfval,coords=WFN.WfnValueGenerateLattice(NSamples,sampling_gap=EffFlux,therm=EffFlux*NbrNe,logform=True)
                    #print("TEST: Re-Generate the wfn")
                    wfval2=WFN.WfnValueSerial(coords,logform=True)
                    wfval3=WFN.WfnValueSerialLattice(coords,logform=True)
                    #print("TEST: Generated")
                    quotientval2=np.sum(np.abs(wfval/wfval2)-1)
                    quotientval3=np.sum(np.abs(wfval/wfval3)-1)
                    ToFDiF2=quotientval2 > 1e-8
                    ToFDiF3=quotientval3 > 1e-8

                    if np.any(ToFDiF2) or np.any(ToFDiF3):
                        np.set_printoptions(linewidth=200)
                        ERRTXT="ERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
                        BothVectors=np.real((np.vstack((wfval,wfval2,wfval3,wfval2-wfval,wfval3-wfval))).T)
                        ERRTXT+="\nwfval,serial,serial lattice, diff, diff lattice real:\n"+str(BothVectors)
                        BothVectors=np.imag((np.vstack((wfval,wfval2,wfval3,wfval2-wfval,wfval3-wfval))).T)
                        ERRTXT+="\nwfval,serial,serial lattice, diff, diff lattice  imag:\n"+str(BothVectors)
                        ERRTXT+="\nToFDiF2: "+str(ToFDiF2)
                        ERRTXT+="\nToFDiF3: "+str(ToFDiF3)
                        ERRTXT+="\nERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
                        ERRTXT+="\nNbrFlux,NbrNe,EffFLux: "+str(NbrFlux)+", "+str(NbrNe)+", "+str(EffFlux)
                        ERRTXT+="\nDenom,NbrQh: "+str(Denom)+", "+str(NbrQh)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        self.assertTrue(False,ERRTXT)
                        
                    try:
                        #print("TEST: Re-Generate the wfn from Lattice with wrong lattice")
                        wfval2=WFN.WfnValueSerialLattice(coords+np.pi)
                        #print("TEST: Generated")
                        ERRTXT="ERROR: Returned answer even tough supplied lattice is complete boncus..."
                        ERRTXT+="\nNbrFlux,NbrNe,EffFLux: "+str(NbrFlux)+", "+str(NbrNe)+", "+str(EffFlux)
                        ERRTXT+="\nDenom,NbrQh: "+str(Denom)+", "+str(NbrQh)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        self.assertTrue(False,ERRTXT)
                    except ValueError:
                        print("This error is expected!")
                    try:
                        #print("TEST: Re-Generate the wfn from Lattice with wrong lattice")
                        wfval2=WFN.WfnValueSerialLattice(coords+1j*np.pi)
                        #print("TEST: Generated")
                        ERRTXT="ERROR: Returned answer even tough supplied lattice is complete boncus..."
                        ERRTXT+="\nNbrFlux,NbrNe,EffFLux: "+str(NbrFlux)+", "+str(NbrNe)+", "+str(EffFlux)
                        ERRTXT+="\nDenom,NbrQh: "+str(Denom)+", "+str(NbrQh)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        self.assertTrue(False,ERRTXT)
                    except ValueError:
                        print("This error is expected!")
                    




    def test_serial_agree_with_each_other(self):
        """ Test that the seial evaluation of the laughlin state agree with each other"""
        self.do_test_serial_agree_with_each_other(False)
    def test_serial_agree_with_each_other_eff_Ns(self):
        """ Test that the seial evaluation of the laughlin state agree with each other even with effective Ns"""
        self.do_test_serial_agree_with_each_other(True)
        
    def do_test_serial_agree_with_each_other(self,UseEffNs):
        """ Test that the seial evaluation of the laughlin state agree with each other"""
        MaxNe=5
        MaxQh=4
        NSamples=20
        for Denom in np.arange(3)*2+1:###denom = 1,3,5
            for NbrNe in range(1,MaxNe+1):
                print("Denom,Ne:",Denom,NbrNe)
                tau=random.uniform(-.5,.5)+1j*random.uniform(.5,2)
                Momenta=(NbrNe*(NbrNe-1))/2
                for NbrQh in range(0,MaxQh+1):
                    print
                    print("**************************************")
                    print("**************************************")
                    print
                    NbrFlux=NbrNe*Denom+NbrQh
                    QHPos=set_qh_pos(NbrQh)
                    if UseEffNs:
                        EffFlux=NbrFlux+random.randint(1,10)
                    else:
                        EffFlux=NbrFlux
                    WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,QHPos=QHPos,Denom=Denom,EffectiveNs=EffFlux)

                    XList,YList,XYList=set_unique_xyint_2d(NbrNe,EffFlux,NSamples)
                    
                    #print("TEST: Re-Generate the wfn")
                    wfval1=WFN.WfnValueSerial(XYList,logform=True)
                    wfval2=WFN.WfnValueSerialLattice(XYList,logform=True)
                    #print("TEST: Generated")
                    quotientval=wfval1-wfval2
                    ToFDiF=np.logical_and(np.logical_or(np.real(quotientval) > 1e-8,
                                                        (np.exp(1j*np.imag(quotientval))-1) > 1e-8),
                                          np.real(wfval1)>-20)
                    if np.any(ToFDiF):
                        np.set_printoptions(linewidth=200)
                        ERRTXT="ERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
                        ERRTXT+="\nQhpos:\n"+str(QHPos)
                        ERRTXT+="\ncoordiantes:\n"+str(XYList)
                        BothVectors=np.real((np.vstack((wfval1,wfval2,quotientval))).T)
                        ERRTXT+="\nwfval,serial,serial lattice, diff, real:\n"+str(BothVectors)
                        BothVectors=np.imag((np.vstack((wfval1,wfval2,quotientval))).T)
                        ERRTXT+="\nwfval,serial,serial lattice, diff,  imag:\n"+str(BothVectors)
                        ERRTXT+="\nToFDiF2: "+str(ToFDiF)
                        ERRTXT+="\nERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
                        ERRTXT+="\nNbrNe, NbrFlux,EffFLux: "+str(NbrNe)+", "+str(NbrFlux)+", "+str(EffFlux)
                        ERRTXT+="\nDenom,NbrQh: "+str(Denom)+", "+str(NbrQh)
                        ERRTXT+="\ntau: "+str(tau)
                        ERRTXT+="\nK1: "+str(Momenta)
                        self.assertTrue(False,ERRTXT)
                        
                    


    def test_serial_v1(self):
        """ Test that the seial evaluation of the laughlin state agree with each other"""
        NbrNe=1
        NbrQh=2
        Denom=1
        tau=(0.122253865722+0.961006688098j)
        Momenta=0
        NbrFlux=NbrNe*Denom+NbrQh
        EffFlux=NbrFlux
        QHPos=np.array([ 0.18450802+0.87780683j,0.21106505+0.68202397j])
        print("QHPos:",QHPos)
        coords=np.zeros((1,1),dtype=np.complex128)
        coords[0,0]=2.6666666666666666+2.33333333333333j
        Xint=np.zeros((1),dtype=np.int)
        Yint=np.zeros((1),dtype=np.int)
        Xint2=np.zeros((1),dtype=np.int)
        Yint2=np.zeros((1),dtype=np.int)
        Xint[0]=8
        Yint[0]=7
        Xint2[0]=int(np.real(coords)*EffFlux)
        Yint2[0]=int(np.round(np.imag(coords)*EffFlux))
        WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,QHPos=QHPos,Denom=Denom,EffectiveNs=EffFlux)
        wfval1=WFN.WfnValueSerial(coords,logform=True)[0]
        wfval2=WFN.WfnValue(coords[:,0],logform=True)
        wfval3=WFN.WfnValueSerialLattice(coords,logform=True)[0]
        wfval4=WFN.WfnValueLattice(Xint,Yint,logform=True)
        wfval5=WFN.WfnValueLattice(Xint2,Yint2,logform=True)
        print(np.real((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
        print(np.imag((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
        if TestWfnDiffernt(wfval1,wfval2,wfval3,self.Threshold,log=True) or TestWfnDiffernt(wfval1,wfval4,wfval5,self.Threshold,log=True) :
            np.set_printoptions(linewidth=200)
            ERRTXT="ERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
            ERRTXT+="\nQhpos:\n"+str(QHPos)
            ERRTXT+="\ncoordiantes:\n"+str(coords)
            ERRTXT+="\n normal serial, normal, serial lattice, lattice, lattice v2"
            ERRTXT+="\n "+str(np.real((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
            ERRTXT+="\n "+str(np.imag((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
            ERRTXT+="\nERROR: The differnt ways of computing laughlin do not agree"
            ERRTXT+="\nNbrNe, NbrFlux,EffFLux: "+str(NbrNe)+", "+str(NbrFlux)+", "+str(EffFlux)
            ERRTXT+="\nDenom,NbrQh: "+str(Denom)+", "+str(NbrQh)
            ERRTXT+="\nX1,Y1: "+str(Xint)+", "+str(Yint)
            ERRTXT+="\nX2,Y2: "+str(Xint2)+", "+str(Yint2)
            ERRTXT+="\ntau: "+str(tau)
            ERRTXT+="\nK1: "+str(Momenta)
            self.assertTrue(False,ERRTXT)
                        
   ###Expteced the numbers: 1.12696490e+01 + 4.05750572e+00j
   #### and                 1.10080152e+01 + 4.22076110e+00j


    def test_serial_v2(self):
        """ Test that the seial evaluation of the laughlin state agree with each other"""
        NbrNe=1
        NbrQh=0
        Denom=1
        tau=(0.297191905407+1.73783459222j)
        Momenta=0
        NbrFlux=1
        EffFlux=9
        QHPos=None
        print("QHPos:",QHPos)
        coords=np.zeros((1,1),dtype=np.complex128)
        coords[0,0]=0.7777777777777777777+1.444444444444444444j
        Xint=np.zeros((1),dtype=np.int)
        Yint=np.zeros((1),dtype=np.int)
        Xint2=np.zeros((1),dtype=np.int)
        Yint2=np.zeros((1),dtype=np.int)
        Xint[0]=7
        Yint[0]=13
        Xint2[0]=int(np.real(coords)*EffFlux)
        Yint2[0]=int(np.round(np.imag(coords)*EffFlux))
        WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,QHPos=QHPos,Denom=Denom,EffectiveNs=EffFlux)
        wfval1=WFN.WfnValueSerial(coords,logform=True)[0]
        wfval2=WFN.WfnValue(coords[:,0],logform=True)
        wfval3=WFN.WfnValueSerialLattice(coords,logform=True)[0]
        wfval4=WFN.WfnValueLattice(Xint,Yint,logform=True)
        wfval5=WFN.WfnValueLattice(Xint2,Yint2,logform=True)
        print(np.real((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
        print(np.imag((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
        if TestWfnDiffernt(wfval1,wfval2,wfval3,self.Threshold,log=True) or TestWfnDiffernt(wfval1,wfval4,wfval5,self.Threshold,log=True) :
            np.set_printoptions(linewidth=200)
            ERRTXT="ERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
            ERRTXT+="\nQhpos:\n"+str(QHPos)
            ERRTXT+="\ncoordiantes:\n"+str(coords)
            ERRTXT+="\n normal serial, normal, serial lattice, lattice, lattice v2"
            ERRTXT+="\n "+str(np.real((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
            ERRTXT+="\n "+str(np.imag((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
            ERRTXT+="\nERROR: The differnt ways of computing laughlin do not agree"
            ERRTXT+="\nNbrNe, NbrFlux,EffFLux: "+str(NbrNe)+", "+str(NbrFlux)+", "+str(EffFlux)
            ERRTXT+="\nDenom,NbrQh: "+str(Denom)+", "+str(NbrQh)
            ERRTXT+="\nX1,Y1: "+str(Xint)+", "+str(Yint)
            ERRTXT+="\nX2,Y2: "+str(Xint2)+", "+str(Yint2)
            ERRTXT+="\ntau: "+str(tau)
            ERRTXT+="\nK1: "+str(Momenta)
            self.assertTrue(False,ERRTXT)
                        
   ###Expteced the numbers: -9.19412202e-01 + 2.06305596e+00j
   #### and                 -9.19412202e-01 + 6.66792559e-01j

   


    def test_serial_v3(self):
        """ Test that the seial evaluation of the laughlin state agree with each other"""
        NbrNe=1
        NbrQh=0
        Denom=5
        tau=(0.447498603869+1.39174347225j)
        Momenta=0
        NbrFlux=5
        EffFlux=10
        QHPos=None
        print("QHPos:",QHPos)
        coords=np.zeros((1,1),dtype=np.complex128)
        coords[0,0]=0.1+1.5j
        Xint=np.zeros((1),dtype=np.int)
        Yint=np.zeros((1),dtype=np.int)
        Xint2=np.zeros((1),dtype=np.int)
        Yint2=np.zeros((1),dtype=np.int)
        Xint[0]=1
        Yint[0]=15
        Xint2[0]=int(np.real(coords)*EffFlux)
        Yint2[0]=int(np.round(np.imag(coords)*EffFlux))
        WFN=FQHTorusLaughlinWF(NbrNe,Momenta,Tau=tau,QHPos=QHPos,Denom=Denom,EffectiveNs=EffFlux)
        wfval1=WFN.WfnValueSerial(coords,logform=True)[0]
        wfval2=WFN.WfnValue(coords[:,0],logform=True)
        wfval3=WFN.WfnValueSerialLattice(coords,logform=True)[0]
        wfval4=WFN.WfnValueLattice(Xint,Yint,logform=True)
        wfval5=WFN.WfnValueLattice(Xint2,Yint2,logform=True)
        print(np.real((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
        print(np.imag((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
        if TestWfnDiffernt(wfval1,wfval2,wfval3,self.Threshold,log=True) or TestWfnDiffernt(wfval1,wfval4,wfval5,self.Threshold,log=True) :
            np.set_printoptions(linewidth=200)
            ERRTXT="ERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
            ERRTXT+="\nQhpos:\n"+str(QHPos)
            ERRTXT+="\ncoordiantes:\n"+str(coords)
            ERRTXT+="\n normal serial, normal, serial lattice, lattice, lattice v2"
            ERRTXT+="\n "+str(np.real((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
            ERRTXT+="\n "+str(np.imag((np.vstack((wfval1,wfval2,wfval3,wfval4,wfval5))).T))
            ERRTXT+="\nERROR: The differnt ways of computing laughlin do not agree"
            ERRTXT+="\nNbrNe, NbrFlux,EffFLux: "+str(NbrNe)+", "+str(NbrFlux)+", "+str(EffFlux)
            ERRTXT+="\nDenom,NbrQh: "+str(Denom)+", "+str(NbrQh)
            ERRTXT+="\nX1,Y1: "+str(Xint)+", "+str(Yint)
            ERRTXT+="\nX2,Y2: "+str(Xint2)+", "+str(Yint2)
            ERRTXT+="\ntau: "+str(tau)
            ERRTXT+="\nK1: "+str(Momenta)
            self.assertTrue(False,ERRTXT)
                        
   ###Expteced the numbers: -3.81777429e+01 +4.43362087e+00j
   #### and                 -4.21040728e+01 +3.32805343e+00j


   

                        
                        

def TestWfnZero(wfval,wfval1,wfval2,WfnThreshold):
    return ((np.abs(wfval) < WfnThreshold)
        or (np.abs(wfval1) < WfnThreshold)
        or (np.abs(wfval2) < WfnThreshold))


def TestWfnDiffernt(wfval,wfval1,wfval2,Threshold,log=False):
    return (TestWfnDiff(wfval,wfval1,Threshold,log=log) or
            TestWfnDiff(wfval,wfval2,Threshold,log=log) or
            TestWfnDiff(wfval1,wfval2,Threshold,log=log))

def TestWfnDiff(wfval,wfval1,Threshold,log=False):
    if not log:
        IsZero = (np.abs(wfval) == 0.0 or
                  np.abs(wfval1) == 0.0)
    else:
        IsZero = ((np.real(wfval) < -30.0) or
                  (np.real(wfval1) < -30.0))
    if not log:
        IsSame=(np.abs(wfval/wfval1-1) > Threshold)
    else:
        IsSame=((np.abs(np.exp(wfval-wfval1))-1) > Threshold)
    return IsSame and not IsZero

def TestWfnSame(wfval,wfval1,Threshold=1.0e-6):
    return (np.abs(wfval/wfval1-1) < Threshold)


def set_unique_xyint(Ne,Ns):
    ###Make sure all values are distributed such that two values are not on top of each other
    XList=np.arange(Ns)[np.argsort(np.random.rand(Ns))[:Ne]]
    YList=np.arange(Ns)[np.argsort(np.random.rand(Ns))[:Ne]]
    ###Now make them mote out of the fundamental domain
    StepsX=np.array(np.round(4*np.random.rand(Ne)-2),dtype=np.int)
    StepsY=np.array(np.round(4*np.random.rand(Ne)-2),dtype=np.int)
    XList+=StepsX*Ns
    YList+=StepsY*Ns
    #print("StepsX:",StepsX)
    #print("StepsY:",StepsY)
    XYList1=(1.0*XList+1j*YList)/Ns
    return XList,YList,XYList1

def set_unique_xyint_2d(Ne,Ns,Samples):
    XList=np.zeros((Samples,Ne),dtype=np.long)
    YList=np.zeros((Samples,Ne),dtype=np.long)
    XYList=np.zeros((Samples,Ne),dtype=np.complex128)
    for n in range(Samples):
        (XList[n,:],YList[n,:],XYList[n,:])=set_unique_xyint(Ne,Ns)
    return XList,YList,XYList




def set_qh_pos(NbrHq):
    QHPos=np.random.rand(NbrHq)+1j*np.random.rand(NbrHq)
    return QHPos

def set_xylist(NbrNe):
    XYList=2*np.random.rand(NbrNe)-1+1j*(2*np.random.rand(NbrNe)-1)
    XYList1=np.array(XYList)
    XYList2=np.array(XYList)
    return XYList,XYList1,XYList2
    
if __name__ == '__main__':
    unittest.main()
