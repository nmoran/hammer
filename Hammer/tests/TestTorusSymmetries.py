#! /usr/bin/env python

"""
 Unit tests to test that the torus symmetries are working as expected.

"""
import unittest
from hammer.TorusDiagonalise import TorusDiagonalise
from hammer.TorusDiagonalise import TorusCoulombStates
from hammer.TorusDiagonalise import TorusCoulombHamiltonian
from hammer.HammerArgs import HammerArgsParser
import hammer.matrixutils as matrixutils

import numpy as np

class TestTorusSymmetries(unittest.TestCase):

    def test_five_particle_fermionic_centre_of_mass_symmetry(self):
        # get all the states in the sector along with their energies.
        Ne=4
        Nphi=8
        k1,k2 = 1,1
        basis, states, energies = TorusCoulombStates(Ne, Nphi, Momentum=k1, COMMomentum=k2, Energies=True)

        # now convert the states to the full basis and check their energies there.
        full_states = []
        full_basis = None
        for state in states:
            full_basis, full_state = basis.ConvertToFullBasis(state, full_basis)
            full_states.append(full_state)

        # get the Hamiltonian in the full sector
        H = TorusCoulombHamiltonian(Ne,Nphi,k1)

        full_energies = []
        for full_state in full_states:
            full_energies.append(matrixutils.ExpectationValue(H, full_state))

        # check ground state is zero (at least close)
        for i in range(len(energies)):
            if np.abs(energies[i] - full_energies[i]) > 1e-10:
                print('Energies ' + str(i) + ' differ: ' + str((energies[i], full_energies[i])))
            self.assertTrue(np.abs(energies[i] - full_energies[i]) < 1e-10)



if __name__ == '__main__':
    unittest.main()
