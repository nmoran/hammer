#! /usr/bin/env python

"""
 Unit tests to test TimeEvolution functionality. Compare computed Fock coefficeints agains exactly evolved coefficients for small systems

"""
import unittest
import random
import petsc4py.PETSc as PETSc
from hammer.FQHTimeEvolution import TimeEvolveNPExact
from hammer.FQHTimeEvolution import TimeEvolveNP,TimeEvolvePETSc
from hammer.FQHTimeEvolution import StepsTimeEvolveNP,StepsTimeEvolvePETSc
from hammer.TorusDiagonalise import TorusDiagonalise,BuildBasis,TorusCoulombHamiltonian
from hammer.FQHTorusHamiltonian import FQHTorusHamiltonian
from hammer.HammerArgs import HammerArgsParser
from hammer.SphereDiagonalise import SphereArgsParser,SphereDiagonalise
import hammer.matrixutils as matutils

import numpy as np

class TestTimeEvolution(unittest.TestCase):

    def setUp(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False


    def test_Ne_3_eigevalue_dense_phase(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.ReturnEigenVectors = True
        self.Opts.Tau=np.random.uniform()+1j*(np.random.uniform()*.5+1)
        
        TorusHamiltonian = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum,Tau=self.Opts.Tau)
        TorusHamiltonianNP = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum,numpy=True,Tau=self.Opts.Tau)
        print("TorusHamiltonian:",TorusHamiltonian)
        (Nstates,Nstates)=TorusHamiltonianNP.shape
        print("Nstates:",Nstates)

        self.Opts.NbrStates=Nstates
        self.Opts.NbrEigvals=Nstates
        Data = TorusDiagonalise(self.Opts);

        ###Extract the diaognalized energies
        Vectors=Data['EigenVectors']
        EnergyList=np.array([Energy[0] for Energy in Data['EigenValues'] ])
        print("Data:",Data)
        print "Vectors:",Vectors
        print "Energies:",EnergyList

        ##### Use NP to get all the eigenvectors and eigenvalues
        [EnergiesNP,EigenVectorsNP]=np.linalg.eigh(TorusHamiltonianNP)
        print "EnergiesNP:",EnergiesNP

                
        MESSAGE="The energies are not the same"
        self.assertTrue(np.max(np.abs(EnergyList-EnergiesNP))<1e-10,MESSAGE)
        
        ####Initialize the time-evolution-routine

        for vecno in range(Nstates):
            NPVector=np.array(Vectors[vecno])
            NPEigvectorVector=EigenVectorsNP[:,vecno]

            print "Vec   ",vecno,":\n",NPVector
            print "Vec NP",vecno,":\n",NPEigvectorVector
            print "Vec   ",vecno,":\n",np.abs(NPVector)
            print "Vec NP",vecno,":\n",np.abs(NPEigvectorVector)
            
            Energy=EnergyList[vecno]
            time=random.uniform(0,1)
            phase=np.exp(1j*time*Energy)
            NewVector=NPVector*phase
            TEVectorNP=TimeEvolveNPExact(EnergiesNP,EigenVectorsNP,NPVector,time)
            MESSAGE="The exactly time evolved eigenvector and Hamiltonian evolved eigenvector are not the same"
            MESSAGEORIGIN="The exactly time evolved eigenvector has not changed from the initial vector"
            print "ExactVector:\n",NewVector
            print "TEVectorNP:\n",TEVectorNP
            Diff=np.abs(NewVector-TEVectorNP)
            DiffOrigin=np.abs(NPVector-TEVectorNP)
            Quota=TEVectorNP/NewVector
            QuotaOrigin=TEVectorNP/NPVector
            print "Energy,time,phase:",Energy,time,phase
            print "DiffOrigin:\n",DiffOrigin
            print "Diff:\n",Diff
            print "QuotaOrigin:\n",QuotaOrigin
            print "Quota:\n",Quota
            self.assertTrue(np.sum(DiffOrigin)>1.0e-10,MESSAGEORIGIN)
            self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)
        
    def test_Ne_3_random_state_dense(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.ReturnEigenVectors = True


        TorusHamiltonianPETSc = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum)
        TorusHamiltonianNP = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum,numpy=True)
        print("TorusHamiltonianPETSc:",TorusHamiltonianPETSc)
        (Nstates,Nstates)=TorusHamiltonianNP.shape
        print("Nstates:",Nstates)

        self.Opts.NbrStates=Nstates
        self.Opts.NbrEigvals=Nstates
        ####Diagonalize the hamiltonian
        Data = TorusDiagonalise(self.Opts);

        ###Extract the diaognalized energies
        Vectors=Data['EigenVectors']
        EnergyList=np.array([Energy[0] for Energy in Data['EigenValues'] ])
        print "Energies:",EnergyList

        [EnergiesNP,EigenVectorsNP]=np.linalg.eigh(TorusHamiltonianNP)
        print "EnergiesNP:",EnergiesNP
        
        ####Initialize the time-evolution-routine
        time=random.uniform(0,1)
        ####Initalize a random unit complex unit vector
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector)
        print "StartVector:\n",NPVector

        ####Evolve the vector some time
        TEVectorNP=TimeEvolveNPExact(EnergiesNP,EigenVectorsNP,NPVector,time)
        ####Do the exact evolution by mapping the vector onto the differnt eigenvectors
        TEVectorExact=TEVectorNP*0j
        for vecno in range(Nstates):
            print "Vec",vecno,":\n",
            LocalVector=np.array(Vectors[vecno])
            Energy=EnergyList[vecno]
            phase=np.exp(1j*time*Energy)

            Overlap=np.vdot(LocalVector,NPVector)
            NewVector=LocalVector*phase*Overlap
            TEVectorExact+=NewVector

        print "TEVectorExact:\n",TEVectorExact
        print "TEVectorNP:\n",TEVectorNP
        Diff=np.abs(TEVectorExact-TEVectorNP)
        Quota=TEVectorNP/TEVectorExact
        print "time:",Energy,time,phase
        print "Diff:\n",Diff
        print "Quota:\n",Quota
        MESSAGE="The exactly time evolved eigenvector and NP Hamiltonian evolved eigenvector are not the same"
        self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)


        #### Now we do the same thing using PETCs to make sure we understand how to set up such vectors

        PETScVector=PETSc.Vec()
        PETScVector.createWithArray(NPVector)
        
        ####Evolve the vector some time
        TEVectorPETSc,order=TimeEvolvePETSc(TorusHamiltonianPETSc,PETScVector,time)

        TENP=np.array(TEVectorPETSc)
        TENPMemoryWiev=TEVectorPETSc.getArray()
        print "TEVectorExact:\n",TEVectorExact
        print "TEVectorPETSC:\n",TENP
        print "TENPMemoryWiev:\n",TENPMemoryWiev
        Diff=np.abs(TEVectorExact-TENP)
        DiffMW=np.abs(TEVectorExact-TENPMemoryWiev)
        Quota=TENP/TEVectorExact
        QuotaMW=TENPMemoryWiev/TEVectorExact
        print "time:",Energy,time,phase
        print "Diff:\n",Diff
        print "Quota:\n",Quota
        print "DiffMW:\n",DiffMW
        print "QuotaMW:\n",QuotaMW
        MESSAGE="The exactly time evolved eigenvector and PETCs Hamiltonian evolved eigenvector are not the same"
        self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)


            
    def test_Ne_3_eigevalue_phase(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.ReturnEigenVectors = True


        TorusHamiltonian = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum)
        TorusHamiltonianNP = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum,numpy=True)
        print("TorusHamiltonian:",TorusHamiltonian)
        (Nstates,Nstates)=TorusHamiltonianNP.shape
        print("Nstates:",Nstates)

        self.Opts.NbrStates=Nstates
        self.Opts.NbrEigvals=Nstates
        Data = TorusDiagonalise(self.Opts);

        ###Extract the diaognalized energies
        Vectors=Data['EigenVectors']
        EnergyList=np.array([Energy[0] for Energy in Data['EigenValues'] ])
        print("Data:",Data)
        print "Energies:",EnergyList
        print "Vectors:",Vectors

        ####Initialize the time-evolution-routine

        for vecno in range(Nstates):
            print "Vec",vecno,":\n",np.array(Vectors[vecno])
            NPVector=np.array(Vectors[vecno])
            Energy=EnergyList[vecno]
            time=random.uniform(0,1)
            phase=np.exp(1j*time*Energy)
            NewVector=NPVector*phase
            TEVectorNP=TimeEvolveNP(TorusHamiltonianNP,NPVector,time)
            MESSAGE="The exactly time evolved eigenvector and Hamiltonian evolved eigenvector are not the same"
            MESSAGEORIGIN="The exactly time evolved eigenvector has not changed from the initial vector"
            print "ExactVector:\n",NewVector
            print "TEVectorNP:\n",TEVectorNP
            Diff=np.abs(NewVector-TEVectorNP)
            DiffOrigin=np.abs(NPVector-TEVectorNP)
            Quota=TEVectorNP/NewVector
            QuotaOrigin=TEVectorNP/NPVector
            print "Energy,time,phase:",Energy,time,phase
            print "DiffOrigin:\n",DiffOrigin
            print "Diff:\n",Diff
            print "QuotaOrigin:\n",QuotaOrigin
            print "Quota:\n",Quota
            self.assertTrue(np.sum(DiffOrigin)>1.0e-10,MESSAGEORIGIN)
            self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)
            print "----- Same test for PETCs-----------"
            TEVectorPETCs,order=TimeEvolvePETSc(TorusHamiltonian,Vectors[vecno],time)
            TEVector=np.array(TEVectorPETCs)
            print "ExactVector:\n",NewVector
            print "TEVector:\n",TEVector
            Diff=np.abs(NewVector-TEVector)
            DiffOrigin=np.abs(NPVector-TEVector)
            Quota=TEVector/NewVector
            QuotaOrigin=TEVector/NPVector
            print "Energy,time,phase:",Energy,time,phase
            print "DiffOrigin:\n",DiffOrigin
            print "Diff:\n",Diff
            print "QuotaOrigin:\n",QuotaOrigin
            print "Quota:\n",Quota
            MESSAGE="The exactly time evolved eigenvector and PETSc Hamiltonian evolved eigenvector are not the same"
            MESSAGEORIGIN="The  PETSc time evolved eigenvector has not changed from the initial vector"
            self.assertTrue(np.sum(DiffOrigin)>1.0e-10,MESSAGEORIGIN)
            self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)
                        

    def test_Ne_3_random_state(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.ReturnEigenVectors = True


        TorusHamiltonianPETSc = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum)
        TorusHamiltonianNP = TorusCoulombHamiltonian(self.Opts.Particles,self.Opts.NbrFlux,self.Opts.Momentum,numpy=True)
        print("TorusHamiltonianPETSc:",TorusHamiltonianPETSc)
        (Nstates,Nstates)=TorusHamiltonianNP.shape
        print("Nstates:",Nstates)

        self.Opts.NbrStates=Nstates
        self.Opts.NbrEigvals=Nstates
        ####Diagonalize the hamiltonian
        Data = TorusDiagonalise(self.Opts);

        ###Extract the diaognalized energies
        Vectors=Data['EigenVectors']
        EnergyList=np.array([Energy[0] for Energy in Data['EigenValues'] ])
        print "Energies:",EnergyList
        
        ####Initialize the time-evolution-routine
        time=random.uniform(0,1)
        ####Initalize a random unit complex unit vector
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector)
        print "StartVector:\n",NPVector

        ####Evolve the vector some time
        TEVectorNP=TimeEvolveNP(TorusHamiltonianNP,NPVector,time)
        ####Do the exact evolution by mapping the vector onto the differnt eigenvectors
        TEVectorExact=TEVectorNP*0j
        for vecno in range(Nstates):
            print "Vec",vecno,":\n",
            LocalVector=np.array(Vectors[vecno])
            Energy=EnergyList[vecno]
            phase=np.exp(1j*time*Energy)

            Overlap=np.vdot(LocalVector,NPVector)
            NewVector=LocalVector*phase*Overlap
            TEVectorExact+=NewVector

        print "TEVectorExact:\n",TEVectorExact
        print "TEVectorNP:\n",TEVectorNP
        Diff=np.abs(TEVectorExact-TEVectorNP)
        Quota=TEVectorNP/TEVectorExact
        print "time:",Energy,time,phase
        print "Diff:\n",Diff
        print "Quota:\n",Quota
        MESSAGE="The exactly time evolved eigenvector and NP Hamiltonian evolved eigenvector are not the same"
        self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)


        #### Now we do the same thing using PETCs to make sure we understand how to set up such vectors

        PETScVector=PETSc.Vec()
        PETScVector.createWithArray(NPVector)
        
        ####Evolve the vector some time
        TEVectorPETSc,order=TimeEvolvePETSc(TorusHamiltonianPETSc,PETScVector,time)

        TENP=np.array(TEVectorPETSc)
        TENPMemoryWiev=TEVectorPETSc.getArray()
        print "TEVectorExact:\n",TEVectorExact
        print "TEVectorPETSC:\n",TENP
        print "TENPMemoryWiev:\n",TENPMemoryWiev
        Diff=np.abs(TEVectorExact-TENP)
        DiffMW=np.abs(TEVectorExact-TENPMemoryWiev)
        Quota=TENP/TEVectorExact
        QuotaMW=TENPMemoryWiev/TEVectorExact
        print "time:",Energy,time,phase
        print "Diff:\n",Diff
        print "Quota:\n",Quota
        print "DiffMW:\n",DiffMW
        print "QuotaMW:\n",QuotaMW
        MESSAGE="The exactly time evolved eigenvector and PETCs Hamiltonian evolved eigenvector are not the same"
        self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)




    def test_sphere_Ne_3_Laughlin_eigevalue_phase(self):
        self.Opts = SphereArgsParser()
        self.Opts.parseArgs('')
        self.Opts.WriteEnergies = False
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 4
        self.Opts.Momentum = 0
        self.Opts.ReturnEigenVectors = True
        self.Opts.TwiceLzMax = 9
        self.Opts.PseudoPotentials=np.zeros(self.Opts.TwiceLzMax+1,dtype=float)
        self.Opts.PseudoPotentials[1]=1
        print "PseudoPotentials:\n",self.Opts.PseudoPotentials

        self.Opts.ReturnHamtiltonian=True
        SpHam=SphereDiagonalise(self.Opts)
        SPHamNP=matutils.PETScMatToNumpyArray(SpHam)
        print("SPHamNP:",SPHamNP)
        

        (Nstates,Nstates)=SPHamNP.shape
        print("Nstates:",Nstates)
        self.Opts.NbrStates=Nstates
        self.Opts.NbrEigvals=Nstates
        self.Opts.ReturnHamtiltonian=False
        Data=SphereDiagonalise(self.Opts)
        print "Data:",Data
        
        ###Extract the diaognalized enrgies
        Vectors=Data['EigenVectors']
        EnergyList=np.array([Energy[0] for Energy in Data['EigenValues'] ])
        print("Data:",Data)
        print "Energies:",EnergyList
        print "Vectors:",Vectors

        ####Initialize the time-evolution-routine

        for vecno in range(Nstates):
            print "Vec",vecno,":\n",np.array(Vectors[vecno])
            NPVector=np.array(Vectors[vecno])
            Energy=EnergyList[vecno]
            time=random.uniform(0.1,.5)
            phase=np.exp(1j*time*Energy)
            NewVector=NPVector*phase
            TEVectorNP=TimeEvolveNP(SPHamNP,NPVector,time)
            MESSAGE="The exactly time evolved eigenvector and Hamiltonian evolved eigenvector are not the same"
            print "ExactVector:\n",NewVector
            print "TEVectorNP:\n",TEVectorNP
            Diff=np.abs(NewVector-TEVectorNP)
            Quota=TEVectorNP/NewVector
            print "Energy,time,phase:",Energy,time,phase
            print "Diff:\n",Diff
            print "Quota:\n",Quota
            self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)
            print "----- Same test for PETCs-----------"
            TEVectorPETCs,order=TimeEvolvePETSc(SpHam,Vectors[vecno],time)
            TEVector=np.array(TEVectorPETCs)
            print "ExactVector:\n",NewVector
            print "TEVector:\n",TEVector
            Diff=np.abs(NewVector-TEVector)
            Quota=TEVector/NewVector
            print "Energy,time,phase:",Energy,time,phase
            print "Diff:\n",Diff
            print "Quota:\n",Quota
            MESSAGE="The exactly time evolved eigenvector and PETSc Hamiltonian evolved eigenvector are not the same"
            self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)



    def test_sphere_Ne_3_Laughlin_several_steps(self):
        self.Opts = SphereArgsParser()
        self.Opts.parseArgs('')
        self.Opts.WriteEnergies = False
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 3
        self.Opts.Momentum = 0
        self.Opts.ReturnEigenVectors = True
        self.Opts.TwiceLzMax = 6
        self.Opts.PseudoPotentials=np.zeros(self.Opts.TwiceLzMax+1,dtype=float)
        self.Opts.PseudoPotentials[1]=1
        print "PseudoPotentials:\n",self.Opts.PseudoPotentials


        deltatime=random.uniform(0.1,.5)
        timeSteps=np.random.randint(10,20)
        Times=(np.arange(timeSteps)+1)*deltatime

        print "Times:",Times

        self.Opts.ReturnHamtiltonian=True
        SpHam=SphereDiagonalise(self.Opts)
        SPHamNP=matutils.PETScMatToNumpyArray(SpHam)
        print("SPHamNP:",SPHamNP)
        


        (Nstates,Nstates)=SPHamNP.shape
        print("Nstates:",Nstates)
        self.Opts.NbrStates=Nstates
        self.Opts.NbrEigvals=Nstates
        self.Opts.ReturnHamtiltonian=False
        Data=SphereDiagonalise(self.Opts)
        print "Data:",Data
        
        ###Extract the diaognalized enrgies
        Vectors=Data['EigenVectors']
        EnergyList=np.array([Energy[0] for Energy in Data['EigenValues'] ])
        print("Data:",Data)
        print "Energies:",EnergyList
        print "Vectors:",Vectors

        ####Initialize the time-evolution-routine
        ####Initalize a random unit complex unit vector
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector)
        print "StartVector:\n",NPVector


        ####Do the exact evolution by mapping the vector onto the differnt eigenvectors
        TEVectorExact=np.zeros((Nstates,timeSteps),dtype=np.complex128)
        for vecno in range(Nstates):
            print "Vec",vecno,":\n",
            LocalVector=np.array(Vectors[vecno])
            Energy=EnergyList[vecno]
            phase=np.exp(1j*Times*Energy)
            print "phase:\n",phase
            Overlap=np.vdot(LocalVector,NPVector)
            for step in range(timeSteps):
                NewVector=LocalVector*phase[step]*Overlap
                TEVectorExact[:,step]+=NewVector

        ####Evolve the np vector some time
        TEVectorNP=StepsTimeEvolveNP(SPHamNP,NPVector,deltatime,timeSteps)
        PETScVector=PETSc.Vec()
        PETScVector.createWithArray(NPVector)
        ####Evolve the PETSc vector some time
        TEVectorPETSc,TimeSteps=StepsTimeEvolvePETSc(SpHam,PETScVector,deltatime,timeSteps)

            
        print "TEVectorExact:\n",TEVectorExact
        print "TEVectorNP:\n",TEVectorNP
        Diff=np.abs(TEVectorExact-TEVectorNP)
        Quota=TEVectorNP/TEVectorExact
        print "Quota:\n",Quota
        print "Diff:\n",Diff
        print "sum(Diff):\n",np.sum(Diff,axis=0)
        MESSAGE="The exactly time evolved eigenvector and NP Hamiltonian evolved eigenvector are not the same"
        self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)


        #### Now we do the same thing using PETCs to make sure we understand how to set up such vectors
        TEVectorNPPETCs=np.zeros((Nstates,timeSteps),dtype=np.complex128)
        for step in range(timeSteps):
            TENPMemoryWiev=TEVectorPETSc[step+1].getArray()
            print "TEVectorExact:\n",TEVectorExact[:,step]
            print "TENPMemoryWiev:\n",TENPMemoryWiev
            DiffMW=np.abs(TEVectorExact[:,step]-TENPMemoryWiev)
            QuotaMW=TENPMemoryWiev/TEVectorExact[:,step]
            print "step:",step
            print "DiffMW:\n",DiffMW
            print "QuotaMW:\n",QuotaMW
            MESSAGE="The exactly time evolved eigenvector and PETCs Hamiltonian evolved eigenvector are not the same"
            self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)

            TEVectorNPPETCs[:,step]=TENPMemoryWiev
            
        print "TEVectorExact:\n",TEVectorExact
        print "TEVectorNPPETCs:\n",TEVectorNPPETCs
        Diff=np.abs(TEVectorExact-TEVectorNPPETCs)
        Quota=TEVectorNPPETCs/TEVectorExact
        print "Quota:\n",Quota
        print "Diff:\n",Diff
        print "sum(Diff):\n",np.sum(Diff,axis=0)
        MESSAGE="The exactly time evolved eigenvector and NP Hamiltonian evolved eigenvector are not the same"
        self.assertTrue(np.sum(Diff)<1.0e-10,MESSAGE)

            
        


        
if __name__ == '__main__':
    unittest.main()


