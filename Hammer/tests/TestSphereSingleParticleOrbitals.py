#! /usr/bin/env python
from __future__ import print_function

"""
 Unit tests to test TimeEvolution functionality. Compare computed Fock coefficeints agains exactly evolved coefficients for small systems

"""
import unittest
import random
import petsc4py.PETSc as PETSc
import hammer.FQHSphereWF as FQHSphereWF
import hammer.FQHTorusWF as FQHTorusWF
from hammer.FQHSphereBasis import FQHSphereBasis
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHSphereWF import  MollweideConversion

import numpy as np

class TestSphereOrbitals(unittest.TestCase):

    def setUp(self):
        self.Threshold=1e-10

    def test_generate_sphere_orbitals(self):
        #### Randmoly select a few differnt sphere sizes, orbital numbers and angular variables
        NumLz=4
        NumAns=5
        MaxLz=20
        for indxLz in range(NumLz):
            ### Generate a twize max Lz
            TwiceMaxLz=np.random.randint(0,MaxLz)
            ###Define the Wave function object
            Wfn=FQHSphereWF(TwiceMaxLz)
            for OrbNo in range(TwiceMaxLz+1):
                for indxAng in range(NumAns):
                    Azimutal=np.random.random()*np.pi
                    Polar=np.random.random()*2*np.pi
                    Qvalue=TwiceMaxLz/2.0
                    print("TwiceMaxLz,OrbNo:",TwiceMaxLz,OrbNo)
                    print("Azimutal,Polar:",Azimutal,Polar)
                    ###Test -azimutal gives same value up to pahse
                    Azimutal1=-Azimutal
                    Polar1=Polar+np.pi
                    ###Test aximutal + 2*pi gives same values up to phase
                    Azimutal2=2*np.pi-Azimutal
                    Polar2=Polar+np.pi
                    ###Test polar + 2*pi gives same values up to phase
                    Azimutal3=Azimutal
                    Polar3=Polar+2*np.pi
                    ###The expected phases
                    Phase1=np.exp(-1j*np.pi*Qvalue)
                    Phase2=(-1.0)**TwiceMaxLz*np.exp(-1j*np.pi*Qvalue)
                    Phase3=np.exp(1j*2*np.pi*Qvalue)
                    Wf0=Wfn.WFValue(Azimutal +1j*Polar ,OrbNo)
                    Wf1=Wfn.WFValue(Azimutal1+1j*Polar1,OrbNo)*Phase1
                    Wf2=Wfn.WFValue(Azimutal2+1j*Polar2,OrbNo)*Phase2
                    Wf3=Wfn.WFValue(Azimutal3+1j*Polar3,OrbNo)*Phase3
                    if TestWfnDiffernt(Wf0,Wf1,Wf2,self.Threshold) or TestWfnDiffernt(Wf1,Wf2,Wf3,self.Threshold) :
                        ERRMESSAGE=""
                        ERRMESSAGE+="\nERROR: Wfns are not invarint under shifts of angles"
                        ERRMESSAGE+="\nTwiceMaxLz,Q,OrbNo: "+str(TwiceMaxLz)+","+str(TwiceMaxLz/2.0)+","+str(OrbNo)
                        ERRMESSAGE+="\nAzimutal: "+PrintZeroSmall(Azimutal)
                        ERRMESSAGE+="\nAzimutal1: "+PrintZeroSmall(Azimutal1)
                        ERRMESSAGE+="\nAzimutal2: "+PrintZeroSmall(Azimutal2)
                        ERRMESSAGE+="\nAzimutal3: "+PrintZeroSmall(Azimutal3)
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nPolar: "+PrintZeroSmall(Polar)
                        ERRMESSAGE+="\nPolar1: "+PrintZeroSmall(Polar1)
                        ERRMESSAGE+="\nPolar2: "+PrintZeroSmall(Polar2)
                        ERRMESSAGE+="\nPolar3: "+PrintZeroSmall(Polar3)
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nwfval: "+PrintZeroSmall(Wf0)
                        ERRMESSAGE+="\nwfval1:"+PrintZeroSmall(Wf1)
                        ERRMESSAGE+="\nwfval2:"+PrintZeroSmall(Wf2)
                        ERRMESSAGE+="\nwfval3:"+PrintZeroSmall(Wf3)
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nPhase1: "+PrintZeroSmall(Phase1)
                        ERRMESSAGE+="\nPhase2: "+PrintZeroSmall(Phase2)
                        ERRMESSAGE+="\nPhase3: "+PrintZeroSmall(Phase3)
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nwfval1/wfval: "+PrintZeroSmall(Wf1/Wf0)
                        ERRMESSAGE+="\nwfval2/wfval: "+PrintZeroSmall(Wf2/Wf0)
                        ERRMESSAGE+="\nwfval3/wfval: "+PrintZeroSmall(Wf3/Wf0)
                        ERRMESSAGE+="\nlog(wfval1/wfval)/(2*pi): "+PrintZeroSmall(np.log(Wf1/Wf0)/(2*np.pi))
                        ERRMESSAGE+="\nlog(wfval2/wfval)/(2*pi): "+PrintZeroSmall(np.log(Wf2/Wf0)/(2*np.pi))
                        ERRMESSAGE+="\nlog(wfval3/wfval)/(2*pi): "+PrintZeroSmall(np.log(Wf3/Wf0)/(2*np.pi))
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nWave functions are not the same"
                        print(ERRMESSAGE)
                        self.assertTrue(False,ERRMESSAGE)

        
    def test_vector_sphere_argument(self):
        """
        Test that the sphere WFn can take vector arguments
        """
        NumLz=4
        NumAns=50
        MaxLz=10
        for indxLz in range(NumLz):
            ### Generate a twize max Lz
            TwiceMaxLz=np.random.randint(0,MaxLz)
            ###Define the Wave function object
            Wfn=FQHSphereWF(TwiceMaxLz)
            for OrbNo in range(TwiceMaxLz+1):
                Azimutal=np.random.random(NumAns)*np.pi
                Polar=np.random.random(NumAns)*2*np.pi
                APVal=Azimutal +1j*Polar
                Qvalue=TwiceMaxLz/2.0
                print("TwiceMaxLz,OrbNo:",TwiceMaxLz,OrbNo)
                WfArray=Wfn.WFValue(APVal,OrbNo)
                WfScalar=np.array([Wfn.WFValue(APVal[indx],OrbNo) for indx in range(NumAns) ])
                self.assertTrue(len(APVal)==len(WfArray))
                self.assertTrue(np.sum(np.abs(WfArray-WfScalar))<self.Threshold)

    def test_sphere_density_corrs(self):
        Ne=4
        denom=3

        TwiceLzMax=denom*(Ne-1)
        OrbNo=np.array(range(Ne-1,2*Ne-1)) ###Just set to get a reasonalbe value of TwiceLz
        print("OrbNo:",OrbNo)
        TwiceLz = np.sum(2*OrbNo-TwiceLzMax)
        print("Ne,TwiceLzMax,TwiceLz:",Ne,TwiceLzMax,TwiceLz)
        Basis=FQHSphereBasis(Ne,TwiceLzMax,TwiceLz)
        print("Generate the basis")
        Basis.GenerateBasis()
        print("Basis obtained")
        print("Basis:",Basis)
        
        Nstates = Basis.GetDimension() ###Get the dimation
        print("Nstates:",Nstates)
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
        print("StartVector:\n",NPVector)
        PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
        PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors

        ### Now create the wfn objects
        Wfn=FQHSphereWF(TwiceLzMax)

        NumAns=20 ###Number of coordinates to try with 
        for indxAng in range(NumAns):
                Azimutal=np.random.random()*np.pi
                Polar=np.random.random()*2*np.pi
                print("Azimutal,Polar:",Azimutal,Polar)
                ###Test -azimutal gives same value
                Azimutal1=-Azimutal
                Polar1=Polar+np.pi
                ###Test 2*pi - polar gives same values
                Azimutal2=2*np.pi-Azimutal
                Polar2=Polar+np.pi
                ###Test polar + 2*pi gives same values
                Azimutal3=Azimutal
                Polar3=Polar+2*np.pi

                Wf0=Wfn.Rho(Azimutal +1j*Polar ,Basis,PETScVector)
                Wf1=Wfn.Rho(Azimutal1+1j*Polar1,Basis,PETScVector)
                Wf2=Wfn.Rho(Azimutal2+1j*Polar2,Basis,PETScVector)
                Wf3=Wfn.Rho(Azimutal3+1j*Polar3,Basis,PETScVector)
                print("wfval: ",Wf0)
                if TestWfnDiffernt(Wf0,Wf1,Wf2,self.Threshold) or TestWfnDiffernt(Wf0,Wf1,Wf3,self.Threshold):
                    print("\nERROR: Wfns are not invarint under shifts of angles")
                    print("TwiceMaxLz,Q,OrbNo:",TwiceLzMax,TwiceLzMax/2.0,OrbNo)
                    print('Azimutal: ',Azimutal)
                    print('Polar:',Polar)
                    print()
                    print("wfval: ",Wf0)
                    print("wfval1:",Wf1)
                    print("wfval2:",Wf2)
                    print("wfval3:",Wf3)
                    print()
                    print("wfval1/wfval",Wf1/Wf0)
                    print("wfval2/wfval:",Wf2/Wf0)
                    print("wfval3/wfval:",Wf3/Wf0)
                    print("log(wfval1/wfval)/(2*pi): ",np.log(Wf1/Wf0)/(2*np.pi))
                    print("log(wfval2/wfval)/(2*pi): ",np.log(Wf2/Wf0)/(2*np.pi))
                    print("log(wfval3/wfval)/(2*pi): ",np.log(Wf3/Wf0)/(2*np.pi))
                    self.assertTrue(False,"Wave functions are not the same")
                if TestWfnReal(Wf0,self.Threshold):
                    print("\nERROR: Density operator is not real!")
                    print("TwiceMaxLz,Q,OrbNo:",TwiceLzMax,TwiceLzMax/2.0,OrbNo)
                    print('Azimutal: ',Azimutal)
                    print('Polar:',Polar)
                    print()
                    print("TwpBpdyCorr: ",Wf0)
                    self.assertTrue(False,"Density operator is not real!")


    def test_sphere_twobody_corrs(self):
        Ne=4
        denom=3
        TwiceLzMax=denom*(Ne-1)
        OrbNo=np.array(range(Ne-1,2*Ne-1)) ###Just set to get a reasonalbe value of TwiceLz
        print("OrbNo:",OrbNo)
        TwiceLz = np.sum(2*OrbNo-TwiceLzMax)
        print("Ne,TwiceLzMax,TwiceLz:",Ne,TwiceLzMax,TwiceLz)
        Basis=FQHSphereBasis(Ne,TwiceLzMax,TwiceLz)
        print("Generate the basis")
        Basis.GenerateBasis()
        print("Basis obtained")
        print("Basis:",Basis)
        
        Nstates = Basis.GetDimension() ###Get the dimation
        print("Nstates:",Nstates)
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
        print("StartVector:\n",NPVector)
        PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
        PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors

        ### Now create the wfn objects
        Wfn=FQHSphereWF(TwiceLzMax)

        NumAns=20 ###Number of coordinates to try with 
        for indxAng in range(NumAns):
                Azimutal=np.random.random()*np.pi
                Polar=np.random.random()*2*np.pi
                Azimutal2p=np.random.random()*np.pi
                Polar2p=np.random.random()*2*np.pi
                print("Azimutal,Polar:",Azimutal,Polar)
                print("Azimutal2p,Polar2p:",Azimutal2p,Polar2p)
                ###Test -azimutal gives same value
                Azimutal1=-Azimutal
                Polar1=Polar+np.pi
                ###Test 2*pi - polar gives same values
                Azimutal2=2*np.pi-Azimutal
                Polar2=Polar+np.pi
                ###Test polar + 2*pi gives same values
                Azimutal3=Azimutal
                Polar3=Polar+2*np.pi

                Wf0=Wfn.TwoBody(Azimutal +1j*Polar ,Azimutal2p +1j*Polar2p,Basis,PETScVector)
                Wf1=Wfn.TwoBody(Azimutal1+1j*Polar1,Azimutal2p +1j*Polar2p,Basis,PETScVector)
                Wf2=Wfn.TwoBody(Azimutal2+1j*Polar2,Azimutal2p +1j*Polar2p,Basis,PETScVector)
                Wf3=Wfn.TwoBody(Azimutal3+1j*Polar3,Azimutal2p +1j*Polar2p,Basis,PETScVector)
                print("wfval: ",Wf0)
                if TestWfnDiffernt(Wf0,Wf1,Wf2,self.Threshold) or TestWfnDiffernt(Wf0,Wf1,Wf3,self.Threshold):
                    print("\nERROR: Wfns are not invarint under shifts of angles")
                    print("TwiceMaxLz,Q,OrbNo:",TwiceLzMax,TwiceLzMax/2.0,OrbNo)
                    print('Azimutal: ',Azimutal)
                    print('Polar:',Polar)
                    print()
                    print("wfval: ",Wf0)
                    print("wfval1:",Wf1)
                    print("wfval2:",Wf2)
                    print("wfval3:",Wf3)
                    print()
                    print("wfval1/wfval",Wf1/Wf0)
                    print("wfval2/wfval:",Wf2/Wf0)
                    print("wfval3/wfval:",Wf3/Wf0)
                    print("log(wfval1/wfval)/(2*pi): ",np.log(Wf1/Wf0)/(2*np.pi))
                    print("log(wfval2/wfval)/(2*pi): ",np.log(Wf2/Wf0)/(2*np.pi))
                    print("log(wfval3/wfval)/(2*pi): ",np.log(Wf3/Wf0)/(2*np.pi))
                    self.assertTrue(False,"Wave functions are not the same")

                if TestWfnReal(Wf0,self.Threshold):
                    print("\nERROR: Two body correlator is not real!")
                    print("TwiceMaxLz,Q,OrbNo:",TwiceLzMax,TwiceLzMax/2.0,OrbNo)
                    print('Azimutal: ',Azimutal)
                    print('Polar:',Polar)
                    print()
                    print("TwpBpdyCorr: ",Wf0)
                    self.assertTrue(False,"Two body correlator is not real!")
                    

    def test_Mollweide_map(self):
        Ne=4
        denom=3

        TwiceLzMax=denom*(Ne-1)
        OrbNo=np.array(range(Ne-1,2*Ne-1)) ###Just set to get a reasonalbe value of TwiceLz
        print("OrbNo:",OrbNo)
        TwiceLz = np.sum(2*OrbNo-TwiceLzMax)
        print("Ne,TwiceLzMax,TwiceLz:",Ne,TwiceLzMax,TwiceLz)
        Basis=FQHSphereBasis(Ne,TwiceLzMax,TwiceLz)
        print("Generate the basis")
        Basis.GenerateBasis()
        print("Basis obtained")
        print("Basis:",Basis)
        
        Nstates = Basis.GetDimension() ###Get the dimation
        print("Nstates:",Nstates)
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
        print("StartVector:\n",NPVector)
        PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
        PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors
        ### Now create the wfn objects
        Wfn=FQHSphereWF(TwiceLzMax)
        NumAns=20 ###Number of coordinates to try with 

        XVal=np.random.random(NumAns)*np.sqrt(2)*2
        YVal=np.random.random(NumAns)*np.sqrt(2)
        XVal2=np.random.random(NumAns)*np.sqrt(2)*2
        YVal2=np.random.random(NumAns)*np.sqrt(2)
        XYVal=XVal + 1j*YVal
        XYVal2=XVal2 + 1j*YVal2
        APVal=MollweideConversion(XYVal)
        APVal2=MollweideConversion(XYVal2)
        Rho    =Wfn.Rho(    APVal,       Basis,PETScVector)
        assert(len(Rho) == len(APVal))
        TwoBody=Wfn.TwoBody(APVal,APVal2,Basis,PETScVector)
        assert(len(TwoBody) == len(APVal))
        TwoBody=Wfn.TwoBody(APVal,APVal2[0],Basis,PETScVector)
        assert(len(TwoBody) == len(APVal))
        TwoBody=Wfn.TwoBody(APVal[0],APVal2,Basis,PETScVector)
        assert(len(TwoBody) == len(APVal2))

        
    def test_torus_density_corrs(self):
        Ne=4
        denom=3
        NumFlux=denom*Ne
        K1 = 0
        print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
        Basis=FQHTorusBasis(Ne,NumFlux,K1)
        print("Generate the basis")
        Basis.GenerateBasis()
        print("Basis obtained")
        print("Basis:",Basis)
        
        Nstates = Basis.GetDimension() ###Get the dimation
        print("Nstates:",Nstates)
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
        print("StartVector:\n",NPVector)
        PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
        PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors

        ### Now create the wfn objects
        Wfn=FQHTorusWF(NumFlux)

        NumAns=20 ###Number of coordinates to try with 
        for indxAng in range(NumAns):
            xval=np.random.random()
            yval=np.random.random()
            print("xval,yval:",xval,yval)
            xyval=xval+1j*yval
            ###Test x-pbc
            xyval1=xyval+1.0
            ###Test x-pbc
            xyval2=xyval+1j
            Wf0=Wfn.Rho(xyval, Basis,PETScVector)
            Wf1=Wfn.Rho(xyval1,Basis,PETScVector)
            Wf2=Wfn.Rho(xyval2,Basis,PETScVector)
            print("wfval: ",Wf0)
            if TestWfnDiffernt(Wf0,Wf1,Wf2,self.Threshold):
                print("\nERROR: Densities are not invarint under shifts of coordinates")
                print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
                print("xval,yval:",xval,yval)
                print()
                print("wfval: ",Wf0)
                print("wfval1:",Wf1)
                print("wfval2:",Wf2)
                print()
                print("wfval1/wfval",Wf1/Wf0)
                print("wfval2/wfval:",Wf2/Wf0)
                print("log(wfval1/wfval)/(2*pi): ",np.log(Wf1/Wf0)/(2*np.pi))
                print("log(wfval2/wfval)/(2*pi): ",np.log(Wf2/Wf0)/(2*np.pi))
                self.assertTrue(False,"Densities are not the same")
            if TestWfnReal(Wf0,self.Threshold):
                print("\nERROR: Density operator is not real!")
                print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
                print("xval,yval:",xval,yval)
                print()
                print("Density: ",Wf0)
                self.assertTrue(False,"Density operator is not real!")

    def test_torus_density_corrs(self):
        Ne=4
        denom=3
        NumFlux=denom*Ne
        K1 = 0
        print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
        Basis=FQHTorusBasis(Ne,NumFlux,K1)
        print("Generate the basis")
        Basis.GenerateBasis()
        print("Basis obtained")
        print("Basis:",Basis)
        
        Nstates = Basis.GetDimension() ###Get the dimation
        print("Nstates:",Nstates)
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
        print("StartVector:\n",NPVector)
        PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
        PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors

        ### Now create the wfn objects
        Wfn=FQHTorusWF(NumFlux)

        NumAns=20 ###Number of coordinates to try with 
        for indxAng in range(NumAns):
            xval=np.random.random()
            yval=np.random.random()
            xval2p=np.random.random()
            yval2p=np.random.random()
            print("xval,yval:",xval,yval)
            xyval=xval+1j*yval
            xyval2p=xval2p+1j*yval2p
            ###Test x-pbc
            xyval1=xyval+1.0
            ###Test x-pbc
            xyval2=xyval+1j
            Wf0=Wfn.TwoBody(xyval, xyval2p,Basis,PETScVector)
            Wf1=Wfn.TwoBody(xyval1,xyval2p,Basis,PETScVector)
            Wf2=Wfn.TwoBody(xyval2,xyval2p,Basis,PETScVector)
            print("wfval: ",Wf0)
            if TestWfnDiffernt(Wf0,Wf1,Wf2,self.Threshold):
                print("\nERROR: TwoBody are not invarint under shifts of coordinates")
                print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
                print("xval,yval:",xval,yval)
                print()
                print("wfval: ",Wf0)
                print("wfval1:",Wf1)
                print("wfval2:",Wf2)
                print()
                print("wfval1/wfval",Wf1/Wf0)
                print("wfval2/wfval:",Wf2/Wf0)
                print("log(wfval1/wfval)/(2*pi): ",np.log(Wf1/Wf0)/(2*np.pi))
                print("log(wfval2/wfval)/(2*pi): ",np.log(Wf2/Wf0)/(2*np.pi))
                self.assertTrue(False,"TwoBody are not the same")
            if TestWfnReal(Wf0,self.Threshold):
                print("\nERROR: TwoBody operator is not real!")
                print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
                print("xval,yval:",xval,yval)
                print()
                print("Density: ",Wf0)
                self.assertTrue(False,"TwoBody operator is not real!")

    def test_torus_array_argument(self):
        """
        Test that the density and twop correlator can be applied using vector input
        """
        Ne=4
        denom=3
        NumFlux=denom*Ne
        K1 = 0
        print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
        Basis=FQHTorusBasis(Ne,NumFlux,K1)
        print("Generate the basis")
        Basis.GenerateBasis()
        print("Basis obtained")
        print("Basis:",Basis)
        
        Nstates = Basis.GetDimension() ###Get the dimation
        print("Nstates:",Nstates)
        NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
        NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
        print("StartVector:\n",NPVector)
        PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
        PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors

        ### Now create the wfn objects
        Wfn=FQHTorusWF(NumFlux)

        NumAns=20 ###Number of coordinates to try with 

        XVal=np.random.random(NumAns)
        YVal=np.random.random(NumAns)
        XVal2=np.random.random(NumAns)
        YVal2=np.random.random(NumAns)
        XYVal=XVal + 1j*YVal
        XYVal2=XVal2 + 1j*YVal2
        Rho    =Wfn.Rho(    XYVal,       Basis,PETScVector)
        assert(len(Rho) == len(XYVal))
        TwoBody=Wfn.TwoBody(XYVal,XYVal2,Basis,PETScVector)
        assert(len(Rho) == len(XYVal))
        TwoBody=Wfn.TwoBody(XYVal,XYVal2[0],Basis,PETScVector)
        assert(len(Rho) == len(XYVal))
        TwoBody=Wfn.TwoBody(XYVal[0],XYVal2,Basis,PETScVector)
        assert(len(Rho) == len(XYVal2))


    def test_torus_correct_density(self):
        MaxFlux=15
        MinFlux=5
        NumAns=20 ###Number of coordinates to try with

        for tryno in range(NumAns):
            print()
            NumFlux=np.random.randint(MinFlux,MaxFlux)
            Ne=np.random.randint(1,NumFlux)
            K1=np.random.randint(0,NumFlux)
            print("Ne,NumFlux,K1:",Ne,NumFlux,K1)
            Basis=FQHTorusBasis(Ne,NumFlux,K1)
            print("Generate the basis")
            Basis.GenerateBasis()
            print("Basis obtained")
            Area=2*np.pi*NumFlux
            DensityPoints=NumFlux*2
            
            Nstates = Basis.GetDimension() ###Get the dimation
            print("Nstates:",Nstates)
            NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
            NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
            PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
            PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors

            ### Now create the wfn objects
            Wfn=FQHTorusWF(NumFlux)

            ####Make a mesh of values and sample the whole fundamental domain
            CoordY=np.linspace(0,1.0,DensityPoints,endpoint=False)
            CoordX=np.linspace(0,1.0,DensityPoints,endpoint=False)
            #print("CoordY",CoordY)
            DensityEvolution=np.zeros((DensityPoints,DensityPoints))

            for cordindx in range(DensityPoints):
                DensityEvolution[:,cordindx]=Wfn.Rho(1j*CoordY+CoordX[cordindx],Basis,PETScVector)

            ###Integrate the Density so see that the numbe rof electrons is the expceted one
            IntegratedElec=np.sum(DensityEvolution)
            print("IntegratedElec",IntegratedElec)
            RescaledNe=IntegratedElec*Area/(1.0*DensityPoints**2)
            print("RescaledNe",RescaledNe)
            self.assertTrue(np.abs(RescaledNe-Ne)<self.Threshold)


    def test_sphere_correct_density(self):
        Threshold=1e-2
        MaxFlux=15
        MinFlux=5
        NumAns=20 ###Number of coordinates to try with
        for tryno in range(NumAns):
            print()
            NumFlux=np.random.randint(MinFlux,MaxFlux)
            TwizeMaxLz=NumFlux-1
            Ne=np.random.randint(1,NumFlux)
            ExtremeLz=Ne*TwizeMaxLz-Ne*(Ne-1)
            print("ExtremeLz:",ExtremeLz)
            
            TwizeLz=np.random.randint(-ExtremeLz,ExtremeLz+1)
            ###Make sure TwizeLz has same parity as ExtremeLz
            print("First TwizeLz:",TwizeLz)
            TwizeLz=TwizeLz-np.abs(TwizeLz%2-ExtremeLz%2)
            print("Ne,NumFlux,TwizeMaxLz,ExtremeLz,TwizeLz:",Ne,NumFlux,TwizeMaxLz,ExtremeLz,TwizeLz)
            Basis=FQHSphereBasis(Ne,TwizeMaxLz,TwizeLz)
            print("Generate the basis")
            Basis.GenerateBasis()
            print("Basis obtained")
            Radius=1.0
            Area=4*np.pi*Radius**2#*NumFlux
            DensityPoints=NumFlux*10
            
            Nstates = Basis.GetDimension() ###Get the dimation
            print("Nstates:",Nstates)
            NPVector=np.random.rand(Nstates)+1j*np.random.rand(Nstates)
            NPVector=NPVector/np.linalg.norm(NPVector) ###Initate a ranom NP-vector
            PETScVector=PETSc.Vec() ###COnstruct a PETSc vecotor
            PETScVector.createWithArray(NPVector) ###Copulate it with ranom nubmer from a NPVetors

            ### Now create the wfn objects
            Wfn=FQHSphereWF(TwizeMaxLz)

            ####Make a mesh of values and sample the whole fundamental domain
            CoordY=np.linspace(-np.sqrt(2),np.sqrt(2),DensityPoints,endpoint=False)+np.sqrt(2)/DensityPoints
            CoordX=np.linspace(-2*np.sqrt(2),2*np.sqrt(2),DensityPoints,endpoint=False)+2*np.sqrt(2)/DensityPoints
            #print("CoordY",CoordY)
            #print("CoordX",CoordX)
            DensityEvolution=np.zeros((DensityPoints,DensityPoints))

            for cordindx in range(DensityPoints):
                MollweideAngles=MollweideConversion(1j*CoordY+CoordX[cordindx])
                #print("MollweideAngles\n",MollweideAngles)
                DensityEvolution[:,cordindx]=Wfn.Rho(MollweideAngles,Basis,PETScVector)
            ###Mask all values outside of the circle (by setting them no Nan)
            UsedPoints=0.0
            for xindx in range(len(CoordX)):
                for yindx in range(len(CoordY)):
                    if (CoordX[xindx]**2/8.0 + (CoordY[yindx]**2/2.0) > 1.0):
                        DensityEvolution[yindx,xindx]=np.nan
                    else:
                        UsedPoints+=1.0
                
            ###Integrate the Density so see that the numbe rof electrons is the expceted one
            IntegratedElec=np.nansum(DensityEvolution)
            print("IntegratedElec",IntegratedElec)
            RescaledNe=IntegratedElec*Area/UsedPoints
            print("RescaledNe",RescaledNe)
            if (np.abs(RescaledNe-Ne)>Threshold):
                ERRORTXT="The Density of the electrons is not correct"
                ERRORTXT+="\nIntegratedElec: "+str(IntegratedElec)
                ERRORTXT+="\nRescaledNe: "+str(RescaledNe)
                ERRORTXT+="\nDesired Ne: "+str(Ne)
                ERRORTXT+="\nDiff Ne: "+str(RescaledNe-Ne)
                ERRORTXT+="\nThreshold Ne: "+str(Threshold)
                print(ERRORTXT)
                self.assertTrue(False,ERRORTXT)
            
def TestWfnReal(wfval,Threshold):
    IsZero = (np.abs(wfval) == 0.0)
    return ((np.abs(np.imag(wfval)) > Threshold)
        or  IsZero)



def TestWfnDiffernt(wfval,wfval1,wfval2,Threshold):
    IsZero = (np.abs(wfval) == 0.0 or
              np.abs(wfval1) == 0.0 or
              np.abs(wfval2) == 0.0)
    return ((np.abs(wfval/wfval1-1) > Threshold)
        or  (np.abs(wfval/wfval2-1) > Threshold )
        or  IsZero)

def PrintZeroSmall(z,eps=10**-14):
    if np.abs(np.real(z))<eps:
        zout=0
    else:
        zout=np.real(z)
    if np.abs(np.imag(z))>eps:
        zout=zout+1j*np.imag(z)
    return str(zout)
        

if __name__ == '__main__':
    unittest.main()


