#! /usr/bin/env python

"""
 Unit tests to test calculation of the entanglement. Makes sure the trace of the reduced density matrix for some small cases is one. 

"""
import unittest
from hammer.TorusDiagonalise import TorusDiagonalise
from hammer.TorusDiagonalise import TorusLaughlinGroundState
from hammer.HammerArgs import HammerArgsParser
from hammer.FQHTorusBasis import FQHTorusBasis

import numpy as np

class TestEntanglement(unittest.TestCase):

    def setUp(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.ReturnEigenVectors = True
        self.Opts.WriteBasisFlag = False
        self.Opts.ReturnBasis = True
        self.Opts.NbrStates=1
 

    def test_on_5_particle_fermionic_laughlin(self):
        self.Opts.Particles = 5
        self.Opts.NbrFlux = 15
        self.Opts.Momentum = 0
        self.Opts.PseudoPotentials = [0.0, 1.0]
        
        Data = TorusDiagonalise(self.Opts);
        
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']

        Matrices = Basis.OrbitalEntanglementMatrices(GS, 0, 8, -1)
        SingularValues = Basis.EntanglementSingularValues(Matrices)
        Norm = Basis.EntanglementNorm(SingularValues)

        self.assertTrue(np.abs(1.0 - Norm) < 1e-7) 


    def test_on_5_particle_fermionic_laughlin_with_com_momentum(self):
        self.Opts.Particles = 5
        self.Opts.NbrFlux = 15
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.PseudoPotentials = [0.0, 1.0]
        
        Data = TorusDiagonalise(self.Opts);
        
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']

        [FullBasis, FullState] = Basis.ConvertToFullBasis(GS) 
        Matrices = FullBasis.OrbitalEntanglementMatrices(FullState, 0, 8, -1)
        SingularValues = Basis.EntanglementSingularValues(Matrices)
        Norm = FullBasis.EntanglementNorm(SingularValues)

        self.assertTrue(np.abs(1.0 - Norm) < 1e-7) 


    def test_on_5_particle_bosonic_laughlin_with_com_momentum(self):
        self.Opts.Particles = 5
        self.Opts.NbrFlux = 10 
        self.Opts.Momentum = 0
        self.Opts.Bosonic = True
        self.Opts.PseudoPotentials = [1.0, 0.0]
        
        Data = TorusDiagonalise(self.Opts);
        
        GS = Data['EigenVectors'][0]
        Basis = Data['Basis']

        Matrices = Basis.OrbitalEntanglementMatrices(GS, 0, 5, -1)
        SingularValues = FQHTorusBasis.EntanglementSingularValues(Matrices)
        Norm = FQHTorusBasis.EntanglementNorm(SingularValues)

        self.assertTrue(np.abs(1.0 - Norm) < 1e-7) 

    def test_on_4_particle_Pfaffian_with_zero_matrices_v1(self):
        self.do_test_on_4_particle_Pfaffian_with_zero_matrices(0.1*1j,get_unity_vectors)
        
    def test_on_4_particle_Pfaffian_with_zero_matrices_v2(self):
        self.do_test_on_4_particle_Pfaffian_with_zero_matrices(0.1585*1j,get_tt_mes_vector)
        
    def do_test_on_4_particle_Pfaffian_with_zero_matrices(self,tau,vector_coise):
        import hammer
        ###This problem arose when the Entanglement matrices worked out
        ### by FQHTorusBasis gave zero matrices, wqhich caued Lanzos to crash!

        N=4 ###Four particles
        dim = 6  ####The dimetion of the Pfaffian state
        [basis0, gs0] = hammer.TorusPfaffianGroundState(N, Tau=tau, StateNo=0)
        [full_basis0, full_gs0] = basis0.ConvertToFullBasis(gs0)
        [basis1, gs1] = hammer.TorusPfaffianGroundState(N, Tau=tau, StateNo=1)
        [full_basis1, full_gs1] = basis1.ConvertToFullBasis(gs1)
        [basis2, gs2] = hammer.TorusPfaffianGroundState(N, Tau=tau, StateNo=2)
        [full_basis2, full_gs2] = basis2.ConvertToFullBasis(gs2)

        bases = list()
        states = list()
        bases.append(full_basis0)
        bases.append(full_basis1)
        bases.append(full_basis2)
        states.append(full_gs0)
        states.append(full_gs1)
        states.append(full_gs2)
        ###Translate the differnt states by one step
        [new_basis3, new_state3] = full_basis0.TranslateState(full_gs0,1)
        [new_basis4, new_state4] = full_basis1.TranslateState(full_gs1,1)
        [new_basis5, new_state5] = full_basis2.TranslateState(full_gs2,1)
        bases.append(new_basis3)
        bases.append(new_basis4)
        bases.append(new_basis5)
        states.append(new_state3)
        states.append(new_state4)
        states.append(new_state5)

        for n in range(dim):
            print("State no:",n)
            Vector=vector_coise(n)
            print("Vector:",Vector)
            hammer.FQHTorusBasis.OrbitalEntanglementEntropySuperposition(
                bases,states,Vector)


    def test_the_svd_solver(self):
        from slepc4py import SLEPc
        from petsc4py import PETSc
        ###Sometimes the svd solver seems to crash for no god reason?
        size=(6,6)
        # create the matrix
        Matrix = PETSc.Mat();
        Matrix.createDense(size)
        Matrix.setFromOptions()
        Matrix.setUp()
        Matrix.zeroEntries()
        Matrix.setValue(0,0,0.00648130+0.32060635j, True)
        Matrix.setValue(5,5,-0.00668978-0.31948359j, True)
        Matrix.assemble()
        matrix_is_nonzero(Matrix)
        S = SLEPc.SVD(); S.create()
        S.setOperator(Matrix)
        S.setType(SLEPc.SVD.Type.LANCZOS)
        S.setDimensions(min(size))
        S.solve()


def matrix_is_nonzero(Matrix):
    (nx,ny)=Matrix.getSize()
    NonZero=False
    Mat=np.zeros((nx,ny),dtype=np.complex128)
    for indx1 in range(nx):
        for indx2 in range(ny):
            ###Test if the matrix is nonzero
            #print(indx1,indx2,Matrix[indx1,indx2])
            if Matrix[indx1,indx2]!=0j:
                Mat[indx1,indx2]=Matrix[indx1,indx2]
                #print("nonzero")
                NonZero=True
    if NonZero:
        print("Non-zero matrix")
        print Mat
    else:
        print("Zero matrix")
    return NonZero


def get_tt_mes_vector(StateNo):
    sqrt2=1/np.sqrt(2.0)
    VectorTuple={0:(0,sqrt2, sqrt2,0,0,0),
                 1:(0,sqrt2,-sqrt2,0,0,0),
                 2:(1,0,0,0,0,0),
                 3:(0,0,0,0,sqrt2, sqrt2),
                 4:(0,0,0,0,sqrt2,-sqrt2),
                 5:(0,0,0,1,0,0)}.get(StateNo)
    print("VectorTuple:",VectorTuple)
    return np.array(VectorTuple,dtype=np.complex128)

def get_unity_vectors(StateNo):
    VectorTuple={0:(1,0,0,0,0,0),
                 1:(0,1,0,0,0,0),
                 2:(0,0,1,0,0,0),
                 3:(0,0,0,1,0,0),
                 4:(0,0,0,0,1,0),
                 5:(0,0,0,0,0,1)}.get(StateNo)
    print("VectorTuple:",VectorTuple)
    return np.array(VectorTuple,dtype=np.complex128)
        
if __name__ == '__main__':
    unittest.main()
