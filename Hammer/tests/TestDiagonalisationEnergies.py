#! /usr/bin/env python

"""
 Unit tests to test basic diagonalisation functionality. Compared energy eigenvalues against the expected values
 for small systems.

"""
import unittest
from hammer.TorusDiagonalise import TorusDiagonalise
from hammer.HammerArgs import HammerArgsParser

import numpy as np

class TestDiagonalisationEnergies(unittest.TestCase):

    def setUp(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.NbrStates=1


    def test_three_particle_fermionic_laughlin(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.PseudoPotentials = [0.0, 1.0]

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_four_particle_fermionic_laughlin(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 2
        self.Opts.PseudoPotentials = [0.0, 1.0]

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_three_particle_bosonic_laughlin(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 6
        self.Opts.Momentum = 0
        self.Opts.PseudoPotentials = [1.0, 1.0]
        self.Opts.Bosonic = True

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_four_particle_bosonic_laughlin(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 8
        self.Opts.Momentum = 0
        self.Opts.PseudoPotentials = [1.0, 1.0]
        self.Opts.Bosonic = True

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_four_particle_fermionic_moore_read(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 8
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 2
        self.Opts.Interaction = '3Body'
        self.Opts.NbrEigvals = 3
        self.Opts.NbrStates = 3

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-7)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-7)


        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 0
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-7)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-7)

        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 2
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-7)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-7)


    def test_four_particle_bosonic_moore_read(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 4
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.Bosonic = True
        self.Opts.Interaction = '3Body'
        self.Opts.NbrEigvals = 3
        self.Opts.NbrStates = 3

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)

        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 2
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)

        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 0
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


if __name__ == '__main__':
    unittest.main()
