#! /usr/bin/env python
from __future__ import print_function

"""
 Unit tests to test TimeEvolution functionality. Compare computed Fock coefficeints agains exactly evolved coefficients for small systems

"""
import unittest
import random
import petsc4py.PETSc as PETSc
from hammer.FQHSphereWF import FQHSphereSlaterDeterminant as SphereSlater
import hammer.FQHSphereWF as FQHSphereWF

import numpy as np

class TestSphereOrbitals(unittest.TestCase):

    def setUp(self):
        self.Threshold=1e-10

    def test_diffent_lengths(self):
        NumLz=4
        MaxLz=3
        MaxLL=2
        for indxLz in range(NumLz):
            TwiceMaxLz=np.random.randint(0,MaxLz)
            MaxNe=(MaxLL+1)*(TwiceMaxLz+MaxLL)
            LenOrb=np.random.randint(1,MaxNe+1)
            LenLL=np.random.randint(1,MaxNe+1)
            OrbList=np.random.choice(MaxNe,LenOrb, replace=False)
            LLList=np.random.choice(MaxNe,LenLL, replace=False)
            if len(LLList) != len(OrbList):
                try:
                    SphereSlater(TwiceMaxLz,OrbList,LLList)
                    ERRMESSAGE="Created class object with incompatible lists"
                    self.assertTrue(False,ERRMESSAGE)
                except ValueError:
                    self.assertTrue(True)
            
    def test_generate_sphere_determinant_LLL(self):
        self.do_test_generate_sphere_determinant(4,5,10,0)
    def test_generate_sphere_determinant_any_LL(self):
        self.do_test_generate_sphere_determinant(4,5,5,1)
        
    def do_test_generate_sphere_determinant(self,NumLz,NumAns,MaxLz,MaxLL):
        #### Randmoly select a few differnt sphere sizes, orbital numbers and angular variables
        for indxLz in range(NumLz):
            ### Generate a twize max Lz
            TwiceMaxLz=np.random.randint(1,MaxLz+1)
            MaxNe=(MaxLL+1)*(1+TwiceMaxLz+MaxLL)
            Qvalue=TwiceMaxLz/2.0
            print("TwiceMaxLz,MaxNe:",TwiceMaxLz,MaxNe)
            ### Generate all the Orbno Combinations
            OrbList=np.zeros(MaxNe,dtype=int)
            LLList=np.zeros(MaxNe,dtype=int)
            for LL in range(MaxLL+1):
                PrevLL=LL*(1+TwiceMaxLz+LL-1)
                EleminLL=TwiceMaxLz+1+2*LL
                OrbList[PrevLL:(PrevLL+EleminLL)]=np.arange(EleminLL)
                LLList[PrevLL:(PrevLL+EleminLL)]=LL
            print("OrbList:",OrbList)
            print("LLList:",LLList)
            ###Define the Wave function object
            for indxAng in range(NumAns):
                ###Choose a subset of the lectrons
                ChooseNe=np.random.randint(2,MaxNe+1)
                #print("ChooseNe:",ChooseNe)
                NeIndxList=np.random.choice(MaxNe,ChooseNe, replace=False)
                #print("NeIndx:",NeIndxList)
                UseOrbList=OrbList[NeIndxList]
                UseLLList=LLList[NeIndxList]
                #print("UseOrbList:",UseOrbList)
                #print("UseLLList:",UseLLList)
                Azimutal=np.random.random(ChooseNe)*np.pi
                Polar=np.random.random(ChooseNe)*2*np.pi
                #print("Azimutal:",Azimutal)
                #print("Polar:",Polar)
                ###Test that changing the order gives minus sign

                Slater=SphereSlater(TwiceMaxLz,UseOrbList,UseLLList)
                Wf0=Slater.EvaluateSlaterDeterminant(Azimutal +1j*Polar)

                for Neindx in range(ChooseNe-1):
                    UseOrbList1=np.array(UseOrbList)
                    UseLLList1=np.array(UseLLList)
                    #print("UseOrbList1:",UseOrbList1)
                    #print("UseLLList1:",UseLLList1)
                    ###Change the order of two elements
                    UseOrbList1[Neindx]=UseOrbList[Neindx+1]
                    UseOrbList1[Neindx+1]=UseOrbList[Neindx]
                    UseLLList1[Neindx]=UseLLList[Neindx+1]
                    UseLLList1[Neindx+1]=UseLLList[Neindx]
                    #print("UseOrbList1:",UseOrbList1)
                    #print("UseLLList1:",UseLLList1)

                    Slater1=SphereSlater(TwiceMaxLz,UseOrbList1,UseLLList1)
                    Wf1=-Slater1.EvaluateSlaterDeterminant(Azimutal +1j*Polar)
                    if TestWfnDiffernt(Wf0,Wf1,self.Threshold) :
                        ERRMESSAGE=""
                        ERRMESSAGE+="\nERROR: Wfns are not invarint permutaions of rows"
                        ERRMESSAGE+="\nTwiceMaxLz,Q: "+str(TwiceMaxLz)+","+str(TwiceMaxLz/2.0)
                        ERRMESSAGE+="\nUseOrbList:\n"+str(UseOrbList)
                        ERRMESSAGE+="\nUseLLList:\n"+str(UseLLList)
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nAzimutal:\n"+str(Azimutal)
                        ERRMESSAGE+="\nPolar:\n"+str(Polar)
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nwfval: "+PrintZeroSmall(Wf0)
                        ERRMESSAGE+="\nwfval1:"+PrintZeroSmall(Wf1)
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nwfval1/wfval: "+PrintZeroSmall(Wf1/Wf0)
                        ERRMESSAGE+="\nlog(wfval1/wfval)/(2*pi): "+PrintZeroSmall(np.log(Wf1/Wf0)/(2*np.pi))
                        ERRMESSAGE+="\n"
                        ERRMESSAGE+="\nWave functions are not the same"
                        print(ERRMESSAGE)
                        self.assertTrue(False,ERRMESSAGE)



    def test_generate_sphere_determinant_array_input(self):
        NumLz=2
        NumAns=2
        TwiceMaxLz=30
        MaxLL=0
        ChooseNe=10
        Configs=10000
        
        #### Randomly select a few differnt sphere sizes, orbital numbers and angular variables
        for indxLz in range(NumLz):
            ### Generate a twize max Lz
            MaxNe=(MaxLL+1)*(1+TwiceMaxLz+MaxLL)
            Qvalue=TwiceMaxLz/2.0
            print("TwiceMaxLz,MaxNe:",TwiceMaxLz,MaxNe)
            ### Generate all the Orbno Combinations
            OrbList=np.zeros(MaxNe,dtype=int)
            LLList=np.zeros(MaxNe,dtype=int)
            for LL in range(MaxLL+1):
                PrevLL=LL*(1+TwiceMaxLz+LL-1)
                EleminLL=TwiceMaxLz+1+2*LL
                OrbList[PrevLL:(PrevLL+EleminLL)]=np.arange(EleminLL)
                LLList[PrevLL:(PrevLL+EleminLL)]=LL
            print("OrbList:",OrbList)
            print("LLList:",LLList)
            ###Define the Wave function object
            for indxAng in range(NumAns):
                ###Choose a subset of the lectrons
                #print("ChooseNe:",ChooseNe)
                NeIndxList=np.random.choice(MaxNe,ChooseNe, replace=False)
                #print("NeIndx:",NeIndxList)
                UseOrbList=OrbList[NeIndxList]
                UseLLList=LLList[NeIndxList]
                #print("UseOrbList:",UseOrbList)
                #print("UseLLList:",UseLLList)
                Azimutal=np.random.rand(Configs,ChooseNe)*np.pi
                Polar=np.random.rand(Configs,ChooseNe)*2*np.pi
                #print("Azimutal:",Azimutal)
                #print("Polar:",Polar)
                ###Test that changing the order gives minus sign

                Slater=SphereSlater(TwiceMaxLz,UseOrbList,UseLLList)
                Wf0=Slater.EvaluateSlaterDeterminant(Azimutal +1j*Polar,Verbose=True)
                

    def test_compare_array_sphere_determinant(self):
        NumLz=2
        NumAns=2
        MaxLz=10
        MaxLL=1
        Configs=10
        #### Randomly select a few differnt sphere sizes, orbital numbers and angular variables
        for indxLz in range(NumLz):
            ### Generate a twize max Lz
            TwiceMaxLz=MaxLz
            MaxNe=(MaxLL+1)*(1+TwiceMaxLz+MaxLL)
            Qvalue=TwiceMaxLz/2.0
            print("TwiceMaxLz,MaxNe:",TwiceMaxLz,MaxNe)
            ### Generate all the Orbno Combinations
            OrbList=np.zeros(MaxNe,dtype=int)
            LLList=np.zeros(MaxNe,dtype=int)
            for LL in range(MaxLL+1):
                PrevLL=LL*(1+TwiceMaxLz+LL-1)
                EleminLL=TwiceMaxLz+1+2*LL
                OrbList[PrevLL:(PrevLL+EleminLL)]=np.arange(EleminLL)
                LLList[PrevLL:(PrevLL+EleminLL)]=LL
            print("OrbList:",OrbList)
            print("LLList:",LLList)
            ###Define the Wave function object
            for indxAng in range(NumAns):
                ###Choose a subset of the lectrons
                ChooseNe=MaxNe/2
                #print("ChooseNe:",ChooseNe)
                NeIndxList=np.random.choice(MaxNe,ChooseNe, replace=False)
                #print("NeIndx:",NeIndxList)
                UseOrbList=OrbList[NeIndxList]
                UseLLList=LLList[NeIndxList]
                #print("UseOrbList:",UseOrbList)
                #print("UseLLList:",UseLLList)
                Azimutal=np.random.rand(Configs,ChooseNe)*np.pi
                Polar=np.random.rand(Configs,ChooseNe)*2*np.pi
                #print("Azimutal:",Azimutal)
                #print("Polar:",Polar)
                ###Test that changing the order gives minus sign

                Slater=SphereSlater(TwiceMaxLz,UseOrbList,UseLLList)
                wfval1=Slater.EvaluateSlaterDeterminant(Azimutal +1j*Polar,Verbose=True)
                wfval2=SimpleDeterminant(Azimutal +1j*Polar,TwiceMaxLz,UseOrbList,UseLLList)

                quotientval=np.abs(wfval1-wfval2)/np.abs(wfval1)
                ToFDiF=quotientval > 1e-12

                if np.any(ToFDiF):
                    np.set_printoptions(linewidth=200)
                    ERRTXT="ERROR: Generated laughlin does not agree with recomputed laughlin using lattice"
                    ERRTXT+="\nAzimutal:\n"+str(Azimutal)
                    ERRTXT+="\nPolar:\n"+str(Polar)
                    ERRTXT+="\nquotientval:\n"+str(quotientval)
                    BothVectors=np.real((np.vstack((wfval1,wfval2,wfval2-wfval1))).T)
                    ERRTXT+="\nwfval,serial,serial lattice, diff, real:\n"+str(BothVectors)
                    BothVectors=np.imag((np.vstack((wfval1,wfval2,wfval2-wfval1))).T)
                    ERRTXT+="\nwfval,serial,serial lattice, diff,  imag:\n"+str(BothVectors)
                    ERRTXT+="\nToFDiF2: "+str(ToFDiF)
                    ERRTXT+="\nERROR: Generated slater determinant does not agree with naive calualation"
                    ERRTXT+="\nNbrNe, TwiceMaxLz: "+str(ChooseNe)+", "+str(TwiceMaxLz)
                    self.assertTrue(False,ERRTXT)
                        

                

def SimpleDeterminant(Positions,TwiceMaxLz,UseOrbList,UseLLList):
    import sympy as syp
    Particles=len(UseOrbList)
    DetNorm = 1.0/np.sqrt(np.float(syp.factorial(Particles)))

    NbrPositions = Positions.shape[0]
    OutputValues = np.zeros(NbrPositions, dtype=np.complex128)
    ### Setup the amtrix used for the determinant
    MatDet = np.zeros((Particles,Particles),dtype=np.complex128)

    # for each set of configurations calculate the Slater determinant
    for l in range(NbrPositions):
        for j in range(Particles):
            OrbNo=UseOrbList[j]
            LLNo=UseLLList[j]
            Wfn=FQHSphereWF(TwiceMaxLz,LLNo)
            for k in range(Particles):
                WfWal=Wfn.WFValueScalar(Positions[l,k],OrbNo)
                MatDet[j,k]=WfWal
        Det = np.linalg.det(MatDet) * DetNorm
        OutputValues[l]=Det
    return OutputValues
                
                
            
def TestWfnReal(wfval,Threshold):
    IsZero = (np.abs(wfval) == 0.0)
    return ((np.abs(np.imag(wfval)) > Threshold)
        or  IsZero)

def TestWfnDifferntList(wfvalList,Threshold):
    Diff=False
    for indx in range(len(wfvalList)-1):
        Diff=Diff or TestWfnDiffernt(wfvalList[0],wfvalList[indx],Threshold)
    return

def TestWfnDiffernt(wfval1,wfval2,Threshold):
    #print("Test",wfval1,"and",wfval2)
    IsZero = (np.abs(wfval1) == 0.0 or
              np.abs(wfval2) == 0.0)
    return ((np.abs(wfval1/wfval2-1) > Threshold)
        or  IsZero)


def PrintZeroSmall(z,eps=10**-14):
    if np.abs(np.real(z))<eps:
        zout=0
    else:
        zout=np.real(z)
    if np.abs(np.imag(z))>eps:
        zout=zout+1j*np.imag(z)
    return str(zout)
        

if __name__ == '__main__':
    unittest.main()


