#! /usr/bin/env python

"""
 Unit tests to for reflection symmetry code. Tests that the eigenvalues retrieved are correct for a few small systems.
"""
from hammer.TorusDiagonalise import TorusDiagonalise
from hammer.HammerArgs import HammerArgsParser
from hammer.FQHTorusBasis import FQHTorusBasis
from hammer.FQHTorusBasisCOMMomentum import FQHTorusBasisCOMMomentum
from hammer.FQHTorusBasisCOMMomentumInversion import FQHTorusBasisCOMMomentumInversion
from hammer.FQHTorusBasisBosonicCOMMomentum import FQHTorusBasisBosonicCOMMomentum
from hammer.FQHTorusBasisBosonicCOMMomentumInversion import FQHTorusBasisBosonicCOMMomentumInversion

import unittest
import numpy as np
import petsc4py.PETSc as PETSc


class TestReflectionSymmetry(unittest.TestCase):
    """
    Test suite for reflection/inversion symmetry. Tests class FQHTorusBasisCOMMomentumInversion.
    """

    def setUp(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Threshold = 1e-10


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_three_particle_fermionic_laughlin1(self):
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.PseudoPotentials = [0.0, 1.0]

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_four_particle_fermionic_laughlin1(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.PseudoPotentials = [0.0, 1.0]

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_four_particle_fermionic_laughlin2(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 2
        self.Opts.PseudoPotentials = [0.0, 1.0]

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_four_particle_fermionic_laughlin3(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 0
        self.Opts.PseudoPotentials = [0.0, 1.0]

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_moore_read1(self):
        self.Opts.Particles = 6
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.Interaction = "3Body"
        Threshold = 1e-6

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.max(np.abs(NewEnergies - Energies)) < Threshold, msg="Maximum difference is :" + str(np.max(np.abs(NewEnergies - Energies))) + ", " +  str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_moore_read2(self):
        self.Opts.Particles = 6
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 3
        self.Opts.COMMomentum = 3
        self.Opts.Interaction = "3Body"
        Threshold = 1e-6

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.max(np.abs(NewEnergies - Energies)) < Threshold, msg="Maximum difference is :" + str(np.max(np.abs(NewEnergies - Energies))) + ", " +  str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_moore_read3(self):
        self.Opts.Particles = 6
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 3
        self.Opts.COMMomentum = 0
        self.Opts.Interaction = "3Body"
        Threshold = 1e-6

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.max(np.abs(NewEnergies - Energies)) < Threshold, msg="Maximum difference is :" + str(np.max(np.abs(NewEnergies - Energies))) + ", " +  str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_moore_read4(self):
        self.Opts.Particles = 8
        self.Opts.NbrFlux = 16
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.Interaction = "3Body"
        Threshold = 1e-6

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in list(NewData.keys()):
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.max(np.abs(NewEnergies - Energies)) < Threshold, msg="Maximum difference is :" + str(np.max(np.abs(NewEnergies - Energies))) + ", " +  str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_two_fifthsN4_1(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 10
        self.Opts.Momentum = 0
        self.Opts.Interaction = "Coulomb"

        FullBasis = FQHTorusBasis(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for COMMomentum in range(int(self.Opts.Particles/2)):
            for InvSector in range(2):
                self.Opts.COMMomentum = COMMomentum
                self.Opts.InversionSector = InvSector
                Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, COMMomentum, InvSector)
                Basis.GenerateBasis()
                if Basis.GetDimension() > 0:
                    self.Opts.NbrEigvals = Basis.GetDimension()
                    self.Opts.NbrStates = Basis.GetDimension()
                    NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                    if NewData is not None and 'EigenValues' in list(NewData.keys()):
                        NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        PetscPrint = PETSc.Sys.Print

        PETSc.Sys.Print("Orig: " + str(Energies))
        PETSc.Sys.Print("With sym: " + str(NewEnergies))
        PETSc.Sys.Print("Diff: " + str(NewEnergies - Energies))
        PETSc.Sys.Print("Diff: " + str(np.sum(np.abs(NewEnergies - Energies))))

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_two_fifthsN4_2(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 10
        self.Opts.Momentum = 1
        self.Opts.Interaction = "Coulomb"

        FullBasis = FQHTorusBasis(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for COMMomentum in range(int(self.Opts.Particles/2)):
            for InvSector in range(2):
                self.Opts.COMMomentum = COMMomentum
                self.Opts.InversionSector = InvSector
                Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, COMMomentum, InvSector)
                Basis.GenerateBasis()
                if Basis.GetDimension() > 0:
                    self.Opts.NbrEigvals = Basis.GetDimension()
                    self.Opts.NbrStates = Basis.GetDimension()
                    NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                    if NewData is not None and 'EigenValues' in NewData:
                        NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_two_fifthsN4_3(self):
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 10
        self.Opts.Momentum = 2
        self.Opts.Interaction = "Coulomb"

        FullBasis = FQHTorusBasis(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for COMMomentum in range(int(self.Opts.Particles/2)):
            for InvSector in range(2):
                self.Opts.COMMomentum = COMMomentum
                self.Opts.InversionSector = InvSector
                Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, COMMomentum, InvSector)
                Basis.GenerateBasis()
                if Basis.GetDimension() > 0:
                    self.Opts.NbrEigvals = Basis.GetDimension()
                    self.Opts.NbrStates = Basis.GetDimension()
                    NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                    if NewData is not None and 'EigenValues' in NewData:
                        NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_two_fifths1(self):
        self.Opts.Particles = 6
        self.Opts.NbrFlux = 15
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.Interaction = "Coulomb"

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in NewData:
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_two_fifths2(self):
        self.Opts.Particles = 6
        self.Opts.NbrFlux = 15
        self.Opts.Momentum = 3
        self.Opts.COMMomentum = 0
        self.Opts.Interaction = "Coulomb"

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = FullBasis.GetDimension()
        self.Opts.NbrStates = FullBasis.GetDimension()

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = Basis.GetDimension()
                self.Opts.NbrStates = Basis.GetDimension()
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in NewData:
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(len(NewEnergies) == len(Energies), msg="Dims: " + str((FullBasis.GetDimension(), len(Energies), len(NewEnergies))) + str(Energies) + " != " + str(NewEnergies))
        self.assertTrue(np.sum(np.abs(NewEnergies - Energies)) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_two_fifths3(self):
        self.Opts.Particles = 8
        self.Opts.NbrFlux = 20
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.Interaction = "Coulomb"

        NumEigenValues = 30

        FullBasis = FQHTorusBasisCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = min(NumEigenValues, FullBasis.GetDimension())
        self.Opts.NbrStates = min(NumEigenValues, FullBasis.GetDimension())

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = min(NumEigenValues, FullBasis.GetDimension())
                self.Opts.NbrStates = min(NumEigenValues, FullBasis.GetDimension())
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in NewData:
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(np.sum(np.abs(NewEnergies[0:NumEigenValues] - Energies[0:NumEigenValues])) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))


    #@unittest.skip("skipping for now to get to subsquent tests faster")
    def test_bosonic_laughlin(self):
        self.Opts.Particles = 8
        self.Opts.NbrFlux = 16
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.Bosonic = True
        self.Opts.Interaction = "Coulomb"

        NumEigenValues = 30

        FullBasis = FQHTorusBasisBosonicCOMMomentum(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum)
        FullBasis.GenerateBasis()
        self.Opts.NbrEigvals = min(NumEigenValues, FullBasis.GetDimension())
        self.Opts.NbrStates = min(NumEigenValues, FullBasis.GetDimension())

        Data = TorusDiagonalise(self.Opts, BasisObj=FullBasis);

        Energies = np.asarray([np.complex(x[0], x[1]) for x in Data['EigenValues']])

        NewEnergiesList = list()

        for InvSector in range(2):
            self.Opts.InversionSector = InvSector
            Basis = FQHTorusBasisBosonicCOMMomentumInversion(self.Opts.Particles, self.Opts.NbrFlux, self.Opts.Momentum, self.Opts.COMMomentum, InvSector)
            Basis.GenerateBasis()
            if Basis.GetDimension() > 0:
                self.Opts.NbrEigvals = min(NumEigenValues, FullBasis.GetDimension())
                self.Opts.NbrStates = min(NumEigenValues, FullBasis.GetDimension())
                NewData = TorusDiagonalise(self.Opts, BasisObj=Basis)
                if NewData is not None and 'EigenValues' in NewData:
                    NewEnergiesList.extend([np.complex(x[0], x[1]) for x in NewData['EigenValues']])

        NewEnergies = np.asarray(NewEnergiesList)
        NewEnergies.sort()

        self.assertTrue(np.sum(np.abs(NewEnergies[0:NumEigenValues] - Energies[0:NumEigenValues])) < self.Threshold, msg=str(Energies) + " != " + str(NewEnergies))




if __name__ == '__main__':
    unittest.main()
