#! /usr/bin/env python

"""
 Unit tests to test that the basis objects are working as expected for small systems.

"""
import unittest
from hammer.TorusDiagonalise import TorusDiagonalise
from hammer.HammerArgs import HammerArgsParser
from hammer.vectorutils import *

import numpy as np

class TestBasisObjects(unittest.TestCase):

    def setUp(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.NbrStates=1


    def test_three_particle_fermionic_laughlin(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 9
        self.Opts.Momentum = 0
        self.Opts.PseudoPotentials = [0.0, 1.0]

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_four_particle_fermionic_laughlin(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 12
        self.Opts.Momentum = 2
        self.Opts.PseudoPotentials = [0.0, 1.0]

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_three_particle_bosonic_laughlin(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 3
        self.Opts.NbrFlux = 6
        self.Opts.Momentum = 0
        self.Opts.PseudoPotentials = [1.0, 1.0]
        self.Opts.Bosonic = True

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_four_particle_bosonic_laughlin(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 8
        self.Opts.Momentum = 0
        self.Opts.PseudoPotentials = [1.0, 1.0]
        self.Opts.Bosonic = True

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_four_particle_fermionic_moore_read(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 8
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 2
        self.Opts.Interaction = '3Body'
        self.Opts.NbrEigvals = 3
        self.Opts.NbrStates = 3

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-7)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-7)


        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 0
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-7)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-7)

        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 2
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-7)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-7)


    def test_four_particle_bosonic_moore_read(self):
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 4
        self.Opts.NbrFlux = 4
        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 0
        self.Opts.Bosonic = True
        self.Opts.Interaction = '3Body'
        self.Opts.NbrEigvals = 3
        self.Opts.NbrStates = 3

        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)

        self.Opts.Momentum = 0
        self.Opts.COMMomentum = 2
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)

        self.Opts.Momentum = 2
        self.Opts.COMMomentum = 0
        Data = TorusDiagonalise(self.Opts);

        # check ground state is zero (at least close)
        Energies = Data['EigenValues']
        self.assertTrue(np.abs(Energies[0][0]) < 1e-10)
        self.assertTrue(np.abs(Energies[0][1]) < 1e-10)


    def test_eight_particle_fermionic_com_basis(self):
        """
        This method tests the conversion from a COMMomentum basis to the full
        basis and the conversion back.
        """
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 8
        self.Opts.NbrFlux = 16
        self.Opts.Bosonic = False
        self.Opts.Interaction = 'Coulomb'
        self.Opts.NbrEigvals = 3
        self.Opts.NbrStates = 3
        self.Opts.ReturnEigenVectors = True
        self.Opts.ReturnBasis = True

        for k1 in range(self.Opts.Particles):
            for k2 in range(self.Opts.Particles):
                self.Opts.Momentum = k1
                self.Opts.COMMomentum = k2

                Data = TorusDiagonalise(self.Opts);

                # We now convert states back to full basis and then back again.
                EigenStates = Data['EigenVectors']
                Basis = Data['Basis']

                print('Basis details ' + str((k1,k2)) + ': ' + str(Basis.GetBasisDetails()))

                FullBasis = None
                FullStates = []
                for state in EigenStates:
                    FullBasis, FullState = Basis.ConvertToFullBasis(state, FullBasis)
                    FullStates.append(FullState)

                # now convert back and compare
                for idx in range(len(EigenStates)):
                    FullBasis, State = Basis.ConvertFromFullBasis(FullStates[idx], FullBasis)
                    overlap = Overlap(State, EigenStates[idx])
                    if np.abs((1.0 - overlap)) >= 1e-10:
                        print('Issue with conversion between bases!! Overlap when converted back: ' + str(overlap))
                    self.assertTrue(np.abs((1.0 - overlap)) < 1e-10)


    def test_eight_particle_fermionic_com_inv_basis(self):
        """
        This method tests the conversion from a COMMomentumInversion basis to the full
        basis and the conversion back. This only works in some sectors so will only look at ones
        in which it works.
        """
        self.Opts = HammerArgsParser()
        self.Opts.parseArgs('')
        self.Opts.ReturnEigenValues = True
        self.Opts.WriteBasisFlag = False
        self.Opts.Particles = 8
        self.Opts.NbrFlux = 16
        self.Opts.Bosonic = False
        self.Opts.Interaction = 'Coulomb'
        self.Opts.NbrEigvals = 3
        self.Opts.NbrStates = 3
        self.Opts.ReturnEigenVectors = True
        self.Opts.ReturnBasis = True


        for self.Opts.InversionSector in [0, 1]:
            for self.Opts.Momentum in [0, 4]:
                for self.Opts.COMMomentum in [0, 4]:
                    Data = TorusDiagonalise(self.Opts);

                    # We now convert states back to full basis and then back again.
                    EigenStates = Data['EigenVectors']
                    Basis = Data['Basis']
                    print('Basis details: ' + str(Basis.GetBasisDetails()))

                    FullBasis = None
                    FullStates = []
                    for state in EigenStates:
                        FullBasis, FullState = Basis.ConvertToFullBasis(state, FullBasis)
                        FullStates.append(FullState)

                    # now convert back and compare
                    for idx in range(len(EigenStates)):
                        FullBasis, State = Basis.ConvertFromFullBasis(FullStates[idx], FullBasis)
                        overlap = Overlap(State, EigenStates[idx])
                        if np.abs((1.0 - overlap)) > 1e-10:
                            print('Issue with conversion between bases!! Overlap when converted back: ' + str(overlap))
                        self.assertTrue(np.abs((1.0 - overlap)) < 1e-10)


if __name__ == '__main__':
    unittest.main()
