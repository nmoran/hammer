from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
from disttest import test
import Cython.Compiler.Options
import numpy
import sysconfig
import sys
import os
import glob

Cython.Compiler.Options.annotate = True


def hammer_extension(Name):
    return Extension("hammer."+Name,
                     ["hammer/"+Name+".pyx"],
                     include_dirs=[numpy.get_include()],
                     extra_compile_args=["-w"])
####The nicer more flexible way to to suppress warning could be 
##https://stackoverflow.com/questions/36557908/is-there-a-clean-way-to-suppress-compiler-warnings-from-cython-when-using-pyximp


def distutils_dir_name(dname):
    """Returns the name of a distutils build directory"""
    f = "{dirname}.{platform}-{version[0]}.{version[1]}"
    return f.format(dirname=dname,
                    platform=sysconfig.get_platform(),
                    version=sys.version_info)


sys.path.append(os.path.join('build', distutils_dir_name('lib')))

extensions = [hammer_extension("FQHTorusBasis"),
              hammer_extension("FQHTorusBasisBosonic"),
              hammer_extension("FQHTorusBasisCOMMomentum"),
              hammer_extension("FQHTorusBasisCOMMomentumInversion"),
              hammer_extension("FQHTorusBasisBosonicCOMMomentum"),
              hammer_extension("FQHTorusBasisBosonicCOMMomentumInversion"),
              hammer_extension("FQHCorrelations"),
              hammer_extension("FQHTimeEvolution"),
              hammer_extension("FQHSphereBasis"),
              hammer_extension("FQHLaughlin"),
              hammer_extension("FQHTorusCoefficients"),
              hammer_extension("FQHSphereCoefficients"),
              hammer_extension("FQHTorusProductWF"),
              hammer_extension("FQHTorusHamiltonian"),
              hammer_extension("FQHSphereHamiltonian"),
              hammer_extension("FQHTorusWF"),
              hammer_extension("FQHTorusJKWF"),
              hammer_extension("FQHSphereWF"),
              hammer_extension("utils_timer"),
              hammer_extension("utils_cython")]

setup(name='Hammer',
      version='1.0',
      description='Tool to diagonalise FQH torus Hamiltonians.',
      author='Niall Moran',
      author_email='niall.moran@gmail.com',
      license='GPLv3',
      scripts=
      ['hammer/TorusDiagonalise.py', 'hammer/SphereDiagonalise.py',
       'hammer/Overlap.py', 'hammer/CalcPlotCoefficients.py',
       'hammer/ConvertBinToASCII.py', 'hammer/ConvertBinToHDF.py',
       'hammer/ConvertASCIIToBin.py', 'hammer/Viscosity.py',
       'hammer/ShowBasis.py', 'hammer/TauValue.py',
       'hammer/SqueezedWeight.py','hammer/FQHTorusGenerateJKWF.py',
       'hammer/FQHThermalThinTorus.py', 'hammer/TorusEntanglement.py',
       'hammer/TorusCorrelations.py', 'hammer/ProductWF.py',
       'hammer/EvaluateStatesRealSpace.py', 'hammer/MCImportanceOverlaps.py',
       'hammer/ModularTransformState.py','hammer/FQHFileManager.py',
       'hammer/FQHFileNames.py', 'hammer/ShowBasisSphere.py',
       'hammer/MergeHDF5.py', 'hammer/SplitHDF5.py','hammer/MCSampleLaughlin.py',
       'hammer/CreateSuperposition.py', 'hammer/MCCreateSuperposition.py',
       'hammer/FQHBasisManager.py',
       'hammer/MC_combine_trans.py', 'hammer/RealSpaceTorusCorrelations.py',
       'hammer/vectorutils.py',
       'hammer/TransformState.py','hammer/FQHTorusMoveCoordinates.py'],
      cmdclass={'build_ext': build_ext,
                'test': test},
      packages=['hammer'],
      package_dir={'hammer': 'hammer'},
      ext_modules=cythonize(extensions),
      options={
          'test': {
              'test_dir': ['tests']
          }
      })
