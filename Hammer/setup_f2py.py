"""
Setup file to build and install fortran modules. 
"""

# To build f2py modules we import
from numpy.distutils.core import setup
from numpy.distutils.core import Extension
from numpy.distutils.core import build_ext


setup(
    name='HammerF2PY',
    version='1.0',
    description='Tool to diagonalise FQH torus Hamiltonians (F2PY modules).',
    author='Niall Moran',
    author_email='niall.moran@gmail.com',
    #cmdclass = {'build_ext': build_ext, 'config_fc': config_fc},
    license = 'GPLv3',
    #ext_modules = [Extension(name="jacobitheta", sources=['hammerf2py/jacobitheta.pyf', 'hammerf2py/jacobitheta.f90']
    #                         ,extra_link_args=['-nostartfiles'])
    ext_modules = [Extension(name="jacobitheta", sources=['hammerf2py/jacobitheta.pyf', 'hammerf2py/jacobitheta.f90'])
                         ]
)
