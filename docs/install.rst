Installation
==============
The diagonalisation component of Hammer has been tested extensively on ubuntu, slackware and SLES Linux systems. It should in principle work on any operating system which has the required packages installed. Here we give some quick installation instructions for those running ubuntu (either 14.04 or 14.10), followed by more detailed instructions in case something goes wrong, or you are using a different OS. There is also a `docker <http://www.docker.com>`_ image provided which should work on any platform on which docker works (Linux, Mac OS X and windows at this time I believe). This is probably the best option for those running windows. See section below for details on how to use this image.   

Docker image (Recommended)
--------------
Hammer can be installed as a Docker image as

- `https://hub.docker.com/r/nmoran/hallway/ <https://hub.docker.com/r/nmoran/hallway/>`_

This image is built automatically when changes are made to the hammer repository.
For instructions about installing docker, see  `https://docs.docker.com/ <https://docs.docker.com/>`_. Once docker is installed, you can run things directly in the docker container. For example to run the Hammer tests use the command ::


    docker run -w /hammer/Hammer nmoran/hallway  python setup.py test

The first time this is run, it will download the image, so will take a little time. It will then create a new container from the image, run the tests and then close the image. To use the image interactively one can use ::

    docker run -it nmoran/hallway bash

If it has been sometime since the image was downloaded, it is possible to update to the latest version by running ::
      
    docker pull nmoran/hallway

which will pull the latest build of the image. When working with docker images you will be able to run all the utilities, however any files that are modified or created will not persist when the container is closed (achieved by typing exit). To work with files on the disk and keep changes, it is recommended that one mount a directory as a volume on the container. For example to create a test folder, mount it at `/data`, run a small calculation in the container and exit, one would enter ::

    mkdir test
    docker run -v ${PWD}/test:/data -w /data nmoran/hallway TorusDiagonaise.py

After running, the output files should appear in the test folder.


There exists also a second image  that is manually created and contains multiple other packages and tools including Diagham (`http://nick-ux.lpa.ens.fr/diagham/wiki/ <http://nick-ux.lpa.ens.fr/diagham/wiki/>`_). This image is much bigger as a result and also can lag behind quite a bit.  
    
- `https://hub.docker.com/r/nmoran/hammer/ <https://hub.docker.com/r/nmoran/hammer/>`_



Manual installation
--------------------
To run hammer, there are multiple pre-requisites. The most troublesome are PETSc and SLEPc and their python wrappers petsc4py and slepc4py. The following steps will show how to build these from source and then show how to download and build and install hammer. These instructions are written for a ubuntu Linux system, but a similar process should work on any platform where the required packages are available. 

Prerequisites
~~~~~~~~~~~~~
The PETSc and SLEPc packages depend on MPI, LAPACK and BLAS. These required packages can be obtained with the command::
  
    sudo apt-get install libhdf5-dev libopenmpi-dev liblapack-dev libblas-dev gfortran build-essential

PETSc
~~~~~~~~~
- Download PETSc from `http://www.mcs.anl.gov/petsc/download/index.html <http://www.mcs.anl.gov/petsc/download/index.html>`_
- Extract and set environment variables PETSC_DIR and PETSC_ARCH to the petsc directory and a string to identify the current petsc build. For example::

    export PETSC_DIR=`pwd`
    export PETSC_ARCH=linux-gnu-cxx-complex

- Configure with::

    ./configure --with-clanguage=C++ --with-cxx-support=1 --with-gcov=1 --with-scalar-type=complex --with-shared-libraries=1 --with-debugging=0 --with-scientific-python=1

If support for complex numbers is not required set the scalar type to real
instead. If there are problems finding MPI, LAPACK or BLAS, make sure these are
installed and specify paths. See `./configure --help` for further help and options.
- Once the configure completes successfully. Start building with `make`

SLEPc
~~~~~~~
- Download SLEPc from `http://www.grycap.upv.es/slepc/ <http://www.grycap.upv.es/slepc/>`_
- Extract and run configure wth `./configure`. This should find your PETSc build via PETSC_DIR and PETSC_ARCH variables set during the PETSc install.
- Set SLEPC_DIR to the slepc directory ::

    export SLEPC_DIR=`pwd`


petsc4py
~~~~~~~~~~
- Download petsc4py from `https://pypi.python.org/pypi/petsc4py` or from `https://bitbucket.org/petsc/petsc4py/ <https://bitbucket.org/petsc/petsc4py/>`_.
- Make sure that The major and minor version conform to the major and minor version of PETc.
- You will need to have numpy (using pip) installed and also the PETSc environment variables set.
- Extract and build using ::

    python setup.py build

- Once this has finished install using :: 

    sudo PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH python setup.py install

If you do not have permissions to install to the system directory you can create
a local python environment with virtualenv (the `--user install` from petsc4py
appears to give problems). To do this install virtualenv ::
  
    pip install --user virtualenv

Setup new python environment: :: 

    virtualenv --system-site-packages ENV

Activate this: ::

    source ENV/bin/activate

Then build and install: ::

    python setup.py build
    python setup.py install

    
slepc4py
~~~~~~~~~
- Download slepc4py from `https://pypi.python.org/pypi/slepc4py <https://pypi.python.org/pypi/slepc4py>`_ or from `https://bitbucket.org/slepc/slepc4py/ <https://bitbucket.org/slepc/slepc4py/>`_
- Make sure that the major and minor version conform to the major and minor version of PETc.

- NB: Currently there is a bug in slepc4py-3.7.0 for complex support. If you encounter issues, apply manually the few patches in `https://bitbucket.org/slepc/slepc4py/commits/a93f720 <https://bitbucket.org/slepc/slepc4py/commits/a93f720>`_. Remove 'slepc4py.SLEPc.c' and then retry the build.

- Extract and build using ::

    python setup.py build

- Once this has finished install using :: 

    sudo PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH  SLEPC_DIR=$SLEPC_DIR python setup.py install

hammer
~~~~~~~~~
Now we download the source by cloning the git repository.::

    sudo apt-get install git
    git clone https://bitbucket.org/nmoran/hammer.git

You should how have a folder called `hammer`. The diagonalisation component is in the `hammer/Hammer` folder. Change to this folder and install the remaining dependencies using pip :: 

    cd hammer/Hammer
    cat requirements.txt | xargs sudo pip install 

Now we are ready to build and install our code. From the hammer folder run. ::

    python setup_f2py.py build
    sudo python setup_f2py.py install

    python setup.py build
    sudo python setup.py install

The executable scripts will get installed to your system path and you should now be able to try out the examples given in the `tutorials <tutorials>`_. 

You can run tests to ensure everything is working with::

    python setup.py test

    
- Other python modules mpi4py, numpy, scipy and cython may already be installed or can be installed using pip with commands. ::
  
    pip install mpi4py
    pip install numpy 
    pip install scipy
    pip install cython



Building with intel compilers
-------------------------------
The intel compilers can give a performance boost. To compile the these use the following command instead. :: 

    sudo python setup_f2py.py build --fcompiler=intelem install 



CFT wave function generator
-------------------------------
The hammer repository also contains functionality to construct FQH wave functions based on CFT correlators. Apart from the prerequisites of the main hammer package part of the (obsolete) CFT code also needs ARPACK. Wrappers for hdf5 is also needed as well as octave for some parts of the tests. Install them all using::

  sudo apt-get install libarpack2-dev octave libhdf5-serial-dev

To acces the code, download the hammer repository::
  
   sudo apt-get install git
   git clone https://bitbucket.org/nmoran/hammer.git

Build the files in the CFT catalouge::

  cd hammer/CFT_wave_functions
  make
  
  
  
Quick install instructions (ubuntu only)
------------------------------------------
There are pacakges for PETSc, SLEPc and their python wrappers in the standard ubuntu repositories. However these are not built with support for complex numbers so we will use some custom versions. Use the following commands to add the custom ppa repository and install these packages.::

    sudo apt-add-repository ppa:niall-moran/hammer
    sudo apt-get update
    sudo apt-get install python-petsc4py python-slepc4py python-cython

There are some additional python pacakges and dependencies which need to be installed. The easiest was to install these is with the pip python package manager. The following commands will install pip and some additional dependencies which we will need later.::

    sudo apt-get install python-pip python-dev libhdf5-dev libopenmpi-dev liblapack-dev libblas-dev gfortran build-essential

Now we download the source by cloning the git repository.::

    sudo apt-get install git
    git clone https://bitbucket.org/nmoran/hammer.git

You should how have a folder called `hammer`. The diagonalisation component is in the `hammer/Hammer` folder. Change to this folder and install the remaining dependencies using pip :: 

    cd hammer/Hammer
    cat requirements.txt | xargs sudo pip install 

Now we are ready to build and install our code. From the hammer folder run. ::

    python setup_f2py.py build
    sudo python setup_f2py.py install

    python setup.py build
    sudo python setup.py install

The executable scripts will get installed to your system path and you should now be able to try out the examples given in the `tutorials <tutorials>`_. 

You can run tests to ensure everything is working with::

    python setup.py test

