Details about sphinx documentation setup
-----------------------------------------
Spinix is a utility used to automatically generate documentation from source
code. This folder contains the soruce and configuration files that are used to
build the documentation for the hammer project. The basic file structure was
created using the command

```
sphinx-quickstart
```

where one should say yes to autodoc. Then to generate the api docs we run the
following command from the folder that was created in the first instance giving
the path the the hammer module source.

```
sphinx-apidoc -o . ../Hammer/hammer 
```
This creates a hammer.rst and a modules.rst. Now to build the actual
documentation we run

```
make html
```


