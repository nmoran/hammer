.. Hammer documentation master file, created by
   sphinx-quickstart on Thu Feb 25 14:29:44 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hammer's documentation!
==================================
Hammer is a set of numerical tools for treating systems of strongly interacting
quantum many body systems and is aimed primarily at the study of fractional
quantum Hall systems. It consists of multiple loosely coupled components. These
docs (for now) focus on the diagonalisation component.

The diagonalisation component contains utilities for diagonalising quantum Hall
systems on toroidal and spherical geometries. It also provides utilities for
manipulating the resulting wave-functions and calculating physical observables.
These utilities are written in python and rely on a number of libraries (in
particular SLEPc and PETSc, see requirements in install docs for a full list),
as well as making use of cython for speed critical parts of the code. 

For installation instructions see `install <install.html>`_, for some simple
example see `tutorials <tutorials.html>`_. To look at the usage of individual modules go to
`API docs <modules.html>`_.


Contents:

.. toctree::
   :maxdepth: 2

   install
   tutorials
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

