Tutorials
==============
.. role:: bash(code)
   :language: bash

Here we give some examples of typical usage of the Hammer diagonalisation component. For installation instructions see `installation <install.html>`_.

Torus diagonalisation
----------------------

The TorusDiagonalise.py script can be used for performing exact diagonalisation calculations on the torus. If run without any arguments it will diagonalise a square torus with 4 electron, 12 flux quanta using the lowest Landau level Coulomb interaction in the sector with total momentum 0. The lowlying eigenvalues will be output along with the error estimates. These will also be written to a file on disk. The behaviour of the script can be changed via command line arguements. To see a full list of options use ::

  TorusDiagonalise.py --help

It is advisable to use symmetries to reduce the computational effort required. The centre of mass momentum can be specified using the -c switch and the inversion sector with the --inversion-sector which takes values 0 and 1 corresponding to symmetric and antisymmetric sectors. The number of unique centre of mass symmetry sectors corresponds to number of fundamental unit cells. i.e. the quotient between the particle number and the numerator of the filling fraction (Ne/p=Ns/q)

As a small example, we show how one would find the overlap between the Laughlin state with the Coulomb ground state for 9 particles. To get the Laughlin state we can diagonalise a hardcore interaction. The following command will get the low lying states of the hardcore interaction for a system with 9 particles and write the states to disk. ::

  TorusDiagonalise.py -p 9 -l 27 -y 0 -c 0 --inversion-sector 0 --pseudopotentials 0.0 1.0 --eigenstate

We should see that as well as the csv file containing the energies, there are a number of vector files on the disk which are the eigenstates. The first state also has zero energy as we would expect. Modern computers have multiple processing cores so we can take advantage of this to speed up our computations. To use multiple process one runs the same command with :bash:`mpiexec -np n` prepended, where n is the number of processes to use. For example :: 
 
    mpiexec -np 4 TorusDiagonalise.py -p 9 -l 27 -y 0 -c 0 --inversion-sector 0 --pseudopotentials 0.0 1.0 --eigenstate
    
Will run the same command with four processes and we expect that if there are four or more cores available, this will be nearly four times faster.

How we get the Coulomb ground state with::

    mpiexec -np 4 TorusDiagonalise.py -p 9 -l 27 -y 0 -c 0 --inversion-sector 0  --eigenstate

We can now take the overlap with:: 

    Overlap.py -a  Torus_p_9_Ns_27_K_0_P_0_I_0_tau1_0.000_tau2_1.000_maglen_1.00_LL0Coulomb_State_0.vec Torus_p_9_Ns_27_K_0_P_0_I_0_tau1_0.000_tau2_1.000_maglen_1.00_PseudoPotential_State_0.vec

Where the :bash:`-a` flag specifies that we want the absolute value. This gives an overlap of 0.9846 which is what we expect. 

Sphere diagonalisation
-----------------------
Diagonalisaton on the sphere is achieved via the :bash:`SphereDiagonalise.py` script. The usage of this script is similar to the
utilities provided by the Diagham package. To see full usage options for this script call with the :bash:`--help` switch.
The script constructs the basis on the sphere with the requested properties, constructs the Hamiltonian and iteratively
diagonalised to find the number of low lying eigenstates that were requested.

For example to diagonalise a hard core interaction to find the Laughlin state with four particles one would enter ::

  SphereDiagonalise.py -p 4 -l 9 --pseudopotentials 0.0 1.0 -f

This specfies to use a Hilbert space with four electrons, 10 flux quanta and a
hard core interaction which is specified via pseudopotential coefficients. The
hard core interaction has all pseudopotential coefficients zero except the V1
pseudopotential. The `-f` switch is given to tell the utility to ignore the fact
that there are less pseudopotentials than flux quanta given.

Pseudopotential files
~~~~~~~~~~~~~~~~~~~~~~~
Instead of explicitly giving the values of the pseudopotentials on the command line, these can be provided in a file via the
:bash:`--interaction-file` switch. The script expects a file in the same format that
is used by Diagham. These can be generated using the :bash:`CoulombPseudopotentials`
utility from Diagham. For example, the coefficients for the LLL Coulomb
interaction for the same system as above can be calculated with
:bash:`CoulombPseudopotentials -s 9`, which will create a file
:bash:`pseudopotential_coulomb_l_0_2s_9.dat` containing ::

  # pseudopotentials on the sphere for coulomb interaction 
  # in the Landau level N=0 for 2S=9 flux quanta
  #
  # Pseudopotentials = V_0 V_1 ...

  Pseudopotentials = 0.97624284500207 0.50131389337944 0.386727860607 0.33203907223833 0.29990625879591 0.27922306853413 0.26543427502627 0.25633367131108 0.25076120019562 0.2481076425216

L2 operator
~~~~~~~~~~~~~~
It is often very useful to add the L2 operator to the interaction. This allows
one isolate the states with the lowest L2 eigenvalues. This is achieved via the
:bash:`--l2-factor` switch. One can also then use the :bash:`--get-l2value` switch to also
give the L2 eigenvalue for each of the calculated eigenstates.

Transforming states
----------------------
Both the sphere and torus diagonalisation utilities can output the Fock
coefficients of the wave-functions they calculate as a PETSc binary vector file
(when the :bash:`--eigenstate`) option is provided. The hammer package has a number of
utilities for examining and manipulating these files. There are scripts for
converting between formats. These are called :bash:`ConvertASCIIToBin.py`,
:bash:`ConvertBinToASCII.py` and :bash:`ConvertBinToHDF.py`. The :bash:`TransformState.py` utility
allows one to manipulate these outputted states. This allows one to convert
states in a particular symmetry sector to the full unsymmetrised sector, to
translate all the orbitals in a state and to particle hole conjugate the state.

Generate CFT wave functions
---------------------------

The catalogue :bash:`CFT_wave_functions` contains routines for generating real space wave functions based on correlation functions in conformal field theory (CFT). All of these wave functions as based on the use of conformal field theory, and includes the most simple state, the Laughlin state.

For a recent review on the usage of conformal field theory to describe hierarchy states, please read `this review <https://arxiv.org/abs/1601.01697>`_ by Hans Hansson et. all.

To familiarize ourselves with the CFT wave function generator we begin by generating the Laughlin state and comparing it to the Coulomb ground state. Currently the CFT generator is not installed in the user path so you need to run it from the :bash:`CFT_wave_functions` catalogue.::

  cd $HammerDirectory/CFT_wave_functions

To generate 10^4 samples for a system of 5 electrons run::

  ./Run_CFT_Wfn -D 1 --Kmatrix 3 -Ne 5 -N 10000

As the program :bash:`Run_CFT_Wfn` is mainly working with K-matrices you need to supply the K-matrix for the Laughlin state. This is done with the flags :code:`-D 1 --Kmatrix 3`. To set number of electrons to 5 use :bash:`-Ne 5` and to generate 10^4 MC samples use :code:`-N 10000`.
Note that three files. The first :bash:`positions.hdf5` contains the positions used to samples the wave function. The second :bash:`weight_values.hdf5` the weight of the probability distribution that generated the positions. The last file  :bash:`wf_values.hdf5` contains the actual value of the wave function at the positions generated.

To have something to compare to we may now generate the numerically exact Coulomb ground state by::

  TorusDiagonalise.py -Ne 5 -Ns 15 -K1 0 --eigenstate

The structure of the filenames can be obtained using the :bash:`FQHFileNames.py` module. We supply :bash:`FQHFileNames.py` with the same flags as :bash:`TorusDiagonalise.py` and also add :bash:`--type 2` with means that we want the full length of filenames. We are interested in the ground state vector so we write::

  StateName=$(FQHFileNames.py -Ne 5 -Ns 15 -K1 0 --type 2)_State_0.vec

We are now ready to evaluate the Coulomb ground state at the same positions that where used for the Laughlin ground state::
  
  EvaluateStatesRealSpace.py $StateName --hdf5-positions positions.hdf5 --hdf5-output -o Coulomb

The flag :bash:`--hdf5-positions positions.hdf5` gives the positions to use for evaluating the wave function and :bash:`--hdf5-output` specifies the output to be in hdf5-format. Finally :bash:`-o Coulomb` gives the path and prefix for the output file :bash:`Coulomb.hdf5`::

  MCImportanceOverlaps.py --wfn-list wf_values.hdf5 --wgt-list weight_values.hdf5 --ed-list Coulomb.hdf5

We have now generated at file names :bash:`Overlaps.csv` containing the overlap of the two states. We can inspect the absolute overlap (row 6) by::

  cat Overlaps.csv |  awk 'BEGIN { FS = "," } ; { print $6 }'

and should (remember the ever present MC errors) find an overlap that is ~0.997.
