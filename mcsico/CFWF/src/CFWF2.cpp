#include "CFWF2.h"

int main(int argc, const char* argv[]) {

	vars V; setupV(V); clock_t starttime=clock(), argr=argin(argc, argv, V);
	if(argr==1) return 0; else if(argr==-1) {showHelp(); return 0;}
	if(V.i["sr"]!=-1) {
		if(strcmp(V.s["fn"].c_str(),"output.dat")!=0 || V.i["sd"]!=0) {cerr << "ERROR: Parameters '-fn' and '-sd' will be ignored when '-sr' is in use" << endl; return 0;}
		stringstream ss; ss << "./n" << V.i["n"] << "/N" << V.i["N"] << "/configs/N" << V.i["N"] 
							<< "os" << n2s(V.ull["os"]) << "_" << V.i["sr"] << ".dat";
		V.s["fn"]=ss.str(); V.i["sd"]=V.i["sr"];
	}
	V.i["sd"]+=time(0); CRandomSFMT rangen(V.i["sd"]);
	getstep gs; int nstep=gs.checkN(V.i["N"], V.i["n"]);
	if(true) {
		if(!different(V.d["st"], 0.26, 6)) {
			if(nstep==-1) {cout << "ERROR: step not found or supplied" << endl; return 0;}
			else {V.d["st"]=gs.steps[V.i["n"]-2][nstep];}
		} V.d["st"]*=pi;
	}
	disp(V); e(); cout << endl <<  "Program started " << timestring() << endl << "-----------------------------------------" << endl << endl;
	
	int tk=V.i["tk"]; 
	if(tk==1) n_CF_MC2(V, rangen);
	else if(tk==3)  LE_arc(V, rangen);
	else if(tk==4)  LE_chord(V, rangen);
	else if(tk==5)  Lcoords(V, rangen); // Get (...)_s.dat
	else {stringstream ss; ss << tk; WE(2, ss.str()); return 0;}
	
	double sec=((double)clock()-starttime)/CLOCKS_PER_SEC; cout << setprecision(4) << endl;
	cout << "-----------------------------------------" << endl << "Total Runtime: " << sec << "s = " << sec/60.0 << "min" << endl;
	cout << endl <<  "Program finished " << timestring() << endl;
	return 0;
}

inline double abs(alglib::complex a) {return sqrt(a.x*a.x+a.y*a.y);}
inline alglib::complex exp(alglib::complex a) {return exp(a.x)*alglib::complex(cos(a.y), sin(a.y));}
inline alglib::complex pow(alglib::complex a, int n) {complex<double> c=complex<double>(a.x, a.y); return alglib::complex(real(pow(c, n)), imag(pow(c, n)));}
inline alglib::complex loga(alglib::complex a) {complex<double> c=complex<double>(a.x, a.y); return alglib::complex(real(log(c)), imag(log(c)));}
inline void newtp(double theta, double addt, double phi, double addp, double& newt, double& newp) {
	newt=theta+addt;
	if(newt<0) {newt=-newt; phi=fmod(phi+pi,twopi); }
	else if(newt>pi) {newt=twopi-newt; phi=fmod(phi+pi,twopi); }
	newp=fmod(phi+addp, twopi);
	if(newp<0) newp+=twopi;
	
	return;
}
inline complex<double> log(alglib::complex a) {return log(complex<double>(a.x, a.y));}
inline alglib::complex cd2al(complex<double> c) {return alglib::complex(real(c), imag(c));}
inline void tp2uv(double theta, double phi, alglib::complex& u, alglib::complex& v) {
	double cp=cos(phi/2.0), sp=sin(phi/2.0);
	u=cos(theta/2.0)*alglib::complex(cp, sp);
	v=sin(theta/2.0)*alglib::complex(cp, -sp);
}
inline alglib::complex_1d_array cd2al(vector<complex<double> > c) {
	int n=c.size(); alglib::complex_1d_array a; a.setlength(n); for(int i=0;i<n;i++) a[i]=cd2al(c[i]);  return a;
}
inline void al2cd(alglib::complex_1d_array a, vector<complex<double> >& c) {
	if(a.length()!=c.size()) {cout << "ERROR: Vectors of unequal length" << endl; return;}
	for(int i=0;i<a.length();i++) c[i]=complex<double>(a[i].x, a[i].y);
}
inline void al2d(alglib::real_1d_array a, vector<double>& d) {
	if(a.length()!=d.size()) {cout << "ERROR: Vectors of unequal length" << endl; return;}
	for(int i=0;i<a.length();i++) d[i]=a[i];
}

alglib::complex_2d_array setupCFmat2(int N, int n, int twoq, alglib::complex_1d_array uv, 
									vector<vector<vector<double> > > logfafr, alglib::complex_2d_array logno) {
						
	alglib::complex_2d_array logesp=getlogesp2(N, uv); alglib::complex_2d_array CFmat; CFmat.setlength(N, N); int Nm1=N-1;
	for(int i=0;i<N;i++) { // rows; particle i
		int state=0; // state dep. on nu and m
		for(int nu=0;nu<n;nu++) { // "Internal" LL
			for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
				CFmat[i][state]=0; // initialize state (i, state)
				for(int s=0;s<=nu;s++) {
					if(s>=-mpq && s<=nu+twoq-mpq) { // only s-terms where binomial survives
						int om=mpq+s; alglib::complex tsum=0.0; // initialize t-sum
						for(int t=om;t<=Nm1+om-nu-twoq;t++) {
							//if(t&1) tsum-=exp(logno[nu][mpq+nu]+z2al(logesp(i, t))+logfafr[nu][om][t-om]+logvpows[i]+(mpq-t)*loga(z[i]));
							if(t&1) tsum-=exp(logno[nu][mpq+nu]+logesp(i, t)+logfafr[nu][om][t-om]+(Nm1+mpq-twoq-t)*loga(uv[2*i+1])+(t-mpq)*loga(uv[2*i]));
							//else tsum+=exp(logno[nu][mpq+nu]+z2al(logesp(i, t))+logfafr[nu][om][t-om]+logvpows[i]+(mpq-t)*loga(z[i]));
							else tsum+=exp(logno[nu][mpq+nu]+logesp(i, t)+logfafr[nu][om][t-om]+(Nm1+mpq-twoq-t)*loga(uv[2*i+1])+(t-mpq)*loga(uv[2*i]));
						} // t end
						if(s&1) CFmat[i][state]-=bin(nu, s)*tsum;
						else CFmat[i][state]+=bin(nu, s)*tsum;
					} // s end (w.r.t. if test)
				}
				state++;
			} // m end (w.r.t. |q|+m
		} // nu end
	} // i end
	//ee(); 
	return CFmat;
}

alglib::complex_2d_array getlogesp2(int N, alglib::complex_1d_array uv) {
	alglib::complex_2d_array *f=new alglib::complex_2d_array[N], logesp; logesp.setlength(N, N); for(int i=0;i<N;i++) f[i].setlength(N, N+1);
	for(int i=0;i<N;i++) {
		for(int t=0;t<N;t++) {
			if(t==0) f[i][t][0]=1;
			else f[i][t][0]=0;
			for(int n=1;n<=N;n++) {
				if(n-1==i) f[i][t][n]=f[i][t][n-1];
				else if(t==0) f[i][t][n]=f[i][t][n-1]*uv[2*n-2]; 
				else f[i][t][n]=f[i][t][n-1]*uv[2*n-2]+f[i][t-1][n-1]*uv[2*n-1];
			}
			logesp[i][t]=loga(f[i][t][N]);
		}
	}
	delete [] f;
	return logesp;
}

vector<vector<vector<double> > > getlogfafr(int N, int n, int twoq) {
	vector<vector<vector<double> > > logfafr(n, vector<vector<double> >(twoq+n, vector<double>(N-twoq)));
	for(int nu=0;nu<n;nu++) {
		for(int om=0;om<=twoq+nu;om++) {
			for(int tmo=0;tmo<N-nu-twoq;tmo++) {
				logfafr[nu][om][tmo]=log(bin(twoq+nu, om));
				for(int i=N-tmo-nu-twoq;i<=N-1-tmo-om;i++) logfafr[nu][om][tmo]+=log(i);
				for(int i=tmo+1;i<=tmo+om;i++) logfafr[nu][om][tmo]+=log(i);
			}
		}
	}
	return logfafr;
}

alglib::complex_2d_array lognrm(int N, int n, int twoq, int twoSnCF, int p) {
	alglib::complex_2d_array logno; logno.setlength(n, twoq+2*n-1); double q=twoq/2.0;
	for(int nu=0;nu<n;nu++) {
		for(double m=-twoq/2.0-nu;m<=twoq/2.0+nu;m++) {
			int i=(2.0*m+twoq)/2.0+nu;
			logno[nu][i]=loga(alglib::complex(facdivfac(twoSnCF+1, 2*p*(N-1)+nu+1)*(1.0-2.0*(nu%2)), 0))
						+0.5*loga(alglib::complex((twoq+2.0*nu+1.0)*facdivfac((int)(q-m)+nu, nu)*facdivfac((int)(q+m)+nu, twoq+nu)/(4.0*pi), 0));
		}
	}
	return logno;
}

void timetest(vars V, CRandomSFMT& rangen) {

	// int runtime=clock(), s=50000; alglib::complex c=1, c2;
	// for(int i=1;i<=s;i++) {c.x=rangen.Random(); c.y=rangen.Random(); c2=pow(c, i);} cout << al2cd(c2) << endl;
	// cout << "Time 1: " << timeint(runtime) << endl;
	// for(int i=1;i<=s;i++) {c.x=rangen.Random(); c.y=rangen.Random(); c2=pipow(c, i);} cout << al2cd(c2) << endl;
	// cout << "Time 2: " << timeint(runtime) << endl;

}

void setupV(vars& V) {

	V.i["N"]=6; V.i["ot"]=100; V.i["oh"]=1; V.i["tk"]=1; V.i["sd"]=0; V.i["n"]=2; 
	V.i["p"]=1; V.i["rs"]=1; V.i["sr"]=-1;
	V.ull["os"]=100; V.b["ra"]=false; V.b["np"]=false;
	V.d["st"]=-1;
	V.s["fn"]="output.dat"; V.s["pl"]="Random";
}

void showHelp() { cout << endl << "Help not configured" << endl << endl;
	// cout << endl << "Command line arguments (d=default):" << endl << endl;
	// cout << "\t-tk\t Task to perform (d=1)" << endl << "\t\t\t1: Simultaneous MC of alt. Laughlin with alternative bootstrap, save data (d)" << endl
		 // << "\t\t\t2: Simultaneous MC of alt. Laughlin with alternative bootstrap, display data" << endl
		 // << "\t\t\t3: Simultaneous MC with alternative bootstrap, save data" << endl << "\t\t\t4: Simultaneous MC with no error calculation, save data" 
		 // << endl << "\t\t\t5: Simultaneous MC with alternative bootstrap, display data"
		 // << endl << "\t\t\t6: Simultaneous MC with no error calculation, display data" << endl << "\t\t\t7: Calculate standard Laughlin energy" << endl
		 // << "\t\t\t8: Combine data from runs of task 1" << endl << "\t\t\t9: Concatenate data from folder into single file" << endl;
	// cout << "\t-N\t Number of particles (d=2)" << endl;
	// cout << "\t-os\t Number of samples (d=10^4)" << endl;
	// cout << "\t-oh\t Harvest period of simultaneous MC in factors of N (d=1)" << endl;
	// cout << "\t-it\t Thermalization of simultaneous MC in factors of N (d=10k)" << endl;
	// cout << "\t-ot\t Thermalization of electrons in factors of N (d=10k)" << endl;
	// cout << "\t-ist\t Ghost steplength in factors of pi (d -> 50% accepted)" << endl;
	// cout << "\t-st\t Electon steplength in factors of pi (d -> 50% accepted)" << endl;
	// cout << "\t-bs\t Number of bootstrap resamplings (d=100)" << endl;
	// cout << "\t-fn\t Output filename (d=\"output.dat\")" << endl;
	// cout << "\t-ifn\t Input filename (or input folder for task 7) (d=\"output.dat\")" << endl;
	// cout << "\t-sd\t Random seed; -1 -> from cpu clock (d=-1)" << endl;
	// cout << endl << "Filename template: ./MCsicoData/[acc/]MCsico_N..is..os..[oh..]_[bs../noE].dat" << endl;
	return;
}

void setupwf2(vars V, alglib::complex_1d_array uv, vector<vector<vector<double> > >& logfafr, alglib::complex_2d_array& logno, double& E) {

	int N=V.i["N"], twoq=V.i["twoq"], n=V.i["n"];
	logfafr=getlogfafr(N, n, twoq); logno=lognrm(N, n, twoq, V.i["twoSnCF"], 1);
	E=EcalcAwoR(N, uv);
}

double initWF2(int N, int twoq, int n, alglib::complex_1d_array uv, vector<vector<vector<double> > > logfafr, alglib::complex_2d_array logno) {
	
	alglib::complex_2d_array CFmat=setupCFmat2(N, n, twoq, uv, logfafr, logno);
	alglib::complex_2d_array LUCF=CFmat; alglib::integer_1d_array pivots; cmatrixlu(LUCF, N, N, pivots);
	complex<double> logdetCF=0;
	for(int i=0;i<N;i++) logdetCF+=log(LUCF(i,i));
	return 2.0*real(logdetCF);
}

void n_CF_MC2(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], n=V.i["n"], twoq=V.i["twoq"], accept=0, ot=V.i["ot"], oh=V.i["oh"], twoSnCF=V.i["twoSnCF"]; 
	unsigned long long os=V.ull["os"];
	double rans[4], twologWFold, E, step; alglib::complex_1d_array uv; alglib::real_1d_array tp; 
	if (V.d["st"]>0) step=pi*V.d["st"];
	else if(n==2) step=pi*(pow(N, -0.2)-0.31);
	//else if(n==3) step=pi*(pow(N, -0.027)-0.8535);
	else {cerr << "ERROR: Step not found for n = " << n << endl; return;} cout << "step  " << step << endl;
	uv.setlength(2*N); tp.setlength(2*N);
	stringstream ss; ss << "./Startconfs/CFstartn" << n << "N" << N << ".dat"; ifstream sc(ss.str().c_str()); sc >> setprecision(15);
	if(V.b["ra"]) {cout << "Random starting point" << endl; rantp(N, tp, rangen); tp2uv(N, tp, uv);}
	else if(!sc || V.b["np"]) {cout << "Thermalizing with Laughlin m=3 wavefunction" << endl; if(elcoords(N, V.s["pl"], tp, uv, rangen)!=0) return;} 
	else {cout << "Starting with previously thermalized N=" << N << " configuration" << endl;
				 for(int i=0;i<2*N;i+=2) sc >> tp[i] >> tp[i+1]; tp2uv(N, tp, uv);
	} sc.close();
	alglib::complex_2d_array logno; vector<vector<vector<double> > > logfafr; ofstream out(V.s["fn"].c_str(), ios::binary); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
		
	setupwf2(V, uv, logfafr, logno, E); double secs; clock_t time=clock(); 
	twologWFold=initWF2(N, twoq, n, uv, logfafr, logno); cout << "Thermalizing...    "; cout.flush();
	for(int i=0;i<ot;i++) { 
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		n_CF_it2(N, n, twoq, uv, tp, rans, accept, step, logfafr, logno, twologWFold);
	}
	secs=timeint(time); cout << endl << "Thermalization time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl; 
	
	accept=0; int twoN=2*N;
	for(unsigned long long i=0;i<os;i++) { 
		for(int j=0;j<twoN;j+=2) {out.write((char *) &tp[j], sizeof(double)); out.write((char *) &tp[j+1], sizeof(double));}
		for(int j=0;j<oh;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			n_CF_it2(N, n, twoq, uv, tp, rans, accept, step, logfafr, logno, twologWFold);
		}
	} 
	cout << "Total # samples: " << n2s(os);
	secs=timeint(time); cout << endl << "Sampling time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl;  
	
	cout << setprecision(3) << 100.0*accept/((double)os*oh) << "% accepted" << endl; 
	out.close();
	
}

void n_CF_it2(int N, int n, int twoq, alglib::complex_1d_array& uv, alglib::real_1d_array& tp, double rans[], int& accept, double step,
				vector<vector<vector<double> > > logfafr, alglib::complex_2d_array logno, double& twologWFold) {

	int r=2*floor(N*rans[0]);
	alglib::complex_1d_array uv2=uv; alglib::real_1d_array tp2=tp;
	newtp(tp[r], step*(rans[1]-0.5), tp[r+1], step*(2.0*rans[2]-1.0), tp2[r], tp2[r+1]);
	tp2uv(tp2[r], tp2[r+1], uv2[r], uv2[r+1]);
	
	alglib::complex_2d_array CFmat=setupCFmat2(N, n, twoq, uv2, logfafr, logno);
	alglib::complex_2d_array LUCF=CFmat; alglib::integer_1d_array pivots; cmatrixlu(LUCF, N, N, pivots);
	complex<double> logdetCF=0;
	for(int i=0;i<N;i++) logdetCF+=log(LUCF[i][i]);
	
	double twologWFnew=2.0*real(logdetCF);
	
	// Testing
	// out << endl <<"tp1={"; for(int i=0;i<2*N-2;i+=2) out << "{" << tp[i] << " , " << tp[i+1] << "}," << endl; 
	// out << "{" << tp[2*N-2] << " , " << tp[2*N-1] << "}};";
	// out << endl <<"tp2={"; for(int i=0;i<r;i+=2) out << "{" << tp[i] << " , " << tp[i+1] << "}," << endl;
	// if(r<2*N-2) {
		// out << "{" << newt << " , " << newp << "},;" << endl;
		// for(int i=r+2;i<2*N-2;i+=2) out << "{" << tp[i] << " , " << tp[i+1] << "}," << endl;
		// out << "{" << tp[2*N-2] << " , " << tp[2*N-1] << "}};" << endl;
	// }
	// else out << "{" << newt << " , " << newp << "}};" << endl;
	// out << logdetCF << 4.0*real(tmp) << "    " << twologWFold << "    " << exp(twologWFnew-twologWFold+6.0*real(tmp)) << endl;
	
	if(exp(twologWFnew-twologWFold)*sin(tp2[r])/sin(tp[r])>rans[3]) {
		twologWFold=twologWFnew;
		tp=tp2; uv=uv2;
		accept++;
	}
	
	return;
}