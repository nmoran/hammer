using namespace std;
#include "../../general/src/MCFSutility2.h"

void n_CF_MC2(vars, CRandomSFMT&);
void setupwf2(vars, alglib::complex_1d_array, vector<vector<vector<double> > >&, alglib::complex_2d_array&, double&);
double initWF2(int, int, int, alglib::complex_1d_array, vector<vector<vector<double> > >, alglib::complex_2d_array);
alglib::complex_2d_array createPM(int, alglib::complex_1d_array);
void n_CF_it2(int, int, int, alglib::complex_1d_array&, alglib::real_1d_array&, double[], int&, double,
				vector<vector<vector<double> > >, alglib::complex_2d_array, double&);
alglib::complex_2d_array setupCFmat2(int, int, int, alglib::complex_1d_array, vector<vector<vector<double> > >, alglib::complex_2d_array);
alglib::complex_2d_array getlogesp2(int, alglib::complex_1d_array);
void showHelp();
void setupV(vars&);
vector<vector<vector<double> > > getlogfafr(int, int, int);
alglib::complex_2d_array lognrm(int, int, int, int, int);
inline void updatePM(int, alglib::complex_2d_array&, alglib::complex_1d_array uv, alglib::complex, alglib::complex, int);

class getstep{

	private:
	vector<int> nos;

	public:
	int **Ns; double **steps;

	 getstep() {
		Ns=new int*[3]; steps=new double*[3];
	 
		nos.push_back(1); Ns[0]=new int[nos[0]]; steps[0]=new double[nos[0]];
		Ns[0][0]=10;
		steps[0][0]=0.34;
	 
		nos.push_back(3); Ns[1]=new int[nos[1]]; steps[1]=new double[nos[1]];
		Ns[1][0]=6; Ns[1][1]=14; Ns[1][1]=22;
		steps[1][0]=0.4; steps[1][1]=0.28; steps[1][1]=0.22;
		
		nos.push_back(1); Ns[2]=new int[nos[2]]; steps[2]=new double[nos[2]];
		Ns[2][0]=20;
		steps[2][0]=0.137;
	}
	
	~getstep() {
		delete Ns[0]; delete Ns[1]; delete Ns[2]; delete steps[0]; delete steps[1]; delete steps[2];
		delete [] Ns; delete [] steps;
	}

	int checkN(int N, int n) {
		n-=2; int found=-1;
		for(int i=0;i<nos[n];i++) {if(Ns[n][i]==N) {found=i; break;}}
		return found;
	}

};