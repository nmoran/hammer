ALOBJECTS = ap.o alglibinternal.o specialfunctions.o alglibmisc.o linalg.o interpolation.o solvers.o optimization.o integration.o
ALHFILES = $G/ap.h $G/alglibinternal.h $G/stdafx.h $G/alglibmisc.h $G/specialfunctions.h $G/linalg.h $G/interpolation.h $G/solvers.h $G/optimization.h $G/integration.h

G = ../../general/src

COMPILER=g++

ifeq ($(cmp),intel)
	COMPILER=icpc
endif

CFWF2: CFWF2.cpp sfmt.o MCFSutility2.o $(ALOBJECTS) $(ALHFILES) CFWF2.h $G/MCFSutility2.h
	${COMPILER} CFWF2.cpp sfmt.o MCFSutility2.o $(ALOBJECTS) -msse2 -std=c++11 -O2 -o ../CFWF2
	
MCFSutility2.o: $G/MCFSutility2.cpp $G/MCFSutility2.h
	${COMPILER} -c -O2 -msse2 -std=c++11 $G/MCFSutility2.cpp
	
sfmt.o: $G/sfmt.cpp $G/randomc.h $G/userintf.cpp $G/sfmt.h
	${COMPILER} -c -O2 -msse2 $G/userintf.cpp $G/sfmt.cpp 
	
ap.o: $G/ap.cpp $G/ap.h $G/stdafx.h
	${COMPILER} -c -O2 $G/ap.cpp
	
alglibinternal.o: $G/alglibinternal.cpp $G/alglibinternal.h $G/stdafx.h $G/ap.h $G/alglibmisc.h
	${COMPILER} -c -O2 $G/alglibinternal.cpp
	
specialfunctions.o: $G/specialfunctions.cpp $G/specialfunctions.h $G/stdafx.h $G/alglibinternal.h $G/ap.h
	${COMPILER} -c -O2 $G/specialfunctions.cpp
	
alglibmisc.o: $G/alglibmisc.cpp $G/alglibmisc.h $G/alglibinternal.h $G/ap.h
	${COMPILER} -c -O2 $G/alglibmisc.cpp
	
linalg.o: $G/linalg.cpp $G/linalg.h $G/alglibinternal.h $G/stdafx.h $G/ap.h $G/alglibmisc.h
	${COMPILER} -c -O2 $G/linalg.cpp
	
integration.o: $G/integration.cpp $G/integration.h $G/alglibinternal.h $G/stdafx.h $G/ap.h $G/alglibmisc.h $G/linalg.h $G/specialfunctions.h
	${COMPILER} -c -O2 $G/integration.cpp
	
solvers.o: $G/solvers.cpp $G/solvers.h $G/alglibinternal.h $G/stdafx.h $G/ap.h $G/alglibmisc.h $G/linalg.h
	${COMPILER} -c -O2 $G/solvers.cpp
	
optimization.o: $G/optimization.cpp $G/optimization.h $G/alglibinternal.h $G/stdafx.h $G/ap.h $G/alglibmisc.h $G/linalg.h $G/solvers.h
	${COMPILER} -c -O2 $G/optimization.cpp
	
interpolation.o: $G/interpolation.cpp $G/interpolation.h $G/alglibinternal.h $G/stdafx.h $G/ap.h $G/alglibmisc.h $G/linalg.h $G/solvers.h $G/optimization.h $G/specialfunctions.h $G/integration.h
	${COMPILER} -c -O2 $G/interpolation.cpp
	
clean:
	rm CFWF2.exe CFWF2 $(ALOBJECTS) sfmt.o MCFSutility2.o