using namespace std;
#include "../../general/src/MCFSutility2.h"

void analyse_arc(vars, CRandomSFMT&);
void analyse_chord(vars, CRandomSFMT&);
void analyse_chord_simple(vars, CRandomSFMT&);
void analyse_z(vars, CRandomSFMT&);
void expansion(vars, CRandomSFMT&);
void expansion_constrained(vars, CRandomSFMT&);
void expansion_binless(vars, CRandomSFMT&);
void count_cfs(vars, CRandomSFMT&);
void showHelp();
void setupV(vars&);
vector<vector<double> > getSVFc(int, int);
alglib::real_2d_array getfitmatrix(double, int, int);
alglib::real_2d_array getfitmatrixz(double, int, int, int);
double statstruc(double, vector<double>, int, double);
double statstrucP(double, vector<double>, int, double);
int fint(unsigned, const double*, void*, unsigned, double*);
double oscstrP(double, vector<double>, int, double, double);
double oscstrP2(double, vector<double>, int, double, double, double);
double oscstrP2_2LL(double, vector<double>, int, double, double, double);
int fint2(unsigned, const double*, void*, unsigned, double*);
int fint2_2LL(unsigned, const double*, void*, unsigned, double*);
alglib::real_2d_array getGamma(int, int, vector<double>);