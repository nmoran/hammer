#include "anWF.h"

//  ./anWF -tk 2 -wf MRWF -N 20 -fn ./MCData/MR/p2/N20/configs/ -fn2 ./MCData/MR/Analyzed/N20c_os2M.dat -n 2 -os 2000000

int main(int argc, const char* argv[]) {

	vars V; setupV(V); clock_t starttime=clock(), argr=argin(argc, argv, V);
	if(argr==1) return 0; else if(argr==-1) {showHelp(); return 0;}
	if(V.i["sr"]!=-1) {
		if(strcmp(V.s["fn"].c_str(),"inputfolder")!=0 || V.i["sd"]!=0 || strcmp(V.s["fn2"].c_str(),"output.dat")!=0) {
			cerr << "ERROR: Parameters '-fn', '-fn2' and '-sd' will be ignored when '-sr' is in use" << endl; return 0;
		}
		stringstream ss; ss << "../" << V.s["wf"] << "/";
		if(strcmp(V.s["wf"].c_str(), "BSWF")==0 || strcmp(V.s["wf"].c_str(), "CFWF")==0) ss << "n" << V.i["n"];
		else if(strcmp(V.s["wf"].c_str(), "MRWF")==0 || strcmp(V.s["wf"].c_str(), "LAWF")==0) ss << "p" << V.i["p"];
		ss << "/N" << V.i["N"] << "/configs/"; V.s["fn"]=ss.str();
		ss.str(""); ss << "../" << V.s["wf"] << "/Analyzed/N" << V.i["N"];
		if(V.i["tk"]==1) ss << "a"; else if(V.i["tk"]==2) ss << "c"; 
		ss << "_os" << n2s(V.ull["os"]) << "s" << n2s(V.i["s"]) << "_" << V.i["sr"] << ".dat"; 
		V.s["fn2"]=ss.str(); V.i["sd"]=V.i["sr"];
	}
	int tk=V.i["tk"]; V.i["sd"]+=time(0); CRandomSFMT rangen(V.i["sd"]);
	if(tk==4) V.s["wf"]="-"; disp(V); ee(); cout <<  "Program started " << timestring() << endl << "-----------------------------------------" << endl << endl;
	
	if(tk==1) analyse_arc(V, rangen);
	else if(tk==2) analyse_chord(V, rangen);
	else if(tk==3) analyse_chord_simple(V, rangen);
	else if(tk==4) count_cfs(V, rangen);
	else if(tk==5) expansion(V, rangen);
	else if(tk==6) expansion_constrained(V, rangen);
	else if(tk==7) expansion_binless(V, rangen);
	else if(tk==8) analyse_z(V, rangen);
	else {stringstream ss; ss << tk; WE(2, ss.str()); return 0;}
	
	double sec=((double)clock()-starttime)/CLOCKS_PER_SEC; cout << setprecision(4) << endl;
	cout << "-----------------------------------------" << endl << "Total Runtime: " << sec << "s = " << sec/60.0 << "min" << endl;
	cout << endl <<  "Program finished " << timestring() << endl;
	return 0;
}

inline vector<complex<double> > qck_kcq(double k, double q, double tq) {
	vector<complex<double> > ret(2); ret[0]=k*q*complex<double>(cos(tq), -sin(tq))/2.0;
	ret[1]=conj(ret[0]); return ret;
}

inline int pick(double ps[], CRandomSFMT& rangen) {
	double r=rangen.Random();
	if(r<ps[0]) return 0;
	if(r<ps[1]) return 1;
	if(r<ps[2]) return 2;
	if(r<ps[3]) return 3;
	if(r<ps[4]) return 4;
	if(r<ps[5]) return 5;
	if(r<ps[6]) return 6;
	if(r<ps[7]) return 7;
	if(r<ps[8]) return 8;
	if(r<ps[9]) return 9;
	return 10;
}

inline void bins_arc(int N, vector<complex<double> > uv, double L, vector<double>& bins, double cutoff) {

	for(int i=0;i<2*N;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			double dist=2.0*asin(abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]));
			if(dist<cutoff) bins[floor(dist/L)]++;
		}
	}			
}

inline void bins_chord(int N, vector<complex<double> > uv, double L, vector<double>& bins, double cutoff) {

	for(int i=0;i<2*N;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			double dist=2.0*abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
			if(dist<cutoff) bins[floor(dist/L)]++;
		}
	}
}

inline void bins_z(int N, vector<complex<double> > uv, double L, vector<double>& bins, double cutoff) {

	for(int i=0;i<2*N;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			double a=(abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]));
			double z=a/sqrt(1.0-a*a);
			if(z<cutoff) bins[floor(z/L)]++;
		}
	}			
}

inline void binless_chord(int N, double twoR, vector<complex<double> > uv, vector<double>& binless, int n, double cutoff) {

	for(int i=0;i<2*N;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			double dist=twoR*abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]); 
			if(dist<cutoff) {
				double d2=dist*dist; double d4=d2*d2, tmp=d2*exp(-d2/4.0)/**dist/sin(dist)*/; binless[0]+=tmp;
				for(int k=1;k<n;k++) {tmp*=d4; binless[k]+=tmp;}
			}
		}
	}			
}

void analyse_arc(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], s=V.i["s"], twoS=V.i["twoS"], bs=V.i["bs"]; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	} ee();
	
	unsigned long long configs=0, os=V.ull["os"]; if(os<1) os=ULLONG_MAX-10; int opfiles=0; double E, Eav=0, Eav2=0; double BG=bg(N, twoS, 'a'), twoRN=2.0*N*sqrt(twoS/2.0), R=sqrt(twoS/2.0);
	double shiftcorr=sqrt(twoS*V.d["nu"]/(double)N), fluxcorr=sqrt((twoS-2.0)/twoS);
	vector<double> tp(2*N); vector<complex<double> > uv(2*N);
	vector<double> bins(s, 0.0); double cutoff=pi; double L=cutoff/(double)s, dens=N/(twopi*twoS); vector<double> areas(s);
	for(int i=0;i<s;i++) areas[i]=twoS*pi*(cos(i*L)-cos((i+1.0)*L));
	
	vector<double> Evals(bs, 0), totn(bs, 0); double probs[10], prob=1.0/os, probinv=1.0-1.0/os; probs[0]=pow(probinv, os);
	for(int i=1;i<10;i++) probs[i]=probs[i-1]+bin(os, i)*pow(probinv, os-i)*pow(prob,i); int bsno;
	
	while(configs<os && opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		while(in.read((char *) &tp[0], sizeof(double)) && configs<os) { configs++;
			in.read((char *) &tp[1], sizeof(double)); for(int i=2;i<2*N;i+=2) {in.read((char *) &tp[i], sizeof(double)); in.read((char *) &tp[i+1], sizeof(double));}
			tp2uv(N, tp, uv); E=EcalcAwoR(N, uv); Eav+=E;
			for(int j=0;j<bs;j++) {bsno=pick(probs, rangen); Evals[j]+=bsno*E; totn[j]+=bsno;}
			bins_arc(N, uv, L, bins, cutoff);
		}
	}
	
	Eav/=(double)configs;
	double e=0, e2=0;	
	for(int i=0;i<bs;i++) {e+=Evals[i]/(double)totn[i]; e2+=Evals[i]*Evals[i]/((double)totn[i]*totn[i]);}
	double dE=sqrt(e2/(double)bs-e*e/((double)bs*bs))/(twoRN);
	for(int i=0;i<s;i++) bins[i]*=2.0/((double)N*dens*configs*areas[i]);
	
	out << "N = " << N  << ", 2S= " << twoS << ", " << n2s(configs) << " configurations, " << s << " bins, arc distance" << endl << endl;
	out << "E_ee/N: " << setprecision(15) << Eav/twoRN << endl;
	out << "LLL energy:  " << Eav/twoRN+BG << " +- " << dE << endl;
	out << "With shift correction:  " << shiftcorr*(Eav/twoRN+BG) << " +- " << shiftcorr*dE << endl << endl;
	out << "Pair correlation function: " << endl << endl << "\tr / l\t\t\t\tg ( r )" << endl;
	for(int i=0;i<s;i++) out << R*(i*L+L/2.0) << "    " << bins[i] << endl;
	
	cout << n2s(configs) << " configurations" << endl;
	cout << "E_LLL = " << setprecision(15) << Eav/twoRN+BG << " +- " << dE << endl;
	
	out.close();

}

void analyse_z(vars V, CRandomSFMT& rangen) {

	clock_t time=clock(); int N=V.i["N"], s=V.i["s"], twoS=V.i["twoS"], bs=V.i["bs"], nk=V.i["nk"], prec=V.i["pr"]; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	} ee();
	
	unsigned long long configs=0, os=V.ull["os"]; if(os<1) os=ULLONG_MAX-10; int opfiles=0; double R=sqrt(twoS/2.0);
	double shiftcorr=sqrt(twoS*V.d["nu"]/(double)N), fluxcorr=sqrt((twoS-2.0)/twoS);
	vector<double> tp(2*N); vector<complex<double> > uv(2*N);
	vector<double> bins(s, 0.0); double cutoff=V.d["co"]/R; double L=cutoff/(double)s, dens=N/(twopi*twoS); double RL=R*L; vector<double> areas(s);
	//for(int i=0;i<s;i++) areas[i]=twoS*pi*(cos(atan(i*L))-cos(atan((i+1.0)*L)));
	//for(int i=0;i<s;i++) areas[i]=twoS*pi*L*L*(2.0*i+1.0)/2.0;
	for(int i=0;i<s;i++) {
		double z1=i*L, z2=(i+1.0)*L;
		areas[i]=twopi*twoS*(z2*z2-z1*z1)/((1.0+z1*z1)*(1.0+z2*z2));
	}
	
	while(configs<os && opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		while(in.read((char *) &tp[0], sizeof(double)) && configs<os) { configs++;
			in.read((char *) &tp[1], sizeof(double)); for(int i=2;i<2*N;i+=2) {in.read((char *) &tp[i], sizeof(double)); in.read((char *) &tp[i+1], sizeof(double));}
			tp2uv(N, tp, uv); bins_z(N, uv, L, bins, cutoff);
		}
	}
	for(int i=0;i<s;i++) bins[i]*=2.0/((double)N*dens*configs*areas[i]);	
	for(int i=0;i<s;i++) out << 2.0*RL*(i+0.5) << "\t\t\t" << bins[i] << endl;
	double secs=timeint(time); cout << endl << "MC time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl << endl;
	
	alglib::real_2d_array fm=getfitmatrixz(L, nk, s, 2*twoS); alglib::real_1d_array gMC; gMC.setlength(s); out << endl << endl;
	//for(int i=0;i<s;i++) gMC[i]=bins[i]-1.0+exp(-RL*(i+0.5)*RL*(i+0.5)/2.0); alglib::real_1d_array cal; cal.setlength(nk);
	for(int i=0;i<s;i++) {double z=L*(i+0.5); gMC[i]=bins[i]-1.0+pow((1.0-z*z)/(1.0+z*z), 2*twoS); out << 2.0*z << "   " << gMC[i] << endl;} alglib::real_1d_array cal; cal.setlength(nk);
	alglib::ae_int_t info; alglib::lsfitreport rep; alglib::lsfitlinear(gMC, fm, info, cal, rep); out << endl << endl;
	vector<double> c=al2d(nk, cal); for(int i=0;i<nk;i++) cout << c[i] << endl;
	
	out << endl << endl;
	for(int i=0;i<s;i++) {
		double z=L*(i+0.5), tmp=0;
		for(int j=0;j<nk;j++) tmp+=bin(2*twoS, 2*j+1)*c[j]*pow(z, 4*j+2)/pow(1+z*z, 2*twoS);
		out << 2.0*z*R << "      " << 1.0-pow((1-z*z)/(1+z*z), 2*twoS)+tmp << endl;
	}
	
	out << endl << endl;
	int Nk=100; for(int i=1;i<Nk;i++) {
		double k=3.0*i/Nk, fp=oscstrP2(k, c, nk, dens, prec, cutoff*R), sp=statstrucP(k, c, nk, dens);
		//double k=3.0*i/Nk, fp=oscstrP2(k, c, nk, 1.0/(twopi*3.0), prec, cutoff*R), sp=statstrucP(k, c, nk, 1.0/(twopi*3.0));
		out << k << "    " << statstruc(k, c, nk, dens) << "    " << sp << "    "
			<< fp << "    " << fp/sp << endl;
	}
	
	cout << n2s(configs) << " configurations" << endl;
	
	out.close(); return;

}

void analyse_chord(vars V, CRandomSFMT& rangen) {

	if(V.ull["os"]<1) cerr << "WARNING: Bootstrap only works with explicit # of configurations" << endl;
	int N=V.i["N"], n=V.i["n"], twoS=V.i["twoS"], s=V.i["s"], bs=V.i["bs"], kM=V.i["kM"]; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	} e();
	
	unsigned long long configs=0, os=V.ull["os"]; if(os<1) os=ULLONG_MAX-10; int opfiles=0; 
	double E, Eav=0, Eav2=0, E2LL, E2LL2, E2LLpal, E2LLav=0, E2LLav2=0, E2LLavpal=0, E2LLavSVF=0, E2LLavSVFl=0; vector<double> E2LLSVF;
	double BG=bg(N, twoS, 'c'), BGpal=bg(N, twoS, 'p'), BGs=bg(N, twoS, 's'), twoRN=2.0*N*sqrt(twoS/2.0), R=sqrt(twoS/2.0), R2=sqrt((twoS-2.0)/2.0);
	double shiftcorr=sqrt(twoS*V.d["nu"]/(double)N), fluxcorr=sqrt((twoS-2.0)/twoS);
	vector<double> bins(s, 0.0); double cutoff=2.0; double ta, tb, L=cutoff/(double)s, dens=N/(twopi*twoS); vector<double> areas(s);
	for(int i=0;i<s;i++) {
		ta=2.0*asin(L*i/2.0);
		tb=2.0*asin(L*(i+1.0)/2.0);
		areas[i]=pi*twoS*sin((tb-ta)/2.0)*(sin(ta)+sin(tb));
	} 
	vector<double> tp(2*N); vector<complex<double> > uv(2*N);
	
	vector<double> Evals(bs, 0), totn(bs, 0), Evals2LL(bs, 0), Evals2LL2(bs, 0), Evals2LLpal(bs, 0), Evals2LLSVF(bs, 0), Evals2LLSVFl(bs, 0); 
	double probs[10], prob=1.0/os, probinv=1.0-1.0/os; probs[0]=pow(probinv, os);
	for(int i=1;i<10;i++) probs[i]=probs[i-1]+bin(os, i)*pow(probinv, os-i)*pow(prob,i); int bsno;
	vector<vector<double> > SVFc=getSVFc(twoS, kM);
	
	while(configs<os && opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		while(in.read((char *) &tp[0], sizeof(double)) && configs<os) { configs++;
			in.read((char *) &tp[1], sizeof(double)); for(int i=2;i<2*N;i+=2) {in.read((char *) &tp[i], sizeof(double)); in.read((char *) &tp[i+1], sizeof(double));}
			tp2uv(N, tp, uv); E=EcalcCwoR(N, uv); Eav+=E; E2LL=EcalcC_2LL(N, R, uv); E2LL2=EcalcC_2LL(N, R2, uv); E2LLav+=E2LL; E2LLav2+=E2LL2;  
			E2LLpal=EcalcC_2LLpal(N, R, uv); E2LLavpal+=E2LLpal; E2LLSVF=EcalcC_2LLSVF(N, R, twoS, uv, SVFc, kM); 
			E2LLavSVF+=E2LLSVF[0]; E2LLavSVFl+=E2LLSVF[1];
			for(int j=0;j<bs;j++) {
				bsno=pick(probs, rangen); Evals[j]+=bsno*E; Evals2LL[j]+=bsno*E2LL; Evals2LL2[j]+=bsno*E2LL2; Evals2LLpal[j]+=bsno*E2LLpal; 
				Evals2LLSVF[j]+=bsno*E2LLSVF[0]; Evals2LLSVFl[j]+=bsno*E2LLSVF[1]; totn[j]+=bsno;
			}
			bins_chord(N, uv, L, bins, cutoff);
		}
	}
	
	Eav/=(double)configs; E2LLav/=(double)configs; E2LLav2/=(double)configs; E2LLavpal/=(double)configs; 
	E2LLavSVF/=(double)configs; E2LLavSVFl/=(double)configs;
	for(int i=0;i<s;i++) bins[i]*=2.0/((double)N*dens*configs*areas[i]);
	double e=0, e2=0, e2LL=0, e2LL2=0, e2LL_2=0, e2LL2_2=0, e2LLpal=0, e2LL2pal=0, e2LLSVF=0, e2LL2SVF=0, e2LLSVFl=0, e2LL2SVFl=0;	
	for(int i=0;i<bs;i++) {
		e+=Evals[i]/(double)totn[i]; e2+=Evals[i]*Evals[i]/((double)totn[i]*totn[i]);
		e2LL+=Evals2LL[i]/(double)totn[i]; e2LL2+=Evals2LL[i]*Evals2LL[i]/((double)totn[i]*totn[i]);
		e2LL_2+=Evals2LL2[i]/(double)totn[i]; e2LL2_2+=Evals2LL2[i]*Evals2LL2[i]/((double)totn[i]*totn[i]);
		e2LLpal+=Evals2LLpal[i]/(double)totn[i]; e2LL2pal+=Evals2LLpal[i]*Evals2LLpal[i]/((double)totn[i]*totn[i]);
		e2LLSVF+=Evals2LLSVF[i]/(double)totn[i]; e2LL2SVF+=Evals2LLSVF[i]*Evals2LLSVF[i]/((double)totn[i]*totn[i]);
		e2LLSVFl+=Evals2LLSVFl[i]/(double)totn[i]; e2LL2SVFl+=Evals2LLSVFl[i]*Evals2LLSVFl[i]/((double)totn[i]*totn[i]);
	}
	double dE=sqrt(e2/(double)bs-e*e/((double)bs*bs))/(twoRN), dE2LL=sqrt(e2LL2/(double)bs-e2LL*e2LL/((double)bs*bs))/(double)N;
	double dE2LL2=sqrt(e2LL2_2/(double)bs-e2LL_2*e2LL_2/((double)bs*bs))/(double)N;
	double dE2LLpal=sqrt(e2LL2pal/(double)bs-e2LLpal*e2LLpal/((double)bs*bs))/(double)N;
	double dE2LLSVF=sqrt(e2LL2SVF/(double)bs-e2LLSVF*e2LLSVF/((double)bs*bs))/(double)N;
	double dE2LLSVFl=sqrt(e2LL2SVFl/(double)bs-e2LLSVFl*e2LLSVFl/((double)bs*bs))/(double)N;
	
	out << "N = " << N << ", 2S = " << twoS << ", " << n2s(configs) << " configurations, " << s << " bins, chord distance" << endl << endl;
	out << "E_ee/N: " << setprecision(15) << Eav/twoRN << endl;
	out << "LLL energy:  " << Eav/twoRN+BG << " +- " << dE << endl;
	out << "With shift correction:  " << shiftcorr*(Eav/twoRN+BG) << " +- " << shiftcorr*dE << endl << endl;
	
	out << "2LL E_ee/N  " << E2LLav/(double)N << endl;
	out << "2LL energy:  " << E2LLav/(double)N+BG << " +- " << dE2LL << endl;
	out << "With shift correction:  " << shiftcorr*(E2LLav/(double)N+BG) << " +- " << shiftcorr*dE2LL << endl;
	out << "With shift and flux corrections:  " << shiftcorr*(fluxcorr*E2LLav/(double)N+BG) << " +- " << shiftcorr*fluxcorr*dE2LL << endl << endl;
	
	out << "2LL E_ee/N 2 " << E2LLav2/(double)N << endl;
	out << "2LL energy: 2 " << E2LLav2/(double)N+BG << " +- " << dE2LL2 << endl;
	out << "With shift correction: 2 " << shiftcorr*(E2LLav2/(double)N+BG) << " +- " << shiftcorr*dE2LL2 << endl;
	out << "With shift and flux corrections: 2 " << shiftcorr*(fluxcorr*E2LLav2/(double)N+BG) << " +- " << shiftcorr*fluxcorr*dE2LL2 << endl << endl;
	
	out << "2LL energy s:  " << E2LLav/(double)N+BGs << " +- " << dE2LL << endl;
	out << "With shift correction s:  " << shiftcorr*(E2LLav/(double)N+BGs) << " +- " << shiftcorr*dE2LL << endl;
	out << "With shift and flux corrections s:  " << shiftcorr*(fluxcorr*E2LLav/(double)N+BGs) << " +- " << shiftcorr*fluxcorr*dE2LL << endl << endl;
	
	out << "2LL energy pal:  " << E2LLavpal/(double)N+BGpal << " +- " << dE2LLpal << endl;
	out << "With shift correction pal:  " << shiftcorr*(E2LLavpal/(double)N+BG) << " +- " << shiftcorr*dE2LLpal << endl;
	out << "With shift and flux corrections pal:  " << shiftcorr*(fluxcorr*E2LLavpal/(double)N+BG) << " +- " << shiftcorr*fluxcorr*dE2LLpal << endl << endl;

	out << "2LL E_ee/N SVF sv:  " << E2LLavSVF/(double)N << endl;	
	out << "2LL energy SVF sv:  " << E2LLavSVF/(double)N+BG << " +- " << dE2LLSVF << endl;
	out << "With shift correction SVF sv:  " << shiftcorr*(E2LLavSVF/(double)N+BG) << " +- " << shiftcorr*dE2LLSVF << endl;
	out << "With shift and flux corrections SVF sv:  " << shiftcorr*(fluxcorr*E2LLavSVF/(double)N+BG) << " +- " << shiftcorr*fluxcorr*dE2LLSVF << endl << endl;

	out << "2LL E_ee/N SVF ls:  " << E2LLavSVFl/(double)N << endl;	
	out << "2LL energy SVF ls:  " << E2LLavSVFl/(double)N+BG << " +- " << dE2LLSVFl << endl;
	out << "With shift correction SVF ls:  " << shiftcorr*(E2LLavSVFl/(double)N+BG) << " +- " << shiftcorr*dE2LLSVFl << endl;
	out << "With shift and flux corrections SVF ls:  " << shiftcorr*(fluxcorr*E2LLavSVFl/(double)N+BG) << " +- " << shiftcorr*fluxcorr*dE2LLSVFl << endl << endl;

	out << "Pair correlation function: " << endl << endl << "\tr / l\t\t\tg ( r )" << endl;
	for(int i=0;i<s;i++) out << R*(i*L+L/2.0) << "\t\t\t" << bins[i] << endl;
	
	cout << n2s(configs) << " configurations" << endl;
	cout << "E_LLL = " << setprecision(15) << Eav/twoRN+BG << " +- " << dE << endl;
	cout << "E_2LL = " << setprecision(15) << E2LLav/(double)N+BG << " +- " << dE2LL << endl;

}

void analyse_chord_simple(vars V, CRandomSFMT& rangen) {

	if(V.ull["os"]<1) cerr << "WARNING: Bootstrap only works with explicit # of configurations" << endl;
	int N=V.i["N"], n=V.i["n"], twoS=V.i["twoS"], s=V.i["s"], bs=V.i["bs"], kM=V.i["kM"]; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	} e();
	
	unsigned long long configs=0, os=V.ull["os"]; if(os<1) os=ULLONG_MAX-10; int opfiles=0; 
	double E, Eav=0, E2LL2, E2LLav2=0;
	double BG=bg(N, twoS, 'c'), twoRN=2.0*N*sqrt(twoS/2.0), R=sqrt(twoS/2.0), R2=sqrt((twoS-2.0)/2.0);
	double shiftcorr=sqrt(twoS*V.d["nu"]/(double)N), fluxcorr=sqrt((twoS-2.0)/twoS);
	vector<double> bins(s, 0.0); double cutoff=2.0; double ta, tb, L=cutoff/(double)s, dens=N/(twopi*twoS); vector<double> areas(s);
	for(int i=0;i<s;i++) {
		ta=2.0*asin(L*i/2.0);
		tb=2.0*asin(L*(i+1.0)/2.0);
		areas[i]=pi*twoS*sin((tb-ta)/2.0)*(sin(ta)+sin(tb));
	} 
	vector<double> tp(2*N); vector<complex<double> > uv(2*N);
	
	vector<double> Evals(bs, 0), totn(bs, 0), Evals2LL2(bs, 0); 
	double probs[10], prob=1.0/os, probinv=1.0-1.0/os; probs[0]=pow(probinv, os);
	for(int i=1;i<10;i++) probs[i]=probs[i-1]+bin(os, i)*pow(probinv, os-i)*pow(prob,i); int bsno;
	
	while(configs<os && opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		while(in.read((char *) &tp[0], sizeof(double)) && configs<os) { configs++;
			in.read((char *) &tp[1], sizeof(double)); for(int i=2;i<2*N;i+=2) {in.read((char *) &tp[i], sizeof(double)); in.read((char *) &tp[i+1], sizeof(double));}
			tp2uv(N, tp, uv); E=EcalcCwoR(N, uv); Eav+=E; E2LL2=EcalcC_2LL(N, R2, uv); E2LLav2+=E2LL2;   
			for(int j=0;j<bs;j++) {
				bsno=pick(probs, rangen); Evals[j]+=bsno*E; Evals2LL2[j]+=bsno*E2LL2; totn[j]+=bsno;
			}
			bins_chord(N, uv, L, bins, cutoff);
		}
	}
	
	Eav/=(double)configs; E2LLav2/=(double)configs;
	for(int i=0;i<s;i++) bins[i]*=2.0/((double)N*dens*configs*areas[i]);
	double e=0, e2=0, e2LL_2=0, e2LL2_2=0;	
	for(int i=0;i<bs;i++) {
		e+=Evals[i]/(double)totn[i]; e2+=Evals[i]*Evals[i]/((double)totn[i]*totn[i]);
		e2LL_2+=Evals2LL2[i]/(double)totn[i]; e2LL2_2+=Evals2LL2[i]*Evals2LL2[i]/((double)totn[i]*totn[i]);
	}
	double dE=sqrt(e2/(double)bs-e*e/((double)bs*bs))/(twoRN);
	double dE2LL2=sqrt(e2LL2_2/(double)bs-e2LL_2*e2LL_2/((double)bs*bs))/(double)N;
	
	out << "N = " << N << ", 2S = " << twoS << ", " << n2s(configs) << " configurations, " << s << " bins, chord distance" << endl << endl;
	out << "E_ee/N: " << setprecision(15) << Eav/twoRN << endl;
	out << "LLL energy:  " << Eav/twoRN+BG << " +- " << dE << endl;
	out << "With shift correction:  " << shiftcorr*(Eav/twoRN+BG) << " +- " << shiftcorr*dE << endl << endl;
	
	out << "2LL E_ee/N 2 " << E2LLav2/(double)N << endl;
	out << "2LL energy: 2 " << E2LLav2/(double)N+BG << " +- " << dE2LL2 << endl;
	out << "With shift correction: 2 " << shiftcorr*(E2LLav2/(double)N+BG) << " +- " << shiftcorr*dE2LL2 << endl;
	out << "With shift and flux corrections: 2 " << shiftcorr*(fluxcorr*E2LLav2/(double)N+BG) << " +- " << shiftcorr*fluxcorr*dE2LL2 << endl << endl;

	out << "Pair correlation function: " << endl << endl << "\tr / l\t\t\tg ( r )" << endl;
	for(int i=0;i<s;i++) out << R*(i*L+L/2.0) << "\t\t\t" << bins[i] << endl;
	
	cout << n2s(configs) << " configurations" << endl;
	cout << "E_LLL = " << setprecision(15) << Eav/twoRN+BG << " +- " << dE << endl;
	cout << "E_2LL = " << setprecision(15) << E2LLav2/(double)N+BG << " +- " << dE2LL2 << endl;

}

void expansion(vars V, CRandomSFMT& rangen) {

	if(V.ull["os"]<1) cerr << "WARNING: Bootstrap only works with explicit # of configurations" << endl;
	int N=V.i["N"], n=V.i["n"], twoS=V.i["twoS"], s=V.i["s"], bs=V.i["bs"], kM=V.i["kM"], nk=V.i["nk"]; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size(); clock_t time=clock();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	} e();
	
	unsigned long long configs=0, os=V.ull["os"]; if(os<1) os=ULLONG_MAX-10; int opfiles=0;
	double R=sqrt(twoS/2.0), prec=V.d["pr"];
	double shiftcorr=sqrt(twoS*V.d["nu"]/(double)N), fluxcorr=sqrt((twoS-2.0)/twoS), Rf=V.d["Rf"];
	vector<double> bins(s, 0.0); double cutoff=2.0; double ta, tb, L=cutoff/(double)s, dens=N/(twopi*twoS); vector<double> areas(s);
	for(int i=0;i<s;i++) {
		ta=2.0*asin(L*i/2.0);
		tb=2.0*asin(L*(i+1.0)/2.0);
		areas[i]=pi*twoS*sin((tb-ta)/2.0)*(sin(ta)+sin(tb));
	} 
	vector<double> tp(2*N); vector<complex<double> > uv(2*N);
	
	while(configs<os && opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		while(in.read((char *) &tp[0], sizeof(double)) && configs<os) { configs++;
			in.read((char *) &tp[1], sizeof(double)); for(int i=2;i<2*N;i+=2) {in.read((char *) &tp[i], sizeof(double)); in.read((char *) &tp[i+1], sizeof(double));}
			tp2uv(N, tp, uv); bins_chord(N, uv, L, bins, cutoff);
		}
	}
	double secs=timeint(time); cout << endl << "MC time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl << endl;
	
	for(int i=0;i<s;i++) bins[i]*=2.0/((double)N*dens*configs*areas[i]);
	
	double RL=R*L; alglib::real_2d_array fm=getfitmatrix(RL, nk, s); alglib::real_1d_array gMC; gMC.setlength(s);
	//for(int i=0;i<s;i++) gMC[i]=bins[i]-1.0+exp(-RL*(i+0.5)*RL*(i+0.5)/2.0); alglib::real_1d_array cal; cal.setlength(nk);
	for(int i=0;i<s;i++) {double r1=RL*i, r2=RL*(i+1.0); gMC[i]=bins[i]-1.0-2.0*(exp(-r2*r2/2.0)-exp(-r1*r1/2.0))/(r2*r2-r1*r1);} alglib::real_1d_array cal; cal.setlength(nk);
	alglib::ae_int_t info; alglib::lsfitreport rep; alglib::lsfitlinear(gMC, fm, info, cal, rep);
	vector<double> c=al2d(nk, cal); for(int i=0;i<nk;i++) cout << c[i] << endl;
	
	// LLL
	/*for(int i=0;i<s;i++) out << RL*(i+0.5) << "\t\t\t" << bins[i] << "  0 0 0" << endl;
	out << endl << endl;
	int Nk=100; for(int i=1;i<Nk;i++) {
		double k=3.0*i/Nk, fp=oscstrP2(k, c, nk, dens, prec, cutoff*R), sp=statstrucP(k, c, nk, dens);
		//double k=3.0*i/Nk, fp=oscstrP2(k, c, nk, 1.0/(twopi*3.0), prec, cutoff*R), sp=statstrucP(k, c, nk, 1.0/(twopi*3.0));
		out << k << "    " << statstruc(k, c, nk, dens) << "    " << sp << "    "
			<< fp << "    " << fp/sp << endl;
	}*/
	
	// 2LL
	for(int i=0;i<s;i++) out << RL*(i+0.5) << "\t\t\t" << bins[i] << "  0 0 0" << endl;
	out << endl << endl;
	int Nk=100; for(int i=1;i<Nk;i++) {
		double k=3.0*i/Nk, fp=oscstrP2_2LL(k, c, nk, dens, prec, cutoff*R), sp=statstrucP(k, c, nk, dens);
		out << k << "    " << statstruc(k, c, nk, dens) << "    " << sp << "    "
			<< fp << "    " << fp/sp << endl;
	}
	
	/*vector<double> cG(14); cG[0]=-1; cG[1]=0.51053; cG[2]=-0.02056; cG[3]=0.31003; cG[4]=-0.4905; cG[5]=0.20102; cG[6]=-0.00904; cG[7]=-0.00148; cG[8]=0;
	cG[9]=0.0012; cG[10]=0.0006; cG[11]=-0.0018; cG[12]=0; cG[13]=0; out << endl << endl;
	for(int i=1;i<100;i++) {
		//double k=2.5*i/100.0, fp=oscstrP2(k, cG, 14, 1.0/(twopi*3.0), prec, cutoff*R), sp=statstrucP(k, cG, 14, 1.0/(twopi*3.0));
		double k=2.5*i/100.0, fp=oscstrP2(k, cG, 14, dens, prec, cutoff*R), sp=statstrucP(k, cG, 14, dens);
		out << k << "    " << sp << "    " << fp << "    " << fp/sp << endl;
	}*/
	
	out.close();
}

void expansion_binless(vars V, CRandomSFMT& rangen) {

	if(V.ull["os"]<1) cerr << "WARNING: Bootstrap only works with explicit # of configurations" << endl;
	int N=V.i["N"], n=V.i["n"], twoS=V.i["twoS"], s=V.i["s"], bs=V.i["bs"], kM=V.i["kM"], nk=V.i["nk"]; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size(); clock_t time=clock();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	} e();
	
	unsigned long long configs=0, os=V.ull["os"]; if(os<1) os=ULLONG_MAX-10; int opfiles=0;
	double R=sqrt(twoS/2.0), prec=V.d["pr"]; double twoR=2.0*R;
	double shiftcorr=sqrt(twoS*V.d["nu"]/(double)N), fluxcorr=sqrt((twoS-2.0)/twoS), Rf=V.d["Rf"];
	vector<double> binless(nk, 0.0); double cutoff=2.0*R; double ta, tb, L=cutoff/(double)s, dens=N/(twopi*twoS); vector<double> areas(s);
	for(int i=0;i<s;i++) {
		ta=2.0*asin(L*i/2.0);
		tb=2.0*asin(L*(i+1.0)/2.0);
		areas[i]=pi*twoS*sin((tb-ta)/2.0)*(sin(ta)+sin(tb));
	} 
	vector<double> tp(2*N); vector<complex<double> > uv(2*N);
	
	while(configs<os && opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		while(in.read((char *) &tp[0], sizeof(double)) && configs<os) { configs++;
			in.read((char *) &tp[1], sizeof(double)); for(int i=2;i<2*N;i+=2) {in.read((char *) &tp[i], sizeof(double)); in.read((char *) &tp[i+1], sizeof(double));}
			tp2uv(N, tp, uv); binless_chord(N, twoR, uv, binless, nk, cutoff);
		}
	}
	for(int i=0;i<nk;i++) {
		binless[i]=binless[i]*2.0/(fact(2*i+1)*pow(2.0, 4*i+2)*N*configs)+8.0*pi*(1.0/pow(9.0, i+1)-alglib::incompletegamma(2*i+2,R*R));
		cout << binless[i]*2.0/(fact(2*i+1)*pow(2.0, 4*i+2)*N*configs) << "   " << 8.0*pi*(1.0/pow(9.0, i+1)-alglib::incompletegamma(2*i+2,R*R)) << endl;
	}
	double secs=timeint(time); cout << endl << "MC time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl << endl;
	
	alglib::real_2d_array Gamma=getGamma(N, nk, binless); alglib::ae_int_t info; alglib::matinvreport mrep;
	alglib::real_2d_array Gammainv=Gamma; alglib::rmatrixtrinverse(Gammainv, true, info, mrep);
	
	ee(); cout << setprecision(5); for(int i=0;i<nk;i++) {
		for(int j=0;j<nk;j++) cout << Gamma[i][j] << "  ";
		e();
	}
	
	ee(); cout << setprecision(5); for(int i=0;i<nk;i++) {
		for(int j=0;j<nk;j++) {
			double tmp=0; for(int k=0;k<nk;k++) tmp+=Gamma[i][k]*Gammainv[k][j];
			cout << tmp << "  ";
		}
		e();
	} ee();
	
	vector<double> c(nk, 0.0);
	for(int i=0;i<nk;i++) {
		for(int j=0;j<i;j++) c[i]+=binless[j]*Gammainv[j][i];
		for(int j=i;j<nk;j++) c[i]+=binless[j]*Gammainv[i][j];
	}
	for(int i=0;i<nk;i++) cout << setprecision(8) << c[i] << endl;
	
	for(int i=0;i<s;i++) {
		double r=L*(i+0.5), tmp=0;
		for(int j=0;j<nk;j++) tmp+=2.0*c[j]*pow(r/2.0, 4.0*j+2.0)*exp(-r*r/4.0)/fact(2*j+1);
		out << r << "    " << 1.0-exp(-r*r/2.0)+tmp << endl;
	}
	
	/*double RL=R*L; alglib::real_2d_array fm=getfitmatrix(RL, nk, s); alglib::real_1d_array gMC; gMC.setlength(s);
	for(int i=0;i<s;i++) gMC[i]=bins[i]-1.0+exp(-RL*(i+0.5)*RL*(i+0.5)/2.0); alglib::real_1d_array cal; cal.setlength(nk);
	alglib::lsfitreport rep; alglib::lsfitlinear(gMC, fm, info, cal, rep);
	vector<double> c=al2d(nk, cal); for(int i=0;i<nk;i++) cout << c[i] << endl;
	
	int Nk=100; for(int i=1;i<Nk;i++) {
		//double k=3.0*i/Nk, fp=oscstrP2(k, c, nk, dens, prec, Rf*R), sp=statstrucP(k, c, nk, dens);
		double k=3.0*i/Nk, fp=oscstrP2(k, c, nk, 1.0/(twopi*3.0), prec, Rf*R), sp=statstrucP(k, c, nk, 1.0/(twopi*3.0));
		out << k << "    " << statstruc(k, c, nk, dens) << "    " << sp << "    "
			<< fp << "    " << fp/sp << endl;
	}*/
	
	out.close();
}

alglib::real_2d_array getGamma(int N, int nk, vector<double> binless) {
	alglib::real_2d_array Gamma; Gamma.setlength(nk, nk);
	for(int i=0;i<nk;i++) {
		for(int j=0;j<i;j++) Gamma[i][j]=0.0;
		for(int j=i;j<nk;j++) Gamma[i][j]=twopi*bin(2*(i+j+1),2*i+1)/pow(2.0, 2*(i+j));
	}
	return Gamma;
}

void expansion_constrained(vars V, CRandomSFMT& rangen) {

	if(V.ull["os"]<1) cerr << "WARNING: Bootstrap only works with explicit # of configurations" << endl;
	int N=V.i["N"], n=V.i["n"], twoS=V.i["twoS"], s=V.i["s"], bs=V.i["bs"], kM=V.i["kM"], nk=V.i["nk"], p=V.i["p"]; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size(); clock_t time=clock();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	} e();
	
	unsigned long long configs=0, os=V.ull["os"]; if(os<1) os=ULLONG_MAX-10; int opfiles=0;
	double R=sqrt(twoS/2.0), prec=V.d["pr"];
	double shiftcorr=sqrt(twoS*V.d["nu"]/(double)N), fluxcorr=sqrt((twoS-2.0)/twoS);
	vector<double> bins(s, 0.0); double cutoff=2.0; double ta, tb, L=cutoff/(double)s, dens=N/(twopi*twoS); vector<double> areas(s);
	for(int i=0;i<s;i++) {
		ta=2.0*asin(L*i/2.0);
		tb=2.0*asin(L*(i+1.0)/2.0);
		areas[i]=pi*twoS*sin((tb-ta)/2.0)*(sin(ta)+sin(tb));
	} 
	vector<double> tp(2*N); vector<complex<double> > uv(2*N);
	
	while(configs<os && opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		while(in.read((char *) &tp[0], sizeof(double)) && configs<os) { configs++;
			in.read((char *) &tp[1], sizeof(double)); for(int i=2;i<2*N;i+=2) {in.read((char *) &tp[i], sizeof(double)); in.read((char *) &tp[i+1], sizeof(double));}
			tp2uv(N, tp, uv); bins_chord(N, uv, L, bins, cutoff);
		}
	}
	double secs=timeint(time); cout << endl << "MC time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl << endl;
	
	for(int i=0;i<s;i++) bins[i]*=2.0/((double)N*dens*configs*areas[i]);
	
	double RL=R*L; alglib::real_2d_array fm=getfitmatrix(RL, nk, s); alglib::real_1d_array gMC; gMC.setlength(s);
	for(int i=0;i<s;i++) gMC[i]=bins[i]-1.0+exp(-RL*(i+0.5)*RL*(i+0.5)/2.0); alglib::real_1d_array cal; cal.setlength(nk);
	alglib::ae_int_t info; alglib::lsfitreport rep; alglib::real_2d_array Cm; Cm.setlength(3,nk+1);
	for(int i=0;i<nk;i++) Cm[0][i]=1.0; Cm[0][nk]=(1.0-p)/4.0;
	for(int i=0;i<nk;i++) Cm[1][i]=2.0*(i+1.0); Cm[1][nk]=(1.0-p)/8.0;
	for(int i=0;i<nk;i++) Cm[2][i]=2.0*(i+1.0)*(2.0*i+3.0); Cm[2][nk]=(1.0-p)*(1.0-p)/8.0;
	alglib::lsfitlinearc(gMC, fm, Cm, info, cal, rep);
	
	vector<double> c=al2d(nk, cal); for(int i=0;i<nk;i++) cout << c[i] << endl;
	
	out << "Pair correlation function: " << endl << endl << "\tr / l\t\t\tg ( r )" << endl;
	for(int i=0;i<s;i++) out << RL*(i+0.5) << "\t\t\t" << bins[i] << endl;
	out << endl << endl; double Rf=V.d["Rf"];
	int Nk=100; for(int i=1;i<Nk;i++) {
		double k=3.0*i/Nk, fp=oscstrP2(k, c, nk, dens, prec, Rf*R), sp=statstrucP(k, c, nk, dens);
		out << k << "    " << statstruc(k, c, nk, dens) << "    " << sp << "    "
			<< fp << "    " << fp/sp << endl;
	}
	
	/*vector<double> cG(14); cG[0]=-1; cG[1]=0.51053; cG[2]=-0.02056; cG[3]=0.31003; cG[4]=-0.4905; cG[5]=0.20102; cG[6]=-0.00904; cG[7]=-0.00148; cG[8]=0;
	cG[9]=0.0012; cG[10]=0.0006; cG[11]=-0.0018; cG[12]=0; cG[13]=0; out << endl << endl;
	for(int i=1;i<100;i++) {
		double k=2.5*i/100.0, fp=oscstrP2(k, cG, 14, 1.0/(twopi*3.0), prec, sqrt(3.0*199/2.0)), sp=statstrucP(k, cG, 14, 1.0/(twopi*3.0));
		out << k << "    " << sp << "    " << fp << "    " << fp/sp << endl;
	}*/
	
	out.close();
}

double oscstrP2(double k, vector<double> c, int nk, double dens, double prec, double rcutoff) {

	double *qmin=new double[2], *qmax=new double[2], *val=new double[1], *err=new double[1];
	qmin[0]=0; qmin[1]=0; qmax[0]=rcutoff; qmax[1]=twopi; 
	double *params=new double[nk+3]; params[0]=k; params[1]=nk; params[2]=dens; for(int i=0;i<nk;i++) params[i+3]=c[i];
	if(cubature(1, fint2, params, 2, qmin, qmax, 0, 1e-15, prec, ERROR_INDIVIDUAL, val, err)!=0) {cerr << "ERROR: Cubature fail" << endl; return 0.0;}
	double result=val[0]; delete [] qmin; delete [] qmax; delete [] val; delete [] err; return result;
}

double oscstrP2_2LL(double k, vector<double> c, int nk, double dens, double prec, double rcutoff) {

	double *qmin=new double[2], *qmax=new double[2], *val=new double[1], *err=new double[1]; 
	qmin[0]=0; qmin[1]=0; qmax[0]=rcutoff; qmax[1]=twopi;
	double *params=new double[nk+3+7]; params[0]=k; params[1]=nk; params[2]=dens; for(int i=0;i<nk;i++) params[i+3]=c[i];
	if(cubature(1, fint2_2LL, params, 2, qmin, qmax, 0, 1e-15, prec, ERROR_INDIVIDUAL, val, err)!=0) {cerr << "ERROR: Cubature fail" << endl; return 0.0;}
	double result=val[0]; delete [] qmin; delete [] qmax; delete [] val; delete [] err; return result;
}

// v(q)=2pi/q  2pi cancels against one factor of (2pi)^-2 from q-integral while 1/q cancels against measure
int fint2(unsigned dim, const double* q, void* pin, unsigned fdim, double* retval) {

	double *p = (double *)pin; double k2=p[0]*p[0]; vector<complex<double> > q_k=qck_kcq(p[0], q[0], q[1]);
	vector<double> c(p[1]); for(int i=0;i<p[1];i++) c[i]=p[i+3];
	complex<double> f=(exp(q_k[0])-exp(q_k[1]))*(statstrucP(q[0], c, p[1], p[2])*exp(-k2/2.0)*(exp(-q_k[1])-exp(-q_k[0]))+
					   statstrucP(sqrt(k2+q[0]*q[0]+2*p[0]*q[0]*cos(q[1])), c, p[1], p[2])*(exp(q_k[1])-exp(q_k[0])))/(4.0*pi);
	retval[0]=real(f); return 0;
}

int fint2_2LL(unsigned dim, const double* q, void* pin, unsigned fdim, double* retval) {

	double *p = (double *)pin; double k2=p[0]*p[0]; vector<complex<double> > q_k=qck_kcq(p[0], q[0], q[1]); double q2=q[0]*q[0];
	vector<double> c(p[1]); for(int i=0;i<p[1];i++) c[i]=p[i+3]; vector<double> cf(7);
	cf[0]=-50.36597363; cf[1]=87.38179510; cf[2]=-56.08455086; cf[3]=17.76579124; cf[4]=-2.971636200; cf[5]=0.2513169758; cf[6]=-0.008434843187;
	double sum=0, x=1.0/sqrt(1.0+q2); double x2=x*x;
	for(int i=0;i<7;i++) sum+=cf[i]*fact(i+1)*alglib::legendrecalculate(i+1, x)/pow(x2, 1.0+0.5*i);
	complex<double> f=(1.0+q[0]*sum)*(exp(q_k[0])-exp(q_k[1]))*(statstrucP(q[0], c, p[1], p[2])*exp(-k2/2.0)*(exp(-q_k[1])-exp(-q_k[0]))+
					   statstrucP(sqrt(k2+q2+2*p[0]*q[0]*cos(q[1])), c, p[1], p[2])*(exp(q_k[1])-exp(q_k[0])))/(4.0*pi);
	retval[0]=real(f); return 0;
}

double oscstrP(double k, vector<double> c, int nk, double dens, double prec) {

	double rcutoff=100; double *qmin=new double[2], *qmax=new double[2], *val=new double[2], *err=new double[2];
	qmin[0]=0; qmin[1]=0; qmax[0]=rcutoff; qmax[1]=twopi;
	double *params=new double[nk+3]; params[0]=k; params[1]=nk; params[2]=dens; for(int i=0;i<nk;i++) params[i+3]=c[i];
	if(cubature(2, fint, params, 2, qmin, qmax, 0, 1e-15, prec, ERROR_INDIVIDUAL, val, err)!=0) {cerr << "ERROR: Cubature fail" << endl; return 0.0;}
	cout << val[0] << " +- " << err[0] << "   " << val[1] << " +- " << err[1] << endl;
	double result=val[0]; delete [] qmin; delete [] qmax; delete [] val; delete [] err; return result;
}

// v(q)=2pi/q  2pi cancels against one factor of (2pi)^-2 from q-integral while 1/q cancels against measure
int fint(unsigned dim, const double* q, void* pin, unsigned fdim, double* retval) {

	double *p = (double *)pin; double k2=p[0]*p[0]; vector<complex<double> > q_k=qck_kcq(p[0], q[0], q[1]);
	vector<double> c(p[1]); for(int i=0;i<p[1];i++) c[i]=p[i+3];
	complex<double> f=(exp(q_k[0])-exp(q_k[1]))*(statstrucP(q[0], c, p[1], p[2])*exp(-k2)*(exp(-q_k[1])-exp(-q_k[0]))+
					   statstrucP(sqrt(k2+q[0]*q[0]+2*p[0]*q[0]*cos(q[1])), c, p[1], p[2])*(exp(q_k[1])-exp(q_k[0])))/(4.0*pi);
	retval[0]=real(f); retval[1]=imag(f); return 0;
}

double statstruc(double k, vector<double> c, int nk, double dens) {

	double sum=0, k2=k*k;
	for(int i=0;i<nk;i++) sum+=c[i]*alglib::laguerrecalculate(2*i+1, k2)*exp(-k2);
	return 1.0-twopi*dens*exp(-k2/2.0)+4.0*twopi*dens*sum;
}	

double statstrucP(double k, vector<double> c, int nk, double dens) {

	double sum=0, k2=k*k;
	for(int i=0;i<nk;i++) sum+=c[i]*alglib::laguerrecalculate(2*i+1, k2)*exp(-k2);
	return (1.0-twopi*dens)*exp(-k2/2.0)+4.0*twopi*dens*sum;
}	

alglib::real_2d_array getfitmatrix(double RL, int nk, int s) {

	alglib::real_2d_array fm; fm.setlength(s, nk);
	for(int i=0;i<s;i++) {
		double r=RL*(i+0.5);
		for(int j=0;j<nk;j++) fm[i][j]=2.0*pow(r/2.0, 4*j+2)*exp(-r*r/4.0)/fact(2*j+1);
	}
	return fm;
}

alglib::real_2d_array getfitmatrixz(double L, int nk, int s, int fourS) {

	alglib::real_2d_array fm; fm.setlength(s, nk);
	for(int i=0;i<s;i++) {
		double z=L*(i+0.5);
		for(int j=0;j<nk;j++) fm[i][j]=bin(fourS, 2*j+1)*pow(z, 4*j+2)/pow(1.0+z*z, fourS);
	}
	return fm;
}

void count_cfs(vars V, CRandomSFMT& rangen) {

	int opfiles=0, configs=0, N=V.i["N"]; double tmp; string path=V.s["fn"];
	vector<string> files=dirfiles(path); int nfiles=files.size();
	if(nfiles<1){cerr << "ERROR: No files found in " << path << endl; return;}
	ofstream out(V.s["fn2"].c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	stringstream ss; ss << N; cout << "Reading the following files in " << path << endl;
	for(int i=0;i<nfiles;i++) {
		cout << files[i] << endl;
		for(int j=0;j<files[i].length()-ss.str().length();j++) {
			if(files[i][j]=='N') {
				for(int k=0;k<ss.str().length();k++) {
					if(files[i][j+k+1]!=ss.str()[k]) { cerr << "WARNING: File seems to have data for a different particle number than N = " << N << endl; break;}
				}
			}
		}
		files[i]=path+files[i];
	}
	
	while(opfiles<nfiles) {
		ifstream in(files[opfiles++].c_str(), ios::binary); if(!in) cerr << "File Error" << endl;
		in.seekg(0, ios::end); streampos cs=in.tellg(); configs+=cs/(sizeof(double)*2*N);
	}

	cout << endl << n2s(configs) << " configurations in " << path << endl;

}

vector<vector<double> > getSVFc(int twoS, int kM) {
	vector<vector<double> > c(twoS+1, vector<double>(2)); stringstream ss1; ss1 << "./SVFc/SVF_2S_" << twoS << "_kM_" << kM << "_sv.dat"; 
	ifstream in1(ss1.str().c_str());
	stringstream ss2; ss2 << "./SVFc/SVF_2S_" << twoS << "_ls.dat"; ifstream in2(ss2.str().c_str()); 
	if(!in1) cerr << "WARNING: Spherical effective potential coefficient file " << ss1.str() << " not found" << endl; 
	else if(!in2) cerr << "WARNING: Spherical effective potential coefficient file " << ss2.str() << " not found" << endl; 
	else {
		in1 >> setprecision(15); in2 >> setprecision(15); 
		for(int i=0;i<=kM;i++) in1 >> c[i][0];
		for(int i=0;i<=twoS;i++) in2 >> c[i][1];
	}
	return c;
}	

void timetest(vars V, CRandomSFMT& rangen) {

	// int runtime=clock(), s=50000; alglib::complex c=1, c2;
	// for(int i=1;i<=s;i++) {c.x=rangen.Random(); c.y=rangen.Random(); c2=pow(c, i);} cout << al2cd(c2) << endl;
	// cout << "Time 1: " << timeint(runtime) << endl;
	// for(int i=1;i<=s;i++) {c.x=rangen.Random(); c.y=rangen.Random(); c2=pipow(c, i);} cout << al2cd(c2) << endl;
	// cout << "Time 2: " << timeint(runtime) << endl;

}

void setupV(vars& V) {

	V.i["N"]=12; V.i["tk"]=1; V.i["sd"]=0; V.i["bs"]=100; V.i["n"]=2; V.i["p"]=1; V.i["s"]=100; V.i["sr"]=-1; V.i["kM"]=12; V.i["nk"]=15;
	V.ull["os"]=100; V.d["pr"]=1e-3; V.d["Rf"]=1.0; V.d["co"]=2.0;
	V.s["fn"]="inputfolder"; V.s["fn2"]="output.dat"; V.s["wf"]="BSWF";
}

void showHelp() { cout << endl << "Help not configured" << endl << endl;
	// cout << endl << "Command line arguments (d=default):" << endl << endl;
	// cout << "\t-tk\t Task to perform (d=1)" << endl << "\t\t\t1: Simultaneous MC of alt. Laughlin with alternative bootstrap, save data (d)" << endl
		 // << "\t\t\t2: Simultaneous MC of alt. Laughlin with alternative bootstrap, display data" << endl
		 // << "\t\t\t3: Simultaneous MC with alternative bootstrap, save data" << endl << "\t\t\t4: Simultaneous MC with no error calculation, save data" 
		 // << endl << "\t\t\t5: Simultaneous MC with alternative bootstrap, display data"
		 // << endl << "\t\t\t6: Simultaneous MC with no error calculation, display data" << endl << "\t\t\t7: Calculate standard Laughlin energy" << endl
		 // << "\t\t\t8: Combine data from runs of task 1" << endl << "\t\t\t9: Concatenate data from folder into single file" << endl;
	// cout << "\t-N\t Number of particles (d=2)" << endl;
	// cout << "\t-os\t Number of samples (d=10^4)" << endl;
	// cout << "\t-oh\t Harvest period of simultaneous MC in factors of N (d=1)" << endl;
	// cout << "\t-it\t Thermalization of simultaneous MC in factors of N (d=10k)" << endl;
	// cout << "\t-ot\t Thermalization of electrons in factors of N (d=10k)" << endl;
	// cout << "\t-ist\t Ghost steplength in factors of pi (d -> 50% accepted)" << endl;
	// cout << "\t-st\t Electon steplength in factors of pi (d -> 50% accepted)" << endl;
	// cout << "\t-bs\t Number of bootstrap resamplings (d=100)" << endl;
	// cout << "\t-fn\t Output filename (d=\"output.dat\")" << endl;
	// cout << "\t-ifn\t Input filename (or input folder for task 7) (d=\"output.dat\")" << endl;
	// cout << "\t-sd\t Random seed; -1 -> from cpu clock (d=-1)" << endl;
	// cout << endl << "Filename template: ./MCsicoData/[acc/]MCsico_N..is..os..[oh..]_[bs../noE].dat" << endl;
	return;
}