#include "../../general/src/MCFSutility2.h"

void setupV(vars&);
complex<double> orint(int, int, vector<complex<double> >, vector<complex<double> >);
complex<double> orint(int, int, vector<complex<double> >, vector<complex<double> >, int);
vector<complex<double> > createsums(int, int, vector<complex<double> >, vector<complex<double> >, complex<double>&);
void MCiteration_nu1(int, int, double, vector<complex<double> >, vector<complex<double> >&, vector<double>&, vector<vector<complex<double> > >&, 
					double[], complex<double>&, vector<complex<double> >&, int&, double);
void MCiteration_nu1(int, int, double, vector<complex<double> >, vector<complex<double> >&, vector<double>&, vector<vector<complex<double> > >&, 
					double[], complex<double>&, vector<complex<double> >&, int&, double, int);
void innerMC_nu1_var(vars, CRandomSFMT&);
void innerMC_nu1_var_ind(vars, CRandomSFMT&);
void comp_L_E(vars, CRandomSFMT&);
vector<vector<complex<double> > > creatematrix(int, int, vector<int>, vector<complex<double> >);
vector<vector<complex<double> > > creatematrix_nu1(int, vector<complex<double> >);
void updateinv(int, int, vector<vector<complex<double> > >&, complex<double>, vector<complex<double> >);
vector<complex<double> > createv(int, int, vector<int>, complex<double>, complex<double>, complex<double>, complex<double>);
inline vector<complex<double> > createv_nu1(int, complex<double>, complex<double>, complex<double>, complex<double>);
complex<double> normQ(int, vector<vector<complex<double> > >);
vector<vector<complex<double> > > Gamma_nu1(int, int, double, double, vector<double>);
void showHelp();
vector<double> innerintGam_nu1(int, int, double, double, vector<double>, int, int, int);
int RIintGam_nu1(unsigned, const double*, void*, unsigned, double*);
double* psisquared_e(int, int, double, vector<complex<double> >,vector<double>,  CRandomSFMT&, int&, double[], int, int, unsigned long long, double, double, int);
double psisquared(int, int, double, vector<complex<double> >,vector<double>,  CRandomSFMT&, int&, int, int, unsigned long long, double, double);
double psisquared(int, int, double, vector<complex<double> >,vector<double>,  CRandomSFMT&, int&, int, int, unsigned long long, double, double);
double psisquared(int, int, double, vector<complex<double> >,vector<double>,  CRandomSFMT&, int&, int, int, unsigned long long, double, double, int);

class getstep{

	private:
	int n;

	public:
	int *Ns; double **steps;
	
	getstep() {
		n=1; Ns=new int[n]; steps=new double*[n]; for(int i=0;i<n;i++) steps[i]=new double[2];
		Ns[0]=2;
		steps[0][0]=0.63; steps[0][1]=0.9;
	}
	
	~getstep() {
		delete [] Ns; for(int i=0;i<n;i++) delete [] steps[i]; delete [] steps;
	}

	int checkN(int no) {
		int found=-1;
		for(int i=0;i<n;i++) {if(Ns[i]==no) {found=i; break;}}
		return found;
	}

};