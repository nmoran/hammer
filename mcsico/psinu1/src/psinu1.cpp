#include "psinu1.h"

int main(int argc, const char* argv[]) {

	vars V; setupV(V); clock_t starttime=clock(), argr=argin(argc, argv, V); V.d["st"]*=pi;
	if(argr==1) return 0; else if(argr==-1) {showHelp(); return 0;}
	/*if(V.i["sr"]!=-1) {
		if(strcmp(V.s["fn"].c_str(),"output.dat")!=0 || V.i["sd"]!=0) {cerr << "ERROR: Parameters '-fn' and '-sd' will be ignored when '-sr' is in use" << endl; return 0;}
		stringstream ss; ss << "../n" << V.i["n"] << "/N" << V.i["N"] << "/configs/N" << V.i["N"] 
							<< "is" << n2s(V.ull["is"]) << "_" << V.i["sr"] << ".dat"; 
		V.s["fn"]=ss.str(); V.i["sd"]=V.i["sr"];
	}*/
	V.i["sd"]+=time(0); CRandomSFMT rangen(V.i["sd"]); if(V.i["N"]==3) V.d["st"]=0.45;
	disp(V); e(); cout << endl <<  "Program started " << timestring() << endl << "-----------------------------------------" << endl << endl;
	
	int tk=V.i["tk"];
	if(tk==1) innerMC_nu1_var(V, rangen);
	else if(tk==2) innerMC_nu1_var_ind(V, rangen); // Use two independent computations to get symmetrical error
	else if(tk==3) comp_L_E(V, rangen); // outer integral
	else {stringstream ss; ss << tk; WE(2, ss.str()); return 0;}
	
	double sec=((double)clock()-starttime)/CLOCKS_PER_SEC; cout << setprecision(4) << endl;
	cout << "-----------------------------------------" << endl << "Total Runtime: " << sec << "s = " << sec/60.0 << "min" << endl;
	cout << endl <<  "Program finished " << timestring() << endl;
	return 0;
}

inline int pick(double ps[], CRandomSFMT& rangen) {
	double r=rangen.Random();
	if(r<ps[0]) return 0;
	if(r<ps[1]) return 1;
	if(r<ps[2]) return 2;
	if(r<ps[3]) return 3;
	if(r<ps[4]) return 4;
	if(r<ps[5]) return 5;
	if(r<ps[6]) return 6;
	if(r<ps[7]) return 7;
	if(r<ps[8]) return 8;
	if(r<ps[9]) return 9;
	return 10;
}
inline void newtp(double theta, double addt, double phi, double addp, double& newt, double& newp) {
	newt=theta+addt;
	if(newt<0) {newt=-newt; phi=fmod(phi+pi,twopi); }
	else if(newt>pi) {newt=twopi-newt; phi=fmod(phi+pi,twopi); }
	newp=fmod(phi+addp, twopi);
	if(newp<0) newp+=twopi;
	
	return;
}
inline void tp2uv_p(int N, vector<double> tp, vector<complex<double> >& uv) {

	for(int i=0;i<2*N;i+=2) {
		double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
		uv[i]=cos(tp[i]/2.0)*complex<double>(cp, sp);
		uv[i+1]=sin(tp[i]/2.0)*complex<double>(cp, -sp);
	}
	
	return;
}
inline void tp2uv_p(double t, double p, complex<double>& u, complex<double>& v) {

	double cp=cos(p/2.0), sp=sin(p/2.0);
	u=cos(t/2.0)*complex<double>(cp, sp);
	v=sin(t/2.0)*complex<double>(cp, -sp);

	return;
}

void setupV(vars& V) {

	V.i["N"]=2; V.i["it"]=100; V.i["ih"]=1; V.i["tk"]=1; V.i["sd"]=0; V.i["sr"]=-1; V.i["bs"]=100; V.i["rs"]=1; V.i["m"]=-1; 
	V.d["sf"]=1; V.d["st"]=0.65; V.d["pr"]=1e-3;
	V.ull["is"]=100; V.ull["os"]=10;
	V.s["fn"]="./Data/output.dat"; V.s["pl"]="Random";
}

void innerMC_nu1_var(vars V, CRandomSFMT& rangen) {

	int accept=0, N=V.i["N"], twoS=V.i["twoS"], bs=V.i["bs"], ih=V.i["ih"], rs=V.i["rs"]; double Sfac=V.d["sf"], step=V.d["st"];
	unsigned long long is=V.ull["is"]; string pl=V.s["pl"], fn=V.s["fn"];
	
	vector<double> tp(2*N); vector<complex<double> > uv(2*N); bool newcoords=true;
	if(strcmp(pl.c_str(), "Random")!=0) {
		newcoords=false; ifstream tpin(pl.c_str()); if(!tpin) {cerr << "ERROR: File " << pl << " not found" << endl; return;}
		tpin >> setprecision(15); for(int i=0;i<2*N;i+=2) tpin >> tp[i] >> tp[i+1];
		tp2uv_p(N, tp, uv); tpin.close();
	}
	ofstream out(fn.c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	vector<double> Ls(rs), MCs(rs);
	for(int k=0;k<rs;k++) {cout << "Run " << k+1 << "    "; cout.flush();
		if(newcoords) elcoords(N, tp, uv, rangen);
		double laugh=norm(Laugh(N, uv));

		vector<complex<double> > pq=uv; vector<double> ab=tp;
		vector<vector<complex<double> > > mat=creatematrix_nu1(N, pq); //cout << "mat:" << endl; printmat(mat, N);
			
		// initialize correlation function sums and their product
		complex<double> prod; vector<complex<double> > sums=createsums(N, twoS*Sfac, pq, uv, prod);
		
		// find inverse and determinant of Slater matrix
		complex<double> det; vector<vector<complex<double> > > invmat=cinvdet(N, mat, det); //cout << "inv:" << endl; printmat(invmat, N);
		
		// compute original integrand
		complex<double> G=orint(N, twoS, pq, uv);
		G/=norm(det*prod); //cout << "G " << G << endl;
		
		// thermalize
		double rans[4];
		for(int i=0;i<V.i["it"];i++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, accept, Sfac); //cout << "inv a:" << endl; printmat(invmat, N); cin >> accept;
		} 
		
		// cout << endl << "ab:" << endl; for(int i=0;i<2*N;i+=2) cout << ab[i] << "   " << ab[i+1] << endl;
		// cout << "G update: " << G << endl;
		// complex<double> G2=orint(N, twoS, pq, uv);
		// complex<double> prod2; vector<complex<double> > sums2=createsums(N, twoS*Sfac, pq, uv, prod2);
		// vector<vector<complex<double> > > mat2=creatematrix_nu1(N, pq);
		// complex<double> det2; vector<vector<complex<double> > > invmat2=cinvdet(N, mat2, det2);
		// G2/=norm(det2*prod2); cout << "G calc: " << G2 << endl << endl;
		// cout << "update:" << endl; for(int i=0;i<N;i++) {for(int j=0;j<N;j++) cout << invmat[i][j] << "  "; cout << endl;}
		// cout << "calc:" << endl; for(int i=0;i<N;i++) {for(int j=0;j<N;j++) cout << invmat2[i][j] << "  "; cout << endl;}
		
		vector<complex<double> > Gvals(bs); vector<double> totn(bs); double s; //Bootstrap setup
		double probs[10], prob=1.0/is, probinv=1.0-1.0/is; probs[0]=pow(probinv, is);
		for(int i=1;i<10;i++) probs[i]=probs[i-1]+bin(is, i)*pow(probinv, is-i)*pow(prob,i);
		
		// harvest
		complex<double> Gsum=0; accept=0;
		for(unsigned long long i=0;i<is;i++) {
			Gsum+=G;
			for(int j=0;j<bs;j++) {s=pick(probs, rangen); Gvals[j]+=s*G; totn[j]+=s;}
			for(int j=0;j<ih;j++) {
				rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
				MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, accept, Sfac);
			}
		}
		Gsum/=(double)is;
		
		double expG, g=0, g2=0; for(int i=0;i<bs;i++) {expG=norm(Gvals[i]/(double)totn[i]); g+=expG; g2+=expG*expG;} // Bootstrap
		double error=sqrt(g2/(double)bs-g*g/((double)bs*bs));
		
		// Normalize
		vector<vector<complex<double> > > gamma=Gamma_nu1(N, twoS, V.d["pr"], V.d["sf"], tp);
		double nQ=real(normQ(N, gamma));
		
		//cout << endl << "norm" << endl; for(int i=0;i<gamma.size();i++) {for(int j=0;j<gamma[i].size();j++) cout << gamma[i][j] << "   "; cout << endl;}
		//cout << normQ(N, gamma) << endl;
		
		// return results and error
		cout << "Inner MC: " << 100.0*accept/((double)is*ih) << "% accepted" << endl;
		
		out << norm(nQ*Gsum) << "    " << nQ*nQ*error << "   " << laugh << "   " << error/norm(Gsum) << endl;
		cout << norm(nQ*Gsum) << "    " << nQ*nQ*error << "   " << laugh << "   " << error/norm(Gsum) << endl;
		Ls[k] = laugh; MCs[k] = norm(nQ*Gsum); 
	}
	
	vector<double> fit = lsfit(Ls, MCs);
	vector<double> diff=rmsdiff(fit, Ls, MCs); e(); clog << "RMS deviation from ls fit: " << diff[0] << ", relative: " << diff[1] << endl;
	out << diff[0] << "  " << diff[1] << " 0 0" << endl;
	
	out.close(); return;
	
}

void innerMC_nu1_var_ind(vars V, CRandomSFMT& rangen) {

	int accept=0, accept2=0, N=V.i["N"], twoS=V.i["twoS"], bs=V.i["bs"], ih=V.i["ih"], rs=V.i["rs"]; double Sfac=V.d["sf"], step=V.d["st"];
	unsigned long long is=V.ull["is"]; string pl=V.s["pl"], fn=V.s["fn"];
	
	vector<double> tp(2*N); vector<complex<double> > uv(2*N); bool newcoords=true;
	if(strcmp(pl.c_str(), "Random")!=0) {
		newcoords=false; ifstream tpin(pl.c_str()); if(!tpin) {cerr << "ERROR: File " << pl << " not found" << endl; return;}
		tpin >> setprecision(15); for(int i=0;i<2*N;i+=2) tpin >> tp[i] >> tp[i+1];
		tp2uv_p(N, tp, uv); tpin.close();
	}
	ofstream out(fn.c_str()); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	vector<double> Ls(rs), MCs(rs);
	for(int k=0;k<rs;k++) {cout << "Run " << k+1 << "    "; cout.flush();
		if(newcoords) elcoords(N, tp, uv, rangen);
		double laugh=norm(Laugh(N, uv));

		vector<complex<double> > pq=uv, pq2=uv; vector<double> ab=tp, ab2=tp;
		vector<vector<complex<double> > > mat=creatematrix_nu1(N, pq);
			
		// initialize correlation function sums and their product
		complex<double> prod; vector<complex<double> > sums=createsums(N, twoS*Sfac, pq, uv, prod); vector<complex<double> > sums2=sums;
		
		// find inverse and determinant of Slater matrix
		complex<double> det; vector<vector<complex<double> > > invmat=cinvdet(N, mat, det); vector<vector<complex<double> > > invmat2=invmat;
		
		// compute original integrand
		complex<double> G=orint(N, twoS, pq, uv), G2;
		G/=norm(det*prod); G2=G;
		
		// thermalize
		double rans[4];
		for(int i=0;i<V.i["it"];i++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, accept, Sfac);
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, accept2, Sfac);
		} 
		
		vector<complex<double> > Gvals(bs), Gvals2(bs); vector<double> totn(bs); double s; //Bootstrap setup
		double probs[10], prob=1.0/is, probinv=1.0-1.0/is; probs[0]=pow(probinv, is);
		for(int i=1;i<10;i++) probs[i]=probs[i-1]+bin(is, i)*pow(probinv, is-i)*pow(prob,i);
		
		// harvest
		complex<double> Gsum=0, Gsum2=0; accept=0; accept2=0;
		for(unsigned long long i=0;i<is;i++) {
			Gsum+=G; Gsum2+=G2;
			for(int j=0;j<bs;j++) {s=pick(probs, rangen); Gvals[j]+=s*G; Gvals2[j]+=s*G2; totn[j]+=s;}
			for(int j=0;j<ih;j++) {
				rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
				MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, accept, Sfac);
				rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
				MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, accept2, Sfac);
			}
		}
		Gsum/=(double)is; Gsum2/=(double)is;
		
		double GR, GI, gR=0, gR2=0, gI=0, gI2=0, gR_2=0, gR2_2=0, gI_2=0, gI2_2=0; 
		for(int i=0;i<bs;i++) { // Bootstrap
			G=Gvals[i]/(double)totn[i]; GR=real(G); GI=imag(G); gR+=GR; gR2+=GR*GR; gI+=GI; gI2+=GI*GI;
			G=Gvals2[i]/(double)totn[i]; GR=real(G); GI=imag(G); gR_2+=GR; gR2_2+=GR*GR; gI_2+=GI; gI2_2+=GI*GI;
		}
		double dR2=gR2/(double)bs-gR*gR/((double)bs*bs), dI2=gI2/(double)bs-gI*gI/((double)bs*bs);
		double dR2_2=gR2_2/(double)bs-gR_2*gR_2/((double)bs*bs), dI2_2=gI2_2/(double)bs-gI_2*gI_2/((double)bs*bs);
		double errR=sqrt(real(Gsum2)*real(Gsum2)*dR2+real(Gsum)*real(Gsum)*dR2_2+imag(Gsum2)*imag(Gsum2)*dI2+imag(Gsum)*imag(Gsum)*dI2_2);
		double errI=sqrt(imag(Gsum2)*imag(Gsum2)*dR2+imag(Gsum)*imag(Gsum)*dR2_2+real(Gsum2)*real(Gsum2)*dI2+real(Gsum)*real(Gsum)*dI2_2);
		
		// Normalize
		vector<vector<complex<double> > > gamma=Gamma_nu1(N, twoS, V.d["pr"], V.d["sf"], tp);
		double nQ=real(normQ(N, gamma));
		
		// return results and error
		cout << "Inner MC: " << 100.0*accept/((double)is*ih) << "% accepted,  " << 100.0*accept2/((double)is*ih) << "% accepted" << endl;
		
		out << real(nQ*Gsum*nQ*conj(Gsum2)) << "    " << imag(nQ*Gsum*nQ*conj(Gsum2)) << "    " << nQ*nQ*errR << "    " << nQ*nQ*errI << "   " 
			<< laugh << "   " << errR/real(Gsum*conj(Gsum2)) << endl;
		cout << nQ*Gsum*nQ*conj(Gsum2) << "  +-  (" << nQ*nQ*errR << "," << nQ*nQ*errI << ")  " << laugh << "   " << errR/real(Gsum*conj(Gsum2)) << endl;
		Ls[k] = laugh; MCs[k]=real(nQ*Gsum*nQ*conj(Gsum2));
	}
	
	vector<double> fit = lsfit(Ls, MCs);
	vector<double> diff=rmsdiff(fit, Ls, MCs); e(); clog << "RMS deviation from ls fit: " << diff[0] << ", relative: " << diff[1] << endl;
	out << diff[0] << "  " << diff[1] << " 0 0 0 0" << endl;
	
	out.close(); return;
	
}

void comp_L_E(vars V, CRandomSFMT& rangen) {

	int accept=0, N=V.i["N"], twoS=V.i["twoS"], bs=V.i["bs"], ih=V.i["ih"], it=V.i["it"], m=V.i["m"]; 
	double Sfac=V.d["sf"], step=V.d["st"], prec=V.d["pr"];
	unsigned long long is=V.ull["is"], os=V.ull["os"]; string pl=V.s["pl"], fn=V.s["fn"];
	double BG=bg(N, twoS, 'c');
	
	vector<double> tp(2*N); vector<complex<double> > uv(2*N); bool newcoords=true;
	if(strcmp(pl.c_str(), "Random")!=0) {
		newcoords=false; ifstream tpin(pl.c_str()); if(!tpin) {cerr << "ERROR: File " << pl << " not found" << endl; return;}
		tpin >> setprecision(15); for(int i=0;i<2*N;i+=2) tpin >> tp[i] >> tp[i+1];
		tp2uv_p(N, tp, uv); tpin.close();
	}
	ofstream out(fn.c_str(), ios::app); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	vector<double> FEvals(bs), Fvals(bs); vector<double> totn(bs); double s; //Bootstrap setup
	double probs[10], prob=1.0/is, probinv=1.0-1.0/is; probs[0]=pow(probinv, is);
	for(int i=1;i<10;i++) probs[i]=probs[i-1]+bin(is, i)*pow(probinv, is-i)*pow(prob,i);
	
	double FEav=0, Fav=0; clock_t comptime=clock();
	if(m<=0) {
		for(unsigned long long k=0;k<os;k++) {cout << "it " << k+1 << "    "; cout.flush();
			if(newcoords) elcoords(N, tp, uv, rangen);
			double laugh=norm(Laugh(N, uv));
			double psisq=psisquared(N, twoS, Sfac,  uv, tp, rangen, accept, it, ih, is, step, prec);
			double E=EcalcC(N, twoS, uv);
			double F=psisq/laugh, FE=F*E; FEav+=FE; Fav+=F;
			for(int j=0;j<bs;j++) {s=pick(probs, rangen); FEvals[j]+=s*FE; Fvals[j]+=s*F;}
		}
	}
	else {cout << "m " << m << endl;
		for(unsigned long long k=0;k<os;k++) {cout << "it " << k+1 << "    "; cout.flush();
			if(newcoords) elcoords(N, tp, uv, rangen);
			double laugh=norm(Laugh(N, uv));
			double psisq=psisquared(N, twoS, Sfac,  uv, tp, rangen, accept, it, ih, is, step, prec, m);
			double E=EcalcC(N, twoS, uv);
			double F=psisq/laugh, FE=F*E; FEav+=FE; Fav+=F;
			for(int j=0;j<bs;j++) {s=pick(probs, rangen); FEvals[j]+=s*FE; Fvals[j]+=s*F;}
		}
	}
	
	double Ebs=0, Ebs2=0, Eest;
	for(int i=0;i<bs;i++) {
		Eest=FEvals[i]/Fvals[i]; Ebs+=Eest; Ebs2+=Eest*Eest;
	}
	double error=sqrt(Ebs2/(double)bs-Ebs*Ebs/((double)bs*bs));
	
	cout << endl << "E = " << FEav/(N*Fav)+BG << " +- " << error/(double)N << endl; if(m==-1) m=3;
	double sec=((double)clock()-comptime)/CLOCKS_PER_SEC;
	out << left << scientific << setw(7) << N << setw(7) << m << setw(11) << os << setw(11) << is << setw(11) << it << setw(7) << ih << setw(7) 
		<< bs << setw(13) << setprecision(2) << prec << setprecision(15) << setw(27) << FEav/(N*Fav)+BG << setprecision(6) << setw(20) 
		<< error/(double)N << sec << endl;
	
	out.close(); return;
	
}

double* psisquared_e(int N, int twoS, double Sfac, vector<complex<double> > uv,vector<double> tp,  CRandomSFMT& rangen, int& accept, double probs[],
				int it, int ih, unsigned long long is, double step, double prec, int bs) {

	vector<complex<double> > pq=uv, pq2=uv; vector<double> ab=tp, ab2=tp;
	vector<vector<complex<double> > > mat=creatematrix_nu1(N, pq);
	
	// initialize correlation function sums and their product
	complex<double> prod; vector<complex<double> > sums=createsums(N, twoS*Sfac, pq, uv, prod); vector<complex<double> > sums2=sums;
	
	// find inverse and determinant of Slater matrix
	complex<double> det; vector<vector<complex<double> > > invmat=cinvdet(N, mat, det); vector<vector<complex<double> > > invmat2=invmat;
	
	// compute original integrand
	complex<double> G=orint(N, twoS, pq, uv), G2;
	G/=norm(det*prod); G2=G;
	
	// thermalize
	double rans[4]; int dummy;
	for(int i=0;i<it;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, dummy, Sfac);
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, dummy, Sfac);
	} 
	
	vector<complex<double> > Gvals(bs), Gvals2(bs); vector<double> totn(bs); double s; //Bootstrap setup
	
	// harvest
	complex<double> Gsum=0, Gsum2=0; int testacc=0;
	for(unsigned long long i=0;i<is;i++) {
		Gsum+=G; Gsum2+=G2;
		for(int j=0;j<bs;j++) {s=pick(probs, rangen); Gvals[j]+=s*G; Gvals2[j]+=s*G2; totn[j]+=s;}
		for(int j=0;j<ih;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, testacc, Sfac);
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, testacc, Sfac);
		}
	}
	Gsum/=(double)is; Gsum2/=(double)is;
	
	double GR, GI, gR=0, gR2=0, gI=0, gI2=0, gR_2=0, gR2_2=0, gI_2=0, gI2_2=0; 
	for(int i=0;i<bs;i++) { // Bootstrap
		G=Gvals[i]/(double)totn[i]; GR=real(G); GI=imag(G); gR+=GR; gR2+=GR*GR; gI+=GI; gI2+=GI*GI;
		G=Gvals2[i]/(double)totn[i]; GR=real(G); GI=imag(G); gR_2+=GR; gR2_2+=GR*GR; gI_2+=GI; gI2_2+=GI*GI;
	}
	double dR2=gR2/(double)bs-gR*gR/((double)bs*bs), dI2=gI2/(double)bs-gI*gI/((double)bs*bs);
	double dR2_2=gR2_2/(double)bs-gR_2*gR_2/((double)bs*bs), dI2_2=gI2_2/(double)bs-gI_2*gI_2/((double)bs*bs);
	double errR=sqrt(real(Gsum2)*real(Gsum2)*dR2+real(Gsum)*real(Gsum)*dR2_2+imag(Gsum2)*imag(Gsum2)*dI2+imag(Gsum)*imag(Gsum)*dI2_2);
	
	// Normalize
	vector<vector<complex<double> > > gamma=Gamma_nu1(N, twoS, prec, Sfac, tp);
	double nQ=real(normQ(N, gamma));
	
	// return results and error
	cout << "Inner MC: " << 100.0*testacc/(2.0*(double)is*ih) << "% accepted" << endl;

	double *ret=new double[2]; ret[0]=real(nQ*Gsum*nQ*conj(Gsum2)); ret[1]=nQ*nQ*errR; return ret;
	
}


double psisquared(int N, int twoS, double Sfac, vector<complex<double> > uv,vector<double> tp,  CRandomSFMT& rangen, int& accept,
				int it, int ih, unsigned long long is, double step, double prec) {

	vector<complex<double> > pq=uv, pq2=uv; vector<double> ab=tp, ab2=tp;
	vector<vector<complex<double> > > mat=creatematrix_nu1(N, pq);
	
	// initialize correlation function sums and their product
	complex<double> prod; vector<complex<double> > sums=createsums(N, twoS*Sfac, pq, uv, prod); vector<complex<double> > sums2=sums;
	
	// find inverse and determinant of Slater matrix
	complex<double> det; vector<vector<complex<double> > > invmat=cinvdet(N, mat, det); vector<vector<complex<double> > > invmat2=invmat;
	
	// compute original integrand
	complex<double> G=orint(N, twoS, pq, uv), G2;
	G/=norm(det*prod); G2=G;
	
	// thermalize
	double rans[4]; int dummy;
	for(int i=0;i<it;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, dummy, Sfac);
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, dummy, Sfac);
	} 
	
	// harvest
	complex<double> Gsum=0, Gsum2=0; int testacc=0;
	for(unsigned long long i=0;i<is;i++) {
		Gsum+=G; Gsum2+=G2;
		for(int j=0;j<ih;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, testacc, Sfac);
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, testacc, Sfac);
		}
	}
	Gsum/=(double)is; Gsum2/=(double)is;
	
	// Normalize
	vector<vector<complex<double> > > gamma=Gamma_nu1(N, twoS, prec, Sfac, tp);
	double nQ=real(normQ(N, gamma));
	
	cout << "Inner MC: " << 100.0*testacc/(2.0*(double)is*ih) << "% accepted" << endl;

	return real(nQ*Gsum*nQ*conj(Gsum2));
	
}

double psisquared(int N, int twoS, double Sfac, vector<complex<double> > uv,vector<double> tp,  CRandomSFMT& rangen, int& accept,
				int it, int ih, unsigned long long is, double step, double prec, int m) {

	vector<complex<double> > pq=uv, pq2=uv; vector<double> ab=tp, ab2=tp;
	vector<vector<complex<double> > > mat=creatematrix_nu1(N, pq);
	
	// initialize correlation function sums and their product
	complex<double> prod; vector<complex<double> > sums=createsums(N, twoS*Sfac, pq, uv, prod); vector<complex<double> > sums2=sums;
	
	// find inverse and determinant of Slater matrix
	complex<double> det; vector<vector<complex<double> > > invmat=cinvdet(N, mat, det); vector<vector<complex<double> > > invmat2=invmat;
	
	// compute original integrand
	complex<double> G=orint(N, twoS, pq, uv, m), G2;
	G/=norm(det*prod); G2=G;
	
	// thermalize
	double rans[4]; int dummy;
	for(int i=0;i<it;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, dummy, Sfac, m);
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, dummy, Sfac, m);
	} 
	
	// harvest
	complex<double> Gsum=0, Gsum2=0; int testacc=0;
	for(unsigned long long i=0;i<is;i++) {
		Gsum+=G; Gsum2+=G2;
		for(int j=0;j<ih;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq, ab, invmat, rans, G, sums, testacc, Sfac, m);
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MCiteration_nu1(N, twoS, step, uv, pq2, ab2, invmat2, rans, G2, sums2, testacc, Sfac, m);
		}
	}
	Gsum/=(double)is; Gsum2/=(double)is;
	
	// Normalize
	vector<vector<complex<double> > > gamma=Gamma_nu1(N, twoS, prec, Sfac, tp);
	double nQ=real(normQ(N, gamma));
	
	cout << "Inner MC: " << 100.0*testacc/(2.0*(double)is*ih) << "% accepted" << endl;

	return real(nQ*Gsum*nQ*conj(Gsum2));
	
}


complex<double> orint(int N, int twoS, vector<complex<double> > pq, vector<complex<double> > uv) {
	complex<double> tmp=1, tmp2=1;
	for(int i=0;i<2*N;i+=2) {
		tmp*=pow(conj(pq[i])*uv[i]+conj(pq[i+1])*uv[i+1], twoS);
		for(int j=i+2;j<2*N;j+=2) tmp2*=pq[i]*pq[j+1]-pq[j]*pq[i+1];
	}
	return tmp*tmp2*tmp2*tmp2*pow((twoS+1.0)/(4*pi), N);
}


complex<double> orint(int N, int twoS, vector<complex<double> > pq, vector<complex<double> > uv, int m) {
	complex<double> tmp=1, tmp2=1;
	for(int i=0;i<2*N;i+=2) {
		tmp*=pow(conj(pq[i])*uv[i]+conj(pq[i+1])*uv[i+1], twoS);
		for(int j=i+2;j<2*N;j+=2) tmp2*=pq[i]*pq[j+1]-pq[j]*pq[i+1];
	}
	return tmp*pow(tmp2, m)*pow((twoS+1.0)/(4*pi), N);
}

void MCiteration_nu1(int N, int twoS, double step, vector<complex<double> > uv, vector<complex<double> >& pq, vector<double>& ab, 
					vector<vector<complex<double> > >& invmat, double rans[], complex<double>& G, vector<complex<double> >& sums,
					int& accept, double Sfac) {

	int r=2*floor(N*rans[0]);
	//int r=2;
	double newa, newb;
	complex<double> newsum=0, newp, newq;
	//cout << setprecision(15) << "ab_1:" << endl; for(int i=0;i<2*N;i+=2) cout << ab[i] << "    " << ab[i+1] << endl;
	newtp(ab[r], step*(rans[1]-0.5), ab[r+1], step*(2.0*rans[2]-1.0), newa, newb);
	//cout << "ab_2: Exchange particle " << r/2+1 << " to " << newa << "    " << newb << endl;
	//newa=ab[r]+0.01; newb=ab[r+1]-0.04;
	tp2uv_p(newa, newb, newp, newq);
	
	vector<complex<double> > v=createv_nu1(N, pq[r], pq[r+1], newp, newq); //for(int i=0;i<N;i++) cout << "v_" << i << " = " << v[i] << endl;
	for(int i=0;i<2*N;i+=2) newsum+=pow(conj(newp)*uv[i]+conj(newq)*uv[i+1], twoS*Sfac);
	complex<double> R=detrat(N, invmat, r/2, v);
	double ratio=norm(R*newsum/sums[r/2]); //cout << "Ratio: " << ratio << "   ";
	
	if(ratio*sin(newa)/sin(ab[r])>rans[3]) {
		complex<double> tmp=1;
		for(int i=0;i<r;i+=2) tmp*=(pq[i]*newq-newp*pq[i+1])/(pq[i]*pq[r+1]-pq[r]*pq[i+1]);
		for(int i=r+2;i<2*N;i+=2) tmp*=(pq[i]*newq-newp*pq[i+1])/(pq[i]*pq[r+1]-pq[r]*pq[i+1]);
		tmp*=tmp*tmp*pow((conj(newp)*uv[r]+conj(newq)*uv[r+1])/(conj(pq[r])*uv[r]+conj(pq[r+1])*uv[r+1]), twoS);
		G*=tmp/ratio; //cout << "update " << tmp/ratio << " " << tmp << " " << 1.0/ratio << endl;
		sums[r/2]=newsum;
		updateinv(N, r/2, invmat, R, v);
		ab[r]=newa; ab[r+1]=newb;
		pq[r]=newp; pq[r+1]=newq;
		accept++; //cout << "y" << endl;
	} //else cout << "n" << endl;
}

void MCiteration_nu1(int N, int twoS, double step, vector<complex<double> > uv, vector<complex<double> >& pq, vector<double>& ab, 
					vector<vector<complex<double> > >& invmat, double rans[], complex<double>& G, vector<complex<double> >& sums,
					int& accept, double Sfac, int m) {

	int r=2*floor(N*rans[0]);
	//int r=2;
	double newa, newb;
	complex<double> newsum=0, newp, newq;
	//cout << setprecision(15) << "ab_1:" << endl; for(int i=0;i<2*N;i+=2) cout << ab[i] << "    " << ab[i+1] << endl;
	newtp(ab[r], step*(rans[1]-0.5), ab[r+1], step*(2.0*rans[2]-1.0), newa, newb);
	//cout << "ab_2: Exchange particle " << r/2+1 << " to " << newa << "    " << newb << endl;
	//newa=ab[r]+0.01; newb=ab[r+1]-0.04;
	tp2uv_p(newa, newb, newp, newq);
	
	vector<complex<double> > v=createv_nu1(N, pq[r], pq[r+1], newp, newq); //for(int i=0;i<N;i++) cout << "v_" << i << " = " << v[i] << endl;
	for(int i=0;i<2*N;i+=2) newsum+=pow(conj(newp)*uv[i]+conj(newq)*uv[i+1], twoS*Sfac);
	complex<double> R=detrat(N, invmat, r/2, v);
	double ratio=norm(R*newsum/sums[r/2]); //cout << "Ratio: " << ratio << "   ";
	
	if(ratio*sin(newa)/sin(ab[r])>rans[3]) {
		complex<double> tmp=1;
		for(int i=0;i<r;i+=2) tmp*=(pq[i]*newq-newp*pq[i+1])/(pq[i]*pq[r+1]-pq[r]*pq[i+1]);
		for(int i=r+2;i<2*N;i+=2) tmp*=(pq[i]*newq-newp*pq[i+1])/(pq[i]*pq[r+1]-pq[r]*pq[i+1]);
		tmp=pow(tmp, m)*pow((conj(newp)*uv[r]+conj(newq)*uv[r+1])/(conj(pq[r])*uv[r]+conj(pq[r+1])*uv[r+1]), twoS);
		G*=tmp/ratio; //cout << "update " << tmp/ratio << " " << tmp << " " << 1.0/ratio << endl;
		sums[r/2]=newsum;
		updateinv(N, r/2, invmat, R, v);
		ab[r]=newa; ab[r+1]=newb;
		pq[r]=newp; pq[r+1]=newq;
		accept++; //cout << "y" << endl;
	} //else cout << "n" << endl;
}

vector<complex<double> > createsums(int N, int twoS, vector<complex<double> > pq, vector<complex<double> > uv, complex<double>& prod) {

	vector<complex<double> > sums(N); complex<double> tmp=1;
	for(int i=0;i<N;i++) {
		sums[i]=0;
		for(int j=0;j<2*N;j+=2) sums[i]+=pow(conj(pq[2*i])*uv[j]+conj(pq[2*i+1])*uv[j+1], twoS);
		tmp*=sums[i];
	}
	prod=tmp;
	return sums;
}

vector<vector<complex<double> > > creatematrix(int N, int twoS, vector<int> ks, vector<complex<double> > pq) {
	vector<vector<complex<double> > > ret(N, vector<complex<double> >(N));
	for(int i=0;i<N;i++) {
		for(int j=0;j<N;j++) ret[i][j]=pow(pq[2*i], ks[j])*pow(pq[2*i+1], twoS-ks[j]);
	}
	return ret;
}

vector<vector<complex<double> > > creatematrix_nu1(int N, vector<complex<double> > pq) {
	vector<vector<complex<double> > > ret(N, vector<complex<double> >(N));
	for(int i=0;i<N;i++) {
		for(int j=0;j<N;j++) ret[i][j]=pow(pq[2*i], j)*pow(pq[2*i+1], N-1-j);
	}
	return ret;
}

void updateinv(int N, int r, vector<vector<complex<double> > >& invmat, complex<double> Rk, vector<complex<double> > v) {
	complex<double> sum; vector<vector<complex<double> > > newinv(N, vector<complex<double> >(N));
	for(int b=0;b<N;b++) {
		sum=0;
		for(int m=0;m<N;m++) sum+=v[m]*invmat[m][b];
		for(int a=0;a<N;a++) newinv[a][b]=invmat[a][b]-invmat[a][r]*sum/Rk;
	}
	invmat=newinv;
}

vector<complex<double> > createv(int N, int twoS, vector<int> ks, complex<double> p, complex<double> q, complex<double> newp, complex<double> newq) {
	vector<complex<double> > v(N);
	for(int i=0;i<N;i++) v[i]=pow(newp, ks[i])*pow(newq, twoS-ks[i])-pow(p, ks[i])*pow(q, twoS-ks[i]);
	return v;
}

inline vector<complex<double> > createv_nu1(int N, complex<double> p, complex<double> q, complex<double> newp, complex<double> newq) {
	vector<complex<double> > v(N);
	for(int i=0;i<N;i++) v[i]=pow(newp, i)*pow(newq, N-1-i)-pow(p, i)*pow(q, N-1-i);
	return v;
}

complex<double> normQ(int n, vector<vector<complex<double> > > Gamma) {
	int ind[n]; for(int i=0;i<n;i++) ind[i]=i;
	complex<double> sum, prod=1; double c=1;
	for(int j=0;j<n;j++) prod*=Gamma[j][j];
	sum=prod;
	while(next_permutation(ind, ind+n)) {
		prod=1;
		for(int j=0;j<n;j++) prod*=Gamma[j][ind[j]];
		sum+=pow(-1, ceil(c++/2.0))*prod;
	}
	return c*sum;
}

vector<vector<complex<double> > > Gamma_nu1(int N, int twoS, double prec, double Sfac, vector<double> tp) {

	vector<vector<complex<double> > > Gam(N, vector<complex<double> >(N));int s=time(0);
	for(int i=0;i<N;i++) {
		 vector<double> tmp=innerintGam_nu1(N, twoS, prec, Sfac, tp, 0, i, i);
		Gam[i][i]=complex<double>(tmp[0], tmp[2]);
		for(int j=i+1;j<N;j++) {
			vector<double> tmp=innerintGam_nu1(N, twoS, prec, Sfac, tp, 0, i, j);
			Gam[i][j]=complex<double>(tmp[0], tmp[2]);
			Gam[j][i]=complex<double>(tmp[0], -tmp[2]);
		}
	}
	return Gam;
}

void showHelp() {
	/*cout << endl << "Command line arguments (d=default):" << endl << endl;
	cout << "\t-rs\t Number of runs (d=1)" << endl;
	cout << "\t-N\t Number of particles (d=2)" << endl;
	cout << "\t-P\t Prob. function  -  1) |Phi Psi| (d)  2) |psi_L^5|  3) Uniform" << endl;
	cout << "\t-is\t Number of samples in inner MC (d=10)" << endl;
	cout << "\t-ih\t Harvest period of inner MC in factors of N (d=1)" << endl;
	cout << "\t-it\t Thermalization of inner MC in factors of N (d=10k)" << endl;
	cout << "\t-ot\t Thermalization of outer MC in factors of N (d=10k)" << endl;
	cout << "\t-st\t Steplength in factors of pi (d=0.65)" << endl;
	cout << "\t-pr\t Precision of numerical integral (d=1e-3)" << endl;
	cout << "\t-fn\t Filename (d=\"output.dat\")" << endl;
	cout << endl << "Example: ./MCFSm -rs 5 -N 2 -is 10000 -ih 2 -it 1000 -ot 1000 -st 0.5 -pr 1e-5 -fn Results.txt" << endl << endl <<
		 "Will do 5 runs with 2 particles, 10000 samples in inner MC, 2*N=4 runs between harvesting, 1000*N=2000" << endl <<
		 "inner thermalization runs, 1000*N=2000 outer thermalization runs, use a steplength of 0.5pi, have the" << endl <<
		 "numerical integration library result to a relative accuracy greater than 1e-5 and write the results in" << endl <<
		 "a file called Results.txt." << endl << endl;*/
	return;
}

vector<double> innerintGam_nu1(int N, int twoS, double prec, double Sfac, vector<double> tp, int maxit, int j, int k) {
	
	double *abmin=new double[2], *abmax=new double[2], *val=new double[2], *err=new double[2]; vector<double> ret(4);
	abmin[0]=0; abmin[1]=0; abmax[0]=pi; abmax[1]=2.0*pi;
	double *tp2=new double[2*N+4]; tp2[0]=j; tp2[1]=k; tp2[2]=twoS*Sfac; tp2[3]=N; for(int i=0;i<2*N;i+=2) {tp2[i+4]=tp[i]; tp2[i+5]=tp[i+1];}
	if(cubature(2, RIintGam_nu1, tp2, 2, abmin, abmax, maxit, 1e-15, prec, ERROR_INDIVIDUAL, val, err)!=0) {cerr << "ERROR: Cubature fail" << endl; return vector<double>();}
	ret[0]=val[0]; ret[1]=err[0]; ret[2]=val[1]; ret[3]=err[1];
	delete [] abmin; delete [] abmax; delete [] err; delete [] val;
	return ret;
}

int RIintGam_nu1(unsigned dim, const double *ab, void *tp, unsigned fdim, double *retval) {

	double *tpin = (double *)tp; //cout << endl; for(int i=0;i<2*tpin[3]+4;i++) cout << tpin[i] << endl;
	complex<double> wf=pow(cos(ab[0]/2.0), tpin[0]+tpin[1])*pow(sin(ab[0]/2.0), 2*tpin[3]-2-tpin[0]-tpin[1])*exp(ci*(tpin[1]-tpin[0])*ab[1]), tmp=0;
	for(int i=4;i<2*tpin[3]+4;i+=2) tmp+=pow(cos(ab[0]/2.0)*cos(tpin[i]/2.0)*exp(ci*(tpin[i+1]-ab[1])/2.0)
											+sin(ab[0]/2.0)*sin(tpin[i]/2.0)*exp(ci*(ab[1]-tpin[i+1])/2.0), tpin[2]);
	wf*=norm(tmp)*sin(ab[0]);								
	retval[0]=real(wf);
	retval[1]=imag(wf);
	return 0;
}