using namespace std;
#include <cstdlib> 
#include <iostream>    
#include <iomanip>  
#include <sstream>
#include <fstream>
#include <stdio.h>    
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <vector>
#include <complex>
#include <limits.h>
#include <algorithm>
#include "sfmt.h"
#include <map>
#include "specialfunctions.h"
#include "interpolation.h"
#include "linalg.h"
#include "cubature.h"
#define cubature hcubature

const double pi=3.1415926535897932384;
const double twopi=6.2831853071795864769;
const complex<double> c1=complex<double>(1, 0);
const complex<double> ci=complex<double>(0, 1);

struct vars{
	map<string, int> i;
	map<string, unsigned long long> ull;
	map<string, double> d;
	map<string, string> s;
	map<string, bool> b;
};

vector<double> lsfit(vector<double>, vector<double>);
vector<double> rmsdiff(vector<double>, vector<double>, vector<double>);
complex<double> Laugh(int, vector<complex<double> >);
double incgam(int, double);
vector<string> dirfiles(string& path);
double zmax(int, int&, vector<complex<double> >);
int nCFprecN12(double);
int nCFprecN15(double);
int nCFprecN18(double);
int nCFprecN21(double);
int nCFprecN24(double);
double abs(alglib::complex);
double timeint(clock_t&);
vector<complex<double> > al2cd(int, alglib::complex_1d_array);
alglib::real_1d_array d2al(vector<double>);
complex<double> al2cd(alglib::complex);
vector<double> al2d(int, alglib::real_1d_array);
double norm(alglib::complex);
double facdivfac(int, int);
double bg(int, int, char);
double EcalcA(int, int, vector<complex<double> >);
double EcalcC(int, int, vector<complex<double> >);
double EcalcC(int, int, alglib::complex_1d_array);
double EcalcAwoR(int, vector<complex<double> >);
double EcalcAwoR(int, alglib::complex_1d_array);
double EcalcCwoR(int, vector<complex<double> >);
double EcalcC_2LL(int, double, vector<complex<double> >);
double EcalcC_2LLpal(int, double, vector<complex<double> >);
vector<double> EcalcC_2LLSVF(int, double, int, vector<complex<double> >, vector<vector<double> >, int);
double fact(double);
complex<double> SlaterDet(int, vector<complex<double> >, int);
alglib::complex SlaterDet(int, alglib::complex_1d_array, int);
void ok();
void ok(string, int);
void ok(string);
void ok(int);
void ok(double);
void pal(alglib::complex_2d_array);
void pal(alglib::real_2d_array);
void palRI(alglib::complex_2d_array);
void pal(alglib::complex_1d_array);
void pal(alglib::real_1d_array);
void pal(alglib::integer_1d_array);
void palRI(alglib::complex_1d_array);
void pal(int, alglib::real_2d_array*);
void tp2uv(int, vector<double>, vector<complex<double> >&);
void tp2uvzal(int, vector<double>, vector<complex<double> >&, vector<complex<double> >&, alglib::complex_1d_array&);
void tp2uvz(int, alglib::real_1d_array, alglib::complex_1d_array&, alglib::complex_1d_array&);
void tp2uv(int, alglib::real_1d_array, alglib::complex_1d_array&);
void tp2uv(double, double, complex<double>&, complex<double>&);
void e();
void ee();
void eee();
void eeed();
void LE_arc(vars, CRandomSFMT&);
void LE_chord(vars, CRandomSFMT&);
void Lcoords(vars, CRandomSFMT&);
complex<double> detrat(int, vector<vector<complex<double> > >, int, vector<complex<double> >);
bool isnumber(string);
bool isint(string);
void disp(vars);
void WE(int, string);
int argin(int, const char*[], vars&);
bool exc(vars&, const char*);
void pc(vector<vector<complex<double> > >);
void pc(vector<complex<double> > );
void pd(vector<double>);
void pd(vector<vector<vector<double> > >);
void pd(vector<vector<double> >);
void pcRI(vector<vector<complex<double> > >);
void pcRI(vector<complex<double> >);
int max(vector<int> v);
vector<int> max2(vector<int>);
bool different(alglib::complex_2d_array, alglib::complex_2d_array, complex<double>&, complex<double>&, int&, int&, int);
bool different(alglib::complex_1d_array, vector<complex<double> >, int);
bool different(alglib::real_1d_array, vector<double>, int);
bool different(vector<complex<double> >, alglib::complex_1d_array, int);
bool different(vector<double>, alglib::real_1d_array, int);
void Laughlin(int, double, vector<double>&, vector<complex<double> >&, double[], int&);
bool different(vector<vector<vector<double> > >, vector<vector<vector<double> > >, int);
bool different(vector<vector<vector<double> > >, vector<vector<vector<double> > >, int, double&, double&, int&, int&, int&);
bool different(vector<vector<complex<double> > >, vector<vector<complex<double> > >, int, complex<double>&, complex<double>&, int&, int&);
bool different(vector<vector<double> >, vector<vector<double> >, int);
bool different(vector<vector<complex<double> > >, vector<vector<complex<double> > >, int);
bool different(complex<double>, complex<double>, int);
bool different(vector<complex<double> >, vector<complex<double> >, int);
bool different(double, double, int);
bool different(alglib::complex_1d_array, alglib::complex_1d_array, int);
bool different(vector<vector<double> >, alglib::real_2d_array, int);
void cpr(int);
int elcoords(int, vector<double>&, vector<complex<double> >&, CRandomSFMT&);
int elcoords(int, string, vector<double>&, vector<complex<double> >&, CRandomSFMT&);
int elcoords(int, alglib::real_1d_array&, alglib::complex_1d_array&, CRandomSFMT&);
int elcoords(int, string, alglib::real_1d_array&, alglib::complex_1d_array&, CRandomSFMT&);
void rantp(int, vector<double>&, CRandomSFMT&);
void rantp(int, alglib::real_1d_array&, CRandomSFMT&);
double Laughlinm3steps(int);
string nostring(int);
string timestring();
string n2s(int);
string n2s(unsigned long long);
double bin(int, int);
vector<vector<complex<double> > > cinvdet(int, vector<vector<complex<double> > >, complex<double>&);
complex<double> cdet(vector<vector<complex<double> > >);