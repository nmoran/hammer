#include "MCFSutility2.h"

inline void newtp(double theta, double addt, double phi, double addp, double& newt, double& newp) {
	newt=theta+addt;
	if(newt<0) {newt=-newt; phi=fmod(phi+pi,twopi); }
	else if(newt>pi) {newt=twopi-newt; phi=fmod(phi+pi,twopi); }
	newp=fmod(phi+addp, twopi);
	if(newp<0) newp+=twopi;
	
	return;
}
inline alglib::complex pow(alglib::complex c1, int n) { 
	complex<double> c2=complex<double>(c1.x, c1.y); c2=pow(c2, n); 
	alglib::complex c3; c3.x=real(c2); c3.y=imag(c2); return c3;
}
inline alglib::complex cd2al(complex<double> c) {return alglib::complex(real(c), imag(c));}
inline void tp2uv(double theta, double phi, alglib::complex& u, alglib::complex& v) {
	double cp=cos(phi/2.0), sp=sin(phi/2.0);
	u=cos(theta/2.0)*alglib::complex(cp, sp);
	v=sin(theta/2.0)*alglib::complex(cp, -sp);
}

void ok() {cout << "ok" << endl;}
void ok(string add) {cout << "ok " << add << endl;}
void ok(string add1, int add2) {cout << "ok " << add1 << add2 << endl;}
void ok(int add) {cout << "ok " << add << endl;}
void ok(double add) {cout << "ok " << add << endl;}

double incgam(int l, double a) {
	return alglib::incompletegamma(l, a)*fact(l-1);
}

double timeint(clock_t& time) {clock_t time2=clock(); double ti=((double)time2-time)/CLOCKS_PER_SEC; time=time2; return ti;}

void pal(alglib::complex_2d_array a) {for(int i=0;i<a.rows();i++) {for(int j=0;j<a.cols();j++) cout << al2cd(a[i][j]) << "  "; e();}}
void pal(alglib::real_2d_array a) {for(int i=0;i<a.rows();i++) {for(int j=0;j<a.cols();j++) cout << a[i][j] << "  "; e();}}
void palRI(alglib::complex_2d_array a) {
	for(int i=0;i<a.rows();i++) {
		for(int j=0;j<a.cols();j++) cout << a[i][j].x << "  "; e();
	} e();
	for(int i=0;i<a.rows();i++) {
		for(int j=0;j<a.cols();j++) cout << a[i][j].y << "  "; e();
	}
}
void pal(alglib::complex_1d_array a) {for(int i=0;i<a.length();i++) cout << al2cd(a[i]) << endl;}
void pal(alglib::real_1d_array a) {for(int i=0;i<a.length();i++) cout << a[i] << endl;}
void pal(alglib::integer_1d_array a) {for(int i=0;i<a.length();i++) cout << a[i] << endl;}
void palRI(alglib::complex_1d_array a) {
	for(int i=0;i<a.length();i++) cout << real(al2cd(a[i])) << endl; e(); 
	for(int i=0;i<a.length();i++) cout << imag(al2cd(a[i])) << endl;
}
void pal(int n, alglib::real_2d_array* a) {
	for(int k=0;k<n;k++) {
		for(int i=0;i<a[k].rows();i++) {
			for(int j=0;j<a[k].cols();j++) cout << al2cd(a[k][i][j]) << "  "; 
			e();
		}
		e(); e();
	}
}

void tp2uv(int N, vector<double> tp, vector<complex<double> >& uv) {

	for(int i=0;i<2*N;i+=2) {
		double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
		uv[i]=cos(tp[i]/2.0)*complex<double>(cp, sp);
		uv[i+1]=sin(tp[i]/2.0)*complex<double>(cp, -sp);
	}
	
	return;
}

void tp2uvzal(int N, vector<double> tp, vector<complex<double> >& uv, vector<complex<double> >& z, alglib::complex_1d_array& uval) {
	z.resize(N); uval.setlength(2*N);
	for(int i=0;i<2*N;i+=2) {
		double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
		uv[i]=cos(tp[i]/2.0)*complex<double>(cp, sp);
		uv[i+1]=sin(tp[i]/2.0)*complex<double>(cp, -sp);
		uval[i]=cd2al(uv[i]); uval[i+1]=cd2al(uv[i+1]);
		z[i/2]=uv[i+1]/uv[i];
	}
	return;
}

void tp2uvz(int N, alglib::real_1d_array tp, alglib::complex_1d_array& uv, alglib::complex_1d_array& z) {
	z.setlength(N);
	for(int i=0;i<2*N;i+=2) {
		double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
		uv[i]=cos(tp[i]/2.0)*alglib::complex(cp, sp);
		uv[i+1]=sin(tp[i]/2.0)*alglib::complex(cp, -sp);
		z[i/2]=uv[i+1]/uv[i];
	}
	return;
}

void tp2uv(int N, alglib::real_1d_array tp, alglib::complex_1d_array& uv) {

	for(int i=0;i<2*N;i+=2) {
		double cp=cos(tp[i+1]/2.0), sp=sin(tp[i+1]/2.0);
		uv[i]=cos(tp[i]/2.0); uv[i].y=uv[i].x*sp; uv[i].x*=cp; 
		uv[i+1]=sin(tp[i]/2.0); uv[i+1].y=-uv[i+1].x*sp; uv[i+1].x*=cp;
	}
	
	return;
}

void tp2uv(double t, double p, complex<double>& u, complex<double>& v) {

	double cp=cos(p/2.0), sp=sin(p/2.0);
	u=cos(t/2.0)*complex<double>(cp, sp);
	v=sin(t/2.0)*complex<double>(cp, -sp);

	return;
}

double fact(double i) {
	if ((int)i<i||i<-0.0) { cerr << "ERROR: " << i << " is not a positive integer" << endl; return -1.0; }
	if (i==0.0||i==-0.0) return 1.0;
    else if (i == 1.0) return i;
    return (i * fact(i - 1.0));
}

vector<double> lsfit(vector<double> x, vector<double> y) {
	
	int n=x.size(); if(n!=y.size()) {cerr << "ERROR: Vectors of unequal length" << endl; return vector<double>();}
	alglib::real_1d_array ax=d2al(x), ay=d2al(y);
	alglib::ae_int_t info; alglib::polynomialfitreport rep; alglib::barycentricinterpolant p;
	alglib::polynomialfit(ax, ay, 2, info, p, rep);
	alglib::real_1d_array ac; alglib::polynomialbar2pow(p, ac);
	vector<double> c(2); c[0]=ac[0]; c[1]=ac[1]; return c;
}

vector<double> rmsdiff(vector<double> c, vector<double> x, vector<double> y) {

	double sum=0, sumr=0; int n=x.size(); if(n!=y.size()) {cerr << "ERROR: Vectors of unequal length" << endl; return vector<double>();}
	for(int i=0;i<n;i++) {sum+=(y[i]-c[1]*x[i]-c[0])*(y[i]-c[1]*x[i]-c[0]); sumr+=(y[i]-c[1]*x[i]-c[0])*(y[i]-c[1]*x[i]-c[0])/((c[1]*x[i]+c[0])*(c[1]*x[i]+c[0]));}
	vector<double> ret(2); ret[0]=sqrt(sum/n); ret[1]=sqrt(sumr/n);
	return ret;
}

double bg(int N, int twoS, char t) {
	double R=sqrt(twoS/2.0);
	if(t=='a') { // arc length
		double si, dum, bg; alglib::sinecosineintegrals(pi, si, dum); 
		return -(double)N*si/(4.0*R);
	}
	else if(t=='c') { // chord length
		return -(double)N/(2.0*R);
	}
	else if(t=='p') { // chord length, Park et al's effective potential
		double a1=117.429, a2=-755.468, alpha1=1.3177, alpha2=2.9026;
		return -N/(2.0*R)-N*(a1*(1-exp(-2.0*alpha1*twoS))/alpha1+a2*(1-exp(-2.0*alpha2*twoS)*(1.0+2.0*alpha2*twoS))/(alpha2*alpha2))/(4.0*twoS);
	}
	else if(t=='s') { // chord length, Davenport & Simon's effective potential
		double c[7], E=0; c[0]=-50.36597363; c[1]=87.38179510; c[2]=-56.08455086; c[3]=17.76579124; c[4]=-2.971636200; c[5]=0.2513169758; c[6]=-0.008434843187;
		for(int l=0;l<7;l++) E+=c[l]*fact(l+1)*alglib::incompletegamma(l+2,2.0*R); 
		return -N/(2.0*R)-N*E/(2.0*twoS);
	}
	else {cerr << "ERROR: bg energy type " << t << " unknown" << endl; return 0.0;}
}

double facdivfac(int a, int b) {
	if(a<0 || b<0) {cerr << "ERROR: negative factorial not defined" << endl; return -1;}
	double ret=1;
	if(a==b) return 1;
	else if(a<b) {
		double d=b+1; while(--d>a) ret/=d;
	}
	else {
		double d=a+1; while(--d>b) ret*=d;
	}
	return ret;
}

double EcalcA(int N, int twoS, vector<complex<double> > uv) {
	double E=0;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			E+=1.0/asin(abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]));
		}
	}
	return E/(2.0*sqrt(twoS/2.0));
}

double EcalcC(int N, int twoS, vector<complex<double> > uv) {
	double E=0;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			E+=1.0/abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
		}
	}
	return E/(2.0*sqrt(twoS/2.0));
}

double EcalcC(int N, int twoS, alglib::complex_1d_array uv) {
	double E=0;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			E+=1.0/abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
		}
	}
	return E/(2.0*sqrt(twoS/2.0));
}

double EcalcAwoR(int N, vector<complex<double> > uv) {
	double E=0;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			E+=1.0/asin(abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]));
		}
	}
	return E;
}

double EcalcCwoR(int N, vector<complex<double> > uv) {
	double E=0;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			E+=1.0/abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
		}
	}
	return E;
}

double EcalcC_2LL(int N, double R, vector<complex<double> > uv) {
	double E=0, dist, c[7]; 
	c[0]=-50.36597363; c[1]=87.38179510; c[2]=-56.08455086; c[3]=17.76579124; c[4]=-2.971636200; c[5]=0.2513169758; c[6]=-0.008434843187; 
	for(int k=0;k<2*N-2;k+=2) {
		for(int j=k+2;j<2*N;j+=2) {
			dist=2.0*R*abs(uv[k]*uv[j+1]-uv[j]*uv[k+1]);
			E+=1.0/dist; for(int l=0;l<7;l++) E+=c[l]*pow(dist, l)*exp(-dist);
		}
	}
	return E;
}

vector<double> EcalcC_2LLSVF(int N, double R, int twoS, vector<complex<double> > uv, vector<vector<double> > c, int kM) {
	double E1=0, E2=0, dist; 
	for(int k=0;k<2*N-2;k+=2) {
		for(int j=k+2;j<2*N;j+=2) {
			dist=2.0*R*abs(uv[k]*uv[j+1]-uv[j]*uv[k+1]);
			E1+=1.0/dist; for(int l=0;l<=kM;l++) E1+=c[l][0]*pow(dist, l)*exp(-dist);
			E2+=1.0/dist; for(int l=0;l<=twoS;l++) E2+=c[l][1]*pow(dist, l)*exp(-dist);
		}
	}
	vector<double> Es(2); Es[0]=E1; Es[1]=E2;
	return Es;
}


double EcalcC_2LLpal(int N, double R, vector<complex<double> > uv) {
	double E=0, dist; double a1=117.429, a2=-755.468, alpha1=1.3177, alpha2=2.9026;
	for(int k=0;k<2*N-2;k+=2) {
		for(int j=k+2;j<2*N;j+=2) {
			dist=2.0*R*abs(uv[k]*uv[j+1]-uv[j]*uv[k+1]);
			E+=1.0/dist+a1*exp(-alpha1*dist*dist)+a2*dist*dist*exp(-alpha2*dist*dist);
		}
	}
	return E;
}

double abs(alglib::complex c) {return sqrt(c.x*c.x+c.y*c.y);}

double EcalcAwoR(int N, alglib::complex_1d_array uv) {
	double E=0;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) {
			E+=1.0/asin(abs(uv[i]*uv[j+1]-uv[j]*uv[i+1]));
		}
	}
	return E;
}

complex<double> SlaterDet(int N, vector<complex<double> > uv, int m) {
	complex<double> det=1;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) det*=uv[i]*uv[j+1]-uv[j]*uv[i+1];
	}
	return pow(det, m);
}

alglib::complex SlaterDet(int N, alglib::complex_1d_array uv, int m) {
	alglib::complex det=1;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) det*=uv[i]*uv[j+1]-uv[j]*uv[i+1];
	}
	return pow(det, m);
}

void e() {cout << endl;}
void ee() {cout << endl << endl;}
void eee() {cout << endl << endl << endl;}
void eeee() {cout << endl << endl << endl << endl;}
	
complex<double> detrat(int N, vector<vector<complex<double> > > invmat, int r, vector<complex<double> > v) {
	complex<double> ret=0;
	for(int i=0;i<N;i++) ret+=v[i]*invmat[i][r];
	return ret+1.0;
}

bool isnumber(string s) {
	string::const_iterator it = s.begin(); bool hasnumbers=false, hase=false;
	if(!isdigit(*it)) {if(*it!='-') return false;}
	else hasnumbers=true; it++;
	while(it!=s.end()) {
		if ((isdigit(*it))) hasnumbers=true;
		else if(*it=='e') {
			if(hase) return false;
			hase=true; 
			if(*(it+1)=='-') {if(it+2==s.end() || !isdigit(*(it+2))) return false; it++;}
			else if(it+1==s.end() || !isdigit(*(it+1))) return false;
		}
		else if(*it!='.') return false;
		it++;
	}
	return hasnumbers;
}

bool isint(string s) {
	string::const_iterator it = s.begin(); bool hase=false;
	if(!isdigit(*it)) {if(*it!='-') return false;}
	it++;
	while(it!=s.end()) {
		if(*it=='e') {
			if(hase) return false;
			hase=true; 
			if(it+1==s.end() || !isdigit(*(it+1))) return false;
		}
		else if (!(isdigit(*it))) return false;
		it++;
	}
	return true;
}

void disp(vars V) {

	cout << endl << "Parameters: " << endl << endl;
	if(V.i.size()>=1) cout << "Integers: " << endl;
	map<string,int>::iterator iti;
	for (map<string,int>::iterator iti=V.i.begin(); iti!=V.i.end(); iti++) cout << "\t" << iti->first << " = " << n2s(iti->second) << endl;
	if(V.ull.size()>=1)cout << "Unsigned long long integers: " << endl;
	map<string, unsigned long long>::iterator itu;
	for (map<string, unsigned long long>::iterator itu=V.ull.begin(); itu!=V.ull.end(); itu++) cout << "\t" << itu->first << " = " << n2s(itu->second) << endl;
	if(V.d.size()>=1)cout << "Floats: " << endl;
	map<string,double>::iterator itd;
	for (map<string,double>::iterator itd=V.d.begin(); itd!=V.d.end(); itd++) cout << "\t" << itd->first << " = " << itd->second << endl;
	if(V.s.size()>=1)cout << "Strings: " << endl;
	map<string,string>::iterator its;
	for (map<string,string>::iterator its=V.s.begin(); its!=V.s.end(); its++) cout << "\t" << its->first << " = " << its->second << endl;
	if(V.b.size()>=1)cout << "Booleans: " << endl;
	map<string,bool>::iterator itb;
	for (map<string,bool>::iterator itb=V.b.begin(); itb!=V.b.end(); itb++) cout << "\t" << itb->first << " = " << itb->second << endl;
}

void WE(int w, string s) {
	switch(w) {
		case 1: cerr << "ERROR: Input parameter " << s << " unknown" << endl; break;
		case 2: cerr << "ERROR: task " << s << " unknown" << endl; break;
		case 3: cerr << "ERROR: File " << s << " not found" << endl; break;
	}	
}

int argin(int argc, const char* argv[], vars& V) {

	if(argc==1) cout << endl << "Running with default parameters" << endl << endl;
	else if(argc==2 && (strcmp(argv[1], "--h")==0 || strcmp(argv[1], "-h")==0 || strcmp(argv[1], "--help")==0 || strcmp(argv[1], "-help")==0 ||
				  strcmp(argv[1], "--H")==0 || strcmp(argv[1], "-H")==0 || strcmp(argv[1], "--Help")==0 || strcmp(argv[1], "-Help")==0 ||
				  strcmp(argv[1], "--HELP")==0 || strcmp(argv[1], "-HELP")==0) ) return -1;
	else {
		for(int i=1;i<argc;i++) {
			string s=argv[i]; string ns=s.substr(1);
			if(argv[i][0]!='-') {cerr << "ERROR: Input parameter names must begin with '-' (i.e. -" << argv[i] << ")" << endl; return 1;}
			else if(s.length()==1) {cerr << "ERROR: The form of input mparameters should be '-<parameter name>'" << endl; return 1;}
			else if(strcmp(argv[i], "-ra")==0) {V.b["ra"]=true;}
			else if(strcmp(argv[i], "-np")==0) {V.b["np"]=true;}
			else if(argc==i+1) {cerr << "ERROR: Input parameter " << ns << " missing" << endl; return 1;}
			else if (argv[i+1][0]=='-' && !(isint(argv[i+1]) || isnumber(argv[i+1]))) {
				cerr << "ERROR: Input parameter " << argv[i] << " missing" << endl; return 1;
			}
			else if(strcmp(argv[i], "--h")==0 || strcmp(argv[i], "-h")==0 || strcmp(argv[i], "--help")==0 || strcmp(argv[i], "-help")==0 ||
				  strcmp(argv[i], "--H")==0 || strcmp(argv[i], "-H")==0 || strcmp(argv[i], "--Help")==0 || strcmp(argv[i], "-Help")==0 ||
				  strcmp(argv[i], "--HELP")==0 || strcmp(argv[i], "-HELP")==0) {cerr << "WARNING: Run with '-h' only for input instructions" << endl;}
			else if(strcmp(argv[i], "-os")==0) {char *c; V.ull["os"]=strtoull(argv[i+1], &c, 10); i++;}
			else if(strcmp(argv[i], "-is")==0) {char *c; V.ull["is"]=strtoull(argv[i+1], &c, 10); i++;}
			else if(isint(argv[i+1])) {if(V.i.count(ns)) V.i.at(ns)=atoi(argv[i+1]); else if(!V.d.count(ns)) {ok(1); WE(1, ns); return 1;} i++;}
			else if(isnumber(argv[i+1])) {if(V.d.count(ns)) V.d.at(ns)=atof(argv[i+1]); else {ok(2); WE(1, ns);  return 1;} i++;}
			else {if(V.s.count(ns)) V.s.at(ns)=argv[i+1]; else {ok(3); WE(1, ns); return 1;} i++;}
		}	
	}
	
	if(V.s.count("wf")) {
		string s=V.s["wf"]; char *c=new char[s.length()+3]; c[0]='.'; c[1]='/'; 
		for(int i=0;i<s.length();i++) c[i+2]=s[i]; c[s.length()+2]='\0'; 
		if(exc(V, c)) return 1;
	}
	else if(exc(V, argv[0])) return 1;
	if(V.i.count("ot")) V.i["ot"]*=V.i["N"];
	if(V.i.count("oh")) V.i["oh"]*=V.i["N"];
	
	return 0;
}

bool exc(vars& V, const char* prog) {
	int N=V.i["N"];
	if(strcmp(prog, "./BSWF")==0 || strcmp(prog, "./BSWF2")==0) {
		int n=V.i["n"], p=V.i["p"];
		if(n<2 || n>4) {cerr << "ERROR: CF wavefunction not set up for n = " << n << endl; return true;}
		if(p!=1) {cerr << "ERROR: CF wavefunction not set up for p = " << p << endl; return true;}
		if(N&1) {cerr << "ERROR: Pfaffian not defined for odd N" << endl; return true;}
		double twoSnCFtmp=n-N/(double)n+2*p*(N-1.0), twoqtmp=N/(double)n-n;
		if(different(twoSnCFtmp, (int)twoSnCFtmp, 12) || different(twoqtmp, (int)twoqtmp, 12) || twoqtmp<=0 || twoSnCFtmp<=0) {
			cerr << "ERROR: negative Jain not defined for N = " << N << " and n = " << n; return true;
		}
		V.i["twoSnCF"]=(int)twoSnCFtmp; V.i["twoq"]=(int)twoqtmp; V.d["nu"]=n/(3.0*n-1.0);
		V.i["twoS"]=3*N-N/(double)n-4+n;
	}
	else if(strcmp(prog, "./CFWF")==0 || strcmp(prog, "./CFWF2")==0) {
		int n=V.i["n"], p=V.i["p"];
		if(n<2 || n>4) {cerr << "ERROR: CF wavefunction not set up for n = " << n << endl; return true;}
		if(p!=1) {cerr << "ERROR: CF wavefunction not set up for p = " << p << endl; return true;}
		double twoStmp=n-N/(double)n+2*p*(N-1.0), twoqtmp=N/(double)n-n;
		if(different(twoStmp, (int)twoStmp, 12) || different(twoqtmp, (int)twoqtmp, 12) || twoqtmp<=0 || twoStmp<=0) {
			cerr << "ERROR: negative Jain not defined for N = " << N << " and n = " << n; return true;
		}
		V.i["twoS"]=(int)twoStmp; V.i["twoq"]=(int)twoqtmp; V.d["nu"]=n/(2.0*n-1.0);
	}
	else if(strcmp(prog, "./MRWF")==0) {
		int p=V.i["p"];
		if(p<1 || p>2) {cerr << "ERROR: MR wavefunction not set up for p = " << p << endl; return true;}
		if(N&1) {cerr << "ERROR: Pfaffian not defined for odd N" << endl; return true;}
		V.d["nu"]=1.0/p;
		V.i["twoS"]=p*N-p-1.0;
	}
	else if(strcmp(prog, "./LAWF")==0) {
		int p=V.i["p"];
		if(p<1) {cerr << "ERROR: Laughlin wavefunction not defined for p = " << p << endl; return true;}
		V.d["nu"]=1.0/p;
		V.i["twoS"]=p*(N-1.0);
	}
	else if(strcmp(prog, "./psinu1")==0) {
		//if(V.ull.count("os")) {cerr << "ERROR: parameter os unknown" << endl; return true;}
		V.d["nu"]=1.0/3.0;
		V.i["twoS"]=3.0*(N-1.0);
	}
	else if(strcmp(prog, "-")==0) {}
	else cerr << "WARNING: No program specific initial parameters set up" << endl;
	return false;
}

	

void pc(vector<vector<complex<double> > > c) {
	int nr=c.size(), nc=c[0].size();
	for(int i=0;i<nr;i++) {
		for(int j=0;j<nc;j++) cout << c[i][j] << "  ";
		e();
	}
}

void pc(vector<complex<double> > c) {
	int n=c.size();
	for(int j=0;j<n;j++) cout << c[j] << endl;
}

void pd(vector<double> d) {
	int n=d.size();
	for(int j=0;j<n;j++) cout << d[j] << endl;
}

void pd(vector<vector<vector<double> > > d) {
	for(int i=0;i<d.size();i++) {
		for(int j=0;j<d[i].size();j++) {
			for(int k=0;k<d[i][j].size();k++) cout << d[i][j][k] << "  ";
			e();
		}
		e(); e();
	}
}

void pd(vector<vector<double> > d) {
	for(int i=0;i<d.size();i++) {
		for(int j=0;j<d[i].size();j++) cout << d[i][j] << "  "; e();
	}
}

void pcRI(vector<vector<complex<double> > > c) {
	int nr=c.size(), nc=c[0].size();
	for(int i=0;i<nr;i++) {
		for(int j=0;j<nc;j++) cout << real(c[i][j]) << "  ";
		e();
	} e();
	e(); for(int i=0;i<nr;i++) {
		for(int j=0;j<nc;j++) cout << imag(c[i][j]) << "  ";
		e();
	} e();
}

double zmax(int N, int& ind, vector<complex<double> > uv) {
	double m; m=abs(uv[1]/uv[0]); ind=0;
	for(int i=2;i<2*N;i+=2) {
		double absz=abs(uv[i+1]/uv[i]);
		if(absz>m) {m=absz; ind=i/2;}
	}
	return m;
}

void pcRI(vector<complex<double> > c) {
	for(int i=0;i<c.size();i++) cout << real(c[i]) << endl; e();
	for(int i=0;i<c.size();i++) cout << imag(c[i]) << endl;
}

int max(vector<int> v) {
	int m=v[0];
	for(int i=1;i<v.size();i++) {
		if(v[i]>m) m=v[i];
	}
	return m;
}

vector<int> max2(vector<int> v) {
	vector<int> m(2); m[0]=v[0];
	for(int i=1;i<v.size();i++) {
		if(v[i]>m[0]) {m[0]=v[i]; m[1]=i;}
	}
	return m;
}

void Laughlin(int N, double step, vector<double>& tp, vector<complex<double> >& uv, double rans[], int& accept) {

	int r=(int)(N*rans[0]);
	double newtheta, newphi;
	complex<double> tmp=1, newu, newv;
	newtp(tp[2*r], step*(rans[1]-0.5), tp[2*r+1], step*(2.0*rans[2]-1.0), newtheta, newphi);
	tp2uv(newtheta, newphi, newu, newv);
	
	for(int i=0;i<r;i++) tmp*=(uv[2*i]*newv-newu*uv[2*i+1])/(uv[2*i]*uv[2*r+1]-uv[2*r]*uv[2*i+1]);
	for(int i=r+1;i<N;i++) tmp*=(uv[2*i]*newv-newu*uv[2*i+1])/(uv[2*i]*uv[2*r+1]-uv[2*r]*uv[2*i+1]);
	
	if(pow(norm(tmp), 3)*sin(newtheta)/sin(tp[2*r])>rans[3]) {
		tp[2*r]=newtheta; tp[2*r+1]=newphi;
		uv[2*r]=newu; uv[2*r+1]=newv;
		accept++;
	}
	
	return;
}

double norm(alglib::complex c) {return c.x*c.x+c.y*c.y;}

int nCFprecN12(double azm) {int pr=ceil(12.0*log(azm-0.78172)+21); return max(pr+3-((pr-1)%4), 32);}
int nCFprecN15(double azm) {int pr=ceil(18.0*log(azm+0.21828)+15); return max(pr+3-((pr-1)%4), 32);}
int nCFprecN18(double azm) {int pr=ceil(23.0*log(azm+0.21828)+12); return max(pr+3-((pr-1)%4), 32);}
int nCFprecN21(double azm) {int pr=ceil(27.0*log(azm+0.21828)+12); return max(pr+3-((pr-1)%4), 32);}
int nCFprecN24(double azm) {int pr=ceil(31.0*log(azm+0.21828)+12); return max(pr+3-((pr-1)%4), 32);}

void Laughlin(int N, double step, alglib::real_1d_array& tp, alglib::complex_1d_array& uv, double rans[], int& accept) {

	int r=(int)(N*rans[0]);
	double newtheta, newphi;
	alglib::complex tmp=1, newu, newv;
	newtp(tp[2*r], step*(rans[1]-0.5), tp[2*r+1], step*(2.0*rans[2]-1.0), newtheta, newphi);
	tp2uv(newtheta, newphi, newu, newv);
	
	for(int i=0;i<r;i++) tmp*=(uv[2*i]*newv-newu*uv[2*i+1])/(uv[2*i]*uv[2*r+1]-uv[2*r]*uv[2*i+1]);
	for(int i=r+1;i<N;i++) tmp*=(uv[2*i]*newv-newu*uv[2*i+1])/(uv[2*i]*uv[2*r+1]-uv[2*r]*uv[2*i+1]);
	
	if(pow(norm(tmp), 3)*sin(newtheta)/sin(tp[2*r])>rans[3]) {
		tp[2*r]=newtheta; tp[2*r+1]=newphi;
		uv[2*r]=newu; uv[2*r+1]=newv;
		accept++;
	}
	
	return;
}

bool different(double a, double b, int prec) {return abs(a-b)/max(abs(a),abs(b))>pow(10, -prec);}
bool different(complex<double> c1, complex<double> c2, int prec) {return abs(c1-c2)/max(abs(c1),abs(c2))>pow(10,-prec);}
bool different(vector<vector<complex<double> > > c1, vector<vector<complex<double> > > c2, int prec, complex<double>& c1d, complex<double>& c2d, int& i, int& j) {
	int nr=c1.size(); if(nr!=c2.size()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
	for(i=0;i<nr;i++) {
		if(c1[i].size()!=c2[i].size()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
		for(j=0;j<c1[i].size();j++) {
			if(abs(c1[i][j]-c2[i][j])/max(abs(c1[i][j]),abs(c2[i][j]))>pow(10,-prec)) {c1d=c1[i][j]; c2d=c2[i][j]; return true;}
		}
	}
	return false;
}
bool different(vector<vector<complex<double> > > c1, vector<vector<complex<double> > > c2, int prec) {
	bool b=false; int nr=c1.size(); if(nr!=c2.size()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
	for(int i=0;i<nr;i++) {
		if(c1[i].size()!=c2[i].size()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
		for(int j=0;j<c1[i].size();j++) {
			if(abs(c1[i][j]-c2[i][j])/max(abs(c1[i][j]),abs(c2[i][j]))>pow(10,-prec)) b=true;
		}
	}
	return b;
}
bool different(vector<vector<double> > c1, vector<vector<double> > c2, int prec) {
	bool b=false; int nr=c1.size(), nc=c1[0].size(); if(nr!=c2.size()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
	for(int i=0;i<nr;i++) {
		if(c1[i].size()!=c2[i].size()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
		for(int j=0;j<c1[i].size();j++) {
			if(abs(c1[i][j]-c2[i][j])/max(abs(c1[i][j]),abs(c2[i][j]))>pow(10,-prec)) b=true;
		}
	}
	return b;
}
bool different(vector<vector<double> > c, alglib::real_2d_array a, int prec) {
	int nr=c.size(), nc=c[0].size(); if(nr!=a.rows()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
	for(int i=0;i<nr;i++) {
		if(c[i].size()!=a.cols()) {cerr << "ERROR: Matrices not of same size" << endl; return 1;}
		for(int j=0;j<c[i].size();j++) {
			if(abs(c[i][j]-al2cd(a[i][j]))/max(abs(c[i][j]),abs(al2cd(a[i][j])))>pow(10,-prec)) return true;
		}
	}
	return false;
}
bool different(alglib::complex_2d_array a1, alglib::complex_2d_array a2, complex<double>& el1, complex<double>& el2, int& i, int& j, int prec) {
	int nr=a1.rows(), nc=a2.cols(); if(nr!=a2.rows() || nc!=a2.cols()) {cerr << "ERROR: Matrices not of same size" << endl; return true;}
	for(i=0;i<nr;i++) {
		for(j=0;j<nc;j++) {
			if(abs(a1[i][j]-a2[i][j])/max(abs(a2[i][j]),abs(a2[i][j]))>pow(10,-prec)) {
				el1=al2cd(a1[i][j]); el2=al2cd(a2(i,j)); return true;
			}
		}
	}
	return false;
}
bool different(vector<vector<vector<double> > > c1, vector<vector<vector<double> > > c2, int prec, double& c1d, double& c2d, int& i, int& j, int& k) {
	int n1=c1.size(); if(n1!=c2.size()) {cerr << "ERROR: Tensors not of same size" << endl; return 1;}
	for(i=0;i<n1;i++) {
		if(c1[i].size()!=c2[i].size()) {cerr << "ERROR: Tensors not of same size" << endl; return 1;}
		for(j=0;j<c1[i].size();j++) {
			if(c1[i][j].size()!=c2[i][j].size()) {cerr << "ERROR: Tensors not of same size" << endl; return 1;}
			for(k=0;k<c1[i][j].size();k++) {
				if(abs(c1[i][j][k]-c2[i][j][k])/max(abs(c1[i][j][k]),abs(c2[i][j][k]))>pow(10,-prec)) {c1d=c1[i][j][k]; c2d=c2[i][j][k]; return true;}
			}
		}
	}
	return false;
}
bool different(vector<complex<double> > c1, vector<complex<double> > c2, int prec) {
	int n=c1.size(); if(n!=c2.size()) {cerr << "ERROR: Vectors not of same length: " << n << " and " << c2.size() << endl; return 1;}
	for(int i=0;i<n;i++) {
		if(abs(c1[i]-c2[i])/max(abs(c1[i]),abs(c2[i]))>pow(10,-prec)) return true;
	}
	return false;
}
bool different(alglib::complex_1d_array a1, alglib::complex_1d_array a2, int prec) {
	int n=a1.length(); if(n!=a2.length()) {cerr << "ERROR: Vectors not of same length" << endl; return 1;}
	for(int i=0;i<n;i++) {
		if(abs(a1[i]-a2[i])/max(abs(a1[i]),abs(a2[i]))>pow(10,-prec)) return true;
	}
	return false;
}
bool different(alglib::complex_1d_array a, vector<complex<double> > c, int prec) {
	int n=a.length(); if(n!=c.size()) {cerr << "ERROR: Vectors not of same length" << endl; return 1;}
	for(int i=0;i<n;i++) {
		if(abs(al2cd(a[i])-c[i])/max(abs(al2cd(a[i])),abs(c[i]))>pow(10,-prec)) return true;
	}
	return false;
}
bool different(alglib::real_1d_array a, vector<double> c, int prec) {
	int n=a.length(); if(n!=c.size()) {cerr << "ERROR: Vectors not of same length" << endl; return 1;}
	for(int i=0;i<n;i++) {
		if(abs(al2cd(a[i])-c[i])/max(abs(al2cd(a[i])),abs(c[i]))>pow(10,-prec)) return true;
	}
	return false;
}
bool different(vector<complex<double> > c, alglib::complex_1d_array a, int prec) {
	int n=a.length(); if(n!=c.size()) {cerr << "ERROR: Vectors not of same length" << endl; return 1;}
	for(int i=0;i<n;i++) {
		if(abs(al2cd(a[i])-c[i])/max(abs(al2cd(a[i])),abs(c[i]))>pow(10,-prec)) return true;
	}
	return false;
}
bool different(vector<double> c, alglib::real_1d_array a, int prec) {
	int n=a.length(); if(n!=c.size()) {cerr << "ERROR: Vectors not of same length" << endl; return 1;}
	for(int i=0;i<n;i++) {
		if(abs(al2cd(a[i])-c[i])/max(abs(al2cd(a[i])),abs(c[i]))>pow(10,-prec)) return true;
	}
	return false;
}
void cpr(int pr) {cout << setprecision(15);}

int elcoords(int N, vector<double>& tp, vector<complex<double> >& uv, CRandomSFMT& rangen) {

	int t=N*10000; double step=pi*(pow(N, -0.65)+0.02);
	rantp(N, tp, rangen);
	tp2uv(N, tp, uv);
	int accept=0; double rans[4];
	for(int i=0;i<t;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		Laughlin(N, step, tp, uv, rans, accept);
	}
	//cout << setprecision(4) << "Laughlin: " << 100.0*accept/(double)t << "% accepted" << endl;

	return 0;
}

int elcoords(int N, string pl, vector<double>& tp, vector<complex<double> >& uv, CRandomSFMT& rangen) {

	if(strcmp(pl.c_str(), "Random")==0) {
		double step=pi*(pow(N, -0.65)+0.02);
		int t=N*10000;
		rantp(N, tp, rangen);
		tp2uv(N, tp, uv);
		int accept=0; double rans[4];
		for(int i=0;i<t;i++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			Laughlin(N, step, tp, uv, rans, accept);
		}
		//cout << setprecision(4) << "Laughlin: " << 100.0*accept/(double)t << "% accepted" << endl;
	}
	else {
		ifstream in(pl.c_str()); if(!in) {WE(3, pl); return 1;}	in >> setprecision(15); 
		for(int i=0;i<2*N;i+=2) in >> tp[i] >> tp[i+1]; in.close(); tp2uv(N, tp, uv);
	}

	return 0;
}

alglib::real_1d_array d2al(vector<double> c) {int n=c.size(); alglib::real_1d_array a; a.setlength(n); for(int i=0;i<n;i++) a[i]=c[i]; return a;}

complex<double> Laugh(int N, vector<complex<double> > pq) {
	complex<double> tmp=1;
	for(int i=0;i<2*N-2;i+=2) {
		for(int j=i+2;j<2*N;j+=2) tmp*=pq[i]*pq[j+1]-pq[j]*pq[i+1];
	}
	return tmp*tmp*tmp;
}

int elcoords(int N, alglib::real_1d_array& tp, alglib::complex_1d_array& uv, CRandomSFMT& rangen) {

	int t=N*10000;
	double step=pi*(pow(N, -0.65)+0.02);
	rantp(N, tp, rangen);
	tp2uv(N, tp, uv);
	int accept=0; double rans[4];
	for(int i=0;i<t;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		Laughlin(N, step, tp, uv, rans, accept);
	}
	//cout << setprecision(4) << "Laughlin: " << 100.0*accept/(double)t << "% accepted" << endl;

	return 0;
}

int elcoords(int N, string pl, alglib::real_1d_array& tp, alglib::complex_1d_array& uv, CRandomSFMT& rangen) {

	if(strcmp(pl.c_str(), "Random")==0) {
		double step=pi*(pow(N, -0.65)+0.02);
		int t=N*10000;
		rantp(N, tp, rangen);
		tp2uv(N, tp, uv);
		int accept=0; double rans[4];
		for(int i=0;i<t;i++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			Laughlin(N, step, tp, uv, rans, accept);
		}
		//cout << setprecision(4) << "Laughlin: " << 100.0*accept/(double)t << "% accepted" << endl;
	}
	else {
		ifstream in(pl.c_str()); if(!in) {WE(3, pl); return 1;}	in >> setprecision(15); 
		for(int i=0;i<2*N;i+=2) in >> tp[i] >> tp[i+1]; in.close(); tp2uv(N, tp, uv);
	}

	return 0;
}

void Lcoords(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], oh=V.i["oh"], accept=0; int twoS=3.0*(N-1.0); double step=pi*(pow(N, -0.65)+0.02);
	unsigned long long os=V.ull["os"];
	vector<double> tp(2*N);
	vector<complex<double> > uv(2*N);
	elcoords(N, tp, uv, rangen);
	string name=V.s["pl"]; if(strcmp(name.c_str(), "Random")==0) {cerr << "ERROR: must use different filename than 'Random'" << endl; return;}
	ofstream out(name.c_str()); out << setprecision(15);
	for(int i=0;i<2*N;i+=2) out << tp[i] << "     " << tp[i+1] << endl; out.close();
}

void LE_arc(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], oh=V.i["oh"], accept=0; int twoS=3.0*(N-1.0); double step=pi*(pow(N, -0.65)+0.02);
	unsigned long long os=V.ull["os"];
	vector<double> tp(2*N);
	vector<complex<double> > uv(2*N);
	elcoords(N, tp, uv, rangen);
	double E=0, E2=0, e, rans[4];
	double si, dum, bg; alglib::sinecosineintegrals(pi, si, dum); bg=-(double)N*si/(4.0*sqrt(twoS/2.0));
	for(unsigned long long i=0;i<os;i++) {
		e=EcalcA(N, twoS, uv); E+=e; E2+=e*e;
		for(int j=0;j<oh;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			Laughlin(N, step, tp, uv, rans, accept);
		}
	}
	E/=(double)os; E2/=(double)os;
	double error=sqrt((E2-E*E)/(double)os);
	cout << 100.0*accept/((double)os*oh) << "% accepted" << endl << endl << "E = " << setprecision(15) << E/(double)N+bg 
		 << " +- " << setprecision(4) << error/(double)N << endl;
}

void LE_chord(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], oh=V.i["oh"], accept=0; int twoS=3.0*(N-1.0); double step=pi*(pow(N, -0.65)+0.02);
	unsigned long long os=V.ull["os"];
	vector<double> tp(2*N);
	vector<complex<double> > uv(2*N);
	elcoords(N, tp, uv, rangen);
	double E=0, E2=0, e, rans[4];
	double bg=-N/(2.0*sqrt(twoS/2.0));
	for(unsigned long long i=0;i<os;i++) {
		e=EcalcC(N, twoS, uv); E+=e; E2+=e*e;
		for(int j=0;j<oh;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			Laughlin(N, step, tp, uv, rans, accept);
		}
	}
	E/=(double)os; E2/=(double)os;
	double error=sqrt((E2-E*E)/(double)os);
	cout << 100.0*accept/((double)os*oh) << "% accepted" << endl << endl << "E = " << setprecision(15) << E/(double)N+bg 
		 << " +- " << setprecision(4) << error/(double)N << endl;
}

vector<string> dirfiles(string& path) {

	DIR* dir;
	dirent* pdir;
	vector<string> files;
	if(path[path.length()-1]!='/') path+="/";
	dir=opendir(path.c_str());
	if(!dir) {cerr << "ERROR: folder " << path << " not found" << endl; return vector<string>();}
	while(pdir=readdir(dir)) files.push_back(pdir->d_name);
	files.erase(files.begin(), files.begin()+2);
	return files;
}

double Laughlinm3steps(int N) {
	switch(N) {
		case 2: return pi*0.63; break; 
		case 3: return pi*0.48; break;
		case 4: return pi*0.4; break; 
		case 5: return pi*0.35; break;
		case 6: return pi*0.32; break;
		case 7: return pi*0.29;  break;
		case 8: return pi*0.26;  break;
		case 9: return pi*0.25;  break;
		case 10: return pi*0.235; break; 
		case 11: return pi*0.22;  break;
		case 12: return pi*0.215; break;
		case 13: return pi*0.205; break; 
		case 14: return pi*0.195; break; 
		case 15: return pi*0.192; break; 
		case 18: return pi*0.175; break;
		case 20: return pi*0.165; break; 
		case 21: return pi*0.162; break; 
		case 24: return pi*0.15;  break;
		case 27: return pi*0.143; break; 
		case 30: return pi*0.135; break; 
		case 33: return pi*0.13;  break;
		case 36: return pi*0.122; break;
		case 39: return pi*0.117; break;
		case 40: return pi*0.115; break; 
		case 42: return pi*0.113; break;
		default: cerr << "ERROR: step for Laughlin m=3 N=" << N << " not found" << endl; return -1;
	}
}

void rantp(int N, vector<double>& tp, CRandomSFMT& rangen) {
	for(int i=0;i<2*N;i+=2) {
		tp[i]=acos(2.0*rangen.Random()-1.0);
		tp[i+1]=2.0*pi*rangen.Random();
	}
	
	return;
}

void rantp(int N, alglib::real_1d_array& tp, CRandomSFMT& rangen) {
	for(int i=0;i<2*N;i+=2) {
		tp[i]=acos(2.0*rangen.Random()-1.0);
		tp[i+1]=2.0*pi*rangen.Random();
	}
	
	return;
}

string nostring(int n) {
	stringstream ss;
	if(n<10) ss << 0 << n;
	else ss << n;
	return ss.str();
}

string timestring() {
	time_t t = time(0);
    struct tm * now = localtime( & t );
	stringstream ss; 
	return nostring(now->tm_year + 1900)+" "+nostring(now->tm_mday)+"."+nostring(now->tm_mon+1)+"  "+nostring(now->tm_hour)+":"
			+nostring(now->tm_min)+":"+nostring(now->tm_sec);
}

string n2s(int n) {
	stringstream ss;
	if(abs(n)<1000) ss << n;
	else if (abs(n)<1e6) {
		if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs(n)<1e9) {
		if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs(n)<INT_MAX) {
		if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else ss << n;
	return ss.str();
}

string n2s(unsigned long long n) {
	stringstream ss;
	if(abs((double)n)<1000) ss << n;
	else if (abs((double)n)<1e6) {
		if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e9) {
		if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e12) {
		if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e15) {
		if((abs(n/1.0e12)-abs((int)(n/1e12)))/1.0e12<1e-13) ss << n/1e9 << "T";
		else if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else if (abs((double)n)<1e18) {
		if((abs(n/1.0e15)-abs((int)(n/1e15)))/1.0e15<1e-13) ss << n/1e15 << "Q";
		else if((abs(n/1.0e12)-abs((int)(n/1e12)))/1.0e12<1e-13) ss << n/1e12 << "T";
		else if((abs(n/1.0e9)-abs((int)(n/1e9)))/1.0e9<1e-13) ss << n/1e9 << "B";
		else if((abs(n/1.0e6)-abs((int)(n/1e6)))/1.0e6<1e-13) ss << n/1e6 << "M";
		else if((abs(n/1000.0)-abs((int)(n/1000)))/1.000<1e-13) ss << n/1000 << "k";
		else ss << n;
	}
	else ss << n;
	return ss.str();
}

double bin(int n, int m) {

	int i;
	double bin=1.0;
	if(m>n||m<0) return 0.0;
	else {
		for(i=1;i<=m;i++) {
			bin*=(double) (n-m+i)/i;
		}
	}
	return bin;
}

vector<complex<double> > al2cd(int n, alglib::complex_1d_array alv) {
	vector<complex<double> > cdv(n); for(int i=0;i<n;i++) cdv[i]=complex<double>(alv[i].x, alv[i].y); return cdv;
}

vector<double> al2d(int n, alglib::real_1d_array alv) {vector<double> dv(n); for(int i=0;i<n;i++) dv[i]=alv[i]; return dv;}
complex<double> al2cd(alglib::complex al) {return complex<double>(al.x, al.y);}	

vector<vector<complex<double> > > cinvdet(int N, vector<vector<complex<double> > > mat, complex<double>& det) {

	alglib::complex_2d_array arr; arr.setlength(N, N); alglib::ae_int_t info; alglib::matinvreport rep;
	for(int i=0;i<N;i++) { for(int j=0;j<N;j++) { arr(i,j).x=real(mat[i][j]); arr(i,j).y=imag(mat[i][j]); } }
	alglib::complex adet=alglib::cmatrixdet(arr);
	alglib::cmatrixinverse(arr, info, rep);
	det=complex<double>(adet.x, adet.y);
	vector<vector<complex<double> > > invmat(N, vector<complex<double> >(N));
	for(int i=0;i<N;i++) {for(int j=0;j<N;j++) invmat[i][j]=complex<double>(arr[i][j].x, arr[i][j].y); }
	return invmat;
}

complex<double> cdet(vector<vector<complex<double> > > mat) {

	int N=mat.size(); if(N!=mat[0].size()) {cerr << "ERROR: Not a square matrix" << endl; return complex<double>();}
	alglib::complex_2d_array arr; arr.setlength(N, N); alglib::ae_int_t info; alglib::matinvreport rep;
	for(int i=0;i<N;i++) { for(int j=0;j<N;j++) { arr(i,j).x=real(mat[i][j]); arr(i,j).y=imag(mat[i][j]); } }
	alglib::complex adet=alglib::cmatrixdet(arr);
	alglib::cmatrixinverse(arr, info, rep);
	return complex<double>(adet.x, adet.y);;
}