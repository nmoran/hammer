#include "MRWF.h"

//  ./MRWF -N 12 -oh 2 -ot 10000 -os 1000000 -fn ./MCData/MR/p2/N12/configs/MR_N12os1M_1.dat > ./MCData/MR/p2/N12/rep/N12os1M_1.txt 2> /dev/null

int main(int argc, const char* argv[]) {

	vars V; setupV(V); clock_t starttime=clock(), argr=argin(argc, argv, V);
	if(argr==1) return 0; else if(argr==-1) {showHelp(); return 0;}
	if(V.i["sr"]!=-1) {
		if(strcmp(V.s["fn"].c_str(),"output.dat")!=0 || V.i["sd"]!=0) {cerr << "ERROR: Parameters '-fn' and '-sd' will be ignored when '-sr' is in use" << endl; return 0;}
		stringstream ss; ss << "p" << V.i["p"] << "/N" << V.i["N"] << "/configs/N" << V.i["N"] << "os" 
							<< n2s(V.ull["os"]) << "_" << V.i["sr"] << ".dat"; 
		V.s["fn"]=ss.str(); V.i["sd"]=V.i["sr"];
	}
	V.i["sd"]+=time(0); CRandomSFMT rangen(V.i["sd"]);
	getstep gs; int nstep=gs.checkN(V.i["N"], V.i["p"]);
	if(true) {
		if(!different(V.d["st"], 0.3, 6)) {
			if(nstep==-1) {cerr << "ERROR: step not found or supplied" << endl; return 0;}
			else {V.d["st"]=gs.steps[V.i["p"]-1][nstep];}
		} V.d["st"]*=pi;
	}
	disp(V); e(); cout << endl <<  "Program started " << timestring() << endl << "-----------------------------------------" << endl << endl;
	
	int tk=V.i["tk"]; 
	if(tk==1) MR_MC(V, rangen);
	else {stringstream ss; ss << tk; WE(2, ss.str()); return 0;}
	
	double sec=((double)clock()-starttime)/CLOCKS_PER_SEC; cout << setprecision(4) << endl;
	cout << "-----------------------------------------" << endl << "Total Runtime: " << sec << "s = " << sec/60.0 << "min" << endl;
	cout << endl <<  "Program finished " << timestring() << endl;
	return 0;
}

inline double abs(alglib::complex a) {return sqrt(a.x*a.x+a.y*a.y);}
inline alglib::complex exp(alglib::complex a) {return exp(a.x)*alglib::complex(cos(a.y), sin(a.y));}
inline alglib::complex pow(alglib::complex a, int n) {complex<double> c=complex<double>(a.x, a.y); return alglib::complex(real(pow(c, n)), imag(pow(c, n)));}
inline alglib::complex loga(alglib::complex a) {complex<double> c=complex<double>(a.x, a.y); return alglib::complex(real(log(c)), imag(log(c)));}
inline void newtp(double theta, double addt, double phi, double addp, double& newt, double& newp) {
	newt=theta+addt;
	if(newt<0) {newt=-newt; phi=fmod(phi+pi,twopi); }
	else if(newt>pi) {newt=twopi-newt; phi=fmod(phi+pi,twopi); }
	newp=fmod(phi+addp, twopi);
	if(newp<0) newp+=twopi;
	
	return;
}
inline complex<double> log(alglib::complex a) {return log(complex<double>(a.x, a.y));}
inline alglib::complex cd2al(complex<double> c) {return alglib::complex(real(c), imag(c));}
inline void tp2uv(double theta, double phi, alglib::complex& u, alglib::complex& v) {
	double cp=cos(phi/2.0), sp=sin(phi/2.0);
	u=cos(theta/2.0)*alglib::complex(cp, sp);
	v=sin(theta/2.0)*alglib::complex(cp, -sp);
}
inline alglib::complex_1d_array cd2al(vector<complex<double> > c) {
	int n=c.size(); alglib::complex_1d_array a; a.setlength(n); for(int i=0;i<n;i++) a[i]=cd2al(c[i]);  return a;
}
inline void updatePM(int N, alglib::complex_2d_array& PfM, alglib::complex_1d_array uv, alglib::complex newu, alglib::complex newv, int r) {
	for(int i=0;i<r;i+=2) PfM[r/2][i/2]=1.0/(newu*uv[i+1]-uv[i]*newv);
	for(int i=r+2;i<2*N;i+=2) PfM[r/2][i/2]=1.0/(newu*uv[i+1]-uv[i]*newv);	
	for(int i=0;i<r;i+=2) PfM[i/2][r/2]=1.0/(uv[i]*newv-newu*uv[i+1]);
	for(int i=r+2;i<2*N;i+=2) PfM[i/2][r/2]=1.0/(uv[i]*newv-newu*uv[i+1]);
}
inline void al2cd(alglib::complex_1d_array a, vector<complex<double> >& c) {
	if(a.length()!=c.size()) {cerr << "ERROR: Vectors of unequal length" << endl; return;}
	for(int i=0;i<a.length();i++) c[i]=complex<double>(a[i].x, a[i].y);
}
inline void al2d(alglib::real_1d_array a, vector<double>& d) {
	if(a.length()!=d.size()) {cerr << "ERROR: Vectors of unequal length" << endl; return;}
	for(int i=0;i<a.length();i++) d[i]=a[i];
}

void timetest(vars V, CRandomSFMT& rangen) {

	// int runtime=clock(), s=50000; alglib::complex c=1, c2;
	// for(int i=1;i<=s;i++) {c.x=rangen.Random(); c.y=rangen.Random(); c2=pow(c, i);} cout << al2cd(c2) << endl;
	// cout << "Time 1: " << timeint(runtime) << endl;
	// for(int i=1;i<=s;i++) {c.x=rangen.Random(); c.y=rangen.Random(); c2=pipow(c, i);} cout << al2cd(c2) << endl;
	// cout << "Time 2: " << timeint(runtime) << endl;

}

void setupV(vars& V) {

	V.i["N"]=12; V.i["ot"]=100; V.i["oh"]=1; V.i["tk"]=1; V.i["sd"]=0; V.i["p"]=2; V.i["sr"]=-1;
	V.ull["os"]=1000; V.b["ra"]=false; V.b["np"]=false; V.b["d"]=false;
	V.d["st"]=0.3;
	V.s["fn"]="output.dat"; V.s["pl"]="Random";
}

void showHelp() { cout << endl << "Help not configured" << endl << endl;
	// cout << endl << "Command line arguments (d=default):" << endl << endl;
	// cout << "\t-tk\t Task to perform (d=1)" << endl << "\t\t\t1: Simultaneous MC of alt. Laughlin with alternative bootstrap, save data (d)" << endl
		 // << "\t\t\t2: Simultaneous MC of alt. Laughlin with alternative bootstrap, display data" << endl
		 // << "\t\t\t3: Simultaneous MC with alternative bootstrap, save data" << endl << "\t\t\t4: Simultaneous MC with no error calculation, save data" 
		 // << endl << "\t\t\t5: Simultaneous MC with alternative bootstrap, display data"
		 // << endl << "\t\t\t6: Simultaneous MC with no error calculation, display data" << endl << "\t\t\t7: Calculate standard Laughlin energy" << endl
		 // << "\t\t\t8: Combine data from runs of task 1" << endl << "\t\t\t9: Concatenate data from folder into single file" << endl;
	// cout << "\t-N\t Number of particles (d=2)" << endl;
	// cout << "\t-os\t Number of samples (d=10^4)" << endl;
	// cout << "\t-oh\t Harvest period of simultaneous MC in factors of N (d=1)" << endl;
	// cout << "\t-it\t Thermalization of simultaneous MC in factors of N (d=10k)" << endl;
	// cout << "\t-ot\t Thermalization of electrons in factors of N (d=10k)" << endl;
	// cout << "\t-ist\t Ghost steplength in factors of pi (d -> 50% accepted)" << endl;
	// cout << "\t-st\t Electon steplength in factors of pi (d -> 50% accepted)" << endl;
	// cout << "\t-bs\t Number of bootstrap resamplings (d=100)" << endl;
	// cout << "\t-fn\t Output filename (d=\"output.dat\")" << endl;
	// cout << "\t-ifn\t Input filename (or input folder for task 7) (d=\"output.dat\")" << endl;
	// cout << "\t-sd\t Random seed; -1 -> from cpu clock (d=-1)" << endl;
	// cout << endl << "Filename template: ./MCsicoData/[acc/]MCsico_N..is..os..[oh..]_[bs../noE].dat" << endl;
	return;
}

alglib::complex_2d_array createPM(int N, alglib::complex_1d_array uv) {
	alglib::complex_2d_array m; m.setlength(N, N);
	for(int i=0;i<2*N;i+=2) {
		for(int j=0;j<i;j+=2) m[i/2][j/2]=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
		for(int j=i+2;j<2*N;j+=2) m[i/2][j/2]=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
		m[i/2][i/2]=0;
	}
	return m;
}

void setupwf(vars V, alglib::complex_1d_array uv, alglib::complex_2d_array& PfM, double& E) {
	int N=V.i["N"];
	PfM=createPM(N, uv); E=EcalcAwoR(N, uv);
}

double initWF(int N, alglib::complex_1d_array uv, alglib::complex_2d_array PfM) {
	
	alglib::complex_2d_array LUPf=PfM; alglib::integer_1d_array pivots; cmatrixlu(LUPf, N, N, pivots);
	complex<double> logdetPf=0;
	for(int i=0;i<N;i++) logdetPf+=log(LUPf[i][i]);
	return real(logdetPf);
}

/*
void n_CF_getstartconfs(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], n=V.i["n"], twoq=V.i["twoq"], zmi, accept=0, prec=V.i["pr"], ot=V.i["ot"]; 
	double rans[4], zm, twologWFold, E, step=V.d["st"];
	int maxprec=V.i["prm"]; if(maxprec<0) maxprec=getmaxprec(N); alglib::complex_1d_array uv, z; alglib::real_1d_array tp; 
	uv.setlength(2*N); tp.setlength(2*N); z.setlength(N); 
	stringstream ss; ss << "./nCFstartconfs/n" << n << "/nCFstartn" << n << "N" << N << ".dat"; ifstream sc(ss.str().c_str()); sc >> setprecision(15);
	if(V.b["ra"]) {cout << "Random starting point" << endl; rantp(N, tp, rangen); tp2uv(N, tp, uv);}
	else if(!sc || V.b["np"]) {cout << "Thermalizing with Laughlin m=3 wavefunction" << endl; if(elcoords(N, V.s["pl"], tp, uv, rangen)!=0) return;} 
	else {cout << "Starting with previously thermalized N=" << N << " configuration" << endl;
				 for(int i=0;i<2*N;i+=2) sc >> tp[i] >> tp[i+1]; tp2uv(N, tp, uv);
	} sc.close();
	alglib::complex_2d_array logno, PfM;
	alglib::complex_1d_array logvpows; vector<vector<vector<double> > > logfafr; alglib::complex loguprod;
	ofstream out(V.s["fn"].c_str(), ios::binary); out << setprecision(15); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	setupwf(V, uv, z, loguprod, logfafr, logno, logvpows, PfM, zm, zmi, E); int time=clock(); double secs;
	twologWFold=initWF(N, twoq, n, uv, PfM, logvpows, z, loguprod, logfafr, logno, prec);
	for(int i=0;i<ot;i++) { cout << "it run " << i+1 << endl;
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		n_CF_it(N, n, twoq, maxprec, uv, tp, z, rans, accept, step, zm, zmi, PfM, logvpows, loguprod, logfafr, logno, twologWFold);
	} secs=timeint(time); cout << "Thermalization time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl; 
	
	for(int i=0;i<2*N;i+=2) out << tp[i] << "    " << tp[i+1] << endl;	
		
	cout << setprecision(3) << 100.0*accept/((double)ot) << "% accepted"  << endl; 
	out.close(); //out2.close();
	
}
*/

void MR_MC(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], twop=2*V.i["p"], accept=0, ot=V.i["ot"], oh=V.i["oh"]; 
	unsigned long long os=V.ull["os"]; double rans[4], twologWFold, E, step=V.d["st"];
	alglib::complex_1d_array uv; alglib::real_1d_array tp; uv.setlength(2*N); tp.setlength(2*N); 
	stringstream ss; ss << "./Startconfs/MRstartp" << twop/2 << "N" << N << ".dat"; ifstream sc(ss.str().c_str()); sc >> setprecision(15);
	if(V.b["ra"]) {cout << "Random starting point" << endl; rantp(N, tp, rangen); tp2uv(N, tp, uv);}
	else if(!sc || V.b["np"]) {cout << "Thermalizing with Laughlin m=3 wavefunction" << endl; if(elcoords(N, V.s["pl"], tp, uv, rangen)!=0) return;} 
	else {cout << "Starting with previously thermalized N=" << N << " configuration" << endl;
				 for(int i=0;i<2*N;i+=2) sc >> tp[i] >> tp[i+1]; tp2uv(N, tp, uv);
	} sc.close();
	alglib::complex_2d_array PfM; ofstream out(V.s["fn"].c_str(), ios::binary); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	setupwf(V, uv, PfM, E); clock_t time=clock(); double secs;
	stringstream sss; sss << 1;
	twologWFold=initWF(N, uv, PfM); cout << "Thermalizing...    "; cout.flush();
	for(int i=0;i<ot;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		MR_it(N, twop, uv, tp, rans, accept, step, PfM, twologWFold);
	} 
	secs=timeint(time); cout << endl << "Thermalization time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl; 
	
	accept=0; int twoN=2*N; cout << "Sampling..."; cout.flush();
	for(unsigned long long i=0;i<os;i++) {
		for(int j=0;j<twoN;j+=2) {out.write((char *) &tp[j], sizeof(double)); out.write((char *) &tp[j+1], sizeof(double));}
		for(int j=0;j<oh;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			MR_it(N, twop, uv, tp, rans, accept, step, PfM, twologWFold);
		}
	} 
	cout << endl << "Total # samples: " << n2s(os) << endl;
	secs=timeint(time); cout << "Sampling time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl;  
	
	cout << setprecision(3) << 100.0*accept/((double)os*oh) << "% accepted" << endl; 
	out.close();
	
}

void MR_it(int N, int twop, alglib::complex_1d_array& uv, alglib::real_1d_array& tp, double rans[], int& accept, double step, 
			alglib::complex_2d_array& PfM, double& twologWFold) {

	int r=2*floor(N*rans[0]);
	complex<double> tmp=0; double newt, newp; alglib::complex newu, newv;
	newtp(tp[r], step*(rans[1]-0.5), tp[r+1], step*(2.0*rans[2]-1.0), newt, newp);
	tp2uv(newt, newp, newu, newv); alglib::complex_2d_array PfM2=PfM;
	
	for(int i=0;i<r;i+=2) tmp+=log(uv[i]*newv-newu*uv[i+1])-log(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
	for(int i=r+2;i<2*N;i+=2) tmp+=log(uv[i]*newv-newu*uv[i+1])-log(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
	
	updatePM(N, PfM2, uv, newu, newv, r);
	alglib::complex_2d_array LUPf=PfM2; alglib::integer_1d_array pivots; cmatrixlu(LUPf, N, N, pivots); complex<double> logdetPf=0;
	for(int i=0;i<N;i++) logdetPf+=log(LUPf[i][i]);
	
	double twologWFnew=real(logdetPf);
	
	// Testing
	// out << endl <<"tp1={"; for(int i=0;i<2*N-2;i+=2) out << "{" << tp[i] << " , " << tp[i+1] << "}," << endl; 
	// out << "{" << tp[2*N-2] << " , " << tp[2*N-1] << "}};";
	// out << endl <<"tp2={"; for(int i=0;i<2*N-2;i+=2) out << "{" << tp2[i] << " , " << tp2[i+1] << "}," << endl;
	// out << "{" << tp2[2*N-2] << " , " << tp2[2*N-1] << "}};" << endl;
	// out << logdetCF << "    " << logdetPf << "    " << 6.0*real(tmp) << "    " << twologWFold << "    " << exp(twologWFnew-twologWFold+6.0*real(tmp)) << endl;
	
	if(exp(twologWFnew-twologWFold+twop*real(tmp))*sin(newt)/sin(tp[r])>rans[3]) {
		PfM=PfM2; twologWFold=twologWFnew;
		tp[r]=newt; tp[r+1]=newp;
		uv[r]=newu; uv[r+1]=newv;
		accept++;
	}
	
	return;
}