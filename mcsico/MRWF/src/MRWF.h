using namespace std;
#include "../../general/src/MCFSutility2.h"

void MR_it(int, int, alglib::complex_1d_array&, alglib::real_1d_array&, double[], int&, double, alglib::complex_2d_array&, double&);
void MR_MC(vars, CRandomSFMT&);
double initWF(int, alglib::complex_1d_array, alglib::complex_2d_array);
void setupwf(vars, alglib::complex_1d_array, alglib::complex_2d_array&, double&);
alglib::complex_2d_array createPM(int, alglib::complex_1d_array);
void showHelp();
void setupV(vars&);
void timetest(vars, CRandomSFMT&);

class getstep{

	private:
	vector<int> nos;

	public:
	int **Ns; double **steps;

	 getstep() {
		Ns=new int*[2]; steps=new double*[2];

		// p=1
		nos.push_back(2); Ns[0]=new int[nos[0]]; steps[0]=new double[nos[0]];
		Ns[0][0]=20; Ns[0][1]=30;
		steps[0][0]=0.22; steps[0][0]=0.17;

		// p=2
		nos.push_back(5); Ns[1]=new int[nos[1]]; steps[1]=new double[nos[1]];
		Ns[1][0]=10; Ns[1][1]=12; Ns[1][2]=16; Ns[1][3]=20; Ns[1][4]=30;
		steps[1][0]=0.3; steps[1][1]=0.28; steps[1][2]=0.24; steps[1][3]=0.22; steps[1][4]=0.17;
	}

	~getstep() {
		delete Ns[0]; delete Ns[1]; delete steps[0]; delete steps[1];
		delete [] Ns; delete [] steps;
	}

	int checkN(int N, int p) {
		p-=1; int found=-1;
		for(int i=0;i<nos[p];i++) {if(Ns[p][i]==N) {found=i; break;}}
		return found;
	}

};