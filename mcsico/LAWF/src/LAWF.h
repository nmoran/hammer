using namespace std;
#include "../../general/src/MCFSutility2.h"

void LA_it(int, int, vector<complex<double> >&, vector<double>&, double[], int&, double);
void LA_MC(vars, CRandomSFMT&);
void showHelp();
void setupV(vars&);
void timetest(vars, CRandomSFMT&);