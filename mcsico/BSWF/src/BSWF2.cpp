#include "BSWF2.h"

// ./BSWF2 -N 10 -n 2 -ot 1000 -os 35000 -fn ./MCData/BS/n2/N10/configs/BS_N10os35k_1.dat 2> /dev/null > MCData/BS/n2/N10/rep/N10os25k_1.txt

void showHelp() {}

int main(int argc, const char* argv[]) {

	vars V; setupV(V); int starttime=clock(), argr=argin(argc, argv, V);
	if(argr==1) return 0; else if(argr==-1) {showHelp(); return 0;}
	if(V.i["sr"]!=-1) {
		if(strcmp(V.s["fn"].c_str(),"output.dat")!=0 || V.i["sd"]!=0) {cerr << "ERROR: Parameters '-fn' and '-sd' will be ignored when '-sr' is in use" << endl; return 0;}
		stringstream ss; ss << "n" << V.i["n"] << "/N" << V.i["N"] << "/configs/N" << V.i["N"] 
							<< "os" << n2s(V.ull["os"]) << "_" << V.i["sr"] << ".dat"; 
		V.s["fn"]=ss.str(); V.i["sd"]=V.i["sr"];
	}
	V.i["sd"]+=time(0); CRandomSFMT rangen(V.i["sd"]);
	//getstep gs; int nstep=gs.checkN(V.i["N"], V.i["n"]);if(true) {if(!different(V.d["st"], 0.35, 6)) {if(nstep==-1) {cout << "ERROR: step not found or supplied" << endl; return 0;}else {V.d["st"]=gs.steps[V.i["n"]-2][nstep];}} V.d["st"]*=pi;}
	
	disp(V); e(); cout << endl <<  "Program started " << timestring() << endl << "-----------------------------------------" << endl << endl;
	
	int tk=V.i["tk"]; 
	if(tk==1) BS_MC2(V, rangen);
	else if(tk==2) getstartconfs(V, rangen);
	else {stringstream ss; ss << tk; WE(2, ss.str()); return 0;}
	
	double sec=((double)clock()-starttime)/CLOCKS_PER_SEC; cout << setprecision(4) << endl;
	cout << "-----------------------------------------" << endl << "Total Runtime: " << sec << "s = " << sec/60.0 << "min" << endl;
	cout << endl <<  "Program finished " << timestring() << endl;
	return 0;
}

inline double abs(alglib::complex a) {return sqrt(a.x*a.x+a.y*a.y);}
inline alglib::complex exp(alglib::complex a) {return exp(a.x)*alglib::complex(cos(a.y), sin(a.y));}
inline alglib::complex pow(alglib::complex a, int n) {complex<double> c=complex<double>(a.x, a.y); return alglib::complex(real(pow(c, n)), imag(pow(c, n)));}
inline alglib::complex loga(alglib::complex a) {complex<double> c=complex<double>(a.x, a.y); return alglib::complex(real(log(c)), imag(log(c)));}
inline void newtp(double theta, double addt, double phi, double addp, double& newt, double& newp) {
	newt=theta+addt;
	if(newt<0) {newt=-newt; phi=fmod(phi+pi,twopi); }
	else if(newt>pi) {newt=twopi-newt; phi=fmod(phi+pi,twopi); }
	newp=fmod(phi+addp, twopi);
	if(newp<0) newp+=twopi;
	
	return;
}
inline complex<double> log(alglib::complex a) {return log(complex<double>(a.x, a.y));}
inline alglib::complex cd2al(complex<double> c) {return alglib::complex(real(c), imag(c));}
inline void tp2uv(double theta, double phi, alglib::complex& u, alglib::complex& v) {
	double cp=cos(phi/2.0), sp=sin(phi/2.0);
	u=cos(theta/2.0)*alglib::complex(cp, sp);
	v=sin(theta/2.0)*alglib::complex(cp, -sp);
}
inline alglib::complex_1d_array cd2al(vector<complex<double> > c) {
	int n=c.size(); alglib::complex_1d_array a; a.setlength(n); for(int i=0;i<n;i++) a[i]=cd2al(c[i]);  return a;
}
inline void updatePM(int N, alglib::complex_2d_array& PfM, alglib::complex_1d_array uv, alglib::complex newu, alglib::complex newv, int r) {
	for(int i=0;i<r;i+=2) PfM[r/2][i/2]=1.0/(newu*uv[i+1]-uv[i]*newv);
	for(int i=r+2;i<2*N;i+=2) PfM[r/2][i/2]=1.0/(newu*uv[i+1]-uv[i]*newv);	
	for(int i=0;i<r;i+=2) PfM[i/2][r/2]=1.0/(uv[i]*newv-newu*uv[i+1]);
	for(int i=r+2;i<2*N;i+=2) PfM[i/2][r/2]=1.0/(uv[i]*newv-newu*uv[i+1]);
}
inline void al2cd(alglib::complex_1d_array a, vector<complex<double> >& c) {
	if(a.length()!=c.size()) {cerr << "ERROR: Vectors of unequal length" << endl; return;}
	for(int i=0;i<a.length();i++) c[i]=complex<double>(a[i].x, a[i].y);
}
inline void al2d(alglib::real_1d_array a, vector<double>& d) {
	if(a.length()!=d.size()) {cerr << "ERROR: Vectors of unequal length" << endl; return;}
	for(int i=0;i<a.length();i++) d[i]=a[i];
}

alglib::complex_2d_array setupCFmat2(int N, int n, int twoq, alglib::complex_1d_array uv, 
									vector<vector<vector<double> > > logfafr, alglib::complex_2d_array logno) {
						
	alglib::complex_2d_array logesp=getlogesp2(N, uv); alglib::complex_2d_array CFmat; CFmat.setlength(N, N); int Nm1=N-1;
	for(int i=0;i<N;i++) { // rows; particle i
		int state=0; // state dep. on nu and m
		for(int nu=0;nu<n;nu++) { // "Internal" LL
			for(int mpq=-nu;mpq<=twoq+nu;mpq++) { // m+|q|
				CFmat[i][state]=0; // initialize state (i, state)
				for(int s=0;s<=nu;s++) {
					if(s>=-mpq && s<=nu+twoq-mpq) { // only s-terms where binomial survives
						int om=mpq+s; alglib::complex tsum=0.0; // initialize t-sum
						for(int t=om;t<=Nm1+om-nu-twoq;t++) {
							//if(t&1) tsum-=exp(logno[nu][mpq+nu]+z2al(logesp(i, t))+logfafr[nu][om][t-om]+logvpows[i]+(mpq-t)*loga(z[i]));
							if(t&1) tsum-=exp(logno[nu][mpq+nu]+logesp(i, t)+logfafr[nu][om][t-om]+(Nm1+mpq-twoq-t)*loga(uv[2*i+1])+(t-mpq)*loga(uv[2*i]));
							//else tsum+=exp(logno[nu][mpq+nu]+z2al(logesp(i, t))+logfafr[nu][om][t-om]+logvpows[i]+(mpq-t)*loga(z[i]));
							else tsum+=exp(logno[nu][mpq+nu]+logesp(i, t)+logfafr[nu][om][t-om]+(Nm1+mpq-twoq-t)*loga(uv[2*i+1])+(t-mpq)*loga(uv[2*i]));
						} // t end
						if(s&1) CFmat[i][state]-=bin(nu, s)*tsum;
						else CFmat[i][state]+=bin(nu, s)*tsum;
					} // s end (w.r.t. if test)
				}
				state++;
			} // m end (w.r.t. |q|+m
		} // nu end
	} // i end
	//ee(); 
	return CFmat;
}


alglib::complex_2d_array getlogesp2(int N, alglib::complex_1d_array uv) {
	alglib::complex_2d_array *f=new alglib::complex_2d_array[N], logesp; logesp.setlength(N, N); for(int i=0;i<N;i++) f[i].setlength(N, N+1);
	for(int i=0;i<N;i++) {
		for(int t=0;t<N;t++) {
			if(t==0) f[i][t][0]=1;
			else f[i][t][0]=0;
			for(int n=1;n<=N;n++) {
				if(n-1==i) f[i][t][n]=f[i][t][n-1];
				else if(t==0) f[i][t][n]=f[i][t][n-1]*uv[2*n-2]; 
				else f[i][t][n]=f[i][t][n-1]*uv[2*n-2]+f[i][t-1][n-1]*uv[2*n-1];
			}
			logesp[i][t]=loga(f[i][t][N]);
		}
	}
	delete [] f;
	return logesp;
}

vector<vector<vector<double> > > getlogfafr(int N, int n, int twoq) {
	vector<vector<vector<double> > > logfafr(n, vector<vector<double> >(twoq+n, vector<double>(N-twoq)));
	for(int nu=0;nu<n;nu++) {
		for(int om=0;om<=twoq+nu;om++) {
			for(int tmo=0;tmo<N-nu-twoq;tmo++) {
				logfafr[nu][om][tmo]=log(bin(twoq+nu, om));
				for(int i=N-tmo-nu-twoq;i<=N-1-tmo-om;i++) logfafr[nu][om][tmo]+=log(i);
				for(int i=tmo+1;i<=tmo+om;i++) logfafr[nu][om][tmo]+=log(i);
			}
		}
	}
	return logfafr;
}

alglib::complex_2d_array lognrm(int N, int n, int twoq, int twoSnCF, int p) {
	alglib::complex_2d_array logno; logno.setlength(n, twoq+2*n-1); double q=twoq/2.0;
	for(int nu=0;nu<n;nu++) {
		for(double m=-twoq/2.0-nu;m<=twoq/2.0+nu;m++) {
			int i=(2.0*m+twoq)/2.0+nu;
			logno[nu][i]=loga(alglib::complex(facdivfac(twoSnCF+1, 2*p*(N-1)+nu+1)*(1.0-2.0*(nu%2)), 0))
						+0.5*loga(alglib::complex((twoq+2.0*nu+1.0)*facdivfac((int)(q-m)+nu, nu)*facdivfac((int)(q+m)+nu, twoq+nu)/(4.0*pi), 0));
		}
	}
	return logno;
}

void setupV(vars& V) {

	V.i["N"]=6; V.i["ot"]=100; V.i["oh"]=1; V.i["tk"]=1; V.i["sd"]=0; V.i["n"]=2; 
	V.i["p"]=1; V.i["sr"]=-1; V.d["st"]=-1;
	V.ull["os"]=100; V.b["ra"]=false; V.b["np"]=false;
	V.s["fn"]="output.dat"; V.s["pl"]="Random";
}


alglib::complex_2d_array createPM(int N, alglib::complex_1d_array uv) {
	alglib::complex_2d_array m; m.setlength(N, N);
	for(int i=0;i<2*N;i+=2) {
		for(int j=0;j<i;j+=2) m[i/2][j/2]=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
		for(int j=i+2;j<2*N;j+=2) m[i/2][j/2]=1.0/(uv[i]*uv[j+1]-uv[j]*uv[i+1]);
		m[i/2][i/2]=0;
	}
	return m;
}
void setupwf2(vars V, alglib::complex_1d_array uv, vector<vector<vector<double> > >& logfafr, 
		alglib::complex_2d_array& logno, alglib::complex_2d_array& PfM, double& E) {

	int N=V.i["N"], twoq=V.i["twoq"], n=V.i["n"];
	logfafr=getlogfafr(N, n, twoq); logno=lognrm(N, n, twoq, V.i["twoSnCF"], 1);
	PfM=createPM(N, uv); E=EcalcAwoR(N, uv);
}
double initWF2(int N, int twoq, int n, alglib::complex_1d_array uv, alglib::complex_2d_array PfM, vector<vector<vector<double> > > logfafr, alglib::complex_2d_array logno) {
	
	alglib::complex_2d_array CFmat=setupCFmat2(N, n, twoq, uv, logfafr, logno);
	alglib::complex_2d_array LUPf=PfM, LUCF=CFmat; alglib::integer_1d_array pivots; cmatrixlu(LUPf, N, N, pivots); cmatrixlu(LUCF, N, N, pivots);
	complex<double> logdetPf=0, logdetCF=0;
	for(int i=0;i<N;i++) {logdetPf+=log(LUPf[i][i]); logdetCF+=log(LUCF(i,i));}
	return real(logdetPf)+2.0*real(logdetCF);
}

void getstartconfs(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], n=V.i["n"], twoq=V.i["twoq"], accept=0, prec=V.i["pr"], ot=V.i["ot"], oh=V.i["oh"], twoSnCF=V.i["twoSnCF"]; 
	unsigned long long os=V.ull["os"];
	double rans[4], twologWFold, E, step;
	if(n==2) step=pi*(pow(N, -0.8)+0.083);
	else if(n==3) step=pi*(pow(N, -0.83)+0.084);
	else if (V.d["st"]>0) step=pi*V.d["st"];
	else {cerr << "ERROR: Step not found for n = " << n << endl; return;}
	alglib::complex_1d_array uv; alglib::real_1d_array tp; 
	uv.setlength(2*N); tp.setlength(2*N);
	stringstream ss; ss << "./Startconfs/BSstartn" << n << "N" << N << ".dat"; ifstream in(ss.str().c_str()); in >> setprecision(15);
	if(strcmp(V.s["pl"].c_str(),"Random")!=0 && in) {
		cout << "Starting with previously thermalized N=" << N << " configuration" << endl;
		for(int i=0;i<2*N;i+=2) in >> tp[i] >> tp[i+1]; tp2uv(N, tp, uv);
	}
	else if(V.b["ra"]) {cout << "Random starting point" << endl; rantp(N, tp, rangen); tp2uv(N, tp, uv);}
	else {cout << "Thermalizing with Laughlin m=3 wavefunction" << endl; if(elcoords(N, V.s["pl"], tp, uv, rangen)!=0) return;} 

	alglib::complex_2d_array logno, PfM;
	vector<vector<vector<double> > > logfafr;
	ofstream out(ss.str().c_str()); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	setupwf2(V, uv, logfafr, logno, PfM, E);
	stringstream sss, ssr; sss << 1; clock_t time=clock();
	twologWFold=initWF2(N, twoq, n, uv, PfM, logfafr, logno); cout << "Thermalizing...    "; cout.flush();
	for(int i=0;i<ot;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		BS_it2(N, n, twoq, uv, tp, rans, accept, step, PfM, logfafr, logno, twologWFold);
	}
	double secs=timeint(time); cout << endl << "Thermalization time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl; 
	for(int i=0;i<2*N;i+=2) out << tp[i] << "     " << tp[i+1] << endl;
	cout << setprecision(3) << 100.0*accept/((double)ot) << "% accepted" << endl; 
	out.close();
	
}

void BS_MC2(vars V, CRandomSFMT& rangen) {

	int N=V.i["N"], n=V.i["n"], twoq=V.i["twoq"], zmi, accept=0, prec=V.i["pr"], ot=V.i["ot"], oh=V.i["oh"], twoSnCF=V.i["twoSnCF"]; 
	unsigned long long os=V.ull["os"];
	double rans[4], twologWFold, E, step;
	if(n==2) step=pi*(pow(N, -0.8)+0.083);
	else if(n==3) step=pi*(pow(N, -0.83)+0.084);
	else if (V.d["st"]>0) step=pi*V.d["st"];
	else {cerr << "ERROR: Step not found for n = " << n << endl; return;}
	alglib::complex_1d_array uv; alglib::real_1d_array tp; 
	uv.setlength(2*N); tp.setlength(2*N);
	stringstream ss; ss << "./Startconfs/BSstartn" << n << "N" << N << ".dat"; ifstream sc(ss.str().c_str()); sc >> setprecision(15);
	if(V.b["ra"]) {cout << "Random starting point" << endl; rantp(N, tp, rangen); tp2uv(N, tp, uv);}
	else if(!sc || V.b["np"]) {cout << "Thermalizing with Laughlin m=3 wavefunction" << endl; if(elcoords(N, V.s["pl"], tp, uv, rangen)!=0) return;} 
	else {cout << "Starting with previously thermalized N=" << N << " configuration" << endl;
				 for(int i=0;i<2*N;i+=2) sc >> tp[i] >> tp[i+1]; tp2uv(N, tp, uv);
	} sc.close();
	alglib::complex_2d_array logno, PfM;
	vector<vector<vector<double> > > logfafr;
	ofstream out(V.s["fn"].c_str(), ios::binary); if(!out) {cerr << "ERROR: Folder not found" << endl; return;}
	
	setupwf2(V, uv, logfafr, logno, PfM, E);
	stringstream sss, ssr; sss << 1; clock_t time=clock();
	twologWFold=initWF2(N, twoq, n, uv, PfM, logfafr, logno); cout << "Thermalizing...    "; cout.flush();
	for(int i=0;i<ot;i++) {
		rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
		BS_it2(N, n, twoq, uv, tp, rans, accept, step, PfM, logfafr, logno, twologWFold);
	}
	double secs=timeint(time); cout << endl << "Thermalization time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl; 
	
	accept=0; int twoN=2*N; ssr.str(""); ssr << 0; cout << "Sampling...    "; cout.flush();
	for(unsigned long long i=0;i<os;i++) {
		for(int j=0;j<twoN;j+=2) {out.write((char *) &tp[j], sizeof(double)); out.write((char *) &tp[j+1], sizeof(double));}
		for(int j=0;j<oh;j++) {
			rans[0]=rangen.Random(); rans[1]=rangen.Random(); rans[2]=rangen.Random(); rans[3]=rangen.Random();
			BS_it2(N, n, twoq, uv, tp, rans, accept, step, PfM, logfafr, logno, twologWFold);
		}
	}
	cout << endl << "Total # samples: " << os;
	secs=timeint(time); cout << endl << "Sampling time: " << setprecision(3) << secs << "s = " << secs/60.0 << "min" << endl;  
	
	cout << setprecision(3) << 100.0*accept/((double)os*oh) << "% accepted" << endl; 
	out.close();
	
}

void BS_it2(int N, int n, int twoq, alglib::complex_1d_array& uv, alglib::real_1d_array& tp,
		double rans[], int& accept, double step, alglib::complex_2d_array& PfM, vector<vector<vector<double> > > logfafr, alglib::complex_2d_array logno, double& twologWFold) {

	int r=2*floor(N*rans[0]);
	complex<double> tmp=0; alglib::complex_1d_array uv2=uv; alglib::real_1d_array tp2=tp;
	newtp(tp[r], step*(rans[1]-0.5), tp[r+1], step*(2.0*rans[2]-1.0), tp2[r], tp2[r+1]);
	tp2uv(tp2[r], tp2[r+1], uv2[r], uv2[r+1]);
	alglib::complex_2d_array PfM2;
	
	for(int i=0;i<r;i+=2) tmp+=log(uv[i]*uv2[r+1]-uv2[r]*uv[i+1])-log(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
	for(int i=r+2;i<2*N;i+=2) tmp+=log(uv[i]*uv2[r+1]-uv2[r]*uv[i+1])-log(uv[i]*uv[r+1]-uv[r]*uv[i+1]);
	
	PfM2=PfM; updatePM(N, PfM2, uv, uv2[r], uv2[r+1], r);
		
	alglib::complex_2d_array CFmat=setupCFmat2(N, n, twoq, uv2, logfafr, logno);
	alglib::complex_2d_array LUPf=PfM2, LUCF=CFmat; alglib::integer_1d_array pivots; 
	cmatrixlu(LUPf, N, N, pivots); cmatrixlu(LUCF, N, N, pivots); complex<double> logdetPf=0, logdetCF=0;
	for(int i=0;i<N;i++) {logdetPf+=log(LUPf[i][i]); logdetCF+=log(LUCF[i][i]);}
	
	double twologWFnew=real(logdetPf)+2.0*real(logdetCF);
	
	// Testing
	// out << endl <<"tp1={"; for(int i=0;i<2*N-2;i+=2) out << "{" << tp[i] << " , " << tp[i+1] << "}," << endl; 
	// out << "{" << tp[2*N-2] << " , " << tp[2*N-1] << "}};";
	// out << endl <<"tp2={"; for(int i=0;i<2*N-2;i+=2) out << "{" << tp2[i] << " , " << tp2[i+1] << "}," << endl;
	// out << "{" << tp2[2*N-2] << " , " << tp2[2*N-1] << "}};" << endl;
	// out << logdetCF << "    " << logdetPf << "    " << 6.0*real(tmp) << "    " << twologWFold << "    " << exp(twologWFnew-twologWFold+6.0*real(tmp)) << endl;
	
	if(exp(twologWFnew-twologWFold+2.0*real(tmp))*sin(tp2[r])/sin(tp[r])>rans[3]) {
		PfM=PfM2; twologWFold=twologWFnew;
		tp=tp2; uv=uv2;
		accept++;
	}
	
	return;
}