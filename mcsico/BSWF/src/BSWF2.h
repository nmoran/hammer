using namespace std;
#include "../../general/src/MCFSutility2.h"

void BS_MC2(vars, CRandomSFMT&);
void getstartconfs(vars, CRandomSFMT&);
void getstartconfs(vars, CRandomSFMT&);
alglib::complex_2d_array createPM(int, alglib::complex_1d_array);
void BS_it2(int, int, int, alglib::complex_1d_array&, alglib::real_1d_array&,
		double[], int&, double, alglib::complex_2d_array&, vector<vector<vector<double> > >, alglib::complex_2d_array, double&);
alglib::complex_2d_array setupCFmat2(int, int, int, alglib::complex_1d_array, vector<vector<vector<double> > >, alglib::complex_2d_array);
void setupV(vars&);
alglib::complex_2d_array getlogesp2(int, alglib::complex_1d_array);
vector<vector<vector<double> > > getlogfafr(int, int, int);
alglib::complex_2d_array lognrm(int, int, int, int, int);
inline void updatePM(int, alglib::complex_2d_array&, alglib::complex_1d_array uv, alglib::complex, alglib::complex, int);

class getstep{

	private:
	vector<int> nos;

	public:
	int **Ns; double **steps;

	 getstep() {
		Ns=new int*[3]; steps=new double*[3];
	 
		nos.push_back(10); Ns[0]=new int[nos[0]]; steps[0]=new double[nos[0]];
		Ns[0][0]=6; Ns[0][1]=8; Ns[0][2]=10; Ns[0][3]=12; Ns[0][4]=14; Ns[0][5]=16; Ns[0][6]=20; Ns[0][7]=24; Ns[0][8]=28; Ns[0][9]=32; Ns[0][10]=36;
		steps[0][0]=0.35; steps[0][1]=0.3; steps[0][2]=0.26; steps[0][3]=0.23; steps[0][4]=0.22; steps[0][5]=0.19; steps[0][6]=0.175; steps[0][7]=0.16;
		steps[0][8]=0.155; steps[0][9]=0.14;
	 
		nos.push_back(11); Ns[1]=new int[nos[1]]; steps[1]=new double[nos[1]];
		Ns[1][0]=12; Ns[1][1]=18; Ns[1][2]=24; Ns[1][3]=30; Ns[1][4]=36; Ns[1][5]=42; Ns[1][6]=48; Ns[1][7]=54; Ns[1][8]=60; Ns[1][9]=66; Ns[1][10]=72;
		steps[1][0]=0.17; steps[1][1]=0.135;  steps[1][2]=0.12; steps[1][3]=0.11; steps[1][4]=0.097; steps[1][5]=0.09; steps[1][6]=0.083; steps[1][7]=0.079;
		steps[1][8]=0.077; steps[1][9]=0.0725; steps[1][10]=0.069;
		
		nos.push_back(1); Ns[2]=new int[nos[2]]; steps[2]=new double[nos[2]];
		Ns[2][0]=20;
		steps[2][0]=0.137;
	}
	
	~getstep() {
		delete Ns[0]; delete Ns[1]; delete Ns[2]; delete steps[0]; delete steps[1]; delete steps[2];
		delete [] Ns; delete [] steps;
	}

	int checkN(int N, int n) {
		n-=2; int found=-1;
		for(int i=0;i<nos[n];i++) {if(Ns[n][i]==N) {found=i; break;}}
		return found;
	}

};