#!/bin/bash

### DOwloand the source files

MyDir=`pwd`

set -e 
set -u

PEVer="3.7.5"
PE4PVer="3.7.0"
SLVer="3.7.4"
SL4PVer="3.7.0"


#wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${PEVer}.tar.gz -O petsc-${PEVer}.tar.gz
#wget https://bitbucket.org/petsc/petsc4py/downloads/petsc4py-${PE4PVer}.tar.gz --no-check-certificate -O petsc4py-${PE4PVer}.tar.gz
#wget http://slepc.upv.es/download/distrib/slepc-${SLVer}.tar.gz -O slepc-${SLVer}.tar.gz
#wget https://bitbucket.org/slepc/slepc4py/downloads/slepc4py-${SL4PVer}.tar.gz --no-check-certificate -O slepc4py-${SL4PVer}.tar.gz


#tar -xzvf petsc-${PEVer}.tar.gz
#tar -xzvf petsc4py-${PE4PVer}.tar.gz
#tar -xzvf slepc-${SLVer}.tar.gz
#tar -xzvf slepc4py-${SL4PVer}.tar.gz

cd petsc-${PEVer}
export PETSC_DIR=`pwd`
export PETSC_ARCH=linux-gnu-cxx-complex
./configure --with-clanguage=C++ --with-cxx-support=1 --with-gcov=1 --with-scalar-type=complex --with-shared-libraries=1 --with-debugging=0 --with-scientific-python=1
make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH all
make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH test


cd $MyDir

cd slepc-${SLVer}
./configure
export SLEPC_DIR=`pwd`
make SLEPC_DIR=$SLEPC_DIR PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH
make SLEPC_DIR=$SLEPC_DIR PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH test


cd $MyDir
cd petsc4py-${PE4PVer}
python setup.py build
PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH python setup.py install


cd $MyDir
cd slepc4py-${SL4PVer}
wget https://bitbucket.org/slepc/slepc4py/raw/a93f720cf03e48aa9bb84283d7b2f5423d0b6cfa/src/SLEPc/BV.pyx  --no-check-certificate -O BV_new.pyx
cp -v BV_new.pyx src/SLEPc/BV.pyx
rm src/slepc4py.SLEPc.c
python setup.py build
PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH  SLEPC_DIR=$SLEPC_DIR python setup.py install




