#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
The matematics beind Importance sampling
\end_layout

\begin_layout Standard
The fundamental formula that we are using is the relationship
\begin_inset Formula 
\begin{equation}
\left\langle f\right\rangle _{p}=\int dx\, f\left(x\right)\, p\left(x\right)=\frac{1}{N}\sum_{i=1}^{N}f\left(x_{i}^{\left(p\right)}\right)\label{eq:Fundamental}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Here we assume that 
\begin_inset Formula $p\left(x\right)$
\end_inset

 is a normalized probability distribution such that 
\begin_inset Formula $\int dx\, p\left(x\right)=1$
\end_inset

 and 
\begin_inset Formula $p\left(x\right)\ge0$
\end_inset

.
 We also assume that 
\begin_inset Formula $x_{i}^{\left(p\right)}$
\end_inset

 are drawn from the distribution 
\begin_inset Formula $p\left(x\right)$
\end_inset

.
 Note that we are calculating the expectation value of 
\begin_inset Formula $f$
\end_inset

 with respect to the distribution 
\begin_inset Formula $p$
\end_inset

.
 Not that we do not need to know the normalization of 
\begin_inset Formula $p$
\end_inset

 in order to compute 
\begin_inset Formula $\left\langle f\right\rangle _{p}$
\end_inset

 using importance sampling.
 It is sufficient to be able to draw 
\begin_inset Formula $x_{i}^{\left(p\right)}$
\end_inset

 from the distribution 
\begin_inset Formula $p$
\end_inset

, which may be done with the Metropolis-Hasting algorithm.
\end_layout

\begin_layout Standard
Sometimes is is hard to draw the distribution 
\begin_inset Formula $p$
\end_inset

 itself.
 But another 
\begin_inset Formula $q$
\end_inset

 might be simpler.
 We may therefore rewrite 
\begin_inset Formula $\left\langle f\right\rangle _{p}$
\end_inset

 as 
\begin_inset Formula 
\begin{equation}
\left\langle f\right\rangle _{p}=\int dx\, f\left(x\right)\,\frac{p\left(x\right)}{q\left(x\right)}\, q\left(x\right)=\frac{1}{N}\sum_{i=1}^{N}f\left(x_{i}^{\left(q\right)}\right)\frac{p\left(x_{i}^{\left(q\right)}\right)}{q\left(x_{i}^{\left(q\right)}\right)}\label{eq:f_of_p_of_q}
\end{equation}

\end_inset

 This may also be interpreted as 
\begin_inset Formula $\left\langle f\right\rangle _{p}=\left\langle \frac{f\, p}{q}\right\rangle _{q}$
\end_inset

 if it is more convenient.
 A problem with the new form is that we do not necessarily know the normalizatio
n of neither 
\begin_inset Formula $p$
\end_inset

 nor 
\begin_inset Formula $q$
\end_inset

.
 Thus we only know 
\begin_inset Formula $\tilde{p}$
\end_inset

 and 
\begin_inset Formula $\tilde{q}$
\end_inset

 such that 
\begin_inset Formula $p=\mathcal{N}_{p}\tilde{p}$
\end_inset

 and 
\begin_inset Formula $q=\mathcal{N}_{q}\tilde{q}$
\end_inset

.
 Substituting this in 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:f_of_p_of_q"

\end_inset

 we have 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\left\langle f\right\rangle _{p}=\frac{\mathcal{N}_{p}}{\mathcal{N}_{q}}\frac{1}{N}\sum_{i=1}^{N}f\left(x_{i}^{\left(q\right)}\right)\frac{\tilde{p}\left(x_{i}^{\left(q\right)}\right)}{\tilde{q}\left(x_{i}^{\left(q\right)}\right)}\label{eq:f_of_p_of_q-1}
\end{equation}

\end_inset

 where we now need to evaluate the normalizing prefactor 
\begin_inset Formula $\frac{\mathcal{N}_{p}}{\mathcal{N}_{q}}$
\end_inset

.
 We do this by noting that we may integrate 
\begin_inset Formula $p=\mathcal{N}_{p}\tilde{p}$
\end_inset

 such that 
\begin_inset Formula $\frac{1}{\mathcal{N}_{p}}=\int dx\,\tilde{p}\left(x\right)$
\end_inset

.
 As we have a sample of 
\begin_inset Formula $x$
\end_inset

 in the distribution 
\begin_inset Formula $q$
\end_inset

 we may write 
\begin_inset Formula 
\[
\frac{1}{\mathcal{N}_{p}}=\int dx\,\tilde{p}\left(x\right)=\int dx\,\frac{\tilde{p}\left(x\right)}{q\left(x\right)}q\left(x\right)=\frac{1}{\mathcal{N}_{q}}\int dx\,\frac{\tilde{p}\left(x\right)}{\tilde{q}\left(x\right)}q\left(x\right)=\frac{1}{\mathcal{N}_{q}}\frac{1}{N}\sum_{i=1}^{N}\frac{\tilde{p}\left(x_{i}^{\left(q\right)}\right)}{\tilde{q}\left(x_{i}^{\left(q\right)}\right)}
\]

\end_inset

 Thus we may write 
\begin_inset Formula $\left\langle f\right\rangle _{p}$
\end_inset

 as 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\left\langle f\right\rangle _{p}=\frac{1}{Z_{N}}\sum_{i=1}^{N}f\left(x_{i}^{\left(q\right)}\right)w\left(x_{i}^{\left(q\right)}\right)\label{eq:f_of_p_of_q-1-1}
\end{equation}

\end_inset

 where 
\begin_inset Formula 
\begin{equation}
Z_{N}=\sum_{i=1}^{N}w\left(x_{i}^{\left(q\right)}\right)\label{eq:Z_N_generic}
\end{equation}

\end_inset

 and 
\begin_inset Formula $w\left(x\right)=\frac{\tilde{p}\left(x\right)}{\tilde{q}\left(x\right)}$
\end_inset

.
 
\end_layout

\begin_layout Standard
Finally we may assume that 
\begin_inset Formula $p$
\end_inset

 is homogeneous such that 
\begin_inset Formula $\tilde{p}=1$
\end_inset

.
 This amount to just calculating the mean value of 
\begin_inset Formula $f$
\end_inset

 over the volume 
\begin_inset Formula $V$
\end_inset

.
 The corresponding equations a re then 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\left\langle f\right\rangle =\frac{1}{Z_{N}}\sum_{i=1}^{N}\frac{f\left(x_{i}^{\left(q\right)}\right)}{q\left(x_{i}^{\left(q\right)}\right)}\label{eq:f_of_p_of_q-1-1-1}
\end{equation}

\end_inset

 where 
\begin_inset Formula 
\begin{equation}
Z_{N}=\sum_{i=1}^{N}\frac{1}{q\left(x_{i}^{\left(q\right)}\right)}\label{eq:Z_N_generic-1}
\end{equation}

\end_inset


\end_layout

\end_body
\end_document
