#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 3cm
\rightmargin 3cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Implementation of the primary correlator
\end_layout

\begin_layout Date
Last revision: 18/4-2013
\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\i}{\imath}
{\imath}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\ellipticgeneralized}[4]{\vartheta\!\left[\begin{array}{c}
 #1\\
#2 
\end{array}\right]\!\left(#3\middle|#4\right)}
{\vartheta\left[\begin{array}{c}
#1\\
#2
\end{array}\right]\left(#3|#4\right)}
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\elliptic}[3]{\vartheta_{#1}\!\left(#2\middle|#3\right)}
{\vartheta_{#1}\left(#2|#3\right)}
\end_inset


\end_layout

\begin_layout Section
The many particle state
\end_layout

\begin_layout Standard
In this particular application we are interested in the 
\begin_inset Formula $\nu=\frac{2}{5}$
\end_inset

 wave function.
 We have 
\begin_inset Formula $N_{s}=5N$
\end_inset

 sites and 
\begin_inset Formula $N_{e}=2N$
\end_inset

 electrons.
\end_layout

\begin_layout Standard
The primary operator (in the program) is divided into thee parts 
\begin_inset Formula 
\[
\psi_{s}=\psi_{gaussian}\psi_{jastrow}\psi_{CoM}
\]

\end_inset


\end_layout

\begin_layout Standard
The Gaussian part is just 
\begin_inset Formula 
\[
\psi_{gaussian}=\exp\left[-\frac{1}{2}\sum_{j}y_{j}^{2}\right]
\]

\end_inset

 The Jastrow factor is computed to be 
\begin_inset Formula 
\begin{eqnarray*}
\psi_{jastrow} & = & \prod_{i<j\in N_{1}}\elliptic 1{\frac{\pi}{L_{x}}\left(z_{i}-z_{j}\right)}{\tau}^{3}\\
 &  & \times\prod_{i<j\in N_{2}}\elliptic 1{\frac{\pi}{L_{x}}\left(z_{i}-z_{j}\right)}{\tau}^{3}\\
 &  & \times\prod_{i\in N_{1},j\in N_{2}}\elliptic 1{\frac{\pi}{L_{x}}\left(z_{i}-z_{j}\right)}{\tau}^{2}
\end{eqnarray*}

\end_inset

 
\end_layout

\begin_layout Standard
The center of mass factor should be 
\end_layout

\begin_layout Standard
\begin_inset Box Ovalbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset Formula 
\begin{eqnarray*}
\psi_{CoM}^{\left(r,t,s\right)}\left(Z^{\left(1\right)},Z^{\left(2\right)}|\tau\right) & = & e^{\i2\pi st\frac{2}{5}}\sum_{l=0}^{3}\mathcal{G}_{2l,r,t}^{\left(1\right)}\left(Z^{\left(1\right)},\tau\right)\mathcal{G}_{5l+3s,r,t}^{\left(2\right)}\left(Z^{\left(2\right)},\tau\right)
\end{eqnarray*}

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The CoM coordinates are 
\begin_inset Formula $Z^{\left(1\right)}=3Z_{1}+2Z_{2}$
\end_inset

 and 
\begin_inset Formula $Z^{\left(2\right)}=5Z_{2}$
\end_inset

.
 Here 
\begin_inset Formula $Z_{j}=\sum_{i\in N_{j}}\frac{z_{j}}{L_{x}}$
\end_inset

.
 The factors 
\begin_inset Formula $r$
\end_inset

 and 
\begin_inset Formula $t$
\end_inset

 ensure periodic boundary conditions, in both 
\begin_inset Formula $x$
\end_inset

 and 
\begin_inset Formula $\tau x$
\end_inset

 direction.
 Thus in general 
\begin_inset Formula $r=N-1+\frac{\phi_{x}}{\pi}$
\end_inset

 and 
\begin_inset Formula $t=N-1+\frac{\phi_{y}}{\pi}$
\end_inset

.
 Here 
\begin_inset Formula $\phi_{x},\phi_{y}=0,\pi$
\end_inset

 characterize the boundary conditions.
 The two center of mass functions are 
\begin_inset Formula 
\begin{eqnarray*}
\mathcal{G}_{p_{1},r,t}^{\left(1\right)}\left(Z^{\left(1\right)},\tau\right) & = & \ellipticgeneralized{\frac{r}{2}+\frac{p_{1}}{3}}{\frac{t}{2}}{Z^{\left(1\right)}}{3\tau}\\
\mathcal{G}_{p_{2},r,t}^{\left(2\right)}\left(Z^{\left(2\right)},\tau\right) & = & \ellipticgeneralized{\frac{r}{2}+\frac{p_{2}}{15}}{\frac{t}{2}}{Z^{\left(2\right)}}{15\tau}
\end{eqnarray*}

\end_inset

 The odd factor 
\begin_inset Formula $e^{\i2\pi st\frac{2}{5}}$
\end_inset

 is present to ensure that 
\begin_inset Formula $s\in\mathbb{Z}_{5}$
\end_inset

 and 
\begin_inset Formula $r,t\in\mathbb{Z}_{2}$
\end_inset

.
 We will here focus on the properties of 
\begin_inset Formula $\psi_{CoM}^{\left(r,t,s\right)}$
\end_inset

 as it is this part that carries all the information about the 
\begin_inset Formula $r,t,s$
\end_inset

 dependence.
 The rest can for the time being be considered as a constant would be considered.
\end_layout

\begin_layout Section
The magnetic translations
\end_layout

\begin_layout Standard
The magnetic translation operators are implemented as 
\end_layout

\begin_layout Standard
\begin_inset Box Ovalbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset Formula 
\[
\Psi_{s}^{\left(n,m\right)}=t\left(x_{m}+\omega_{n}+\i y_{n}\right)\psi_{s}
\]

\end_inset


\end_layout

\end_inset

 where as usuall
\begin_inset Formula 
\[
x_{m}=\frac{L_{x}}{N_{s}}m,\qquad y_{n}=\frac{L_{y}}{N_{s}},\qquad\omega_{n}=\frac{L_{\Delta}}{N_{s}}n
\]

\end_inset

 In eplicit for the translation operator is 
\begin_inset Formula 
\begin{eqnarray*}
t\left(x_{m}+\omega_{n}+\i y_{n}\right) & = & e^{\partial_{x}\left(x_{m}+\omega_{n}\right)+y_{n}\left(\partial_{y}+\i x\right)}\\
 & = & e^{\sqrt{\frac{2\pi}{\tau_{y}N_{s}}}\left[\partial_{x}\left(m+\tau_{x}n\right)+\tau_{y}n\left(\partial_{y}+\i x\right)\right]}
\end{eqnarray*}

\end_inset

 such that 
\begin_inset Formula 
\[
\Psi_{s}^{\left(n,m\right)}\left(x,y\right)=e^{\i y_{n}\left[x+\frac{1}{2}\left(x_{m}+\omega_{n}\right)\right]}\psi_{s}\left(x+x_{m}+\omega_{n},y+y_{n}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Ofctoruse the change goes for all the variables.
 Not only a single pair of them.
\end_layout

\begin_layout Section
Transformations properties: 
\begin_inset Formula $\mathcal{T}$
\end_inset

 transformation
\end_layout

\begin_layout Standard
Some properties under the change of the modular parameter 
\begin_inset Formula $\tau$
\end_inset

 are worthy of note.
 We now consider the change 
\begin_inset Formula $\tau\rightarrow\tau+1$
\end_inset

.
 Both the center of mass factors behave in the same way: We have 
\end_layout

\begin_layout Standard
\begin_inset Box Ovalbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset Formula 
\begin{eqnarray*}
\psi_{CoM}^{\left(r,t,s\right)}\left(\tau+1\right) & = & e^{-2\pi\i\frac{s^{2}}{5}}\psi_{CoM}^{\left(r,t+r-1,s\right)}\left(\tau\right)
\end{eqnarray*}

\end_inset


\end_layout

\end_inset

 Had we also looked that the Jastrow factor we would have picked up and
 extra factor of 
\begin_inset Formula $e^{\i\pi\frac{1}{2}N}$
\end_inset

 such that the full transformation is
\end_layout

\begin_layout Standard
\begin_inset Box Ovalbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset Formula 
\begin{equation}
\psi_{s}^{\left(r,t\right)}\left(\tau+1\right)=e^{\i\pi\frac{1}{2}N}e^{-2\pi\i\frac{s^{2}}{5}}\psi_{s}^{\left(r,t+r+1\right)}\left(\tau\right)\label{eq:psi_t_transform}
\end{equation}

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Transformation properties - 
\begin_inset Formula $\mathcal{S}$
\end_inset

 transform (for general 
\begin_inset Formula $r$
\end_inset

 and 
\begin_inset Formula $t$
\end_inset

)
\end_layout

\begin_layout Standard
We now study the transformation under 
\begin_inset Formula $\mathcal{S}$
\end_inset

.
 We start by letting 
\begin_inset Formula $\tau\rightarrow-\frac{1}{\tau}$
\end_inset

.
 Under this change we get a similar response to what we had for the single
 particle case.
 We should also remember that since 
\begin_inset Formula $Z^{\left(n\right)}$
\end_inset

 contains a 
\begin_inset Formula $\frac{1}{L_{x}}$
\end_inset

 there will transform as 
\begin_inset Formula $Z^{\left(n\right)}\rightarrow\frac{Z^{\left(n\right)}}{\left|\tau\right|}$
\end_inset

.
 
\end_layout

\begin_layout Standard
Thus to summarize: The transformation properties are 
\end_layout

\begin_layout Standard
\begin_inset Box Ovalbox
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "100col%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout
\begin_inset Formula 
\begin{eqnarray}
\psi_{CoM}^{\left(r,t,s\right)}\left(Z^{\left(1\right)},Z^{\left(2\right)},-\frac{1}{\tau}\right) & = & \rho\left(\tau\right)\left(-1\right)^{rt}\sum_{s^{\prime}=1}^{5}e^{\i2\pi\frac{2}{5}s^{\prime}s}\psi_{CoM}^{\left(t,r,s^{\prime}\right)}\left(-\frac{\tau}{\left|\tau\right|}Z^{\left(1\right)},-\frac{\tau}{\left|\tau\right|}Z^{\left(2\right)},\tau\right)\label{eq:psi_s_transsform}
\end{eqnarray}

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The extra factors of 
\begin_inset Formula $Z^{\left(1\right)}$
\end_inset

 and 
\begin_inset Formula $Z^{\left(2\right)}$
\end_inset

 will cancel of against other parts in the wave functions, and against a
 gauge transform.
 We now have a slightly of dependence in the transformation on 
\begin_inset Formula $t,r$
\end_inset

 but it is the same for all 
\begin_inset Formula $s$
\end_inset

 at least.
 The factor now mentioned is 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\rho\left(\tau\right)=-\i\frac{\tau}{3}\frac{1}{\sqrt{5}}e^{\i\frac{\tau}{3}\pi\left\{ Z^{\left(1\right)}\right\} ^{2}}e^{\i\frac{\tau}{15}\pi\left\{ Z^{\left(1\right)}\right\} ^{2}}.
\]

\end_inset


\end_layout

\end_body
\end_document
