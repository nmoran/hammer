cdef extern from "HilbertSpace/FermionOnSphere.h":
     cdef cppclass FermionOnSphere:
          FermionOnSphere (int, int, int, unsigned long)
          int GetTargetHilbertSpaceDimension()

cdef extern from "Hamiltonian/AbstractQHEOnSphereHamiltonian":
     cdef cppclass ParticleOnSphereGenericHamiltonian:
          ParticleOnSphereGenericHamiltonian(ParticleOnSphere, int, int, double*, double,
                                                               )


cdef class  Py_FermionOnSphere:
     cdef FermionOnSphere *class_ptr

     def __init__(self):
         self.class_ptr = new FermionOnSphere(5,0,12,10000000)

     def get_dim(self):
         return self.class_ptr.GetTargetHilbertSpaceDimension()
