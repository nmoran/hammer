from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import Cython.Compiler.Options
import numpy

Cython.Compiler.Options.annotate = True

diagham_build_dir = '/home/nmoran/local/DiagHam/trunk/build'
lib_basenames = [line.rstrip('\n') for line in open('lib_basenames.txt')]
lib_dirs = [diagham_build_dir + '/' + line.rstrip('\n') for line in open('lib_dirs.txt')]
static_libs = [diagham_build_dir + line.rstrip('\n') for line in open('libs.txt')]

#static_libs += ['/usr/lib/liblapack.a']
static_libs += ['-llapack']

setup(
    name='diagham4py',
    version='1.0',
    description='Python wrapper for diagham.',
    author='Niall Moran',
    author_email='niall.moran@gmail.com',
    license = 'GPLv3',
#    scripts =  [''],
    cmdclass = {'build_ext': build_ext},
    packages = ['diagham4py'],
    package_dir = {'diagham4py' : 'diagham'},
    ext_modules = [Extension("diagham", ["diagham/test.pyx"],
                             include_dirs=[numpy.get_include(),
                                           '/home/nmoran/local/DiagHam/trunk/src',
                                           '/home/nmoran/local/DiagHam/trunk/FQHE/src',
                                           '/home/nmoran/local/DiagHam/trunk/build/src'
                                          ],
                             language='c++',
                             libraries=['blas', 'lapack', 'pthread', 'mpi', 'mpi_cxx', 'gfortran'],
 #                            library_dirs=lib_dirs
                             extra_link_args=static_libs
                             )
                   ]
)
